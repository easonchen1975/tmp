//0 X.1,     ... weighting x 8   , the mask[0] :  sensor 7:0, mask[1] : sensor 15:8 
//1 X.2,			 , the mask value =  mask[1]<<8 | mask[0] =  sensor[15:14:13.......:1:0]
//2 Y.1,			 , the mask value from LSB bit 0 represent the sensor 0,  
//3 Y.2,			 , 			   bit 1 represent the sensor 1, .. etc.. 
//4 Z.1,
//5 Z.2,
// Please don't modify this line and above.  selection mask, note: the XYZ's selection mask must be the same
0, 0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0xFF
1, 0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0xFF
2, 0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0xFF
3, 0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0xFF
4, 0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0xFF
5, 0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0.0625	0xFF
