/*   CSL configuration is no longer supported.    */
/*   Your CSL configuration has been saved here as part  */
/*   of the process of converting cdb to tcf.    */

/* INPUT gpsr_ext_ram_ft.cdb */

/*  Include Header File  */
#include "gpsrcfg_csl.h"

//#error "Please read the comment below to finish updating your project"
/*
 * The following steps are required to use this file:
 *
 * 1.  Modify any source files that use these structures to
 *     `#include <gpsr_ext_ram_ftcfg_csl.h>`.  These structure definitions were
 *     previously contained in <gpsr_ext_ram_ftcfg.h>.
 * 2.  Call `CSL_init()` and `cslCfgInit()` from your application's main()
 *     function.  This is necessary to initialize CSL and the assorted
 *     CSL handles.  These functions were previously called by BIOS internally.
 * 3.  Remove the #error line above.
 * 4.  Update your linker options to include the CSL library for the device
 *     that you are using. This library name was listed in the original
 *     generated gpsr_ext_ram_ftcfg.cmd file (e.g., "-lcsl6416.lib").
 * 5.  If you are using the 54x or 55x, you will need to create an additional
 *     linker .cmd file that includes the generated gpsr_ext_ram_ftcfg.cmd file
 *     and adds section placement for the '.csldata' section.
 *
 * See the DSP/BIOS Setup Guide for more information.
 */

#pragma CODE_SECTION(cslCfgInit,".text:cslCfgInit")

/*  Config Structures */

/* EMIFB_Config emifbCfg0
 * comment                                                 <add comments here>
 * External HOLD disable (NOHOLD)                          Enable Hold
 * CLKOUT4 Enable (CLK4EN)                                 Enabled to clock
 * CLKOUT6 Enable (CLK6EN)                                 Enabled to clock
 * ECLKOUT1 Enable (EK1EN)                                 Enabled to clock
 * ECLKOUT2 Enable (EK2EN)                                 Enabled to clock
 * ECLKOUT1 High-Z Control (EK1HZ)                         High-Z during hold
 * ECLKOUT2 High-Z Control (EK2HZ)                         Clock during hold
 * ECLKOUT2 Rate (EK2RATE)                                 1/4x EMIF input clock
 * Bus Request Mode                                        access/refresh pending or in progress
 * Memory Type (MTYPE)                                     16-bit async. interf.
 * Read Strobe Width                                       0x3
 * Read Setup Width                                        0x3
 * Read Hold Width                                         0x3
 * Write Strobe Width                                      0x1
 * Write Setup Width                                       0x1
 * Write Hold Width (WRHLD-WRHLDMSB)                       0x1
 * Turn around time (TA)                                   0x1
 * Memory Type (MTYPE)                                       8-bit async. interf.
 * Read Strobe Width                                       0x3F
 * Read Setup Width                                        0xF
 * Read Hold Width                                         0x3
 * Write Strobe Width                                      0x3F
 * Write Setup Width                                       0xF
 * Write Hold Width (WRHLD - WHLDMSB)                      0x3
 * Turn around time (TA)                                   0x3
 * Memory Type (MTYPE)                                     16-bit async. interf.
 * Read Strobe Width                                       0x3
 * Read Setup Width                                        0x3
 * Read Hold Width                                         0x3
 * Write Strobe Width                                      0x3
 * Write Setup Width                                       0x3
 * Write Hold Width                                        0x3
 * Turn around time (TA)                                   0x3
 * Memory Type (MTYPE)                                     16-bit async. interf.
 * Read Strobe Width                                       0x3
 * Read Setup Width                                        0x3
 * Read Hold Width                                         0x3
 * Write Strobe Width                                      0x3
 * Write Setup Width                                       0x3
 * Write Hold Width                                        0x3
 * Turn around time (TA)                                   0x3
 * Sync. interf. data read latency (SYNCRL)                0 cycle
 * Sync. interf. data write latency (SYNCWL)               0 cycle
 * CE Extension Register (CEEXT)                           Active
 * Read Enable Enable (RENEN)                              ADS Mode
 * Synchronization Clock (SNCCLK)                          Sync. to ECLKOUT2
 * Sync. interf. data read latency (SYNCRL)                2 cycles
 * Sync. interf. data write latency (SYNCWL)               0 cycle
 * CE Extension Register (CEEXT)                           Inactive
 * Read Enable Enable (RENEN)                              ADS Mode
 * Synchronization Clock (SNCCLK)                          Sync. to ECLKOUT1
 * Sync. interf. data read latency (SYNCRL)                2 cycles
 * Sync. interf. data write latency (SYNCWL)               0 cycle
 * CE Extension Register (CEEXT)                           Inactive
 * Read Enable Enable (RENEN)                              Read Enable Mode
 * Synchronization Clock (SNCCLK)                          Sync. to ECLKOUT1
 * Sync. interf. data read latency (SYNCRL)                2 cycles
 * Sync. interf. data write latency (SYNCWL)               0 cycle
 * CE Extension Register (CEEXT)                           Inactive
 * Read Enable Enable (RENEN)                              Read Enable Mode
 * Synchronization Clock (SNCCLK)                          Sync. to ECLKOUT1
 * TRC = trc/(eclkout1 period-1) (TRC)                     0xF
 * TRP = trp/(eclkout1 period-1) (TRP)                     0x8
 * TRCD = trcd/(eclkout1 period-1) (TRCD)                  0x4
 * Initialization of all SDRAMs (INIT)                     Initialize
 * SDRAM Refresh Enable (RFEN)                             Enable
 * Column Size (SDCSZ)                                      9 addresses
 * Row Size (SDRSZ)                                        11 addresses
 * Bank Size (SDBSZ)                                       Two banks
 * Refresh Period (ECLKOU1 cycles)                         0x5DC
 * Extra Refreshes Ctrl. (XRFR)                            0x1
 * CAS Latency (TCL)                                       0x3
 * tras = TRAS + 1 (ECLKOU1 cyc.)                          0x8
 * trrd = TRRD (2 or 3 ECLKOU1 cyc.)                       0x3
 * twr  = TWR + 1 (ECLKOU1 cyc.)                           0x2
 * thzp = THZP + 1 (ECLKOU1 cyc.)                          0x3
 * READ-To-READ (ECLKOU1 cyc.)                             0x2
 * READ-To-DEAC/DEAB (ECLKOU1 cyc.)                        0x4
 * READ-To-WRITE (ECLKOU1 cyc.)                            0x6
 * READ-To-WRITE with Interrupt (BEx cyc.)                 0x3
 * WRITE-To-WRITE (ECLKOU1 cyc.)                           0x2
 * WRITE-To-DEAC/DEAB (ECLKOU1 cyc.)                       0x2
 * WRITE-To-READ (# of ECLKOU1 cyc.)                       0x2
 * Global Control Reg. (GBLCTL)                            0x0009207C
 * CE0 Space Control Reg. (CECTL0)                         0x10534313
 * CE1 Space Control Reg. (CECTL1)                         0xFFFFFF03
 * CE2 Space Control Reg. (CECTL2)                         0x30F3C313
 * CE3 Space Control Reg. (CECTL3)                         0x30F3C313
 * SDRAM Control Reg.(SDCTL)                               0x0348F000
 * SDRAM Timing Reg.(SDTIM)                                0x005DC5DC
 * SDRAM Extended Reg.(SDEXT)                              0x00175F3F
 * CE0 Space Secondary Control Reg. (CESEC0)               0x00000050
 * CE1 Space Secondary Control Reg. (CESEC1)               0x00000002
 * CE2 Space Secondary Control Reg. (CESEC2)               0x00000022
 * CE3 Space Secondary Control Reg. (CESEC3)               0x00000022
 */
EMIFB_Config emifbCfg0 = {
    0x0009207C,        /*  Global Control Reg. (GBLCTL)   */
    0x10534313,        /*  CE0 Space Control Reg. (CECTL0)   */
    0xFFFFFF03,        /*  CE1 Space Control Reg. (CECTL1)   */
    0x30F3C313,        /*  CE2 Space Control Reg. (CECTL2)   */
    0x30F3C313,        /*  CE3 Space Control Reg. (CECTL3)   */
    0x0348F000,        /*  SDRAM Control Reg.(SDCTL)   */
    0x005DC5DC,        /*  SDRAM Timing Reg.(SDTIM)   */
    0x00175F3F,        /*  SDRAM Extended Reg.(SDEXT)   */
    0x00000050,        /*  CE0 Space Secondary Control Reg. (CESEC0)  */
    0x00000002,        /*  CE1 Space Secondary Control Reg. (CESEC1)  */
    0x00000022,        /*  CE2 Space Secondary Control Reg. (CESEC2)  */
    0x00000022         /*  CE3 Space Secondary Control Reg. (CESEC3)  */
};

/*  Handles  */



/*
 *  ======== cslCfgInit() ========  
 */
void cslCfgInit()
{
    EMIFB_config(&emifbCfg0);
}
