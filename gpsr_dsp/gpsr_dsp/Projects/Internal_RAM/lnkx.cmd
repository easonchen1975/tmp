
-lcsl6416.lib
/*
-l gpsr_int_ramcfg.cmd
*/
MEMORY 
{
/*
   IDRAM1       : origin = 0x60000,     len = 0x57000, fill=0

   IDRAM1       : origin = 0x67000,     len = 0x4f000, fill=0
  */
   IDRAM1       : origin = 0x67000,     len = 0x5f000, fill=0
}

SECTIONS
{
		FrameworkState        > IDRAM1
		SampleManagerState    > IDRAM1
		SatelliteManagerState > IDRAM1
		NavTaskState          > IDRAM1
		MainManagerState      > IDRAM1
		OPManagerState        > IDRAM1		
}

