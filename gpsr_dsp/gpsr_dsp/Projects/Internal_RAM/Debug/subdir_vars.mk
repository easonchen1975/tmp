################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnkx.cmd 

TCF_SRCS += \
../gpsr_int_ram.tcf 

S??_SRCS += \
./gpsr_int_ramcfg.s?? 

C_SRCS += \
./gpsr_int_ramcfg_c.c \
../gpsrcfg_csl.c 

OBJS += \
./gpsr_int_ramcfg.obj \
./gpsr_int_ramcfg_c.obj \
./gpsrcfg_csl.obj 

GEN_MISC_FILES += \
./gpsr_int_ram.cdb 

GEN_HDRS += \
./gpsr_int_ramcfg.h \
./gpsr_int_ramcfg.h?? 

S??_DEPS += \
./gpsr_int_ramcfg.d 

C_DEPS += \
./gpsr_int_ramcfg_c.d \
./gpsrcfg_csl.d 

GEN_CMDS += \
./gpsr_int_ramcfg.cmd 

GEN_FILES += \
./gpsr_int_ramcfg.cmd \
./gpsr_int_ramcfg.s?? \
./gpsr_int_ramcfg_c.c 

GEN_HDRS__QUOTED += \
"gpsr_int_ramcfg.h" \
"gpsr_int_ramcfg.h??" 

GEN_MISC_FILES__QUOTED += \
"gpsr_int_ram.cdb" 

GEN_FILES__QUOTED += \
"gpsr_int_ramcfg.cmd" \
"gpsr_int_ramcfg.s??" \
"gpsr_int_ramcfg_c.c" 

C_DEPS__QUOTED += \
"gpsr_int_ramcfg_c.d" \
"gpsrcfg_csl.d" 

S??_DEPS__QUOTED += \
"gpsr_int_ramcfg.d" 

OBJS__QUOTED += \
"gpsr_int_ramcfg.obj" \
"gpsr_int_ramcfg_c.obj" \
"gpsrcfg_csl.obj" 

S??_SRCS__QUOTED += \
"./gpsr_int_ramcfg.s??" 

S??_OBJS__QUOTED += \
"gpsr_int_ramcfg.obj" 

C_SRCS__QUOTED += \
"./gpsr_int_ramcfg_c.c" \
"../gpsrcfg_csl.c" 


