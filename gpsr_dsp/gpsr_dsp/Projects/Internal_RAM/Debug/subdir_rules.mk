################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
gpsr_int_ramcfg.cmd: ../gpsr_int_ram.tcf
	@echo 'Building file: $<'
	@echo 'Invoking: TConf'
	"C:/ti/bios_5_42_02_10/xdctools/tconf" -b -Dconfig.importPath="C:/ti/bios_5_42_02_10/packages;" "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpsr_int_ramcfg.s??: | gpsr_int_ramcfg.cmd
gpsr_int_ramcfg_c.c: | gpsr_int_ramcfg.cmd
gpsr_int_ramcfg.h: | gpsr_int_ramcfg.cmd
gpsr_int_ramcfg.h??: | gpsr_int_ramcfg.cmd
gpsr_int_ram.cdb: | gpsr_int_ramcfg.cmd

gpsr_int_ramcfg.obj: ./gpsr_int_ramcfg.s?? $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="gpsr_int_ramcfg.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpsr_int_ramcfg_c.obj: ./gpsr_int_ramcfg_c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="gpsr_int_ramcfg_c.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpsrcfg_csl.obj: ../gpsrcfg_csl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="gpsrcfg_csl.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


