################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
AlmEph.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/AlmEph.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/AlmEph.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Atan2.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Atan2.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Atan2.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

AtmosphericDelay.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/AtmosphericDelay.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/AtmosphericDelay.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

BasicDSPEvents.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/BasicDSPEvents.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/BasicDSPEvents.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

BuiltInTest.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/BuiltInTest.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/BuiltInTest.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Control.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Control.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Control.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

DataConversion.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/DataConversion.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/DataConversion.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

DataProtocol.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/DataProtocol.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/DataProtocol.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Div31Fast.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Div31Fast.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Div31Fast.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

DynamicsModel.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/DynamicsModel.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/DynamicsModel.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Exp.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Exp.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Exp.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

FFT32.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/FFT32.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/FFT32.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

FIFO.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/FIFO.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --symdebug:skeletal --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --auto_inline=0 --opt_for_speed=2 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/FIFO.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

FindBit.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/FindBit.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/FindBit.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

FrameDecoderGPS.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/FrameDecoderGPS.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/FrameDecoderGPS.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Framework.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Framework.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Framework.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Fsearch.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Fsearch.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Fsearch.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GPSChan.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/GPSChan.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/GPSChan.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Hardware.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Hardware.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Hardware.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

L_mpy_ll.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/L_mpy_ll.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/L_mpy_ll.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

L_sqrt_l.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/L_sqrt_l.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/L_sqrt_l.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

L_sqrt_s.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/L_sqrt_s.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/L_sqrt_s.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Log2.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Log2.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Log2.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

MainManager.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/MainManager.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -Ooff -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --auto_inline=0 --opt_for_speed=0 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/MainManager.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

NavTask.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/NavTask.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/NavTask.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

OPManager.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/OPManager.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/OPManager.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ProtocolBin.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/ProtocolBin.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -Ooff -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --auto_inline=0 --opt_for_speed=0 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/ProtocolBin.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

RawMeasurForm.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/RawMeasurForm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/RawMeasurForm.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

SVCordComp.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/SVCordComp.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/SVCordComp.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

SampleDriver.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/SampleDriver.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/SampleDriver.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

SatelliteManager.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/SatelliteManager.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/SatelliteManager.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

SunMoonGravitation.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/SunMoonGravitation.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/SunMoonGravitation.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

Terminal.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/Terminal.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --auto_inline=0 --opt_for_speed=0 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/Terminal.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TestProtocol.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/TestProtocol.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/TestProtocol.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TimeEx.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/TimeEx.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/TimeEx.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

UartTerminal.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/UartTerminal.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -Ooff -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --auto_inline=0 --opt_for_speed=0 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/UartTerminal.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

asm.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/asm.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/asm.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

assumedDynamics.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/assumedDynamics.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/assumedDynamics.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

atmosphere.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/atmosphere.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/atmosphere.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

caCode.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/caCode.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/caCode.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

can_drv.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/can_drv.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/can_drv.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

expTbl.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/expTbl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/expTbl.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

flash.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/flash.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/flash.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

geopotential.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/geopotential.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/geopotential.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

integration.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/integration.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/integration.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O0 -g --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --auto_inline=0 --opt_for_speed=0 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/main.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

matrix.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/matrix.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/matrix.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

matrix2.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/OP/matrix2.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/matrix2.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

plldll.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/plldll.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/plldll.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

satellite.obj: D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources/satellite.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Common_Files/Sources" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM/Debug" --include_path="C:/ti/bios_5_42_02_10/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_02_10/packages/ti/rtdx/include/c6000" --include_path="D:/workspace/uart_no_output/0928_support_RXID_gpsr_mems_aligned/Projects/Internal_RAM" --define="_DEBUG" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="NEW_TMTC" --define="FASTER_TRACKING" --define="DELAYED_OPMGRINIT" --undefine=CAN_PORT_TMTC --quiet --display_error_number --interrupt_threshold=8000 --asm_directory="../Asm" --preproc_with_compile --preproc_dependency="Source/satellite.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


