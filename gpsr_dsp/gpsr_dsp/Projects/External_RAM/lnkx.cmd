
-lcsl6416.lib
/*
-l gpsr_ext_ramcfg.cmd
*/
MEMORY 
{
   IDRAM1       : origin = 0x65000,     len = 0x4f000, fill=0
}

SECTIONS
{
		FrameworkState        > EXTSRAM1
		SampleManagerState    > EXTSRAM1
		SatelliteManagerState > EXTSRAM1
		NavTaskState          > EXTSRAM1
		MainManagerState      > EXTSRAM1
		OPManagerState        > EXTSRAM1
		fast_text				> EXTSRAM1
		fast_data				> IDRAM1
}

