################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnkx.cmd 

TCF_SRCS += \
../gpsr_ext_ram.tcf 

S??_SRCS += \
./gpsr_ext_ramcfg.s?? 

C_SRCS += \
./gpsr_ext_ramcfg_c.c \
../gpsrcfg_csl.c 

OBJS += \
./gpsr_ext_ramcfg.obj \
./gpsr_ext_ramcfg_c.obj \
./gpsrcfg_csl.obj 

GEN_MISC_FILES += \
./gpsr_ext_ram.cdb 

GEN_HDRS += \
./gpsr_ext_ramcfg.h \
./gpsr_ext_ramcfg.h?? 

S??_DEPS += \
./gpsr_ext_ramcfg.d 

C_DEPS += \
./gpsr_ext_ramcfg_c.d \
./gpsrcfg_csl.d 

GEN_CMDS += \
./gpsr_ext_ramcfg.cmd 

GEN_FILES += \
./gpsr_ext_ramcfg.cmd \
./gpsr_ext_ramcfg.s?? \
./gpsr_ext_ramcfg_c.c 

GEN_HDRS__QUOTED += \
"gpsr_ext_ramcfg.h" \
"gpsr_ext_ramcfg.h??" 

GEN_MISC_FILES__QUOTED += \
"gpsr_ext_ram.cdb" 

GEN_FILES__QUOTED += \
"gpsr_ext_ramcfg.cmd" \
"gpsr_ext_ramcfg.s??" \
"gpsr_ext_ramcfg_c.c" 

C_DEPS__QUOTED += \
"gpsr_ext_ramcfg_c.d" \
"gpsrcfg_csl.d" 

S??_DEPS__QUOTED += \
"gpsr_ext_ramcfg.d" 

OBJS__QUOTED += \
"gpsr_ext_ramcfg.obj" \
"gpsr_ext_ramcfg_c.obj" \
"gpsrcfg_csl.obj" 

S??_SRCS__QUOTED += \
"./gpsr_ext_ramcfg.s??" 

S??_OBJS__QUOTED += \
"gpsr_ext_ramcfg.obj" 

C_SRCS__QUOTED += \
"./gpsr_ext_ramcfg_c.c" \
"../gpsrcfg_csl.c" 


