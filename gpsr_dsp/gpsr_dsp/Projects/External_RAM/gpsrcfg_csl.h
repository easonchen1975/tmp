/*   CSL configuration is no longer supported.    */
/*   Your CSL configuration has been saved here as part  */
/*   of the process of converting cdb to tcf.    */

/* INPUT gpsr_ext_ram.cdb */

#define CHIP_6416 1

/*  Include Header Files  */
#include <csl.h>
#include <csl_emifb.h>

#ifdef __cplusplus
extern "C" {
#endif

extern far EMIFB_Config emifbCfg0;
extern far void cslCfgInit();

#ifdef __cplusplus
}
#endif /* extern "C" */
