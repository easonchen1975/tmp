################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
gpsr_ext_ramcfg.cmd: ../gpsr_ext_ram.tcf
	@echo 'Building file: $<'
	@echo 'Invoking: TConf'
	"C:/ti/bios_5_42_00_07/xdctools/tconf" -b -Dconfig.importPath="C:/ti/bios_5_42_00_07/packages;" "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpsr_ext_ramcfg.s??: | gpsr_ext_ramcfg.cmd
gpsr_ext_ramcfg_c.c: | gpsr_ext_ramcfg.cmd
gpsr_ext_ramcfg.h: | gpsr_ext_ramcfg.cmd
gpsr_ext_ramcfg.h??: | gpsr_ext_ramcfg.cmd
gpsr_ext_ram.cdb: | gpsr_ext_ramcfg.cmd

gpsr_ext_ramcfg.obj: ./gpsr_ext_ramcfg.s?? $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Projects/External_RAM" --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Common_Files/Sources" --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Projects/External_RAM/Release" --include_path="C:/ti/bios_5_42_00_07/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_00_07/packages/ti/rtdx/include/c6000" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="EXT_SRAM" --define="NEW_TMTC" --quiet --display_error_number --interrupt_threshold=8000 --preproc_with_compile --preproc_dependency="gpsr_ext_ramcfg.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpsr_ext_ramcfg_c.obj: ./gpsr_ext_ramcfg_c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Projects/External_RAM" --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Common_Files/Sources" --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Projects/External_RAM/Release" --include_path="C:/ti/bios_5_42_00_07/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_00_07/packages/ti/rtdx/include/c6000" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="EXT_SRAM" --define="NEW_TMTC" --quiet --display_error_number --interrupt_threshold=8000 --preproc_with_compile --preproc_dependency="gpsr_ext_ramcfg_c.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpsrcfg_csl.obj: ../gpsrcfg_csl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccsv6/tools/compiler/c6000_7.4.16/bin/cl6x" -mv6400 --abi=coffabi -O2 --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Projects/External_RAM" --include_path="C:/ti/ccsv6/tools/compiler/c6000_7.4.16/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --include_path="C:/ti/C6xCSL/include" --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Common_Files/Sources" --include_path="E:/Alex/GPSR_CCSv6/Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323/Projects/External_RAM/Release" --include_path="C:/ti/bios_5_42_00_07/packages/ti/bios/include" --include_path="C:/ti/bios_5_42_00_07/packages/ti/rtdx/include/c6000" --define="__CODECOMPOSER_C6XX" --define="USE_DSPBIOS" --define="EXT_SRAM" --define="NEW_TMTC" --quiet --display_error_number --interrupt_threshold=8000 --preproc_with_compile --preproc_dependency="gpsrcfg_csl.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


