/*****************************************************************************
 *    Copyright (c) 2007 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains specifi� constants for OP
 *   @file                   ControlOP.h
 *   @author                 Dmitry Churikov
 *   @date                   09.01.2007
 *   @version                1.0
 */
/*****************************************************************************/
#ifndef __CONTROLOP_H_
#define __CONTROLOP_H_

#include "..\Sources\Control.h"

#define L1_C (f0_GPS*l_cLight)

#define LOCK_OBJ(pObject)	while ( (pObject)->flag_bl );	(pObject)->flag_bl = 1;

#define UNLOCK_OBJ(pObject)	(pObject)->flag_bl = 0;

#endif 
