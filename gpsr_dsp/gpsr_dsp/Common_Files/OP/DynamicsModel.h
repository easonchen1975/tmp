/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains dynamics model parameters
 *   @file                   DynamicsModel.h
 *   @author                 Dmitry Churikov
 *   @date                   08.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#ifndef __DYNAMICSMODEL_H
#define __DYNAMICSMODEL_H


#include "atmosphere.h"
#include "geopotential.h"
#include "SunMoonGravitation.h"

// Indexes of variables in state vector
#define I_VX  (0)
#define I_VY  (1)
#define I_VZ  (2)
#define I_X   (3)
#define I_Y   (4)
#define I_Z   (5)
#define I_B   (6)
#define I_D   (7)
#define I_T   (6)

// ------------------------------------------------------------------------
///  @brief   Dynamics Model object. 
// ------------------------------------------------------------------------
typedef struct _tDynamicsModel
{
  tGeopotential GP;         ///< Geopotential object to Earth's gravitation force calculation
  tSunMoonData  SMD;        ///< SMD object to Sun and Moon gravitation perturbation calculation
  tAtmosphere   Atm;        ///< TAtm object to atmospheric drag calculation
  
  int32         GP_N;       ///< (N,N) - geopotential resolution (harmonics count)
  int32         SM_fUsing;  ///< Flag of Sun and Moon gravity perturbation account
  float64       OBJ_Sigma;  ///< Object ballistic coefficient

} tDynamicsModel;


//===================================================================================
///  @brief   Initialization Dynamics Model object
///
///  @param[in] pDM - pointer to Dynamics Model object
//===================================================================================
EXTERN_C void DynamicsModel_Init(tDynamicsModel* pDM);

//===================================================================================
///  @brief   Assumed dynamics model parameters initialization
///
///  @param[in] pDM - pointer to Dynamics Model object
//===================================================================================
EXTERN_C void assumedDynamics_Init(tDynamicsModel* pDM);

//==========================================================================================
///  @brief   The assumed dynamics model equation
///
///  @param[in] Q         - pointer to current satellite vector {Vx,Vy,Vz,x,y,z, time_drift}
///  @param[in] dQ        - pointer to state vector derivatives (it is calculated)
///  @param[in] ptrParams - pointer of tDynamicsModel object
//==========================================================================================
EXTERN_C void assumedDynamics(float64 *Q, float64 *dQ, int32 ptrParams);

//===================================================================================
///  @brief   True dynamics model parameters initialization
///
///  @param[in] pDM - pointer of Dynamics Model object
//===================================================================================
EXTERN_C void trueDynamics_Init(tDynamicsModel* pDM);

//======================================================================================
///  @brief   The true dynamics model equation
///
///  @param[in] Q         - pointer to current state vector {x,y,z,Vx,Vy,Vz, time_drift}
///  @param[in] dQ        - pointer to state vector derivatives (it is calculated)
///  @param[in] ptrParams - pointer to Dynamics Model object
//======================================================================================
EXTERN_C void trueDynamics(float64 *Q, float64 *dQ, int32 ptrParams);

#endif
