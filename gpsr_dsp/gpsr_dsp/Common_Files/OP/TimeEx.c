/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains  functions to time processing and 
 *                           conversion
 *   @file                   TimeEx.c
 *   @author                 Dmitry Churikov
 *   @date                   25.12.2006
 *   @version                1.0
 */
/*****************************************************************************/

#include "TimeEx.h"


//===================================================================================
///  @brief   The function calculates MJT from J2000.0 by UTC
///
///  @param[in]  utc - pointer to tGPSTime structure of UTC
///  @param[out] mjd - pointer to calculated MJD from J2000.0 as days.part_of_day
//===================================================================================
void MJD_From_UTCEx(tGPSTime *utc, float64 *mjd)
{
  float64 mjd0, t1, t2;

  /// GPS time origin in J2000 definition
  mjd0 = JD_UT_OF_GPS_0 - JD_2000_0;

  /// Current time calculation
  t1 = utc->wn * T_DAYS_PER_WEEK;
  t2 = utc->t * T_DAYS_PER_SECOND;
  mjd0 += t1 + t2;

  *mjd = mjd0;
} 
// MJD_From_UTCEx

