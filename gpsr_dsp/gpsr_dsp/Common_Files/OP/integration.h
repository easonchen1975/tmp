/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Cauchy problem solution functions
 *   @file                   integration.h
 *   @author                 Dmitry Churikov
 *   @date                   11.12.2006
 *   @version                1.0
 */
/*****************************************************************************/

#ifndef __INTEGRATION_H
#define __INTEGRATION_H

#include "../sources/comtypes.h"

/// RightPart functions prototype
typedef void (*tRightPart)(float64 * ,float64 *, int32);//(Q,dQ,ptrParam)

/// Implementation parameters
#define CP_AVAL_VECTOR_SIZE  (7) ///< Max count components of state vector to integration


//============================================================================================
///  @brief   
///
///  @param[in] fp     - TRightPart-function pointer
///  @param[in] Q      - pointer to start vector to Cauchy problem solution
///  @param[in] cnt    - count of Q-vector components, it must be no more than 
///                      CP_AVAL_VECTOR_SIZE. 
///                      This condition is not checked into the function body.
///  @param[in] h      - integration step
///  @param[in] pParam - addition parameter of RightPart-function
///  @param[in] dQ     - pointer to Q vector increase
//============================================================================================
EXTERN_C void rk4(tRightPart fp, float64 *Q, int32 cnt, float64 h, int32 pParam, float64 *dQ);

#endif // __INTEGRATION_H
