/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains functions to gravitation force calculation
 *   @file                   geopotential.c
 *   @author                 Dmitry Churikov
 *   @date                   06.12.2006
 *   @version                1.0
 */
/*****************************************************************************/

#include "geopotential.h"
#include "Tables.h"

#include <math.h>
#include <string.h>
#include <stdio.h>

// ------------------------------------------------------------------------
// Indexes of phisical vectors in Cartesian frame
// ------------------------------------------------------------------------
#define X  (0)
#define Y  (1)
#define Z  (2)

//===================================================================================
///  @brief   The function calculates matrix of Normalized Associated Legendre 
///           Functions and matrix of their derivatives of fi angle.
///           The NALF's and their derivatives argument is sin(fi); fi = [-pi/2;pi/2]
///
///  @param[in] pGP - pointer to Geopotential object
///  @param[in] N   - polynomial matrix rank (N,N)
///  @param[in] z   - polynomial argument (it must be sin(fi))
//===================================================================================
static void NALF_matrix(tGeopotential* pGP, int32 N, float64 z);



//===================================================================================
///  @brief   The function calculates matrix of Normalized Associated Legendre 
///           Functions and matrix of their derivatives of fi angle.
///           The NALF's and their derivatives argument is sin(fi); fi = [-pi/2;pi/2]
///
///  @param[in] pGP - pointer to Geopotential object
///  @param[in] N   - polynomial matrix rank (N,N)
///  @param[in] z   - polynomial argument (it must be sin(fi))
//===================================================================================
static void NALF_matrix(tGeopotential* pGP, int32 N, float64 z)
{
  int32 n, m;
  float64 val, val_1_2, cosfi, c1;

  /// Initial polynomial matrix calculation
  memset(&pGP->P[0][0], 0, sizeof(pGP->P));
  val = 1 - z*z;      val_1_2= sqrt(val);     cosfi = val_1_2;    //fi = [-pi/2;pi/2]
  pGP->P[0][0] = 1.;
  pGP->P[1][0] = z;               pGP->P[1][1] = -val_1_2;
  pGP->P[2][0] = 0.5*(3*z*z - 1); pGP->P[2][1] = -3*z*val_1_2;    pGP->P[2][2] = 3*val;

  /// Initial derivatives matrix calculation
  memset(&pGP->dPfi[0][0], 0, sizeof(pGP->dPfi));
  pGP->dPfi[0][0] = 0;
  pGP->dPfi[1][0] = cosfi;        pGP->dPfi[1][1] = z;   //(sinfi*cosfi/cosfi)
  pGP->dPfi[2][0] = 3*z*cosfi;    pGP->dPfi[2][1] = 3*(2*z*z - 1);   pGP->dPfi[2][2] = -6*z*cosfi;

  /// Recurrence calculation
  for (n = 3; n <= N; n++)
  {
    for (m = 0; m <= (n-2); m++)
    {
      c1 = 1./(n-m);
      /// polynomials
      pGP->P[n][m] = ((2*n-1)*z*pGP->P[n-1][m] - (float64)(m+n-1)*pGP->P[n-2][m]) * c1;
      /// derivatives 
      pGP->dPfi[n][m] = ((2*n-1)*(z*pGP->dPfi[n-1][m] + cosfi*pGP->P[n-1][m]) 
                - (m+n-1)*pGP->dPfi[n-2][m]) * c1;
    }

     /// Last two polynomials and derivatives calculation
     //  m = m+1;
     pGP->P[n][m] = z*(2*n-1)*pGP->P[m][m];
     pGP->dPfi[n][m] = (2*n-1)*(cosfi*pGP->P[m][m] + z*pGP->dPfi[m][m]);
   
     pGP->P[n][n] = -pGP->P[n-1][n-1]*(2*n-1)*cosfi;
     pGP->dPfi[n][n] = (2*n-1)*(2*n-3)*(pGP->dPfi[n-2][n-2]*val - 2*z*cosfi*pGP->P[n-2][n-2]);
  }
    
  /// Matrices normalization
  for  (n = 0; n <= (N); n++)
     for (m = 0; m <= n; m++)
     {
      pGP->P[n][m] *= pGP->NormFact[n][m];
      pGP->dPfi[n][m] *= pGP->NormFact[n][m];
     }
}
// NALF_matrix


//===================================================================================
///  @brief   The function calculates a gravitation acceleration vector
/// 
///  @param[in] pGP - pointer to Geopotential object
///  @param[in] r   - pointer to current radius-vector
///  @param[in] N   - geopotential level (N,N)
///  @param[in] g   - pointer to calculated the gravitation acceleration vector
//===================================================================================
void Geopotential_Gravitation(tGeopotential *pGP,
                              float64       *r,
                              int32          N,
                              float64       *g)
{
  int32 n, m;
  float64 R, r1, ro, ro1;
  float64 sinfi, sinl, cosl, sml, cml;
  float64 a_r_0, a_r_n;
  float64 C1, C2, S1, S2, S3;
  float64 dUR, dUfi, dUl;
  
  /// available geopotential level checking
  if(N > pGP->N)  N = pGP->N;
  
  /// magnitude, slope and projection calculation
  r1 = r[X]*r[X] + r[Y]*r[Y];
  R = sqrt(r1 + r[Z]*r[Z]);
  r1 = sqrt(r1);
  ro = 1/R;   ro1 = 1/r1;
  sinfi = ro*r[Z];  //cosfi = ro*r1;  // cosfi is positive always
  sinl = ro1*r[Y];  cosl = ro1*r[X];

  /// Normalized Associated Legendre Polynomial matrix 
  /// and matrix of its derivatives of fi calculation
  NALF_matrix( pGP, N, sinfi);

  /// Components of gravitation gradient calculation
  a_r_0 = EARTH_R0*ro;    a_r_n = a_r_0;
  dUR = 1;     dUfi = 0;    dUl = 0;
  for (n = 2; n <= N; n++)
    {
    sml = 0;  cml = 1;  ///< sin(0*l)and cos(0*l) calculation
    S1 = 0;    S2 = 0;  S3 = 0;
    
    for (m = 0; m <= n; m++)
    {
      C1 = pGP->C[n][m]*cml + pGP->S[n][m]*sml;
      S1 = S1 + C1 * pGP->P[n][m];
      S2 = S2 + C1 * pGP->dPfi[n][m];
      C2 = pGP->S[n][m]*cml - pGP->C[n][m]*sml;
      S3 = S3 + m*C2 * pGP->P[n][m];

      /// sin((m+1)*l)and cos((m+1)*l) calculation
      C1 = sml;
      sml = sinl*cml + cosl*sml;
      cml = cosl*cml - sinl*C1;
    }
    
    /// differential of geopotenpial calculation
    a_r_n = a_r_n * a_r_0;
    dUR = dUR + (n+1)*S1*a_r_n;
    dUfi = dUfi + S2*a_r_n;
    dUl = dUl + S3*a_r_n;
  }

  dUR = -EARTH_MU*ro*ro*dUR;
  dUfi=  EARTH_MU*ro*dUfi;
  dUl =  EARTH_MU*ro*dUl;

  /// Gravitation acceleration calculation
  C1 = dUfi*ro*ro*ro1;
  C2 = dUl*ro1*ro1;
  g[X] = dUR*r[X]*ro - C1*r[X]*r[Z] - C2*r[Y];
  g[Y] = dUR*r[Y]*ro - C1*r[Y]*r[Z] + C2*r[X];
  g[Z] = dUR*r[Z]*ro + C1*r1*r1;

} 
// Geopotential_Gravitation


//===================================================================================
///  @brief   Initialization of Geopotential object, geopotencial resolution 
///           up to 18x18 is supported
///
///  @param[in] pGP - pointer to Geopotential object
//===================================================================================
void Geopotential_Init(tGeopotential* pGP)
{
  float64 f, c;
  int32 n, m, s;
  int i, j, k;

  /// zero object
  memset (pGP, 0, sizeof (tGeopotential));

  /// preferences
  pGP->N = GP_AVAL_LEVEL;

  /// Norming factors calculation
  for(n = 0; n <= pGP->N; n++)
  {
    c = 2*n+1;
    pGP->NormFact[n][0]= sqrt(c);
    
    f = 1.;   c *= 2.;  s = -1;
    for (m = 1; m <= n; m++)
    {
      f /= (float64)((n-m+1)*(n+m));
      pGP->NormFact[n][m]= (float64)s*sqrt(c*f);
      s *= -1;
    }
    }

  /// Normalized gravitational coefficients initialization
  k= 0;
  for(i=2; i<19; i++)
  {
    for(j=0; j<=i; j++)
    {
      pGP->C[i][j] = GP_C[k];
      pGP->S[i][j] = GP_S[k];
      k++;
    }
  }
  return;

} 
// Geopotential_Init

