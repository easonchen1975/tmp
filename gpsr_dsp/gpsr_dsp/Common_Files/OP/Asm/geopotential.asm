;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:48:53 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("geopotential.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt"), DW_AT_symbol_name("_sqrt")
	.dwattr DW$1, DW_AT_type(*DW$T$17)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$1


DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("memset"), DW_AT_symbol_name("_memset")
	.dwattr DW$3, DW_AT_type(*DW$T$3)
	.dwattr DW$3, DW_AT_declaration(0x01)
	.dwattr DW$3, DW_AT_external(0x01)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$24)
	.dwendtag DW$3

	.global	_GP_C
	.sect	".const"
	.align	8
_GP_C:
	.word	06ffd7f28h,0bf3fbaf2h		; _GP_C[0] @ 0
	.word	0e52d1d89h,0bde9b30ah		; _GP_C[1] @ 64
	.word	0554d988ch,03ec47605h		; _GP_C[2] @ 128
	.word	01846374fh,03eb00f60h		; _GP_C[3] @ 192
	.word	0282cd6d4h,03ec1075eh		; _GP_C[4] @ 256
	.word	0803326c2h,03eae5ab1h		; _GP_C[5] @ 320
	.word	08ab4206eh,03ea831f7h		; _GP_C[6] @ 384
	.word	02e7930d4h,03ea21d7bh		; _GP_C[7] @ 448
	.word	0b54aec3dh,0bea1fef7h		; _GP_C[8] @ 512
	.word	0fd089947h,03e9788e0h		; _GP_C[9] @ 576
	.word	01e688921h,03eb09f55h		; _GP_C[10] @ 640
	.word	05c778e37h,0be894ee6h		; _GP_C[11] @ 704
	.word	0cef292c7h,03e726581h		; _GP_C[12] @ 768
	.word	017102905h,0be70ab90h		; _GP_C[13] @ 832
	.word	0fd403e9fh,03ea5e466h		; _GP_C[14] @ 896
	.word	0e597a4dah,0be9e5488h		; _GP_C[15] @ 960
	.word	06195885dh,0be93d13eh		; _GP_C[16] @ 1024
	.word	0d72d392eh,03e877bfdh		; _GP_C[17] @ 1088
	.word	0788ebec8h,0be842084h		; _GP_C[18] @ 1152
	.word	02b73be25h,0be746cb9h		; _GP_C[19] @ 1216
	.word	05a018f6eh,03e69dce1h		; _GP_C[20] @ 1280
	.word	0972b36c4h,03e6eb1cfh		; _GP_C[21] @ 1344
	.word	06847dc9dh,0be772499h		; _GP_C[22] @ 1408
	.word	0ca48a3eeh,0be91ed50h		; _GP_C[23] @ 1472
	.word	0a030e5fch,03e44c786h		; _GP_C[24] @ 1536
	.word	0636cbec3h,03e786c06h		; _GP_C[25] @ 1600
	.word	013d8a16eh,03e92c82eh		; _GP_C[26] @ 1664
	.word	0a4440989h,03e9620f4h		; _GP_C[27] @ 1728
	.word	0f1627beeh,03e90cdd0h		; _GP_C[28] @ 1792
	.word	0baf475f5h,0be92766dh		; _GP_C[29] @ 1856
	.word	0d5307b93h,03e20a4f2h		; _GP_C[30] @ 1920
	.word	029f854ach,0be98151dh		; _GP_C[31] @ 1984
	.word	07ae972bah,03e12c203h		; _GP_C[32] @ 2048
	.word	0cff96471h,03e6aaac0h		; _GP_C[33] @ 2112
	.word	0a32a1568h,03e591041h		; _GP_C[34] @ 2176
	.word	01b9874fdh,03e758e07h		; _GP_C[35] @ 2240
	.word	0adfc8fb2h,0be549a4bh		; _GP_C[36] @ 2304
	.word	0a3f6a79ah,0be906a32h		; _GP_C[37] @ 2368
	.word	0caddc1bfh,0be5b6b12h		; _GP_C[38] @ 2432
	.word	0c69dfe63h,0be71a55ah		; _GP_C[39] @ 2496
	.word	0a6215a1fh,03e720f86h		; _GP_C[40] @ 2560
	.word	01ca735efh,0be80a7c9h		; _GP_C[41] @ 2624
	.word	0c8e244ddh,03e5db643h		; _GP_C[42] @ 2688
	.word	0cff99906h,03e833ec1h		; _GP_C[43] @ 2752
	.word	0fb762219h,03e57de36h		; _GP_C[44] @ 2816
	.word	0ee9065bbh,0be859570h		; _GP_C[45] @ 2880
	.word	0a1e73771h,0be4354c9h		; _GP_C[46] @ 2944
	.word	0e43ad471h,0be51d782h		; _GP_C[47] @ 3008
	.word	0ebf29a0ah,03e70d44fh		; _GP_C[48] @ 3072
	.word	0624b93e8h,0be7fc612h		; _GP_C[49] @ 3136
	.word	0c904e4f1h,03e894a9ch		; _GP_C[50] @ 3200
	.word	02a36687ch,0be69a25fh		; _GP_C[51] @ 3264
	.word	0c8afc942h,03e6c4058h		; _GP_C[52] @ 3328
	.word	0359a8fd1h,03e766adfh		; _GP_C[53] @ 3392
	.word	008d11ee1h,0be794c38h		; _GP_C[54] @ 3456
	.word	0ed1a38d1h,0be3da179h		; _GP_C[55] @ 3520
	.word	0a347ddedh,0be7691b0h		; _GP_C[56] @ 3584
	.word	0a0abd4d7h,0be6a7d2eh		; _GP_C[57] @ 3648
	.word	09a4b9d05h,0be642e20h		; _GP_C[58] @ 3712
	.word	02f2dc976h,03e416d0dh		; _GP_C[59] @ 3776
	.word	0ad7e697ch,03e65bd49h		; _GP_C[60] @ 3840
	.word	074a27f30h,03e80d7d9h		; _GP_C[61] @ 3904
	.word	05c75f127h,03e7afcf6h		; _GP_C[62] @ 3968
	.word	0d4a8e857h,0be6b5c13h		; _GP_C[63] @ 4032
	.word	0d8da3a45h,03e50498bh		; _GP_C[64] @ 4096
	.word	0500ce85bh,03e54013eh		; _GP_C[65] @ 4160
	.word	080115f61h,0be60a2d6h		; _GP_C[66] @ 4224
	.word	07d427d13h,0be64ea59h		; _GP_C[67] @ 4288
	.word	090edb4d8h,03e64491ah		; _GP_C[68] @ 4352
	.word	0a4aa053ah,0be146373h		; _GP_C[69] @ 4416
	.word	0046c7dffh,03e31ad35h		; _GP_C[70] @ 4480
	.word	0c6b3f177h,0be39b398h		; _GP_C[71] @ 4544
	.word	04c679adeh,0be60dec2h		; _GP_C[72] @ 4608
	.word	0725f1b4fh,0be6c04b3h		; _GP_C[73] @ 4672
	.word	00991212ch,03e68b6edh		; _GP_C[74] @ 4736
	.word	0b6af784ah,03e6440ebh		; _GP_C[75] @ 4800
	.word	09757f8f7h,0be6d06b4h		; _GP_C[76] @ 4864
	.word	092db94fch,03e4eb463h		; _GP_C[77] @ 4928
	.word	0c06e49bbh,03e652709h		; _GP_C[78] @ 4992
	.word	08ee66f19h,0be727065h		; _GP_C[79] @ 5056
	.word	0a0cf7fb7h,03e609c83h		; _GP_C[80] @ 5120
	.word	01e638a7fh,03e2d562dh		; _GP_C[81] @ 5184
	.word	0e7f00d0eh,0be5411bdh		; _GP_C[82] @ 5248
	.word	064110b5bh,0be5b3f8fh		; _GP_C[83] @ 5312
	.word	0f2226294h,03e66b406h		; _GP_C[84] @ 5376
	.word	0c7a0b338h,0be3a86cch		; _GP_C[85] @ 5440
	.word	0e1ecbaadh,03e4828eah		; _GP_C[86] @ 5504
	.word	0b2736020h,0be256f47h		; _GP_C[87] @ 5568
	.word	0556feb84h,03e66b56ch		; _GP_C[88] @ 5632
	.word	0bd1ee7d3h,0be6b9272h		; _GP_C[89] @ 5696
	.word	03f6ee6e9h,03e6e05d4h		; _GP_C[90] @ 5760
	.word	04d575248h,0be578dbfh		; _GP_C[91] @ 5824
	.word	0689ecf4ch,0be2af3b6h		; _GP_C[92] @ 5888
	.word	0bf013f17h,03e6fad93h		; _GP_C[93] @ 5952
	.word	0ea6c2788h,0be634694h		; _GP_C[94] @ 6016
	.word	0781c4219h,03e25bb93h		; _GP_C[95] @ 6080
	.word	0c64c8eedh,0be451cedh		; _GP_C[96] @ 6144
	.word	065788821h,03e5a8e70h		; _GP_C[97] @ 6208
	.word	087b5407bh,03e660775h		; _GP_C[98] @ 6272
	.word	096bf3dffh,0be67d47fh		; _GP_C[99] @ 6336
	.word	094e2b0b4h,0be60c8a5h		; _GP_C[100] @ 6400
	.word	0018ba41bh,0be7072dah		; _GP_C[101] @ 6464
	.word	021a25d97h,0be5a11aah		; _GP_C[102] @ 6528
	.word	0ac6ce5ach,0be54135ah		; _GP_C[103] @ 6592
	.word	0d67ec5ddh,0be63bedbh		; _GP_C[104] @ 6656
	.word	07eec9e43h,03e634457h		; _GP_C[105] @ 6720
	.word	0d1c68a76h,03e1f967eh		; _GP_C[106] @ 6784
	.word	0914ce1b2h,03e5eda75h		; _GP_C[107] @ 6848
	.word	0ec187774h,0be54eae8h		; _GP_C[108] @ 6912
	.word	0764a5656h,03e642203h		; _GP_C[109] @ 6976
	.word	02c97fe05h,0be62d75fh		; _GP_C[110] @ 7040
	.word	072b19673h,03e6131f6h		; _GP_C[111] @ 7104
	.word	0b5ed5b25h,03e64f4a4h		; _GP_C[112] @ 7168
	.word	0304c3b4fh,03e50884fh		; _GP_C[113] @ 7232
	.word	0d8ba035fh,03e420e81h		; _GP_C[114] @ 7296
	.word	0256e5318h,03e614b8eh		; _GP_C[115] @ 7360
	.word	0565f5b97h,0be6bdcd1h		; _GP_C[116] @ 7424
	.word	053674039h,03e196927h		; _GP_C[117] @ 7488
	.word	0e91d9544h,03e45a67ch		; _GP_C[118] @ 7552
	.word	0ba5b505eh,0be56f8cfh		; _GP_C[119] @ 7616
	.word	04830439fh,03e6bfdf8h		; _GP_C[120] @ 7680
	.word	071501fafh,0be65e992h		; _GP_C[121] @ 7744
	.word	0fcd3db17h,03e4ad469h		; _GP_C[122] @ 7808
	.word	05f34c6d6h,03e61c823h		; _GP_C[123] @ 7872
	.word	0f44d3d19h,03e700120h		; _GP_C[124] @ 7936
	.word	01afa0ee7h,0be614f6dh		; _GP_C[125] @ 8000
	.word	0bced03b9h,03e4ba835h		; _GP_C[126] @ 8064
	.word	0c9636c8dh,03e467b52h		; _GP_C[127] @ 8128
	.word	06b147c89h,0be132f85h		; _GP_C[128] @ 8192
	.word	06d9113d6h,0be616480h		; _GP_C[129] @ 8256
	.word	0ffa32cadh,0be5e7cb0h		; _GP_C[130] @ 8320
	.word	06d070094h,03e364c52h		; _GP_C[131] @ 8384
	.word	0387f09c7h,0be548042h		; _GP_C[132] @ 8448
	.word	09b5c5e76h,0be2b1606h		; _GP_C[133] @ 8512
	.word	0e656f170h,03e5bbdc4h		; _GP_C[134] @ 8576
	.word	0541d6d97h,0be59171ch		; _GP_C[135] @ 8640
	.word	043661f06h,0be620a37h		; _GP_C[136] @ 8704
	.word	0959fd39bh,03e659964h		; _GP_C[137] @ 8768
	.word	0c4431475h,0be4bcf71h		; _GP_C[138] @ 8832
	.word	0039a89b6h,03e4e1dbch		; _GP_C[139] @ 8896
	.word	04cbd5c18h,0be3e6d14h		; _GP_C[140] @ 8960
	.word	08695a96dh,0be567177h		; _GP_C[141] @ 9024
	.word	06055867ch,0be577885h		; _GP_C[142] @ 9088
	.word	04e02375fh,0be493d46h		; _GP_C[143] @ 9152
	.word	0fb54249ah,03e5423feh		; _GP_C[144] @ 9216
	.word	0a06c4de8h,03e54fb1eh		; _GP_C[145] @ 9280
	.word	0fed23670h,03e4dad6ch		; _GP_C[146] @ 9344
	.word	0a97844f7h,0be54be26h		; _GP_C[147] @ 9408
	.word	06a88f28fh,0be4f2ba7h		; _GP_C[148] @ 9472
	.word	01301fc9eh,0be64622bh		; _GP_C[149] @ 9536
	.word	0095a6c79h,03e5537bah		; _GP_C[150] @ 9600
	.word	0ad08f21eh,0be5b4ac7h		; _GP_C[151] @ 9664
	.word	0908d0f60h,0be550b4bh		; _GP_C[152] @ 9728
	.word	044ef1ab5h,03e383a99h		; _GP_C[153] @ 9792
	.word	0a38e1b71h,03e3e6292h		; _GP_C[154] @ 9856
	.word	01914cafeh,0be50a440h		; _GP_C[155] @ 9920
	.word	0d9c5aadah,0be4961c8h		; _GP_C[156] @ 9984
	.word	00bc3ae5ch,03e5a0029h		; _GP_C[157] @ 10048
	.word	0212a1288h,03e64dab5h		; _GP_C[158] @ 10112
	.word	019bacbcbh,03e30610fh		; _GP_C[159] @ 10176
	.word	022e8e7f5h,0be30ac7bh		; _GP_C[160] @ 10240
	.word	0a2b4219eh,0be50e562h		; _GP_C[161] @ 10304
	.word	062fc48c0h,03e5eecd7h		; _GP_C[162] @ 10368
	.word	0d2948c80h,03e51c552h		; _GP_C[163] @ 10432
	.word	05cc3837dh,0be4e7da5h		; _GP_C[164] @ 10496
	.word	0dde1854ch,03e374874h		; _GP_C[165] @ 10560
	.word	09c6b8b02h,0be60368ch		; _GP_C[166] @ 10624
	.word	08f678645h,0be626b59h		; _GP_C[167] @ 10688
	.word	0fb4efff2h,03e35d91dh		; _GP_C[168] @ 10752
	.word	0b1763d05h,03e3ef88fh		; _GP_C[169] @ 10816
	.word	039ea8e3ah,03e4e3350h		; _GP_C[170] @ 10880
	.word	09eb81154h,0be35c914h		; _GP_C[171] @ 10944
	.word	02b334fach,03e6d7617h		; _GP_C[172] @ 11008
	.word	0b61ff09bh,03e379122h		; _GP_C[173] @ 11072
	.word	0ff0eb8fdh,03e4f79cfh		; _GP_C[174] @ 11136
	.word	09811a957h,03e3d06a2h		; _GP_C[175] @ 11200
	.word	01bfe11b4h,03e6083e5h		; _GP_C[176] @ 11264
	.word	0f4f57585h,0be543ca3h		; _GP_C[177] @ 11328
	.word	00758e14bh,03e36a850h		; _GP_C[178] @ 11392
	.word	0b42d593ch,0be3f5659h		; _GP_C[179] @ 11456
	.word	0292d9911h,0be5ff03ah		; _GP_C[180] @ 11520
	.word	0e0ab63edh,0be3af80ch		; _GP_C[181] @ 11584
	.word	077218907h,0be4183d6h		; _GP_C[182] @ 11648
	.word	013dd7913h,0be65be53h		; _GP_C[183] @ 11712
	.word	0de74ff50h,03e465d36h		; _GP_C[184] @ 11776
	.word	07b237de5h,03e2ed178h		; _GP_C[185] @ 11840
	.word	02d8ed03bh,03e2ad4b1h		; _GP_C[186] @ 11904

DW$7	.dwtag  DW_TAG_variable, DW_AT_name("GP_C"), DW_AT_symbol_name("_GP_C")
	.dwattr DW$7, DW_AT_location[DW_OP_addr _GP_C]
	.dwattr DW$7, DW_AT_type(*DW$T$41)
	.dwattr DW$7, DW_AT_external(0x01)
	.global	_GP_S
	.sect	".const"
	.align	8
_GP_S:
	.word	000000000h,000000000h		; _GP_S[0] @ 0
	.word	0c6af015dh,03e1488e5h		; _GP_S[1] @ 64
	.word	0b7b4c4e5h,0beb77dabh		; _GP_S[2] @ 128
	.word	000000000h,000000000h		; _GP_S[3] @ 192
	.word	06e3f5cdbh,03e90ad6ch		; _GP_S[4] @ 256
	.word	07274f016h,0bea4c564h		; _GP_S[5] @ 320
	.word	02a0a64d0h,03eb7ba9dh		; _GP_S[6] @ 384
	.word	000000000h,000000000h		; _GP_S[7] @ 448
	.word	04f382d36h,0be9fc5a4h		; _GP_S[8] @ 512
	.word	032e52012h,03ea63c4eh		; _GP_S[9] @ 576
	.word	0a046df99h,0be8af7d8h		; _GP_S[10] @ 640
	.word	09a947fb9h,03e94ba0eh		; _GP_S[11] @ 704
	.word	000000000h,000000000h		; _GP_S[12] @ 768
	.word	02b9b859fh,0be7958ach		; _GP_S[13] @ 832
	.word	0a2c7ced5h,0be95b31ah		; _GP_S[14] @ 896
	.word	0dfd16359h,0be8cd617h		; _GP_S[15] @ 960
	.word	012782c23h,03e6aaa07h		; _GP_S[16] @ 1024
	.word	0983e8d5eh,0bea675f7h		; _GP_S[17] @ 1088
	.word	000000000h,000000000h		; _GP_S[18] @ 1152
	.word	0c11a5effh,03e5c3a47h		; _GP_S[19] @ 1216
	.word	00314bf9ch,0be99149ah		; _GP_S[20] @ 1280
	.word	0974a3bbfh,03e43629dh		; _GP_S[21] @ 1344
	.word	0fcf95c90h,0be9fa2bah		; _GP_S[22] @ 1408
	.word	08a13cf04h,0bea20066h		; _GP_S[23] @ 1472
	.word	0f5061be4h,0be8fd5dah		; _GP_S[24] @ 1536
	.word	000000000h,000000000h		; _GP_S[25] @ 1600
	.word	0401127c5h,03e799e27h		; _GP_S[26] @ 1664
	.word	0bfdfc20ch,03e78fb7fh		; _GP_S[27] @ 1728
	.word	02ab05d4ah,0be8d26e3h		; _GP_S[28] @ 1792
	.word	0c45df9beh,0be809dbfh		; _GP_S[29] @ 1856
	.word	0bd3f6c58h,03e530bb8h		; _GP_S[30] @ 1920
	.word	05b4dd2cfh,03e845f75h		; _GP_S[31] @ 1984
	.word	0a49388f1h,03e5a3e72h		; _GP_S[32] @ 2048
	.word	000000000h,000000000h		; _GP_S[33] @ 2112
	.word	0cba67e65h,03e6fadbah		; _GP_S[34] @ 2176
	.word	09469d17dh,03e718f75h		; _GP_S[35] @ 2240
	.word	01faf4ee2h,0be772d9dh		; _GP_S[36] @ 2304
	.word	0f369e4d7h,03e72cbf6h		; _GP_S[37] @ 2368
	.word	0d350beech,03e77ee14h		; _GP_S[38] @ 2432
	.word	023573557h,03e94c0adh		; _GP_S[39] @ 2496
	.word	02fda8053h,03e74105fh		; _GP_S[40] @ 2560
	.word	0efd904bdh,03e802d7ch		; _GP_S[41] @ 2624
	.word	000000000h,000000000h		; _GP_S[42] @ 2688
	.word	04bb7a2f6h,03e574850h		; _GP_S[43] @ 2752
	.word	0abfa73b7h,0be614c3ch		; _GP_S[44] @ 2816
	.word	0d210c3c8h,0be73ecf5h		; _GP_S[45] @ 2880
	.word	0ae33b6eah,03e54e6f5h		; _GP_S[46] @ 2944
	.word	0c981704bh,0be6d0d00h		; _GP_S[47] @ 3008
	.word	026315723h,03e8deae8h		; _GP_S[48] @ 3072
	.word	0878b71a7h,0be79e87ah		; _GP_S[49] @ 3136
	.word	064e5f5f3h,0be2a8171h		; _GP_S[50] @ 3200
	.word	07615c800h,03e79f123h		; _GP_S[51] @ 3264
	.word	000000000h,000000000h		; _GP_S[52] @ 3328
	.word	013df57f1h,0be819fedh		; _GP_S[53] @ 3392
	.word	08f3458feh,0be6bb0fch		; _GP_S[54] @ 3456
	.word	0ebcdc26eh,0be84a374h		; _GP_S[55] @ 3520
	.word	0839bcb2dh,0be75481fh		; _GP_S[56] @ 3584
	.word	0649a98dah,0be6b21c1h		; _GP_S[57] @ 3648
	.word	048bb9ecfh,0be755bc8h		; _GP_S[58] @ 3712
	.word	068b2e14ah,0be2cea90h		; _GP_S[59] @ 3776
	.word	0a5152120h,0be78a94ch		; _GP_S[60] @ 3840
	.word	0afc5d06dh,0be6436cch		; _GP_S[61] @ 3904
	.word	07b1bd706h,0be59c926h		; _GP_S[62] @ 3968
	.word	000000000h,000000000h		; _GP_S[63] @ 4032
	.word	0a5088970h,0be5cd755h		; _GP_S[64] @ 4096
	.word	012cb997bh,0be7a97ffh		; _GP_S[65] @ 4160
	.word	0213bc877h,0be83e1c5h		; _GP_S[66] @ 4224
	.word	0929c7a3ch,0be711723h		; _GP_S[67] @ 4288
	.word	06392aabfh,03e6a8f9ah		; _GP_S[68] @ 4352
	.word	0228a53edh,03e62827ah		; _GP_S[69] @ 4416
	.word	00d944f18h,0be781cbfh		; _GP_S[70] @ 4480
	.word	042b37d42h,03e5a32bch		; _GP_S[71] @ 4544
	.word	036a1b973h,03e666d43h		; _GP_S[72] @ 4608
	.word	064db4748h,0be53b049h		; _GP_S[73] @ 4672
	.word	04b278d8ch,0be72b36dh		; _GP_S[74] @ 4736
	.word	000000000h,000000000h		; _GP_S[75] @ 4800
	.word	0c426b77ch,0be6763e1h		; _GP_S[76] @ 4864
	.word	0ae9c754ch,03e613b75h		; _GP_S[77] @ 4928
	.word	02b4de885h,03e5a3a4dh		; _GP_S[78] @ 4992
	.word	074759314h,03e31d3ddh		; _GP_S[79] @ 5056
	.word	0e79292d6h,03e40ce09h		; _GP_S[80] @ 5120
	.word	041f4cb56h,03e650862h		; _GP_S[81] @ 5184
	.word	08cd59538h,03e631ea3h		; _GP_S[82] @ 5248
	.word	042f6cf41h,03e522f5ch		; _GP_S[83] @ 5312
	.word	0149e7b23h,03e5b21f6h		; _GP_S[84] @ 5376
	.word	0de617233h,03e608e48h		; _GP_S[85] @ 5440
	.word	0c4459679h,0be3b664bh		; _GP_S[86] @ 5504
	.word	01da6b9c0h,0be480134h		; _GP_S[87] @ 5568
	.word	000000000h,000000000h		; _GP_S[88] @ 5632
	.word	046644b5bh,03e64f722h		; _GP_S[89] @ 5696
	.word	0ea918bbfh,0be70d707h		; _GP_S[90] @ 5760
	.word	0f3408582h,03e7a2af9h		; _GP_S[91] @ 5824
	.word	0a8c9c9dfh,0be49b09ch		; _GP_S[92] @ 5888
	.word	013ae9fddh,03e71d9aeh		; _GP_S[93] @ 5952
	.word	0744cc394h,0be3c3ae0h		; _GP_S[94] @ 6016
	.word	0da382d82h,0be3ab124h		; _GP_S[95] @ 6080
	.word	096c9cde9h,0be467e27h		; _GP_S[96] @ 6144
	.word	0f5dbb1cbh,03e685033h		; _GP_S[97] @ 6208
	.word	0ba3d19c2h,0be63c36ah		; _GP_S[98] @ 6272
	.word	03b2c4299h,0be347742h		; _GP_S[99] @ 6336
	.word	0d7878cech,03e77945bh		; _GP_S[100] @ 6400
	.word	0c277e468h,03e726514h		; _GP_S[101] @ 6464
	.word	000000000h,000000000h		; _GP_S[102] @ 6528
	.word	070938e0fh,03e5fa5f5h		; _GP_S[103] @ 6592
	.word	0ac660ed7h,0be36320ch		; _GP_S[104] @ 6656
	.word	0feb671d8h,03e55f884h		; _GP_S[105] @ 6720
	.word	0b2b3454ah,0be5859b2h		; _GP_S[106] @ 6784
	.word	0cf85e781h,0be5198c2h		; _GP_S[107] @ 6848
	.word	008e10e5fh,03e2549deh		; _GP_S[108] @ 6912
	.word	0a4e8df9bh,0be31ec2ah		; _GP_S[109] @ 6976
	.word	0a4772f4ch,0be507bcbh		; _GP_S[110] @ 7040
	.word	0f7c18e58h,03e5f029bh		; _GP_S[111] @ 7104
	.word	0bb5ebfb7h,0be18cac0h		; _GP_S[112] @ 7168
	.word	0397fd0f2h,0be64f7a7h		; _GP_S[113] @ 7232
	.word	02a945bc1h,0be60b6d9h		; _GP_S[114] @ 7296
	.word	002c9b55ah,03e6842d4h		; _GP_S[115] @ 7360
	.word	0b192d472h,0be34ae38h		; _GP_S[116] @ 7424
	.word	000000000h,000000000h		; _GP_S[117] @ 7488
	.word	090794235h,03e4792d6h		; _GP_S[118] @ 7552
	.word	097e34050h,0be6095b1h		; _GP_S[119] @ 7616
	.word	0cf076ba4h,03e529071h		; _GP_S[120] @ 7680
	.word	01b7abf83h,03e3becbfh		; _GP_S[121] @ 7744
	.word	0771a5fc8h,03e415c17h		; _GP_S[122] @ 7808
	.word	0718d8682h,0be63c522h		; _GP_S[123] @ 7872
	.word	0d08f2309h,03e36d7a7h		; _GP_S[124] @ 7936
	.word	09c3155e1h,03e57c931h		; _GP_S[125] @ 8000
	.word	0f03912ffh,03e642a9dh		; _GP_S[126] @ 8064
	.word	08526632bh,03e4f9d9fh		; _GP_S[127] @ 8128
	.word	084b9bc81h,03e536f2fh		; _GP_S[128] @ 8192
	.word	03b79704ch,03e50ab4ah		; _GP_S[129] @ 8256
	.word	066e6e384h,0be3220ach		; _GP_S[130] @ 8320
	.word	06b031b38h,0be5a2c39h		; _GP_S[131] @ 8384
	.word	093bd28eah,0be343c3bh		; _GP_S[132] @ 8448
	.word	000000000h,000000000h		; _GP_S[133] @ 8512
	.word	0cec33cc2h,03e6178eah		; _GP_S[134] @ 8576
	.word	0da3a3c92h,03e5f0274h		; _GP_S[135] @ 8640
	.word	0bce829c1h,0be57aad4h		; _GP_S[136] @ 8704
	.word	0a050da6bh,03e69f9d0h		; _GP_S[137] @ 8768
	.word	0eab1efabh,0be2b70f7h		; _GP_S[138] @ 8832
	.word	0c4ae94c7h,0be62d4cfh		; _GP_S[139] @ 8896
	.word	0bf376ca8h,0be42ee8bh		; _GP_S[140] @ 8960
	.word	05b1de2c6h,03e357f5bh		; _GP_S[141] @ 9024
	.word	0657f9329h,0be653502h		; _GP_S[142] @ 9088
	.word	021f8bd9dh,03e4886d9h		; _GP_S[143] @ 9152
	.word	0e31304a0h,0be2a0a99h		; _GP_S[144] @ 9216
	.word	0dc1a8ef0h,03e3ca58fh		; _GP_S[145] @ 9280
	.word	007bc288ah,03e11a83fh		; _GP_S[146] @ 9344
	.word	01d8ab919h,0be64bb8ch		; _GP_S[147] @ 9408
	.word	0e97a92bfh,0be619457h		; _GP_S[148] @ 9472
	.word	087954330h,03e29f477h		; _GP_S[149] @ 9536
	.word	000000000h,000000000h		; _GP_S[150] @ 9600
	.word	043d97cbeh,0be60764ch		; _GP_S[151] @ 9664
	.word	005b339ffh,03e3be2c1h		; _GP_S[152] @ 9728
	.word	008bedca8h,03e3d2249h		; _GP_S[153] @ 9792
	.word	012779f7ah,03e5ac87dh		; _GP_S[154] @ 9856
	.word	0826547b8h,03e3c5903h		; _GP_S[155] @ 9920
	.word	0f654fbebh,0be5f1d29h		; _GP_S[156] @ 9984
	.word	004524aa5h,0be322264h		; _GP_S[157] @ 10048
	.word	070e15518h,03e2ed462h		; _GP_S[158] @ 10112
	.word	0892188c4h,0be5e38e7h		; _GP_S[159] @ 10176
	.word	09b42e341h,03e53784fh		; _GP_S[160] @ 10240
	.word	0d0c231a4h,03e46e23bh		; _GP_S[161] @ 10304
	.word	00ed292bfh,03e55d865h		; _GP_S[162] @ 10368
	.word	0b7527bd6h,03e55f9dbh		; _GP_S[163] @ 10432
	.word	0a2ecb7f9h,03e48af55h		; _GP_S[164] @ 10496
	.word	0ea1512bbh,03e36e01ch		; _GP_S[165] @ 10560
	.word	0dd7df92ah,03e2f61bbh		; _GP_S[166] @ 10624
	.word	09fdf0b8ch,0be5550f8h		; _GP_S[167] @ 10688
	.word	000000000h,000000000h		; _GP_S[168] @ 10752
	.word	07729b89bh,0be64de73h		; _GP_S[169] @ 10816
	.word	051a75fdeh,03e457eb1h		; _GP_S[170] @ 10880
	.word	0c41265f8h,0be351520h		; _GP_S[171] @ 10944
	.word	05a36c46dh,0be173d1ch		; _GP_S[172] @ 11008
	.word	0bd40a201h,03e5c6215h		; _GP_S[173] @ 11072
	.word	06ca5fd1eh,0be4d4cc2h		; _GP_S[174] @ 11136
	.word	0576eb14eh,03e3d92fdh		; _GP_S[175] @ 11200
	.word	0846bb2abh,03e31f210h		; _GP_S[176] @ 11264
	.word	00dc7bc09h,03e63c5eah		; _GP_S[177] @ 11328
	.word	00ac530b1h,0be3404bbh		; _GP_S[178] @ 11392
	.word	0ec30d179h,03e20c4d3h		; _GP_S[179] @ 11456
	.word	0e98984deh,0be51a9aeh		; _GP_S[180] @ 11520
	.word	0046ad609h,0be62b427h		; _GP_S[181] @ 11584
	.word	0fd1d73ceh,0be4b9fdeh		; _GP_S[182] @ 11648
	.word	0e09fd7c5h,0be55c35ch		; _GP_S[183] @ 11712
	.word	0334a7debh,03e3c68edh		; _GP_S[184] @ 11776
	.word	0821c4b47h,03e333e88h		; _GP_S[185] @ 11840
	.word	0e5c3e02fh,0be479a25h		; _GP_S[186] @ 11904

DW$8	.dwtag  DW_TAG_variable, DW_AT_name("GP_S"), DW_AT_symbol_name("_GP_S")
	.dwattr DW$8, DW_AT_location[DW_OP_addr _GP_S]
	.dwattr DW$8, DW_AT_type(*DW$T$41)
	.dwattr DW$8, DW_AT_external(0x01)
;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI8442 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI8444 
	.sect	".text"

DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("NALF_matrix"), DW_AT_symbol_name("_NALF_matrix")
	.dwattr DW$9, DW_AT_low_pc(_NALF_matrix)
	.dwattr DW$9, DW_AT_high_pc(0x00)
	.dwattr DW$9, DW_AT_begin_file("geopotential.c")
	.dwattr DW$9, DW_AT_begin_line(0x32)
	.dwattr DW$9, DW_AT_begin_column(0x0d)
	.dwattr DW$9, DW_AT_frame_base[DW_OP_breg31 192]
	.dwattr DW$9, DW_AT_skeletal(0x01)
	.dwpsn	"geopotential.c",51,1

;******************************************************************************
;* FUNCTION NAME: _NALF_matrix                                                *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 144 Auto + 44 Save = 188 byte               *
;******************************************************************************
_NALF_matrix:
;** --------------------------------------------------------------------------*
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pGP"), DW_AT_symbol_name("_pGP")
	.dwattr DW$10, DW_AT_type(*DW$T$29)
	.dwattr DW$10, DW_AT_location[DW_OP_reg4]
DW$11	.dwtag  DW_TAG_formal_parameter, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$11, DW_AT_type(*DW$T$19)
	.dwattr DW$11, DW_AT_location[DW_OP_reg20]
DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("z"), DW_AT_symbol_name("_z")
	.dwattr DW$12, DW_AT_type(*DW$T$20)
	.dwattr DW$12, DW_AT_location[DW_OP_reg6]

           ADDK    .S2     -192,SP           ; |51| 
||         MVKL    .S1     _memset,A3        ; |56| 
||         MV      .L1X    SP,A31            ; |51| 

           STDW    .D2T2   B11:B10,*+SP(176)
||         MVKH    .S1     _memset,A3        ; |56| 

           STW     .D2T2   B4,*+SP(12)       ; |51| 

           CALL    .S2X    A3                ; |56| 
||         STW     .D2T2   B3,*+SP(172)

           STW     .D2T1   A15,*+SP(192)

           STDW    .D2T2   B13:B12,*+SP(184)
||         STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .L2X    A6,B10            ; |51| 

           STW     .D2T2   B10,*+SP(4)       ; |51| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         MV      .L2X    A7,B11            ; |51| 

           STW     .D2T2   B11,*+SP(16)      ; |51| 
||         MV      .L1     A4,A13            ; |51| 
||         STW     .D1T1   A14,*-A31(24)
||         MVK     .S1     8672,A5           ; |56| 

           ADDKPC  .S2     RL0,B3,0          ; |56| 
||         STW     .D2T1   A13,*+SP(8)       ; |51| 
||         ADD     .L1     A5,A4,A4          ; |56| 
||         MVK     .S1     0xb48,A6          ; |56| 
||         ZERO    .L2     B4                ; |56| 

RL0:       ; CALL OCCURS {_memset}           ; |56| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |57| 
           MVKH    .S1     __mpyd,A3         ; |57| 
           MV      .L1X    B10,A4            ; |57| 
           CALL    .S2X    A3                ; |57| 
           ADDKPC  .S2     RL2,B3,1          ; |57| 
           MV      .L2     B11,B5            ; |57| 
           MV      .D2     B10,B4            ; |57| 
           MV      .L1X    B11,A5            ; |57| 
RL2:       ; CALL OCCURS {__mpyd}            ; |57| 
           MVKL    .S1     __subd,A3         ; |57| 
           MVKH    .S1     __subd,A3         ; |57| 
           MV      .L2X    A5,B5             ; |57| 
           CALL    .S2X    A3                ; |57| 
           ZERO    .L1     A5                ; |57| 
           MV      .L2X    A4,B4             ; |57| 
           MVKH    .S1     0x3ff00000,A5     ; |57| 
           ADDKPC  .S2     RL3,B3,0          ; |57| 
           ZERO    .L1     A4                ; |57| 
RL3:       ; CALL OCCURS {__subd}            ; |57| 
           MVKL    .S1     _sqrt,A3          ; |57| 
           MVKH    .S1     _sqrt,A3          ; |57| 
           STW     .D2T1   A4,*+SP(24)       ; |57| 
           CALL    .S2X    A3                ; |57| 
           STW     .D2T1   A5,*+SP(20)       ; |57| 
           ADDKPC  .S2     RL4,B3,3          ; |57| 
RL4:       ; CALL OCCURS {_sqrt}             ; |57| 
           MVK     .S1     8672,A3           ; |58| 

           MVKL    .S1     __negd,A3         ; |59| 
||         ADD     .L1     A3,A13,A10        ; |58| 

           MVKH    .S1     __negd,A3         ; |59| 
           MV      .L1     A5,A11            ; |57| 
           CALL    .S2X    A3                ; |59| 
           MV      .L1     A4,A14            ; |57| 
           STW     .D2T1   A11,*+SP(32)      ; |57| 

           ZERO    .L1     A7                ; |58| 
||         STW     .D2T1   A14,*+SP(36)      ; |57| 

           ZERO    .L1     A6                ; |58| 
||         MVKH    .S1     0x3ff00000,A7     ; |58| 
||         STW     .D2T1   A10,*+SP(28)      ; |58| 
||         STDW    .D1T2   B11:B10,*+A10(152) ; |59| 

           STDW    .D1T1   A7:A6,*A10        ; |58| 
||         ADDKPC  .S2     RL5,B3,0          ; |59| 

RL5:       ; CALL OCCURS {__negd}            ; |59| 
           MVKL    .S1     __mpyd,A3         ; |60| 
           MVKH    .S1     __mpyd,A3         ; |60| 
           ZERO    .L2     B5                ; |60| 
           CALL    .S2X    A3                ; |60| 
           MV      .D1     A4,A6             ; |59| 
           MV      .S1     A5,A7             ; |59| 
           MVKH    .S2     0x40080000,B5     ; |60| 
           MV      .L1X    B10,A4            ; |60| 

           ZERO    .L2     B4                ; |60| 
||         MV      .L1X    B11,A5            ; |60| 
||         STDW    .D1T1   A7:A6,*+A10(160)  ; |59| 
||         ADDKPC  .S2     RL6,B3,0          ; |60| 

RL6:       ; CALL OCCURS {__mpyd}            ; |60| 
           MVKL    .S1     __mpyd,A3         ; |60| 
           MVKH    .S1     __mpyd,A3         ; |60| 
           MV      .D2     B10,B4            ; |60| 
           CALL    .S2X    A3                ; |60| 
           MV      .L2     B11,B5            ; |60| 
           MV      .S1     A4,A15            ; |60| 
           MV      .L1     A5,A12            ; |60| 
           ADDKPC  .S2     RL10,B3,1         ; |60| 
RL10:      ; CALL OCCURS {__mpyd}            ; |60| 
           MVKL    .S1     __subd,A3         ; |60| 
           MVKH    .S1     __subd,A3         ; |60| 
           ZERO    .L2     B5                ; |60| 
           CALL    .S2X    A3                ; |60| 
           MVKH    .S2     0x3ff00000,B5     ; |60| 
           ZERO    .L2     B4                ; |60| 
           ADDKPC  .S2     RL11,B3,2         ; |60| 
RL11:      ; CALL OCCURS {__subd}            ; |60| 
           MVKL    .S2     __mpyd,B6         ; |60| 
           MVKH    .S2     __mpyd,B6         ; |60| 
           CALL    .S2     B6                ; |60| 
           ZERO    .L2     B5                ; |60| 
           MVKH    .S2     0x3fe00000,B5     ; |60| 
           ZERO    .L2     B4                ; |60| 
           ADDKPC  .S2     RL12,B3,1         ; |60| 
RL12:      ; CALL OCCURS {__mpyd}            ; |60| 
           MVKL    .S2     __mpyd,B6         ; |60| 
           MVKH    .S2     __mpyd,B6         ; |60| 
           CALL    .S2     B6                ; |60| 
           ZERO    .L2     B5                ; |60| 
           MVK     .S1     8976,A3           ; |60| 
           MV      .L1     A4,A6             ; |60| 

           MVKH    .S2     0xc0080000,B5     ; |60| 
||         MV      .D1     A5,A7             ; |60| 
||         ADD     .S1     A3,A13,A10        ; |60| 
||         MV      .L1X    B10,A4            ; |60| 

           ZERO    .L2     B4                ; |60| 
||         MV      .L1X    B11,A5            ; |60| 
||         STDW    .D1T1   A7:A6,*A10        ; |60| 
||         ADDKPC  .S2     RL14,B3,0         ; |60| 

RL14:      ; CALL OCCURS {__mpyd}            ; |60| 
           MVKL    .S2     __mpyd,B6         ; |60| 
           MVKH    .S2     __mpyd,B6         ; |60| 
           CALL    .S2     B6                ; |60| 
           MV      .L2X    A14,B4            ; |60| 
           MV      .L2X    A11,B5            ; |60| 
           ADDKPC  .S2     RL15,B3,2         ; |60| 
RL15:      ; CALL OCCURS {__mpyd}            ; |60| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B8         ; |60| 
           MVKH    .S2     __mpyd,B8         ; |60| 

           CALL    .S2     B8                ; |60| 
||         LDW     .D2T1   *+SP(20),A5       ; |60| 
||         MV      .L2X    A5,B7             ; |60| 

           LDW     .D2T1   *+SP(24),A4       ; |60| 
||         MV      .L2X    A4,B6             ; |60| 

           ZERO    .L2     B5                ; |60| 
           MVKH    .S2     0x40080000,B5     ; |60| 
           ADDKPC  .S2     RL16,B3,0         ; |60| 

           STDW    .D1T2   B7:B6,*+A10(8)    ; |60| 
||         ZERO    .L2     B4                ; |60| 

RL16:      ; CALL OCCURS {__mpyd}            ; |60| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _memset,A3        ; |63| 
           MVKH    .S1     _memset,A3        ; |63| 
           MVK     .S1     11560,A6          ; |63| 
           CALL    .S2X    A3                ; |63| 
           STDW    .D1T1   A5:A4,*+A10(16)   ; |60| 
           ADDKPC  .S2     RL17,B3,0         ; |63| 
           ADD     .L1     A6,A13,A4         ; |63| 
           MVK     .S1     0xb48,A6          ; |63| 

           MV      .D1     A13,A11           ; |63| 
||         ZERO    .L2     B4                ; |63| 

RL17:      ; CALL OCCURS {_memset}           ; |63| 

           LDW     .D2T2   *+SP(32),B11      ; |66| 
||         MVKL    .S2     __mpyd,B6         ; |66| 

           LDW     .D2T2   *+SP(16),B13      ; |65| 
||         MVKH    .S2     __mpyd,B6         ; |66| 

           CALL    .S2     B6                ; |66| 
||         LDW     .D2T2   *+SP(4),B12       ; |65| 

           MVK     .S1     11560,A3          ; |64| 

           ADD     .S1     A3,A11,A10        ; |64| 
||         ZERO    .L1     A7:A6             ; |64| 

           STDW    .D1T1   A7:A6,*A10        ; |64| 
||         MV      .L2X    A14,B10           ; |64| 

           STDW    .D1T2   B11:B10,*+A10(152) ; |65| 
||         MV      .L2X    A14,B4            ; |64| 

           ADDKPC  .S2     RL18,B3,0         ; |66| 
||         STDW    .D1T2   B13:B12,*+A10(160) ; |65| 
||         STW     .D2T1   A10,*+SP(40)      ; |64| 
||         MV      .L2     B11,B5            ; |66| 
||         MV      .L1     A12,A5            ; |66| 
||         MV      .S1     A15,A4            ; |66| 

RL18:      ; CALL OCCURS {__mpyd}            ; |66| 
           MVKL    .S1     __addd,A3         ; |66| 
           MVKH    .S1     __addd,A3         ; |66| 
           MV      .L1X    B13,A13           ; |66| 
           CALL    .S2X    A3                ; |66| 
           MV      .L2X    A4,B6             ; |66| 
           MVK     .S1     11864,A6          ; |66| 
           MV      .L1X    B12,A4            ; |66| 

           ADD     .S1     A6,A11,A12        ; |66| 
||         MV      .L2X    A5,B7             ; |66| 

           ADDKPC  .S2     RL19,B3,0         ; |66| 
||         STDW    .D1T2   B7:B6,*A12        ; |66| 
||         MV      .L1X    B13,A5            ; |66| 
||         MV      .L2     B13,B5            ; |66| 
||         MV      .D2     B12,B4            ; |66| 

RL19:      ; CALL OCCURS {__addd}            ; |66| 
           MVKL    .S1     __mpyd,A3         ; |66| 
           MVKH    .S1     __mpyd,A3         ; |66| 
           MV      .L2     B12,B4            ; |66| 
           CALL    .S2X    A3                ; |66| 
           ADDKPC  .S2     RL23,B3,1         ; |66| 
           MV      .L1     A5,A14            ; |66| 
           MV      .S1     A4,A15            ; |66| 
           MV      .D2X    A13,B5            ; |66| 
RL23:      ; CALL OCCURS {__mpyd}            ; |66| 
           MVKL    .S1     __subd,A3         ; |66| 
           MVKH    .S1     __subd,A3         ; |66| 
           ZERO    .L2     B5                ; |66| 
           CALL    .S2X    A3                ; |66| 
           MVKH    .S2     0x3ff00000,B5     ; |66| 
           ZERO    .L2     B4                ; |66| 
           ADDKPC  .S2     RL24,B3,2         ; |66| 
RL24:      ; CALL OCCURS {__subd}            ; |66| 
           MVKL    .S2     __mpyd,B6         ; |66| 
           MVKH    .S2     __mpyd,B6         ; |66| 
           CALL    .S2     B6                ; |66| 
           ZERO    .L2     B5                ; |66| 
           MVKH    .S2     0x40080000,B5     ; |66| 
           ZERO    .L2     B4                ; |66| 
           ADDKPC  .S2     RL25,B3,1         ; |66| 
RL25:      ; CALL OCCURS {__mpyd}            ; |66| 
           MVKL    .S1     __mpyd,A3         ; |66| 
           MVKH    .S1     __mpyd,A3         ; |66| 
           ZERO    .L2     B5                ; |66| 
           CALL    .S2X    A3                ; |66| 
           MV      .S1     A4,A6             ; |66| 
           MV      .L1     A5,A7             ; |66| 
           MVKH    .S2     0xc0180000,B5     ; |66| 
           ZERO    .L2     B4                ; |66| 

           MV      .S1     A13,A5            ; |66| 
||         MV      .L1X    B12,A4            ; |66| 
||         STDW    .D1T1   A7:A6,*+A12(8)    ; |66| 
||         ADDKPC  .S2     RL27,B3,0         ; |66| 

RL27:      ; CALL OCCURS {__mpyd}            ; |66| 
           MVKL    .S2     __mpyd,B6         ; |66| 
           MVKH    .S2     __mpyd,B6         ; |66| 
           CALL    .S2     B6                ; |66| 
           MV      .D2     B10,B4            ; |66| 
           MV      .L2     B11,B5            ; |66| 
           ADDKPC  .S2     RL28,B3,2         ; |66| 
RL28:      ; CALL OCCURS {__mpyd}            ; |66| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *+SP(12),B30
||         MV      .L1     A4,A6             ; |66| 
||         MVK     .S1     11256,A18
||         MV      .D1     A11,A9
||         MVK     .S2     11408,B5

           LDW     .D2T1   *+SP(28),A4
||         MVK     .S2     480,B4
||         MVK     .S1     8992,A17
||         ADD     .L2X    B5,A11,B6
||         ADD     .L1     A18,A11,A30
||         MV      .D1     A5,A7             ; |66| 

           MVK     .S1     480,A8
||         MVK     .S2     8520,B31
||         MVK     .L1     0x5,A5
||         STW     .D2T2   B6,*+SP(44)
||         STDW    .D1T1   A7:A6,*+A12(16)   ; |66| 

           MVK     .S1     11720,A16
||         STW     .D2T1   A30,*+SP(68)
||         MVK     .L1     0x3,A30
||         ADD     .D1     A8,A10,A6

           MVK     .S1     8368,A19
||         STW     .D2T1   A5,*+SP(52)
||         ADD     .L1     A16,A11,A5
||         ADD     .D1     A17,A11,A16

           CMPLT   .L2     B30,3,B0          ; |69| 
||         MVK     .S2     8832,B30
||         ADD     .L1     A19,A11,A31
||         MVKL    .S1     __mpyd,A3
||         SUB     .D2     B30,2,B5          ; |71| 

           MV      .L1     A15,A4
||         ADD     .L2X    B4,A4,B4
||         STW     .D2T2   B5,*+SP(64)
||         MV      .S2     B11,B5
||         MVKH    .S1     __mpyd,A3

           STW     .D2T2   B4,*+SP(56)
||         ADD     .L2X    B31,A9,B4
||         MVK     .S2     0x3,B11           ; |69| 

           STW     .D2T2   B4,*+SP(80)
||         ADD     .L2X    B30,A11,B6
||         MV      .S2     B10,B4

           STW     .D2T2   B6,*+SP(84)       ; |66| 
|| [ B0]   B       .S1     L3                ; |69| 
||         MVK     .S2     0x1c8,B10

           STW     .D2T1   A30,*+SP(48)
|| [!B0]   CALL    .S2X    A3

           STW     .D2T1   A6,*+SP(88)       ; |66| 

           MV      .L1     A14,A5
||         STW     .D2T1   A5,*+SP(76)

           STW     .D2T1   A16,*+SP(72)
           STW     .D2T1   A31,*+SP(60)
           ; BRANCHCC OCCURS {L3}            ; |69| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL29,B3,0
RL29:      ; CALL OCCURS {__mpyd} 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __fltid,B4
           MVKH    .S2     __fltid,B4

           CALL    .S2     B4
||         STW     .D2T1   A4,*+SP(92)

           LDW     .D2T1   *+SP(52),A4
           NOP             1
           STW     .D2T1   A5,*+SP(96)
	.dwpsn	"geopotential.c",69,0
           NOP             1
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L1
;** --------------------------------------------------------------------------*
L1:    
DW$L$_NALF_matrix$8$B:
	.dwpsn	"geopotential.c",70,0
           ADDKPC  .S2     RL30,B3,0
RL30:      ; CALL OCCURS {__fltid} 
DW$L$_NALF_matrix$8$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$9$B:
           MVKL    .S1     __mpyd,A3
           MVKH    .S1     __mpyd,A3
           LDW     .D2T2   *+SP(16),B5
           CALL    .S2X    A3
           LDW     .D2T2   *+SP(4),B4
           ADDKPC  .S2     RL31,B3,0
           STW     .D2T1   A5,*+SP(100)
           STW     .D2T1   A4,*+SP(104)
           NOP             1
RL31:      ; CALL OCCURS {__mpyd} 
DW$L$_NALF_matrix$9$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$10$B:
           LDW     .D2T2   *+SP(44),B4
           LDW     .D2T2   *+SP(80),B31
           LDW     .D2T1   *+SP(68),A3
           STW     .D2T1   A5,*+SP(120)
           STW     .D2T1   A4,*+SP(116)
           LDW     .D2T1   *+SP(40),A29

           ADD     .L2     B10,B31,B4
||         ADD     .S2     B10,B4,B13

           STW     .D2T2   B4,*+SP(112)
           LDW     .D2T2   *+SP(112),B6      ; |75| 
           LDW     .D2T1   *+SP(60),A30
           LDW     .D2T1   *+SP(28),A31
           SUB     .L2     B11,1,B5          ; |73| 

           MVKL    .S1     __fltid,A3        ; |75| 
||         ADD     .L1X    B10,A3,A12
||         STW     .D2T2   B5,*+SP(108)

           LDDW    .D2T2   *B6++,B5:B4       ; |75| 
||         MVKH    .S1     __fltid,A3        ; |75| 

           MV      .L1X    B11,A4            ; |75| Register A/B partition copy

           CALL    .S2X    A3                ; |75| 
||         ADD     .L1X    B10,A29,A10

           ADD     .L1X    B10,A30,A13
	.dwpsn	"geopotential.c",71,0

           ADD     .L1X    B10,A31,A11
||         ZERO    .L2     B12               ; |71| 

DW$L$_NALF_matrix$10$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
L2:    
DW$L$_NALF_matrix$11$B:
	.dwpsn	"geopotential.c",72,0
           STW     .D2T2   B5,*+SP(128)      ; |75| 
           STW     .D2T2   B4,*+SP(124)      ; |75| 

           STW     .D2T2   B6,*+SP(112)      ; |75| 
||         ADDKPC  .S2     RL33,B3,0         ; |75| 
||         SUB     .L1X    A4,B12,A4         ; |75| 

RL33:      ; CALL OCCURS {__fltid}           ; |75| 
DW$L$_NALF_matrix$11$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$12$B:
           MVKL    .S1     __divd,A3         ; |75| 
           MVKH    .S1     __divd,A3         ; |75| 
           MV      .L2X    A5,B5             ; |75| 
           CALL    .S2X    A3                ; |75| 
           ZERO    .L1     A5                ; |75| 
           ADDKPC  .S2     RL34,B3,0         ; |75| 
           MVKH    .S1     0x3ff00000,A5     ; |75| 
           MV      .L2X    A4,B4             ; |75| 
           ZERO    .L1     A4                ; |75| 
RL34:      ; CALL OCCURS {__divd}            ; |75| 
           MVKL    .S1     __fltid,A3        ; |75| 
           MVKH    .S1     __fltid,A3        ; |75| 
           ADD     .L2     B11,B12,B4        ; |75| 
           CALL    .S2X    A3                ; |75| 
           ADDKPC  .S2     RL35,B3,0         ; |75| 
           STW     .D2T1   A5,*+SP(136)      ; |75| 
           NOP             1
           STW     .D2T1   A4,*+SP(132)      ; |75| 
           SUB     .L1X    B4,1,A4           ; |75| 
RL35:      ; CALL OCCURS {__fltid}           ; |75| 
           STW     .D2T1   A5,*+SP(140)      ; |75| 

           MVKL    .S1     __mpyd,A3         ; |75| 
||         STW     .D2T1   A4,*+SP(144)      ; |75| 

           MVKH    .S1     __mpyd,A3         ; |75| 
||         LDW     .D2T2   *+SP(124),B4      ; |75| 

           LDW     .D2T2   *+SP(128),B5      ; |75| 

           LDW     .D2T1   *+SP(120),A5      ; |75| 
||         CALL    .S2X    A3                ; |75| 

           LDW     .D2T1   *+SP(116),A4      ; |75| 
           ADDKPC  .S2     RL41,B3,3         ; |75| 
RL41:      ; CALL OCCURS {__mpyd}            ; |75| 
           MVKL    .S1     __mpyd,A3         ; |75| 
           MVKH    .S1     __mpyd,A3         ; |75| 
           LDW     .D2T2   *+SP(140),B5      ; |75| 

           LDDW    .D1T1   *A13++,A7:A6      ; |75| 
||         CALL    .S2X    A3                ; |75| 

           LDW     .D2T2   *+SP(144),B4      ; |75| 
           MV      .L1     A4,A15            ; |75| 
           MV      .D1     A5,A14            ; |75| 
           ADDKPC  .S2     RL42,B3,0         ; |75| 

           MV      .S1     A6,A4             ; |75| 
||         MV      .L1     A7,A5             ; |75| 

RL42:      ; CALL OCCURS {__mpyd}            ; |75| 
           MVKL    .S1     __subd,A3         ; |75| 
           MVKH    .S1     __subd,A3         ; |75| 
           MV      .L2X    A4,B4             ; |75| 
           CALL    .S2X    A3                ; |75| 
           MV      .L2X    A5,B5             ; |75| 
           MV      .S1     A15,A4            ; |75| 
           ADDKPC  .S2     RL43,B3,1         ; |75| 
           MV      .L1     A14,A5            ; |75| 
RL43:      ; CALL OCCURS {__subd}            ; |75| 
           MVKL    .S2     __mpyd,B6         ; |75| 
           MVKH    .S2     __mpyd,B6         ; |75| 

           CALL    .S2     B6                ; |75| 
||         LDW     .D2T2   *+SP(136),B5      ; |75| 

           LDW     .D2T2   *+SP(132),B4      ; |75| 
           ADDKPC  .S2     RL44,B3,3         ; |75| 
RL44:      ; CALL OCCURS {__mpyd}            ; |75| 
           MVKL    .S2     __mpyd,B6         ; |77| 

           LDW     .D2T2   *+SP(16),B5       ; |75| 
||         MVKH    .S2     __mpyd,B6         ; |77| 

           CALL    .S2     B6                ; |77| 
||         LDDW    .D2T1   *B13++,A7:A6      ; |77| 

           LDW     .D2T2   *+SP(4),B4        ; |77| 
           ADDKPC  .S2     RL61,B3,0         ; |77| 
           STDW    .D1T1   A5:A4,*A11++      ; |75| 
           NOP             1

           MV      .S1     A6,A4             ; |77| 
||         MV      .L1     A7,A5             ; |77| 

RL61:      ; CALL OCCURS {__mpyd}            ; |77| 

           MVKL    .S2     __mpyd,B6         ; |77| 
||         LDW     .D2T2   *+SP(32),B5       ; |77| 

           MVKH    .S2     __mpyd,B6         ; |77| 
||         LDW     .D2T2   *+SP(36),B4       ; |77| 

           LDW     .D2T1   *+SP(128),A5      ; |77| 
||         MV      .L1     A5,A14            ; |77| 
||         CALL    .S2     B6                ; |77| 

           LDW     .D2T1   *+SP(124),A4      ; |77| 
||         MV      .L1     A4,A15            ; |77| 

           ADDKPC  .S2     RL62,B3,3         ; |77| 
RL62:      ; CALL OCCURS {__mpyd}            ; |77| 
           MVKL    .S2     __addd,B6         ; |77| 
           MVKH    .S2     __addd,B6         ; |77| 
           CALL    .S2     B6                ; |77| 
           MV      .L2X    A4,B4             ; |77| 
           MV      .L2X    A5,B5             ; |77| 
           MV      .S1     A15,A4            ; |77| 
           ADDKPC  .S2     RL63,B3,0         ; |77| 
           MV      .L1     A14,A5            ; |77| 
RL63:      ; CALL OCCURS {__addd}            ; |77| 
           MVKL    .S1     __mpyd,A3         ; |77| 
           MVKH    .S1     __mpyd,A3         ; |77| 
           LDW     .D2T2   *+SP(100),B5      ; |77| 
           CALL    .S2X    A3                ; |77| 
           LDW     .D2T2   *+SP(104),B4      ; |77| 
           ADDKPC  .S2     RL64,B3,3         ; |77| 
RL64:      ; CALL OCCURS {__mpyd}            ; |77| 
DW$L$_NALF_matrix$12$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$13$B:
           MVKL    .S1     __mpyd,A3         ; |77| 
           MVKH    .S1     __mpyd,A3         ; |77| 
           LDW     .D2T2   *+SP(140),B5      ; |77| 

           CALL    .S2X    A3                ; |77| 
||         LDDW    .D1T1   *A12++,A7:A6      ; |77| 

           LDW     .D2T2   *+SP(144),B4      ; |77| 
           MV      .L1     A4,A15            ; |77| 
           ADDKPC  .S2     RL65,B3,0         ; |77| 
           MV      .D1     A5,A14            ; |77| 

           MV      .S1     A6,A4             ; |77| 
||         MV      .L1     A7,A5             ; |77| 

RL65:      ; CALL OCCURS {__mpyd}            ; |77| 
DW$L$_NALF_matrix$13$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$14$B:
           MVKL    .S1     __subd,A3         ; |77| 
           MVKH    .S1     __subd,A3         ; |77| 
           MV      .L2X    A4,B4             ; |77| 
           CALL    .S2X    A3                ; |77| 
           ADDKPC  .S2     RL66,B3,1         ; |77| 
           MV      .S1     A15,A4            ; |77| 
           MV      .L2X    A5,B5             ; |77| 
           MV      .L1     A14,A5            ; |77| 
RL66:      ; CALL OCCURS {__subd}            ; |77| 
           MVKL    .S2     __mpyd,B6         ; |77| 
           MVKH    .S2     __mpyd,B6         ; |77| 

           LDW     .D2T2   *+SP(136),B5      ; |77| 
||         CALL    .S2     B6                ; |77| 

           LDW     .D2T2   *+SP(132),B4      ; |77| 
           ADDKPC  .S2     RL67,B3,3         ; |77| 
RL67:      ; CALL OCCURS {__mpyd}            ; |77| 
DW$L$_NALF_matrix$14$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$15$B:

           LDW     .D2T2   *+SP(108),B4      ; |77| 
||         STDW    .D1T1   A5:A4,*A10++      ; |77| 
||         ADD     .L2     1,B12,B12         ; |71| 

           NOP             1
           LDW     .D2T2   *+SP(112),B6      ; |75| 
           NOP             2

           SUB     .L2     B4,1,B4           ; |71| 
||         SUB     .L1X    B4,1,A0           ; |71| 

   [!A0]   SHL     .S2     B12,4,B4          ; |83| 
||         STW     .D2T2   B4,*+SP(108)      ; |77| 
|| [ A0]   MV      .L1X    B11,A4            ; |75| Register A/B partition copy
|| [ A0]   MVKL    .S1     __fltid,A3        ; |75| 

   [ A0]   B       .S2     L2                ; |71| 
|| [ A0]   LDDW    .D2T2   *B6++,B5:B4       ; |75| 
|| [ A0]   MVKH    .S1     __fltid,A3        ; |75| 

   [!A0]   LDW     .D2T1   *+SP(28),A3       ; |83| 
   [!A0]   ADDAW   .D2     B4,B12,B4         ; |83| 
   [ A0]   CALL    .S2X    A3                ; |75| 
   [!A0]   MV      .L1X    B4,A12            ; |83| Define a twin register
	.dwpsn	"geopotential.c",79,0
           NOP             1
           ; BRANCHCC OCCURS {L2}            ; |71| 
DW$L$_NALF_matrix$15$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$16$B:

           LDDW    .D1T1   *+A3[A12],A5:A4   ; |83| 
||         MVKL    .S1     __mpyd,A3         ; |83| 

           MVKH    .S1     __mpyd,A3         ; |83| 
           LDW     .D2T2   *+SP(116),B4      ; |83| 
           CALL    .S2X    A3                ; |83| 
           LDW     .D2T2   *+SP(120),B5      ; |83| 
           ADDKPC  .S2     RL68,B3,3         ; |83| 
RL68:      ; CALL OCCURS {__mpyd}            ; |83| 
DW$L$_NALF_matrix$16$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$17$B:
           LDW     .D2T1   *+SP(28),A3       ; |83| 
           NOP             1
           STDW    .D1T1   A5:A4,*A11        ; |83| 
           LDW     .D2T2   *+SP(32),B5       ; |84| 
           LDW     .D2T2   *+SP(36),B4       ; |84| 

           MVKL    .S1     __mpyd,A3         ; |84| 
||         LDDW    .D1T1   *+A3[A12],A5:A4   ; |84| 

           MVKH    .S1     __mpyd,A3         ; |84| 
           NOP             1
           CALL    .S2X    A3                ; |84| 
           ADDKPC  .S2     RL74,B3,4         ; |84| 
RL74:      ; CALL OCCURS {__mpyd}            ; |84| 
           LDW     .D2T1   *+SP(40),A3       ; |84| 
           MVKL    .S2     __mpyd,B6         ; |84| 
           MVKH    .S2     __mpyd,B6         ; |84| 
           LDW     .D2T2   *+SP(16),B5       ; |84| 
           LDW     .D2T2   *+SP(4),B4        ; |84| 

           LDDW    .D1T1   *+A3[A12],A7:A6   ; |84| 
||         CALL    .S2     B6                ; |84| 

           MV      .L1     A5,A11            ; |84| 
           ADDKPC  .S2     RL75,B3,1         ; |84| 
           MV      .D1     A4,A12            ; |84| 

           MV      .L1     A6,A4             ; |84| 
||         MV      .S1     A7,A5             ; |84| 

RL75:      ; CALL OCCURS {__mpyd}            ; |84| 
           MVKL    .S1     __addd,A3         ; |84| 
           MVKH    .S1     __addd,A3         ; |84| 
           MV      .L2X    A4,B4             ; |84| 
           CALL    .S2X    A3                ; |84| 
           MV      .L2X    A5,B5             ; |84| 
           MV      .S1     A12,A4            ; |84| 
           ADDKPC  .S2     RL76,B3,1         ; |84| 
           MV      .L1     A11,A5            ; |84| 
RL76:      ; CALL OCCURS {__addd}            ; |84| 
           MVKL    .S1     __mpyd,A3         ; |84| 
           MVKH    .S1     __mpyd,A3         ; |84| 
           LDW     .D2T2   *+SP(100),B5      ; |84| 
           CALL    .S2X    A3                ; |84| 
           LDW     .D2T2   *+SP(104),B4      ; |84| 
           ADDKPC  .S2     RL77,B3,3         ; |84| 
RL77:      ; CALL OCCURS {__mpyd}            ; |84| 
           LDW     .D2T1   *+SP(72),A3       ; |84| 
           MVKL    .S2     __negd,B4         ; |86| 
           MVKH    .S2     __negd,B4         ; |86| 
           STDW    .D1T1   A5:A4,*A10        ; |84| 
           CALL    .S2     B4                ; |86| 
           LDDW    .D1T1   *A3,A5:A4         ; |86| 
           ADDKPC  .S2     RL81,B3,3         ; |86| 
RL81:      ; CALL OCCURS {__negd}            ; |86| 
           MVKL    .S2     __mpyd,B6         ; |86| 
           MVKH    .S2     __mpyd,B6         ; |86| 

           CALL    .S2     B6                ; |86| 
||         LDW     .D2T2   *+SP(104),B4      ; |86| 

           LDW     .D2T2   *+SP(100),B5      ; |86| 
           ADDKPC  .S2     RL82,B3,3         ; |86| 
RL82:      ; CALL OCCURS {__mpyd}            ; |86| 
           MVKL    .S2     __mpyd,B6         ; |86| 
           MVKH    .S2     __mpyd,B6         ; |86| 

           CALL    .S2     B6                ; |86| 
||         LDW     .D2T2   *+SP(32),B5       ; |86| 

           LDW     .D2T2   *+SP(36),B4       ; |86| 
           ADDKPC  .S2     RL83,B3,3         ; |86| 
RL83:      ; CALL OCCURS {__mpyd}            ; |86| 
           LDW     .D2T1   *+SP(52),A3       ; |86| 
           LDW     .D2T1   *+SP(48),A8       ; |86| 
           NOP             1
           MVKL    .S1     __fltid,A6        ; |87| 
           MVKH    .S1     __fltid,A6        ; |87| 
           LDW     .D2T2   *+SP(56),B4       ; |86| 
           MPYLH   .M1     A8,A3,A9          ; |87| 

           MPYLH   .M1     A3,A8,A7          ; |87| 
||         CALL    .S2X    A6                ; |87| 

           ADDKPC  .S2     RL90,B3,0         ; |87| 
           ADD     .L1     A9,A7,A7          ; |87| 

           SHL     .S1     A7,16,A3          ; |87| 
||         MPYU    .M1     A8,A3,A7          ; |87| 

           STDW    .D2T1   A5:A4,*B4         ; |86| 
           ADD     .L1     A7,A3,A4          ; |87| 
RL90:      ; CALL OCCURS {__fltid}           ; |87| 
           LDW     .D2T1   *+SP(76),A3       ; |87| 
           LDW     .D2T2   *+SP(24),B4       ; |87| 
           LDW     .D2T2   *+SP(20),B5       ; |87| 
           MV      .L1     A5,A10            ; |87| 
           MV      .D1     A4,A11            ; |87| 

           MVKL    .S1     __mpyd,A3         ; |87| 
||         LDDW    .D1T1   *A3,A7:A6         ; |87| 

           MVKH    .S1     __mpyd,A3         ; |87| 
           NOP             1
           CALL    .S2X    A3                ; |87| 
           ADDKPC  .S2     RL91,B3,2         ; |87| 
           MV      .S1     A7,A5             ; |87| 
           MV      .L1     A6,A4             ; |87| 
RL91:      ; CALL OCCURS {__mpyd}            ; |87| 
           LDW     .D2T2   *+SP(84),B4       ; |87| 
           MVKL    .S1     __mpyd,A3         ; |87| 
           MVKH    .S1     __mpyd,A3         ; |87| 
           LDW     .D2T2   *+SP(96),B5       ; |87| 
           MV      .S1     A4,A13            ; |87| 
           LDDW    .D2T2   *B4,B7:B6         ; |87| 
           CALL    .S2X    A3                ; |87| 
           LDW     .D2T2   *+SP(92),B4       ; |87| 
           MV      .S1     A5,A12            ; |87| 
           ADDKPC  .S2     RL92,B3,0         ; |87| 
           MV      .L1X    B6,A4             ; |87| 
           MV      .L1X    B7,A5             ; |87| 
RL92:      ; CALL OCCURS {__mpyd}            ; |87| 
DW$L$_NALF_matrix$17$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$18$B:
           MVKL    .S1     __subd,A3         ; |87| 
           MVKH    .S1     __subd,A3         ; |87| 
           MV      .L2X    A4,B4             ; |87| 
           CALL    .S2X    A3                ; |87| 
           ADDKPC  .S2     RL93,B3,1         ; |87| 
           MV      .S1     A13,A4            ; |87| 
           MV      .L2X    A5,B5             ; |87| 
           MV      .L1     A12,A5            ; |87| 
RL93:      ; CALL OCCURS {__subd}            ; |87| 
DW$L$_NALF_matrix$18$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$19$B:
           MVKL    .S2     __mpyd,B6         ; |87| 
           MVKH    .S2     __mpyd,B6         ; |87| 
           CALL    .S2     B6                ; |87| 
           MV      .L2X    A4,B4             ; |87| 
           ADDKPC  .S2     RL94,B3,0         ; |87| 
           MV      .S1     A11,A4            ; |87| 
           MV      .L2X    A5,B5             ; |87| 
           MV      .L1     A10,A5            ; |87| 
RL94:      ; CALL OCCURS {__mpyd}            ; |87| 
DW$L$_NALF_matrix$19$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$20$B:

           LDW     .D2T2   *+SP(56),B4       ; |87| 
||         ADDK    .S2     152,B10           ; |69| 
||         ADD     .L2     1,B11,B11         ; |69| 

           LDW     .D2T1   *+SP(48),A29      ; |69| 
           LDW     .D2T1   *+SP(88),A30      ; |69| 
           LDW     .D2T2   *+SP(84),B5       ; |69| 
           LDW     .D2T1   *+SP(52),A6       ; |69| 

           ADDK    .S2     160,B4            ; |69| 
||         LDW     .D2T1   *+SP(76),A31      ; |69| 

           STW     .D2T2   B4,*+SP(56)       ; |69| 
||         ADD     .L1     2,A29,A28         ; |69| 

           LDW     .D2T2   *+SP(64),B4       ; |69| 
||         STDW    .D1T1   A5:A4,*A30        ; |87| 
||         ADDK    .S1     160,A30           ; |69| 

           LDW     .D2T1   *+SP(72),A3       ; |87| 
||         ADDK    .S2     160,B5            ; |69| 

           ADD     .L1     2,A6,A6           ; |69| 
||         STW     .D2T1   A28,*+SP(48)      ; |69| 

           ADDK    .S1     160,A31           ; |69| 
||         STW     .D2T1   A30,*+SP(88)      ; |69| 

           STW     .D2T2   B5,*+SP(84)       ; |69| 

           SUB     .L1X    B4,1,A0           ; |69| 
||         SUB     .L2     B4,1,B4           ; |69| 
||         STW     .D2T1   A6,*+SP(52)       ; |69| 

           ADDK    .S1     160,A3            ; |69| 
||         MVKL    .S2     __fltid,B4
||         STW     .D2T2   B4,*+SP(64)       ; |69| 

   [ A0]   B       .S1     L1                ; |69| 
||         MVKH    .S2     __fltid,B4
|| [ A0]   LDW     .D2T1   *+SP(52),A4

           STW     .D2T1   A3,*+SP(72)       ; |69| 
|| [ A0]   CALL    .S2     B4

           STW     .D2T1   A31,*+SP(76)      ; |69| 
	.dwpsn	"geopotential.c",88,0
           NOP             3
           ; BRANCHCC OCCURS {L1}            ; |69| 
DW$L$_NALF_matrix$20$E:
;** --------------------------------------------------------------------------*
L3:    

           LDW     .D2T2   *+SP(12),B4
||         ZERO    .L1     A13               ; |91| 
||         MVK     .S1     152,A3

           NOP             2
           LDW     .D2T1   *+SP(8),A31
           MPYLI   .M1     A3,A13,A5:A4

           ADD     .L1X    1,B4,A14          ; |92| 
||         CMPLT   .L2     B4,0,B0           ; |91| 

   [ B0]   BNOP    .S1     L7,5              ; |91| 
|| [!B0]   CMPLT   .L1     A13,0,A0          ; |92| 

           ; BRANCHCC OCCURS {L7}            ; |91| 
;** --------------------------------------------------------------------------*

   [ A0]   B       .S2     L6                ; |92| 
|| [!A0]   LDW     .D2T1   *+SP(40),A6       ; |94| 
|| [!A0]   MVKL    .S1     __mpyd,A3         ; |94| 
||         ADD     .L1     A31,A4,A5

   [!A0]   LDW     .D2T1   *+SP(28),A30      ; |94| 
|| [!A0]   MVKH    .S1     __mpyd,A3         ; |94| 

	.dwpsn	"geopotential.c",91,0
           NOP             2
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L4
;** --------------------------------------------------------------------------*
L4:    
DW$L$_NALF_matrix$23$B:
	.dwpsn	"geopotential.c",92,0
           NOP             2
           ; BRANCHCC OCCURS {L6}            ; |92| 
DW$L$_NALF_matrix$23$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$24$B:

           ADD     .L2X    8,A5,B12
||         ADD     .L1     A30,A4,A12
||         ADD     .S1     A6,A4,A11
||         ADD     .D1     1,A13,A10         ; |94| 

           LDDW    .D2T2   *B12++,B11:B10    ; |94| 
||         CALL    .S2X    A3                ; |94| 
||         LDDW    .D1T1   *A12,A5:A4        ; |94| 

           NOP             3
DW$L$_NALF_matrix$24$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L5:    
DW$L$_NALF_matrix$25$B:
	.dwpsn	"geopotential.c",93,0
           ADDKPC  .S2     RL95,B3,0         ; |94| 

           MV      .L2     B11,B5            ; |94| 
||         MV      .D2     B10,B4            ; |94| 

RL95:      ; CALL OCCURS {__mpyd}            ; |94| 
DW$L$_NALF_matrix$25$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$26$B:
           MVKL    .S2     __mpyd,B6         ; |95| 
           MVKH    .S2     __mpyd,B6         ; |95| 

           CALL    .S2     B6                ; |95| 
||         LDDW    .D1T1   *A11,A7:A6        ; |95| 

           ADDKPC  .S2     RL96,B3,0         ; |95| 
           MV      .L2     B11,B5            ; |95| 
           MV      .L2X    A4,B8             ; |94| 
           MV      .L2X    A5,B9             ; |94| 

           STDW    .D1T2   B9:B8,*A12++      ; |94| 
||         MV      .S1     A6,A4             ; |95| 
||         MV      .L1     A7,A5             ; |95| 
||         MV      .D2     B10,B4            ; |95| 

RL96:      ; CALL OCCURS {__mpyd}            ; |95| 
DW$L$_NALF_matrix$26$E:
;** --------------------------------------------------------------------------*
DW$L$_NALF_matrix$27$B:

           STDW    .D1T1   A5:A4,*A11++      ; |95| 
||         MVKL    .S1     __mpyd,A3         ; |94| 
||         SUB     .L1     A10,1,A0          ; |92| 

   [ A0]   BNOP    .S2     L5,1              ; |92| 
||         SUB     .L1     A10,1,A10         ; |92| 
||         MVKH    .S1     __mpyd,A3         ; |94| 
|| [ A0]   LDDW    .D2T2   *B12++,B11:B10    ; |94| 
|| [ A0]   LDDW    .D1T1   *A12,A5:A4        ; |94| 

   [ A0]   CALL    .S2X    A3                ; |94| 
	.dwpsn	"geopotential.c",96,0
           NOP             3
           ; BRANCHCC OCCURS {L5}            ; |92| 
DW$L$_NALF_matrix$27$E:
;** --------------------------------------------------------------------------*
L6:    
DW$L$_NALF_matrix$28$B:

           MVK     .S1     152,A3
||         SUB     .L1     A14,1,A0          ; |91| 
||         ADD     .D1     1,A13,A13         ; |91| 
||         LDW     .D2T1   *+SP(8),A31

   [ A0]   B       .S2     L4                ; |91| 
||         MV      .S1     A0,A1             ; guard predicate rewrite
|| [ A0]   CMPLT   .L1     A13,0,A0          ; |92| 
||         SUB     .D1     A14,1,A14         ; |91| 
||         MPYLI   .M1     A3,A13,A5:A4

   [!A1]   ZERO    .L1     A0                ; nullify predicate

   [ A0]   B       .S2     L6                ; |92| 
|| [!A0]   LDW     .D2T1   *+SP(40),A6       ; |94| 
|| [!A0]   MVKL    .S1     __mpyd,A3         ; |94| 

   [!A0]   LDW     .D2T1   *+SP(28),A30      ; |94| 
|| [!A0]   MVKH    .S1     __mpyd,A3         ; |94| 

           NOP             1
           ADD     .L1     A31,A4,A5
           ; BRANCHCC OCCURS {L4}            ; |91| 
DW$L$_NALF_matrix$28$E:
;** --------------------------------------------------------------------------*
L7:    
           LDW     .D2T2   *+SP(172),B3      ; |97| 
           LDDW    .D2T1   *+SP(152),A11:A10 ; |97| 
           LDDW    .D2T1   *+SP(160),A13:A12 ; |97| 
           LDW     .D2T1   *+SP(168),A14     ; |97| 
           LDDW    .D2T2   *+SP(176),B11:B10 ; |97| 

           RET     .S2     B3                ; |97| 
||         LDW     .D2T1   *+SP(192),A15     ; |97| 

           LDDW    .D2T2   *+SP(184),B13:B12 ; |97| 
           NOP             3
	.dwpsn	"geopotential.c",97,1
           ADDK    .S2     192,SP            ; |97| 
           ; BRANCH OCCURS {B3}              ; |97| 

DW$13	.dwtag  DW_TAG_loop
	.dwattr DW$13, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L4:1:1472539734")
	.dwattr DW$13, DW_AT_begin_file("geopotential.c")
	.dwattr DW$13, DW_AT_begin_line(0x5b)
	.dwattr DW$13, DW_AT_end_line(0x60)
DW$14	.dwtag  DW_TAG_loop_range
	.dwattr DW$14, DW_AT_low_pc(DW$L$_NALF_matrix$23$B)
	.dwattr DW$14, DW_AT_high_pc(DW$L$_NALF_matrix$23$E)
DW$15	.dwtag  DW_TAG_loop_range
	.dwattr DW$15, DW_AT_low_pc(DW$L$_NALF_matrix$24$B)
	.dwattr DW$15, DW_AT_high_pc(DW$L$_NALF_matrix$24$E)
DW$16	.dwtag  DW_TAG_loop_range
	.dwattr DW$16, DW_AT_low_pc(DW$L$_NALF_matrix$28$B)
	.dwattr DW$16, DW_AT_high_pc(DW$L$_NALF_matrix$28$E)

DW$17	.dwtag  DW_TAG_loop
	.dwattr DW$17, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L5:2:1472539734")
	.dwattr DW$17, DW_AT_begin_file("geopotential.c")
	.dwattr DW$17, DW_AT_begin_line(0x5c)
	.dwattr DW$17, DW_AT_end_line(0x60)
DW$18	.dwtag  DW_TAG_loop_range
	.dwattr DW$18, DW_AT_low_pc(DW$L$_NALF_matrix$25$B)
	.dwattr DW$18, DW_AT_high_pc(DW$L$_NALF_matrix$25$E)
DW$19	.dwtag  DW_TAG_loop_range
	.dwattr DW$19, DW_AT_low_pc(DW$L$_NALF_matrix$26$B)
	.dwattr DW$19, DW_AT_high_pc(DW$L$_NALF_matrix$26$E)
DW$20	.dwtag  DW_TAG_loop_range
	.dwattr DW$20, DW_AT_low_pc(DW$L$_NALF_matrix$27$B)
	.dwattr DW$20, DW_AT_high_pc(DW$L$_NALF_matrix$27$E)
	.dwendtag DW$17

	.dwendtag DW$13


DW$21	.dwtag  DW_TAG_loop
	.dwattr DW$21, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L1:1:1472539734")
	.dwattr DW$21, DW_AT_begin_file("geopotential.c")
	.dwattr DW$21, DW_AT_begin_line(0x45)
	.dwattr DW$21, DW_AT_end_line(0x58)
DW$22	.dwtag  DW_TAG_loop_range
	.dwattr DW$22, DW_AT_low_pc(DW$L$_NALF_matrix$8$B)
	.dwattr DW$22, DW_AT_high_pc(DW$L$_NALF_matrix$8$E)
DW$23	.dwtag  DW_TAG_loop_range
	.dwattr DW$23, DW_AT_low_pc(DW$L$_NALF_matrix$9$B)
	.dwattr DW$23, DW_AT_high_pc(DW$L$_NALF_matrix$9$E)
DW$24	.dwtag  DW_TAG_loop_range
	.dwattr DW$24, DW_AT_low_pc(DW$L$_NALF_matrix$10$B)
	.dwattr DW$24, DW_AT_high_pc(DW$L$_NALF_matrix$10$E)
DW$25	.dwtag  DW_TAG_loop_range
	.dwattr DW$25, DW_AT_low_pc(DW$L$_NALF_matrix$16$B)
	.dwattr DW$25, DW_AT_high_pc(DW$L$_NALF_matrix$16$E)
DW$26	.dwtag  DW_TAG_loop_range
	.dwattr DW$26, DW_AT_low_pc(DW$L$_NALF_matrix$17$B)
	.dwattr DW$26, DW_AT_high_pc(DW$L$_NALF_matrix$17$E)
DW$27	.dwtag  DW_TAG_loop_range
	.dwattr DW$27, DW_AT_low_pc(DW$L$_NALF_matrix$18$B)
	.dwattr DW$27, DW_AT_high_pc(DW$L$_NALF_matrix$18$E)
DW$28	.dwtag  DW_TAG_loop_range
	.dwattr DW$28, DW_AT_low_pc(DW$L$_NALF_matrix$19$B)
	.dwattr DW$28, DW_AT_high_pc(DW$L$_NALF_matrix$19$E)
DW$29	.dwtag  DW_TAG_loop_range
	.dwattr DW$29, DW_AT_low_pc(DW$L$_NALF_matrix$20$B)
	.dwattr DW$29, DW_AT_high_pc(DW$L$_NALF_matrix$20$E)

DW$30	.dwtag  DW_TAG_loop
	.dwattr DW$30, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L2:2:1472539734")
	.dwattr DW$30, DW_AT_begin_file("geopotential.c")
	.dwattr DW$30, DW_AT_begin_line(0x47)
	.dwattr DW$30, DW_AT_end_line(0x4f)
DW$31	.dwtag  DW_TAG_loop_range
	.dwattr DW$31, DW_AT_low_pc(DW$L$_NALF_matrix$11$B)
	.dwattr DW$31, DW_AT_high_pc(DW$L$_NALF_matrix$11$E)
DW$32	.dwtag  DW_TAG_loop_range
	.dwattr DW$32, DW_AT_low_pc(DW$L$_NALF_matrix$12$B)
	.dwattr DW$32, DW_AT_high_pc(DW$L$_NALF_matrix$12$E)
DW$33	.dwtag  DW_TAG_loop_range
	.dwattr DW$33, DW_AT_low_pc(DW$L$_NALF_matrix$13$B)
	.dwattr DW$33, DW_AT_high_pc(DW$L$_NALF_matrix$13$E)
DW$34	.dwtag  DW_TAG_loop_range
	.dwattr DW$34, DW_AT_low_pc(DW$L$_NALF_matrix$14$B)
	.dwattr DW$34, DW_AT_high_pc(DW$L$_NALF_matrix$14$E)
DW$35	.dwtag  DW_TAG_loop_range
	.dwattr DW$35, DW_AT_low_pc(DW$L$_NALF_matrix$15$B)
	.dwattr DW$35, DW_AT_high_pc(DW$L$_NALF_matrix$15$E)
	.dwendtag DW$30

	.dwendtag DW$21

	.dwattr DW$9, DW_AT_end_file("geopotential.c")
	.dwattr DW$9, DW_AT_end_line(0x61)
	.dwattr DW$9, DW_AT_end_column(0x01)
	.dwendtag DW$9

	.sect	".text"
	.global	_Geopotential_Gravitation

DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("Geopotential_Gravitation"), DW_AT_symbol_name("_Geopotential_Gravitation")
	.dwattr DW$36, DW_AT_low_pc(_Geopotential_Gravitation)
	.dwattr DW$36, DW_AT_high_pc(0x00)
	.dwattr DW$36, DW_AT_begin_file("geopotential.c")
	.dwattr DW$36, DW_AT_begin_line(0x6d)
	.dwattr DW$36, DW_AT_begin_column(0x06)
	.dwattr DW$36, DW_AT_frame_base[DW_OP_breg31 224]
	.dwattr DW$36, DW_AT_skeletal(0x01)
	.dwpsn	"geopotential.c",113,1

;******************************************************************************
;* FUNCTION NAME: _Geopotential_Gravitation                                   *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 176 Auto + 44 Save = 220 byte               *
;******************************************************************************
_Geopotential_Gravitation:
;** --------------------------------------------------------------------------*
DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pGP"), DW_AT_symbol_name("_pGP")
	.dwattr DW$37, DW_AT_type(*DW$T$29)
	.dwattr DW$37, DW_AT_location[DW_OP_reg4]
DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("r"), DW_AT_symbol_name("_r")
	.dwattr DW$38, DW_AT_type(*DW$T$32)
	.dwattr DW$38, DW_AT_location[DW_OP_reg20]
DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$39, DW_AT_type(*DW$T$19)
	.dwattr DW$39, DW_AT_location[DW_OP_reg6]
DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("g"), DW_AT_symbol_name("_g")
	.dwattr DW$40, DW_AT_type(*DW$T$32)
	.dwattr DW$40, DW_AT_location[DW_OP_reg22]

           ADDK    .S2     -224,SP           ; |113| 
||         MV      .L1X    SP,A31            ; |113| 

           STW     .D2T2   B4,*+SP(4)        ; |113| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         MVKL    .S1     __mpyd,A3         ; |125| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .L1X    B4,A11            ; |113| 
||         STDW    .D2T2   B13:B12,*+SP(216)
||         MVKH    .S1     __mpyd,A3         ; |125| 

           LDDW    .D1T2   *A11,B5:B4        ; |125| 
||         MV      .L1     A4,A10            ; |113| 

           CALL    .S2X    A3                ; |125| 
||         LDW     .D1T2   *A10,B12          ; |122| 

           LDDW    .D1T1   *+A11(8),A13:A12  ; |125| 
           STDW    .D2T2   B11:B10,*+SP(208)
           STW     .D2T2   B3,*+SP(204)

           MV      .L1X    B5,A5             ; |125| 
||         MV      .L2X    A6,B10            ; |113| 
||         STW     .D2T1   A15,*+SP(224)

           ADDKPC  .S2     RL103,B3,0        ; |125| 
||         MV      .L1X    B4,A4             ; |125| 
||         CMPGT   .L2X    A6,B12,B11        ; |122| 
||         STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B6,*+SP(8)        ; |113| 

RL103:     ; CALL OCCURS {__mpyd}            ; |125| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |125| 
           MVKH    .S1     __mpyd,A3         ; |125| 
           MV      .L2X    A12,B4            ; |125| 
           CALL    .S2X    A3                ; |125| 
           MV      .L1     A4,A15            ; |125| 
           ADDKPC  .S2     RL104,B3,0        ; |125| 
           MV      .S1     A12,A4            ; |125| 
           MV      .L2X    A13,B5            ; |125| 

           MV      .D1     A5,A14            ; |125| 
||         MV      .L1     A13,A5            ; |125| 

RL104:     ; CALL OCCURS {__mpyd}            ; |125| 
           MVKL    .S2     __addd,B6         ; |125| 
           MVKH    .S2     __addd,B6         ; |125| 
           CALL    .S2     B6                ; |125| 
           MV      .L2X    A4,B4             ; |125| 
           ADDKPC  .S2     RL105,B3,0        ; |125| 
           MV      .S1     A15,A4            ; |125| 
           MV      .L2X    A5,B5             ; |125| 
           MV      .L1     A14,A5            ; |125| 
RL105:     ; CALL OCCURS {__addd}            ; |125| 
           MVKL    .S2     __mpyd,B6         ; |126| 

           LDDW    .D1T2   *+A11(16),B5:B4   ; |126| 
||         MVKH    .S2     __mpyd,B6         ; |126| 

           CALL    .S2     B6                ; |126| 
           MV      .S1     A4,A13            ; |125| 
           ADDKPC  .S2     RL110,B3,0        ; |126| 
           MV      .S1     A5,A12            ; |125| 
           MV      .L1X    B4,A4             ; |126| 
           MV      .L1X    B5,A5             ; |126| 
RL110:     ; CALL OCCURS {__mpyd}            ; |126| 
           MVKL    .S2     __addd,B6         ; |126| 
           MVKH    .S2     __addd,B6         ; |126| 
           CALL    .S2     B6                ; |126| 
           MV      .L2X    A4,B4             ; |126| 
           ADDKPC  .S2     RL111,B3,0        ; |126| 
           MV      .S1     A13,A4            ; |126| 
           MV      .L2X    A5,B5             ; |126| 
           MV      .L1     A12,A5            ; |126| 
RL111:     ; CALL OCCURS {__addd}            ; |126| 
           MVKL    .S2     _sqrt,B4          ; |126| 
           MVKH    .S2     _sqrt,B4          ; |126| 
           CALL    .S2     B4                ; |126| 
           ADDKPC  .S2     RL112,B3,4        ; |126| 
RL112:     ; CALL OCCURS {_sqrt}             ; |126| 
           MVKL    .S2     _sqrt,B4          ; |127| 
           MVKH    .S2     _sqrt,B4          ; |127| 
           CALL    .S2     B4                ; |127| 
           MV      .L1     A4,A15            ; |126| 
           ADDKPC  .S2     RL113,B3,0        ; |127| 
           MV      .S1     A13,A4            ; |127| 
           MV      .D1     A5,A14            ; |126| 
           MV      .L1     A12,A5            ; |127| 
RL113:     ; CALL OCCURS {_sqrt}             ; |127| 
           MVKL    .S2     __divd,B6         ; |128| 
           MVKH    .S2     __divd,B6         ; |128| 
           CALL    .S2     B6                ; |128| 
           MV      .S1     A5,A12            ; |127| 
           ZERO    .L1     A5                ; |128| 
           MV      .L1     A4,A13            ; |127| 

           STW     .D2T1   A13,*+SP(16)      ; |127| 
||         MV      .L2X    A15,B4            ; |128| 

           ADDKPC  .S2     RL114,B3,0        ; |128| 
||         MVKH    .S1     0x3ff00000,A5     ; |128| 
||         STW     .D2T1   A12,*+SP(12)      ; |127| 
||         ZERO    .L1     A4                ; |128| 
||         MV      .L2X    A14,B5            ; |128| 

RL114:     ; CALL OCCURS {__divd}            ; |128| 
           MVKL    .S2     __divd,B6         ; |128| 
           MVKH    .S2     __divd,B6         ; |128| 
           CALL    .S2     B6                ; |128| 
           MV      .L2X    A13,B4            ; |128| 
           STW     .D2T1   A5,*+SP(20)       ; |128| 
           ZERO    .L1     A5                ; |128| 
           ADDKPC  .S2     RL115,B3,0        ; |128| 

           MVKH    .S1     0x3ff00000,A5     ; |128| 
||         MV      .L2X    A12,B5            ; |128| 
||         STW     .D2T1   A4,*+SP(24)       ; |128| 
||         ZERO    .L1     A4                ; |128| 

RL115:     ; CALL OCCURS {__divd}            ; |128| 
           MVKL    .S2     __mpyd,B6         ; |130| 
           MVKH    .S2     __mpyd,B6         ; |130| 

           CALL    .S2     B6                ; |130| 
||         LDDW    .D1T1   *+A11(8),A7:A6    ; |130| 

           MV      .L1     A5,A12            ; |128| 
           MV      .L2X    A4,B4             ; |128| 
           MV      .L1     A4,A13            ; |128| 
           STW     .D2T1   A13,*+SP(32)      ; |128| 

           ADDKPC  .S2     RL116,B3,0        ; |130| 
||         MV      .L1     A7,A5             ; |130| 
||         MV      .S1     A6,A4             ; |130| 
||         STW     .D2T1   A12,*+SP(28)      ; |128| 
||         MV      .L2X    A5,B5             ; |128| 

RL116:     ; CALL OCCURS {__mpyd}            ; |130| 
           MVKL    .S2     __mpyd,B6         ; |130| 
           MVKH    .S2     __mpyd,B6         ; |130| 

           CALL    .S2     B6                ; |130| 
||         LDDW    .D1T1   *A11,A7:A6        ; |130| 

           MV      .L2X    A13,B4            ; |130| 
           ADDKPC  .S2     RL117,B3,0        ; |130| 
           MV      .L2X    A12,B5            ; |130| 
           STW     .D2T1   A4,*+SP(40)       ; |130| 

           MV      .S1     A6,A4             ; |130| 
||         STW     .D2T1   A5,*+SP(36)       ; |130| 
||         MV      .L1     A7,A5             ; |130| 

RL117:     ; CALL OCCURS {__mpyd}            ; |130| 
;** --------------------------------------------------------------------------*
           MV      .L1X    B11,A0            ; |130| 
   [ A0]   MV      .L2     B12,B10           ; |122| 
;** --------------------------------------------------------------------------*

           LDW     .D2T1   *+SP(24),A12      ; |134| 
||         MVKL    .S2     __mpyd,B6         ; |134| 

           LDW     .D2T1   *+SP(20),A11      ; |134| 
||         MVKH    .S2     __mpyd,B6         ; |134| 
||         MV      .L1     A11,A3

           CALL    .S2     B6                ; |134| 
||         LDDW    .D1T1   *+A3(16),A7:A6    ; |134| 

           STW     .D2T1   A4,*+SP(48)       ; |130| 
           ADDKPC  .S2     RL119,B3,0        ; |134| 
           STW     .D2T1   A5,*+SP(44)       ; |130| 
           MV      .L2X    A12,B4            ; |134| 

           MV      .L2X    A11,B5            ; |134| 
||         MV      .S1     A6,A4             ; |134| 
||         MV      .L1     A7,A5             ; |134| 

RL119:     ; CALL OCCURS {__mpyd}            ; |134| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _NALF_matrix,A3   ; |134| 
           MVKH    .S1     _NALF_matrix,A3   ; |134| 
           MV      .S1     A5,A7             ; |134| 
           CALL    .S2X    A3                ; |134| 
           ADDKPC  .S2     RL120,B3,1        ; |134| 
           MV      .L2     B10,B4            ; |134| 
           MV      .D1     A4,A6             ; |134| 
           MV      .L1     A10,A4            ; |134| 
RL120:     ; CALL OCCURS {_NALF_matrix}      ; |134| 
           MVKL    .S1     __mpyd,A3         ; |137| 
           MVKH    .S1     __mpyd,A3         ; |137| 
           MVKL    .S2     0x415854a6,B5     ; |137| 
           CALL    .S2X    A3                ; |137| 
           ZERO    .L2     B4                ; |137| 
           MVKH    .S2     0x415854a6,B5     ; |137| 
           MVKH    .S2     0x40000000,B4     ; |137| 
           MV      .S1     A12,A4            ; |137| 

           MV      .L1     A11,A5            ; |137| 
||         ADDKPC  .S2     RL121,B3,0        ; |137| 

RL121:     ; CALL OCCURS {__mpyd}            ; |137| 
;** --------------------------------------------------------------------------*

           ZERO    .L1     A6                ; |138| 
||         CMPLT   .L2     B10,2,B0          ; |139| 
||         MVK     .S1     5784,A8
||         MVK     .S2     2896,B31
||         ZERO    .D1     A30               ; |138| 
||         STW     .D2T1   A5,*+SP(100)      ; |137| 

           MVK     .S1     11560,A3
||         MVK     .S2     8672,B4
||         ZERO    .L1     A7:A6             ; |138| 
||         ADD     .D1     A8,A10,A28
||         STW     .D2T1   A6,*+SP(52)       ; |138| 
||         SUB     .L2     B10,1,B6          ; |141| 

           ADD     .L1     A3,A10,A3
||         ZERO    .S1     A9                ; |138| 
||         ZERO    .D1     A31               ; |138| 
||         LDW     .D2T1   *+SP(52),A29
||         ADD     .L2X    B4,A10,B5

           MVK     .S1     0x130,A12
||         ADD     .L2X    B31,A10,B4
||         STW     .D2T1   A7,*+SP(64)       ; |138| 

           STW     .D2T1   A6,*+SP(60)       ; |138| 
           STW     .D2T1   A28,*+SP(92)      ; |138| 
           STW     .D2T2   B5,*+SP(76)

           MVKH    .S1     0x3ff00000,A29    ; |138| 
||         STW     .D2T2   B4,*+SP(88)       ; |138| 

           STW     .D2T1   A29,*+SP(52)      ; |138| 

           MVK     .L2     0x3,B30           ; |139| 
||         STW     .D2T1   A3,*+SP(96)       ; |138| 

           STW     .D2T2   B30,*+SP(84)      ; |138| 
           STW     .D2T2   B6,*+SP(80)       ; |138| 

   [ B0]   B       .S1     L10               ; |139| 
||         STW     .D2T1   A30,*+SP(68)

           STW     .D2T1   A9,*+SP(56)       ; |138| 
           STW     .D2T1   A31,*+SP(72)
           STW     .D2T1   A4,*+SP(104)      ; |137| 
           STW     .D2T1   A5,*+SP(108)      ; |137| 
           STW     .D2T1   A4,*+SP(112)      ; |137| 
           ; BRANCHCC OCCURS {L10}           ; |139| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(88),B4
           NOP             1
           LDW     .D2T2   *+SP(76),B31
           LDW     .D2T1   *+SP(92),A3
           LDW     .D2T1   *+SP(84),A29
           ADD     .L2X    A12,B4,B4
	.dwpsn	"geopotential.c",139,0
           STW     .D2T2   B4,*+SP(120)
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L8
;** --------------------------------------------------------------------------*
L8:    
DW$L$_Geopotential_Gravitation$8$B:
	.dwpsn	"geopotential.c",140,0

           LDW     .D2T2   *+SP(120),B6
||         ZERO    .S1     A28               ; |141| 
||         ADD     .L2X    A12,B31,B4
||         ZERO    .S2     B5                ; |141| 
||         ZERO    .L1     A5:A4             ; |141| 
||         ZERO    .D1     A27               ; |141| 

           STW     .D2T1   A28,*+SP(156)     ; |141| 
||         ZERO    .L1     A28               ; |141| 
||         ZERO    .S1     A30               ; |141| 
||         ADD     .D1     A12,A3,A3
||         ZERO    .L2     B10               ; |141| 
||         ZERO    .S2     B13               ; |141| 

           LDW     .D2T1   *+SP(96),A31
||         MVKH    .S2     0x3ff00000,B10    ; |141| 
||         ZERO    .L2     B12               ; |141| 

           STW     .D2T1   A29,*+SP(148)     ; |146| 
||         ZERO    .L2     B11               ; |141| 

           STW     .D2T1   A29,*+SP(144)     ; |146| 
           STW     .D2T1   A28,*+SP(160)     ; |141| 
           STW     .D2T2   B4,*+SP(132)

           STW     .D2T1   A27,*+SP(152)     ; |141| 
||         ADD     .L1     A12,A31,A13

           STW     .D2T1   A30,*+SP(128)
           STW     .D2T2   B5,*+SP(124)
           STW     .D2T1   A4,*+SP(136)
           STW     .D2T1   A5,*+SP(140)

           STW     .D2T1   A3,*+SP(116)
||         MVKL    .S1     __mpyd,A3         ; |147| 

           LDDW    .D2T2   *B6++,B5:B4       ; |147| 
||         MVKH    .S1     __mpyd,A3         ; |147| 

           LDW     .D2T1   *+SP(116),A8      ; |147| 
	.dwpsn	"geopotential.c",144,0
           NOP             2
DW$L$_Geopotential_Gravitation$8$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
L9:    
DW$L$_Geopotential_Gravitation$9$B:
	.dwpsn	"geopotential.c",145,0
           STW     .D2T2   B6,*+SP(120)      ; |147| 
           STW     .D2T2   B4,*+SP(164)      ; |147| 
           STW     .D2T2   B5,*+SP(168)      ; |147| 
           LDDW    .D1T1   *A8++,A7:A6       ; |147| 

           LDW     .D2T1   *+SP(164),A4      ; |147| 
||         CALL    .S2X    A3                ; |147| 

           LDW     .D2T1   *+SP(168),A5      ; |147| 
           STW     .D2T1   A8,*+SP(116)      ; |147| 
           ADDKPC  .S2     RL124,B3,0        ; |147| 

           MV      .S1     A7,A10            ; |147| 
||         MV      .L1     A6,A11            ; |147| 

           MV      .L2     B10,B5            ; |147| 
||         MV      .D2     B12,B4            ; |147| 

RL124:     ; CALL OCCURS {__mpyd}            ; |147| 
DW$L$_Geopotential_Gravitation$9$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$10$B:
           MVKL    .S1     __mpyd,A3         ; |147| 
           MVKH    .S1     __mpyd,A3         ; |147| 
           LDW     .D2T2   *+SP(124),B5      ; |147| 
           CALL    .S2X    A3                ; |147| 
           MV      .L1     A4,A15            ; |147| 
           ADDKPC  .S2     RL125,B3,0        ; |147| 
           MV      .S1     A11,A4            ; |147| 
           MV      .L2     B11,B4            ; |147| 

           MV      .D1     A5,A14            ; |147| 
||         MV      .L1     A10,A5            ; |147| 

RL125:     ; CALL OCCURS {__mpyd}            ; |147| 
           MVKL    .S1     __addd,A3         ; |147| 
           MVKH    .S1     __addd,A3         ; |147| 
           MV      .L2X    A4,B4             ; |147| 
           CALL    .S2X    A3                ; |147| 
           ADDKPC  .S2     RL126,B3,1        ; |147| 
           MV      .S1     A15,A4            ; |147| 
           MV      .L2X    A5,B5             ; |147| 
           MV      .L1     A14,A5            ; |147| 
RL126:     ; CALL OCCURS {__addd}            ; |147| 
           LDW     .D2T2   *+SP(132),B7      ; |147| 
           MVKL    .S2     __mpyd,B6         ; |147| 
           MVKH    .S2     __mpyd,B6         ; |147| 
           MV      .L1     A5,A14            ; |147| 
           MV      .L1     A4,A15            ; |147| 
           LDDW    .D2T2   *B7++,B5:B4       ; |147| 
           NOP             3
           STW     .D2T2   B7,*+SP(132)      ; |147| 
           STW     .D2T2   B5,*+SP(176)      ; |147| 
           STW     .D2T2   B4,*+SP(172)      ; |147| 

           LDW     .D2T1   *+SP(176),A5      ; |147| 
||         MV      .L2X    A5,B5             ; |147| 
||         CALL    .S2     B6                ; |147| 

           LDW     .D2T1   *+SP(172),A4      ; |147| 
||         MV      .L2X    A4,B4             ; |147| 

           ADDKPC  .S2     RL128,B3,3        ; |147| 
RL128:     ; CALL OCCURS {__mpyd}            ; |147| 
           MVKL    .S2     __addd,B6         ; |147| 
           MVKH    .S2     __addd,B6         ; |147| 

           LDW     .D2T1   *+SP(128),A5      ; |147| 
||         CALL    .S2     B6                ; |147| 
||         MV      .L2X    A5,B5             ; |147| 

           LDW     .D2T1   *+SP(156),A4      ; |147| 
||         MV      .L2X    A4,B4             ; |147| 

           ADDKPC  .S2     RL129,B3,3        ; |147| 
RL129:     ; CALL OCCURS {__addd}            ; |147| 
           MVKL    .S2     __mpyd,B6         ; |148| 

           MVKH    .S2     __mpyd,B6         ; |148| 
||         STW     .D2T1   A5,*+SP(128)      ; |147| 

           CALL    .S2     B6                ; |148| 
||         LDDW    .D1T1   *A13++,A7:A6      ; |148| 

           ADDKPC  .S2     RL131,B3,0        ; |148| 
           STW     .D2T1   A4,*+SP(156)      ; |147| 
           MV      .L2X    A14,B5            ; |148| 
           MV      .L2X    A15,B4            ; |148| 

           MV      .L1     A6,A4             ; |148| 
||         MV      .S1     A7,A5             ; |148| 

RL131:     ; CALL OCCURS {__mpyd}            ; |148| 
           MVKL    .S2     __addd,B6         ; |148| 
           MVKH    .S2     __addd,B6         ; |148| 

           LDW     .D2T1   *+SP(140),A5      ; |148| 
||         CALL    .S2     B6                ; |148| 
||         MV      .L2X    A5,B5             ; |148| 

           LDW     .D2T1   *+SP(136),A4      ; |148| 
||         MV      .L2X    A4,B4             ; |148| 

           ADDKPC  .S2     RL132,B3,3        ; |148| 
RL132:     ; CALL OCCURS {__addd}            ; |148| 
           MVKL    .S2     __fltid,B4        ; |150| 
           MVKH    .S2     __fltid,B4        ; |150| 
           CALL    .S2     B4                ; |150| 
           ADDKPC  .S2     RL150,B3,0        ; |150| 
           STW     .D2T1   A5,*+SP(140)      ; |148| 
           NOP             1
           STW     .D2T1   A4,*+SP(136)      ; |148| 
           MV      .L1X    B13,A4            ; |150| 
RL150:     ; CALL OCCURS {__fltid}           ; |150| 
           MVKL    .S1     __mpyd,A3         ; |150| 
           MVKH    .S1     __mpyd,A3         ; |150| 
           MV      .L1     A5,A14            ; |150| 
           CALL    .S2X    A3                ; |150| 
           MV      .D2     B12,B4            ; |150| 
           MV      .L2     B10,B5            ; |150| 
           MV      .D1     A4,A15            ; |150| 
           MV      .L1     A11,A4            ; |150| 

           MV      .S1     A10,A5            ; |150| 
||         ADDKPC  .S2     RL151,B3,0        ; |150| 

RL151:     ; CALL OCCURS {__mpyd}            ; |150| 
           MVKL    .S2     __mpyd,B6         ; |150| 

           MVKH    .S2     __mpyd,B6         ; |150| 
||         LDW     .D2T2   *+SP(124),B5      ; |150| 

           LDW     .D2T1   *+SP(168),A5      ; |150| 
||         MV      .L1     A5,A10            ; |150| 
||         CALL    .S2     B6                ; |150| 

           LDW     .D2T1   *+SP(164),A4      ; |150| 
||         MV      .L1     A4,A11            ; |150| 

           MV      .L2     B11,B4            ; |150| 
           ADDKPC  .S2     RL152,B3,2        ; |150| 
RL152:     ; CALL OCCURS {__mpyd}            ; |150| 
           MVKL    .S1     __subd,A3         ; |150| 
           MVKH    .S1     __subd,A3         ; |150| 
           MV      .L2X    A4,B4             ; |150| 
           CALL    .S2X    A3                ; |150| 
           MV      .L2X    A5,B5             ; |150| 
           MV      .S1     A11,A4            ; |150| 
           ADDKPC  .S2     RL153,B3,1        ; |150| 
           MV      .L1     A10,A5            ; |150| 
RL153:     ; CALL OCCURS {__subd}            ; |150| 
DW$L$_Geopotential_Gravitation$10$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$11$B:
           MVKL    .S1     __mpyd,A3         ; |150| 
           MVKH    .S1     __mpyd,A3         ; |150| 
           MV      .L2X    A4,B4             ; |150| 
           CALL    .S2X    A3                ; |150| 
           ADDKPC  .S2     RL154,B3,1        ; |150| 
           MV      .S1     A15,A4            ; |150| 
           MV      .L2X    A5,B5             ; |150| 
           MV      .L1     A14,A5            ; |150| 
RL154:     ; CALL OCCURS {__mpyd}            ; |150| 
DW$L$_Geopotential_Gravitation$11$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$12$B:
           MVKL    .S1     __mpyd,A3         ; |150| 
           MVKH    .S1     __mpyd,A3         ; |150| 
           LDW     .D2T2   *+SP(176),B5      ; |150| 
           CALL    .S2X    A3                ; |150| 
           LDW     .D2T2   *+SP(172),B4      ; |150| 
           ADDKPC  .S2     RL155,B3,3        ; |150| 
RL155:     ; CALL OCCURS {__mpyd}            ; |150| 
           MVKL    .S1     __addd,A3         ; |150| 
           MVKH    .S1     __addd,A3         ; |150| 
           MV      .L2X    A5,B5             ; |150| 

           CALL    .S2X    A3                ; |150| 
||         LDW     .D2T1   *+SP(152),A5      ; |150| 

           LDW     .D2T1   *+SP(160),A4      ; |150| 
||         MV      .L2X    A4,B4             ; |150| 

           ADDKPC  .S2     RL156,B3,3        ; |150| 
RL156:     ; CALL OCCURS {__addd}            ; |150| 
           MVKL    .S1     __mpyd,A3         ; |154| 

           MVKH    .S1     __mpyd,A3         ; |154| 
||         STW     .D2T1   A5,*+SP(152)      ; |150| 

           STW     .D2T1   A4,*+SP(160)      ; |150| 

           CALL    .S2X    A3                ; |154| 
||         LDW     .D2T1   *+SP(36),A5       ; |154| 

           LDW     .D2T1   *+SP(40),A4       ; |154| 
           ADDKPC  .S2     RL159,B3,1        ; |154| 
           MV      .L2     B10,B5            ; |154| 
           MV      .D2     B12,B4            ; |154| 
RL159:     ; CALL OCCURS {__mpyd}            ; |154| 
           MVKL    .S1     __mpyd,A3         ; |154| 
           MVKH    .S1     __mpyd,A3         ; |154| 
           LDW     .D2T2   *+SP(124),B5      ; |154| 

           LDW     .D2T1   *+SP(44),A5       ; |153| 
||         MV      .L1     A5,A10            ; |154| 
||         CALL    .S2X    A3                ; |154| 

           LDW     .D2T1   *+SP(48),A4       ; |154| 
||         MV      .L1     A4,A11            ; |154| 

           MV      .L2     B11,B4            ; |153| 
           ADDKPC  .S2     RL160,B3,2        ; |154| 
RL160:     ; CALL OCCURS {__mpyd}            ; |154| 
           MVKL    .S2     __addd,B6         ; |154| 
           MVKH    .S2     __addd,B6         ; |154| 
           CALL    .S2     B6                ; |154| 
           MV      .L2X    A4,B4             ; |154| 
           MV      .L2X    A5,B5             ; |154| 
           MV      .S1     A11,A4            ; |154| 
           ADDKPC  .S2     RL161,B3,0        ; |154| 
           MV      .L1     A10,A5            ; |154| 
RL161:     ; CALL OCCURS {__addd}            ; |154| 

           MVKL    .S2     __mpyd,B6         ; |155| 
||         LDW     .D2T1   *+SP(124),A10     ; |154| 

           MVKH    .S2     __mpyd,B6         ; |155| 
||         STW     .D2T1   A5,*+SP(124)      ; |154| 

           LDW     .D2T1   *+SP(44),A5       ; |155| 
||         CALL    .S2     B6                ; |155| 

           LDW     .D2T1   *+SP(48),A4       ; |155| 
||         MV      .L2X    A4,B11            ; |154| 
||         MV      .L1X    B11,A11           ; |153| 

           MV      .D2     B12,B4            ; |155| 
           MV      .L2     B10,B5            ; |155| 
           ADDKPC  .S2     RL164,B3,1        ; |155| 
RL164:     ; CALL OCCURS {__mpyd}            ; |155| 
           MVKL    .S2     __mpyd,B6         ; |155| 
           MVKH    .S2     __mpyd,B6         ; |155| 

           LDW     .D2T1   *+SP(36),A5       ; |155| 
||         MV      .L1     A5,A14            ; |155| 
||         CALL    .S2     B6                ; |155| 

           LDW     .D2T1   *+SP(40),A4       ; |155| 
||         MV      .L1     A4,A15            ; |155| 

           MV      .L2X    A11,B4            ; |155| 
           MV      .L2X    A10,B5            ; |155| 
           ADDKPC  .S2     RL165,B3,1        ; |155| 
RL165:     ; CALL OCCURS {__mpyd}            ; |155| 
           MVKL    .S2     __subd,B6         ; |155| 
           MVKH    .S2     __subd,B6         ; |155| 
           CALL    .S2     B6                ; |155| 
           MV      .L2X    A4,B4             ; |155| 
           MV      .L2X    A5,B5             ; |155| 
           MV      .S1     A15,A4            ; |155| 
           ADDKPC  .S2     RL166,B3,0        ; |155| 
           MV      .L1     A14,A5            ; |155| 
RL166:     ; CALL OCCURS {__subd}            ; |155| 
           NOP             1
DW$L$_Geopotential_Gravitation$12$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$13$B:

           LDW     .D2T1   *+SP(144),A3      ; |155| 
||         MV      .L2X    A5,B10            ; |155| 
||         ADD     .S2     1,B13,B13         ; |144| 

           NOP             1
           LDW     .D2T2   *+SP(120),B6
           NOP             1
           MV      .L2X    A4,B12            ; |155| 

           SUB     .S1     A3,1,A3           ; |144| 
||         SUB     .L1     A3,1,A0           ; |144| 

   [ A0]   MVKL    .S1     __mpyd,A3         ; |147| 
||         STW     .D2T1   A3,*+SP(144)      ; |144| 
|| [ A0]   B       .S2     L9                ; |144| 

   [ A0]   LDDW    .D2T2   *B6++,B5:B4       ; |147| 
|| [!A0]   MVKL    .S1     __mpyd,A3         ; |159| 

   [!A0]   LDW     .D2T1   *+SP(104),A4      ; |159| 
|| [!A0]   MVKH    .S1     __mpyd,A3         ; |159| 

   [!A0]   LDW     .D2T1   *+SP(100),A5      ; |159| 
|| [ A0]   MVKH    .S1     __mpyd,A3         ; |147| 

   [ A0]   LDW     .D2T1   *+SP(116),A8      ; |147| 
	.dwpsn	"geopotential.c",156,0

   [!A0]   CALL    .S2X    A3                ; |159| 
|| [!A0]   LDW     .D2T2   *+SP(112),B4      ; |159| 

           ; BRANCHCC OCCURS {L9}            ; |144| 
DW$L$_Geopotential_Gravitation$13$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$14$B:
           LDW     .D2T2   *+SP(108),B5      ; |159| 
           ADDKPC  .S2     RL167,B3,3        ; |159| 
RL167:     ; CALL OCCURS {__mpyd}            ; |159| 
DW$L$_Geopotential_Gravitation$14$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$15$B:
           MVKL    .S1     __fltid,A3        ; |160| 
           MVKH    .S1     __fltid,A3        ; |160| 
           STW     .D2T1   A4,*+SP(104)      ; |159| 
           CALL    .S2X    A3                ; |160| 
           LDW     .D2T1   *+SP(148),A4      ; |160| 
           ADDKPC  .S2     RL174,B3,0        ; |160| 
           STW     .D2T1   A5,*+SP(100)      ; |159| 
           NOP             2
RL174:     ; CALL OCCURS {__fltid}           ; |160| 
           MVKL    .S2     __mpyd,B6         ; |160| 
           MVKH    .S2     __mpyd,B6         ; |160| 

           LDW     .D2T2   *+SP(128),B5      ; |160| 
||         CALL    .S2     B6                ; |160| 

           LDW     .D2T2   *+SP(156),B4      ; |160| 
           ADDKPC  .S2     RL175,B3,3        ; |160| 
RL175:     ; CALL OCCURS {__mpyd}            ; |160| 
           MVKL    .S1     __mpyd,A3         ; |160| 
           MVKH    .S1     __mpyd,A3         ; |160| 
           LDW     .D2T2   *+SP(100),B5      ; |160| 
           CALL    .S2X    A3                ; |160| 
           LDW     .D2T2   *+SP(104),B4      ; |160| 
           ADDKPC  .S2     RL176,B3,3        ; |160| 
RL176:     ; CALL OCCURS {__mpyd}            ; |160| 
           MVKL    .S1     __addd,A3         ; |160| 
           MVKH    .S1     __addd,A3         ; |160| 
           MV      .L2X    A5,B5             ; |160| 

           LDW     .D2T1   *+SP(52),A5       ; |160| 
||         CALL    .S2X    A3                ; |160| 

           LDW     .D2T1   *+SP(56),A4       ; |160| 
||         MV      .L2X    A4,B4             ; |160| 

           ADDKPC  .S2     RL177,B3,3        ; |160| 
RL177:     ; CALL OCCURS {__addd}            ; |160| 
           STW     .D2T1   A5,*+SP(52)       ; |160| 
           STW     .D2T1   A4,*+SP(56)       ; |160| 

           MVKL    .S2     __mpyd,B6         ; |161| 
||         LDW     .D2T2   *+SP(100),B5      ; |161| 

           MVKH    .S2     __mpyd,B6         ; |161| 
||         LDW     .D2T2   *+SP(104),B4      ; |161| 

           LDW     .D2T1   *+SP(140),A5      ; |161| 
||         CALL    .S2     B6                ; |161| 

           LDW     .D2T1   *+SP(136),A4      ; |161| 
           ADDKPC  .S2     RL179,B3,3        ; |161| 
RL179:     ; CALL OCCURS {__mpyd}            ; |161| 
           MVKL    .S2     __addd,B6         ; |161| 
           MVKH    .S2     __addd,B6         ; |161| 

           LDW     .D2T1   *+SP(64),A5       ; |161| 
||         MV      .L2X    A5,B5             ; |161| 
||         CALL    .S2     B6                ; |161| 

           LDW     .D2T1   *+SP(60),A4       ; |161| 
||         MV      .L2X    A4,B4             ; |161| 

           ADDKPC  .S2     RL180,B3,3        ; |161| 
RL180:     ; CALL OCCURS {__addd}            ; |161| 
           STW     .D2T1   A5,*+SP(64)       ; |161| 
           STW     .D2T1   A4,*+SP(60)       ; |161| 

           MVKL    .S2     __mpyd,B6         ; |162| 
||         LDW     .D2T2   *+SP(100),B5      ; |162| 

           MVKH    .S2     __mpyd,B6         ; |162| 
||         LDW     .D2T2   *+SP(104),B4      ; |162| 

           LDW     .D2T1   *+SP(152),A5      ; |162| 
||         CALL    .S2     B6                ; |162| 

           LDW     .D2T1   *+SP(160),A4      ; |162| 
           ADDKPC  .S2     RL182,B3,3        ; |162| 
RL182:     ; CALL OCCURS {__mpyd}            ; |162| 
           MVKL    .S2     __addd,B6         ; |162| 
           MVKH    .S2     __addd,B6         ; |162| 

           LDW     .D2T1   *+SP(68),A5       ; |162| 
||         MV      .L2X    A5,B5             ; |162| 
||         CALL    .S2     B6                ; |162| 

           LDW     .D2T1   *+SP(72),A4       ; |162| 
||         MV      .L2X    A4,B4             ; |162| 

           ADDKPC  .S2     RL183,B3,3        ; |162| 
RL183:     ; CALL OCCURS {__addd}            ; |162| 
DW$L$_Geopotential_Gravitation$15$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Gravitation$16$B:

           LDW     .D2T2   *+SP(80),B5       ; |162| 
||         ADDK    .S1     152,A12           ; |139| 

           LDW     .D2T2   *+SP(84),B4       ; |162| 
           NOP             2
           STW     .D2T1   A4,*+SP(72)       ; |162| 
           STW     .D2T1   A5,*+SP(68)       ; |162| 

           ADD     .L2     1,B4,B5           ; |139| 
||         SUB     .S2     B5,1,B4           ; |139| 

           STW     .D2T2   B4,*+SP(80)       ; |139| 

           LDW     .D2T2   *+SP(88),B4
||         MV      .L1X    B4,A0             ; |162| 

   [ A0]   LDW     .D2T2   *+SP(76),B31
|| [ A0]   B       .S1     L8                ; |139| 

   [ A0]   LDW     .D2T1   *+SP(92),A3
           STW     .D2T2   B5,*+SP(84)       ; |139| 
   [ A0]   LDW     .D2T1   *+SP(84),A29
           ADD     .L2X    A12,B4,B4
	.dwpsn	"geopotential.c",163,0
   [ A0]   STW     .D2T2   B4,*+SP(120)
           ; BRANCHCC OCCURS {L8}            ; |139| 
DW$L$_Geopotential_Gravitation$16$E:
;** --------------------------------------------------------------------------*
L10:    
           MVKL    .S1     __mpyd,A3         ; |165| 
           MVKH    .S1     __mpyd,A3         ; |165| 
           LDW     .D2T1   *+SP(20),A10      ; |165| 

           CALL    .S2X    A3                ; |165| 
||         LDW     .D2T1   *+SP(24),A11      ; |165| 

           MVKL    .S2     0xc2f6a866,B5     ; |165| 
           MVKL    .S2     0x5bda5400,B4     ; |165| 
           MVKH    .S2     0xc2f6a866,B5     ; |165| 
           MVKH    .S2     0x5bda5400,B4     ; |165| 

           ADDKPC  .S2     RL187,B3,0        ; |165| 
||         MV      .L1     A10,A5            ; |165| 
||         MV      .S1     A11,A4            ; |165| 

RL187:     ; CALL OCCURS {__mpyd}            ; |165| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |165| 
           MVKH    .S1     __mpyd,A3         ; |165| 
           MV      .L2X    A11,B4            ; |165| 
           CALL    .S2X    A3                ; |165| 
           ADDKPC  .S2     RL188,B3,3        ; |165| 
           MV      .L2X    A10,B5            ; |165| 
RL188:     ; CALL OCCURS {__mpyd}            ; |165| 
           MVKL    .S1     __mpyd,A3         ; |165| 
           MVKH    .S1     __mpyd,A3         ; |165| 
           MV      .L2X    A5,B5             ; |165| 

           LDW     .D2T1   *+SP(52),A5       ; |165| 
||         CALL    .S2X    A3                ; |165| 

           LDW     .D2T1   *+SP(56),A4       ; |165| 
||         MV      .L2X    A4,B4             ; |165| 

           ADDKPC  .S2     RL189,B3,3        ; |165| 
RL189:     ; CALL OCCURS {__mpyd}            ; |165| 
           MVKL    .S1     __mpyd,A3         ; |166| 
           MVKH    .S1     __mpyd,A3         ; |166| 
           MVKL    .S2     0x42f6a866,B5     ; |166| 
           CALL    .S2X    A3                ; |166| 
           MVKL    .S2     0x5bda5400,B4     ; |166| 
           MVKH    .S2     0x42f6a866,B5     ; |166| 
           STW     .D2T1   A4,*+SP(56)       ; |165| 
           MVKH    .S2     0x5bda5400,B4     ; |166| 

           MV      .L1     A10,A5            ; |166| 
||         STW     .D2T1   A5,*+SP(52)       ; |165| 
||         MV      .S1     A11,A4            ; |166| 
||         ADDKPC  .S2     RL190,B3,0        ; |166| 

RL190:     ; CALL OCCURS {__mpyd}            ; |166| 
           MVKL    .S1     __mpyd,A3         ; |166| 
           MVKH    .S1     __mpyd,A3         ; |166| 
           MV      .L2X    A5,B5             ; |166| 

           LDW     .D2T1   *+SP(64),A5       ; |166| 
||         MV      .L1     A5,A12            ; |166| 
||         CALL    .S2X    A3                ; |166| 

           LDW     .D2T1   *+SP(60),A4       ; |166| 
||         MV      .L1     A4,A13            ; |166| 
||         MV      .L2X    A4,B4             ; |166| 

           ADDKPC  .S2     RL191,B3,3        ; |166| 
RL191:     ; CALL OCCURS {__mpyd}            ; |166| 
           MVKL    .S1     __mpyd,A3         ; |167| 
           MVKH    .S1     __mpyd,A3         ; |167| 
           MV      .L1     A5,A10            ; |166| 

           CALL    .S2X    A3                ; |167| 
||         LDW     .D2T1   *+SP(68),A5       ; |167| 

           LDW     .D2T1   *+SP(72),A4       ; |167| 
||         MV      .L1     A4,A11            ; |166| 

           MV      .L2X    A13,B4            ; |167| 
           MV      .L2X    A12,B5            ; |167| 
           ADDKPC  .S2     RL192,B3,1        ; |167| 
RL192:     ; CALL OCCURS {__mpyd}            ; |167| 
           MVKL    .S1     __mpyd,A3         ; |172| 
           MVKH    .S1     __mpyd,A3         ; |172| 
           LDW     .D2T1   *+SP(32),A12      ; |172| 

           LDW     .D2T1   *+SP(28),A13      ; |172| 
||         CALL    .S2X    A3                ; |172| 

           ADDKPC  .S2     RL194,B3,2        ; |172| 
           MV      .L2X    A12,B4            ; |172| 
           MV      .L2X    A13,B5            ; |172| 
RL194:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S1     __mpyd,A3         ; |172| 
           MVKH    .S1     __mpyd,A3         ; |172| 
           MV      .L2X    A12,B4            ; |172| 
           CALL    .S2X    A3                ; |172| 
           MV      .L2X    A13,B5            ; |172| 
           ADDKPC  .S2     RL195,B3,3        ; |172| 
RL195:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S2     __mpyd,B6         ; |172| 
           MVKH    .S2     __mpyd,B6         ; |172| 

           LDW     .D2T2   *+SP(20),B5       ; |172| 
||         CALL    .S2     B6                ; |172| 

           LDW     .D2T2   *+SP(24),B4       ; |172| 
           MV      .L1     A4,A15            ; |172| 
           MV      .D1     A5,A14            ; |172| 
           MV      .S1     A11,A4            ; |172| 

           ADDKPC  .S2     RL199,B3,0        ; |172| 
||         MV      .L1     A10,A5            ; |172| 

RL199:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S2     __mpyd,B6         ; |172| 
           MVKH    .S2     __mpyd,B6         ; |172| 

           LDW     .D2T2   *+SP(20),B5       ; |172| 
||         CALL    .S2     B6                ; |172| 

           LDW     .D2T2   *+SP(24),B4       ; |172| 
           ADDKPC  .S2     RL200,B3,3        ; |172| 
RL200:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S2     __mpyd,B6         ; |172| 
           MVKH    .S2     __mpyd,B6         ; |172| 
           CALL    .S2     B6                ; |172| 
           MV      .L2X    A12,B4            ; |172| 
           MV      .L2X    A13,B5            ; |172| 
           ADDKPC  .S2     RL201,B3,2        ; |172| 
RL201:     ; CALL OCCURS {__mpyd}            ; |172| 
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *+SP(4),A3        ; |172| 
           LDW     .D2T2   *+SP(52),B5       ; |172| 
           LDW     .D2T2   *+SP(56),B4       ; |172| 
           MV      .L1     A4,A13            ; |172| 
           MV      .D1     A5,A12            ; |172| 

           MVKL    .S1     __mpyd,A3         ; |172| 
||         LDDW    .D1T1   *A3,A11:A10       ; |172| 

           MVKH    .S1     __mpyd,A3         ; |172| 
           NOP             1
           CALL    .S2X    A3                ; |172| 
           ADDKPC  .S2     RL214,B3,2        ; |172| 
           MV      .S1     A10,A4            ; |172| 
           MV      .L1     A11,A5            ; |172| 
RL214:     ; CALL OCCURS {__mpyd}            ; |172| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |172| 
           MVKH    .S1     __mpyd,A3         ; |172| 
           LDW     .D2T2   *+SP(20),B5       ; |172| 
           CALL    .S2X    A3                ; |172| 
           LDW     .D2T2   *+SP(24),B4       ; |172| 
           ADDKPC  .S2     RL215,B3,3        ; |172| 
RL215:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S1     __mpyd,A3         ; |172| 
           MVKH    .S1     __mpyd,A3         ; |172| 
           MV      .L2X    A4,B11            ; |172| 
           CALL    .S2X    A3                ; |172| 
           MV      .L2X    A12,B5            ; |172| 
           MV      .L2X    A13,B4            ; |172| 
           ADDKPC  .S2     RL216,B3,0        ; |172| 
           MV      .S1     A10,A4            ; |172| 

           MV      .L2X    A5,B10            ; |172| 
||         MV      .L1     A11,A5            ; |172| 

RL216:     ; CALL OCCURS {__mpyd}            ; |172| 
           LDW     .D2T1   *+SP(4),A3        ; |172| 
           NOP             4

           MVKL    .S1     __mpyd,A3         ; |172| 
||         LDDW    .D1T1   *+A3(16),A7:A6    ; |172| 

           MVKH    .S1     __mpyd,A3         ; |172| 
           NOP             1
           CALL    .S2X    A3                ; |172| 
           ADDKPC  .S2     RL217,B3,2        ; |172| 
           MV      .L2X    A7,B5             ; |172| 
           MV      .L2X    A6,B4             ; |172| 
RL217:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S1     __subd,A3         ; |172| 
           MVKH    .S1     __subd,A3         ; |172| 
           MV      .L2X    A4,B4             ; |172| 
           CALL    .S2X    A3                ; |172| 
           MV      .L1X    B11,A4            ; |172| 
           MV      .L2X    A5,B5             ; |172| 
           ADDKPC  .S2     RL218,B3,1        ; |172| 
           MV      .L1X    B10,A5            ; |172| 
RL218:     ; CALL OCCURS {__subd}            ; |172| 
           LDW     .D2T1   *+SP(4),A3        ; |172| 
           MV      .L2X    A15,B4            ; |172| 
           MV      .L1     A4,A11            ; |172| 
           MV      .D1     A5,A10            ; |172| 
           MV      .L2X    A14,B5            ; |172| 

           MVKL    .S1     __mpyd,A3         ; |172| 
||         LDDW    .D1T1   *+A3(8),A7:A6     ; |172| 

           MVKH    .S1     __mpyd,A3         ; |172| 
           NOP             1
           CALL    .S2X    A3                ; |172| 
           ADDKPC  .S2     RL219,B3,2        ; |172| 
           MV      .S1     A6,A4             ; |172| 
           MV      .L1     A7,A5             ; |172| 
RL219:     ; CALL OCCURS {__mpyd}            ; |172| 
           MVKL    .S1     __subd,A3         ; |172| 
           MVKH    .S1     __subd,A3         ; |172| 
           MV      .L2X    A4,B4             ; |172| 
           CALL    .S2X    A3                ; |172| 
           ADDKPC  .S2     RL220,B3,1        ; |172| 
           MV      .S1     A11,A4            ; |172| 
           MV      .L2X    A5,B5             ; |172| 
           MV      .L1     A10,A5            ; |172| 
RL220:     ; CALL OCCURS {__subd}            ; |172| 
           LDW     .D2T1   *+SP(8),A3        ; |172| 
           MVKL    .S2     __mpyd,B6         ; |173| 
           MVKH    .S2     __mpyd,B6         ; |173| 
           LDW     .D2T2   *+SP(52),B5       ; |173| 
           LDW     .D2T2   *+SP(56),B4       ; |173| 
           STDW    .D1T1   A5:A4,*A3         ; |172| 
           LDW     .D2T1   *+SP(4),A3        ; |172| 
           NOP             4

           LDDW    .D1T1   *+A3(8),A11:A10   ; |173| 
||         CALL    .S2     B6                ; |173| 

           ADDKPC  .S2     RL233,B3,3        ; |173| 

           MV      .S1     A10,A4            ; |173| 
||         MV      .L1     A11,A5            ; |173| 

RL233:     ; CALL OCCURS {__mpyd}            ; |173| 
           MVKL    .S2     __mpyd,B6         ; |173| 
           MVKH    .S2     __mpyd,B6         ; |173| 

           CALL    .S2     B6                ; |173| 
||         LDW     .D2T2   *+SP(20),B5       ; |173| 

           LDW     .D2T2   *+SP(24),B4       ; |173| 
           ADDKPC  .S2     RL234,B3,3        ; |173| 
RL234:     ; CALL OCCURS {__mpyd}            ; |173| 
           MVKL    .S2     __mpyd,B6         ; |173| 
           MVKH    .S2     __mpyd,B6         ; |173| 
           CALL    .S2     B6                ; |173| 
           MV      .L2X    A4,B11            ; |173| 
           MV      .L2X    A12,B5            ; |173| 
           MV      .L2X    A13,B4            ; |173| 
           ADDKPC  .S2     RL235,B3,0        ; |173| 

           MV      .S1     A10,A4            ; |173| 
||         MV      .L2X    A5,B10            ; |173| 
||         MV      .L1     A11,A5            ; |173| 

RL235:     ; CALL OCCURS {__mpyd}            ; |173| 
           LDW     .D2T1   *+SP(4),A3        ; |173| 
           NOP             1
           MVKL    .S2     __mpyd,B6         ; |173| 
           MVKH    .S2     __mpyd,B6         ; |173| 
           CALL    .S2     B6                ; |173| 
           LDDW    .D1T2   *+A3(16),B5:B4    ; |173| 
           ADDKPC  .S2     RL236,B3,3        ; |173| 
RL236:     ; CALL OCCURS {__mpyd}            ; |173| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |173| 
           MVKH    .S1     __subd,A3         ; |173| 
           MV      .L2X    A4,B4             ; |173| 
           CALL    .S2X    A3                ; |173| 
           MV      .L1X    B11,A4            ; |173| 
           ADDKPC  .S2     RL237,B3,1        ; |173| 
           MV      .L2X    A5,B5             ; |173| 
           MV      .L1X    B10,A5            ; |173| 
RL237:     ; CALL OCCURS {__subd}            ; |173| 
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *+SP(4),A3        ; |173| 
           MV      .L2X    A15,B4            ; |173| 
           MV      .L1     A4,A11            ; |173| 
           MV      .L2X    A14,B5            ; |173| 
           MV      .D1     A5,A10            ; |173| 

           MVKL    .S1     __mpyd,A3         ; |173| 
||         LDDW    .D1T1   *A3,A7:A6         ; |173| 

           MVKH    .S1     __mpyd,A3         ; |173| 
           NOP             1
           CALL    .S2X    A3                ; |173| 
           ADDKPC  .S2     RL238,B3,2        ; |173| 
           MV      .S1     A6,A4             ; |173| 
           MV      .L1     A7,A5             ; |173| 
RL238:     ; CALL OCCURS {__mpyd}            ; |173| 
           MVKL    .S1     __addd,A3         ; |173| 
           MVKH    .S1     __addd,A3         ; |173| 
           MV      .L2X    A4,B4             ; |173| 
           CALL    .S2X    A3                ; |173| 
           ADDKPC  .S2     RL239,B3,1        ; |173| 
           MV      .S1     A11,A4            ; |173| 
           MV      .L2X    A5,B5             ; |173| 
           MV      .L1     A10,A5            ; |173| 
RL239:     ; CALL OCCURS {__addd}            ; |173| 
           LDW     .D2T1   *+SP(8),A10       ; |173| 
           LDW     .D2T1   *+SP(4),A3        ; |173| 
           NOP             1
           LDW     .D2T2   *+SP(52),B5       ; |174| 
           LDW     .D2T2   *+SP(56),B4       ; |174| 
           STDW    .D1T1   A5:A4,*+A10(8)    ; |173| 

           MVKL    .S1     __mpyd,A3         ; |174| 
||         LDDW    .D1T1   *+A3(16),A5:A4    ; |174| 

           MVKH    .S1     __mpyd,A3         ; |174| 
           NOP             1
           CALL    .S2X    A3                ; |174| 
           ADDKPC  .S2     RL246,B3,4        ; |174| 
RL246:     ; CALL OCCURS {__mpyd}            ; |174| 
           MVKL    .S1     __mpyd,A3         ; |174| 
           MVKH    .S1     __mpyd,A3         ; |174| 
           LDW     .D2T2   *+SP(20),B5       ; |174| 
           CALL    .S2X    A3                ; |174| 
           LDW     .D2T2   *+SP(24),B4       ; |174| 
           ADDKPC  .S2     RL247,B3,3        ; |174| 
RL247:     ; CALL OCCURS {__mpyd}            ; |174| 
           MVKL    .S2     __mpyd,B6         ; |174| 

           MVKH    .S2     __mpyd,B6         ; |174| 
||         LDW     .D2T1   *+SP(16),A14      ; |174| 

           LDW     .D2T1   *+SP(12),A11      ; |174| 
||         CALL    .S2     B6                ; |174| 

           MV      .L2X    A4,B10            ; |174| 
           MV      .D1     A5,A15            ; |174| 
           MV      .S1     A13,A4            ; |174| 
           MV      .L2X    A14,B4            ; |174| 

           MV      .L2X    A11,B5            ; |174| 
||         ADDKPC  .S2     RL248,B3,0        ; |174| 
||         MV      .L1     A12,A5            ; |174| 

RL248:     ; CALL OCCURS {__mpyd}            ; |174| 
           MVKL    .S2     __mpyd,B6         ; |174| 
           MVKH    .S2     __mpyd,B6         ; |174| 
           CALL    .S2     B6                ; |174| 
           MV      .L2X    A14,B4            ; |174| 
           MV      .L2X    A11,B5            ; |174| 
           ADDKPC  .S2     RL249,B3,2        ; |174| 
RL249:     ; CALL OCCURS {__mpyd}            ; |174| 
           MVKL    .S2     __addd,B6         ; |174| 
           MVKH    .S2     __addd,B6         ; |174| 
           CALL    .S2     B6                ; |174| 
           MV      .L2X    A5,B5             ; |174| 
           MV      .L2X    A4,B4             ; |174| 
           MV      .S1     A15,A5            ; |174| 
           ADDKPC  .S2     RL250,B3,0        ; |174| 
           MV      .L1X    B10,A4            ; |174| 
RL250:     ; CALL OCCURS {__addd}            ; |174| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(204),B3      ; |176| 
           LDDW    .D2T1   *+SP(192),A13:A12 ; |176| 
           LDW     .D2T1   *+SP(200),A14     ; |176| 
           LDDW    .D2T2   *+SP(208),B11:B10 ; |176| 

           LDDW    .D2T2   *+SP(216),B13:B12 ; |176| 
||         STDW    .D1T1   A5:A4,*+A10(16)   ; |174| 

           RET     .S2     B3                ; |176| 
||         LDW     .D2T1   *+SP(224),A15     ; |176| 

           LDDW    .D2T1   *+SP(184),A11:A10 ; |176| 
           NOP             3
	.dwpsn	"geopotential.c",176,1
           ADDK    .S2     224,SP            ; |176| 
           ; BRANCH OCCURS {B3}              ; |176| 

DW$41	.dwtag  DW_TAG_loop
	.dwattr DW$41, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L8:1:1472539734")
	.dwattr DW$41, DW_AT_begin_file("geopotential.c")
	.dwattr DW$41, DW_AT_begin_line(0x8b)
	.dwattr DW$41, DW_AT_end_line(0xa3)
DW$42	.dwtag  DW_TAG_loop_range
	.dwattr DW$42, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$8$B)
	.dwattr DW$42, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$8$E)
DW$43	.dwtag  DW_TAG_loop_range
	.dwattr DW$43, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$14$B)
	.dwattr DW$43, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$14$E)
DW$44	.dwtag  DW_TAG_loop_range
	.dwattr DW$44, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$15$B)
	.dwattr DW$44, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$15$E)
DW$45	.dwtag  DW_TAG_loop_range
	.dwattr DW$45, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$16$B)
	.dwattr DW$45, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$16$E)

DW$46	.dwtag  DW_TAG_loop
	.dwattr DW$46, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L9:2:1472539734")
	.dwattr DW$46, DW_AT_begin_file("geopotential.c")
	.dwattr DW$46, DW_AT_begin_line(0x90)
	.dwattr DW$46, DW_AT_end_line(0x9c)
DW$47	.dwtag  DW_TAG_loop_range
	.dwattr DW$47, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$9$B)
	.dwattr DW$47, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$9$E)
DW$48	.dwtag  DW_TAG_loop_range
	.dwattr DW$48, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$10$B)
	.dwattr DW$48, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$10$E)
DW$49	.dwtag  DW_TAG_loop_range
	.dwattr DW$49, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$11$B)
	.dwattr DW$49, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$11$E)
DW$50	.dwtag  DW_TAG_loop_range
	.dwattr DW$50, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$12$B)
	.dwattr DW$50, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$12$E)
DW$51	.dwtag  DW_TAG_loop_range
	.dwattr DW$51, DW_AT_low_pc(DW$L$_Geopotential_Gravitation$13$B)
	.dwattr DW$51, DW_AT_high_pc(DW$L$_Geopotential_Gravitation$13$E)
	.dwendtag DW$46

	.dwendtag DW$41

	.dwattr DW$36, DW_AT_end_file("geopotential.c")
	.dwattr DW$36, DW_AT_end_line(0xb0)
	.dwattr DW$36, DW_AT_end_column(0x01)
	.dwendtag DW$36

	.sect	".text"
	.global	_Geopotential_Init

DW$52	.dwtag  DW_TAG_subprogram, DW_AT_name("Geopotential_Init"), DW_AT_symbol_name("_Geopotential_Init")
	.dwattr DW$52, DW_AT_low_pc(_Geopotential_Init)
	.dwattr DW$52, DW_AT_high_pc(0x00)
	.dwattr DW$52, DW_AT_begin_file("geopotential.c")
	.dwattr DW$52, DW_AT_begin_line(0xba)
	.dwattr DW$52, DW_AT_begin_column(0x06)
	.dwattr DW$52, DW_AT_frame_base[DW_OP_breg31 64]
	.dwattr DW$52, DW_AT_skeletal(0x01)
	.dwpsn	"geopotential.c",187,1

;******************************************************************************
;* FUNCTION NAME: _Geopotential_Init                                          *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,   *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Local Frame Size  : 0 Args + 20 Auto + 44 Save = 64 byte                 *
;******************************************************************************
_Geopotential_Init:
;** --------------------------------------------------------------------------*
DW$53	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pGP"), DW_AT_symbol_name("_pGP")
	.dwattr DW$53, DW_AT_type(*DW$T$29)
	.dwattr DW$53, DW_AT_location[DW_OP_reg4]
           MVKL    .S1     _memset,A3        ; |193| 
           MVKH    .S1     _memset,A3        ; |193| 
           MV      .L1X    SP,A31            ; |187| 

           CALL    .S2X    A3                ; |193| 
||         STW     .D2T1   A15,*SP--(64)     ; |187| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(44)

           STW     .D1T1   A14,*-A31(24)
||         STDW    .D2T2   B11:B10,*+SP(48)

           STDW    .D1T1   A13:A12,*-A31(32)
||         STDW    .D2T2   B13:B12,*+SP(56)
||         ADD     .L1     8,A4,A5

           STW     .D2T1   A5,*+SP(8)        ; |193| 
||         MV      .L1     A4,A10            ; |187| 

           ADDKPC  .S2     RL252,B3,0        ; |193| 
||         STW     .D2T1   A10,*+SP(4)       ; |187| 
||         MVK     .S1     0x3870,A6         ; |193| 
||         ZERO    .L2     B4                ; |193| 

RL252:     ; CALL OCCURS {_memset}           ; |193| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fltid,A3        ; |202| 
           MVKH    .S1     __fltid,A3        ; |202| 
           MVK     .S2     18,B4             ; |196| 
           CALL    .S2X    A3                ; |202| 
           STW     .D1T2   B4,*A10           ; |196| 
           MVK     .L2     0x8,B11
	.dwpsn	"geopotential.c",199,0

           ZERO    .L1     A11               ; |199| 
||         MVK     .S2     0x1,B10

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L11
;** --------------------------------------------------------------------------*
L11:    
DW$L$_Geopotential_Init$3$B:
	.dwpsn	"geopotential.c",200,0
           ADDKPC  .S2     RL253,B3,0        ; |202| 
           MV      .L1X    B10,A4            ; |202| 
RL253:     ; CALL OCCURS {__fltid}           ; |202| 
DW$L$_Geopotential_Init$3$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Init$4$B:
           MVKL    .S2     _sqrt,B4          ; |202| 
           MVKH    .S2     _sqrt,B4          ; |202| 
           CALL    .S2     B4                ; |202| 
           ADDKPC  .S2     RL254,B3,2        ; |202| 
           MV      .L1     A5,A10            ; |202| 
           MV      .S1     A4,A12            ; |202| 
RL254:     ; CALL OCCURS {_sqrt}             ; |202| 
           MVKL    .S2     __addd,B6         ; |204| 
           MVKH    .S2     __addd,B6         ; |204| 

           LDW     .D2T1   *+SP(8),A3        ; |204| 
||         CALL    .S2     B6                ; |204| 

           MV      .L2X    A12,B4            ; |204| 
           ADDKPC  .S2     RL255,B3,1        ; |204| 
           MV      .L2X    A10,B5            ; |204| 

           MV      .L1     A10,A5            ; |204| 
||         MV      .S1     A12,A4            ; |204| 
||         STDW    .D1T1   A5:A4,*A3         ; |202| 

RL255:     ; CALL OCCURS {__addd}            ; |204| 
DW$L$_Geopotential_Init$4$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Init$5$B:

           CMPLT   .L2     B10,2,B0          ; |205| 
||         LDW     .D2T1   *+SP(4),A3

   [ B0]   BNOP    .S1     L13,2             ; |205| 
||         STW     .D2T1   A5,*+SP(12)       ; |204| 
|| [!B0]   MVKL    .S2     __fltid,B4        ; |207| 
|| [!B0]   MVK     .L1     0x1,A10           ; |205| 

           STW     .D2T1   A4,*+SP(16)       ; |204| 
|| [!B0]   MVKH    .S2     __fltid,B4        ; |207| 

           ADD     .L1X    B11,A3,A3

           ADD     .L1     8,A3,A15
||         SUB     .S1     A11,A10,A3        ; |207| 

           ; BRANCHCC OCCURS {L13}           ; |205| 
DW$L$_Geopotential_Init$5$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Init$6$B:

           ADD     .L1     A10,A11,A4        ; |207| 
||         ADD     .S1     1,A3,A3           ; |207| 
||         ZERO    .D1     A12               ; |204| 
||         STW     .D2T1   A11,*+SP(20)      ; |207| 

           MPYLH   .M1     A4,A3,A6          ; |207| 
||         CALL    .S2     B4                ; |207| 
||         MVKH    .S1     0x3ff00000,A12    ; |204| 
||         ZERO    .L1     A14               ; |204| 
||         MVK     .D1     0xffffffff,A13    ; |204| 

           MPYLH   .M1     A3,A4,A5          ; |207| 
           MPYU    .M1     A4,A3,A4          ; |207| 
	.dwpsn	"geopotential.c",205,0
           ADD     .L1     A6,A5,A3          ; |207| 
DW$L$_Geopotential_Init$6$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L12:    
DW$L$_Geopotential_Init$7$B:
	.dwpsn	"geopotential.c",206,0
           SHL     .S1     A3,16,A3          ; |207| 

           ADD     .L1     A4,A3,A4          ; |207| 
||         ADDKPC  .S2     RL257,B3,0        ; |207| 

RL257:     ; CALL OCCURS {__fltid}           ; |207| 
DW$L$_Geopotential_Init$7$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Init$8$B:
           MVKL    .S2     __divd,B6         ; |207| 
           MVKH    .S2     __divd,B6         ; |207| 
           CALL    .S2     B6                ; |207| 
           MV      .L2X    A4,B4             ; |207| 
           ADDKPC  .S2     RL258,B3,0        ; |207| 
           MV      .S1     A14,A4            ; |207| 
           MV      .L2X    A5,B5             ; |207| 
           MV      .L1     A12,A5            ; |207| 
RL258:     ; CALL OCCURS {__divd}            ; |207| 
           MVKL    .S1     __fltid,A3        ; |208| 
           MVKH    .S1     __fltid,A3        ; |208| 
           MV      .S1     A5,A12            ; |207| 
           CALL    .S2X    A3                ; |208| 
           ADDKPC  .S2     RL263,B3,2        ; |208| 
           MV      .D1     A4,A14            ; |207| 
           MV      .L1     A13,A4            ; |208| 
RL263:     ; CALL OCCURS {__fltid}           ; |208| 
           MVKL    .S2     __mpyd,B6         ; |208| 
           MVKH    .S2     __mpyd,B6         ; |208| 

           LDW     .D2T1   *+SP(12),A5       ; |208| 
||         MV      .L2X    A5,B12            ; |208| 
||         CALL    .S2     B6                ; |208| 

           LDW     .D2T1   *+SP(16),A4       ; |208| 
||         MV      .L2X    A4,B13            ; |208| 

           MV      .L2X    A14,B4            ; |208| 
           MV      .L2X    A12,B5            ; |208| 
           ADDKPC  .S2     RL264,B3,1        ; |208| 
RL264:     ; CALL OCCURS {__mpyd}            ; |208| 
           MVKL    .S1     _sqrt,A3          ; |208| 
           MVKH    .S1     _sqrt,A3          ; |208| 
           NOP             1
           CALL    .S2X    A3                ; |208| 
           ADDKPC  .S2     RL265,B3,4        ; |208| 
RL265:     ; CALL OCCURS {_sqrt}             ; |208| 
           MVKL    .S2     __mpyd,B6         ; |208| 
           MVKH    .S2     __mpyd,B6         ; |208| 
           CALL    .S2     B6                ; |208| 
           MV      .L2X    A4,B4             ; |208| 
           MV      .L1X    B13,A4            ; |208| 
           ADDKPC  .S2     RL266,B3,0        ; |208| 
           MV      .L2X    A5,B5             ; |208| 
           MV      .L1X    B12,A5            ; |208| 
RL266:     ; CALL OCCURS {__mpyd}            ; |208| 
DW$L$_Geopotential_Init$8$E:
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Init$9$B:

           LDW     .D2T2   *+SP(20),B4       ; |208| 
||         STDW    .D1T1   A5:A4,*A15++      ; |208| 
||         ADD     .L1     1,A10,A10         ; |205| 
||         NEG     .S1     A13,A13           ; |209| 

           NOP             4

           SUB     .L2     B4,1,B4           ; |205| 
||         SUB     .L1X    B4,1,A0           ; |205| 

   [ A0]   ADD     .D1     A10,A11,A4        ; |207| 
|| [ A0]   SUB     .L1     A11,A10,A3        ; |207| 
||         MVKL    .S2     __fltid,B4        ; |207| 
||         STW     .D2T2   B4,*+SP(20)       ; |208| 
|| [ A0]   B       .S1     L12               ; |205| 

   [ A0]   ADD     .L1     1,A3,A3           ; |207| 
||         MVKH    .S2     __fltid,B4        ; |207| 

   [ A0]   MPYLH   .M1     A3,A4,A5          ; |207| 
|| [ A0]   CALL    .S2     B4                ; |207| 

   [ A0]   MPYLH   .M1     A4,A3,A6          ; |207| 
   [ A0]   MPYU    .M1     A4,A3,A4          ; |207| 
	.dwpsn	"geopotential.c",210,0
   [ A0]   ADD     .L1     A6,A5,A3          ; |207| 
           ; BRANCHCC OCCURS {L12}           ; |205| 
DW$L$_Geopotential_Init$9$E:
;** --------------------------------------------------------------------------*
L13:    
DW$L$_Geopotential_Init$10$B:

           LDW     .D2T1   *+SP(4),A3
||         ADD     .L1     1,A11,A11         ; |199| 
||         ADD     .L2     2,B10,B10         ; |199| 
||         ADDK    .S2     152,B11           ; |199| 

           LDW     .D2T1   *+SP(8),A4        ; |199| 
           NOP             3
           LDW     .D1T1   *A3,A3            ; |199| 
           NOP             2
           ADDK    .S1     152,A4            ; |199| 
           STW     .D2T1   A4,*+SP(8)        ; |199| 

           MVKL    .S1     __fltid,A3        ; |202| 
||         CMPLT   .L1     A3,A11,A0         ; |199| 

   [!A0]   B       .S2     L11               ; |199| 
|| [ A0]   LDW     .D2T1   *+SP(4),A3
||         MVKH    .S1     __fltid,A3        ; |202| 
|| [ A0]   MVK     .L1     0x3,A7            ; |215| 

   [ A0]   MVKL    .S1     _GP_C,A4
|| [ A0]   MVKL    .S2     _GP_S,B5

   [!A0]   CALL    .S2X    A3                ; |202| 
|| [ A0]   MVKH    .S1     _GP_C,A4

   [ A0]   MVKH    .S2     _GP_S,B5
|| [ A0]   MVK     .S1     5784,A5

   [ A0]   MVK     .S2     2896,B4
|| [ A0]   MV      .L2X    A4,B6

	.dwpsn	"geopotential.c",211,0

   [ A0]   MV      .S1X    B5,A3
|| [ A0]   ADD     .L1     A5,A3,A8
|| [ A0]   MVK     .S2     0x130,B8
|| [ A0]   ADD     .L2X    B4,A3,B9

           ; BRANCHCC OCCURS {L11}           ; |199| 
DW$L$_Geopotential_Init$10$E:
;** --------------------------------------------------------------------------*
           MVK     .S2     0x11,B0           ; |215| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L14
;** --------------------------------------------------------------------------*
L14:    
DW$L$_Geopotential_Init$12$B:

           MVC     .S2     CSR,B16
||         SUB     .L1     A7,1,A0
||         ADD     .L2     B8,B9,B7
||         ADD     .S1X    B8,A8,A6

           AND     .L2     -2,B16,B4
           MVC     .S2     B4,CSR            ; interrupts off
DW$L$_Geopotential_Init$12$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 217
;*      Loop opening brace source line   : 218
;*      Loop closing brace source line   : 222
;*      Known Minimum Trip Count         : 3                    
;*      Known Maximum Trip Count         : 19                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 12
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1        0     
;*      .D units                     2*       2*    
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             2*       2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 12 Schedule found with 1 iterations in parallel
;*      Done
;*
;*      Collapsed epilog stages     : 0
;*      Collapsed prolog stages     : 0
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L15:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L16:    ; PIPED LOOP KERNEL
DW$L$_Geopotential_Init$14$B:
	.dwpsn	"geopotential.c",218,0
           LDDW    .D2T2   *B6++,B5:B4       ; |219| <0,0>  ^ 
           NOP             4
           STDW    .D2T2   B5:B4,*B7++       ; |219| <0,5>  ^ 

   [ A0]   BDEC    .S1     L16,A0            ; |217| <0,6> 
||         LDDW    .D1T1   *A3++,A5:A4       ; |220| <0,6>  ^ 

           NOP             4
	.dwpsn	"geopotential.c",222,0
           STDW    .D1T1   A5:A4,*A6++       ; |220| <0,11>  ^ 
DW$L$_Geopotential_Init$14$E:
;** --------------------------------------------------------------------------*
L17:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
DW$L$_Geopotential_Init$16$B:

           SUB     .L2     B0,1,B0           ; |215| 
||         ADDK    .S2     152,B8            ; |215| 
||         ADD     .L1     1,A7,A7           ; |215| 

   [ B0]   B       .S1     L14               ; |215| 
||         MVC     .S2     B16,CSR           ; interrupts on
|| [!B0]   LDW     .D2T2   *+SP(44),B3       ; |226| 
|| [!B0]   MV      .L1X    SP,A31            ; |226| 

   [!B0]   LDDW    .D1T1   *+A31(24),A11:A10 ; |226| 
|| [!B0]   LDDW    .D2T2   *+SP(48),B11:B10  ; |226| 

   [!B0]   LDDW    .D1T1   *+A31(32),A13:A12 ; |226| 
|| [!B0]   LDDW    .D2T2   *+SP(56),B13:B12  ; |226| 

   [!B0]   LDW     .D1T1   *+A31(40),A14     ; |226| 
   [!B0]   LDW     .D2T1   *++SP(64),A15     ; |226| 
   [!B0]   RET     .S2     B3                ; |226| 
           ; BRANCHCC OCCURS {L14}           ; |215| 
DW$L$_Geopotential_Init$16$E:
;** --------------------------------------------------------------------------*
	.dwpsn	"geopotential.c",226,1
           NOP             5
           ; BRANCH OCCURS {B3}              ; |226| 

DW$54	.dwtag  DW_TAG_loop
	.dwattr DW$54, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L14:1:1472539734")
	.dwattr DW$54, DW_AT_begin_file("geopotential.c")
	.dwattr DW$54, DW_AT_begin_line(0xd7)
	.dwattr DW$54, DW_AT_end_line(0xe2)
DW$55	.dwtag  DW_TAG_loop_range
	.dwattr DW$55, DW_AT_low_pc(DW$L$_Geopotential_Init$12$B)
	.dwattr DW$55, DW_AT_high_pc(DW$L$_Geopotential_Init$12$E)
DW$56	.dwtag  DW_TAG_loop_range
	.dwattr DW$56, DW_AT_low_pc(DW$L$_Geopotential_Init$16$B)
	.dwattr DW$56, DW_AT_high_pc(DW$L$_Geopotential_Init$16$E)

DW$57	.dwtag  DW_TAG_loop
	.dwattr DW$57, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L16:2:1472539734")
	.dwattr DW$57, DW_AT_begin_file("geopotential.c")
	.dwattr DW$57, DW_AT_begin_line(0xd9)
	.dwattr DW$57, DW_AT_end_line(0xde)
DW$58	.dwtag  DW_TAG_loop_range
	.dwattr DW$58, DW_AT_low_pc(DW$L$_Geopotential_Init$14$B)
	.dwattr DW$58, DW_AT_high_pc(DW$L$_Geopotential_Init$14$E)
	.dwendtag DW$57

	.dwendtag DW$54


DW$59	.dwtag  DW_TAG_loop
	.dwattr DW$59, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L11:1:1472539734")
	.dwattr DW$59, DW_AT_begin_file("geopotential.c")
	.dwattr DW$59, DW_AT_begin_line(0xc7)
	.dwattr DW$59, DW_AT_end_line(0xd3)
DW$60	.dwtag  DW_TAG_loop_range
	.dwattr DW$60, DW_AT_low_pc(DW$L$_Geopotential_Init$3$B)
	.dwattr DW$60, DW_AT_high_pc(DW$L$_Geopotential_Init$3$E)
DW$61	.dwtag  DW_TAG_loop_range
	.dwattr DW$61, DW_AT_low_pc(DW$L$_Geopotential_Init$6$B)
	.dwattr DW$61, DW_AT_high_pc(DW$L$_Geopotential_Init$6$E)
DW$62	.dwtag  DW_TAG_loop_range
	.dwattr DW$62, DW_AT_low_pc(DW$L$_Geopotential_Init$4$B)
	.dwattr DW$62, DW_AT_high_pc(DW$L$_Geopotential_Init$4$E)
DW$63	.dwtag  DW_TAG_loop_range
	.dwattr DW$63, DW_AT_low_pc(DW$L$_Geopotential_Init$5$B)
	.dwattr DW$63, DW_AT_high_pc(DW$L$_Geopotential_Init$5$E)
DW$64	.dwtag  DW_TAG_loop_range
	.dwattr DW$64, DW_AT_low_pc(DW$L$_Geopotential_Init$10$B)
	.dwattr DW$64, DW_AT_high_pc(DW$L$_Geopotential_Init$10$E)

DW$65	.dwtag  DW_TAG_loop
	.dwattr DW$65, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\geopotential.asm:L12:2:1472539734")
	.dwattr DW$65, DW_AT_begin_file("geopotential.c")
	.dwattr DW$65, DW_AT_begin_line(0xcd)
	.dwattr DW$65, DW_AT_end_line(0xd2)
DW$66	.dwtag  DW_TAG_loop_range
	.dwattr DW$66, DW_AT_low_pc(DW$L$_Geopotential_Init$7$B)
	.dwattr DW$66, DW_AT_high_pc(DW$L$_Geopotential_Init$7$E)
DW$67	.dwtag  DW_TAG_loop_range
	.dwattr DW$67, DW_AT_low_pc(DW$L$_Geopotential_Init$8$B)
	.dwattr DW$67, DW_AT_high_pc(DW$L$_Geopotential_Init$8$E)
DW$68	.dwtag  DW_TAG_loop_range
	.dwattr DW$68, DW_AT_low_pc(DW$L$_Geopotential_Init$9$B)
	.dwattr DW$68, DW_AT_high_pc(DW$L$_Geopotential_Init$9$E)
	.dwendtag DW$65

	.dwendtag DW$59

	.dwattr DW$52, DW_AT_end_file("geopotential.c")
	.dwattr DW$52, DW_AT_end_line(0xe2)
	.dwattr DW$52, DW_AT_end_column(0x01)
	.dwendtag DW$52

;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_sqrt
	.global	_memset
	.global	__mpyd
	.global	__subd
	.global	__negd
	.global	__addd
	.global	__fltid
	.global	__divd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr DW$T$3, DW_AT_address_class(0x20)

DW$T$25	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$3)
	.dwattr DW$T$25, DW_AT_language(DW_LANG_C)
DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$70	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$71	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$24)
	.dwendtag DW$T$25


DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$30, DW_AT_language(DW_LANG_C)
DW$72	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$74	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
	.dwendtag DW$T$30


DW$T$33	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$33, DW_AT_language(DW_LANG_C)
DW$75	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$76	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$77	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
	.dwendtag DW$T$33


DW$T$34	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$34, DW_AT_language(DW_LANG_C)
DW$79	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
	.dwendtag DW$T$34

DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("int32"), DW_AT_type(*DW$T$10)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)
DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("size_t"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$24, DW_AT_language(DW_LANG_C)
DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)
DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$20, DW_AT_language(DW_LANG_C)
DW$T$32	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$32, DW_AT_address_class(0x20)

DW$T$41	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$40)
	.dwattr DW$T$41, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$41, DW_AT_byte_size(0x5d8)
DW$80	.dwtag  DW_TAG_subrange_type
	.dwattr DW$80, DW_AT_upper_bound(0xba)
	.dwendtag DW$T$41


DW$T$42	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$42, DW_AT_language(DW_LANG_C)
DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$T$42

DW$T$29	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$28)
	.dwattr DW$T$29, DW_AT_address_class(0x20)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)
DW$T$40	.dwtag  DW_TAG_const_type
	.dwattr DW$T$40, DW_AT_type(*DW$T$20)
DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("tGeopotential"), DW_AT_type(*DW$T$23)
	.dwattr DW$T$28, DW_AT_language(DW_LANG_C)

DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$23, DW_AT_name("__tGeopotential")
	.dwattr DW$T$23, DW_AT_byte_size(0x3870)
DW$82	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$82, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$82, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$82, DW_AT_accessibility(DW_ACCESS_public)
DW$83	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$83, DW_AT_name("NormFact"), DW_AT_symbol_name("_NormFact")
	.dwattr DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$83, DW_AT_accessibility(DW_ACCESS_public)
DW$84	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$84, DW_AT_name("C"), DW_AT_symbol_name("_C")
	.dwattr DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0xb50]
	.dwattr DW$84, DW_AT_accessibility(DW_ACCESS_public)
DW$85	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$85, DW_AT_name("S"), DW_AT_symbol_name("_S")
	.dwattr DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x1698]
	.dwattr DW$85, DW_AT_accessibility(DW_ACCESS_public)
DW$86	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$86, DW_AT_name("P"), DW_AT_symbol_name("_P")
	.dwattr DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x21e0]
	.dwattr DW$86, DW_AT_accessibility(DW_ACCESS_public)
DW$87	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$87, DW_AT_name("dPfi"), DW_AT_symbol_name("_dPfi")
	.dwattr DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d28]
	.dwattr DW$87, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$23


DW$T$22	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$22, DW_AT_byte_size(0xb48)
DW$88	.dwtag  DW_TAG_subrange_type
	.dwattr DW$88, DW_AT_upper_bound(0x12)
DW$89	.dwtag  DW_TAG_subrange_type
	.dwattr DW$89, DW_AT_upper_bound(0x12)
	.dwendtag DW$T$22


	.dwattr DW$36, DW_AT_external(0x01)
	.dwattr DW$52, DW_AT_external(0x01)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$90	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$90, DW_AT_location[DW_OP_reg0]
DW$91	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$91, DW_AT_location[DW_OP_reg1]
DW$92	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$92, DW_AT_location[DW_OP_reg2]
DW$93	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$93, DW_AT_location[DW_OP_reg3]
DW$94	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$94, DW_AT_location[DW_OP_reg4]
DW$95	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$95, DW_AT_location[DW_OP_reg5]
DW$96	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$96, DW_AT_location[DW_OP_reg6]
DW$97	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$97, DW_AT_location[DW_OP_reg7]
DW$98	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$98, DW_AT_location[DW_OP_reg8]
DW$99	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$99, DW_AT_location[DW_OP_reg9]
DW$100	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$100, DW_AT_location[DW_OP_reg10]
DW$101	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$101, DW_AT_location[DW_OP_reg11]
DW$102	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$102, DW_AT_location[DW_OP_reg12]
DW$103	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$103, DW_AT_location[DW_OP_reg13]
DW$104	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$104, DW_AT_location[DW_OP_reg14]
DW$105	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$105, DW_AT_location[DW_OP_reg15]
DW$106	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$106, DW_AT_location[DW_OP_reg16]
DW$107	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$107, DW_AT_location[DW_OP_reg17]
DW$108	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$108, DW_AT_location[DW_OP_reg18]
DW$109	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$109, DW_AT_location[DW_OP_reg19]
DW$110	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$110, DW_AT_location[DW_OP_reg20]
DW$111	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$111, DW_AT_location[DW_OP_reg21]
DW$112	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$112, DW_AT_location[DW_OP_reg22]
DW$113	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$113, DW_AT_location[DW_OP_reg23]
DW$114	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$114, DW_AT_location[DW_OP_reg24]
DW$115	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$115, DW_AT_location[DW_OP_reg25]
DW$116	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$116, DW_AT_location[DW_OP_reg26]
DW$117	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$117, DW_AT_location[DW_OP_reg27]
DW$118	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$118, DW_AT_location[DW_OP_reg28]
DW$119	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$119, DW_AT_location[DW_OP_reg29]
DW$120	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$120, DW_AT_location[DW_OP_reg30]
DW$121	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$121, DW_AT_location[DW_OP_reg31]
DW$122	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$122, DW_AT_location[DW_OP_regx 0x20]
DW$123	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$123, DW_AT_location[DW_OP_regx 0x21]
DW$124	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$124, DW_AT_location[DW_OP_regx 0x22]
DW$125	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$125, DW_AT_location[DW_OP_regx 0x23]
DW$126	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$126, DW_AT_location[DW_OP_regx 0x24]
DW$127	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$127, DW_AT_location[DW_OP_regx 0x25]
DW$128	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$128, DW_AT_location[DW_OP_regx 0x26]
DW$129	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$129, DW_AT_location[DW_OP_regx 0x27]
DW$130	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$130, DW_AT_location[DW_OP_regx 0x28]
DW$131	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$131, DW_AT_location[DW_OP_regx 0x29]
DW$132	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$132, DW_AT_location[DW_OP_regx 0x2a]
DW$133	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$133, DW_AT_location[DW_OP_regx 0x2b]
DW$134	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$134, DW_AT_location[DW_OP_regx 0x2c]
DW$135	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$135, DW_AT_location[DW_OP_regx 0x2d]
DW$136	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$136, DW_AT_location[DW_OP_regx 0x2e]
DW$137	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$137, DW_AT_location[DW_OP_regx 0x2f]
DW$138	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$138, DW_AT_location[DW_OP_regx 0x30]
DW$139	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$139, DW_AT_location[DW_OP_regx 0x31]
DW$140	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$140, DW_AT_location[DW_OP_regx 0x32]
DW$141	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$141, DW_AT_location[DW_OP_regx 0x33]
DW$142	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$142, DW_AT_location[DW_OP_regx 0x34]
DW$143	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$143, DW_AT_location[DW_OP_regx 0x35]
DW$144	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$144, DW_AT_location[DW_OP_regx 0x36]
DW$145	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$145, DW_AT_location[DW_OP_regx 0x37]
DW$146	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$146, DW_AT_location[DW_OP_regx 0x38]
DW$147	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$147, DW_AT_location[DW_OP_regx 0x39]
DW$148	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$148, DW_AT_location[DW_OP_regx 0x3a]
DW$149	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$149, DW_AT_location[DW_OP_regx 0x3b]
DW$150	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$150, DW_AT_location[DW_OP_regx 0x3c]
DW$151	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$151, DW_AT_location[DW_OP_regx 0x3d]
DW$152	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$152, DW_AT_location[DW_OP_regx 0x3e]
DW$153	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$153, DW_AT_location[DW_OP_regx 0x3f]
DW$154	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$154, DW_AT_location[DW_OP_regx 0x40]
DW$155	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$155, DW_AT_location[DW_OP_regx 0x41]
DW$156	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$156, DW_AT_location[DW_OP_regx 0x42]
DW$157	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$157, DW_AT_location[DW_OP_regx 0x43]
DW$158	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$158, DW_AT_location[DW_OP_regx 0x44]
DW$159	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$159, DW_AT_location[DW_OP_regx 0x45]
DW$160	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$160, DW_AT_location[DW_OP_regx 0x46]
DW$161	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$161, DW_AT_location[DW_OP_regx 0x47]
DW$162	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$162, DW_AT_location[DW_OP_regx 0x48]
DW$163	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$163, DW_AT_location[DW_OP_regx 0x49]
DW$164	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$164, DW_AT_location[DW_OP_regx 0x4a]
DW$165	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$165, DW_AT_location[DW_OP_regx 0x4b]
DW$166	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$166, DW_AT_location[DW_OP_regx 0x4c]
DW$167	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$167, DW_AT_location[DW_OP_regx 0x4d]
DW$168	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$168, DW_AT_location[DW_OP_regx 0x4e]
DW$169	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$169, DW_AT_location[DW_OP_regx 0x4f]
DW$170	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$170, DW_AT_location[DW_OP_regx 0x50]
DW$171	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$171, DW_AT_location[DW_OP_regx 0x51]
DW$172	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$172, DW_AT_location[DW_OP_regx 0x52]
DW$173	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$173, DW_AT_location[DW_OP_regx 0x53]
DW$174	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$174, DW_AT_location[DW_OP_regx 0x54]
DW$175	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$175, DW_AT_location[DW_OP_regx 0x55]
DW$176	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$176, DW_AT_location[DW_OP_regx 0x56]
DW$177	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$177, DW_AT_location[DW_OP_regx 0x57]
DW$178	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$178, DW_AT_location[DW_OP_regx 0x58]
DW$179	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$179, DW_AT_location[DW_OP_regx 0x59]
DW$180	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$180, DW_AT_location[DW_OP_regx 0x5a]
DW$181	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$181, DW_AT_location[DW_OP_regx 0x5b]
DW$182	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$182, DW_AT_location[DW_OP_regx 0x5c]
DW$183	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$183, DW_AT_location[DW_OP_regx 0x5d]
DW$184	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$184, DW_AT_location[DW_OP_regx 0x5e]
DW$185	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$185, DW_AT_location[DW_OP_regx 0x5f]
DW$186	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$186, DW_AT_location[DW_OP_regx 0x60]
DW$187	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$187, DW_AT_location[DW_OP_regx 0x61]
DW$188	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$188, DW_AT_location[DW_OP_regx 0x62]
DW$189	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$189, DW_AT_location[DW_OP_regx 0x63]
DW$190	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$190, DW_AT_location[DW_OP_regx 0x64]
DW$191	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$191, DW_AT_location[DW_OP_regx 0x65]
DW$192	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$192, DW_AT_location[DW_OP_regx 0x66]
DW$193	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$193, DW_AT_location[DW_OP_regx 0x67]
DW$194	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$194, DW_AT_location[DW_OP_regx 0x68]
DW$195	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$195, DW_AT_location[DW_OP_regx 0x69]
DW$196	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$196, DW_AT_location[DW_OP_regx 0x6a]
DW$197	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$197, DW_AT_location[DW_OP_regx 0x6b]
DW$198	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$198, DW_AT_location[DW_OP_regx 0x6c]
DW$199	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$199, DW_AT_location[DW_OP_regx 0x6d]
DW$200	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$200, DW_AT_location[DW_OP_regx 0x6e]
DW$201	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$201, DW_AT_location[DW_OP_regx 0x6f]
DW$202	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$202, DW_AT_location[DW_OP_regx 0x70]
DW$203	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$203, DW_AT_location[DW_OP_regx 0x71]
DW$204	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$204, DW_AT_location[DW_OP_regx 0x72]
DW$205	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$205, DW_AT_location[DW_OP_regx 0x73]
DW$206	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$206, DW_AT_location[DW_OP_regx 0x74]
DW$207	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$207, DW_AT_location[DW_OP_regx 0x75]
DW$208	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$208, DW_AT_location[DW_OP_regx 0x76]
DW$209	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$209, DW_AT_location[DW_OP_regx 0x77]
DW$210	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$210, DW_AT_location[DW_OP_regx 0x78]
DW$211	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$211, DW_AT_location[DW_OP_regx 0x79]
DW$212	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$212, DW_AT_location[DW_OP_regx 0x7a]
DW$213	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$213, DW_AT_location[DW_OP_regx 0x7b]
DW$214	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$214, DW_AT_location[DW_OP_regx 0x7c]
DW$215	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$215, DW_AT_location[DW_OP_regx 0x7d]
DW$216	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$216, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

