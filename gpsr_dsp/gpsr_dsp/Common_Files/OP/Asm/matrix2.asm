;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:48:59 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("matrix2.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt"), DW_AT_symbol_name("_sqrt")
	.dwattr DW$1, DW_AT_type(*DW$T$17)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$1


DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("memset"), DW_AT_symbol_name("_memset")
	.dwattr DW$3, DW_AT_type(*DW$T$3)
	.dwattr DW$3, DW_AT_declaration(0x01)
	.dwattr DW$3, DW_AT_external(0x01)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$24)
	.dwendtag DW$3

;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI9642 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI9644 
	.sect	".text"
	.global	_CreateMatrixEx

DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("CreateMatrixEx"), DW_AT_symbol_name("_CreateMatrixEx")
	.dwattr DW$7, DW_AT_low_pc(_CreateMatrixEx)
	.dwattr DW$7, DW_AT_high_pc(0x00)
	.dwattr DW$7, DW_AT_begin_file("matrix2.c")
	.dwattr DW$7, DW_AT_begin_line(0x1e)
	.dwattr DW$7, DW_AT_begin_column(0x06)
	.dwattr DW$7, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr DW$7, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",31,1

;******************************************************************************
;* FUNCTION NAME: _CreateMatrixEx                                             *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,*
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Local Frame Size  : 0 Args + 0 Auto + 4 Save = 4 byte                    *
;******************************************************************************
_CreateMatrixEx:
;** --------------------------------------------------------------------------*
DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$8, DW_AT_type(*DW$T$29)
	.dwattr DW$8, DW_AT_location[DW_OP_reg4]
DW$9	.dwtag  DW_TAG_formal_parameter, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$9, DW_AT_type(*DW$T$22)
	.dwattr DW$9, DW_AT_location[DW_OP_reg20]
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("M"), DW_AT_symbol_name("_M")
	.dwattr DW$10, DW_AT_type(*DW$T$22)
	.dwattr DW$10, DW_AT_location[DW_OP_reg6]
           MVKL    .S1     _memset,A3        ; |35| 
           MVKH    .S1     _memset,A3        ; |35| 
           MVK     .S2     1152,B5           ; |33| 
           CALLRET .S2X    A3                ; |35| 
           MV      .L1X    B4,A5             ; |31| 
           MV      .L2X    A4,B4             ; |31| 
           STW     .D2T1   A5,*+B4[B5]       ; |33| 
           MVK     .S2     1153,B5           ; |34| 
	.dwpsn	"matrix2.c",37,1

           ZERO    .L2     B4                ; |35| 
||         MVK     .S1     0x1200,A6         ; |35| 
||         STW     .D2T1   A6,*+B4[B5]       ; |34| 

RL0:       ; CALL-RETURN OCCURS {_memset}    ; |35| 
	.dwattr DW$7, DW_AT_end_file("matrix2.c")
	.dwattr DW$7, DW_AT_end_line(0x25)
	.dwattr DW$7, DW_AT_end_column(0x01)
	.dwendtag DW$7

	.sect	".text"
	.global	_DestroyMatrixEx

DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("DestroyMatrixEx"), DW_AT_symbol_name("_DestroyMatrixEx")
	.dwattr DW$11, DW_AT_low_pc(_DestroyMatrixEx)
	.dwattr DW$11, DW_AT_high_pc(0x00)
	.dwattr DW$11, DW_AT_begin_file("matrix2.c")
	.dwattr DW$11, DW_AT_begin_line(0x2c)
	.dwattr DW$11, DW_AT_begin_column(0x06)
	.dwattr DW$11, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr DW$11, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",45,1

;******************************************************************************
;* FUNCTION NAME: _DestroyMatrixEx                                            *
;*                                                                            *
;*   Regs Modified     : A3,A5                                                *
;*   Regs Used         : A3,A4,A5,B3                                          *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_DestroyMatrixEx:
;** --------------------------------------------------------------------------*
DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$12, DW_AT_type(*DW$T$29)
	.dwattr DW$12, DW_AT_location[DW_OP_reg4]
           RETNOP  .S2     B3,2              ; |50| 

           MVK     .S1     1153,A5           ; |47| 
||         ZERO    .L1     A3                ; |46| 

           MVK     .S1     1152,A5           ; |46| 
||         STW     .D1T1   A3,*+A4[A5]       ; |47| 

	.dwpsn	"matrix2.c",50,1
           STW     .D1T1   A3,*+A4[A5]       ; |46| 
           ; BRANCH OCCURS {B3}              ; |50| 
	.dwattr DW$11, DW_AT_end_file("matrix2.c")
	.dwattr DW$11, DW_AT_end_line(0x32)
	.dwattr DW$11, DW_AT_end_column(0x01)
	.dwendtag DW$11

	.sect	".text"
	.global	_CopyMatrixEx

DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("CopyMatrixEx"), DW_AT_symbol_name("_CopyMatrixEx")
	.dwattr DW$13, DW_AT_low_pc(_CopyMatrixEx)
	.dwattr DW$13, DW_AT_high_pc(0x00)
	.dwattr DW$13, DW_AT_begin_file("matrix2.c")
	.dwattr DW$13, DW_AT_begin_line(0x3a)
	.dwattr DW$13, DW_AT_begin_column(0x06)
	.dwattr DW$13, DW_AT_frame_base[DW_OP_breg31 24]
	.dwattr DW$13, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",59,1

;******************************************************************************
;* FUNCTION NAME: _CopyMatrixEx                                               *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B0,B1,B2,  *
;*                           B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,A19, *
;*                           A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31, *
;*                           B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26,B27, *
;*                           B28,B29,B30,B31                                  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B0,B1,B2,  *
;*                           B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,A19, *
;*                           A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31, *
;*                           B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26,B27, *
;*                           B28,B29,B30,B31                                  *
;*   Local Frame Size  : 0 Args + 0 Auto + 20 Save = 20 byte                  *
;******************************************************************************
_CopyMatrixEx:
;** --------------------------------------------------------------------------*
DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$14, DW_AT_type(*DW$T$29)
	.dwattr DW$14, DW_AT_location[DW_OP_reg4]
DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("B"), DW_AT_symbol_name("_B")
	.dwattr DW$15, DW_AT_type(*DW$T$29)
	.dwattr DW$15, DW_AT_location[DW_OP_reg20]

           STW     .D2T1   A12,*SP--(24)     ; |59| 
||         MVK     .S1     1152,A3           ; |60| 
||         MVKL    .S2     _CreateMatrixEx,B5 ; |63| 

           STDW    .D2T2   B11:B10,*+SP(16)  ; |59| 
||         MVK     .S1     1153,A6           ; |60| 
||         MVKH    .S2     _CreateMatrixEx,B5 ; |63| 
||         MV      .L2     B3,B11            ; |59| 

           STDW    .D2T1   A11:A10,*+SP(8)   ; |59| 
||         MV      .L1X    B4,A11            ; |59| 
||         MV      .S1     A4,A10            ; |59| 

           LDW     .D1T1   *+A11[A3],A12     ; |60| 

           LDW     .D1T1   *+A10[A3],A5      ; |62| 
||         MVK     .S1     1153,A3           ; |62| 

           LDW     .D1T1   *+A10[A3],A3      ; |62| 
           LDW     .D1T2   *+A11[A6],B10     ; |60| 
           NOP             3
           CMPEQ   .L1     A5,A12,A5         ; |62| 

           MV      .L1X    B10,A6            ; |63| 
||         CMPEQ   .L2X    A3,B10,B4         ; |62| 

           AND     .L2X    A5,B4,B0

   [ B0]   ZERO    .D1     A4                ; |65| 
|| [ B0]   CMPEQ   .L1     A10,A11,A0        ; |65| 
||         MV      .L2X    A12,B4            ; |63| 
|| [ B0]   B       .S1     L1                ; |62| 

   [ B0]   CMPGT   .L1     A12,0,A3          ; |65| 
|| [!B0]   CALL    .S2     B5                ; |63| 

           NOP             4
           ; BRANCHCC OCCURS {L1}            ; |62| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL1,B3,0          ; |63| 
RL1:       ; CALL OCCURS {_CreateMatrixEx}   ; |63| 

           CMPGT   .L1     A12,0,A3          ; |65| 
||         ZERO    .S1     A4                ; |65| 

           CMPEQ   .L1     A10,A11,A0        ; |65| 
;** --------------------------------------------------------------------------*
L1:    

           XOR     .L1     1,A3,A3           ; |65| 
|| [ A0]   MVK     .S1     0x1,A4            ; |65| 
||         MV      .S2X    A12,B1            ; |67| 
||         ZERO    .D1     A7                ; |66| 
||         CMPGT   .L2     B10,0,B0          ; |67| 

           OR      .L1     A3,A4,A0          ; |65| 
||         MVK     .S1     192,A3

   [ A0]   BNOP    .S1     L7,5              ; |65| 
           ; BRANCHCC OCCURS {L7}            ; |65| 
;** --------------------------------------------------------------------------*

   [!B0]   BNOP    .S1     L6,3              ; |67| 
||         MPYLI   .M1     A3,A7,A5:A4
||         MV      .L2     B0,B2             ; guard predicate rewrite
|| [ B0]   SUB     .S2     B10,1,B0

	.dwpsn	"matrix2.c",66,0

   [ B2]   ADD     .L1     A10,A4,A6
|| [ B2]   ADD     .S1     A11,A4,A3

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L2
;** --------------------------------------------------------------------------*
L2:    
DW$L$_CopyMatrixEx$5$B:
	.dwpsn	"matrix2.c",67,0
           NOP             1
           ; BRANCHCC OCCURS {L6}            ; |67| 
DW$L$_CopyMatrixEx$5$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 67
;*      Loop opening brace source line   : 68
;*      Loop closing brace source line   : 68
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 6
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        1     
;*      .D units                     2*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             2*       0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        1     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 1 iterations in parallel
;*      Done
;*
;*      Loop is interruptible
;*      Collapsed epilog stages     : 0
;*      Collapsed prolog stages     : 0
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L3:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L4:    ; PIPED LOOP KERNEL
DW$L$_CopyMatrixEx$7$B:
	.dwpsn	"matrix2.c",68,0

   [ B0]   BDEC    .S2     L4,B0             ; |67| <0,0> 
||         LDDW    .D1T1   *A3++,A5:A4       ; |68| <0,0>  ^ 

           NOP             4
           STDW    .D1T1   A5:A4,*A6++       ; |68| <0,5>  ^ 
DW$L$_CopyMatrixEx$7$E:
;** --------------------------------------------------------------------------*
L5:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
L6:    
DW$L$_CopyMatrixEx$9$B:

           SUB     .S2     B1,1,B1           ; |66| 
||         ADD     .L1     1,A7,A7           ; |66| 
||         CMPGT   .L2     B10,0,B0          ; |67| 

   [ B1]   B       .S2     L2                ; |66| 
|| [!B1]   MVK     .L2     0x1,B0            ; nullify predicate
|| [ B1]   MVK     .S1     192,A3
|| [ B1]   MV      .D2     B0,B2             ; guard predicate rewrite

   [!B0]   BNOP    .S1     L6,3              ; |67| 
|| [ B1]   MPYLI   .M1     A3,A7,A5:A4
|| [ B0]   SUB     .L2     B10,1,B0


   [ B2]   ADD     .L1     A10,A4,A6
|| [ B2]   ADD     .S1     A11,A4,A3

           ; BRANCHCC OCCURS {L2}            ; |66| 
DW$L$_CopyMatrixEx$9$E:
;** --------------------------------------------------------------------------*
L7:    

           LDDW    .D2T1   *+SP(8),A11:A10   ; |71| 
||         MV      .L2     B11,B3            ; |71| 

           RET     .S2     B3                ; |71| 
||         LDDW    .D2T2   *+SP(16),B11:B10  ; |71| 

           LDW     .D2T1   *++SP(24),A12     ; |71| 
	.dwpsn	"matrix2.c",71,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |71| 

DW$16	.dwtag  DW_TAG_loop
	.dwattr DW$16, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L2:1:1472539739")
	.dwattr DW$16, DW_AT_begin_file("matrix2.c")
	.dwattr DW$16, DW_AT_begin_line(0x42)
	.dwattr DW$16, DW_AT_end_line(0x44)
DW$17	.dwtag  DW_TAG_loop_range
	.dwattr DW$17, DW_AT_low_pc(DW$L$_CopyMatrixEx$5$B)
	.dwattr DW$17, DW_AT_high_pc(DW$L$_CopyMatrixEx$5$E)
DW$18	.dwtag  DW_TAG_loop_range
	.dwattr DW$18, DW_AT_low_pc(DW$L$_CopyMatrixEx$9$B)
	.dwattr DW$18, DW_AT_high_pc(DW$L$_CopyMatrixEx$9$E)

DW$19	.dwtag  DW_TAG_loop
	.dwattr DW$19, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L4:2:1472539739")
	.dwattr DW$19, DW_AT_begin_file("matrix2.c")
	.dwattr DW$19, DW_AT_begin_line(0x43)
	.dwattr DW$19, DW_AT_end_line(0x44)
DW$20	.dwtag  DW_TAG_loop_range
	.dwattr DW$20, DW_AT_low_pc(DW$L$_CopyMatrixEx$7$B)
	.dwattr DW$20, DW_AT_high_pc(DW$L$_CopyMatrixEx$7$E)
	.dwendtag DW$19

	.dwendtag DW$16

	.dwattr DW$13, DW_AT_end_file("matrix2.c")
	.dwattr DW$13, DW_AT_end_line(0x47)
	.dwattr DW$13, DW_AT_end_column(0x01)
	.dwendtag DW$13

	.sect	".text"
	.global	_MultMatrixEx

DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("MultMatrixEx"), DW_AT_symbol_name("_MultMatrixEx")
	.dwattr DW$21, DW_AT_low_pc(_MultMatrixEx)
	.dwattr DW$21, DW_AT_high_pc(0x00)
	.dwattr DW$21, DW_AT_begin_file("matrix2.c")
	.dwattr DW$21, DW_AT_begin_line(0x50)
	.dwattr DW$21, DW_AT_begin_column(0x06)
	.dwattr DW$21, DW_AT_frame_base[DW_OP_breg31 4688]
	.dwattr DW$21, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",81,1

;******************************************************************************
;* FUNCTION NAME: _MultMatrixEx                                               *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 4640 Auto + 44 Save = 4684 byte             *
;******************************************************************************
_MultMatrixEx:
;** --------------------------------------------------------------------------*
DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$22, DW_AT_type(*DW$T$29)
	.dwattr DW$22, DW_AT_location[DW_OP_reg4]
DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_name("B"), DW_AT_symbol_name("_B")
	.dwattr DW$23, DW_AT_type(*DW$T$29)
	.dwattr DW$23, DW_AT_location[DW_OP_reg20]
DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("C"), DW_AT_symbol_name("_C")
	.dwattr DW$24, DW_AT_type(*DW$T$29)
	.dwattr DW$24, DW_AT_location[DW_OP_reg6]

           MV      .L1X    SP,A31            ; |81| 
||         ADDK    .S2     -4688,SP          ; |81| 
||         MVK     .S1     1153,A3           ; |89| 
||         CMPEQ   .L2X    A4,B4,B0          ; |95| 
||         ZERO    .D2     B6                ; |86| 

           STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B12,*+SP(4680)
||         MV      .L1X    B4,A14            ; |81| 
||         MVK     .S2     4620,B5           ; |86| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B13,*+SP(4684)
||         MVKL    .S2     _DestroyMatrixEx,B4 ; |91| 
||         ADD     .L2     B5,SP,B5          ; |86| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B10,*+SP(4672)
||         MVKH    .S2     _DestroyMatrixEx,B4 ; |91| 

           STW     .D2T1   A15,*+SP(4688)
||         MV      .L1     A6,A15            ; |81| 

           STW     .D2T2   B11,*+SP(4676)
           STW     .D2T2   B3,*+SP(4668)

           LDW     .D1T1   *+A14[A3],A5      ; |89| 
||         MVK     .S1     1152,A3           ; |89| 
||         STW     .D2T2   B6,*-B5(4)        ; |87| 

           LDW     .D1T1   *+A15[A3],A3      ; |89| 
||         STW     .D2T2   B6,*B5            ; |86| 
||         ZERO    .L2     B5                ; |95| 

           NOP             3
           STW     .D2T1   A4,*+SP(4624)     ; |81| 
           CMPEQ   .L1     A5,A3,A0          ; |89| 

   [!A0]   BNOP    .S1     L14,5             ; |89| 
||         CMPEQ   .L1     A4,A15,A0         ; |95| 
|| [ A0]   ZERO    .L2     B4                ; |95| 

           ; BRANCHCC OCCURS {L14}           ; |89| 
;** --------------------------------------------------------------------------*

   [ B0]   MVK     .L2     0x1,B5            ; |95| 
|| [ A0]   MVK     .D2     0x1,B4            ; |95| 
||         MVK     .S1     1153,A3           ; |95| 
||         ADD     .L1X    8,SP,A4           ; |95| 
||         MVKL    .S2     _CreateMatrixEx,B6 ; |95| 

           OR      .L2     B4,B5,B4          ; |95| 
||         LDW     .D1T1   *+A15[A3],A6      ; |95| 
||         MVKH    .S2     _CreateMatrixEx,B6 ; |95| 
||         MVK     .S1     1152,A3           ; |95| 

           STW     .D2T1   A4,*+SP(4628)     ; |95| 

           MV      .L1X    B4,A0             ; |95| 
||         STW     .D2T2   B4,*+SP(4632)     ; |95| 

   [!A0]   LDW     .D2T1   *+SP(4624),A4     ; |95| 
           NOP             4

           CALL    .S2     B6                ; |95| 
|| [!A0]   STW     .D2T1   A4,*+SP(4628)     ; |95| 

           LDW     .D2T1   *+SP(4628),A4     ; |95| 
||         LDW     .D1T2   *+A14[A3],B4      ; |95| 

           ADDKPC  .S2     RL2,B3,3          ; |95| 
RL2:       ; CALL OCCURS {_CreateMatrixEx}   ; |95| 
;** --------------------------------------------------------------------------*

           MVK     .S1     1152,A3           ; |97| 
||         ZERO    .L1     A31               ; |97| 

           LDW     .D1T1   *+A14[A3],A3      ; |97| 
||         MVK     .S1     1153,A4

           NOP             3
           STW     .D2T1   A31,*+SP(4636)
           CMPGT   .L1     A3,0,A0           ; |97| 

   [!A0]   BNOP    .S1     L13,2             ; |97| 
||         MV      .L1     A0,A1             ; guard predicate rewrite
|| [ A0]   LDW     .D1T1   *+A15[A4],A3

   [ A1]   LDW     .D2T1   *+SP(4636),A4
           NOP             1
           CMPGT   .L1     A3,0,A0           ; |98| 
           ; BRANCHCC OCCURS {L13}           ; |97| 
;** --------------------------------------------------------------------------*

   [!A0]   BNOP    .S2     L12,1             ; |98| 
|| [ A0]   MVK     .S1     192,A3
|| [ A0]   LDW     .D2T1   *+SP(4628),A31

	.dwpsn	"matrix2.c",97,0
           MPYLI   .M1     A3,A4,A5:A4
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L8
;** --------------------------------------------------------------------------*
L8:    
DW$L$_MultMatrixEx$5$B:
	.dwpsn	"matrix2.c",98,0
           NOP             3
           ; BRANCHCC OCCURS {L12}           ; |98| 
DW$L$_MultMatrixEx$5$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrixEx$6$B:

           ZERO    .L2     B13
||         ZERO    .S1     A13               ; |98| 
||         ZERO    .L1     A11:A10           ; |100| 

           ADD     .L1     A31,A4,A3
||         STW     .D2T1   A4,*+SP(4640)

           NOP             1

           MVK     .S1     1153,A3           ; |100| 
||         MV      .L2X    A3,B11

           LDW     .D1T1   *+A14[A3],A3      ; |100| 
           NOP             4
DW$L$_MultMatrixEx$6$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L9
;** --------------------------------------------------------------------------*
L9:    
DW$L$_MultMatrixEx$7$B:
	.dwpsn	"matrix2.c",99,0

           MV      .L2X    A3,B10
||         CMPGT   .L1     A3,0,A0           ; |100| 
||         LDW     .D2T1   *+SP(4640),A3

   [!A0]   BNOP    .S1     L11,3             ; |100| 

           ADD     .L1     A3,A14,A12
||         MVKL    .S1     __mpyd,A3         ; |101| 

           MVKH    .S1     __mpyd,A3         ; |101| 
           ; BRANCHCC OCCURS {L11}           ; |100| 
DW$L$_MultMatrixEx$7$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrixEx$8$B:

           ADD     .L2X    B13,A15,B12
||         LDDW    .D1T1   *A12++,A5:A4      ; |101| 

           CALL    .S2X    A3                ; |101| 
||         LDDW    .D2T2   *B12,B5:B4        ; |101| 

	.dwpsn	"matrix2.c",100,0
           NOP             3
DW$L$_MultMatrixEx$8$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L10:    
DW$L$_MultMatrixEx$9$B:
	.dwpsn	"matrix2.c",101,0
           ADDKPC  .S2     RL4,B3,1          ; |101| 
RL4:       ; CALL OCCURS {__mpyd}            ; |101| 
DW$L$_MultMatrixEx$9$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrixEx$10$B:
           MVKL    .S2     __addd,B6         ; |101| 
           MVKH    .S2     __addd,B6         ; |101| 
           CALL    .S2     B6                ; |101| 
           MV      .L2X    A4,B4             ; |101| 
           ADDKPC  .S2     RL5,B3,0          ; |101| 
           MV      .S1     A10,A4            ; |101| 
           MV      .L2X    A5,B5             ; |101| 
           MV      .L1     A11,A5            ; |101| 
RL5:       ; CALL OCCURS {__addd}            ; |101| 
DW$L$_MultMatrixEx$10$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrixEx$11$B:

           SUB     .L2     B10,1,B10         ; |100| 
||         ADDK    .S2     192,B12           ; |100| 
||         MVKL    .S1     __mpyd,A3         ; |101| 
||         SUB     .L1X    B10,1,A0          ; |100| 
||         MV      .D1     A4,A10            ; |101| 

   [ A0]   BNOP    .S2     L10,1             ; |100| 
||         MV      .L1     A5,A11            ; |101| 
||         MVKH    .S1     __mpyd,A3         ; |101| 
|| [ A0]   LDDW    .D1T1   *A12++,A5:A4      ; |101| 
|| [ A0]   LDDW    .D2T2   *B12,B5:B4        ; |101| 

   [ A0]   CALL    .S2X    A3                ; |101| 
           NOP             3
           ; BRANCHCC OCCURS {L10}           ; |100| 
DW$L$_MultMatrixEx$11$E:
;** --------------------------------------------------------------------------*
L11:    
DW$L$_MultMatrixEx$12$B:

           MV      .L1X    B11,A3
||         MVK     .S1     1153,A4           ; |98| 
||         ADD     .D1     1,A13,A13         ; |98| 
||         ADD     .L2     8,B13,B13         ; |98| 

           STDW    .D1T1   A11:A10,*A3++     ; |102| 
           NOP             1

           LDW     .D1T1   *+A15[A4],A3      ; |98| 
||         MV      .L2X    A3,B11            ; |102| 

           NOP             4
           CMPGT   .L1     A3,A13,A0         ; |98| 

   [ A0]   B       .S2     L9                ; |98| 
|| [ A0]   MVK     .S1     1153,A3           ; |100| 
|| [ A0]   ZERO    .L1     A11:A10           ; |100| 

   [ A0]   LDW     .D1T1   *+A14[A3],A3      ; |100| 
	.dwpsn	"matrix2.c",103,0
           NOP             4
           ; BRANCHCC OCCURS {L9}            ; |98| 
DW$L$_MultMatrixEx$12$E:
;** --------------------------------------------------------------------------*
L12:    
DW$L$_MultMatrixEx$13$B:

           LDW     .D2T1   *+SP(4636),A31
||         MVK     .S1     1152,A4           ; |97| 

           LDW     .D1T1   *+A14[A4],A5      ; |97| 
           NOP             3
           ADD     .L1     1,A31,A4          ; |97| 

           STW     .D2T1   A4,*+SP(4636)     ; |97| 
||         CMPGT   .L1     A5,A4,A0          ; |97| 

   [ A0]   CMPGT   .L1     A3,0,A0           ; |98| 
||         MV      .D1     A0,A1             ; guard predicate rewrite
||         LDW     .D2T1   *+SP(4636),A4
|| [ A0]   B       .S1     L8                ; |97| 

   [!A1]   MVK     .L1     0x1,A0            ; nullify predicate

   [ A0]   LDW     .D2T1   *+SP(4628),A31
|| [ A0]   MVK     .S1     192,A3

   [!A0]   BNOP    .S1     L12,1             ; |98| 
           MPYLI   .M1     A3,A4,A5:A4
           ; BRANCHCC OCCURS {L8}            ; |97| 
DW$L$_MultMatrixEx$13$E:
;** --------------------------------------------------------------------------*
L13:    

           LDW     .D2T1   *+SP(4632),A0     ; |106| 
||         MVKL    .S2     _CopyMatrixEx,B5  ; |106| 
||         MVKL    .S1     _DestroyMatrixEx,A10 ; |107| 
||         ADD     .L2     8,SP,B10          ; |107| 

           MVKH    .S2     _CopyMatrixEx,B5  ; |106| 
           LDW     .D2T1   *+SP(4624),A4     ; |106| 
           MVKH    .S1     _DestroyMatrixEx,A10 ; |107| 
           ADD     .L2     8,SP,B4           ; |106| 

   [!A0]   B       .S1     L15               ; |104| 
|| [!A0]   LDW     .D2T2   *+SP(4668),B3     ; |111| 

   [ A0]   CALL    .S2     B5                ; |106| 
|| [!A0]   LDW     .D2T1   *+SP(4688),A15    ; |111| 

   [!A0]   LDW     .D2T1   *+SP(4652),A11    ; |111| 
   [!A0]   LDW     .D2T1   *+SP(4648),A10    ; |111| 
   [!A0]   LDW     .D2T1   *+SP(4660),A13    ; |111| 
   [!A0]   LDW     .D2T1   *+SP(4656),A12    ; |111| 
           ; BRANCHCC OCCURS {L15}           ; |104| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL6,B3,0          ; |106| 
RL6:       ; CALL OCCURS {_CopyMatrixEx}     ; |106| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A10               ; |107| 
           ADDKPC  .S2     RL7,B3,3          ; |107| 
           MV      .L1X    B10,A4            ; |107| 
RL7:       ; CALL OCCURS {_DestroyMatrixEx}  ; |107| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4668),B3     ; |111| 
           LDW     .D2T1   *+SP(4652),A11    ; |111| 
           LDW     .D2T1   *+SP(4648),A10    ; |111| 
           LDW     .D2T1   *+SP(4660),A13    ; |111| 
           LDW     .D2T1   *+SP(4656),A12    ; |111| 
           LDW     .D2T1   *+SP(4664),A14    ; |111| 
           LDW     .D2T2   *+SP(4676),B11    ; |111| 
           LDW     .D2T2   *+SP(4672),B10    ; |111| 
           LDW     .D2T2   *+SP(4684),B13    ; |111| 

           RET     .S2     B3                ; |111| 
||         LDW     .D2T1   *+SP(4688),A15    ; |111| 

           LDW     .D2T2   *+SP(4680),B12    ; |111| 
           NOP             3
           ADDK    .S2     4688,SP           ; |111| 
           ; BRANCH OCCURS {B3}              ; |111| 
;** --------------------------------------------------------------------------*
L14:    
           CALL    .S2     B4                ; |91| 
           ADDKPC  .S2     RL8,B3,4          ; |91| 
RL8:       ; CALL OCCURS {_DestroyMatrixEx}  ; |91| 
           LDW     .D2T1   *+SP(4688),A15    ; |111| 
           LDW     .D2T1   *+SP(4652),A11    ; |111| 
           LDW     .D2T1   *+SP(4648),A10    ; |111| 
           LDW     .D2T1   *+SP(4660),A13    ; |111| 
           LDW     .D2T2   *+SP(4668),B3     ; |111| 
           LDW     .D2T1   *+SP(4656),A12    ; |111| 
;** --------------------------------------------------------------------------*
L15:    
           LDW     .D2T2   *+SP(4680),B12    ; |111| 
           LDW     .D2T2   *+SP(4684),B13    ; |111| 
           LDW     .D2T2   *+SP(4672),B10    ; |111| 

           LDW     .D2T2   *+SP(4676),B11    ; |111| 
||         RET     .S2     B3                ; |111| 

           LDW     .D2T1   *+SP(4664),A14    ; |111| 
||         ADDK    .S2     4688,SP           ; |111| 

	.dwpsn	"matrix2.c",111,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |111| 

DW$25	.dwtag  DW_TAG_loop
	.dwattr DW$25, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L8:1:1472539739")
	.dwattr DW$25, DW_AT_begin_file("matrix2.c")
	.dwattr DW$25, DW_AT_begin_line(0x61)
	.dwattr DW$25, DW_AT_end_line(0x67)
DW$26	.dwtag  DW_TAG_loop_range
	.dwattr DW$26, DW_AT_low_pc(DW$L$_MultMatrixEx$5$B)
	.dwattr DW$26, DW_AT_high_pc(DW$L$_MultMatrixEx$5$E)
DW$27	.dwtag  DW_TAG_loop_range
	.dwattr DW$27, DW_AT_low_pc(DW$L$_MultMatrixEx$6$B)
	.dwattr DW$27, DW_AT_high_pc(DW$L$_MultMatrixEx$6$E)
DW$28	.dwtag  DW_TAG_loop_range
	.dwattr DW$28, DW_AT_low_pc(DW$L$_MultMatrixEx$13$B)
	.dwattr DW$28, DW_AT_high_pc(DW$L$_MultMatrixEx$13$E)

DW$29	.dwtag  DW_TAG_loop
	.dwattr DW$29, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L9:2:1472539739")
	.dwattr DW$29, DW_AT_begin_file("matrix2.c")
	.dwattr DW$29, DW_AT_begin_line(0x62)
	.dwattr DW$29, DW_AT_end_line(0x67)
DW$30	.dwtag  DW_TAG_loop_range
	.dwattr DW$30, DW_AT_low_pc(DW$L$_MultMatrixEx$7$B)
	.dwattr DW$30, DW_AT_high_pc(DW$L$_MultMatrixEx$7$E)
DW$31	.dwtag  DW_TAG_loop_range
	.dwattr DW$31, DW_AT_low_pc(DW$L$_MultMatrixEx$8$B)
	.dwattr DW$31, DW_AT_high_pc(DW$L$_MultMatrixEx$8$E)
DW$32	.dwtag  DW_TAG_loop_range
	.dwattr DW$32, DW_AT_low_pc(DW$L$_MultMatrixEx$12$B)
	.dwattr DW$32, DW_AT_high_pc(DW$L$_MultMatrixEx$12$E)

DW$33	.dwtag  DW_TAG_loop
	.dwattr DW$33, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L10:3:1472539739")
	.dwattr DW$33, DW_AT_begin_file("matrix2.c")
	.dwattr DW$33, DW_AT_begin_line(0x64)
	.dwattr DW$33, DW_AT_end_line(0x65)
DW$34	.dwtag  DW_TAG_loop_range
	.dwattr DW$34, DW_AT_low_pc(DW$L$_MultMatrixEx$9$B)
	.dwattr DW$34, DW_AT_high_pc(DW$L$_MultMatrixEx$9$E)
DW$35	.dwtag  DW_TAG_loop_range
	.dwattr DW$35, DW_AT_low_pc(DW$L$_MultMatrixEx$10$B)
	.dwattr DW$35, DW_AT_high_pc(DW$L$_MultMatrixEx$10$E)
DW$36	.dwtag  DW_TAG_loop_range
	.dwattr DW$36, DW_AT_low_pc(DW$L$_MultMatrixEx$11$B)
	.dwattr DW$36, DW_AT_high_pc(DW$L$_MultMatrixEx$11$E)
	.dwendtag DW$33

	.dwendtag DW$29

	.dwendtag DW$25

	.dwattr DW$21, DW_AT_end_file("matrix2.c")
	.dwattr DW$21, DW_AT_end_line(0x6f)
	.dwattr DW$21, DW_AT_end_column(0x01)
	.dwendtag DW$21

	.sect	".text"
	.global	_MultMatrix_B_TC_Ex

DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("MultMatrix_B_TC_Ex"), DW_AT_symbol_name("_MultMatrix_B_TC_Ex")
	.dwattr DW$37, DW_AT_low_pc(_MultMatrix_B_TC_Ex)
	.dwattr DW$37, DW_AT_high_pc(0x00)
	.dwattr DW$37, DW_AT_begin_file("matrix2.c")
	.dwattr DW$37, DW_AT_begin_line(0x79)
	.dwattr DW$37, DW_AT_begin_column(0x06)
	.dwattr DW$37, DW_AT_frame_base[DW_OP_breg31 4680]
	.dwattr DW$37, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",122,1

;******************************************************************************
;* FUNCTION NAME: _MultMatrix_B_TC_Ex                                         *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 4632 Auto + 44 Save = 4676 byte             *
;******************************************************************************
_MultMatrix_B_TC_Ex:
;** --------------------------------------------------------------------------*
DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$38, DW_AT_type(*DW$T$29)
	.dwattr DW$38, DW_AT_location[DW_OP_reg4]
DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("B"), DW_AT_symbol_name("_B")
	.dwattr DW$39, DW_AT_type(*DW$T$29)
	.dwattr DW$39, DW_AT_location[DW_OP_reg20]
DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("C"), DW_AT_symbol_name("_C")
	.dwattr DW$40, DW_AT_type(*DW$T$29)
	.dwattr DW$40, DW_AT_location[DW_OP_reg6]

           MV      .L1X    SP,A31            ; |122| 
||         ADDK    .S2     -4680,SP          ; |122| 
||         MVK     .S1     1153,A3           ; |130| 
||         CMPEQ   .L2X    A4,B4,B0          ; |136| 
||         ZERO    .D2     B6                ; |127| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(4660)
||         MVK     .S2     4620,B5           ; |127| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B11,*+SP(4668)
||         ADD     .L2     B5,SP,B5          ; |127| 

           STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B10,*+SP(4664)
||         MV      .L1X    B4,A14            ; |122| 
||         MVKL    .S2     _DestroyMatrixEx,B4 ; |132| 

           STW     .D2T2   B13,*+SP(4676)
||         MVKH    .S2     _DestroyMatrixEx,B4 ; |132| 

           STW     .D2T2   B12,*+SP(4672)

           STW     .D2T1   A15,*+SP(4680)
||         MV      .L1     A6,A15            ; |122| 

           LDW     .D1T1   *+A14[A3],A5      ; |130| 
||         STW     .D2T2   B6,*B5            ; |127| 

           LDW     .D1T1   *+A15[A3],A3      ; |130| 
||         STW     .D2T2   B6,*-B5(4)        ; |128| 

           NOP             3
           STW     .D2T1   A4,*+SP(4624)     ; |122| 
           CMPEQ   .L1     A5,A3,A0          ; |130| 

   [!A0]   BNOP    .S1     L22,4             ; |130| 
|| [ A0]   CMPEQ   .L1     A4,A15,A0         ; |136| 
||         MV      .D1     A0,A1             ; guard predicate rewrite

   [ A1]   ZERO    .L2     B4                ; |136| 
           ; BRANCHCC OCCURS {L22}           ; |130| 
;** --------------------------------------------------------------------------*

   [ B0]   MVK     .L2     0x1,B6            ; |136| 
|| [ A0]   MVK     .D2     0x1,B4            ; |136| 
||         MVK     .S1     1152,A3           ; |136| 
||         ADD     .L1X    8,SP,A4           ; |136| 
||         MVKL    .S2     _CreateMatrixEx,B5 ; |136| 

           OR      .L2     B4,B6,B0          ; |136| 
||         LDW     .D1T1   *+A15[A3],A6      ; |136| 
||         MVKH    .S2     _CreateMatrixEx,B5 ; |136| 

           STW     .D2T1   A4,*+SP(4628)     ; |136| 
   [!B0]   LDW     .D2T1   *+SP(4624),A4     ; |136| 
           NOP             4

           CALL    .S2     B5                ; |136| 
|| [!B0]   STW     .D2T1   A4,*+SP(4628)     ; |136| 

           LDW     .D2T1   *+SP(4628),A4     ; |136| 
||         LDW     .D1T2   *+A14[A3],B4      ; |136| 

           ADDKPC  .S2     RL9,B3,3          ; |136| 
RL9:       ; CALL OCCURS {_CreateMatrixEx}   ; |136| 
;** --------------------------------------------------------------------------*

           MVK     .S1     1152,A3           ; |139| 
||         ZERO    .L2     B11               ; |139| 

           LDW     .D1T1   *+A14[A3],A3      ; |139| 
           NOP             4

           MVK     .S1     1152,A3
||         CMPGT   .L1     A3,0,A0           ; |139| 

   [!A0]   BNOP    .S1     L21,4             ; |139| 
|| [ A0]   LDW     .D1T1   *+A15[A3],A4

           CMPGT   .L1     A4,0,A0           ; |140| 
           ; BRANCHCC OCCURS {L21}           ; |139| 
;** --------------------------------------------------------------------------*

           MVK     .S1     192,A3
||         LDW     .D2T1   *+SP(4628),A31
|| [!A0]   B       .S2     L20               ; |140| 
|| [ A0]   MV      .L1X    B11,A4

   [ A0]   MPYLI   .M1     A3,A4,A5:A4
	.dwpsn	"matrix2.c",139,0
           NOP             2
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L16
;** --------------------------------------------------------------------------*
L16:    
DW$L$_MultMatrix_B_TC_Ex$5$B:
	.dwpsn	"matrix2.c",140,0
           NOP             1
           ADD     .L1     A31,A4,A3
           ; BRANCHCC OCCURS {L20}           ; |140| 
DW$L$_MultMatrix_B_TC_Ex$5$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_B_TC_Ex$6$B:

           STW     .D2T1   A4,*+SP(4632)
||         ZERO    .S1     A13               ; |140| 
||         ZERO    .L1     A11:A10           ; |142| 

           MV      .L2X    A3,B10
||         MVK     .S1     1153,A3           ; |142| 

           LDW     .D1T1   *+A14[A3],A3      ; |142| 
           NOP             4
           CMPGT   .L1     A3,0,A0           ; |142| 
DW$L$_MultMatrix_B_TC_Ex$6$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L17
;** --------------------------------------------------------------------------*
L17:    
DW$L$_MultMatrix_B_TC_Ex$7$B:
	.dwpsn	"matrix2.c",141,0

           MV      .L2X    A3,B13
||         LDW     .D2T1   *+SP(4632),A3
|| [ A0]   MVK     .S2     192,B4

   [!A0]   BNOP    .S1     L19,3             ; |142| 
|| [ A0]   MPYLI   .M2X    B4,A13,B5:B4

           ADD     .L1     A3,A14,A12
||         MVKL    .S1     __mpyd,A3         ; |143| 

           MVKH    .S1     __mpyd,A3         ; |143| 
           ; BRANCHCC OCCURS {L19}           ; |142| 
DW$L$_MultMatrix_B_TC_Ex$7$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_B_TC_Ex$8$B:

           ADD     .L2X    A15,B4,B12
||         LDDW    .D1T1   *A12++,A5:A4      ; |143| 

           CALL    .S2X    A3                ; |143| 
||         LDDW    .D2T2   *B12++,B5:B4      ; |143| 

	.dwpsn	"matrix2.c",142,0
           NOP             3
DW$L$_MultMatrix_B_TC_Ex$8$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L18:    
DW$L$_MultMatrix_B_TC_Ex$9$B:
	.dwpsn	"matrix2.c",143,0
           ADDKPC  .S2     RL11,B3,1         ; |143| 
RL11:      ; CALL OCCURS {__mpyd}            ; |143| 
DW$L$_MultMatrix_B_TC_Ex$9$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_B_TC_Ex$10$B:
           MVKL    .S2     __addd,B6         ; |143| 
           MVKH    .S2     __addd,B6         ; |143| 
           CALL    .S2     B6                ; |143| 
           MV      .L2X    A4,B4             ; |143| 
           ADDKPC  .S2     RL12,B3,0         ; |143| 
           MV      .S1     A10,A4            ; |143| 
           MV      .L2X    A5,B5             ; |143| 
           MV      .L1     A11,A5            ; |143| 
RL12:      ; CALL OCCURS {__addd}            ; |143| 
DW$L$_MultMatrix_B_TC_Ex$10$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_B_TC_Ex$11$B:

           SUB     .L2     B13,1,B13         ; |142| 
||         MVKL    .S1     __mpyd,A3         ; |143| 
||         SUB     .L1X    B13,1,A0          ; |142| 
||         MV      .D1     A4,A10            ; |143| 

   [ A0]   BNOP    .S2     L18,1             ; |142| 
||         MV      .L1     A5,A11            ; |143| 
||         MVKH    .S1     __mpyd,A3         ; |143| 
|| [ A0]   LDDW    .D1T1   *A12++,A5:A4      ; |143| 
|| [ A0]   LDDW    .D2T2   *B12++,B5:B4      ; |143| 

   [ A0]   CALL    .S2X    A3                ; |143| 
           NOP             3
           ; BRANCHCC OCCURS {L18}           ; |142| 
DW$L$_MultMatrix_B_TC_Ex$11$E:
;** --------------------------------------------------------------------------*
L19:    
DW$L$_MultMatrix_B_TC_Ex$12$B:

           MV      .L1X    B10,A3
||         MVK     .S1     1152,A4           ; |140| 
||         ADD     .D1     1,A13,A13         ; |140| 

           STDW    .D1T1   A11:A10,*A3++     ; |145| 
           LDW     .D1T1   *+A15[A4],A4      ; |140| 
           NOP             2
           MV      .L2X    A3,B10            ; |145| 
           MVK     .S1     1153,A3           ; |142| 
           CMPGT   .L1     A4,A13,A0         ; |140| 

   [ A0]   BNOP    .S1     L17,4             ; |140| 
|| [ A0]   ZERO    .L1     A11:A10           ; |142| 
|| [ A0]   LDW     .D1T1   *+A14[A3],A3      ; |142| 

	.dwpsn	"matrix2.c",146,0
           CMPGT   .L1     A3,0,A0           ; |142| 
           ; BRANCHCC OCCURS {L17}           ; |140| 
DW$L$_MultMatrix_B_TC_Ex$12$E:
;** --------------------------------------------------------------------------*
L20:    
DW$L$_MultMatrix_B_TC_Ex$13$B:

           MVK     .S1     1152,A3           ; |139| 
||         ADD     .L1X    1,B11,A31         ; |139| 
||         ADD     .L2     1,B11,B11         ; |139| 

           LDW     .D1T1   *+A14[A3],A5      ; |139| 
           NOP             4
           CMPGT   .L1     A5,A31,A0         ; |139| 

   [ A0]   CMPGT   .L1     A4,0,A0           ; |140| 
||         MV      .D1     A0,A1             ; guard predicate rewrite
|| [ A0]   B       .S1     L16               ; |139| 

   [ A1]   LDW     .D2T1   *+SP(4628),A31
|| [ A1]   MVK     .S1     192,A3
|| [!A1]   MVK     .L1     0x1,A0            ; nullify predicate

   [!A0]   B       .S1     L20               ; |140| 
|| [ A0]   MV      .L1X    B11,A4

   [ A0]   MPYLI   .M1     A3,A4,A5:A4
           NOP             2
           ; BRANCHCC OCCURS {L16}           ; |139| 
DW$L$_MultMatrix_B_TC_Ex$13$E:
;** --------------------------------------------------------------------------*
L21:    

           LDW     .D2T1   *+SP(4624),A4     ; |150| 
||         MVKL    .S2     _CopyMatrixEx,B5  ; |150| 
||         MVKL    .S1     _DestroyMatrixEx,A10 ; |151| 
||         ADD     .L2     8,SP,B10          ; |151| 

           LDW     .D2T1   *+SP(4628),A5     ; |150| 
||         MVKH    .S2     _CopyMatrixEx,B5  ; |150| 
||         MVKH    .S1     _DestroyMatrixEx,A10 ; |151| 
||         ADD     .L2     8,SP,B4           ; |150| 

           NOP             4
           CMPEQ   .L1     A4,A5,A0          ; |148| 

   [ A0]   LDW     .D2T2   *+SP(4660),B3     ; |155| 
|| [ A0]   B       .S1     L23               ; |148| 

   [ A0]   LDW     .D2T1   *+SP(4680),A15    ; |155| 
|| [!A0]   CALL    .S2     B5                ; |150| 

   [ A0]   LDW     .D2T1   *+SP(4644),A11    ; |155| 
   [ A0]   LDW     .D2T1   *+SP(4640),A10    ; |155| 
   [ A0]   LDW     .D2T1   *+SP(4652),A13    ; |155| 
   [ A0]   LDW     .D2T1   *+SP(4648),A12    ; |155| 
           ; BRANCHCC OCCURS {L23}           ; |148| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL13,B3,0         ; |150| 
RL13:      ; CALL OCCURS {_CopyMatrixEx}     ; |150| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A10               ; |151| 
           ADDKPC  .S2     RL14,B3,3         ; |151| 
           MV      .L1X    B10,A4            ; |151| 
RL14:      ; CALL OCCURS {_DestroyMatrixEx}  ; |151| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4660),B3     ; |155| 
           LDW     .D2T1   *+SP(4644),A11    ; |155| 
           LDW     .D2T1   *+SP(4640),A10    ; |155| 
           LDW     .D2T1   *+SP(4652),A13    ; |155| 
           LDW     .D2T1   *+SP(4648),A12    ; |155| 
           LDW     .D2T1   *+SP(4656),A14    ; |155| 
           LDW     .D2T2   *+SP(4668),B11    ; |155| 
           LDW     .D2T2   *+SP(4664),B10    ; |155| 
           LDW     .D2T2   *+SP(4676),B13    ; |155| 

           RET     .S2     B3                ; |155| 
||         LDW     .D2T1   *+SP(4680),A15    ; |155| 

           LDW     .D2T2   *+SP(4672),B12    ; |155| 
           NOP             3
           ADDK    .S2     4680,SP           ; |155| 
           ; BRANCH OCCURS {B3}              ; |155| 
;** --------------------------------------------------------------------------*
L22:    
           CALL    .S2     B4                ; |132| 
           ADDKPC  .S2     RL15,B3,4         ; |132| 
RL15:      ; CALL OCCURS {_DestroyMatrixEx}  ; |132| 
           LDW     .D2T1   *+SP(4680),A15    ; |155| 
           LDW     .D2T1   *+SP(4644),A11    ; |155| 
           LDW     .D2T1   *+SP(4640),A10    ; |155| 
           LDW     .D2T1   *+SP(4652),A13    ; |155| 
           LDW     .D2T2   *+SP(4660),B3     ; |155| 
           LDW     .D2T1   *+SP(4648),A12    ; |155| 
;** --------------------------------------------------------------------------*
L23:    
           LDW     .D2T2   *+SP(4672),B12    ; |155| 
           LDW     .D2T2   *+SP(4676),B13    ; |155| 
           LDW     .D2T2   *+SP(4664),B10    ; |155| 

           LDW     .D2T2   *+SP(4668),B11    ; |155| 
||         RET     .S2     B3                ; |155| 

           LDW     .D2T1   *+SP(4656),A14    ; |155| 
||         ADDK    .S2     4680,SP           ; |155| 

	.dwpsn	"matrix2.c",155,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |155| 

DW$41	.dwtag  DW_TAG_loop
	.dwattr DW$41, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L16:1:1472539739")
	.dwattr DW$41, DW_AT_begin_file("matrix2.c")
	.dwattr DW$41, DW_AT_begin_line(0x8b)
	.dwattr DW$41, DW_AT_end_line(0x92)
DW$42	.dwtag  DW_TAG_loop_range
	.dwattr DW$42, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$5$B)
	.dwattr DW$42, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$5$E)
DW$43	.dwtag  DW_TAG_loop_range
	.dwattr DW$43, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$6$B)
	.dwattr DW$43, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$6$E)
DW$44	.dwtag  DW_TAG_loop_range
	.dwattr DW$44, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$13$B)
	.dwattr DW$44, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$13$E)

DW$45	.dwtag  DW_TAG_loop
	.dwattr DW$45, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L17:2:1472539739")
	.dwattr DW$45, DW_AT_begin_file("matrix2.c")
	.dwattr DW$45, DW_AT_begin_line(0x8c)
	.dwattr DW$45, DW_AT_end_line(0x92)
DW$46	.dwtag  DW_TAG_loop_range
	.dwattr DW$46, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$7$B)
	.dwattr DW$46, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$7$E)
DW$47	.dwtag  DW_TAG_loop_range
	.dwattr DW$47, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$8$B)
	.dwattr DW$47, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$8$E)
DW$48	.dwtag  DW_TAG_loop_range
	.dwattr DW$48, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$12$B)
	.dwattr DW$48, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$12$E)

DW$49	.dwtag  DW_TAG_loop
	.dwattr DW$49, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L18:3:1472539739")
	.dwattr DW$49, DW_AT_begin_file("matrix2.c")
	.dwattr DW$49, DW_AT_begin_line(0x8e)
	.dwattr DW$49, DW_AT_end_line(0x8f)
DW$50	.dwtag  DW_TAG_loop_range
	.dwattr DW$50, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$9$B)
	.dwattr DW$50, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$9$E)
DW$51	.dwtag  DW_TAG_loop_range
	.dwattr DW$51, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$10$B)
	.dwattr DW$51, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$10$E)
DW$52	.dwtag  DW_TAG_loop_range
	.dwattr DW$52, DW_AT_low_pc(DW$L$_MultMatrix_B_TC_Ex$11$B)
	.dwattr DW$52, DW_AT_high_pc(DW$L$_MultMatrix_B_TC_Ex$11$E)
	.dwendtag DW$49

	.dwendtag DW$45

	.dwendtag DW$41

	.dwattr DW$37, DW_AT_end_file("matrix2.c")
	.dwattr DW$37, DW_AT_end_line(0x9b)
	.dwattr DW$37, DW_AT_end_column(0x01)
	.dwendtag DW$37

	.sect	".text"
	.global	_MultMatrix_SimResEx

DW$53	.dwtag  DW_TAG_subprogram, DW_AT_name("MultMatrix_SimResEx"), DW_AT_symbol_name("_MultMatrix_SimResEx")
	.dwattr DW$53, DW_AT_low_pc(_MultMatrix_SimResEx)
	.dwattr DW$53, DW_AT_high_pc(0x00)
	.dwattr DW$53, DW_AT_begin_file("matrix2.c")
	.dwattr DW$53, DW_AT_begin_line(0xa5)
	.dwattr DW$53, DW_AT_begin_column(0x06)
	.dwattr DW$53, DW_AT_frame_base[DW_OP_breg31 4688]
	.dwattr DW$53, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",166,1

;******************************************************************************
;* FUNCTION NAME: _MultMatrix_SimResEx                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 4640 Auto + 44 Save = 4684 byte             *
;******************************************************************************
_MultMatrix_SimResEx:
;** --------------------------------------------------------------------------*
DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$54, DW_AT_type(*DW$T$29)
	.dwattr DW$54, DW_AT_location[DW_OP_reg4]
DW$55	.dwtag  DW_TAG_formal_parameter, DW_AT_name("B"), DW_AT_symbol_name("_B")
	.dwattr DW$55, DW_AT_type(*DW$T$29)
	.dwattr DW$55, DW_AT_location[DW_OP_reg20]
DW$56	.dwtag  DW_TAG_formal_parameter, DW_AT_name("C"), DW_AT_symbol_name("_C")
	.dwattr DW$56, DW_AT_type(*DW$T$29)
	.dwattr DW$56, DW_AT_location[DW_OP_reg6]

           MV      .L1X    SP,A31            ; |166| 
||         ADDK    .S2     -4688,SP          ; |166| 
||         MVK     .S1     1153,A3           ; |174| 
||         ZERO    .L2     B7                ; |171| 

           STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B12,*+SP(4680)
||         MV      .L2X    A6,B12            ; |166| 
||         MVK     .S2     1152,B5           ; |174| 
||         CMPEQ   .L1X    A4,B4,A0          ; |180| 
||         MVKL    .S1     _DestroyMatrixEx,A5 ; |176| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B13,*+SP(4684)
||         MV      .L2X    A4,B13            ; |166| 
||         MVKH    .S1     _DestroyMatrixEx,A5 ; |176| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B10,*+SP(4672)
||         CMPEQ   .L2     B13,B12,B0        ; |180| 

           STW     .D2T1   A15,*+SP(4688)
||         MV      .L1X    B4,A15            ; |166| 
||         ZERO    .L2     B4                ; |180| 

           STW     .D2T2   B11,*+SP(4676)
|| [ B0]   MVK     .L2     0x1,B4            ; |180| 

           STW     .D2T2   B3,*+SP(4668)

           LDW     .D2T2   *+B12[B5],B6      ; |174| 
||         LDW     .D1T1   *+A15[A3],A3      ; |174| 
||         MVK     .S2     4620,B5           ; |171| 

           NOP             1
           ADD     .L2     B5,SP,B5          ; |171| 
           STW     .D2T2   B7,*-B5(4)        ; |172| 
           STW     .D2T2   B7,*B5            ; |171| 

           ZERO    .S1     A3                ; |180| 
||         CMPEQ   .L1X    A3,B6,A1          ; |174| 

   [ A1]   MVKL    .S2     _CreateMatrixEx,B5 ; |180| 
|| [ A1]   ADD     .D1X    8,SP,A4           ; |180| 
|| [ A0]   MVK     .L1     0x1,A3            ; |180| 
|| [!A1]   B       .S1     L30               ; |174| 

   [!A1]   CALL    .S2X    A5                ; |176| 
|| [ A1]   STW     .D2T1   A4,*+SP(4624)     ; |180| 

           NOP             3
           OR      .L2X    B4,A3,B0          ; |180| 
           ; BRANCHCC OCCURS {L30}           ; |174| 
;** --------------------------------------------------------------------------*

           MVKH    .S2     _CreateMatrixEx,B5 ; |180| 
|| [!B0]   MV      .L1X    B13,A4            ; |180| 

   [!B0]   STW     .D2T1   A4,*+SP(4624)     ; |180| 
||         MVK     .S2     1153,B6           ; |180| 

           CALL    .S2     B5                ; |180| 
||         MVK     .S1     1152,A3           ; |180| 
||         LDW     .D2T1   *+SP(4624),A4     ; |180| 

           LDW     .D2T1   *+B12[B6],A6      ; |180| 
||         LDW     .D1T2   *+A15[A3],B4      ; |180| 

           ADDKPC  .S2     RL16,B3,3         ; |180| 
RL16:      ; CALL OCCURS {_CreateMatrixEx}   ; |180| 
;** --------------------------------------------------------------------------*

           MVK     .S1     1152,A3           ; |182| 
||         ZERO    .L1     A13               ; |182| 
||         LDW     .D2T1   *+SP(4624),A31

           LDW     .D1T1   *+A15[A3],A3      ; |182| 
           NOP             4

           MVK     .S1     192,A3
||         CMPGT   .L1     A3,0,A0           ; |182| 

   [!A0]   BNOP    .S1     L29,5             ; |182| 
|| [ A0]   CMPLT   .L1     A13,0,A0          ; |183| 
||         MV      .D1     A0,A1             ; guard predicate rewrite
||         MPYLI   .M1     A3,A13,A5:A4

           ; BRANCHCC OCCURS {L29}           ; |182| 
;** --------------------------------------------------------------------------*
	.dwpsn	"matrix2.c",182,0

   [ A0]   BNOP    .S1     L28,2             ; |183| 
||         ADD     .L1     A31,A4,A3
|| [!A0]   ADD     .L2X    1,A13,B4          ; |185| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L24
;** --------------------------------------------------------------------------*
L24:    
DW$L$_MultMatrix_SimResEx$5$B:
	.dwpsn	"matrix2.c",183,0

   [!A0]   STW     .D2T1   A3,*+SP(4632)
||         MVK     .S1     1153,A3           ; |185| 

   [!A0]   STW     .D2T1   A4,*+SP(4636)
   [!A0]   STW     .D2T2   B4,*+SP(4628)
           ; BRANCHCC OCCURS {L28}           ; |183| 
DW$L$_MultMatrix_SimResEx$5$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_SimResEx$6$B:

           LDW     .D1T1   *+A15[A3],A3      ; |185| 
||         ZERO    .L2     B11
||         ZERO    .L1     A11:A10           ; |185| 

           NOP             2
           ADDAD   .D1     A31,A13,A14
DW$L$_MultMatrix_SimResEx$6$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L25
;** --------------------------------------------------------------------------*
L25:    
DW$L$_MultMatrix_SimResEx$7$B:
	.dwpsn	"matrix2.c",184,0
           NOP             1
           CMPGT   .L1     A3,0,A0           ; |185| 

   [ A0]   STW     .D2T1   A3,*+SP(4640)
|| [!A0]   B       .S1     L27               ; |185| 

           LDW     .D2T1   *+SP(4636),A3
           NOP             4
           ; BRANCHCC OCCURS {L27}           ; |185| 
DW$L$_MultMatrix_SimResEx$7$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_SimResEx$8$B:

           ADD     .L1     A3,A15,A12
||         MVKL    .S1     __mpyd,A3         ; |186| 
||         ADD     .L2     B11,B12,B10

           MVKH    .S1     __mpyd,A3         ; |186| 
||         LDDW    .D1T1   *A12++,A5:A4      ; |186| 
||         LDDW    .D2T2   *B10,B5:B4        ; |186| 

           NOP             1
           CALL    .S2X    A3                ; |186| 
	.dwpsn	"matrix2.c",185,0
           NOP             4
DW$L$_MultMatrix_SimResEx$8$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L26:    
DW$L$_MultMatrix_SimResEx$9$B:
	.dwpsn	"matrix2.c",186,0
           ADDKPC  .S2     RL18,B3,0         ; |186| 
RL18:      ; CALL OCCURS {__mpyd}            ; |186| 
DW$L$_MultMatrix_SimResEx$9$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_SimResEx$10$B:
           MVKL    .S2     __addd,B6         ; |186| 
           MVKH    .S2     __addd,B6         ; |186| 
           CALL    .S2     B6                ; |186| 
           MV      .L2X    A4,B4             ; |186| 
           ADDKPC  .S2     RL19,B3,0         ; |186| 
           MV      .S1     A10,A4            ; |186| 
           MV      .L2X    A5,B5             ; |186| 
           MV      .L1     A11,A5            ; |186| 
RL19:      ; CALL OCCURS {__addd}            ; |186| 
DW$L$_MultMatrix_SimResEx$10$E:
;** --------------------------------------------------------------------------*
DW$L$_MultMatrix_SimResEx$11$B:

           LDW     .D2T2   *+SP(4640),B4     ; |186| 
||         MV      .L1     A4,A10            ; |186| 
||         MV      .D1     A5,A11            ; |186| 
||         ADDK    .S2     192,B10           ; |185| 
||         MVKL    .S1     __mpyd,A3         ; |186| 

           NOP             3
           MVKH    .S1     __mpyd,A3         ; |186| 

           SUB     .L2     B4,1,B4           ; |185| 
||         SUB     .L1X    B4,1,A0           ; |185| 

           STW     .D2T2   B4,*+SP(4640)     ; |185| 
|| [ A0]   B       .S1     L26               ; |185| 

   [ A0]   LDDW    .D2T2   *B10,B5:B4        ; |186| 
|| [ A0]   LDDW    .D1T1   *A12++,A5:A4      ; |186| 
|| [ A0]   CALL    .S2X    A3                ; |186| 

           NOP             4
           ; BRANCHCC OCCURS {L26}           ; |185| 
DW$L$_MultMatrix_SimResEx$11$E:
;** --------------------------------------------------------------------------*
L27:    
DW$L$_MultMatrix_SimResEx$12$B:

           LDW     .D2T2   *+SP(4628),B4     ; |187| 
||         STDW    .D1T1   A11:A10,*A14      ; |187| 
||         ADDK    .S1     192,A14           ; |183| 
||         ADD     .L2     8,B11,B11         ; |183| 

           LDW     .D2T1   *+SP(4632),A3     ; |187| 
           NOP             3

           SUB     .L2     B4,1,B4           ; |183| 
||         SUB     .L1X    B4,1,A0           ; |183| 

   [ A0]   ZERO    .L1     A11:A10           ; |185| 
||         STW     .D2T2   B4,*+SP(4628)     ; |187| 
||         STDW    .D1T1   A11:A10,*A3++     ; |187| 
|| [ A0]   B       .S1     L25               ; |183| 

           MVK     .S1     1153,A3           ; |185| 
||         STW     .D2T1   A3,*+SP(4632)     ; |187| 

   [ A0]   LDW     .D1T1   *+A15[A3],A3      ; |185| 
	.dwpsn	"matrix2.c",189,0
           NOP             3
           ; BRANCHCC OCCURS {L25}           ; |183| 
DW$L$_MultMatrix_SimResEx$12$E:
;** --------------------------------------------------------------------------*
L28:    
DW$L$_MultMatrix_SimResEx$13$B:

           MVK     .S1     1152,A3           ; |182| 
||         ADD     .L1     1,A13,A13         ; |182| 
||         LDW     .D2T1   *+SP(4624),A31

           LDW     .D1T1   *+A15[A3],A3      ; |182| 
           NOP             4

           MVK     .S1     192,A3
||         CMPGT   .L1     A3,A13,A0         ; |182| 

           MPYLI   .M1     A3,A13,A5:A4
|| [ A0]   CMPLT   .L1     A13,0,A0          ; |183| 
||         MV      .D1     A0,A1             ; guard predicate rewrite
|| [ A0]   B       .S1     L24               ; |182| 

   [!A1]   ZERO    .L1     A0                ; nullify predicate
   [!A0]   ADD     .L2X    1,A13,B4          ; |185| 
   [ A0]   B       .S1     L28               ; |183| 
           ADD     .L1     A31,A4,A3
           NOP             1
           ; BRANCHCC OCCURS {L24}           ; |182| 
DW$L$_MultMatrix_SimResEx$13$E:
;** --------------------------------------------------------------------------*
L29:    

           LDW     .D2T1   *+SP(4624),A3
||         MVKL    .S2     _CopyMatrixEx,B5  ; |192| 
||         MVKL    .S1     _DestroyMatrixEx,A10 ; |193| 
||         ADD     .L2     8,SP,B10          ; |193| 
||         MV      .L1X    B13,A4            ; |192| 

           NOP             1
           MVKH    .S2     _CopyMatrixEx,B5  ; |192| 
           MVKH    .S1     _DestroyMatrixEx,A10 ; |193| 
           ADD     .L2     8,SP,B4           ; |192| 
           CMPEQ   .L2X    B13,A3,B0         ; |190| 

   [ B0]   LDW     .D2T2   *+SP(4668),B3     ; |197| 
|| [ B0]   B       .S1     L31               ; |190| 

   [ B0]   LDW     .D2T1   *+SP(4688),A15    ; |197| 
|| [!B0]   CALL    .S2     B5                ; |192| 

   [ B0]   LDW     .D2T1   *+SP(4652),A11    ; |197| 
   [ B0]   LDW     .D2T1   *+SP(4648),A10    ; |197| 
   [ B0]   LDW     .D2T1   *+SP(4660),A13    ; |197| 
   [ B0]   LDW     .D2T1   *+SP(4656),A12    ; |197| 
           ; BRANCHCC OCCURS {L31}           ; |190| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL20,B3,0         ; |192| 
RL20:      ; CALL OCCURS {_CopyMatrixEx}     ; |192| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A10               ; |193| 
           ADDKPC  .S2     RL21,B3,3         ; |193| 
           MV      .L1X    B10,A4            ; |193| 
RL21:      ; CALL OCCURS {_DestroyMatrixEx}  ; |193| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4668),B3     ; |197| 
           LDW     .D2T1   *+SP(4652),A11    ; |197| 
           LDW     .D2T1   *+SP(4648),A10    ; |197| 
           LDW     .D2T1   *+SP(4660),A13    ; |197| 
           LDW     .D2T1   *+SP(4656),A12    ; |197| 
           LDW     .D2T1   *+SP(4664),A14    ; |197| 
           LDW     .D2T2   *+SP(4676),B11    ; |197| 
           LDW     .D2T2   *+SP(4672),B10    ; |197| 
           LDW     .D2T2   *+SP(4684),B13    ; |197| 

           RET     .S2     B3                ; |197| 
||         LDW     .D2T1   *+SP(4688),A15    ; |197| 

           LDW     .D2T2   *+SP(4680),B12    ; |197| 
           NOP             3
           ADDK    .S2     4688,SP           ; |197| 
           ; BRANCH OCCURS {B3}              ; |197| 
;** --------------------------------------------------------------------------*
L30:    
           ADDKPC  .S2     RL22,B3,0         ; |176| 
RL22:      ; CALL OCCURS {_DestroyMatrixEx}  ; |176| 
           LDW     .D2T2   *+SP(4668),B3     ; |197| 
           LDW     .D2T1   *+SP(4688),A15    ; |197| 
           LDW     .D2T1   *+SP(4660),A13    ; |197| 
           LDW     .D2T1   *+SP(4656),A12    ; |197| 
           LDW     .D2T1   *+SP(4652),A11    ; |197| 
           LDW     .D2T1   *+SP(4648),A10    ; |197| 
;** --------------------------------------------------------------------------*
L31:    
           LDW     .D2T2   *+SP(4680),B12    ; |197| 
           LDW     .D2T2   *+SP(4684),B13    ; |197| 
           LDW     .D2T2   *+SP(4672),B10    ; |197| 

           LDW     .D2T2   *+SP(4676),B11    ; |197| 
||         RET     .S2     B3                ; |197| 

           LDW     .D2T1   *+SP(4664),A14    ; |197| 
||         ADDK    .S2     4688,SP           ; |197| 

	.dwpsn	"matrix2.c",197,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |197| 

DW$57	.dwtag  DW_TAG_loop
	.dwattr DW$57, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L24:1:1472539739")
	.dwattr DW$57, DW_AT_begin_file("matrix2.c")
	.dwattr DW$57, DW_AT_begin_line(0xb6)
	.dwattr DW$57, DW_AT_end_line(0xbd)
DW$58	.dwtag  DW_TAG_loop_range
	.dwattr DW$58, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$5$B)
	.dwattr DW$58, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$5$E)
DW$59	.dwtag  DW_TAG_loop_range
	.dwattr DW$59, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$6$B)
	.dwattr DW$59, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$6$E)
DW$60	.dwtag  DW_TAG_loop_range
	.dwattr DW$60, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$13$B)
	.dwattr DW$60, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$13$E)

DW$61	.dwtag  DW_TAG_loop
	.dwattr DW$61, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L25:2:1472539739")
	.dwattr DW$61, DW_AT_begin_file("matrix2.c")
	.dwattr DW$61, DW_AT_begin_line(0xb7)
	.dwattr DW$61, DW_AT_end_line(0xbd)
DW$62	.dwtag  DW_TAG_loop_range
	.dwattr DW$62, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$7$B)
	.dwattr DW$62, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$7$E)
DW$63	.dwtag  DW_TAG_loop_range
	.dwattr DW$63, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$8$B)
	.dwattr DW$63, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$8$E)
DW$64	.dwtag  DW_TAG_loop_range
	.dwattr DW$64, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$12$B)
	.dwattr DW$64, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$12$E)

DW$65	.dwtag  DW_TAG_loop
	.dwattr DW$65, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L26:3:1472539739")
	.dwattr DW$65, DW_AT_begin_file("matrix2.c")
	.dwattr DW$65, DW_AT_begin_line(0xb9)
	.dwattr DW$65, DW_AT_end_line(0xba)
DW$66	.dwtag  DW_TAG_loop_range
	.dwattr DW$66, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$9$B)
	.dwattr DW$66, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$9$E)
DW$67	.dwtag  DW_TAG_loop_range
	.dwattr DW$67, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$10$B)
	.dwattr DW$67, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$10$E)
DW$68	.dwtag  DW_TAG_loop_range
	.dwattr DW$68, DW_AT_low_pc(DW$L$_MultMatrix_SimResEx$11$B)
	.dwattr DW$68, DW_AT_high_pc(DW$L$_MultMatrix_SimResEx$11$E)
	.dwendtag DW$65

	.dwendtag DW$61

	.dwendtag DW$57

	.dwattr DW$53, DW_AT_end_file("matrix2.c")
	.dwattr DW$53, DW_AT_end_line(0xc5)
	.dwattr DW$53, DW_AT_end_column(0x01)
	.dwendtag DW$53

	.sect	".text"
	.global	_InvSymmetricalMatrixEx

DW$69	.dwtag  DW_TAG_subprogram, DW_AT_name("InvSymmetricalMatrixEx"), DW_AT_symbol_name("_InvSymmetricalMatrixEx")
	.dwattr DW$69, DW_AT_low_pc(_InvSymmetricalMatrixEx)
	.dwattr DW$69, DW_AT_high_pc(0x00)
	.dwattr DW$69, DW_AT_begin_file("matrix2.c")
	.dwattr DW$69, DW_AT_begin_line(0xcd)
	.dwattr DW$69, DW_AT_begin_column(0x06)
	.dwattr DW$69, DW_AT_frame_base[DW_OP_breg31 13920]
	.dwattr DW$69, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",206,1

;******************************************************************************
;* FUNCTION NAME: _InvSymmetricalMatrixEx                                     *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 13876 Auto + 44 Save = 13920 byte           *
;******************************************************************************
_InvSymmetricalMatrixEx:
;** --------------------------------------------------------------------------*
DW$70	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$70, DW_AT_type(*DW$T$29)
	.dwattr DW$70, DW_AT_location[DW_OP_reg4]
DW$71	.dwtag  DW_TAG_formal_parameter, DW_AT_name("B"), DW_AT_symbol_name("_B")
	.dwattr DW$71, DW_AT_type(*DW$T$29)
	.dwattr DW$71, DW_AT_location[DW_OP_reg20]

           MV      .L1X    SP,A31            ; |206| 
||         ADDK    .S2     -13920,SP         ; |206| 
||         MVK     .S1     1153,A5           ; |210| 
||         CMPEQ   .L2X    A4,B4,B0          ; |216| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(13900)
||         MV      .L1X    B4,A10            ; |206| 
||         MVK     .S1     1152,A3           ; |210| 
||         ADD     .L2     8,SP,B4           ; |216| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B11,*+SP(13908)

           STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B10,*+SP(13904)

           STW     .D2T2   B13,*+SP(13916)
           STW     .D2T2   B12,*+SP(13912)

           STW     .D2T1   A15,*+SP(13920)
||         MV      .L1     A4,A15            ; |206| 

           LDW     .D1T1   *+A10[A5],A6      ; |210| 
           LDW     .D1T1   *+A10[A3],A3      ; |210| 
           NOP             3
           CMPEQ   .L2X    A6,0,B5           ; |210| 
           CMPEQ   .L1     A6,A3,A5          ; |210| 
           XOR     .L1     1,A5,A5           ; |210| 
           NOP             1

           MVKL    .S2     _CreateMatrixEx,B5 ; |216| 
||         OR      .L2X    A5,B5,B1          ; |210| 

   [ B1]   BNOP    .S1     L37,5             ; |210| 
|| [!B1]   STW     .D2T2   B4,*+SP(13856)    ; |216| 
||         MVKH    .S2     _CreateMatrixEx,B5 ; |216| 

           ; BRANCHCC OCCURS {L37}           ; |210| 
;** --------------------------------------------------------------------------*

           CALL    .S2     B5                ; |216| 
|| [!B0]   STW     .D2T1   A4,*+SP(13856)    ; |216| 

           LDW     .D2T1   *+SP(13856),A4    ; |216| 
           ADDKPC  .S2     RL23,B3,2         ; |216| 
           MV      .L2X    A3,B4             ; |216| 
RL23:      ; CALL OCCURS {_CreateMatrixEx}   ; |216| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _QR_DecompositionEx,A3 ; |221| 
           MVKH    .S1     _QR_DecompositionEx,A3 ; |221| 
           MVK     .S1     9240,A5           ; |221| 
           CALL    .S2X    A3                ; |221| 
           MVK     .S2     4624,B4           ; |221| 
           ADDKPC  .S2     RL24,B3,0         ; |221| 
           ADD     .L1X    A5,SP,A6          ; |221| 
           ADD     .L2     B4,SP,B4          ; |221| 
           MV      .S1     A10,A4            ; |221| 
RL24:      ; CALL OCCURS {_QR_DecompositionEx}  ; |221| 
           MVKL    .S1     _InvUpTriangularMatrixEx,A3 ; |224| 
           MVKH    .S1     _InvUpTriangularMatrixEx,A3 ; |224| 
           MVK     .S2     9240,B4           ; |224| 
           CALL    .S2X    A3                ; |224| 
           MVK     .S1     9240,A4           ; |224| 
           ADDKPC  .S2     RL25,B3,1         ; |224| 
           ADD     .L2     B4,SP,B4          ; |224| 
           ADD     .L1X    A4,SP,A4          ; |224| 
RL25:      ; CALL OCCURS {_InvUpTriangularMatrixEx}  ; |224| 
;** --------------------------------------------------------------------------*
           MVK     .S2     13852,B4          ; |225| 
           ADD     .L2     B4,SP,B4          ; |225| 
           LDW     .D2T2   *B4,B0            ; |225| 
           NOP             4

   [!B0]   BNOP    .S1     L37,4             ; |225| 
|| [ B0]   LDW     .D2T2   *-B4(4),B4        ; |233| 

           CMPGT   .L2     B4,0,B1           ; |233| 
           ; BRANCHCC OCCURS {L37}           ; |225| 
;** --------------------------------------------------------------------------*

   [!B1]   B       .S1     L36               ; |233| 
||         STW     .D2T2   B4,*+SP(13864)    ; |234| 
||         ZERO    .L2     B10               ; |233| 

           STW     .D2T2   B0,*+SP(13860)
           LDW     .D2T2   *+SP(13860),B4
           NOP             3
           ; BRANCHCC OCCURS {L36}           ; |233| 
;** --------------------------------------------------------------------------*
	.dwpsn	"matrix2.c",233,0
           NOP             1
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L32
;** --------------------------------------------------------------------------*
L32:    
DW$L$_InvSymmetricalMatrixEx$7$B:
	.dwpsn	"matrix2.c",234,0

           LDW     .D2T2   *+SP(13856),B5
||         CMPLT   .L2     B10,B4,B0         ; |234| 
||         MVK     .S2     192,B4

   [!B0]   BNOP    .S1     L35,3             ; |234| 
||         MPYLI   .M2     B4,B10,B13:B12
|| [ B0]   LDW     .D2T2   *+SP(13860),B31

           ADD     .L2     B5,B12,B30

           ADDAD   .D2     B30,B10,B4
||         MV      .L1X    B12,A3

           ; BRANCHCC OCCURS {L35}           ; |234| 
DW$L$_InvSymmetricalMatrixEx$7$E:
;** --------------------------------------------------------------------------*
DW$L$_InvSymmetricalMatrixEx$8$B:

           STW     .D2T1   A3,*+SP(13872)
||         SUB     .L2     B31,B10,B13
||         ZERO    .L1     A13:A12

           MV      .L1X    B4,A14
||         STW     .D2T2   B13,*+SP(13868)   ; |236| 

           MV      .L1X    B4,A10
||         LDW     .D2T2   *+SP(13872),B4

           NOP             4
DW$L$_InvSymmetricalMatrixEx$8$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L33
;** --------------------------------------------------------------------------*
L33:    
DW$L$_InvSymmetricalMatrixEx$9$B:
	.dwpsn	"matrix2.c",235,0

           MVK     .S1     9240,A3
||         ADDAD   .D2     B4,B10,B5
||         MVK     .S2     4624,B31
||         STDW    .D1T1   A13:A12,*A10      ; |236| 

           ADD     .L1X    A3,SP,A3
||         ADD     .L2     B31,SP,B6
||         ADDAD   .D2     B12,B10,B30

           ADD     .L1X    A3,B5,A11
||         MVKL    .S1     __mpyd,A3         ; |238| 
||         ADD     .L2     B6,B30,B11
||         STW     .D2T2   B13,*+SP(13876)   ; |238| 

           MVKH    .S1     __mpyd,A3         ; |238| 
||         LDDW    .D2T2   *B11++,B5:B4      ; |238| 
||         LDDW    .D1T1   *A11++,A5:A4      ; |238| 

           NOP             1
           CALL    .S2X    A3                ; |238| 
	.dwpsn	"matrix2.c",237,0
           NOP             4
DW$L$_InvSymmetricalMatrixEx$9$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L34:    
DW$L$_InvSymmetricalMatrixEx$10$B:
	.dwpsn	"matrix2.c",238,0
           ADDKPC  .S2     RL27,B3,0         ; |238| 
RL27:      ; CALL OCCURS {__mpyd}            ; |238| 
DW$L$_InvSymmetricalMatrixEx$10$E:
;** --------------------------------------------------------------------------*
DW$L$_InvSymmetricalMatrixEx$11$B:
           MVKL    .S2     __addd,B6         ; |238| 
           MVKH    .S2     __addd,B6         ; |238| 

           CALL    .S2     B6                ; |238| 
||         LDDW    .D1T1   *A10,A7:A6        ; |238| 

           MV      .L2X    A4,B4             ; |238| 
           ADDKPC  .S2     RL28,B3,1         ; |238| 
           MV      .L2X    A5,B5             ; |238| 

           MV      .L1     A7,A5             ; |238| 
||         MV      .S1     A6,A4             ; |238| 

RL28:      ; CALL OCCURS {__addd}            ; |238| 
DW$L$_InvSymmetricalMatrixEx$11$E:
;** --------------------------------------------------------------------------*
DW$L$_InvSymmetricalMatrixEx$12$B:

           LDW     .D2T2   *+SP(13876),B4    ; |238| 
||         MVKL    .S1     __mpyd,A3         ; |238| 
||         STDW    .D1T1   A5:A4,*A10        ; |238| 

           NOP             3
           MVKH    .S1     __mpyd,A3         ; |238| 

           SUB     .L2     B4,1,B4           ; |237| 
||         SUB     .L1X    B4,1,A0           ; |237| 

           STW     .D2T2   B4,*+SP(13876)    ; |238| 
|| [ A0]   B       .S1     L34               ; |237| 

   [!A0]   LDW     .D2T2   *+SP(13868),B4
|| [!A0]   LDDW    .D1T1   *A10++,A5:A4      ; |240| 
|| [ A0]   CALL    .S2X    A3                ; |238| 

   [ A0]   LDDW    .D2T2   *B11++,B5:B4      ; |238| 
|| [ A0]   LDDW    .D1T1   *A11++,A5:A4      ; |238| 

           NOP             3
           ; BRANCHCC OCCURS {L34}           ; |237| 
DW$L$_InvSymmetricalMatrixEx$12$E:
;** --------------------------------------------------------------------------*
DW$L$_InvSymmetricalMatrixEx$13$B:

           SUB     .L1X    B4,1,A0           ; |234| 
||         SUB     .L2     B4,1,B4           ; |234| 
||         ADDK    .S2     192,B12           ; |234| 
||         STDW    .D1T1   A5:A4,*A14        ; |240| 
||         ADDK    .S1     192,A14           ; |234| 

   [ A0]   B       .S1     L33               ; |234| 
||         STW     .D2T2   B4,*+SP(13868)    ; |234| 

           LDW     .D2T2   *+SP(13872),B4
	.dwpsn	"matrix2.c",241,0
           NOP             4
           ; BRANCHCC OCCURS {L33}           ; |234| 
DW$L$_InvSymmetricalMatrixEx$13$E:
;** --------------------------------------------------------------------------*
L35:    
DW$L$_InvSymmetricalMatrixEx$14$B:

           LDW     .D2T2   *+SP(13864),B4
||         ADD     .L2     1,B10,B10         ; |233| 

           NOP             4

           SUB     .L2     B4,1,B4           ; |233| 
||         SUB     .L1X    B4,1,A0           ; |233| 

           STW     .D2T2   B4,*+SP(13864)    ; |233| 
|| [ A0]   B       .S1     L32               ; |233| 

           LDW     .D2T2   *+SP(13860),B4
           NOP             4
           ; BRANCHCC OCCURS {L32}           ; |233| 
DW$L$_InvSymmetricalMatrixEx$14$E:
;** --------------------------------------------------------------------------*
L36:    

           LDW     .D2T1   *+SP(13856),A3    ; |245| 
||         MVKL    .S2     _CopyMatrixEx,B5  ; |245| 
||         MVKL    .S1     _DestroyMatrixEx,A10 ; |246| 
||         MV      .L1     A15,A4            ; |245| 
||         ADD     .L2     8,SP,B10          ; |246| 

           NOP             1
           MVKH    .S2     _CopyMatrixEx,B5  ; |245| 
           ADD     .L2     8,SP,B4           ; |245| 
           MVKH    .S1     _DestroyMatrixEx,A10 ; |246| 
           CMPEQ   .L1     A3,A15,A0         ; |243| 

   [ A0]   LDW     .D2T2   *+SP(13900),B3    ; |250| 
|| [ A0]   B       .S1     L38               ; |243| 

   [ A0]   LDW     .D2T1   *+SP(13920),A15   ; |250| 
|| [!A0]   CALL    .S2     B5                ; |245| 

   [ A0]   LDW     .D2T1   *+SP(13884),A11   ; |250| 
   [ A0]   LDW     .D2T1   *+SP(13880),A10   ; |250| 
   [ A0]   LDW     .D2T1   *+SP(13892),A13   ; |250| 
   [ A0]   LDW     .D2T1   *+SP(13888),A12   ; |250| 
           ; BRANCHCC OCCURS {L38}           ; |243| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL29,B3,0         ; |245| 
RL29:      ; CALL OCCURS {_CopyMatrixEx}     ; |245| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A10               ; |246| 
           ADDKPC  .S2     RL30,B3,3         ; |246| 
           MV      .L1X    B10,A4            ; |246| 
RL30:      ; CALL OCCURS {_DestroyMatrixEx}  ; |246| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(13900),B3    ; |250| 
           LDW     .D2T1   *+SP(13884),A11   ; |250| 
           LDW     .D2T1   *+SP(13880),A10   ; |250| 
           LDW     .D2T1   *+SP(13892),A13   ; |250| 
           LDW     .D2T1   *+SP(13888),A12   ; |250| 
           LDW     .D2T1   *+SP(13896),A14   ; |250| 
           LDW     .D2T2   *+SP(13908),B11   ; |250| 
           LDW     .D2T2   *+SP(13904),B10   ; |250| 
           LDW     .D2T2   *+SP(13916),B13   ; |250| 

           RET     .S2     B3                ; |250| 
||         LDW     .D2T1   *+SP(13920),A15   ; |250| 

           LDW     .D2T2   *+SP(13912),B12   ; |250| 
           NOP             3
           ADDK    .S2     13920,SP          ; |250| 
           ; BRANCH OCCURS {B3}              ; |250| 
;** --------------------------------------------------------------------------*
L37:    
           MVKL    .S1     _DestroyMatrixEx,A3 ; |212| 
           MVKH    .S1     _DestroyMatrixEx,A3 ; |212| 
           MV      .L1     A15,A4            ; |212| 
           CALL    .S2X    A3                ; |212| 
           ADDKPC  .S2     RL31,B3,4         ; |212| 
RL31:      ; CALL OCCURS {_DestroyMatrixEx}  ; |212| 
           LDW     .D2T1   *+SP(13884),A11   ; |250| 
           LDW     .D2T1   *+SP(13880),A10   ; |250| 
           LDW     .D2T1   *+SP(13892),A13   ; |250| 
           LDW     .D2T1   *+SP(13888),A12   ; |250| 
           LDW     .D2T2   *+SP(13900),B3    ; |250| 
           LDW     .D2T1   *+SP(13920),A15   ; |250| 
;** --------------------------------------------------------------------------*
L38:    
           LDW     .D2T2   *+SP(13912),B12   ; |250| 
           LDW     .D2T2   *+SP(13916),B13   ; |250| 
           LDW     .D2T2   *+SP(13904),B10   ; |250| 

           LDW     .D2T2   *+SP(13908),B11   ; |250| 
||         RET     .S2     B3                ; |250| 

           LDW     .D2T1   *+SP(13896),A14   ; |250| 
||         ADDK    .S2     13920,SP          ; |250| 

	.dwpsn	"matrix2.c",250,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |250| 

DW$72	.dwtag  DW_TAG_loop
	.dwattr DW$72, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L32:1:1472539739")
	.dwattr DW$72, DW_AT_begin_file("matrix2.c")
	.dwattr DW$72, DW_AT_begin_line(0xe9)
	.dwattr DW$72, DW_AT_end_line(0xf1)
DW$73	.dwtag  DW_TAG_loop_range
	.dwattr DW$73, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$7$B)
	.dwattr DW$73, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$7$E)
DW$74	.dwtag  DW_TAG_loop_range
	.dwattr DW$74, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$8$B)
	.dwattr DW$74, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$8$E)
DW$75	.dwtag  DW_TAG_loop_range
	.dwattr DW$75, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$14$B)
	.dwattr DW$75, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$14$E)

DW$76	.dwtag  DW_TAG_loop
	.dwattr DW$76, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L33:2:1472539739")
	.dwattr DW$76, DW_AT_begin_file("matrix2.c")
	.dwattr DW$76, DW_AT_begin_line(0xea)
	.dwattr DW$76, DW_AT_end_line(0xf1)
DW$77	.dwtag  DW_TAG_loop_range
	.dwattr DW$77, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$9$B)
	.dwattr DW$77, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$9$E)
DW$78	.dwtag  DW_TAG_loop_range
	.dwattr DW$78, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$13$B)
	.dwattr DW$78, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$13$E)

DW$79	.dwtag  DW_TAG_loop
	.dwattr DW$79, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L34:3:1472539739")
	.dwattr DW$79, DW_AT_begin_file("matrix2.c")
	.dwattr DW$79, DW_AT_begin_line(0xed)
	.dwattr DW$79, DW_AT_end_line(0xee)
DW$80	.dwtag  DW_TAG_loop_range
	.dwattr DW$80, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$10$B)
	.dwattr DW$80, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$10$E)
DW$81	.dwtag  DW_TAG_loop_range
	.dwattr DW$81, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$11$B)
	.dwattr DW$81, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$11$E)
DW$82	.dwtag  DW_TAG_loop_range
	.dwattr DW$82, DW_AT_low_pc(DW$L$_InvSymmetricalMatrixEx$12$B)
	.dwattr DW$82, DW_AT_high_pc(DW$L$_InvSymmetricalMatrixEx$12$E)
	.dwendtag DW$79

	.dwendtag DW$76

	.dwendtag DW$72

	.dwattr DW$69, DW_AT_end_file("matrix2.c")
	.dwattr DW$69, DW_AT_end_line(0xfa)
	.dwattr DW$69, DW_AT_end_column(0x01)
	.dwendtag DW$69

	.sect	".text"
	.global	_InvUpTriangularMatrixEx

DW$83	.dwtag  DW_TAG_subprogram, DW_AT_name("InvUpTriangularMatrixEx"), DW_AT_symbol_name("_InvUpTriangularMatrixEx")
	.dwattr DW$83, DW_AT_low_pc(_InvUpTriangularMatrixEx)
	.dwattr DW$83, DW_AT_high_pc(0x00)
	.dwattr DW$83, DW_AT_begin_file("matrix2.c")
	.dwattr DW$83, DW_AT_begin_line(0x103)
	.dwattr DW$83, DW_AT_begin_column(0x06)
	.dwattr DW$83, DW_AT_frame_base[DW_OP_breg31 4688]
	.dwattr DW$83, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",260,1

;******************************************************************************
;* FUNCTION NAME: _InvUpTriangularMatrixEx                                    *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 4644 Auto + 44 Save = 4688 byte             *
;******************************************************************************
_InvUpTriangularMatrixEx:
;** --------------------------------------------------------------------------*
DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$84, DW_AT_type(*DW$T$29)
	.dwattr DW$84, DW_AT_location[DW_OP_reg4]
DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("B"), DW_AT_symbol_name("_B")
	.dwattr DW$85, DW_AT_type(*DW$T$29)
	.dwattr DW$85, DW_AT_location[DW_OP_reg20]

           MV      .L1X    SP,A31            ; |260| 
||         ADDK    .S2     -4688,SP          ; |260| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B4,*+SP(4624)     ; |260| 
||         MVK     .S2     1152,B5           ; |264| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B3,*+SP(4668)

           STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B11,*+SP(4676)

           STW     .D2T2   B10,*+SP(4672)
           STW     .D2T2   B13,*+SP(4684)
           STW     .D2T2   B12,*+SP(4680)
           STW     .D2T1   A15,*+SP(4688)

           LDW     .D2T2   *+B4[B5],B11      ; |264| 
||         MVK     .S2     1153,B5           ; |264| 

           LDW     .D2T2   *+B4[B5],B10      ; |264| 
           NOP             3
           STW     .D2T1   A4,*+SP(4628)     ; |260| 

           CMPEQ   .L1X    B10,0,A3          ; |264| 
||         CMPEQ   .L2     B10,B11,B4        ; |264| 

           CMPGT   .L2     B10,0,B0          ; |271| 
||         XOR     .S2     1,B4,B4           ; |264| 

           NOP             1
           OR      .L1X    B4,A3,A0          ; |264| 

   [ A0]   B       .S1     L46               ; |264| 
|| [ A0]   MVK     .L2     0x1,B0            ; |271| nullify predicate
|| [!A0]   LDW     .D2T1   *+SP(4624),A10
|| [!A0]   MV      .L1X    B10,A11           ; |272| 

   [!B0]   BNOP    .S1     L40,3             ; |271| 
   [!A0]   ADDK    .S1     -200,A10          ; |271| 
           ; BRANCHCC OCCURS {L46}           ; |264| 
;** --------------------------------------------------------------------------*
   [ B0]   MVKL    .S2     __cmpd,B6         ; |272| 
           ; BRANCHCC OCCURS {L40}           ; |271| 
;** --------------------------------------------------------------------------*

           MVKH    .S2     __cmpd,B6         ; |272| 
||         LDDW    .D1T1   *+A10(200),A5:A4  ; |272| 

           CALL    .S2     B6                ; |272| 
	.dwpsn	"matrix2.c",271,0
           NOP             4
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L39:    
DW$L$_InvUpTriangularMatrixEx$4$B:
	.dwpsn	"matrix2.c",272,0

           ZERO    .L2     B5:B4             ; |272| 
||         ADDKPC  .S2     RL32,B3,0         ; |272| 

RL32:      ; CALL OCCURS {__cmpd}            ; |272| 
DW$L$_InvUpTriangularMatrixEx$4$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$5$B:

           ZERO    .L2     B4                ; |272| 
||         CMPEQ   .L1     A4,0,A0           ; |272| 
||         SUB     .D1     A11,1,A11         ; |271| 
||         ADDK    .S1     200,A10           ; |271| 
||         MVKL    .S2     __cmpd,B6         ; |272| 

   [ A0]   MVK     .L2     0x1,B4            ; |272| 
||         MVKH    .S2     __cmpd,B6         ; |272| 

           SUB     .L2     B4,1,B0           ; |272| 
           AND     .L2X    B0,A11,B1         ; |271| 

   [ B1]   B       .S1     L39               ; |271| 
|| [ B1]   LDDW    .D1T1   *+A10(200),A5:A4  ; |272| 

   [ B1]   CALL    .S2     B6                ; |272| 
	.dwpsn	"matrix2.c",276,0
           NOP             4
           ; BRANCHCC OCCURS {L39}           ; |271| 
DW$L$_InvUpTriangularMatrixEx$5$E:
;** --------------------------------------------------------------------------*
   [!B0]   BNOP    .S1     L46,5
           ; BRANCHCC OCCURS {L46} 
;** --------------------------------------------------------------------------*
L40:    
           LDW     .D2T1   *+SP(4628),A3
           LDW     .D2T2   *+SP(4624),B12
           NOP             1
           ADD     .L1X    8,SP,A14          ; |278| 
           MV      .S1X    B10,A6            ; |278| 
           MV      .L2     B11,B4            ; |278| 

           MVKL    .S1     _CreateMatrixEx,A3 ; |278| 
||         CMPEQ   .L2X    A3,B12,B0         ; |278| 

           MVKH    .S1     _CreateMatrixEx,A3 ; |278| 
   [!B0]   LDW     .D2T1   *+SP(4628),A14    ; |278| 
           CALL    .S2X    A3                ; |278| 
           ADDKPC  .S2     RL33,B3,3         ; |278| 
           MV      .L1     A14,A4            ; |278| 
RL33:      ; CALL OCCURS {_CreateMatrixEx}   ; |278| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __divd,B6         ; |281| 
           MVKH    .S2     __divd,B6         ; |281| 

           CALL    .S2     B6                ; |281| 
||         MV      .L2     B12,B10           ; |278| 

           LDDW    .D2T2   *B10,B5:B4        ; |281| 
           ZERO    .L1     A5                ; |281| 
           ADDKPC  .S2     RL34,B3,0         ; |281| 
           MVKH    .S1     0x3ff00000,A5     ; |281| 
           ZERO    .L1     A4                ; |281| 
RL34:      ; CALL OCCURS {__divd}            ; |281| 
;** --------------------------------------------------------------------------*

           MVK     .S2     1153,B4           ; |283| 
||         STDW    .D1T1   A5:A4,*A14        ; |281| 
||         MVK     .L2     0x8,B31
||         ADDAD   .D2     B10,25,B5
||         ZERO    .L1     A13:A12

           LDW     .D2T2   *+B10[B4],B4      ; |283| 
||         MVK     .L2     0x1,B10           ; |283| 
||         ADDAD   .D1     A14,25,A15

           NOP             2
           STW     .D2T2   B31,*+SP(4636)
           STW     .D2T2   B5,*+SP(4632)
           CMPLT   .L2     B4,2,B0           ; |283| 

   [ B0]   BNOP    .S1     L45,4             ; |283| 
|| [!B0]   MVKL    .S2     __divd,B6         ; |285| 
|| [!B0]   LDW     .D2T2   *+SP(4632),B4

   [!B0]   MVKH    .S2     __divd,B6         ; |285| 
           ; BRANCHCC OCCURS {L45}           ; |283| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L41
;** --------------------------------------------------------------------------*
L41:    
DW$L$_InvUpTriangularMatrixEx$10$B:
	.dwpsn	"matrix2.c",284,0
           ZERO    .L1     A5                ; |285| 

           CALL    .S2     B6                ; |285| 
||         MVKH    .S1     0x3ff00000,A5     ; |285| 

           LDDW    .D2T2   *B4,B5:B4         ; |285| 
           ADDKPC  .S2     RL35,B3,2         ; |285| 
           ZERO    .L1     A4                ; |285| 
RL35:      ; CALL OCCURS {__divd}            ; |285| 
DW$L$_InvUpTriangularMatrixEx$10$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$11$B:

           LDW     .D2T1   *+SP(4636),A3     ; |285| 
||         ZERO    .L2     B12               ; |286| 

           CMPLT   .L2     B12,B10,B0        ; |289| 

   [!B0]   B       .S1     L44               ; |289| 
||         LDW     .D2T2   *+SP(4636),B6
||         MVK     .S2     192,B4

           MPYLI   .M2     B4,B12,B5:B4
|| [ B0]   LDW     .D2T2   *+SP(4624),B30

           STW     .D2T2   B10,*+SP(4640)    ; |288| 

           ADD     .L1     A3,A14,A10
||         ZERO    .L2     B13
|| [ B0]   MVKL    .S1     __mpyd,A3         ; |290| 

	.dwpsn	"matrix2.c",286,0

   [ B0]   MVKH    .S1     __mpyd,A3         ; |290| 
|| [ B0]   ADDAD   .D2     B13,B12,B31
||         STDW    .D1T1   A5:A4,*A15        ; |285| 

DW$L$_InvUpTriangularMatrixEx$11$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L42
;** --------------------------------------------------------------------------*
L42:    
DW$L$_InvUpTriangularMatrixEx$12$B:
	.dwpsn	"matrix2.c",287,0

           STDW    .D1T1   A13:A12,*A10      ; |288| 
|| [ B0]   SUB     .L2     B10,B12,B29
||         ADD     .S2     B6,B4,B4

           ; BRANCHCC OCCURS {L44}           ; |289| 
DW$L$_InvUpTriangularMatrixEx$12$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$13$B:

           STW     .D2T2   B29,*+SP(4644)
||         ADD     .L1X    A14,B31,A11
||         ADD     .L2     B30,B4,B11
||         CALL    .S2X    A3                ; |290| 

           LDDW    .D1T1   *A11++,A5:A4      ; |290| 
||         LDDW    .D2T2   *B11,B5:B4        ; |290| 

	.dwpsn	"matrix2.c",289,0
           NOP             3
DW$L$_InvUpTriangularMatrixEx$13$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L43:    
DW$L$_InvUpTriangularMatrixEx$14$B:
	.dwpsn	"matrix2.c",290,0
           ADDKPC  .S2     RL37,B3,0         ; |290| 
RL37:      ; CALL OCCURS {__mpyd}            ; |290| 
DW$L$_InvUpTriangularMatrixEx$14$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$15$B:
           MVKL    .S2     __addd,B6         ; |290| 
           MVKH    .S2     __addd,B6         ; |290| 

           CALL    .S2     B6                ; |290| 
||         LDDW    .D1T1   *A10,A7:A6        ; |290| 

           MV      .L2X    A4,B4             ; |290| 
           ADDKPC  .S2     RL38,B3,1         ; |290| 
           MV      .L2X    A5,B5             ; |290| 

           MV      .L1     A7,A5             ; |290| 
||         MV      .S1     A6,A4             ; |290| 

RL38:      ; CALL OCCURS {__addd}            ; |290| 
DW$L$_InvUpTriangularMatrixEx$15$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$16$B:

           LDW     .D2T2   *+SP(4644),B4     ; |290| 
||         ADDK    .S2     192,B11           ; |289| 
||         STDW    .D1T1   A5:A4,*A10        ; |290| 
||         MVKL    .S1     __mpyd,A3         ; |290| 

           NOP             3
           MVKH    .S1     __mpyd,A3         ; |290| 

           SUB     .L2     B4,1,B4           ; |289| 
||         SUB     .L1X    B4,1,A0           ; |289| 

           STW     .D2T2   B4,*+SP(4644)     ; |290| 
|| [ A0]   B       .S1     L43               ; |289| 

   [ A0]   LDDW    .D2T2   *B11,B5:B4        ; |290| 
|| [ A0]   LDDW    .D1T1   *A11++,A5:A4      ; |290| 
|| [ A0]   CALL    .S2X    A3                ; |290| 

           NOP             4
           ; BRANCHCC OCCURS {L43}           ; |289| 
DW$L$_InvUpTriangularMatrixEx$16$E:
;** --------------------------------------------------------------------------*
L44:    
DW$L$_InvUpTriangularMatrixEx$17$B:
           MVKL    .S1     __negd,A3         ; |292| 
           MVKH    .S1     __negd,A3         ; |292| 
           LDDW    .D1T1   *A15,A5:A4        ; |292| 
           CALL    .S2X    A3                ; |292| 
           ADDKPC  .S2     RL40,B3,4         ; |292| 
RL40:      ; CALL OCCURS {__negd}            ; |292| 
DW$L$_InvUpTriangularMatrixEx$17$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$18$B:
           MVKL    .S2     __mpyd,B6         ; |292| 
           MVKH    .S2     __mpyd,B6         ; |292| 

           CALL    .S2     B6                ; |292| 
||         LDDW    .D1T1   *A10,A7:A6        ; |292| 

           MV      .L2X    A4,B4             ; |292| 
           ADDKPC  .S2     RL41,B3,1         ; |292| 
           MV      .L2X    A5,B5             ; |292| 

           MV      .L1     A7,A5             ; |292| 
||         MV      .S1     A6,A4             ; |292| 

RL41:      ; CALL OCCURS {__mpyd}            ; |292| 
DW$L$_InvUpTriangularMatrixEx$18$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$19$B:

           LDW     .D2T2   *+SP(4640),B4     ; |292| 
||         ADD     .L2     1,B12,B12         ; |286| 
||         ADDK    .S2     192,B13           ; |286| 
||         STDW    .D1T1   A5:A4,*A10        ; |292| 
||         ADDK    .S1     192,A10           ; |286| 

           NOP             3
           CMPLT   .L2     B12,B10,B0        ; |289| 

           SUB     .L2     B4,1,B4           ; |286| 
||         SUB     .L1X    B4,1,A0           ; |286| 

   [!A0]   MVK     .L2     0x1,B0            ; nullify predicate
|| [ A0]   MVK     .S2     192,B4
||         STW     .D2T2   B4,*+SP(4640)     ; |286| 

   [!A0]   MVK     .S2     1153,B5           ; |283| 
|| [ A0]   LDW     .D2T2   *+SP(4636),B6
|| [ B0]   MVKL    .S1     __mpyd,A3         ; |290| 
|| [ A0]   MPYLI   .M2     B4,B12,B5:B4

   [ A0]   B       .S2     L42               ; |286| 
|| [ B0]   LDW     .D2T2   *+SP(4624),B30
|| [ B0]   MVKH    .S1     __mpyd,A3         ; |290| 

   [!B0]   BNOP    .S1     L44,3             ; |289| 
|| [!A0]   LDW     .D2T2   *+SP(4624),B4     ; |283| 

	.dwpsn	"matrix2.c",293,0
   [ B0]   ADDAD   .D2     B13,B12,B31
           ; BRANCHCC OCCURS {L42}           ; |286| 
DW$L$_InvUpTriangularMatrixEx$19$E:
;** --------------------------------------------------------------------------*
DW$L$_InvUpTriangularMatrixEx$20$B:

           LDW     .D2T2   *+B4[B5],B5       ; |283| 
||         ADD     .L2     1,B10,B10         ; |283| 
||         ADDK    .S1     200,A15           ; |283| 

           LDW     .D2T2   *+SP(4636),B31    ; |283| 
           LDW     .D2T2   *+SP(4632),B6     ; |283| 
           NOP             2
           CMPGT   .L2     B5,B10,B0         ; |283| 

           ADD     .L2     8,B31,B4          ; |283| 
|| [ B0]   B       .S1     L41               ; |283| 

           STW     .D2T2   B4,*+SP(4636)     ; |283| 
||         ADDK    .S2     200,B6            ; |283| 

   [ B0]   MVKL    .S2     __divd,B6         ; |285| 
||         STW     .D2T2   B6,*+SP(4632)     ; |283| 

   [ B0]   MVKH    .S2     __divd,B6         ; |285| 
|| [ B0]   LDW     .D2T2   *+SP(4632),B4

	.dwpsn	"matrix2.c",294,0
           NOP             2
           ; BRANCHCC OCCURS {L41}           ; |283| 
DW$L$_InvUpTriangularMatrixEx$20$E:
;** --------------------------------------------------------------------------*
L45:    

           LDW     .D2T1   *+SP(4628),A4     ; |298| 
||         MVKL    .S2     _CopyMatrixEx,B5  ; |298| 
||         MVKL    .S1     _DestroyMatrixEx,A10 ; |299| 
||         ADD     .L2     8,SP,B10          ; |299| 

           NOP             1
           MVKH    .S2     _CopyMatrixEx,B5  ; |298| 
           MVKH    .S1     _DestroyMatrixEx,A10 ; |299| 
           ADD     .L2     8,SP,B4           ; |298| 
           CMPEQ   .L1     A14,A4,A0         ; |296| 

   [ A0]   LDW     .D2T2   *+SP(4668),B3     ; |303| 
|| [ A0]   B       .S1     L47               ; |296| 

   [ A0]   LDW     .D2T1   *+SP(4688),A15    ; |303| 
|| [!A0]   CALL    .S2     B5                ; |298| 

   [ A0]   LDW     .D2T1   *+SP(4652),A11    ; |303| 
   [ A0]   LDW     .D2T1   *+SP(4648),A10    ; |303| 
   [ A0]   LDW     .D2T1   *+SP(4660),A13    ; |303| 
   [ A0]   LDW     .D2T1   *+SP(4656),A12    ; |303| 
           ; BRANCHCC OCCURS {L47}           ; |296| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL42,B3,0         ; |298| 
RL42:      ; CALL OCCURS {_CopyMatrixEx}     ; |298| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A10               ; |299| 
           ADDKPC  .S2     RL43,B3,3         ; |299| 
           MV      .L1X    B10,A4            ; |299| 
RL43:      ; CALL OCCURS {_DestroyMatrixEx}  ; |299| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4668),B3     ; |303| 
           LDW     .D2T1   *+SP(4652),A11    ; |303| 
           LDW     .D2T1   *+SP(4648),A10    ; |303| 
           LDW     .D2T1   *+SP(4660),A13    ; |303| 
           LDW     .D2T1   *+SP(4656),A12    ; |303| 
           LDW     .D2T1   *+SP(4664),A14    ; |303| 
           LDW     .D2T2   *+SP(4676),B11    ; |303| 
           LDW     .D2T2   *+SP(4672),B10    ; |303| 
           LDW     .D2T2   *+SP(4684),B13    ; |303| 

           RET     .S2     B3                ; |303| 
||         LDW     .D2T1   *+SP(4688),A15    ; |303| 

           LDW     .D2T2   *+SP(4680),B12    ; |303| 
           NOP             3
           ADDK    .S2     4688,SP           ; |303| 
           ; BRANCH OCCURS {B3}              ; |303| 
;** --------------------------------------------------------------------------*
L46:    
           MVKL    .S1     _DestroyMatrixEx,A3 ; |266| 
           MVKH    .S1     _DestroyMatrixEx,A3 ; |266| 
           LDW     .D2T1   *+SP(4628),A4     ; |266| 
           CALL    .S2X    A3                ; |266| 
           ADDKPC  .S2     RL44,B3,4         ; |266| 
RL44:      ; CALL OCCURS {_DestroyMatrixEx}  ; |266| 
           LDW     .D2T1   *+SP(4688),A15    ; |303| 
           LDW     .D2T1   *+SP(4652),A11    ; |303| 
           LDW     .D2T1   *+SP(4648),A10    ; |303| 
           LDW     .D2T1   *+SP(4660),A13    ; |303| 
           LDW     .D2T2   *+SP(4668),B3     ; |303| 
           LDW     .D2T1   *+SP(4656),A12    ; |303| 
;** --------------------------------------------------------------------------*
L47:    
           LDW     .D2T2   *+SP(4680),B12    ; |303| 
           LDW     .D2T2   *+SP(4684),B13    ; |303| 
           LDW     .D2T2   *+SP(4672),B10    ; |303| 

           LDW     .D2T2   *+SP(4676),B11    ; |303| 
||         RET     .S2     B3                ; |303| 

           LDW     .D2T1   *+SP(4664),A14    ; |303| 
||         ADDK    .S2     4688,SP           ; |303| 

	.dwpsn	"matrix2.c",303,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |303| 

DW$86	.dwtag  DW_TAG_loop
	.dwattr DW$86, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L41:1:1472539739")
	.dwattr DW$86, DW_AT_begin_file("matrix2.c")
	.dwattr DW$86, DW_AT_begin_line(0x11b)
	.dwattr DW$86, DW_AT_end_line(0x126)
DW$87	.dwtag  DW_TAG_loop_range
	.dwattr DW$87, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$10$B)
	.dwattr DW$87, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$10$E)
DW$88	.dwtag  DW_TAG_loop_range
	.dwattr DW$88, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$11$B)
	.dwattr DW$88, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$11$E)
DW$89	.dwtag  DW_TAG_loop_range
	.dwattr DW$89, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$20$B)
	.dwattr DW$89, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$20$E)

DW$90	.dwtag  DW_TAG_loop
	.dwattr DW$90, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L42:2:1472539739")
	.dwattr DW$90, DW_AT_begin_file("matrix2.c")
	.dwattr DW$90, DW_AT_begin_line(0x11e)
	.dwattr DW$90, DW_AT_end_line(0x125)
DW$91	.dwtag  DW_TAG_loop_range
	.dwattr DW$91, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$12$B)
	.dwattr DW$91, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$12$E)
DW$92	.dwtag  DW_TAG_loop_range
	.dwattr DW$92, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$13$B)
	.dwattr DW$92, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$13$E)
DW$93	.dwtag  DW_TAG_loop_range
	.dwattr DW$93, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$17$B)
	.dwattr DW$93, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$17$E)
DW$94	.dwtag  DW_TAG_loop_range
	.dwattr DW$94, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$18$B)
	.dwattr DW$94, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$18$E)
DW$95	.dwtag  DW_TAG_loop_range
	.dwattr DW$95, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$19$B)
	.dwattr DW$95, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$19$E)

DW$96	.dwtag  DW_TAG_loop
	.dwattr DW$96, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L43:3:1472539739")
	.dwattr DW$96, DW_AT_begin_file("matrix2.c")
	.dwattr DW$96, DW_AT_begin_line(0x121)
	.dwattr DW$96, DW_AT_end_line(0x122)
DW$97	.dwtag  DW_TAG_loop_range
	.dwattr DW$97, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$14$B)
	.dwattr DW$97, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$14$E)
DW$98	.dwtag  DW_TAG_loop_range
	.dwattr DW$98, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$15$B)
	.dwattr DW$98, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$15$E)
DW$99	.dwtag  DW_TAG_loop_range
	.dwattr DW$99, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$16$B)
	.dwattr DW$99, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$16$E)
	.dwendtag DW$96

	.dwendtag DW$90

	.dwendtag DW$86


DW$100	.dwtag  DW_TAG_loop
	.dwattr DW$100, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L39:1:1472539739")
	.dwattr DW$100, DW_AT_begin_file("matrix2.c")
	.dwattr DW$100, DW_AT_begin_line(0x10f)
	.dwattr DW$100, DW_AT_end_line(0x114)
DW$101	.dwtag  DW_TAG_loop_range
	.dwattr DW$101, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$4$B)
	.dwattr DW$101, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$4$E)
DW$102	.dwtag  DW_TAG_loop_range
	.dwattr DW$102, DW_AT_low_pc(DW$L$_InvUpTriangularMatrixEx$5$B)
	.dwattr DW$102, DW_AT_high_pc(DW$L$_InvUpTriangularMatrixEx$5$E)
	.dwendtag DW$100

	.dwattr DW$83, DW_AT_end_file("matrix2.c")
	.dwattr DW$83, DW_AT_end_line(0x12f)
	.dwattr DW$83, DW_AT_end_column(0x01)
	.dwendtag DW$83

	.sect	".text"
	.global	_QR_DecompositionEx

DW$103	.dwtag  DW_TAG_subprogram, DW_AT_name("QR_DecompositionEx"), DW_AT_symbol_name("_QR_DecompositionEx")
	.dwattr DW$103, DW_AT_low_pc(_QR_DecompositionEx)
	.dwattr DW$103, DW_AT_high_pc(0x00)
	.dwattr DW$103, DW_AT_begin_file("matrix2.c")
	.dwattr DW$103, DW_AT_begin_line(0x139)
	.dwattr DW$103, DW_AT_begin_column(0x06)
	.dwattr DW$103, DW_AT_frame_base[DW_OP_breg31 64]
	.dwattr DW$103, DW_AT_skeletal(0x01)
	.dwpsn	"matrix2.c",314,1

;******************************************************************************
;* FUNCTION NAME: _QR_DecompositionEx                                         *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 20 Auto + 44 Save = 64 byte                 *
;******************************************************************************
_QR_DecompositionEx:
;** --------------------------------------------------------------------------*
DW$104	.dwtag  DW_TAG_formal_parameter, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$104, DW_AT_type(*DW$T$29)
	.dwattr DW$104, DW_AT_location[DW_OP_reg4]
DW$105	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Q"), DW_AT_symbol_name("_Q")
	.dwattr DW$105, DW_AT_type(*DW$T$29)
	.dwattr DW$105, DW_AT_location[DW_OP_reg20]
DW$106	.dwtag  DW_TAG_formal_parameter, DW_AT_name("R"), DW_AT_symbol_name("_R")
	.dwattr DW$106, DW_AT_type(*DW$T$29)
	.dwattr DW$106, DW_AT_location[DW_OP_reg6]

           STW     .D2T1   A15,*SP--(64)     ; |314| 
||         MV      .L1X    SP,A31            ; |314| 

           STDW    .D2T2   B11:B10,*+SP(48)
           STW     .D2T1   A6,*+SP(8)        ; |314| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(44)
||         MVKL    .S1     _CopyMatrixEx,A3  ; |319| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B4,*+SP(4)        ; |314| 
||         MVKH    .S1     _CopyMatrixEx,A3  ; |319| 

           STW     .D1T1   A14,*-A31(24)
||         STDW    .D2T2   B13:B12,*+SP(56)
||         MV      .L1     A4,A5             ; |314| 
||         MVK     .S1     1152,A8           ; |316| 

           CALL    .S2X    A3                ; |319| 
||         LDW     .D1T2   *+A5[A8],B13      ; |316| 
||         MVK     .S1     1153,A7           ; |316| 

           LDW     .D1T1   *+A5[A7],A14      ; |316| 
           ADDKPC  .S2     RL45,B3,0         ; |319| 
           MV      .L1     A6,A15            ; |314| 
           MV      .S1X    B4,A4             ; |314| 
           MV      .L2X    A5,B4             ; |319| 
RL45:      ; CALL OCCURS {_CopyMatrixEx}     ; |319| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     _CreateMatrixEx,B5 ; |320| 
           MVKH    .S2     _CreateMatrixEx,B5 ; |320| 
           CALL    .S2     B5                ; |320| 
           ADDKPC  .S2     RL46,B3,1         ; |320| 
           MV      .L1     A14,A6            ; |320| 
           MV      .L2X    A14,B4            ; |320| 
           MV      .S1     A15,A4            ; |320| 
RL46:      ; CALL OCCURS {_CreateMatrixEx}   ; |320| 
;** --------------------------------------------------------------------------*

           CMPGT   .L1     A14,0,A0          ; |322| 
||         STW     .D2T1   A14,*+SP(12)      ; |324| 
||         ZERO    .S1     A11               ; |322| 

   [!A0]   BNOP    .S1     L59,5             ; |322| 
|| [ A0]   LDW     .D2T2   *+SP(4),B5        ; |326| 
|| [ A0]   CMPGT   .L2     B13,0,B0          ; |324| 

           ; BRANCHCC OCCURS {L59}           ; |322| 
;** --------------------------------------------------------------------------*
	.dwpsn	"matrix2.c",322,0

   [!B0]   BNOP    .S2     L50,3             ; |324| 
|| [ B0]   MV      .L2X    A11,B4
|| [ B0]   MVKL    .S1     __mpyd,A3         ; |326| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L48
;** --------------------------------------------------------------------------*
L48:    
DW$L$_QR_DecompositionEx$5$B:
	.dwpsn	"matrix2.c",323,0

   [ B0]   MVKH    .S1     __mpyd,A3         ; |326| 
||         ADDAD   .D2     B5,B4,B10
||         ZERO    .L1     A13:A12           ; |324| 

   [ B0]   LDDW    .D2T2   *B10,B5:B4        ; |326| 
           ; BRANCHCC OCCURS {L50}           ; |324| 
DW$L$_QR_DecompositionEx$5$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$6$B:

           CALL    .S2X    A3                ; |326| 
||         MV      .L1X    B13,A10           ; |326| 

	.dwpsn	"matrix2.c",324,0
           NOP             3
DW$L$_QR_DecompositionEx$6$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L49:    
DW$L$_QR_DecompositionEx$7$B:
	.dwpsn	"matrix2.c",325,0
           MV      .L1X    B4,A4             ; |326| 

           MV      .L1X    B5,A5             ; |326| 
||         ADDKPC  .S2     RL48,B3,0         ; |326| 

RL48:      ; CALL OCCURS {__mpyd}            ; |326| 
DW$L$_QR_DecompositionEx$7$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$8$B:
           MVKL    .S2     __addd,B6         ; |326| 
           MVKH    .S2     __addd,B6         ; |326| 
           CALL    .S2     B6                ; |326| 
           MV      .L2X    A4,B4             ; |326| 
           ADDKPC  .S2     RL49,B3,0         ; |326| 
           MV      .S1     A12,A4            ; |326| 
           MV      .L2X    A5,B5             ; |326| 
           MV      .L1     A13,A5            ; |326| 
RL49:      ; CALL OCCURS {__addd}            ; |326| 
DW$L$_QR_DecompositionEx$8$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$9$B:

           ADDK    .S2     192,B10           ; |324| 
||         MVKL    .S1     __mpyd,A3         ; |326| 
||         SUB     .L1     A10,1,A0          ; |324| 
||         SUB     .D1     A10,1,A10         ; |324| 

   [ A0]   BNOP    .S2     L49,1             ; |324| 
||         MV      .L1     A5,A13            ; |326| 
||         MV      .D1     A4,A12            ; |326| 
||         MVKH    .S1     __mpyd,A3         ; |326| 
|| [ A0]   LDDW    .D2T2   *B10,B5:B4        ; |326| 

   [ A0]   CALL    .S2X    A3                ; |326| 
	.dwpsn	"matrix2.c",327,0
           NOP             3
           ; BRANCHCC OCCURS {L49}           ; |324| 
DW$L$_QR_DecompositionEx$9$E:
;** --------------------------------------------------------------------------*
L50:    
DW$L$_QR_DecompositionEx$10$B:
           MVKL    .S2     _sqrt,B4          ; |329| 
           MVKH    .S2     _sqrt,B4          ; |329| 
           CALL    .S2     B4                ; |329| 
           ADDKPC  .S2     RL50,B3,2         ; |329| 
           MV      .L1     A13,A5            ; |329| 
           MV      .S1     A12,A4            ; |329| 
RL50:      ; CALL OCCURS {_sqrt}             ; |329| 
DW$L$_QR_DecompositionEx$10$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$11$B:
           MVKL    .S2     __divd,B6         ; |330| 
           MVKH    .S2     __divd,B6         ; |330| 

           CALL    .S2     B6                ; |330| 
||         LDW     .D2T2   *+SP(8),B7        ; |330| 

           MV      .S1     A4,A6             ; |329| 
           MV      .D1     A5,A7             ; |329| 
           MV      .L2X    A5,B5             ; |329| 
           ZERO    .L1     A5                ; |330| 

           ADDKPC  .S2     RL51,B3,0         ; |330| 
||         STDW    .D2T1   A7:A6,*B7         ; |330| 
||         MVKH    .S1     0x3ff00000,A5     ; |330| 
||         ZERO    .L1     A4                ; |330| 
||         MV      .L2X    A4,B4             ; |329| 

RL51:      ; CALL OCCURS {__divd}            ; |330| 
           NOP             1
DW$L$_QR_DecompositionEx$11$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$12$B:

           CMPGT   .L2     B13,0,B0          ; |332| 
||         MV      .S2X    A4,B10            ; |330| 
||         MV      .L1     A5,A13            ; |330| 
||         LDW     .D2T1   *+SP(4),A3

   [!B0]   BNOP    .S1     L52,4             ; |332| 
|| [ B0]   MVKL    .S2     __mpyd,B6         ; |334| 

   [ B0]   MVKH    .S2     __mpyd,B6         ; |334| 
           ; BRANCHCC OCCURS {L52}           ; |332| 
DW$L$_QR_DecompositionEx$12$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$13$B:

           ADDAD   .D1     A3,A11,A10
||         CALL    .S2     B6                ; |334| 
||         MV      .L1X    B13,A12           ; |334| 

           LDDW    .D1T1   *A10,A5:A4        ; |334| 
	.dwpsn	"matrix2.c",332,0
           NOP             3
DW$L$_QR_DecompositionEx$13$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L51:    
DW$L$_QR_DecompositionEx$14$B:
	.dwpsn	"matrix2.c",333,0

           MV      .L2X    A13,B5            ; |334| 
||         MV      .D2     B10,B4            ; |334| 
||         ADDKPC  .S2     RL52,B3,0         ; |334| 

RL52:      ; CALL OCCURS {__mpyd}            ; |334| 
DW$L$_QR_DecompositionEx$14$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$15$B:

           SUB     .L1     A12,1,A0          ; |332| 
||         STDW    .D1T1   A5:A4,*A10        ; |334| 
||         ADDK    .S1     192,A10           ; |332| 
||         MVKL    .S2     __mpyd,B6         ; |334| 

   [ A0]   B       .S1     L51               ; |332| 
||         SUB     .L1     A12,1,A12         ; |332| 
||         MVKH    .S2     __mpyd,B6         ; |334| 
|| [ A0]   LDDW    .D1T1   *A10,A5:A4        ; |334| 

   [ A0]   CALL    .S2     B6                ; |334| 
	.dwpsn	"matrix2.c",335,0
           NOP             4
           ; BRANCHCC OCCURS {L51}           ; |332| 
DW$L$_QR_DecompositionEx$15$E:
;** --------------------------------------------------------------------------*
L52:    
DW$L$_QR_DecompositionEx$16$B:

           ADD     .L1     1,A11,A3          ; |337| 
||         MVK     .S1     192,A4

           CMPLT   .L1     A3,A14,A0         ; |337| 
||         MPYLI   .M1     A4,A11,A5:A4

   [!A0]   BNOP    .S1     L58,5             ; |337| 
|| [ A0]   CMPGT   .L2     B13,0,B0          ; |339| 

           ; BRANCHCC OCCURS {L58}           ; |337| 
DW$L$_QR_DecompositionEx$16$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$17$B:

           SUB     .L1     A14,A3,A31        ; |339| 
||         SHL     .S1     A3,3,A13
||         LDW     .D2T1   *+SP(4),A3
||         ADDAD   .D1     A4,A11,A4
|| [!B0]   B       .S2     L55               ; |339| 

           ADD     .L1     A15,A4,A4
||         STW     .D2T1   A31,*+SP(16)      ; |339| 
|| [ B0]   MVKL    .S2     __mpyd,B6         ; |341| 

           NOP             1
           ADD     .L2X    8,A4,B4
	.dwpsn	"matrix2.c",337,0
           STW     .D2T2   B4,*+SP(20)
DW$L$_QR_DecompositionEx$17$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L53
;** --------------------------------------------------------------------------*
L53:    
DW$L$_QR_DecompositionEx$18$B:
	.dwpsn	"matrix2.c",338,0

           ZERO    .L2     B11:B10           ; |339| 
|| [ B0]   MVKH    .S2     __mpyd,B6         ; |341| 
||         MV      .D2X    A3,B4             ; |341| 

           ; BRANCHCC OCCURS {L55}           ; |339| 
DW$L$_QR_DecompositionEx$18$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$19$B:

           ADDAD   .D1     A3,A11,A10
||         ADD     .L2X    A13,B4,B12
||         CALL    .S2     B6                ; |341| 
||         MV      .L1X    B13,A12           ; |341| 

           LDDW    .D1T1   *A10,A5:A4        ; |341| 
||         LDDW    .D2T2   *B12,B5:B4        ; |341| 

	.dwpsn	"matrix2.c",339,0
           NOP             2
DW$L$_QR_DecompositionEx$19$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L54:    
DW$L$_QR_DecompositionEx$20$B:
	.dwpsn	"matrix2.c",340,0
           ADDKPC  .S2     RL54,B3,1         ; |341| 
RL54:      ; CALL OCCURS {__mpyd}            ; |341| 
DW$L$_QR_DecompositionEx$20$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$21$B:
           MVKL    .S1     __addd,A3         ; |341| 
           MVKH    .S1     __addd,A3         ; |341| 
           MV      .L2X    A4,B4             ; |341| 
           CALL    .S2X    A3                ; |341| 
           MV      .L1X    B10,A4            ; |341| 
           ADDKPC  .S2     RL55,B3,1         ; |341| 
           MV      .L2X    A5,B5             ; |341| 
           MV      .L1X    B11,A5            ; |341| 
RL55:      ; CALL OCCURS {__addd}            ; |341| 
DW$L$_QR_DecompositionEx$21$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$22$B:

           SUB     .L1     A12,1,A0          ; |339| 
||         SUB     .D1     A12,1,A12         ; |339| 
||         ADDK    .S1     192,A10           ; |339| 
||         MVKL    .S2     __mpyd,B6         ; |341| 
||         MV      .L2X    A4,B10            ; |341| 

           MVKH    .S2     __mpyd,B6         ; |341| 
|| [ A0]   B       .S1     L54               ; |339| 
||         MV      .L2X    A5,B11            ; |341| 
|| [ A0]   LDDW    .D1T1   *A10,A5:A4        ; |341| 

           ADDK    .S2     192,B12           ; |339| 

   [ A0]   CALL    .S2     B6                ; |341| 
|| [ A0]   LDDW    .D2T2   *B12,B5:B4        ; |341| 

	.dwpsn	"matrix2.c",342,0
           NOP             3
           ; BRANCHCC OCCURS {L54}           ; |339| 
DW$L$_QR_DecompositionEx$22$E:
;** --------------------------------------------------------------------------*
L55:    
DW$L$_QR_DecompositionEx$23$B:

           CMPGT   .L2     B13,0,B0          ; |344| 
||         LDW     .D2T1   *+SP(4),A3

   [!B0]   BNOP    .S1     L57,4             ; |344| 
|| [ B0]   MVKL    .S2     __mpyd,B6         ; |346| 

   [ B0]   MVKH    .S2     __mpyd,B6         ; |346| 
           ; BRANCHCC OCCURS {L57}           ; |344| 
DW$L$_QR_DecompositionEx$23$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$24$B:

           ADDAD   .D1     A3,A11,A10
||         CALL    .S2     B6                ; |346| 
||         MV      .L2X    A3,B4             ; |346| 
||         MV      .L1X    B13,A12           ; |346| 

           LDDW    .D1T1   *A10,A5:A4        ; |346| 
||         ADD     .L2X    A13,B4,B12

	.dwpsn	"matrix2.c",344,0
           NOP             3
DW$L$_QR_DecompositionEx$24$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L56:    
DW$L$_QR_DecompositionEx$25$B:
	.dwpsn	"matrix2.c",345,0

           MV      .L2     B11,B5            ; |346| 
||         MV      .D2     B10,B4            ; |346| 
||         ADDKPC  .S2     RL57,B3,0         ; |346| 

RL57:      ; CALL OCCURS {__mpyd}            ; |346| 
DW$L$_QR_DecompositionEx$25$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$26$B:
           MVKL    .S1     __subd,A3         ; |346| 
           MVKH    .S1     __subd,A3         ; |346| 
           LDDW    .D2T2   *B12,B7:B6        ; |346| 
           CALL    .S2X    A3                ; |346| 
           MV      .L2X    A4,B4             ; |346| 
           ADDKPC  .S2     RL58,B3,0         ; |346| 
           MV      .L2X    A5,B5             ; |346| 
           MV      .L1X    B6,A4             ; |346| 
           MV      .L1X    B7,A5             ; |346| 
RL58:      ; CALL OCCURS {__subd}            ; |346| 
DW$L$_QR_DecompositionEx$26$E:
;** --------------------------------------------------------------------------*
DW$L$_QR_DecompositionEx$27$B:

           SUB     .L1     A12,1,A0          ; |344| 
||         STDW    .D2T1   A5:A4,*B12        ; |346| 
||         SUB     .D1     A12,1,A12         ; |344| 
||         ADDK    .S1     192,A10           ; |344| 
||         MVKL    .S2     __mpyd,B6         ; |346| 

           MVKH    .S2     __mpyd,B6         ; |346| 
|| [ A0]   B       .S1     L56               ; |344| 
|| [ A0]   LDDW    .D1T1   *A10,A5:A4        ; |346| 

   [ A0]   CALL    .S2     B6                ; |346| 
           NOP             3
	.dwpsn	"matrix2.c",347,0
           ADDK    .S2     192,B12           ; |344| 
           ; BRANCHCC OCCURS {L56}           ; |344| 
DW$L$_QR_DecompositionEx$27$E:
;** --------------------------------------------------------------------------*
L57:    
DW$L$_QR_DecompositionEx$28$B:

           LDW     .D2T2   *+SP(16),B4
||         ADD     .L1     8,A13,A13         ; |337| 
||         CMPGT   .L2     B13,0,B0          ; |339| 

           LDW     .D2T2   *+SP(20),B5
           NOP             3

           SUB     .L2     B4,1,B4           ; |337| 
||         SUB     .L1X    B4,1,A0           ; |337| 

   [!A0]   MVK     .L2     0x1,B0            ; nullify predicate
||         STDW    .D2T2   B11:B10,*B5++     ; |349| 
|| [ A0]   B       .S1     L53               ; |337| 

   [!B0]   BNOP    .S1     L55,2             ; |339| 
|| [ B0]   MVKL    .S2     __mpyd,B6         ; |341| 
|| [ A0]   LDW     .D2T1   *+SP(4),A3

           STW     .D2T2   B5,*+SP(20)       ; |349| 
	.dwpsn	"matrix2.c",350,0
           STW     .D2T2   B4,*+SP(16)       ; |349| 
           ; BRANCHCC OCCURS {L53}           ; |337| 
DW$L$_QR_DecompositionEx$28$E:
;** --------------------------------------------------------------------------*
L58:    
DW$L$_QR_DecompositionEx$29$B:

           LDW     .D2T2   *+SP(12),B5
||         ADD     .L1     1,A11,A11         ; |322| 
||         CMPGT   .L2     B13,0,B0          ; |324| 

           LDW     .D2T2   *+SP(8),B4
           NOP             3

           SUB     .L2     B5,1,B5           ; |322| 
||         SUB     .L1X    B5,1,A0           ; |322| 

   [!A0]   MVK     .L2     0x1,B0            ; |326| nullify predicate
||         STW     .D2T2   B5,*+SP(12)       ; |322| 
||         ADDK    .S2     200,B4            ; |322| 
|| [ A0]   B       .S1     L48               ; |322| 

   [ B0]   MVKL    .S1     __mpyd,A3         ; |326| 
||         LDW     .D2T2   *+SP(4),B5        ; |326| 

	.dwpsn	"matrix2.c",351,0

   [!B0]   BNOP    .S1     L50,3             ; |324| 
|| [ B0]   MV      .L2X    A11,B4
||         STW     .D2T2   B4,*+SP(8)        ; |322| 

           ; BRANCHCC OCCURS {L48}           ; |322| 
DW$L$_QR_DecompositionEx$29$E:
;** --------------------------------------------------------------------------*
L59:    
           LDW     .D2T2   *+SP(44),B3       ; |352| 
           MV      .L1X    SP,A31            ; |352| 
           LDDW    .D1T1   *+A31(32),A13:A12 ; |352| 
           LDW     .D1T1   *+A31(40),A14     ; |352| 
           LDDW    .D2T2   *+SP(56),B13:B12  ; |352| 

           RET     .S2     B3                ; |352| 
||         LDDW    .D1T1   *+A31(24),A11:A10 ; |352| 
||         LDDW    .D2T2   *+SP(48),B11:B10  ; |352| 

           LDW     .D2T1   *++SP(64),A15     ; |352| 
	.dwpsn	"matrix2.c",352,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |352| 

DW$107	.dwtag  DW_TAG_loop
	.dwattr DW$107, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L48:1:1472539739")
	.dwattr DW$107, DW_AT_begin_file("matrix2.c")
	.dwattr DW$107, DW_AT_begin_line(0x142)
	.dwattr DW$107, DW_AT_end_line(0x15f)
DW$108	.dwtag  DW_TAG_loop_range
	.dwattr DW$108, DW_AT_low_pc(DW$L$_QR_DecompositionEx$5$B)
	.dwattr DW$108, DW_AT_high_pc(DW$L$_QR_DecompositionEx$5$E)
DW$109	.dwtag  DW_TAG_loop_range
	.dwattr DW$109, DW_AT_low_pc(DW$L$_QR_DecompositionEx$17$B)
	.dwattr DW$109, DW_AT_high_pc(DW$L$_QR_DecompositionEx$17$E)
DW$110	.dwtag  DW_TAG_loop_range
	.dwattr DW$110, DW_AT_low_pc(DW$L$_QR_DecompositionEx$13$B)
	.dwattr DW$110, DW_AT_high_pc(DW$L$_QR_DecompositionEx$13$E)
DW$111	.dwtag  DW_TAG_loop_range
	.dwattr DW$111, DW_AT_low_pc(DW$L$_QR_DecompositionEx$6$B)
	.dwattr DW$111, DW_AT_high_pc(DW$L$_QR_DecompositionEx$6$E)
DW$112	.dwtag  DW_TAG_loop_range
	.dwattr DW$112, DW_AT_low_pc(DW$L$_QR_DecompositionEx$10$B)
	.dwattr DW$112, DW_AT_high_pc(DW$L$_QR_DecompositionEx$10$E)
DW$113	.dwtag  DW_TAG_loop_range
	.dwattr DW$113, DW_AT_low_pc(DW$L$_QR_DecompositionEx$11$B)
	.dwattr DW$113, DW_AT_high_pc(DW$L$_QR_DecompositionEx$11$E)
DW$114	.dwtag  DW_TAG_loop_range
	.dwattr DW$114, DW_AT_low_pc(DW$L$_QR_DecompositionEx$12$B)
	.dwattr DW$114, DW_AT_high_pc(DW$L$_QR_DecompositionEx$12$E)
DW$115	.dwtag  DW_TAG_loop_range
	.dwattr DW$115, DW_AT_low_pc(DW$L$_QR_DecompositionEx$16$B)
	.dwattr DW$115, DW_AT_high_pc(DW$L$_QR_DecompositionEx$16$E)
DW$116	.dwtag  DW_TAG_loop_range
	.dwattr DW$116, DW_AT_low_pc(DW$L$_QR_DecompositionEx$29$B)
	.dwattr DW$116, DW_AT_high_pc(DW$L$_QR_DecompositionEx$29$E)

DW$117	.dwtag  DW_TAG_loop
	.dwattr DW$117, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L49:2:1472539739")
	.dwattr DW$117, DW_AT_begin_file("matrix2.c")
	.dwattr DW$117, DW_AT_begin_line(0x144)
	.dwattr DW$117, DW_AT_end_line(0x147)
DW$118	.dwtag  DW_TAG_loop_range
	.dwattr DW$118, DW_AT_low_pc(DW$L$_QR_DecompositionEx$7$B)
	.dwattr DW$118, DW_AT_high_pc(DW$L$_QR_DecompositionEx$7$E)
DW$119	.dwtag  DW_TAG_loop_range
	.dwattr DW$119, DW_AT_low_pc(DW$L$_QR_DecompositionEx$8$B)
	.dwattr DW$119, DW_AT_high_pc(DW$L$_QR_DecompositionEx$8$E)
DW$120	.dwtag  DW_TAG_loop_range
	.dwattr DW$120, DW_AT_low_pc(DW$L$_QR_DecompositionEx$9$B)
	.dwattr DW$120, DW_AT_high_pc(DW$L$_QR_DecompositionEx$9$E)
	.dwendtag DW$117


DW$121	.dwtag  DW_TAG_loop
	.dwattr DW$121, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L51:2:1472539739")
	.dwattr DW$121, DW_AT_begin_file("matrix2.c")
	.dwattr DW$121, DW_AT_begin_line(0x14c)
	.dwattr DW$121, DW_AT_end_line(0x14f)
DW$122	.dwtag  DW_TAG_loop_range
	.dwattr DW$122, DW_AT_low_pc(DW$L$_QR_DecompositionEx$14$B)
	.dwattr DW$122, DW_AT_high_pc(DW$L$_QR_DecompositionEx$14$E)
DW$123	.dwtag  DW_TAG_loop_range
	.dwattr DW$123, DW_AT_low_pc(DW$L$_QR_DecompositionEx$15$B)
	.dwattr DW$123, DW_AT_high_pc(DW$L$_QR_DecompositionEx$15$E)
	.dwendtag DW$121


DW$124	.dwtag  DW_TAG_loop
	.dwattr DW$124, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L53:2:1472539739")
	.dwattr DW$124, DW_AT_begin_file("matrix2.c")
	.dwattr DW$124, DW_AT_begin_line(0x151)
	.dwattr DW$124, DW_AT_end_line(0x15e)
DW$125	.dwtag  DW_TAG_loop_range
	.dwattr DW$125, DW_AT_low_pc(DW$L$_QR_DecompositionEx$18$B)
	.dwattr DW$125, DW_AT_high_pc(DW$L$_QR_DecompositionEx$18$E)
DW$126	.dwtag  DW_TAG_loop_range
	.dwattr DW$126, DW_AT_low_pc(DW$L$_QR_DecompositionEx$24$B)
	.dwattr DW$126, DW_AT_high_pc(DW$L$_QR_DecompositionEx$24$E)
DW$127	.dwtag  DW_TAG_loop_range
	.dwattr DW$127, DW_AT_low_pc(DW$L$_QR_DecompositionEx$19$B)
	.dwattr DW$127, DW_AT_high_pc(DW$L$_QR_DecompositionEx$19$E)
DW$128	.dwtag  DW_TAG_loop_range
	.dwattr DW$128, DW_AT_low_pc(DW$L$_QR_DecompositionEx$23$B)
	.dwattr DW$128, DW_AT_high_pc(DW$L$_QR_DecompositionEx$23$E)
DW$129	.dwtag  DW_TAG_loop_range
	.dwattr DW$129, DW_AT_low_pc(DW$L$_QR_DecompositionEx$28$B)
	.dwattr DW$129, DW_AT_high_pc(DW$L$_QR_DecompositionEx$28$E)

DW$130	.dwtag  DW_TAG_loop
	.dwattr DW$130, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L54:3:1472539739")
	.dwattr DW$130, DW_AT_begin_file("matrix2.c")
	.dwattr DW$130, DW_AT_begin_line(0x153)
	.dwattr DW$130, DW_AT_end_line(0x156)
DW$131	.dwtag  DW_TAG_loop_range
	.dwattr DW$131, DW_AT_low_pc(DW$L$_QR_DecompositionEx$20$B)
	.dwattr DW$131, DW_AT_high_pc(DW$L$_QR_DecompositionEx$20$E)
DW$132	.dwtag  DW_TAG_loop_range
	.dwattr DW$132, DW_AT_low_pc(DW$L$_QR_DecompositionEx$21$B)
	.dwattr DW$132, DW_AT_high_pc(DW$L$_QR_DecompositionEx$21$E)
DW$133	.dwtag  DW_TAG_loop_range
	.dwattr DW$133, DW_AT_low_pc(DW$L$_QR_DecompositionEx$22$B)
	.dwattr DW$133, DW_AT_high_pc(DW$L$_QR_DecompositionEx$22$E)
	.dwendtag DW$130


DW$134	.dwtag  DW_TAG_loop
	.dwattr DW$134, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\matrix2.asm:L56:3:1472539739")
	.dwattr DW$134, DW_AT_begin_file("matrix2.c")
	.dwattr DW$134, DW_AT_begin_line(0x158)
	.dwattr DW$134, DW_AT_end_line(0x15b)
DW$135	.dwtag  DW_TAG_loop_range
	.dwattr DW$135, DW_AT_low_pc(DW$L$_QR_DecompositionEx$25$B)
	.dwattr DW$135, DW_AT_high_pc(DW$L$_QR_DecompositionEx$25$E)
DW$136	.dwtag  DW_TAG_loop_range
	.dwattr DW$136, DW_AT_low_pc(DW$L$_QR_DecompositionEx$26$B)
	.dwattr DW$136, DW_AT_high_pc(DW$L$_QR_DecompositionEx$26$E)
DW$137	.dwtag  DW_TAG_loop_range
	.dwattr DW$137, DW_AT_low_pc(DW$L$_QR_DecompositionEx$27$B)
	.dwattr DW$137, DW_AT_high_pc(DW$L$_QR_DecompositionEx$27$E)
	.dwendtag DW$134

	.dwendtag DW$124

	.dwendtag DW$107

	.dwattr DW$103, DW_AT_end_file("matrix2.c")
	.dwattr DW$103, DW_AT_end_line(0x160)
	.dwattr DW$103, DW_AT_end_column(0x01)
	.dwendtag DW$103

;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_sqrt
	.global	_memset
	.global	__mpyd
	.global	__addd
	.global	__cmpd
	.global	__divd
	.global	__negd
	.global	__subd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr DW$T$3, DW_AT_address_class(0x20)

DW$T$25	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$3)
	.dwattr DW$T$25, DW_AT_language(DW_LANG_C)
DW$138	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$139	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$140	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$24)
	.dwendtag DW$T$25


DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$30, DW_AT_language(DW_LANG_C)
DW$141	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$142	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$22)
DW$143	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$22)
	.dwendtag DW$T$30


DW$T$32	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$32, DW_AT_language(DW_LANG_C)
DW$144	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
	.dwendtag DW$T$32


DW$T$34	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$34, DW_AT_language(DW_LANG_C)
DW$145	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$146	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
	.dwendtag DW$T$34


DW$T$36	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$36, DW_AT_language(DW_LANG_C)
DW$147	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$148	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$149	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
	.dwendtag DW$T$36

DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("int32"), DW_AT_type(*DW$T$10)
	.dwattr DW$T$22, DW_AT_language(DW_LANG_C)
DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("size_t"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$24, DW_AT_language(DW_LANG_C)
DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)

DW$T$43	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$43, DW_AT_language(DW_LANG_C)
DW$150	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$T$43

DW$T$29	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$28)
	.dwattr DW$T$29, DW_AT_address_class(0x20)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)
DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("MatrixEx"), DW_AT_type(*DW$T$23)
	.dwattr DW$T$28, DW_AT_language(DW_LANG_C)

DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$23, DW_AT_byte_size(0x1208)
DW$151	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$21)
	.dwattr DW$151, DW_AT_name("Matr"), DW_AT_symbol_name("_Matr")
	.dwattr DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$151, DW_AT_accessibility(DW_ACCESS_public)
DW$152	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$152, DW_AT_name("mRow"), DW_AT_symbol_name("_mRow")
	.dwattr DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x1200]
	.dwattr DW$152, DW_AT_accessibility(DW_ACCESS_public)
DW$153	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$153, DW_AT_name("mCol"), DW_AT_symbol_name("_mCol")
	.dwattr DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x1204]
	.dwattr DW$153, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$23


DW$T$21	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$21, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$21, DW_AT_byte_size(0x1200)
DW$154	.dwtag  DW_TAG_subrange_type
	.dwattr DW$154, DW_AT_upper_bound(0x17)
DW$155	.dwtag  DW_TAG_subrange_type
	.dwattr DW$155, DW_AT_upper_bound(0x17)
	.dwendtag DW$T$21

DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)

	.dwattr DW$13, DW_AT_external(0x01)
	.dwattr DW$7, DW_AT_external(0x01)
	.dwattr DW$11, DW_AT_external(0x01)
	.dwattr DW$69, DW_AT_external(0x01)
	.dwattr DW$83, DW_AT_external(0x01)
	.dwattr DW$21, DW_AT_external(0x01)
	.dwattr DW$37, DW_AT_external(0x01)
	.dwattr DW$53, DW_AT_external(0x01)
	.dwattr DW$103, DW_AT_external(0x01)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$156	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$156, DW_AT_location[DW_OP_reg0]
DW$157	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$157, DW_AT_location[DW_OP_reg1]
DW$158	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$158, DW_AT_location[DW_OP_reg2]
DW$159	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$159, DW_AT_location[DW_OP_reg3]
DW$160	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$160, DW_AT_location[DW_OP_reg4]
DW$161	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$161, DW_AT_location[DW_OP_reg5]
DW$162	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$162, DW_AT_location[DW_OP_reg6]
DW$163	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$163, DW_AT_location[DW_OP_reg7]
DW$164	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$164, DW_AT_location[DW_OP_reg8]
DW$165	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$165, DW_AT_location[DW_OP_reg9]
DW$166	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$166, DW_AT_location[DW_OP_reg10]
DW$167	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$167, DW_AT_location[DW_OP_reg11]
DW$168	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$168, DW_AT_location[DW_OP_reg12]
DW$169	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$169, DW_AT_location[DW_OP_reg13]
DW$170	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$170, DW_AT_location[DW_OP_reg14]
DW$171	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$171, DW_AT_location[DW_OP_reg15]
DW$172	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$172, DW_AT_location[DW_OP_reg16]
DW$173	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$173, DW_AT_location[DW_OP_reg17]
DW$174	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$174, DW_AT_location[DW_OP_reg18]
DW$175	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$175, DW_AT_location[DW_OP_reg19]
DW$176	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$176, DW_AT_location[DW_OP_reg20]
DW$177	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$177, DW_AT_location[DW_OP_reg21]
DW$178	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$178, DW_AT_location[DW_OP_reg22]
DW$179	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$179, DW_AT_location[DW_OP_reg23]
DW$180	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$180, DW_AT_location[DW_OP_reg24]
DW$181	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$181, DW_AT_location[DW_OP_reg25]
DW$182	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$182, DW_AT_location[DW_OP_reg26]
DW$183	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$183, DW_AT_location[DW_OP_reg27]
DW$184	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$184, DW_AT_location[DW_OP_reg28]
DW$185	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$185, DW_AT_location[DW_OP_reg29]
DW$186	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$186, DW_AT_location[DW_OP_reg30]
DW$187	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$187, DW_AT_location[DW_OP_reg31]
DW$188	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$188, DW_AT_location[DW_OP_regx 0x20]
DW$189	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$189, DW_AT_location[DW_OP_regx 0x21]
DW$190	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$190, DW_AT_location[DW_OP_regx 0x22]
DW$191	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$191, DW_AT_location[DW_OP_regx 0x23]
DW$192	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$192, DW_AT_location[DW_OP_regx 0x24]
DW$193	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$193, DW_AT_location[DW_OP_regx 0x25]
DW$194	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$194, DW_AT_location[DW_OP_regx 0x26]
DW$195	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$195, DW_AT_location[DW_OP_regx 0x27]
DW$196	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$196, DW_AT_location[DW_OP_regx 0x28]
DW$197	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$197, DW_AT_location[DW_OP_regx 0x29]
DW$198	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$198, DW_AT_location[DW_OP_regx 0x2a]
DW$199	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$199, DW_AT_location[DW_OP_regx 0x2b]
DW$200	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$200, DW_AT_location[DW_OP_regx 0x2c]
DW$201	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$201, DW_AT_location[DW_OP_regx 0x2d]
DW$202	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$202, DW_AT_location[DW_OP_regx 0x2e]
DW$203	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$203, DW_AT_location[DW_OP_regx 0x2f]
DW$204	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$204, DW_AT_location[DW_OP_regx 0x30]
DW$205	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$205, DW_AT_location[DW_OP_regx 0x31]
DW$206	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$206, DW_AT_location[DW_OP_regx 0x32]
DW$207	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$207, DW_AT_location[DW_OP_regx 0x33]
DW$208	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$208, DW_AT_location[DW_OP_regx 0x34]
DW$209	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$209, DW_AT_location[DW_OP_regx 0x35]
DW$210	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$210, DW_AT_location[DW_OP_regx 0x36]
DW$211	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$211, DW_AT_location[DW_OP_regx 0x37]
DW$212	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$212, DW_AT_location[DW_OP_regx 0x38]
DW$213	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$213, DW_AT_location[DW_OP_regx 0x39]
DW$214	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$214, DW_AT_location[DW_OP_regx 0x3a]
DW$215	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$215, DW_AT_location[DW_OP_regx 0x3b]
DW$216	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$216, DW_AT_location[DW_OP_regx 0x3c]
DW$217	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$217, DW_AT_location[DW_OP_regx 0x3d]
DW$218	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$218, DW_AT_location[DW_OP_regx 0x3e]
DW$219	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$219, DW_AT_location[DW_OP_regx 0x3f]
DW$220	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$220, DW_AT_location[DW_OP_regx 0x40]
DW$221	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$221, DW_AT_location[DW_OP_regx 0x41]
DW$222	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$222, DW_AT_location[DW_OP_regx 0x42]
DW$223	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$223, DW_AT_location[DW_OP_regx 0x43]
DW$224	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$224, DW_AT_location[DW_OP_regx 0x44]
DW$225	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$225, DW_AT_location[DW_OP_regx 0x45]
DW$226	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$226, DW_AT_location[DW_OP_regx 0x46]
DW$227	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$227, DW_AT_location[DW_OP_regx 0x47]
DW$228	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$228, DW_AT_location[DW_OP_regx 0x48]
DW$229	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$229, DW_AT_location[DW_OP_regx 0x49]
DW$230	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$230, DW_AT_location[DW_OP_regx 0x4a]
DW$231	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$231, DW_AT_location[DW_OP_regx 0x4b]
DW$232	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$232, DW_AT_location[DW_OP_regx 0x4c]
DW$233	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$233, DW_AT_location[DW_OP_regx 0x4d]
DW$234	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$234, DW_AT_location[DW_OP_regx 0x4e]
DW$235	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$235, DW_AT_location[DW_OP_regx 0x4f]
DW$236	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$236, DW_AT_location[DW_OP_regx 0x50]
DW$237	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$237, DW_AT_location[DW_OP_regx 0x51]
DW$238	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$238, DW_AT_location[DW_OP_regx 0x52]
DW$239	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$239, DW_AT_location[DW_OP_regx 0x53]
DW$240	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$240, DW_AT_location[DW_OP_regx 0x54]
DW$241	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$241, DW_AT_location[DW_OP_regx 0x55]
DW$242	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$242, DW_AT_location[DW_OP_regx 0x56]
DW$243	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$243, DW_AT_location[DW_OP_regx 0x57]
DW$244	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$244, DW_AT_location[DW_OP_regx 0x58]
DW$245	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$245, DW_AT_location[DW_OP_regx 0x59]
DW$246	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$246, DW_AT_location[DW_OP_regx 0x5a]
DW$247	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$247, DW_AT_location[DW_OP_regx 0x5b]
DW$248	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$248, DW_AT_location[DW_OP_regx 0x5c]
DW$249	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$249, DW_AT_location[DW_OP_regx 0x5d]
DW$250	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$250, DW_AT_location[DW_OP_regx 0x5e]
DW$251	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$251, DW_AT_location[DW_OP_regx 0x5f]
DW$252	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$252, DW_AT_location[DW_OP_regx 0x60]
DW$253	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$253, DW_AT_location[DW_OP_regx 0x61]
DW$254	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$254, DW_AT_location[DW_OP_regx 0x62]
DW$255	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$255, DW_AT_location[DW_OP_regx 0x63]
DW$256	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$256, DW_AT_location[DW_OP_regx 0x64]
DW$257	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$257, DW_AT_location[DW_OP_regx 0x65]
DW$258	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$258, DW_AT_location[DW_OP_regx 0x66]
DW$259	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$259, DW_AT_location[DW_OP_regx 0x67]
DW$260	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$260, DW_AT_location[DW_OP_regx 0x68]
DW$261	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$261, DW_AT_location[DW_OP_regx 0x69]
DW$262	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$262, DW_AT_location[DW_OP_regx 0x6a]
DW$263	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$263, DW_AT_location[DW_OP_regx 0x6b]
DW$264	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$264, DW_AT_location[DW_OP_regx 0x6c]
DW$265	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$265, DW_AT_location[DW_OP_regx 0x6d]
DW$266	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$266, DW_AT_location[DW_OP_regx 0x6e]
DW$267	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$267, DW_AT_location[DW_OP_regx 0x6f]
DW$268	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$268, DW_AT_location[DW_OP_regx 0x70]
DW$269	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$269, DW_AT_location[DW_OP_regx 0x71]
DW$270	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$270, DW_AT_location[DW_OP_regx 0x72]
DW$271	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$271, DW_AT_location[DW_OP_regx 0x73]
DW$272	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$272, DW_AT_location[DW_OP_regx 0x74]
DW$273	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$273, DW_AT_location[DW_OP_regx 0x75]
DW$274	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$274, DW_AT_location[DW_OP_regx 0x76]
DW$275	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$275, DW_AT_location[DW_OP_regx 0x77]
DW$276	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$276, DW_AT_location[DW_OP_regx 0x78]
DW$277	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$277, DW_AT_location[DW_OP_regx 0x79]
DW$278	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$278, DW_AT_location[DW_OP_regx 0x7a]
DW$279	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$279, DW_AT_location[DW_OP_regx 0x7b]
DW$280	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$280, DW_AT_location[DW_OP_regx 0x7c]
DW$281	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$281, DW_AT_location[DW_OP_regx 0x7d]
DW$282	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$282, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

