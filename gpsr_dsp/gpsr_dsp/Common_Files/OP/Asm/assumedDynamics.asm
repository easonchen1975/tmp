;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:48:49 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("assumedDynamics.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("Atmosphere_Drag_Factor"), DW_AT_symbol_name("_Atmosphere_Drag_Factor")
	.dwattr DW$1, DW_AT_type(*DW$T$19)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$49)
DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$1


DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("Geopotential_Gravitation"), DW_AT_symbol_name("_Geopotential_Gravitation")
	.dwattr DW$6, DW_AT_declaration(0x01)
	.dwattr DW$6, DW_AT_external(0x01)
DW$7	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$31)
DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$9	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$21)
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
	.dwendtag DW$6


DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("SunMoonData_Gravitation"), DW_AT_symbol_name("_SunMoonData_Gravitation")
	.dwattr DW$11, DW_AT_declaration(0x01)
	.dwattr DW$11, DW_AT_external(0x01)
DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$35)
DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$36)
DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
	.dwendtag DW$11


DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("DynamicsModel_Init"), DW_AT_symbol_name("_DynamicsModel_Init")
	.dwattr DW$17, DW_AT_declaration(0x01)
	.dwattr DW$17, DW_AT_external(0x01)
DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$40)
	.dwendtag DW$17


DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt"), DW_AT_symbol_name("_sqrt")
	.dwattr DW$19, DW_AT_type(*DW$T$17)
	.dwattr DW$19, DW_AT_declaration(0x01)
	.dwattr DW$19, DW_AT_external(0x01)
DW$20	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$19

;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI6642 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI6644 
	.sect	".text"
	.global	_assumedDynamics_Init

DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("assumedDynamics_Init"), DW_AT_symbol_name("_assumedDynamics_Init")
	.dwattr DW$21, DW_AT_low_pc(_assumedDynamics_Init)
	.dwattr DW$21, DW_AT_high_pc(0x00)
	.dwattr DW$21, DW_AT_begin_file("assumedDynamics.c")
	.dwattr DW$21, DW_AT_begin_line(0x19)
	.dwattr DW$21, DW_AT_begin_column(0x06)
	.dwattr DW$21, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr DW$21, DW_AT_skeletal(0x01)
	.dwpsn	"assumedDynamics.c",26,1

;******************************************************************************
;* FUNCTION NAME: _assumedDynamics_Init                                       *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 8 Save = 8 byte                    *
;******************************************************************************
_assumedDynamics_Init:
;** --------------------------------------------------------------------------*
DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pDM"), DW_AT_symbol_name("_pDM")
	.dwattr DW$22, DW_AT_type(*DW$T$40)
	.dwattr DW$22, DW_AT_location[DW_OP_reg4]
           MVKL    .S1     _DynamicsModel_Init,A3 ; |27| 
           MVKH    .S1     _DynamicsModel_Init,A3 ; |27| 
           STW     .D2T1   A10,*SP--(8)      ; |26| 
           CALL    .S2X    A3                ; |27| 
           STW     .D2T2   B13,*+SP(4)       ; |26| 
           MV      .L1     A4,A10            ; |26| 
           MV      .L2     B3,B13            ; |26| 
           ADDKPC  .S2     RL0,B3,1          ; |27| 
RL0:       ; CALL OCCURS {_DynamicsModel_Init}  ; |27| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     0x3f1e573a,B7     ; |42| 

           MVKL    .S2     0xc901e573,B6     ; |42| 
||         ZERO    .L2     B5                ; |49| 

           MVKH    .S2     0x3ff00000,B5     ; |49| 
||         MVK     .S1     14456,A3          ; |49| 

           MVKH    .S2     0x3f1e573a,B7     ; |42| 
||         ADD     .L1     A3,A10,A3         ; |49| 
||         ZERO    .L2     B4                ; |49| 
||         MVK     .S1     14448,A4          ; |42| 

           ADD     .L1     A4,A10,A3         ; |42| 
||         STDW    .D1T2   B5:B4,*A3         ; |49| 
||         MVKH    .S2     0xc901e573,B6     ; |42| 

           ZERO    .L1     A3                ; |40| 
||         STDW    .D1T2   B7:B6,*A3         ; |42| 
||         MVK     .S1     3631,A4           ; |40| 
||         MVK     .S2     14528,B4          ; |46| 

           ZERO    .L1     A5:A4             ; |46| 
||         MVK     .S1     3630,A3           ; |32| 
||         STW     .D1T1   A3,*+A10[A4]      ; |40| 
||         ADD     .L2X    B4,A10,B4         ; |46| 
||         MVK     .S2     15,B5             ; |32| 

           STDW    .D2T1   A5:A4,*B4         ; |46| 
||         STW     .D1T2   B5,*+A10[A3]      ; |32| 
||         MV      .L2     B13,B3            ; |52| 

           RET     .S2     B3                ; |52| 
||         LDW     .D2T2   *+SP(4),B13       ; |52| 

           LDW     .D2T1   *++SP(8),A10      ; |52| 
	.dwpsn	"assumedDynamics.c",52,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |52| 
	.dwattr DW$21, DW_AT_end_file("assumedDynamics.c")
	.dwattr DW$21, DW_AT_end_line(0x34)
	.dwattr DW$21, DW_AT_end_column(0x01)
	.dwendtag DW$21

	.sect	".text"
	.global	_assumedDynamics

DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("assumedDynamics"), DW_AT_symbol_name("_assumedDynamics")
	.dwattr DW$23, DW_AT_low_pc(_assumedDynamics)
	.dwattr DW$23, DW_AT_high_pc(0x00)
	.dwattr DW$23, DW_AT_begin_file("assumedDynamics.c")
	.dwattr DW$23, DW_AT_begin_line(0x3e)
	.dwattr DW$23, DW_AT_begin_column(0x06)
	.dwattr DW$23, DW_AT_frame_base[DW_OP_breg31 120]
	.dwattr DW$23, DW_AT_skeletal(0x01)
	.dwpsn	"assumedDynamics.c",63,1

;******************************************************************************
;* FUNCTION NAME: _assumedDynamics                                            *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 76 Auto + 40 Save = 116 byte                *
;******************************************************************************
_assumedDynamics:
;** --------------------------------------------------------------------------*
DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Q"), DW_AT_symbol_name("_Q")
	.dwattr DW$24, DW_AT_type(*DW$T$32)
	.dwattr DW$24, DW_AT_location[DW_OP_reg4]
DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dQ"), DW_AT_symbol_name("_dQ")
	.dwattr DW$25, DW_AT_type(*DW$T$32)
	.dwattr DW$25, DW_AT_location[DW_OP_reg20]
DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrParams"), DW_AT_symbol_name("_ptrParams")
	.dwattr DW$26, DW_AT_type(*DW$T$21)
	.dwattr DW$26, DW_AT_location[DW_OP_reg6]

           STW     .D2T2   B12,*SP--(120)    ; |63| 
||         MV      .L1X    SP,A31            ; |63| 

           STDW    .D1T1   A11:A10,*-A31(32)
||         MVKL    .S1     __mpyd,A3         ; |71| 

           STDW    .D1T1   A13:A12,*-A31(24)
||         MV      .L1     A4,A10            ; |63| 
||         STDW    .D2T2   B11:B10,*+SP(112)
||         MVKH    .S1     __mpyd,A3         ; |71| 

           LDDW    .D1T2   *A10,B7:B6        ; |71| 

           CALL    .S2X    A3                ; |71| 
||         LDDW    .D1T2   *+A10(16),B11:B10 ; |71| 

           LDDW    .D1T1   *+A10(8),A13:A12  ; |71| 
           MV      .L1X    B4,A11            ; |63| 
           STW     .D2T2   B13,*+SP(84)

           MV      .L1X    B7,A5             ; |71| 
||         MV      .L2     B3,B13

           ADDKPC  .S2     RL29,B3,0         ; |71| 
||         MV      .L2     B6,B4             ; |71| 
||         MV      .L1X    B6,A4             ; |71| 
||         MV      .D2     B7,B5             ; |71| 
||         MV      .S1     A6,A14            ; |63| 
||         STDW    .D1T1   A15:A14,*-A31(16)

RL29:      ; CALL OCCURS {__mpyd}            ; |71| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B6         ; |71| 
           MVKH    .S2     __mpyd,B6         ; |71| 
           CALL    .S2     B6                ; |71| 
           MV      .L2X    A4,B12            ; |71| 
           MV      .L2X    A12,B4            ; |71| 
           ADDKPC  .S2     RL30,B3,0         ; |71| 
           MV      .S1     A12,A4            ; |71| 

           MV      .L2X    A13,B5            ; |71| 
||         MV      .D1     A5,A15            ; |71| 
||         MV      .L1     A13,A5            ; |71| 

RL30:      ; CALL OCCURS {__mpyd}            ; |71| 
           MVKL    .S2     __addd,B6         ; |71| 
           MVKH    .S2     __addd,B6         ; |71| 
           CALL    .S2     B6                ; |71| 
           MV      .L2X    A5,B5             ; |71| 
           ADDKPC  .S2     RL31,B3,0         ; |71| 
           MV      .S1     A15,A5            ; |71| 
           MV      .L2X    A4,B4             ; |71| 
           MV      .L1X    B12,A4            ; |71| 
RL31:      ; CALL OCCURS {__addd}            ; |71| 
           MVKL    .S2     __mpyd,B6         ; |71| 
           MVKH    .S2     __mpyd,B6         ; |71| 
           CALL    .S2     B6                ; |71| 
           MV      .S1     A4,A13            ; |71| 
           MV      .L1X    B10,A4            ; |71| 
           ADDKPC  .S2     RL32,B3,0         ; |71| 
           MV      .S1     A5,A12            ; |71| 

           MV      .L1X    B11,A5            ; |71| 
||         MV      .L2     B11,B5            ; |71| 
||         MV      .D2     B10,B4            ; |71| 

RL32:      ; CALL OCCURS {__mpyd}            ; |71| 
           MVKL    .S1     __addd,A3         ; |71| 
           MVKH    .S1     __addd,A3         ; |71| 
           MV      .L2X    A4,B4             ; |71| 
           CALL    .S2X    A3                ; |71| 
           ADDKPC  .S2     RL33,B3,1         ; |71| 
           MV      .S1     A13,A4            ; |71| 
           MV      .L2X    A5,B5             ; |71| 
           MV      .L1     A12,A5            ; |71| 
RL33:      ; CALL OCCURS {__addd}            ; |71| 
           MVKL    .S2     _sqrt,B4          ; |71| 
           MVKH    .S2     _sqrt,B4          ; |71| 
           CALL    .S2     B4                ; |71| 
           ADDKPC  .S2     RL34,B3,4         ; |71| 
RL34:      ; CALL OCCURS {_sqrt}             ; |71| 
           MVK     .S2     14528,B4          ; |72| 

           MVKL    .S2     _Atmosphere_Drag_Factor,B5 ; |72| 
||         ADD     .L2X    B4,A14,B4         ; |72| 

           MVKH    .S2     _Atmosphere_Drag_Factor,B5 ; |72| 
||         LDDW    .D2T1   *B4,A9:A8         ; |72| 

           ADD     .D1     A10,24,A3         ; |72| 
||         CALL    .S2     B5                ; |72| 

           LDDW    .D1T1   *+A10(48),A7:A6   ; |72| 
           MV      .L2X    A3,B4             ; |72| 
           MV      .L1     A4,A13            ; |71| 

           MV      .L2X    A8,B6             ; |72| 
||         MVK     .S1     14496,A4          ; |72| 

           MV      .S1     A5,A12            ; |71| 
||         ADD     .L1     A4,A14,A4         ; |72| 
||         MV      .L2X    A9,B7             ; |72| 
||         ADDKPC  .S2     RL35,B3,0         ; |72| 

RL35:      ; CALL OCCURS {_Atmosphere_Drag_Factor}  ; |72| 
           MVKL    .S2     __mpyd,B6         ; |73| 
           MVKH    .S2     __mpyd,B6         ; |73| 
           CALL    .S2     B6                ; |73| 
           MV      .L2X    A13,B4            ; |73| 
           MV      .L2X    A12,B5            ; |73| 
           ADDKPC  .S2     RL36,B3,2         ; |73| 
RL36:      ; CALL OCCURS {__mpyd}            ; |73| 
           MVKL    .S1     __mpyd,A3         ; |74| 
           MVKH    .S1     __mpyd,A3         ; |74| 
           LDDW    .D1T1   *A10,A7:A6        ; |74| 
           CALL    .S2X    A3                ; |74| 
           MV      .L2X    A4,B4             ; |73| 
           MV      .L1     A4,A13            ; |73| 
           MV      .L2X    A5,B5             ; |73| 
           MV      .D1     A5,A12            ; |73| 

           MV      .L1     A7,A5             ; |74| 
||         MV      .S1     A6,A4             ; |74| 
||         ADDKPC  .S2     RL37,B3,0         ; |74| 

RL37:      ; CALL OCCURS {__mpyd}            ; |74| 
           MVKL    .S1     __mpyd,A3         ; |75| 
           MVKH    .S1     __mpyd,A3         ; |75| 
           LDDW    .D1T1   *+A10(8),A7:A6    ; |75| 
           CALL    .S2X    A3                ; |75| 
           STDW    .D2T1   A5:A4,*+SP(56)    ; |74| 
           MV      .L2X    A12,B5            ; |75| 
           ADDKPC  .S2     RL38,B3,0         ; |75| 
           MV      .L2X    A13,B4            ; |75| 

           MV      .S1     A6,A4             ; |75| 
||         MV      .L1     A7,A5             ; |75| 

RL38:      ; CALL OCCURS {__mpyd}            ; |75| 
           MVKL    .S2     __mpyd,B6         ; |76| 
           MVKH    .S2     __mpyd,B6         ; |76| 

           LDDW    .D1T1   *+A10(16),A7:A6   ; |76| 
||         CALL    .S2     B6                ; |76| 

           STDW    .D2T1   A5:A4,*+SP(64)    ; |75| 
           MV      .L2X    A12,B5            ; |76| 
           ADDKPC  .S2     RL39,B3,0         ; |76| 
           MV      .L2X    A13,B4            ; |76| 

           MV      .S1     A6,A4             ; |76| 
||         MV      .L1     A7,A5             ; |76| 

RL39:      ; CALL OCCURS {__mpyd}            ; |76| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     _Geopotential_Gravitation,B5 ; |79| 
           MVKH    .S2     _Geopotential_Gravitation,B5 ; |79| 

           CALL    .S2     B5                ; |79| 
||         MVK     .S1     3630,A4           ; |79| 
||         MV      .L1     A4,A8             ; |76| 

           LDW     .D1T1   *+A14[A4],A6      ; |79| 
           ADD     .D1     A10,24,A3         ; |79| 
           ADD     .L2     8,SP,B6           ; |79| 
           MV      .L1     A5,A9             ; |76| 

           ADDKPC  .S2     RL40,B3,0         ; |79| 
||         MV      .L1     A14,A4            ; |79| 
||         STDW    .D2T1   A9:A8,*+SP(72)    ; |76| 
||         MV      .L2X    A3,B4             ; |79| 

RL40:      ; CALL OCCURS {_Geopotential_Gravitation}  ; |79| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _SunMoonData_Gravitation,A3 ; |82| 
           MVKH    .S1     _SunMoonData_Gravitation,A3 ; |82| 
           LDDW    .D1T1   *+A10(48),A7:A6   ; |82| 

           CALL    .S2X    A3                ; |82| 
||         MVK     .S1     3631,A4           ; |82| 

           LDW     .D1T2   *+A14[A4],B6      ; |82| 
           ADD     .D1     A10,24,A9         ; |82| 
           ADDAD   .D2     SP,4,B4           ; |82| 
           MVK     .S1     14448,A5          ; |82| 

           ADDKPC  .S2     RL41,B3,0         ; |82| 
||         MV      .L2X    A9,B4             ; |82| 
||         ADD     .L1     A5,A14,A4         ; |82| 
||         MV      .S1X    B4,A8             ; |82| 

RL41:      ; CALL OCCURS {_SunMoonData_Gravitation}  ; |82| 
           LDDW    .D1T1   *A10,A5:A4        ; |85| 
           MVKL    .S1     __subd,A3         ; |88| 
           MVKH    .S1     __subd,A3         ; |88| 
           LDDW    .D2T2   *+SP(8),B7:B6     ; |88| 
           LDDW    .D2T2   *+SP(56),B5:B4    ; |88| 
           STDW    .D1T1   A5:A4,*+A11(24)   ; |85| 
           LDDW    .D1T1   *+A10(8),A5:A4    ; |86| 
           NOP             4
           STDW    .D1T1   A5:A4,*+A11(32)   ; |86| 

           CALL    .S2X    A3                ; |88| 
||         LDDW    .D1T1   *+A10(16),A7:A6   ; |87| 

           MV      .L1X    B7,A5             ; |88| 
           ADDKPC  .S2     RL51,B3,1         ; |88| 
           MV      .L1X    B6,A4             ; |88| 
           STDW    .D1T1   A7:A6,*+A11(40)   ; |87| 
RL51:      ; CALL OCCURS {__subd}            ; |88| 
           MVKL    .S1     __addd,A3         ; |88| 
           MVKH    .S1     __addd,A3         ; |88| 
           LDDW    .D2T2   *+SP(32),B5:B4    ; |88| 
           CALL    .S2X    A3                ; |88| 
           ADDKPC  .S2     RL52,B3,4         ; |88| 
RL52:      ; CALL OCCURS {__addd}            ; |88| 
           MVKL    .S2     __mpyd,B6         ; |88| 
           MVKH    .S2     __mpyd,B6         ; |88| 

           LDDW    .D1T1   *+A10(24),A7:A6   ; |88| 
||         CALL    .S2     B6                ; |88| 

           MVKL    .S2     0x3e36d6a5,B5     ; |88| 
           MVKL    .S2     0x97d265b1,B4     ; |88| 
           MVKH    .S2     0x3e36d6a5,B5     ; |88| 

           MV      .L1     A4,A13            ; |88| 
||         MVKH    .S2     0x97d265b1,B4     ; |88| 

           MV      .L1     A7,A5             ; |88| 
||         MV      .D1     A5,A12            ; |88| 
||         MV      .S1     A6,A4             ; |88| 
||         ADDKPC  .S2     RL53,B3,0         ; |88| 

RL53:      ; CALL OCCURS {__mpyd}            ; |88| 
           MVKL    .S1     __addd,A3         ; |88| 
           MVKH    .S1     __addd,A3         ; |88| 
           MV      .L2X    A4,B4             ; |88| 
           CALL    .S2X    A3                ; |88| 
           MV      .L2X    A5,B5             ; |88| 
           MV      .S1     A13,A4            ; |88| 
           ADDKPC  .S2     RL54,B3,1         ; |88| 
           MV      .L1     A12,A5            ; |88| 
RL54:      ; CALL OCCURS {__addd}            ; |88| 
           MVKL    .S1     __mpyd,A3         ; |88| 
           MVKH    .S1     __mpyd,A3         ; |88| 
           LDDW    .D1T1   *+A10(8),A7:A6    ; |88| 
           CALL    .S2X    A3                ; |88| 
           MVKL    .S2     0x3f231da7,B5     ; |88| 
           MVKL    .S2     0xd7cb8d5b,B4     ; |88| 
           MVKH    .S2     0x3f231da7,B5     ; |88| 

           MVKH    .S2     0xd7cb8d5b,B4     ; |88| 
||         MV      .L1     A4,A12            ; |88| 

           MV      .L1     A7,A5             ; |88| 
||         ADDKPC  .S2     RL55,B3,0         ; |88| 
||         MV      .S1     A6,A4             ; |88| 
||         MV      .D1     A5,A13            ; |88| 

RL55:      ; CALL OCCURS {__mpyd}            ; |88| 
           MVKL    .S2     __addd,B6         ; |88| 
           MVKH    .S2     __addd,B6         ; |88| 
           CALL    .S2     B6                ; |88| 
           MV      .L2X    A4,B4             ; |88| 
           ADDKPC  .S2     RL56,B3,0         ; |88| 
           MV      .S1     A12,A4            ; |88| 
           MV      .L2X    A5,B5             ; |88| 
           MV      .L1     A13,A5            ; |88| 
RL56:      ; CALL OCCURS {__addd}            ; |88| 
           MVKL    .S1     __subd,A3         ; |89| 
           MVKH    .S1     __subd,A3         ; |89| 
           LDDW    .D2T2   *+SP(16),B7:B6    ; |89| 
           CALL    .S2X    A3                ; |89| 
           LDDW    .D2T2   *+SP(64),B5:B4    ; |89| 
           STDW    .D1T1   A5:A4,*A11        ; |88| 
           ADDKPC  .S2     RL66,B3,0         ; |89| 
           MV      .L1X    B6,A4             ; |89| 
           MV      .L1X    B7,A5             ; |89| 
RL66:      ; CALL OCCURS {__subd}            ; |89| 
           MVKL    .S1     __addd,A3         ; |89| 
           MVKH    .S1     __addd,A3         ; |89| 
           LDDW    .D2T2   *+SP(40),B5:B4    ; |89| 
           CALL    .S2X    A3                ; |89| 
           ADDKPC  .S2     RL67,B3,4         ; |89| 
RL67:      ; CALL OCCURS {__addd}            ; |89| 
           MVKL    .S1     __mpyd,A3         ; |89| 
           MVKH    .S1     __mpyd,A3         ; |89| 
           LDDW    .D1T1   *+A10(32),A7:A6   ; |89| 
           CALL    .S2X    A3                ; |89| 
           MVKL    .S2     0x3e36d6a5,B5     ; |89| 
           MVKL    .S2     0x97d265b1,B4     ; |89| 
           MVKH    .S2     0x3e36d6a5,B5     ; |89| 

           MVKH    .S2     0x97d265b1,B4     ; |89| 
||         MV      .L1     A4,A12            ; |89| 

           MV      .L1     A7,A5             ; |89| 
||         ADDKPC  .S2     RL68,B3,0         ; |89| 
||         MV      .S1     A6,A4             ; |89| 
||         MV      .D1     A5,A13            ; |89| 

RL68:      ; CALL OCCURS {__mpyd}            ; |89| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |89| 
           MVKH    .S1     __addd,A3         ; |89| 
           MV      .L2X    A4,B4             ; |89| 
           CALL    .S2X    A3                ; |89| 
           ADDKPC  .S2     RL69,B3,1         ; |89| 
           MV      .S1     A12,A4            ; |89| 
           MV      .L2X    A5,B5             ; |89| 
           MV      .L1     A13,A5            ; |89| 
RL69:      ; CALL OCCURS {__addd}            ; |89| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |89| 
           MVKH    .S1     __mpyd,A3         ; |89| 
           LDDW    .D1T1   *A10,A7:A6        ; |89| 
           CALL    .S2X    A3                ; |89| 
           MVKL    .S2     0x3f231da7,B5     ; |89| 
           MVKL    .S2     0xd7cb8d5b,B4     ; |89| 
           MVKH    .S2     0x3f231da7,B5     ; |89| 

           MVKH    .S2     0xd7cb8d5b,B4     ; |89| 
||         MV      .L1     A5,A12            ; |89| 

           ADDKPC  .S2     RL70,B3,0         ; |89| 
||         MV      .L1     A6,A4             ; |89| 
||         MV      .S1     A7,A5             ; |89| 
||         MV      .D1     A4,A10            ; |89| 

RL70:      ; CALL OCCURS {__mpyd}            ; |89| 
           MVKL    .S2     __subd,B6         ; |89| 
           MVKH    .S2     __subd,B6         ; |89| 
           CALL    .S2     B6                ; |89| 
           MV      .L2X    A4,B4             ; |89| 
           ADDKPC  .S2     RL71,B3,0         ; |89| 
           MV      .S1     A10,A4            ; |89| 
           MV      .L2X    A5,B5             ; |89| 
           MV      .L1     A12,A5            ; |89| 
RL71:      ; CALL OCCURS {__subd}            ; |89| 
           MVKL    .S1     __subd,A3         ; |90| 
           MVKH    .S1     __subd,A3         ; |90| 
           LDDW    .D2T2   *+SP(24),B7:B6    ; |90| 
           CALL    .S2X    A3                ; |90| 
           LDDW    .D2T2   *+SP(72),B5:B4    ; |90| 
           STDW    .D1T1   A5:A4,*+A11(8)    ; |89| 
           ADDKPC  .S2     RL73,B3,0         ; |90| 
           MV      .L1X    B6,A4             ; |90| 
           MV      .L1X    B7,A5             ; |90| 
RL73:      ; CALL OCCURS {__subd}            ; |90| 
           MVKL    .S2     __addd,B6         ; |90| 
           MVKH    .S2     __addd,B6         ; |90| 
           CALL    .S2     B6                ; |90| 
           LDDW    .D2T2   *+SP(48),B5:B4    ; |90| 
           ADDKPC  .S2     RL74,B3,3         ; |90| 
RL74:      ; CALL OCCURS {__addd}            ; |90| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     0x3ee845c8,A7     ; |91| 

           STDW    .D1T1   A5:A4,*+A11(16)   ; |90| 
||         MVKL    .S1     0xa0ce5129,A6     ; |91| 
||         MV      .L1X    SP,A31            ; |94| 

           LDDW    .D1T1   *+A31(96),A13:A12 ; |94| 
||         MVKH    .S1     0x3ee845c8,A7     ; |91| 

           LDDW    .D1T1   *+A31(104),A15:A14 ; |94| 
||         MVKH    .S1     0xa0ce5129,A6     ; |91| 
||         LDDW    .D2T2   *+SP(112),B11:B10 ; |94| 
||         MV      .L2     B13,B3            ; |94| 

           RET     .S2     B3                ; |94| 
||         STDW    .D1T1   A7:A6,*+A11(48)   ; |91| 
||         LDW     .D2T2   *+SP(84),B13      ; |94| 

           LDW     .D2T2   *++SP(120),B12    ; |94| 
||         LDDW    .D1T1   *+A31(88),A11:A10 ; |94| 

	.dwpsn	"assumedDynamics.c",94,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |94| 
	.dwattr DW$23, DW_AT_end_file("assumedDynamics.c")
	.dwattr DW$23, DW_AT_end_line(0x5e)
	.dwattr DW$23, DW_AT_end_column(0x01)
	.dwendtag DW$23

;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_Atmosphere_Drag_Factor
	.global	_Geopotential_Gravitation
	.global	_SunMoonData_Gravitation
	.global	_DynamicsModel_Init
	.global	_sqrt
	.global	__mpyd
	.global	__addd
	.global	__subd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

DW$T$33	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$33, DW_AT_language(DW_LANG_C)
DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$31)
DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$21)
DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
	.dwendtag DW$T$33


DW$T$37	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$37, DW_AT_language(DW_LANG_C)
DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$35)
DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$36)
DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
	.dwendtag DW$T$37


DW$T$41	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$41, DW_AT_language(DW_LANG_C)
DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$40)
	.dwendtag DW$T$41


DW$T$43	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$43, DW_AT_language(DW_LANG_C)
DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$21)
	.dwendtag DW$T$43

DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("int32"), DW_AT_type(*DW$T$10)
	.dwattr DW$T$21, DW_AT_language(DW_LANG_C)
DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("uint32"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$36, DW_AT_language(DW_LANG_C)
DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)
DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)
DW$T$32	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$32, DW_AT_address_class(0x20)

DW$T$50	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$50, DW_AT_language(DW_LANG_C)
DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$49)
DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$50


DW$T$51	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$51, DW_AT_language(DW_LANG_C)
DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$T$51

DW$T$49	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$29)
	.dwattr DW$T$49, DW_AT_address_class(0x20)
DW$T$31	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$27)
	.dwattr DW$T$31, DW_AT_address_class(0x20)
DW$T$35	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$28)
	.dwattr DW$T$35, DW_AT_address_class(0x20)
DW$T$40	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$39)
	.dwattr DW$T$40, DW_AT_address_class(0x20)
DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)
DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("tAtmosphere"), DW_AT_type(*DW$T$20)
	.dwattr DW$T$29, DW_AT_language(DW_LANG_C)
DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("tGeopotential"), DW_AT_type(*DW$T$24)
	.dwattr DW$T$27, DW_AT_language(DW_LANG_C)
DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("tSunMoonData"), DW_AT_type(*DW$T$26)
	.dwattr DW$T$28, DW_AT_language(DW_LANG_C)
DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("tDynamicsModel"), DW_AT_type(*DW$T$30)
	.dwattr DW$T$39, DW_AT_language(DW_LANG_C)

DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$20, DW_AT_name("__tAtmosphere")
	.dwattr DW$T$20, DW_AT_byte_size(0x18)
DW$45	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$45, DW_AT_name("mjdMean_t"), DW_AT_symbol_name("_mjdMean_t")
	.dwattr DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$45, DW_AT_accessibility(DW_ACCESS_public)
DW$46	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$46, DW_AT_name("mjdTau"), DW_AT_symbol_name("_mjdTau")
	.dwattr DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$46, DW_AT_accessibility(DW_ACCESS_public)
DW$47	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$47, DW_AT_name("Mean_AF"), DW_AT_symbol_name("_Mean_AF")
	.dwattr DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$47, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$20


DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$24, DW_AT_name("__tGeopotential")
	.dwattr DW$T$24, DW_AT_byte_size(0x3870)
DW$48	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$21)
	.dwattr DW$48, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$48, DW_AT_accessibility(DW_ACCESS_public)
DW$49	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$23)
	.dwattr DW$49, DW_AT_name("NormFact"), DW_AT_symbol_name("_NormFact")
	.dwattr DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$49, DW_AT_accessibility(DW_ACCESS_public)
DW$50	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$23)
	.dwattr DW$50, DW_AT_name("C"), DW_AT_symbol_name("_C")
	.dwattr DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0xb50]
	.dwattr DW$50, DW_AT_accessibility(DW_ACCESS_public)
DW$51	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$23)
	.dwattr DW$51, DW_AT_name("S"), DW_AT_symbol_name("_S")
	.dwattr DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x1698]
	.dwattr DW$51, DW_AT_accessibility(DW_ACCESS_public)
DW$52	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$23)
	.dwattr DW$52, DW_AT_name("P"), DW_AT_symbol_name("_P")
	.dwattr DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x21e0]
	.dwattr DW$52, DW_AT_accessibility(DW_ACCESS_public)
DW$53	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$23)
	.dwattr DW$53, DW_AT_name("dPfi"), DW_AT_symbol_name("_dPfi")
	.dwattr DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d28]
	.dwattr DW$53, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$24


DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$26, DW_AT_name("__tSunMoonData")
	.dwattr DW$T$26, DW_AT_byte_size(0x30)
DW$54	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$54, DW_AT_name("mjdTau"), DW_AT_symbol_name("_mjdTau")
	.dwattr DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$54, DW_AT_accessibility(DW_ACCESS_public)
DW$55	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$55, DW_AT_name("dATex"), DW_AT_symbol_name("_dATex")
	.dwattr DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$55, DW_AT_accessibility(DW_ACCESS_public)
DW$56	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$56, DW_AT_name("mjdMean_t"), DW_AT_symbol_name("_mjdMean_t")
	.dwattr DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$56, DW_AT_accessibility(DW_ACCESS_public)
DW$57	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$25)
	.dwattr DW$57, DW_AT_name("Mean_g"), DW_AT_symbol_name("_Mean_g")
	.dwattr DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$57, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$26


DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$30, DW_AT_name("_tDynamicsModel")
	.dwattr DW$T$30, DW_AT_byte_size(0x38c8)
DW$58	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$27)
	.dwattr DW$58, DW_AT_name("GP"), DW_AT_symbol_name("_GP")
	.dwattr DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$58, DW_AT_accessibility(DW_ACCESS_public)
DW$59	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$28)
	.dwattr DW$59, DW_AT_name("SMD"), DW_AT_symbol_name("_SMD")
	.dwattr DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x3870]
	.dwattr DW$59, DW_AT_accessibility(DW_ACCESS_public)
DW$60	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$29)
	.dwattr DW$60, DW_AT_name("Atm"), DW_AT_symbol_name("_Atm")
	.dwattr DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x38a0]
	.dwattr DW$60, DW_AT_accessibility(DW_ACCESS_public)
DW$61	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$21)
	.dwattr DW$61, DW_AT_name("GP_N"), DW_AT_symbol_name("_GP_N")
	.dwattr DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x38b8]
	.dwattr DW$61, DW_AT_accessibility(DW_ACCESS_public)
DW$62	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$21)
	.dwattr DW$62, DW_AT_name("SM_fUsing"), DW_AT_symbol_name("_SM_fUsing")
	.dwattr DW$62, DW_AT_data_member_location[DW_OP_plus_uconst 0x38bc]
	.dwattr DW$62, DW_AT_accessibility(DW_ACCESS_public)
DW$63	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$63, DW_AT_name("OBJ_Sigma"), DW_AT_symbol_name("_OBJ_Sigma")
	.dwattr DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x38c0]
	.dwattr DW$63, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$30


DW$T$23	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$23, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$23, DW_AT_byte_size(0xb48)
DW$64	.dwtag  DW_TAG_subrange_type
	.dwattr DW$64, DW_AT_upper_bound(0x12)
DW$65	.dwtag  DW_TAG_subrange_type
	.dwattr DW$65, DW_AT_upper_bound(0x12)
	.dwendtag DW$T$23


DW$T$25	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$25, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$25, DW_AT_byte_size(0x18)
DW$66	.dwtag  DW_TAG_subrange_type
	.dwattr DW$66, DW_AT_upper_bound(0x02)
	.dwendtag DW$T$25


	.dwattr DW$23, DW_AT_external(0x01)
	.dwattr DW$21, DW_AT_external(0x01)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$67	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$67, DW_AT_location[DW_OP_reg0]
DW$68	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$68, DW_AT_location[DW_OP_reg1]
DW$69	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$69, DW_AT_location[DW_OP_reg2]
DW$70	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$70, DW_AT_location[DW_OP_reg3]
DW$71	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$71, DW_AT_location[DW_OP_reg4]
DW$72	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$72, DW_AT_location[DW_OP_reg5]
DW$73	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$73, DW_AT_location[DW_OP_reg6]
DW$74	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$74, DW_AT_location[DW_OP_reg7]
DW$75	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$75, DW_AT_location[DW_OP_reg8]
DW$76	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$76, DW_AT_location[DW_OP_reg9]
DW$77	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$77, DW_AT_location[DW_OP_reg10]
DW$78	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$78, DW_AT_location[DW_OP_reg11]
DW$79	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$79, DW_AT_location[DW_OP_reg12]
DW$80	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$80, DW_AT_location[DW_OP_reg13]
DW$81	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$81, DW_AT_location[DW_OP_reg14]
DW$82	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$82, DW_AT_location[DW_OP_reg15]
DW$83	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$83, DW_AT_location[DW_OP_reg16]
DW$84	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$84, DW_AT_location[DW_OP_reg17]
DW$85	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$85, DW_AT_location[DW_OP_reg18]
DW$86	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$86, DW_AT_location[DW_OP_reg19]
DW$87	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$87, DW_AT_location[DW_OP_reg20]
DW$88	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$88, DW_AT_location[DW_OP_reg21]
DW$89	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$89, DW_AT_location[DW_OP_reg22]
DW$90	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$90, DW_AT_location[DW_OP_reg23]
DW$91	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$91, DW_AT_location[DW_OP_reg24]
DW$92	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$92, DW_AT_location[DW_OP_reg25]
DW$93	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$93, DW_AT_location[DW_OP_reg26]
DW$94	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$94, DW_AT_location[DW_OP_reg27]
DW$95	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$95, DW_AT_location[DW_OP_reg28]
DW$96	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$96, DW_AT_location[DW_OP_reg29]
DW$97	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$97, DW_AT_location[DW_OP_reg30]
DW$98	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$98, DW_AT_location[DW_OP_reg31]
DW$99	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$99, DW_AT_location[DW_OP_regx 0x20]
DW$100	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$100, DW_AT_location[DW_OP_regx 0x21]
DW$101	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$101, DW_AT_location[DW_OP_regx 0x22]
DW$102	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$102, DW_AT_location[DW_OP_regx 0x23]
DW$103	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$103, DW_AT_location[DW_OP_regx 0x24]
DW$104	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$104, DW_AT_location[DW_OP_regx 0x25]
DW$105	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$105, DW_AT_location[DW_OP_regx 0x26]
DW$106	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$106, DW_AT_location[DW_OP_regx 0x27]
DW$107	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$107, DW_AT_location[DW_OP_regx 0x28]
DW$108	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$108, DW_AT_location[DW_OP_regx 0x29]
DW$109	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$109, DW_AT_location[DW_OP_regx 0x2a]
DW$110	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$110, DW_AT_location[DW_OP_regx 0x2b]
DW$111	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$111, DW_AT_location[DW_OP_regx 0x2c]
DW$112	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$112, DW_AT_location[DW_OP_regx 0x2d]
DW$113	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$113, DW_AT_location[DW_OP_regx 0x2e]
DW$114	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$114, DW_AT_location[DW_OP_regx 0x2f]
DW$115	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$115, DW_AT_location[DW_OP_regx 0x30]
DW$116	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$116, DW_AT_location[DW_OP_regx 0x31]
DW$117	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$117, DW_AT_location[DW_OP_regx 0x32]
DW$118	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$118, DW_AT_location[DW_OP_regx 0x33]
DW$119	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$119, DW_AT_location[DW_OP_regx 0x34]
DW$120	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$120, DW_AT_location[DW_OP_regx 0x35]
DW$121	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$121, DW_AT_location[DW_OP_regx 0x36]
DW$122	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$122, DW_AT_location[DW_OP_regx 0x37]
DW$123	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$123, DW_AT_location[DW_OP_regx 0x38]
DW$124	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$124, DW_AT_location[DW_OP_regx 0x39]
DW$125	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$125, DW_AT_location[DW_OP_regx 0x3a]
DW$126	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$126, DW_AT_location[DW_OP_regx 0x3b]
DW$127	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$127, DW_AT_location[DW_OP_regx 0x3c]
DW$128	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$128, DW_AT_location[DW_OP_regx 0x3d]
DW$129	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$129, DW_AT_location[DW_OP_regx 0x3e]
DW$130	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$130, DW_AT_location[DW_OP_regx 0x3f]
DW$131	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$131, DW_AT_location[DW_OP_regx 0x40]
DW$132	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$132, DW_AT_location[DW_OP_regx 0x41]
DW$133	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$133, DW_AT_location[DW_OP_regx 0x42]
DW$134	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$134, DW_AT_location[DW_OP_regx 0x43]
DW$135	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$135, DW_AT_location[DW_OP_regx 0x44]
DW$136	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$136, DW_AT_location[DW_OP_regx 0x45]
DW$137	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$137, DW_AT_location[DW_OP_regx 0x46]
DW$138	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$138, DW_AT_location[DW_OP_regx 0x47]
DW$139	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$139, DW_AT_location[DW_OP_regx 0x48]
DW$140	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$140, DW_AT_location[DW_OP_regx 0x49]
DW$141	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$141, DW_AT_location[DW_OP_regx 0x4a]
DW$142	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$142, DW_AT_location[DW_OP_regx 0x4b]
DW$143	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$143, DW_AT_location[DW_OP_regx 0x4c]
DW$144	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$144, DW_AT_location[DW_OP_regx 0x4d]
DW$145	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$145, DW_AT_location[DW_OP_regx 0x4e]
DW$146	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$146, DW_AT_location[DW_OP_regx 0x4f]
DW$147	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$147, DW_AT_location[DW_OP_regx 0x50]
DW$148	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$148, DW_AT_location[DW_OP_regx 0x51]
DW$149	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$149, DW_AT_location[DW_OP_regx 0x52]
DW$150	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$150, DW_AT_location[DW_OP_regx 0x53]
DW$151	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$151, DW_AT_location[DW_OP_regx 0x54]
DW$152	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$152, DW_AT_location[DW_OP_regx 0x55]
DW$153	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$153, DW_AT_location[DW_OP_regx 0x56]
DW$154	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$154, DW_AT_location[DW_OP_regx 0x57]
DW$155	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$155, DW_AT_location[DW_OP_regx 0x58]
DW$156	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$156, DW_AT_location[DW_OP_regx 0x59]
DW$157	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$157, DW_AT_location[DW_OP_regx 0x5a]
DW$158	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$158, DW_AT_location[DW_OP_regx 0x5b]
DW$159	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$159, DW_AT_location[DW_OP_regx 0x5c]
DW$160	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$160, DW_AT_location[DW_OP_regx 0x5d]
DW$161	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$161, DW_AT_location[DW_OP_regx 0x5e]
DW$162	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$162, DW_AT_location[DW_OP_regx 0x5f]
DW$163	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$163, DW_AT_location[DW_OP_regx 0x60]
DW$164	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$164, DW_AT_location[DW_OP_regx 0x61]
DW$165	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$165, DW_AT_location[DW_OP_regx 0x62]
DW$166	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$166, DW_AT_location[DW_OP_regx 0x63]
DW$167	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$167, DW_AT_location[DW_OP_regx 0x64]
DW$168	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$168, DW_AT_location[DW_OP_regx 0x65]
DW$169	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$169, DW_AT_location[DW_OP_regx 0x66]
DW$170	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$170, DW_AT_location[DW_OP_regx 0x67]
DW$171	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$171, DW_AT_location[DW_OP_regx 0x68]
DW$172	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$172, DW_AT_location[DW_OP_regx 0x69]
DW$173	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$173, DW_AT_location[DW_OP_regx 0x6a]
DW$174	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$174, DW_AT_location[DW_OP_regx 0x6b]
DW$175	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$175, DW_AT_location[DW_OP_regx 0x6c]
DW$176	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$176, DW_AT_location[DW_OP_regx 0x6d]
DW$177	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$177, DW_AT_location[DW_OP_regx 0x6e]
DW$178	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$178, DW_AT_location[DW_OP_regx 0x6f]
DW$179	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$179, DW_AT_location[DW_OP_regx 0x70]
DW$180	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$180, DW_AT_location[DW_OP_regx 0x71]
DW$181	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$181, DW_AT_location[DW_OP_regx 0x72]
DW$182	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$182, DW_AT_location[DW_OP_regx 0x73]
DW$183	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$183, DW_AT_location[DW_OP_regx 0x74]
DW$184	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$184, DW_AT_location[DW_OP_regx 0x75]
DW$185	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$185, DW_AT_location[DW_OP_regx 0x76]
DW$186	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$186, DW_AT_location[DW_OP_regx 0x77]
DW$187	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$187, DW_AT_location[DW_OP_regx 0x78]
DW$188	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$188, DW_AT_location[DW_OP_regx 0x79]
DW$189	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$189, DW_AT_location[DW_OP_regx 0x7a]
DW$190	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$190, DW_AT_location[DW_OP_regx 0x7b]
DW$191	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$191, DW_AT_location[DW_OP_regx 0x7c]
DW$192	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$192, DW_AT_location[DW_OP_regx 0x7d]
DW$193	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$193, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

