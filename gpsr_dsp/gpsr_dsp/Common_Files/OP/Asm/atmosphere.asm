;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:48:51 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("atmosphere.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("memset"), DW_AT_symbol_name("_memset")
	.dwattr DW$1, DW_AT_type(*DW$T$3)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$21)
	.dwendtag DW$1


DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt"), DW_AT_symbol_name("_sqrt")
	.dwattr DW$5, DW_AT_type(*DW$T$17)
	.dwattr DW$5, DW_AT_declaration(0x01)
	.dwattr DW$5, DW_AT_external(0x01)
DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$5


DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("exp"), DW_AT_symbol_name("_exp")
	.dwattr DW$7, DW_AT_type(*DW$T$17)
	.dwattr DW$7, DW_AT_declaration(0x01)
	.dwattr DW$7, DW_AT_external(0x01)
DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$7

;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI7322 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI7324 
	.sect	".text"
	.global	_Atmosphere_Init

DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("Atmosphere_Init"), DW_AT_symbol_name("_Atmosphere_Init")
	.dwattr DW$9, DW_AT_low_pc(_Atmosphere_Init)
	.dwattr DW$9, DW_AT_high_pc(0x00)
	.dwattr DW$9, DW_AT_begin_file("atmosphere.c")
	.dwattr DW$9, DW_AT_begin_line(0x17)
	.dwattr DW$9, DW_AT_begin_column(0x06)
	.dwattr DW$9, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr DW$9, DW_AT_skeletal(0x01)
	.dwpsn	"atmosphere.c",24,1

;******************************************************************************
;* FUNCTION NAME: _Atmosphere_Init                                            *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,*
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Local Frame Size  : 0 Args + 0 Auto + 4 Save = 4 byte                    *
;******************************************************************************
_Atmosphere_Init:
;** --------------------------------------------------------------------------*
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pAtm"), DW_AT_symbol_name("_pAtm")
	.dwattr DW$10, DW_AT_type(*DW$T$26)
	.dwattr DW$10, DW_AT_location[DW_OP_reg4]
           MVKL    .S1     _memset,A3        ; |26| 
           MVKH    .S1     _memset,A3        ; |26| 
           ZERO    .L2     B4                ; |26| 
           CALLRET .S2X    A3                ; |26| 
           NOP             4
	.dwpsn	"atmosphere.c",29,1
           MVK     .S1     0x18,A6           ; |26| 
RL0:       ; CALL-RETURN OCCURS {_memset}    ; |26| 
	.dwattr DW$9, DW_AT_end_file("atmosphere.c")
	.dwattr DW$9, DW_AT_end_line(0x1d)
	.dwattr DW$9, DW_AT_end_column(0x01)
	.dwendtag DW$9

	.sect	".text"
	.global	_Atmosphere_Drag_Factor

DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("Atmosphere_Drag_Factor"), DW_AT_symbol_name("_Atmosphere_Drag_Factor")
	.dwattr DW$11, DW_AT_low_pc(_Atmosphere_Drag_Factor)
	.dwattr DW$11, DW_AT_high_pc(0x00)
	.dwattr DW$11, DW_AT_begin_file("atmosphere.c")
	.dwattr DW$11, DW_AT_begin_line(0x2a)
	.dwattr DW$11, DW_AT_begin_column(0x09)
	.dwattr DW$11, DW_AT_frame_base[DW_OP_breg31 48]
	.dwattr DW$11, DW_AT_skeletal(0x01)
	.dwpsn	"atmosphere.c",46,1

;******************************************************************************
;* FUNCTION NAME: _Atmosphere_Drag_Factor                                     *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 0 Auto + 44 Save = 44 byte                  *
;******************************************************************************
_Atmosphere_Drag_Factor:
;** --------------------------------------------------------------------------*
DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pAtm"), DW_AT_symbol_name("_pAtm")
	.dwattr DW$12, DW_AT_type(*DW$T$26)
	.dwattr DW$12, DW_AT_location[DW_OP_reg4]
DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("r"), DW_AT_symbol_name("_r")
	.dwattr DW$13, DW_AT_type(*DW$T$30)
	.dwattr DW$13, DW_AT_location[DW_OP_reg20]
DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mjd_u"), DW_AT_symbol_name("_mjd_u")
	.dwattr DW$14, DW_AT_type(*DW$T$19)
	.dwattr DW$14, DW_AT_location[DW_OP_reg6]
DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sigma"), DW_AT_symbol_name("_sigma")
	.dwattr DW$15, DW_AT_type(*DW$T$19)
	.dwattr DW$15, DW_AT_location[DW_OP_reg22]

           STW     .D2T1   A15,*SP--(48)     ; |46| 
||         MV      .L1X    SP,A31            ; |46| 

           STDW    .D2T2   B11:B10,*+SP(32)
||         MVKL    .S1     __mpyd,A3         ; |49| 

           STDW    .D2T2   B13:B12,*+SP(40)
||         MVKH    .S1     __mpyd,A3         ; |49| 
||         MV      .L2     B4,B8             ; |46| 

           LDDW    .D2T2   *B8,B5:B4         ; |49| 

           CALL    .S2X    A3                ; |49| 
||         LDDW    .D2T2   *+B8(16),B11:B10  ; |49| 

           LDDW    .D2T2   *+B8(8),B13:B12   ; |49| 

           MV      .L1X    B7,A12            ; |46| 
||         STDW    .D1T1   A13:A12,*-A31(32)

           MV      .L1X    B6,A13            ; |46| 

           MV      .L1X    B4,A4             ; |49| 
||         STDW    .D1T1   A11:A10,*-A31(40)

           ADDKPC  .S2     RL44,B3,0         ; |49| 
||         MV      .L1X    B5,A5             ; |49| 
||         STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B3,*+SP(28)

RL44:      ; CALL OCCURS {__mpyd}            ; |49| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |49| 
           MVKH    .S1     __mpyd,A3         ; |49| 
           MV      .S1     A4,A11            ; |49| 
           CALL    .S2X    A3                ; |49| 
           MV      .L1X    B12,A4            ; |49| 
           ADDKPC  .S2     RL45,B3,0         ; |49| 
           MV      .L2     B13,B5            ; |49| 
           MV      .D2     B12,B4            ; |49| 

           MV      .S1     A5,A10            ; |49| 
||         MV      .L1X    B13,A5            ; |49| 

RL45:      ; CALL OCCURS {__mpyd}            ; |49| 
           MVKL    .S1     __addd,A3         ; |49| 
           MVKH    .S1     __addd,A3         ; |49| 
           MV      .L2X    A4,B4             ; |49| 
           CALL    .S2X    A3                ; |49| 
           ADDKPC  .S2     RL46,B3,1         ; |49| 
           MV      .S1     A11,A4            ; |49| 
           MV      .L2X    A5,B5             ; |49| 
           MV      .L1     A10,A5            ; |49| 
RL46:      ; CALL OCCURS {__addd}            ; |49| 
           MVKL    .S1     __mpyd,A3         ; |49| 
           MVKH    .S1     __mpyd,A3         ; |49| 
           MV      .S1     A4,A11            ; |49| 
           CALL    .S2X    A3                ; |49| 
           MV      .L1X    B10,A4            ; |49| 
           MV      .D2     B10,B4            ; |49| 
           MV      .L2     B11,B5            ; |49| 
           MV      .S1     A5,A10            ; |49| 

           MV      .L1X    B11,A5            ; |49| 
||         ADDKPC  .S2     RL47,B3,0         ; |49| 

RL47:      ; CALL OCCURS {__mpyd}            ; |49| 
           MVKL    .S1     __addd,A3         ; |49| 
           MVKH    .S1     __addd,A3         ; |49| 
           MV      .L2X    A4,B4             ; |49| 
           CALL    .S2X    A3                ; |49| 
           MV      .L2X    A5,B5             ; |49| 
           MV      .S1     A11,A4            ; |49| 
           ADDKPC  .S2     RL48,B3,1         ; |49| 
           MV      .L1     A10,A5            ; |49| 
RL48:      ; CALL OCCURS {__addd}            ; |49| 
           MVKL    .S1     _sqrt,A3          ; |49| 
           MVKH    .S1     _sqrt,A3          ; |49| 
           NOP             1
           CALL    .S2X    A3                ; |49| 
           ADDKPC  .S2     RL49,B3,4         ; |49| 
RL49:      ; CALL OCCURS {_sqrt}             ; |49| 
           MVKL    .S1     __subd,A3         ; |49| 
           MVKH    .S1     __subd,A3         ; |49| 
           MVKL    .S2     0x415854a6,B5     ; |49| 
           CALL    .S2X    A3                ; |49| 
           ZERO    .L2     B4                ; |49| 
           MVKH    .S2     0x415854a6,B5     ; |49| 
           MVKH    .S2     0x40000000,B4     ; |49| 
           ADDKPC  .S2     RL50,B3,1         ; |49| 
RL50:      ; CALL OCCURS {__subd}            ; |49| 
           MVKL    .S1     __subd,A3         ; |52| 
           MVKH    .S1     __subd,A3         ; |52| 
           MVKL    .S2     0x411e8480,B5     ; |52| 
           CALL    .S2X    A3                ; |52| 
           MVKH    .S2     0x411e8480,B5     ; |52| 
           ZERO    .L2     B4                ; |52| 
           ADDKPC  .S2     RL51,B3,2         ; |52| 
RL51:      ; CALL OCCURS {__subd}            ; |52| 
           MVKL    .S1     __negd,A3         ; |52| 
           MVKH    .S1     __negd,A3         ; |52| 
           MV      .S1     A4,A15            ; |52| 
           CALL    .S2X    A3                ; |52| 
           MV      .L1     A5,A14            ; |52| 
           ADDKPC  .S2     RL71,B3,3         ; |52| 
RL71:      ; CALL OCCURS {__negd}            ; |52| 
           MVKL    .S2     __mpyd,B6         ; |52| 
           MVKH    .S2     __mpyd,B6         ; |52| 
           CALL    .S2     B6                ; |52| 
           MVKL    .S2     0x3fb33333,B5     ; |52| 
           MVKL    .S2     0x33333333,B4     ; |52| 
           MVKH    .S2     0x3fb33333,B5     ; |52| 

           MV      .L1     A4,A11            ; |52| 
||         MVKH    .S2     0x33333333,B4     ; |52| 

           MV      .L1     A14,A5            ; |52| 
||         MV      .D1     A5,A10            ; |52| 
||         MV      .S1     A15,A4            ; |52| 
||         ADDKPC  .S2     RL72,B3,0         ; |52| 

RL72:      ; CALL OCCURS {__mpyd}            ; |52| 
           MVKL    .S2     __addd,B6         ; |52| 
           MVKH    .S2     __addd,B6         ; |52| 
           CALL    .S2     B6                ; |52| 
           MVKL    .S2     0x40f11700,B5     ; |52| 
           MVKH    .S2     0x40f11700,B5     ; |52| 
           ZERO    .L2     B4                ; |52| 
           ADDKPC  .S2     RL73,B3,1         ; |52| 
RL73:      ; CALL OCCURS {__addd}            ; |52| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __divd,A3         ; |52| 
           MVKH    .S1     __divd,A3         ; |52| 
           MV      .L2X    A4,B4             ; |52| 
           CALL    .S2X    A3                ; |52| 
           ADDKPC  .S2     RL74,B3,1         ; |52| 
           MV      .S1     A11,A4            ; |52| 
           MV      .L2X    A5,B5             ; |52| 
           MV      .L1     A10,A5            ; |52| 
RL74:      ; CALL OCCURS {__divd}            ; |52| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _exp,A3           ; |52| 
           MVKH    .S1     _exp,A3           ; |52| 
           NOP             1
           CALL    .S2X    A3                ; |52| 
           ADDKPC  .S2     RL75,B3,4         ; |52| 
RL75:      ; CALL OCCURS {_exp}              ; |52| 
           MVKL    .S1     __mpyd,A3         ; |52| 
           MVKH    .S1     __mpyd,A3         ; |52| 
           MVKL    .S2     0x3d4c25c2,B5     ; |52| 
           CALL    .S2X    A3                ; |52| 
           MVKL    .S2     0x68497682,B4     ; |52| 
           MVKH    .S2     0x3d4c25c2,B5     ; |52| 
           MVKH    .S2     0x68497682,B4     ; |52| 
           ADDKPC  .S2     RL76,B3,1         ; |52| 
RL76:      ; CALL OCCURS {__mpyd}            ; |52| 
           MVKL    .S2     __mpyd,B6         ; |54| 
           MVKH    .S2     __mpyd,B6         ; |54| 
           CALL    .S2     B6                ; |54| 
           ZERO    .L2     B5                ; |54| 
           MV      .L1     A4,A11            ; |52| 
           MVKH    .S2     0x3fe00000,B5     ; |54| 
           ZERO    .L2     B4                ; |54| 

           MV      .D1     A5,A10            ; |52| 
||         MV      .S1     A13,A4            ; |54| 
||         ADDKPC  .S2     RL78,B3,0         ; |54| 
||         MV      .L1     A12,A5            ; |54| 

RL78:      ; CALL OCCURS {__mpyd}            ; |54| 
           MVKL    .S1     __mpyd,A3         ; |54| 
           MVKH    .S1     __mpyd,A3         ; |54| 
           MV      .L2X    A11,B4            ; |54| 
           CALL    .S2X    A3                ; |54| 
           MV      .L2X    A10,B5            ; |54| 
           ADDKPC  .S2     RL79,B3,3         ; |54| 
RL79:      ; CALL OCCURS {__mpyd}            ; |54| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(28),B3       ; |56| 
           MV      .L1X    SP,A31            ; |56| 
           LDDW    .D1T1   *+A31(16),A13:A12 ; |56| 
           LDW     .D1T1   *+A31(24),A14     ; |56| 
           LDDW    .D2T2   *+SP(40),B13:B12  ; |56| 

           RET     .S2     B3                ; |56| 
||         LDDW    .D1T1   *+A31(8),A11:A10  ; |56| 
||         LDDW    .D2T2   *+SP(32),B11:B10  ; |56| 

           LDW     .D2T1   *++SP(48),A15     ; |56| 
	.dwpsn	"atmosphere.c",56,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |56| 
	.dwattr DW$11, DW_AT_end_file("atmosphere.c")
	.dwattr DW$11, DW_AT_end_line(0x38)
	.dwattr DW$11, DW_AT_end_column(0x01)
	.dwendtag DW$11

;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_memset
	.global	_sqrt
	.global	_exp
	.global	__mpyd
	.global	__addd
	.global	__divd
	.global	__negd
	.global	__subd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr DW$T$3, DW_AT_address_class(0x20)

DW$T$22	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$3)
	.dwattr DW$T$22, DW_AT_language(DW_LANG_C)
DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$21)
	.dwendtag DW$T$22


DW$T$27	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$27, DW_AT_language(DW_LANG_C)
DW$19	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$26)
	.dwendtag DW$T$27

DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("size_t"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$21, DW_AT_language(DW_LANG_C)
DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)
DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)
DW$T$30	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$30, DW_AT_address_class(0x20)

DW$T$32	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$32, DW_AT_language(DW_LANG_C)
DW$20	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$26)
DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$30)
DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$32


DW$T$34	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$34, DW_AT_language(DW_LANG_C)
DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$T$34

DW$T$26	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$25)
	.dwattr DW$T$26, DW_AT_address_class(0x20)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)
DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("tAtmosphere"), DW_AT_type(*DW$T$20)
	.dwattr DW$T$25, DW_AT_language(DW_LANG_C)

DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$20, DW_AT_name("__tAtmosphere")
	.dwattr DW$T$20, DW_AT_byte_size(0x18)
DW$25	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$25, DW_AT_name("mjdMean_t"), DW_AT_symbol_name("_mjdMean_t")
	.dwattr DW$25, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$25, DW_AT_accessibility(DW_ACCESS_public)
DW$26	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$26, DW_AT_name("mjdTau"), DW_AT_symbol_name("_mjdTau")
	.dwattr DW$26, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$26, DW_AT_accessibility(DW_ACCESS_public)
DW$27	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$27, DW_AT_name("Mean_AF"), DW_AT_symbol_name("_Mean_AF")
	.dwattr DW$27, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$27, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$20


	.dwattr DW$11, DW_AT_external(0x01)
	.dwattr DW$11, DW_AT_type(*DW$T$19)
	.dwattr DW$9, DW_AT_external(0x01)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$28	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$28, DW_AT_location[DW_OP_reg0]
DW$29	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$29, DW_AT_location[DW_OP_reg1]
DW$30	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$30, DW_AT_location[DW_OP_reg2]
DW$31	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$31, DW_AT_location[DW_OP_reg3]
DW$32	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$32, DW_AT_location[DW_OP_reg4]
DW$33	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$33, DW_AT_location[DW_OP_reg5]
DW$34	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$34, DW_AT_location[DW_OP_reg6]
DW$35	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$35, DW_AT_location[DW_OP_reg7]
DW$36	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$36, DW_AT_location[DW_OP_reg8]
DW$37	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$37, DW_AT_location[DW_OP_reg9]
DW$38	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$38, DW_AT_location[DW_OP_reg10]
DW$39	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$39, DW_AT_location[DW_OP_reg11]
DW$40	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$40, DW_AT_location[DW_OP_reg12]
DW$41	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$41, DW_AT_location[DW_OP_reg13]
DW$42	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$42, DW_AT_location[DW_OP_reg14]
DW$43	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$43, DW_AT_location[DW_OP_reg15]
DW$44	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$44, DW_AT_location[DW_OP_reg16]
DW$45	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$45, DW_AT_location[DW_OP_reg17]
DW$46	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$46, DW_AT_location[DW_OP_reg18]
DW$47	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$47, DW_AT_location[DW_OP_reg19]
DW$48	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$48, DW_AT_location[DW_OP_reg20]
DW$49	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$49, DW_AT_location[DW_OP_reg21]
DW$50	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$50, DW_AT_location[DW_OP_reg22]
DW$51	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$51, DW_AT_location[DW_OP_reg23]
DW$52	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$52, DW_AT_location[DW_OP_reg24]
DW$53	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$53, DW_AT_location[DW_OP_reg25]
DW$54	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$54, DW_AT_location[DW_OP_reg26]
DW$55	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$55, DW_AT_location[DW_OP_reg27]
DW$56	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$56, DW_AT_location[DW_OP_reg28]
DW$57	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$57, DW_AT_location[DW_OP_reg29]
DW$58	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$58, DW_AT_location[DW_OP_reg30]
DW$59	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$59, DW_AT_location[DW_OP_reg31]
DW$60	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$60, DW_AT_location[DW_OP_regx 0x20]
DW$61	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$61, DW_AT_location[DW_OP_regx 0x21]
DW$62	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$62, DW_AT_location[DW_OP_regx 0x22]
DW$63	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$63, DW_AT_location[DW_OP_regx 0x23]
DW$64	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$64, DW_AT_location[DW_OP_regx 0x24]
DW$65	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$65, DW_AT_location[DW_OP_regx 0x25]
DW$66	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$66, DW_AT_location[DW_OP_regx 0x26]
DW$67	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$67, DW_AT_location[DW_OP_regx 0x27]
DW$68	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$68, DW_AT_location[DW_OP_regx 0x28]
DW$69	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$69, DW_AT_location[DW_OP_regx 0x29]
DW$70	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$70, DW_AT_location[DW_OP_regx 0x2a]
DW$71	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$71, DW_AT_location[DW_OP_regx 0x2b]
DW$72	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$72, DW_AT_location[DW_OP_regx 0x2c]
DW$73	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$73, DW_AT_location[DW_OP_regx 0x2d]
DW$74	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$74, DW_AT_location[DW_OP_regx 0x2e]
DW$75	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$75, DW_AT_location[DW_OP_regx 0x2f]
DW$76	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$76, DW_AT_location[DW_OP_regx 0x30]
DW$77	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$77, DW_AT_location[DW_OP_regx 0x31]
DW$78	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$78, DW_AT_location[DW_OP_regx 0x32]
DW$79	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$79, DW_AT_location[DW_OP_regx 0x33]
DW$80	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$80, DW_AT_location[DW_OP_regx 0x34]
DW$81	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$81, DW_AT_location[DW_OP_regx 0x35]
DW$82	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$82, DW_AT_location[DW_OP_regx 0x36]
DW$83	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$83, DW_AT_location[DW_OP_regx 0x37]
DW$84	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$84, DW_AT_location[DW_OP_regx 0x38]
DW$85	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$85, DW_AT_location[DW_OP_regx 0x39]
DW$86	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$86, DW_AT_location[DW_OP_regx 0x3a]
DW$87	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$87, DW_AT_location[DW_OP_regx 0x3b]
DW$88	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$88, DW_AT_location[DW_OP_regx 0x3c]
DW$89	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$89, DW_AT_location[DW_OP_regx 0x3d]
DW$90	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$90, DW_AT_location[DW_OP_regx 0x3e]
DW$91	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$91, DW_AT_location[DW_OP_regx 0x3f]
DW$92	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$92, DW_AT_location[DW_OP_regx 0x40]
DW$93	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$93, DW_AT_location[DW_OP_regx 0x41]
DW$94	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$94, DW_AT_location[DW_OP_regx 0x42]
DW$95	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$95, DW_AT_location[DW_OP_regx 0x43]
DW$96	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$96, DW_AT_location[DW_OP_regx 0x44]
DW$97	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$97, DW_AT_location[DW_OP_regx 0x45]
DW$98	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$98, DW_AT_location[DW_OP_regx 0x46]
DW$99	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$99, DW_AT_location[DW_OP_regx 0x47]
DW$100	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$100, DW_AT_location[DW_OP_regx 0x48]
DW$101	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$101, DW_AT_location[DW_OP_regx 0x49]
DW$102	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$102, DW_AT_location[DW_OP_regx 0x4a]
DW$103	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$103, DW_AT_location[DW_OP_regx 0x4b]
DW$104	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$104, DW_AT_location[DW_OP_regx 0x4c]
DW$105	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$105, DW_AT_location[DW_OP_regx 0x4d]
DW$106	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$106, DW_AT_location[DW_OP_regx 0x4e]
DW$107	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$107, DW_AT_location[DW_OP_regx 0x4f]
DW$108	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$108, DW_AT_location[DW_OP_regx 0x50]
DW$109	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$109, DW_AT_location[DW_OP_regx 0x51]
DW$110	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$110, DW_AT_location[DW_OP_regx 0x52]
DW$111	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$111, DW_AT_location[DW_OP_regx 0x53]
DW$112	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$112, DW_AT_location[DW_OP_regx 0x54]
DW$113	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$113, DW_AT_location[DW_OP_regx 0x55]
DW$114	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$114, DW_AT_location[DW_OP_regx 0x56]
DW$115	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$115, DW_AT_location[DW_OP_regx 0x57]
DW$116	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$116, DW_AT_location[DW_OP_regx 0x58]
DW$117	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$117, DW_AT_location[DW_OP_regx 0x59]
DW$118	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$118, DW_AT_location[DW_OP_regx 0x5a]
DW$119	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$119, DW_AT_location[DW_OP_regx 0x5b]
DW$120	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$120, DW_AT_location[DW_OP_regx 0x5c]
DW$121	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$121, DW_AT_location[DW_OP_regx 0x5d]
DW$122	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$122, DW_AT_location[DW_OP_regx 0x5e]
DW$123	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$123, DW_AT_location[DW_OP_regx 0x5f]
DW$124	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$124, DW_AT_location[DW_OP_regx 0x60]
DW$125	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$125, DW_AT_location[DW_OP_regx 0x61]
DW$126	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$126, DW_AT_location[DW_OP_regx 0x62]
DW$127	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$127, DW_AT_location[DW_OP_regx 0x63]
DW$128	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$128, DW_AT_location[DW_OP_regx 0x64]
DW$129	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$129, DW_AT_location[DW_OP_regx 0x65]
DW$130	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$130, DW_AT_location[DW_OP_regx 0x66]
DW$131	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$131, DW_AT_location[DW_OP_regx 0x67]
DW$132	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$132, DW_AT_location[DW_OP_regx 0x68]
DW$133	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$133, DW_AT_location[DW_OP_regx 0x69]
DW$134	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$134, DW_AT_location[DW_OP_regx 0x6a]
DW$135	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$135, DW_AT_location[DW_OP_regx 0x6b]
DW$136	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$136, DW_AT_location[DW_OP_regx 0x6c]
DW$137	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$137, DW_AT_location[DW_OP_regx 0x6d]
DW$138	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$138, DW_AT_location[DW_OP_regx 0x6e]
DW$139	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$139, DW_AT_location[DW_OP_regx 0x6f]
DW$140	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$140, DW_AT_location[DW_OP_regx 0x70]
DW$141	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$141, DW_AT_location[DW_OP_regx 0x71]
DW$142	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$142, DW_AT_location[DW_OP_regx 0x72]
DW$143	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$143, DW_AT_location[DW_OP_regx 0x73]
DW$144	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$144, DW_AT_location[DW_OP_regx 0x74]
DW$145	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$145, DW_AT_location[DW_OP_regx 0x75]
DW$146	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$146, DW_AT_location[DW_OP_regx 0x76]
DW$147	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$147, DW_AT_location[DW_OP_regx 0x77]
DW$148	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$148, DW_AT_location[DW_OP_regx 0x78]
DW$149	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$149, DW_AT_location[DW_OP_regx 0x79]
DW$150	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$150, DW_AT_location[DW_OP_regx 0x7a]
DW$151	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$151, DW_AT_location[DW_OP_regx 0x7b]
DW$152	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$152, DW_AT_location[DW_OP_regx 0x7c]
DW$153	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$153, DW_AT_location[DW_OP_regx 0x7d]
DW$154	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$154, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

