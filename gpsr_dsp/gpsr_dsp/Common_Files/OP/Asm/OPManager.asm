;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:49:03 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("OPManager.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("fabs"), DW_AT_symbol_name("_fabs")
	.dwattr DW$1, DW_AT_type(*DW$T$17)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$1


DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("sprintf"), DW_AT_symbol_name("_sprintf")
	.dwattr DW$3, DW_AT_type(*DW$T$10)
	.dwattr DW$3, DW_AT_declaration(0x01)
	.dwattr DW$3, DW_AT_external(0x01)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$73)
DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$77)
DW$6	.dwtag  DW_TAG_unspecified_parameters
	.dwendtag DW$3


DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("memset"), DW_AT_symbol_name("_memset")
	.dwattr DW$7, DW_AT_type(*DW$T$3)
	.dwattr DW$7, DW_AT_declaration(0x01)
	.dwattr DW$7, DW_AT_external(0x01)
DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$9	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$154)
	.dwendtag DW$7


DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt"), DW_AT_symbol_name("_sqrt")
	.dwattr DW$11, DW_AT_type(*DW$T$17)
	.dwattr DW$11, DW_AT_declaration(0x01)
	.dwattr DW$11, DW_AT_external(0x01)
DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$11


DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("sin"), DW_AT_symbol_name("_sin")
	.dwattr DW$13, DW_AT_type(*DW$T$17)
	.dwattr DW$13, DW_AT_declaration(0x01)
	.dwattr DW$13, DW_AT_external(0x01)
DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$13


DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("ldexp"), DW_AT_symbol_name("_ldexp")
	.dwattr DW$15, DW_AT_type(*DW$T$17)
	.dwattr DW$15, DW_AT_declaration(0x01)
	.dwattr DW$15, DW_AT_external(0x01)
DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
	.dwendtag DW$15


DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("rk4"), DW_AT_symbol_name("_rk4")
	.dwattr DW$18, DW_AT_declaration(0x01)
	.dwattr DW$18, DW_AT_external(0x01)
DW$19	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$60)
DW$20	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
	.dwendtag DW$18


DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("assumedDynamics_Init"), DW_AT_symbol_name("_assumedDynamics_Init")
	.dwattr DW$25, DW_AT_declaration(0x01)
	.dwattr DW$25, DW_AT_external(0x01)
DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$165)
	.dwendtag DW$25


DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("assumedDynamics"), DW_AT_symbol_name("_assumedDynamics")
	.dwattr DW$27, DW_AT_declaration(0x01)
	.dwattr DW$27, DW_AT_external(0x01)
DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$27


DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("MJD_From_UTCEx"), DW_AT_symbol_name("_MJD_From_UTCEx")
	.dwattr DW$31, DW_AT_declaration(0x01)
	.dwattr DW$31, DW_AT_external(0x01)
DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$168)
DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
	.dwendtag DW$31


DW$34	.dwtag  DW_TAG_subprogram, DW_AT_name("CreateMatrixEx"), DW_AT_symbol_name("_CreateMatrixEx")
	.dwattr DW$34, DW_AT_declaration(0x01)
	.dwattr DW$34, DW_AT_external(0x01)
DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$34


DW$38	.dwtag  DW_TAG_subprogram, DW_AT_name("DestroyMatrixEx"), DW_AT_symbol_name("_DestroyMatrixEx")
	.dwattr DW$38, DW_AT_declaration(0x01)
	.dwattr DW$38, DW_AT_external(0x01)
DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$38


DW$40	.dwtag  DW_TAG_subprogram, DW_AT_name("CopyMatrixEx"), DW_AT_symbol_name("_CopyMatrixEx")
	.dwattr DW$40, DW_AT_declaration(0x01)
	.dwattr DW$40, DW_AT_external(0x01)
DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$40


DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("MultMatrixEx"), DW_AT_symbol_name("_MultMatrixEx")
	.dwattr DW$43, DW_AT_declaration(0x01)
	.dwattr DW$43, DW_AT_external(0x01)
DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$43


DW$47	.dwtag  DW_TAG_subprogram, DW_AT_name("MultMatrix_B_TC_Ex"), DW_AT_symbol_name("_MultMatrix_B_TC_Ex")
	.dwattr DW$47, DW_AT_declaration(0x01)
	.dwattr DW$47, DW_AT_external(0x01)
DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$49	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$47


DW$51	.dwtag  DW_TAG_subprogram, DW_AT_name("MultMatrix_SimResEx"), DW_AT_symbol_name("_MultMatrix_SimResEx")
	.dwattr DW$51, DW_AT_declaration(0x01)
	.dwattr DW$51, DW_AT_external(0x01)
DW$52	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$53	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$51


DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("InvSymmetricalMatrixEx"), DW_AT_symbol_name("_InvSymmetricalMatrixEx")
	.dwattr DW$55, DW_AT_declaration(0x01)
	.dwattr DW$55, DW_AT_external(0x01)
DW$56	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$57	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$55


DW$58	.dwtag  DW_TAG_subprogram, DW_AT_name("Compensate_Rotation"), DW_AT_symbol_name("_Compensate_Rotation")
	.dwattr DW$58, DW_AT_declaration(0x01)
	.dwattr DW$58, DW_AT_external(0x01)
DW$59	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$180)
DW$60	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$180)
DW$61	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
	.dwendtag DW$58


DW$62	.dwtag  DW_TAG_subprogram, DW_AT_name("ECEF_to_Geoid"), DW_AT_symbol_name("_ECEF_to_Geoid")
	.dwattr DW$62, DW_AT_declaration(0x01)
	.dwattr DW$62, DW_AT_external(0x01)
DW$63	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$183)
DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$184)
DW$65	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$185)
	.dwendtag DW$62

_H$1:	.usect	".far",4616,8
_Pp$2:	.usect	".far",4616,8
_K$3:	.usect	".far",4616,8
_X0$4:	.usect	".far",64,8
_Xp$5:	.usect	".far",64,8
_DX$6:	.usect	".far",64,8
_dX$7:	.usect	".far",64,8
_dYp$8:	.usect	".far",96,8
_dYv$9:	.usect	".far",96,8
;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI0242 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI0244 
	.sect	".text"
	.global	_FirstSolutionValidity

DW$66	.dwtag  DW_TAG_subprogram, DW_AT_name("FirstSolutionValidity"), DW_AT_symbol_name("_FirstSolutionValidity")
	.dwattr DW$66, DW_AT_low_pc(_FirstSolutionValidity)
	.dwattr DW$66, DW_AT_high_pc(0x00)
	.dwattr DW$66, DW_AT_begin_file("OPManager.c")
	.dwattr DW$66, DW_AT_begin_line(0xc1)
	.dwattr DW$66, DW_AT_begin_column(0x06)
	.dwattr DW$66, DW_AT_frame_base[DW_OP_breg31 120]
	.dwattr DW$66, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",200,1

;******************************************************************************
;* FUNCTION NAME: _FirstSolutionValidity                                      *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 24 Args + 52 Auto + 44 Save = 120 byte               *
;******************************************************************************
_FirstSolutionValidity:
;** --------------------------------------------------------------------------*
DW$67	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pUser"), DW_AT_symbol_name("_pUser")
	.dwattr DW$67, DW_AT_type(*DW$T$219)
	.dwattr DW$67, DW_AT_location[DW_OP_reg4]
DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$68, DW_AT_type(*DW$T$221)
	.dwattr DW$68, DW_AT_location[DW_OP_reg20]
DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pSolVal"), DW_AT_symbol_name("_pSolVal")
	.dwattr DW$69, DW_AT_type(*DW$T$222)
	.dwattr DW$69, DW_AT_location[DW_OP_reg6]
DW$70	.dwtag  DW_TAG_formal_parameter, DW_AT_name("fProcessing"), DW_AT_symbol_name("_fProcessing")
	.dwattr DW$70, DW_AT_type(*DW$T$52)
	.dwattr DW$70, DW_AT_location[DW_OP_reg22]
DW$71	.dwtag  DW_TAG_formal_parameter, DW_AT_name("BinStrFlag"), DW_AT_symbol_name("_BinStrFlag")
	.dwattr DW$71, DW_AT_type(*DW$T$52)
	.dwattr DW$71, DW_AT_location[DW_OP_reg8]
DW$72	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataStr"), DW_AT_symbol_name("_dataStr")
	.dwattr DW$72, DW_AT_type(*DW$T$200)
	.dwattr DW$72, DW_AT_location[DW_OP_reg24]
DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DebugTerminal"), DW_AT_symbol_name("_DebugTerminal")
	.dwattr DW$73, DW_AT_type(*DW$T$202)
	.dwattr DW$73, DW_AT_location[DW_OP_reg10]

           MV      .L1X    SP,A31            ; |200| 
||         STW     .D2T1   A15,*SP--(120)    ; |200| 
||         MVKL    .S1     __mpyd,A3         ; |212| 
||         MVK     .S2     14640,B5          ; |212| 
||         CMPEQ   .L2     B6,1,B0           ; |203| 

           STW     .D2T1   A3,*+SP(28)       ; |212| 
||         MVKL    .S1     __subd,A3         ; |212| 
||         MVK     .S2     14632,B9          ; |212| 
||         ADD     .L2     B5,B4,B5          ; |212| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         MV      .L1     A4,A11            ; |200| 
||         STW     .D2T2   B8,*+SP(48)       ; |200| 
||         MVK     .S2     14648,B8          ; |214| 
||         MVKL    .S1     __addd,A15        ; |212| 

           LDW     .D2T1   *+SP(28),A4       ; |200| 
||         MVK     .S2     14624,B7          ; |212| 
||         MVKH    .S1     __addd,A15        ; |212| 

           STW     .D2T1   A3,*+SP(32)       ; |212| 
||         MVKL    .S1     __subd,A3         ; |212| 
||         MVKL    .S2     __subd,B16        ; |212| 

           STW     .D2T1   A3,*+SP(36)       ; |212| 
||         MVK     .S1     14656,A3          ; |214| 
||         MVKH    .S2     __subd,B16        ; |212| 

           STW     .D1T1   A14,*-A31(24)
||         MV      .L1     A6,A14            ; |200| 
||         STW     .D2T2   B6,*+SP(40)       ; |200| 
||         ADD     .L2     B9,B4,B6          ; |212| 

   [!B0]   LDH     .D1T1   *A14,A0
||         STW     .D2T2   B5,*+SP(60)       ; |212| 
||         ADD     .L2     B8,B4,B5          ; |214| 

           MVKH    .S1     __mpyd,A4         ; |212| 
||         STW     .D2T2   B6,*+SP(56)       ; |212| 
||         ADD     .L2     B7,B4,B6          ; |212| 
||         ADD     .S2X    A3,B4,B4          ; |214| 
||         STDW    .D1T1   A13:A12,*-A31(32)

           STW     .D2T1   A4,*+SP(28)       ; |212| 
||         MVKL    .S1     __subd,A13        ; |214| 

           LDW     .D2T1   *+SP(32),A4       ; |212| 
||         MVKL    .S1     __subd,A12        ; |214| 

           STW     .D2T2   B4,*+SP(68)
||         MVKH    .S1     __subd,A13        ; |214| 

   [!B0]   LDW     .D2T2   *+SP(40),B4       ; |220| 
|| [!B0]   CMPGT   .L1     A0,0,A3           ; |220| 
||         MVKH    .S1     __subd,A12        ; |214| 

   [!B0]   XOR     .L1     1,A3,A3           ; |220| 
||         STW     .D2T1   A10,*+SP(52)      ; |200| 
||         MV      .S1X    B6,A10            ; |212| 

           STDW    .D2T2   B11:B10,*+SP(104)
||         MVKL    .S2     __subd,B10        ; |214| 

           MVKH    .S1     __subd,A4         ; |212| 
||         STDW    .D2T2   B13:B12,*+SP(112)
||         MVKL    .S2     __addd,B11        ; |212| 
|| [ B0]   MV      .L2X    A10,B4

           STW     .D2T1   A4,*+SP(32)       ; |212| 
||         MVKL    .S2     __mpyd,B12        ; |212| 

           LDW     .D2T1   *+SP(36),A4       ; |212| 
|| [!B0]   CMPEQ   .L2     B4,0,B4           ; |220| 
||         MVKL    .S2     __mpyd,B13        ; |212| 

   [!B0]   B       .S1     L1
||         STW     .D2T2   B5,*+SP(64)
||         MVKH    .S2     __subd,B10        ; |214| 

           MVKH    .S2     __addd,B11        ; |212| 
||         STW     .D2T1   A8,*+SP(44)       ; |200| 

           MVKH    .S2     __mpyd,B12        ; |212| 
||         STW     .D2T2   B3,*+SP(100)

           MVKH    .S2     __mpyd,B13        ; |212| 
           MVKH    .S1     __subd,A4         ; |212| 

   [ B0]   CALL    .S2     B16               ; |212| 
||         STW     .D2T1   A4,*+SP(36)       ; |212| 
|| [!B0]   MVK     .L1     0xffffffff,A4     ; |247| 
|| [!B0]   OR      .L2X    A3,B4,B0          ; |220| 

           ; BRANCHCC OCCURS {L1} 
;** --------------------------------------------------------------------------*

           LDDW    .D1T1   *+A11(8),A5:A4    ; |212| 
||         LDDW    .D2T2   *B4,B5:B4         ; |212| 

           ADDKPC  .S2     RL6,B3,3          ; |212| 
RL6:       ; CALL OCCURS {__subd}            ; |212| 
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *+SP(36),A3       ; |212| 
           MV      .L2X    A10,B4            ; |212| 
           LDDW    .D1T1   *+A11(16),A7:A6   ; |212| 
           LDDW    .D2T2   *+B4(8),B5:B4     ; |212| 
           STW     .D2T1   A4,*+SP(76)       ; |212| 
           CALL    .S2X    A3                ; |212| 
           ADDKPC  .S2     RL7,B3,0          ; |212| 
           MV      .S1     A6,A4             ; |212| 
           STW     .D2T1   A5,*+SP(72)       ; |212| 
           NOP             1
           MV      .L1     A7,A5             ; |212| 
RL7:       ; CALL OCCURS {__subd}            ; |212| 
           LDW     .D2T1   *+SP(32),A3       ; |212| 
           LDW     .D2T2   *+SP(56),B4       ; |212| 
           LDDW    .D1T1   *+A11(24),A7:A6   ; |212| 
           MV      .D1     A5,A10            ; |212| 
           STW     .D2T1   A4,*+SP(56)       ; |212| 
           CALL    .S2X    A3                ; |212| 
           LDDW    .D2T2   *+B4(8),B5:B4     ; |212| 
           MV      .L1     A6,A4             ; |212| 
           ADDKPC  .S2     RL8,B3,1          ; |212| 
           MV      .S1     A7,A5             ; |212| 
RL8:       ; CALL OCCURS {__subd}            ; |212| 
           CALL    .S2     B13               ; |212| 
           MV      .L2X    A4,B4             ; |212| 
           ADDKPC  .S2     RL15,B3,2         ; |212| 
           MV      .L2X    A5,B5             ; |212| 
RL15:      ; CALL OCCURS {__mpyd}            ; |212| 
           LDW     .D2T1   *+SP(28),A3       ; |212| 
           STW     .D2T1   A4,*+SP(36)       ; |212| 
           LDW     .D2T1   *+SP(56),A4       ; |212| 
           MV      .L2X    A10,B5            ; |212| 
           STW     .D2T1   A5,*+SP(32)       ; |212| 
           CALL    .S2X    A3                ; |212| 
           ADDKPC  .S2     RL16,B3,2         ; |212| 
           MV      .L1     A10,A5            ; |212| 
           MV      .L2X    A4,B4             ; |212| 
RL16:      ; CALL OCCURS {__mpyd}            ; |212| 

           LDW     .D2T1   *+SP(72),A5       ; |212| 
||         MV      .L2X    A5,B13            ; |212| 

           LDW     .D2T1   *+SP(76),A4       ; |212| 
||         CALL    .S2     B12               ; |212| 
||         MV      .L1     A4,A10            ; |212| 

           ADDKPC  .S2     RL17,B3,2         ; |212| 
           MV      .L2X    A5,B5             ; |212| 
           MV      .L2X    A4,B4             ; |212| 
RL17:      ; CALL OCCURS {__mpyd}            ; |212| 
           CALL    .S2X    A15               ; |212| 
           MV      .L2X    A4,B4             ; |212| 
           ADDKPC  .S2     RL18,B3,0         ; |212| 
           MV      .S1     A10,A4            ; |212| 
           MV      .L2X    A5,B5             ; |212| 
           MV      .L1X    B13,A5            ; |212| 
RL18:      ; CALL OCCURS {__addd}            ; |212| 
           MV      .L2X    A4,B4             ; |212| 

           LDW     .D2T1   *+SP(32),A5       ; |212| 
||         MV      .L2X    A5,B5             ; |212| 
||         CALL    .S2     B11               ; |212| 

           LDW     .D2T1   *+SP(36),A4       ; |212| 
           ADDKPC  .S2     RL19,B3,3         ; |212| 
RL19:      ; CALL OCCURS {__addd}            ; |212| 
           LDW     .D2T2   *+SP(60),B4       ; |212| 
           LDDW    .D1T1   *+A11(32),A7:A6   ; |214| 
           MV      .L2X    A4,B11            ; |212| 
           MV      .D1     A5,A15            ; |212| 
           CALL    .S2X    A12               ; |214| 
           LDDW    .D2T2   *+B4(8),B5:B4     ; |214| 
           MV      .S1     A6,A4             ; |214| 
           ADDKPC  .S2     RL20,B3,1         ; |214| 
           MV      .L1     A7,A5             ; |214| 
RL20:      ; CALL OCCURS {__subd}            ; |214| 
           LDW     .D2T2   *+SP(64),B4       ; |214| 
           LDDW    .D1T1   *+A11(40),A7:A6   ; |214| 
           MV      .D1     A5,A12            ; |214| 
           MV      .L2X    A4,B12            ; |214| 
           CALL    .S2X    A13               ; |214| 
           LDDW    .D2T2   *+B4(8),B5:B4     ; |214| 
           MV      .L1     A6,A4             ; |214| 
           ADDKPC  .S2     RL21,B3,1         ; |214| 
           MV      .S1     A7,A5             ; |214| 
RL21:      ; CALL OCCURS {__subd}            ; |214| 
           LDW     .D2T2   *+SP(68),B4       ; |214| 
           LDDW    .D1T1   *+A11(48),A7:A6   ; |214| 
           MV      .L1     A4,A13            ; |214| 
           MV      .D1     A5,A11            ; |214| 
           CALL    .S2     B10               ; |214| 
           LDDW    .D2T2   *+B4(8),B5:B4     ; |214| 
           MV      .S1     A6,A4             ; |214| 
           ADDKPC  .S2     RL22,B3,1         ; |214| 
           MV      .L1     A7,A5             ; |214| 
RL22:      ; CALL OCCURS {__subd}            ; |214| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |214| 
           MVKH    .S1     __mpyd,A3         ; |214| 
           MV      .L2X    A4,B4             ; |214| 
           CALL    .S2X    A3                ; |214| 
           ADDKPC  .S2     RL34,B3,3         ; |214| 
           MV      .L2X    A5,B5             ; |214| 
RL34:      ; CALL OCCURS {__mpyd}            ; |214| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |214| 
           MVKH    .S1     __mpyd,A3         ; |214| 
           MV      .L2X    A5,B10            ; |214| 
           CALL    .S2X    A3                ; |214| 
           MV      .L2X    A4,B13            ; |214| 
           MV      .L2X    A13,B4            ; |214| 
           ADDKPC  .S2     RL35,B3,0         ; |214| 
           MV      .L1     A11,A5            ; |214| 

           MV      .S1     A13,A4            ; |214| 
||         MV      .L2X    A11,B5            ; |214| 

RL35:      ; CALL OCCURS {__mpyd}            ; |214| 
           MVKL    .S2     __mpyd,B6         ; |214| 
           MVKH    .S2     __mpyd,B6         ; |214| 
           CALL    .S2     B6                ; |214| 
           MV      .L1     A4,A13            ; |214| 
           ADDKPC  .S2     RL36,B3,0         ; |214| 
           MV      .S1X    B12,A4            ; |214| 
           MV      .D1     A5,A11            ; |214| 

           MV      .L1     A12,A5            ; |214| 
||         MV      .L2X    A12,B5            ; |214| 
||         MV      .D2     B12,B4            ; |214| 

RL36:      ; CALL OCCURS {__mpyd}            ; |214| 
           MVKL    .S2     __addd,B6         ; |214| 
           MVKH    .S2     __addd,B6         ; |214| 
           CALL    .S2     B6                ; |214| 
           MV      .L2X    A4,B4             ; |214| 
           ADDKPC  .S2     RL37,B3,0         ; |214| 
           MV      .S1     A13,A4            ; |214| 
           MV      .L2X    A5,B5             ; |214| 
           MV      .L1     A11,A5            ; |214| 
RL37:      ; CALL OCCURS {__addd}            ; |214| 
           MVKL    .S1     __addd,A3         ; |214| 
           MVKH    .S1     __addd,A3         ; |214| 
           MV      .L2X    A4,B4             ; |214| 
           CALL    .S2X    A3                ; |214| 
           MV      .L1X    B13,A4            ; |214| 
           ADDKPC  .S2     RL38,B3,1         ; |214| 
           MV      .L2X    A5,B5             ; |214| 
           MV      .L1X    B10,A5            ; |214| 
RL38:      ; CALL OCCURS {__addd}            ; |214| 
           MVKL    .S2     __addd,B6         ; |214| 
           MVKH    .S2     __addd,B6         ; |214| 

           LDDW    .D1T1   *+A14(8),A7:A6    ; |214| 
||         CALL    .S2     B6                ; |214| 

           MV      .L2X    A4,B4             ; |214| 
           MV      .L2X    A5,B5             ; |214| 
           ADDKPC  .S2     RL39,B3,1         ; |214| 

           MV      .L1     A7,A5             ; |214| 
||         MV      .S1     A6,A4             ; |214| 

RL39:      ; CALL OCCURS {__addd}            ; |214| 
           MVKL    .S2     __addd,B6         ; |215| 
           MVKH    .S2     __addd,B6         ; |215| 

           LDDW    .D1T1   *+A14(16),A7:A6   ; |215| 
||         CALL    .S2     B6                ; |215| 

           MV      .D2     B11,B4            ; |215| 
           MV      .L2X    A15,B5            ; |215| 
           STDW    .D1T1   A5:A4,*+A14(8)    ; |214| 
           ADDKPC  .S2     RL40,B3,0         ; |215| 

           MV      .L1     A7,A5             ; |215| 
||         MV      .S1     A6,A4             ; |215| 

RL40:      ; CALL OCCURS {__addd}            ; |215| 
;** --------------------------------------------------------------------------*
           LDH     .D1T2   *A14,B4           ; |216| 
           NOP             2
           STDW    .D1T1   A5:A4,*+A14(16)   ; |215| 
           MVK     .L1     0xffffffff,A4     ; |247| 
           ADD     .L2     1,B4,B4           ; |216| 
           STH     .D1T2   B4,*A14           ; |216| 

           LDW     .D2T2   *+SP(40),B4       ; |220| 
||         EXT     .S2     B4,16,16,B5       ; |216| 

           NOP             1
           MV      .L1X    B5,A0             ; |216| Define a twin register
           CMPGT   .L1     A0,0,A3           ; |220| 
           XOR     .L1     1,A3,A3           ; |220| 
           CMPEQ   .L2     B4,0,B4           ; |220| 
           OR      .L2X    A3,B4,B0          ; |220| 
;** --------------------------------------------------------------------------*
L1:    

   [!B0]   B       .S2     L7                ; |247| 
|| [ B0]   MVKL    .S1     __fltid,A5        ; |226| 
|| [ B0]   ZERO    .L1     A11               ; |230| 
|| [ B0]   ZERO    .D1     A13               ; |231| 

   [ B0]   MVKL    .S1     __divd,A3         ; |225| 
|| [ B0]   MVKL    .S2     __divd,B13        ; |226| 

   [ B0]   MVKH    .S1     __fltid,A5        ; |226| 
|| [ B0]   MVKH    .S2     __divd,B13        ; |226| 

   [ B0]   MVKH    .S1     __divd,A3         ; |225| 
|| [ B0]   MVKL    .S2     __fltid,B4        ; |225| 

   [ B0]   MV      .L2X    A5,B11            ; |226| 
|| [ B0]   MVKH    .S1     0x407e0000,A11    ; |230| 
|| [ B0]   MVKL    .S2     _sqrt,B10         ; |225| 

   [ B0]   MV      .L2X    A3,B12            ; |225| 
|| [ B0]   MVKL    .S1     _sqrt,A15         ; |226| 

           ; BRANCHCC OCCURS {L7}            ; |247| 
;** --------------------------------------------------------------------------*

   [!A0]   LDW     .D2T1   *+SP(44),A1       ; |236| 
|| [ A0]   ZERO    .L1     A1                ; |236| nullify predicate
|| [!A0]   B       .S2     L2                ; |231| 
|| [!A0]   MVKL    .S1     _sprintf,A3       ; |236| 
||         ZERO    .D1     A12               ; |231| 

           MVKH    .S2     __fltid,B4        ; |225| 
||         MV      .L1     A0,A4             ; |225| 
||         MVKH    .S1     _sqrt,A15         ; |226| 
||         ZERO    .D1     A10               ; |230| 
|| [!A0]   LDW     .D2T1   *+SP(48),A4       ; |236| 

   [ A0]   CALL    .S2     B4                ; |225| 
||         MVKH    .S1     0x40180000,A13    ; |231| 

   [!A0]   MVKL    .S2     SL1+0,B5          ; |236| 
           MVKH    .S2     _sqrt,B10         ; |225| 
   [ A1]   B       .S1     L6                ; |234| 
           ; BRANCHCC OCCURS {L2}            ; |231| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL45,B3,1         ; |225| 
RL45:      ; CALL OCCURS {__fltid}           ; |225| 
;** --------------------------------------------------------------------------*

           CALL    .S2     B12               ; |225| 
||         LDDW    .D1T1   *+A14(8),A7:A6    ; |225| 

           MV      .L2X    A4,B4             ; |225| 
           ADDKPC  .S2     RL46,B3,1         ; |225| 
           MV      .L2X    A5,B5             ; |225| 

           MV      .L1     A7,A5             ; |225| 
||         MV      .S1     A6,A4             ; |225| 

RL46:      ; CALL OCCURS {__divd}            ; |225| 
           CALL    .S2     B10               ; |225| 
           ADDKPC  .S2     RL47,B3,4         ; |225| 
RL47:      ; CALL OCCURS {_sqrt}             ; |225| 
           CALL    .S2     B11               ; |226| 

           LDH     .D1T1   *A14,A4           ; |226| 
||         MV      .L1     A4,A10            ; |225| 

           MV      .L1     A5,A11            ; |225| 
           ADDKPC  .S2     RL52,B3,2         ; |226| 
RL52:      ; CALL OCCURS {__fltid}           ; |226| 

           LDDW    .D1T1   *+A14(16),A7:A6   ; |226| 
||         CALL    .S2     B13               ; |226| 

           MV      .L2X    A5,B5             ; |226| 
           MV      .L2X    A4,B4             ; |226| 
           ADDKPC  .S2     RL53,B3,1         ; |226| 

           MV      .L1     A6,A4             ; |226| 
||         MV      .S1     A7,A5             ; |226| 

RL53:      ; CALL OCCURS {__divd}            ; |226| 
           CALL    .S2X    A15               ; |226| 
           ADDKPC  .S2     RL54,B3,4         ; |226| 
RL54:      ; CALL OCCURS {_sqrt}             ; |226| 
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *+SP(44),A1       ; |236| 

           LDW     .D2T1   *+SP(48),A4       ; |236| 
||         MV      .L1     A4,A12            ; |226| 

           MVKL    .S1     _sprintf,A3       ; |236| 
           MV      .L1     A5,A13            ; |226| 
           LDH     .D1T1   *A14,A0           ; |227| 

   [ A1]   B       .S1     L6                ; |234| 
||         MVKL    .S2     SL1+0,B5          ; |236| 

;** --------------------------------------------------------------------------*
L2:    
           NOP             1
           MVKH    .S2     SL1+0,B5          ; |236| 
           MVK     .L2     0xffffffff,B10    ; |106| 
           MVKH    .S1     _sprintf,A3       ; |236| 
           SUB     .D2X    A4,1,B11          ; |107| 
           ; BRANCHCC OCCURS {L6}            ; |234| 
;** --------------------------------------------------------------------------*
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 109
;*      Loop opening brace source line   : 109
;*      Loop closing brace source line   : 109
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 6
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1*       0     
;*      .D units                     1*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             1*       0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        2     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1*       0     
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop is interruptible
;*      Collapsed epilog stages     : 1
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      For further improvement on this loop, try option -mh14
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L3:    ; PIPED LOOP PROLOG
           CALL    .S2X    A3                ; |236| 
           NOP             1
           STW     .D2T2   B5,*+SP(4)        ; |236| 
           STDW    .D2T1   A11:A10,*+SP(8)   ; |236| 
           STW     .D2T1   A0,*+SP(24)       ; |236| 

           STDW    .D2T1   A13:A12,*+SP(16)  ; |236| 
||         ADDKPC  .S2     RL55,B3,0         ; |236| 

RL55:      ; CALL OCCURS {_sprintf}          ; |236| 
           NOP             1
           MV      .L1X    B11,A3
           LDB     .D1T1   *++A3,A0          ; |109| (P) <0,0>  ^ 
           NOP             3
           MVK     .L2     0x1,B0
	.dwpsn	"C:/CCStudio_v3.1/C6000/cgtools/include/string.h",109,0

           MV      .L2     B10,B4
|| [!A0]   ZERO    .S2     B0                ; |109| (P) <0,5>  ^ 

;** --------------------------------------------------------------------------*
L4:    ; PIPED LOOP KERNEL
DW$L$_FirstSolutionValidity$15$B:

   [ B0]   BNOP    .S1     L4,4              ; |109| <0,6> 
|| [ B0]   LDB     .D1T1   *++A3,A0          ; |109| <1,0>  ^ 

           ADD     .L2     1,B4,B4           ; |109| <0,11> 
|| [!A0]   ZERO    .S2     B0                ; |109| <1,5>  ^ 

DW$L$_FirstSolutionValidity$15$E:
;** --------------------------------------------------------------------------*
L5:    ; PIPED LOOP EPILOG
           LDW     .D2T1   *+SP(52),A4

           LDW     .D2T2   *+SP(48),B4       ; |110| 
||         MV      .L1X    B4,A6

           NOP             3
           LDW     .D1T1   *+A4(4),A3        ; |110| 
           NOP             4
           CALL    .S2X    A3                ; |110| 
           ADDKPC  .S2     RL56,B3,4         ; |110| 
RL56:      ; CALL OCCURS {A3}                ; |110| 
;** --------------------------------------------------------------------------*
L6:    
           MVKL    .S2     __cmpd,B6         ; |244| 
           MVKH    .S2     __cmpd,B6         ; |244| 
           CALL    .S2     B6                ; |244| 
           ZERO    .L1     A3                ; |240| 
           STH     .D1T1   A3,*A14           ; |240| 

           ZERO    .L2     B5                ; |244| 
||         ZERO    .L1     A7:A6             ; |241| 

           STDW    .D1T1   A7:A6,*+A14(8)    ; |241| 
||         MVKH    .S2     0x406e0000,B5     ; |244| 

           ADDKPC  .S2     RL58,B3,0         ; |244| 
||         STDW    .D1T1   A7:A6,*+A14(16)   ; |242| 
||         ZERO    .L2     B4                ; |244| 
||         MV      .L1     A11,A5            ; |244| 
||         MV      .S1     A10,A4            ; |244| 

RL58:      ; CALL OCCURS {__cmpd}            ; |244| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __cmpd,A3         ; |244| 
           MVKH    .S1     __cmpd,A3         ; |244| 
           ZERO    .L2     B5                ; |244| 
           CALL    .S2X    A3                ; |244| 
           MVKH    .S2     0x40080000,B5     ; |244| 
           ADDKPC  .S2     RL57,B3,0         ; |244| 
           ZERO    .L2     B4                ; |244| 
           MV      .D1     A13,A5            ; |244| 

           CMPGT   .L1     A4,0,A10          ; |244| 
||         MV      .S1     A12,A4            ; |244| 

RL57:      ; CALL OCCURS {__cmpd}            ; |244| 
;** --------------------------------------------------------------------------*

           ZERO    .L2     B4                ; |244| 
||         CMPGT   .L1     A4,0,A0           ; |244| 

   [!A0]   MVK     .L2     0x1,B4            ; |244| 
||         ZERO    .L1     A3                ; |244| 
||         MV      .S1     A10,A2            ; |244| 

   [!A2]   MVK     .L1     0x1,A3            ; |244| 
           AND     .L1X    B4,A3,A3          ; |244| 
           EXT     .S1     A3,24,24,A4       ; |244| 
;** --------------------------------------------------------------------------*
L7:    
           LDW     .D2T2   *+SP(100),B3      ; |248| 
           MV      .L1X    SP,A31            ; |248| 
           LDDW    .D1T1   *+A31(88),A13:A12 ; |248| 
           LDW     .D1T1   *+A31(96),A14     ; |248| 
           LDDW    .D2T2   *+SP(112),B13:B12 ; |248| 

           RET     .S2     B3                ; |248| 
||         LDDW    .D1T1   *+A31(80),A11:A10 ; |248| 
||         LDDW    .D2T2   *+SP(104),B11:B10 ; |248| 

           LDW     .D2T1   *++SP(120),A15    ; |248| 
	.dwpsn	"OPManager.c",248,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |248| 

DW$74	.dwtag  DW_TAG_loop
	.dwattr DW$74, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L4:1:1472539744")
	.dwattr DW$74, DW_AT_begin_file("C:/CCStudio_v3.1/C6000/cgtools/include/string.h")
	.dwattr DW$74, DW_AT_begin_line(0x6d)
	.dwattr DW$74, DW_AT_end_line(0x6d)
DW$75	.dwtag  DW_TAG_loop_range
	.dwattr DW$75, DW_AT_low_pc(DW$L$_FirstSolutionValidity$15$B)
	.dwattr DW$75, DW_AT_high_pc(DW$L$_FirstSolutionValidity$15$E)
	.dwendtag DW$74

	.dwattr DW$66, DW_AT_end_file("OPManager.c")
	.dwattr DW$66, DW_AT_end_line(0xf8)
	.dwattr DW$66, DW_AT_end_column(0x01)
	.dwendtag DW$66

	.sect	".text"
	.global	_SolutionValidity

DW$76	.dwtag  DW_TAG_subprogram, DW_AT_name("SolutionValidity"), DW_AT_symbol_name("_SolutionValidity")
	.dwattr DW$76, DW_AT_low_pc(_SolutionValidity)
	.dwattr DW$76, DW_AT_high_pc(0x00)
	.dwattr DW$76, DW_AT_begin_file("OPManager.c")
	.dwattr DW$76, DW_AT_begin_line(0x103)
	.dwattr DW$76, DW_AT_begin_column(0x06)
	.dwattr DW$76, DW_AT_frame_base[DW_OP_breg31 40]
	.dwattr DW$76, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",264,1

;******************************************************************************
;* FUNCTION NAME: _SolutionValidity                                           *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B13,SP,*
;*                           A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27, *
;*                           A28,A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23, *
;*                           B24,B25,B26,B27,B28,B29,B30,B31                  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B13,SP,*
;*                           A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27, *
;*                           A28,A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23, *
;*                           B24,B25,B26,B27,B28,B29,B30,B31                  *
;*   Local Frame Size  : 0 Args + 0 Auto + 36 Save = 36 byte                  *
;******************************************************************************
_SolutionValidity:
;** --------------------------------------------------------------------------*
DW$77	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pUser"), DW_AT_symbol_name("_pUser")
	.dwattr DW$77, DW_AT_type(*DW$T$219)
	.dwattr DW$77, DW_AT_location[DW_OP_reg4]
DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$78, DW_AT_type(*DW$T$221)
	.dwattr DW$78, DW_AT_location[DW_OP_reg20]
DW$79	.dwtag  DW_TAG_formal_parameter, DW_AT_name("BinStrFlag"), DW_AT_symbol_name("_BinStrFlag")
	.dwattr DW$79, DW_AT_type(*DW$T$52)
	.dwattr DW$79, DW_AT_location[DW_OP_reg6]
DW$80	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataStr"), DW_AT_symbol_name("_dataStr")
	.dwattr DW$80, DW_AT_type(*DW$T$200)
	.dwattr DW$80, DW_AT_location[DW_OP_reg22]
DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DebugTerminal"), DW_AT_symbol_name("_DebugTerminal")
	.dwattr DW$81, DW_AT_type(*DW$T$202)
	.dwattr DW$81, DW_AT_location[DW_OP_reg8]

           STW     .D2T1   A15,*SP--(40)     ; |264| 
||         MVKL    .S1     __subd,A3         ; |273| 
||         MV      .L1X    SP,A31            ; |264| 

           MVKH    .S1     __subd,A3         ; |273| 
||         STDW    .D2T2   B11:B10,*+SP(32)
||         MVK     .S2     14640,B5          ; |273| 

           ADD     .L2     B5,B4,B11         ; |273| 
||         STDW    .D2T1   A11:A10,*+SP(8)

           CALL    .S2X    A3                ; |273| 
||         LDDW    .D2T2   *B11,B7:B6        ; |273| 
||         MV      .L1     A4,A10            ; |264| 

           LDDW    .D1T1   *+A10(24),A5:A4   ; |273| 
           STDW    .D1T1   A13:A12,*-A31(24)
           MV      .S2     B4,B10            ; |264| 

           STW     .D2T2   B13,*+SP(28)
||         MV      .L2     B3,B13

           ADDKPC  .S2     RL70,B3,0         ; |273| 
||         MV      .L2     B7,B5             ; |273| 
||         MV      .D2     B6,B4             ; |273| 
||         STW     .D1T1   A14,*-A31(16)

RL70:      ; CALL OCCURS {__subd}            ; |273| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |273| 
           MVKH    .S1     __subd,A3         ; |273| 
           LDDW    .D1T1   *+A10(16),A7:A6   ; |273| 
           CALL    .S2X    A3                ; |273| 
           LDDW    .D2T2   *-B11(8),B5:B4    ; |273| 
           MV      .L1     A4,A12            ; |273| 
           ADDKPC  .S2     RL71,B3,0         ; |273| 
           MV      .S1     A6,A4             ; |273| 

           MV      .D1     A5,A11            ; |273| 
||         MV      .L1     A7,A5             ; |273| 

RL71:      ; CALL OCCURS {__subd}            ; |273| 
           MVK     .S1     14632,A3          ; |273| 
           ADD     .L1X    A3,B10,A3         ; |273| 

           MVKL    .S1     __subd,A3         ; |273| 
||         LDDW    .D1T1   *-A3(8),A7:A6     ; |273| 

           MVKH    .S1     __subd,A3         ; |273| 
           LDDW    .D1T1   *+A10(8),A9:A8    ; |273| 
           CALL    .S2X    A3                ; |273| 
           MV      .L1     A4,A14            ; |273| 
           ADDKPC  .S2     RL72,B3,0         ; |273| 
           MV      .L2X    A7,B5             ; |273| 
           MV      .L2X    A6,B4             ; |273| 

           MV      .S1     A8,A4             ; |273| 
||         MV      .D1     A5,A13            ; |273| 
||         MV      .L1     A9,A5             ; |273| 

RL72:      ; CALL OCCURS {__subd}            ; |273| 
           MVKL    .S1     __mpyd,A3         ; |273| 
           MVKH    .S1     __mpyd,A3         ; |273| 
           MV      .L2X    A4,B4             ; |273| 
           CALL    .S2X    A3                ; |273| 
           ADDKPC  .S2     RL79,B3,3         ; |273| 
           MV      .L2X    A5,B5             ; |273| 
RL79:      ; CALL OCCURS {__mpyd}            ; |273| 
           MVKL    .S1     __mpyd,A3         ; |273| 
           MVKH    .S1     __mpyd,A3         ; |273| 
           MV      .L2X    A13,B5            ; |273| 
           CALL    .S2X    A3                ; |273| 
           MV      .L2X    A14,B4            ; |273| 
           MV      .D1     A5,A15            ; |273| 
           MV      .L2X    A4,B11            ; |273| 
           MV      .L1     A14,A4            ; |273| 

           ADDKPC  .S2     RL80,B3,0         ; |273| 
||         MV      .S1     A13,A5            ; |273| 

RL80:      ; CALL OCCURS {__mpyd}            ; |273| 
           MVKL    .S2     __addd,B6         ; |273| 
           MVKH    .S2     __addd,B6         ; |273| 
           CALL    .S2     B6                ; |273| 
           MV      .L2X    A5,B5             ; |273| 
           MV      .L2X    A4,B4             ; |273| 
           MV      .S1     A15,A5            ; |273| 
           ADDKPC  .S2     RL81,B3,0         ; |273| 
           MV      .L1X    B11,A4            ; |273| 
RL81:      ; CALL OCCURS {__addd}            ; |273| 
           MVKL    .S2     __mpyd,B6         ; |273| 
           MVKH    .S2     __mpyd,B6         ; |273| 
           CALL    .S2     B6                ; |273| 
           MV      .L1     A4,A14            ; |273| 
           MV      .L2X    A12,B4            ; |273| 
           MV      .L2X    A11,B5            ; |273| 
           MV      .D1     A5,A13            ; |273| 

           MV      .L1     A11,A5            ; |273| 
||         MV      .S1     A12,A4            ; |273| 
||         ADDKPC  .S2     RL82,B3,0         ; |273| 

RL82:      ; CALL OCCURS {__mpyd}            ; |273| 
           MVKL    .S2     __addd,B6         ; |273| 
           MVKH    .S2     __addd,B6         ; |273| 
           CALL    .S2     B6                ; |273| 
           MV      .L2X    A4,B4             ; |273| 
           MV      .L2X    A5,B5             ; |273| 
           MV      .S1     A14,A4            ; |273| 
           ADDKPC  .S2     RL83,B3,0         ; |273| 
           MV      .L1     A13,A5            ; |273| 
RL83:      ; CALL OCCURS {__addd}            ; |273| 

           MVKL    .S2     __subd,B6         ; |275| 
||         MVK     .S1     14624,A3          ; |273| 

           MVKH    .S2     __subd,B6         ; |275| 

           ADD     .L2X    A3,B10,B4         ; |273| 
||         LDDW    .D1T1   *+A10(48),A7:A6   ; |275| 
||         CALL    .S2     B6                ; |275| 

           LDDW    .D2T2   *+B4(40),B5:B4    ; |275| 
           MV      .L1     A4,A12            ; |273| 
           MV      .D1     A5,A11            ; |273| 
           ADDKPC  .S2     RL84,B3,0         ; |275| 

           MV      .S1     A6,A4             ; |275| 
||         MV      .L1     A7,A5             ; |275| 

RL84:      ; CALL OCCURS {__subd}            ; |275| 
           MVKL    .S1     __subd,A3         ; |275| 
           MVKH    .S1     __subd,A3         ; |275| 
           MVK     .S2     14664,B4          ; |275| 

           ADD     .L2     B4,B10,B4         ; |275| 
||         LDDW    .D1T1   *+A10(40),A7:A6   ; |275| 
||         CALL    .S2X    A3                ; |275| 

           LDDW    .D2T2   *-B4(8),B5:B4     ; |275| 
           MV      .L1     A4,A14            ; |275| 
           MV      .D1     A5,A13            ; |275| 
           ADDKPC  .S2     RL85,B3,0         ; |275| 

           MV      .S1     A6,A4             ; |275| 
||         MV      .L1     A7,A5             ; |275| 

RL85:      ; CALL OCCURS {__subd}            ; |275| 

           MVKL    .S2     __subd,B6         ; |275| 
||         MVK     .S1     14656,A3          ; |275| 

           MVKH    .S2     __subd,B6         ; |275| 

           LDDW    .D1T1   *+A10(32),A7:A6   ; |275| 
||         ADD     .L2X    A3,B10,B4         ; |275| 
||         CALL    .S2     B6                ; |275| 

           LDDW    .D2T2   *-B4(8),B5:B4     ; |275| 
           MV      .L2X    A4,B11            ; |275| 
           MV      .L2X    A5,B10            ; |275| 
           ADDKPC  .S2     RL86,B3,0         ; |275| 

           MV      .L1     A7,A5             ; |275| 
||         MV      .S1     A6,A4             ; |275| 

RL86:      ; CALL OCCURS {__subd}            ; |275| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |275| 
           MVKH    .S1     __mpyd,A3         ; |275| 
           MV      .L2X    A4,B4             ; |275| 
           CALL    .S2X    A3                ; |275| 
           ADDKPC  .S2     RL104,B3,3        ; |275| 
           MV      .L2X    A5,B5             ; |275| 
RL104:     ; CALL OCCURS {__mpyd}            ; |275| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |275| 
           MVKH    .S1     __mpyd,A3         ; |275| 
           MV      .S1     A4,A15            ; |275| 
           CALL    .S2X    A3                ; |275| 
           MV      .L1X    B11,A4            ; |275| 
           ADDKPC  .S2     RL105,B3,0        ; |275| 
           MV      .L2     B10,B5            ; |275| 
           MV      .D2     B11,B4            ; |275| 

           MV      .S1     A5,A10            ; |275| 
||         MV      .L1X    B10,A5            ; |275| 

RL105:     ; CALL OCCURS {__mpyd}            ; |275| 
           MVKL    .S2     __addd,B6         ; |275| 
           MVKH    .S2     __addd,B6         ; |275| 
           CALL    .S2     B6                ; |275| 
           MV      .L2X    A4,B4             ; |275| 
           ADDKPC  .S2     RL106,B3,0        ; |275| 
           MV      .S1     A15,A4            ; |275| 
           MV      .L2X    A5,B5             ; |275| 
           MV      .L1     A10,A5            ; |275| 
RL106:     ; CALL OCCURS {__addd}            ; |275| 
           MVKL    .S1     __mpyd,A3         ; |275| 
           MVKH    .S1     __mpyd,A3         ; |275| 
           MV      .L2X    A14,B4            ; |275| 
           CALL    .S2X    A3                ; |275| 
           MV      .L1     A4,A15            ; |275| 
           ADDKPC  .S2     RL107,B3,0        ; |275| 
           MV      .S1     A14,A4            ; |275| 
           MV      .D1     A5,A10            ; |275| 

           MV      .L1     A13,A5            ; |275| 
||         MV      .L2X    A13,B5            ; |275| 

RL107:     ; CALL OCCURS {__mpyd}            ; |275| 
           MVKL    .S1     __addd,A3         ; |275| 
           MVKH    .S1     __addd,A3         ; |275| 
           MV      .L2X    A4,B4             ; |275| 
           CALL    .S2X    A3                ; |275| 
           ADDKPC  .S2     RL108,B3,1        ; |275| 
           MV      .S1     A15,A4            ; |275| 
           MV      .L2X    A5,B5             ; |275| 
           MV      .L1     A10,A5            ; |275| 
RL108:     ; CALL OCCURS {__addd}            ; |275| 
           MVKL    .S1     _sqrt,A3          ; |275| 
           MVKH    .S1     _sqrt,A3          ; |275| 
           NOP             1
           CALL    .S2X    A3                ; |275| 
           ADDKPC  .S2     RL109,B3,4        ; |275| 
RL109:     ; CALL OCCURS {_sqrt}             ; |275| 
           MVKL    .S2     _sqrt,B4          ; |276| 
           MVKH    .S2     _sqrt,B4          ; |276| 
           CALL    .S2     B4                ; |276| 
           MV      .L1     A5,A10            ; |275| 
           ADDKPC  .S2     RL110,B3,0        ; |276| 
           MV      .S1     A11,A5            ; |276| 
           MV      .D1     A4,A13            ; |275| 
           MV      .L1     A12,A4            ; |276| 
RL110:     ; CALL OCCURS {_sqrt}             ; |276| 
           MVKL    .S1     __cmpd,A3         ; |288| 
           MVKH    .S1     __cmpd,A3         ; |288| 
           MVKL    .S2     0x40a38800,B5     ; |288| 
           CALL    .S2X    A3                ; |288| 
           MVKH    .S2     0x40a38800,B5     ; |288| 
           MV      .L1     A4,A12            ; |276| 
           ADDKPC  .S2     RL112,B3,0        ; |288| 
           MV      .S1     A13,A4            ; |288| 

           MV      .D1     A5,A11            ; |276| 
||         MV      .L1     A10,A5            ; |288| 
||         ZERO    .L2     B4                ; |288| 

RL112:     ; CALL OCCURS {__cmpd}            ; |288| 
           MVKL    .S2     __cmpd,B6         ; |288| 
           MVKH    .S2     __cmpd,B6         ; |288| 
           CALL    .S2     B6                ; |288| 
           ZERO    .L2     B5                ; |288| 
           MVKH    .S2     0x40180000,B5     ; |288| 
           ADDKPC  .S2     RL111,B3,0        ; |288| 
           CMPGT   .L1     A4,0,A10          ; |288| 

           MV      .S1     A12,A4            ; |288| 
||         MV      .D1     A11,A5            ; |288| 
||         ZERO    .L2     B4                ; |288| 

RL111:     ; CALL OCCURS {__cmpd}            ; |288| 
;** --------------------------------------------------------------------------*
           MV      .L1X    SP,A31            ; |289| 

           LDDW    .D1T1   *+A31(16),A13:A12 ; |289| 
||         LDDW    .D2T2   *+SP(32),B11:B10  ; |289| 

           LDW     .D1T1   *+A31(24),A14     ; |289| 
||         LDW     .D2T2   *+SP(28),B13      ; |289| 
||         CMPGT   .L1     A4,0,A0           ; |288| 
||         ZERO    .L2     B4                ; |288| 
||         MV      .S2     B13,B3            ; |289| 

           RET     .S2     B3                ; |289| 
||         LDDW    .D2T1   *+SP(8),A11:A10   ; |289| 
||         MV      .L1     A10,A0            ; |288| 
|| [!A0]   MVK     .L2     0x1,B4            ; |288| 

           LDW     .D2T1   *++SP(40),A15     ; |289| 
           ZERO    .L1     A3                ; |288| 
   [!A0]   MVK     .L1     0x1,A3            ; |288| 
           AND     .L1X    B4,A3,A3          ; |288| 
	.dwpsn	"OPManager.c",289,1
           EXT     .S1     A3,24,24,A4       ; |288| 
           ; BRANCH OCCURS {B3}              ; |289| 
	.dwattr DW$76, DW_AT_end_file("OPManager.c")
	.dwattr DW$76, DW_AT_end_line(0x121)
	.dwattr DW$76, DW_AT_end_column(0x01)
	.dwendtag DW$76

	.sect	".text"
	.global	_OPMgr_Init

DW$82	.dwtag  DW_TAG_subprogram, DW_AT_name("OPMgr_Init"), DW_AT_symbol_name("_OPMgr_Init")
	.dwattr DW$82, DW_AT_low_pc(_OPMgr_Init)
	.dwattr DW$82, DW_AT_high_pc(0x00)
	.dwattr DW$82, DW_AT_begin_file("OPManager.c")
	.dwattr DW$82, DW_AT_begin_line(0x12a)
	.dwattr DW$82, DW_AT_begin_column(0x06)
	.dwattr DW$82, DW_AT_frame_base[DW_OP_breg31 16]
	.dwattr DW$82, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",299,1

;******************************************************************************
;* FUNCTION NAME: _OPMgr_Init                                                 *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,A19,A20,A21,  *
;*                           A22,A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17, *
;*                           B18,B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29, *
;*                           B30,B31                                          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,A19,A20,A21,  *
;*                           A22,A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17, *
;*                           B18,B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29, *
;*                           B30,B31                                          *
;*   Local Frame Size  : 0 Args + 0 Auto + 12 Save = 12 byte                  *
;******************************************************************************
_OPMgr_Init:
;** --------------------------------------------------------------------------*
DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$83, DW_AT_type(*DW$T$189)
	.dwattr DW$83, DW_AT_location[DW_OP_reg4]
DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mDynModelType"), DW_AT_symbol_name("_mDynModelType")
	.dwattr DW$84, DW_AT_type(*DW$T$42)
	.dwattr DW$84, DW_AT_location[DW_OP_reg20]
           MVKL    .S2     _memset,B5        ; |300| 
           MVKH    .S2     _memset,B5        ; |300| 
           CALL    .S2     B5                ; |300| 
           MVK     .S1     0x4c70,A6         ; |300| 
           NOP             1
           STW     .D2T1   A10,*SP--(16)     ; |299| 

           MV      .L2     B4,B10            ; |299| 
||         STDW    .D2T2   B11:B10,*+SP(8)   ; |299| 

           MV      .L1     A4,A10            ; |299| 
||         ADDKPC  .S2     RL113,B3,0        ; |300| 
||         MV      .D2     B3,B11            ; |299| 
||         ZERO    .L2     B4                ; |300| 

RL113:     ; CALL OCCURS {_memset}           ; |300| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _assumedDynamics_Init,A3 ; |303| 
           MVKH    .S1     _assumedDynamics_Init,A3 ; |303| 
           MV      .L1     A10,A4            ; |303| 
           CALL    .S2X    A3                ; |303| 
           ADDKPC  .S2     RL114,B3,4        ; |303| 
RL114:     ; CALL OCCURS {_assumedDynamics_Init}  ; |303| 
           MVKL    .S2     _assumedDynamics,B4 ; |304| 
           MVKH    .S2     _assumedDynamics,B4 ; |304| 

           MVK     .S2     14560,B5          ; |308| 
||         MVK     .S1     3634,A3           ; |304| 

           MVKL    .S2     0xeb851eb8,B4     ; |311| 
||         STW     .D1T2   B4,*+A10[A3]      ; |304| 

           MVKL    .S2     0x3f9eb851,B5     ; |311| 
||         ADD     .L1X    B5,A10,A3         ; |308| 

           MVKH    .S2     0xeb851eb8,B4     ; |311| 

           MVKL    .S1     0x3e7ad7f2,A7     ; |308| 
||         MVKH    .S2     0x3f9eb851,B5     ; |311| 

           STDW    .D1T2   B5:B4,*+A3(40)    ; |313| 
||         MVKL    .S1     0x9abcaf48,A6     ; |308| 

           MVKH    .S1     0x3e7ad7f2,A7     ; |308| 
||         STDW    .D1T2   B5:B4,*+A3(32)    ; |312| 

           MVK     .S2     14544,B4          ; |318| 
||         ZERO    .L2     B5                ; |318| 
||         STDW    .D1T2   B5:B4,*+A3(24)    ; |311| 
||         MVKH    .S1     0x9abcaf48,A6     ; |308| 
||         ZERO    .L1     A9                ; |314| 

           MVKH    .S1     0x3fd00000,A9     ; |314| 
||         ZERO    .L2     B4                ; |318| 
||         ADD     .D2X    B4,A10,B6         ; |318| 
||         MVKH    .S2     0x40420000,B5     ; |318| 
||         STDW    .D1T1   A7:A6,*+A3(16)    ; |310| 

           MVK     .S1     14552,A5          ; |319| 
||         MVKL    .S2     _OP_ResetEKF,B4   ; |328| 
||         STDW    .D2T2   B5:B4,*B6         ; |318| 
||         STDW    .D1T1   A7:A6,*+A3(8)     ; |309| 

           MVKL    .S1     0x3f8d7dbf,A7     ; |319| 
||         MVKH    .S2     _OP_ResetEKF,B4   ; |328| 
||         STDW    .D1T1   A7:A6,*A3         ; |308| 

           CALL    .S2     B4                ; |328| 
||         MVKL    .S1     0x487fcb93,A6     ; |319| 
||         ZERO    .L1     A8                ; |314| 

           MVKH    .S1     0x3f8d7dbf,A7     ; |319| 
||         STDW    .D1T1   A9:A8,*+A3(56)    ; |315| 

           ADD     .L1     A5,A10,A3         ; |319| 
||         MVKH    .S1     0x487fcb93,A6     ; |319| 
||         STDW    .D1T1   A9:A8,*+A3(48)    ; |314| 

           MVK     .S1     19537,A3          ; |322| 
||         ZERO    .L2     B5                ; |322| 
||         STDW    .D1T1   A7:A6,*A3         ; |319| 

           MVK     .S1     19538,A3          ; |325| 
||         STB     .D1T2   B5,*+A10[A3]      ; |322| 

           ADDKPC  .S2     RL115,B3,0        ; |328| 
||         STB     .D1T2   B5,*+A3[A10]      ; |325| 
||         MV      .L1     A10,A4            ; |328| 

RL115:     ; CALL OCCURS {_OP_ResetEKF}      ; |328| 
;** --------------------------------------------------------------------------*
           MVK     .S1     19536,A3          ; |331| 

           STB     .D1T2   B10,*+A3[A10]     ; |331| 
||         MV      .L2     B11,B3            ; |334| 

           RET     .S2     B3                ; |334| 
||         LDDW    .D2T2   *+SP(8),B11:B10   ; |334| 

           LDW     .D2T1   *++SP(16),A10     ; |334| 
	.dwpsn	"OPManager.c",334,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |334| 
	.dwattr DW$82, DW_AT_end_file("OPManager.c")
	.dwattr DW$82, DW_AT_end_line(0x14e)
	.dwattr DW$82, DW_AT_end_column(0x01)
	.dwendtag DW$82

	.sect	".text"

DW$85	.dwtag  DW_TAG_subprogram, DW_AT_name("OPMgr_Message"), DW_AT_symbol_name("_OPMgr_Message")
	.dwattr DW$85, DW_AT_low_pc(_OPMgr_Message)
	.dwattr DW$85, DW_AT_high_pc(0x00)
	.dwattr DW$85, DW_AT_begin_file("OPManager.c")
	.dwattr DW$85, DW_AT_begin_line(0x157)
	.dwattr DW$85, DW_AT_begin_column(0x0d)
	.dwattr DW$85, DW_AT_frame_base[DW_OP_breg31 248]
	.dwattr DW$85, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",344,1

;******************************************************************************
;* FUNCTION NAME: _OPMgr_Message                                              *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B13,SP,A16,A17,A18,  *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B13,SP,A16,A17,A18,  *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Local Frame Size  : 0 Args + 220 Auto + 24 Save = 244 byte               *
;******************************************************************************
_OPMgr_Message:
;** --------------------------------------------------------------------------*
DW$86	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$86, DW_AT_type(*DW$T$189)
	.dwattr DW$86, DW_AT_location[DW_OP_reg4]
DW$87	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$87, DW_AT_type(*DW$T$31)
	.dwattr DW$87, DW_AT_location[DW_OP_reg20]

           ADDK    .S2     -248,SP           ; |344| 
||         MVK     .S1     14648,A3          ; |351| 
||         MV      .L1X    SP,A31            ; |344| 

           ADD     .L1     A3,A4,A10         ; |351| 
||         STDW    .D1T1   A11:A10,*-A31(16)

           LDDW    .D1T1   *-A10(16),A7:A6   ; |355| 
           LDDW    .D1T1   *-A10(8),A19:A18  ; |356| 

           LDDW    .D1T1   *+A10(16),A17:A16 ; |353| 
||         STW     .D2T2   B13,*+SP(228)

           LDDW    .D1T2   *A10,B7:B6        ; |351| 
||         MVKL    .S1     _memset,A3        ; |358| 

           LDDW    .D1T1   *-A10(24),A9:A8   ; |354| 
||         MVKH    .S1     _memset,A3        ; |358| 
||         ADDAD   .D2     SP,16,B5          ; |358| 

           LDDW    .D1T1   *+A10(8),A21:A20  ; |352| 
||         STW     .D2T2   B10,*+SP(248)

           CALL    .S2X    A3                ; |358| 
||         STDW    .D2T1   A7:A6,*+SP(88)    ; |355| 

           STDW    .D2T1   A19:A18,*+SP(96)  ; |356| 
           STDW    .D2T1   A17:A16,*+SP(120) ; |353| 

           STDW    .D2T2   B7:B6,*+SP(104)   ; |351| 
||         STDW    .D1T1   A13:A12,*-A31(8)

           STDW    .D2T1   A9:A8,*+SP(80)    ; |354| 
||         MV      .L1     A4,A12            ; |344| 
||         MV      .S1X    B4,A13            ; |344| 
||         MV      .L2     B3,B13

           ADDKPC  .S2     RL116,B3,0        ; |358| 
||         MVK     .S1     0x30,A6           ; |358| 
||         STDW    .D2T1   A21:A20,*+SP(112) ; |352| 
||         MV      .L1X    B5,A4             ; |358| 
||         ZERO    .L2     B4                ; |358| 
||         MV      .D1     A4,A11            ; |344| 

RL116:     ; CALL OCCURS {_memset}           ; |358| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _memset,A3        ; |359| 
           MVKH    .S1     _memset,A3        ; |359| 
           ADDAD   .D2     SP,22,B5          ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL117,B3,1        ; |359| 
           MV      .L1X    B5,A4             ; |359| 
           MVK     .S1     0x30,A6           ; |359| 
           ZERO    .L2     B4                ; |359| 
RL117:     ; CALL OCCURS {_memset}           ; |359| 
           MVKL    .S2     _ECEF_to_Geoid,B6 ; |361| 
           MVKH    .S2     _ECEF_to_Geoid,B6 ; |361| 
           CALL    .S2     B6                ; |361| 
           ADDAD   .D2     SP,16,B5          ; |361| 
           ADD     .L1X    8,SP,A6           ; |361| 
           ADDAD   .D2     SP,10,B4          ; |361| 
           MV      .L1X    B5,A4             ; |361| 
           ADDKPC  .S2     RL118,B3,0        ; |361| 
RL118:     ; CALL OCCURS {_ECEF_to_Geoid}    ; |361| 
           MVK     .S2     19512,B4          ; |365| 

           MVKL    .S2     __mpyd,B8         ; |365| 
||         ADD     .L2X    B4,A11,B10        ; |365| 

           MVKH    .S2     __mpyd,B8         ; |365| 
||         LDDW    .D2T2   *B10,B7:B6        ; |365| 

           CALL    .S2     B8                ; |365| 
           MVKL    .S2     0x412e8480,B5     ; |365| 
           MVK     .S2     19304,B4          ; |364| 
           MVKH    .S2     0x412e8480,B5     ; |365| 

           ADD     .L2X    B4,A11,B4         ; |364| 
||         MV      .L1X    B6,A4             ; |365| 

           ZERO    .L2     B4                ; |365| 
||         STH     .D2T1   A13,*B4           ; |364| 
||         MV      .L1X    B7,A5             ; |365| 
||         ADDKPC  .S2     RL122,B3,0        ; |365| 

RL122:     ; CALL OCCURS {__mpyd}            ; |365| 
           MVKL    .S1     __addd,A3         ; |365| 
           MVKH    .S1     __addd,A3         ; |365| 
           ZERO    .L2     B5                ; |365| 
           CALL    .S2X    A3                ; |365| 
           MVKH    .S2     0x3fe00000,B5     ; |365| 
           ZERO    .L2     B4                ; |365| 
           ADDKPC  .S2     RL123,B3,2        ; |365| 
RL123:     ; CALL OCCURS {__addd}            ; |365| 
           MVKL    .S1     __fixdlli,A3      ; |365| 
           MVKH    .S1     __fixdlli,A3      ; |365| 
           NOP             1
           CALL    .S2X    A3                ; |365| 
           ADDKPC  .S2     RL124,B3,4        ; |365| 
RL124:     ; CALL OCCURS {__fixdlli}         ; |365| 
           MVKL    .S1     __mpyd,A3         ; |366| 
           MVKH    .S1     __mpyd,A3         ; |366| 
           LDDW    .D2T2   *+SP(104),B7:B6   ; |366| 
           CALL    .S2X    A3                ; |366| 
           STDW    .D2T1   A5:A4,*-B10(200)  ; |365| 
           ADDKPC  .S2     RL126,B3,0        ; |366| 
           MVKL    .S2     0x408f4000,B5     ; |366| 

           MV      .L1X    B6,A4             ; |366| 
||         MVKH    .S2     0x408f4000,B5     ; |366| 

           MV      .L1X    B7,A5             ; |366| 
||         ZERO    .L2     B4                ; |366| 

RL126:     ; CALL OCCURS {__mpyd}            ; |366| 
           MVKL    .S1     __fixdlli,A3      ; |366| 
           MVKH    .S1     __fixdlli,A3      ; |366| 
           NOP             1
           CALL    .S2X    A3                ; |366| 
           ADDKPC  .S2     RL127,B3,4        ; |366| 
RL127:     ; CALL OCCURS {__fixdlli}         ; |366| 
           MVKL    .S1     __mpyd,A3         ; |367| 
           MVKH    .S1     __mpyd,A3         ; |367| 
           LDDW    .D2T2   *+SP(112),B7:B6   ; |367| 
           CALL    .S2X    A3                ; |367| 
           STDW    .D2T1   A5:A4,*-B10(192)  ; |366| 
           ZERO    .L2     B4                ; |367| 
           MVKL    .S2     0x408f4000,B5     ; |367| 

           MV      .L1X    B6,A4             ; |367| 
||         MVKH    .S2     0x408f4000,B5     ; |367| 

           MV      .L1X    B7,A5             ; |367| 
||         ADDKPC  .S2     RL129,B3,0        ; |367| 

RL129:     ; CALL OCCURS {__mpyd}            ; |367| 
           MVKL    .S1     __fixdlli,A3      ; |367| 
           MVKH    .S1     __fixdlli,A3      ; |367| 
           NOP             1
           CALL    .S2X    A3                ; |367| 
           ADDKPC  .S2     RL130,B3,4        ; |367| 
RL130:     ; CALL OCCURS {__fixdlli}         ; |367| 
           MVKL    .S1     __mpyd,A3         ; |368| 
           MVKH    .S1     __mpyd,A3         ; |368| 
           LDDW    .D2T2   *+SP(120),B7:B6   ; |368| 
           CALL    .S2X    A3                ; |368| 
           STDW    .D2T1   A5:A4,*-B10(184)  ; |367| 
           ADDKPC  .S2     RL132,B3,0        ; |368| 
           MVKL    .S2     0x408f4000,B5     ; |368| 

           MV      .L1X    B6,A4             ; |368| 
||         MVKH    .S2     0x408f4000,B5     ; |368| 

           MV      .L1X    B7,A5             ; |368| 
||         ZERO    .L2     B4                ; |368| 

RL132:     ; CALL OCCURS {__mpyd}            ; |368| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fixdlli,A3      ; |368| 
           MVKH    .S1     __fixdlli,A3      ; |368| 
           NOP             1
           CALL    .S2X    A3                ; |368| 
           ADDKPC  .S2     RL133,B3,4        ; |368| 
RL133:     ; CALL OCCURS {__fixdlli}         ; |368| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |369| 
           MVKH    .S1     __mpyd,A3         ; |369| 
           LDDW    .D2T1   *+SP(80),A7:A6    ; |369| 
           CALL    .S2X    A3                ; |369| 
           MV      .L2X    A5,B7             ; |368| 
           MVKL    .S2     0x408f4000,B5     ; |369| 
           MVKH    .S2     0x408f4000,B5     ; |369| 
           MV      .L2X    A4,B6             ; |368| 

           ADDKPC  .S2     RL135,B3,0        ; |369| 
||         MV      .L1     A7,A5             ; |369| 
||         MV      .S1     A6,A4             ; |369| 
||         STDW    .D2T2   B7:B6,*-B10(176)  ; |368| 
||         ZERO    .L2     B4                ; |369| 

RL135:     ; CALL OCCURS {__mpyd}            ; |369| 
           MVKL    .S1     __fixdi,A3        ; |369| 
           MVKH    .S1     __fixdi,A3        ; |369| 
           NOP             1
           CALL    .S2X    A3                ; |369| 
           ADDKPC  .S2     RL136,B3,4        ; |369| 
RL136:     ; CALL OCCURS {__fixdi}           ; |369| 
           MVKL    .S2     __mpyd,B6         ; |370| 
           MVKH    .S2     __mpyd,B6         ; |370| 

           LDDW    .D2T1   *+SP(88),A7:A6    ; |370| 
||         CALL    .S2     B6                ; |370| 

           MVK     .S2     19344,B4          ; |369| 
           MVKL    .S2     0x408f4000,B5     ; |370| 
           ADD     .L2X    B4,A11,B10        ; |369| 
           MVKH    .S2     0x408f4000,B5     ; |370| 

           ZERO    .L2     B4                ; |370| 
||         MV      .L1     A6,A4             ; |370| 
||         STW     .D2T1   A4,*B10           ; |369| 
||         MV      .S1     A7,A5             ; |370| 
||         ADDKPC  .S2     RL138,B3,0        ; |370| 

RL138:     ; CALL OCCURS {__mpyd}            ; |370| 
           MVKL    .S1     __fixdi,A3        ; |370| 
           MVKH    .S1     __fixdi,A3        ; |370| 
           NOP             1
           CALL    .S2X    A3                ; |370| 
           ADDKPC  .S2     RL139,B3,4        ; |370| 
RL139:     ; CALL OCCURS {__fixdi}           ; |370| 
           MVKL    .S1     __mpyd,A3         ; |371| 
           MVKH    .S1     __mpyd,A3         ; |371| 
           LDDW    .D2T2   *+SP(96),B7:B6    ; |371| 
           CALL    .S2X    A3                ; |371| 
           STW     .D2T1   A4,*+B10(4)       ; |370| 
           ADDKPC  .S2     RL141,B3,0        ; |371| 
           MVKL    .S2     0x408f4000,B5     ; |371| 

           MV      .L1X    B6,A4             ; |371| 
||         MVKH    .S2     0x408f4000,B5     ; |371| 

           MV      .L1X    B7,A5             ; |371| 
||         ZERO    .L2     B4                ; |371| 

RL141:     ; CALL OCCURS {__mpyd}            ; |371| 
           MVKL    .S1     __fixdi,A3        ; |371| 
           MVKH    .S1     __fixdi,A3        ; |371| 
           NOP             1
           CALL    .S2X    A3                ; |371| 
           ADDKPC  .S2     RL142,B3,4        ; |371| 
RL142:     ; CALL OCCURS {__fixdi}           ; |371| 
           MVKL    .S1     _ldexp,A6         ; |372| 
           MVKH    .S1     _ldexp,A6         ; |372| 
           MV      .L1     A4,A3             ; |371| 
           CALL    .S2X    A6                ; |372| 
           LDDW    .D2T1   *+SP(160),A5:A4   ; |372| 
           STW     .D2T1   A3,*+B10(8)       ; |371| 
           ADDKPC  .S2     RL143,B3,1        ; |372| 
           MVK     .S2     0x30,B4           ; |372| 
RL143:     ; CALL OCCURS {_ldexp}            ; |372| 
           MVKL    .S1     __fixdlli,A3      ; |372| 
           MVKH    .S1     __fixdlli,A3      ; |372| 
           NOP             1
           CALL    .S2X    A3                ; |372| 
           ADDKPC  .S2     RL144,B3,4        ; |372| 
RL144:     ; CALL OCCURS {__fixdlli}         ; |372| 
           MVKL    .S1     _ldexp,A3         ; |373| 
           MVKH    .S1     _ldexp,A3         ; |373| 
           MV      .L1     A5,A7             ; |372| 
           CALL    .S2X    A3                ; |373| 

           MV      .S1     A4,A6             ; |372| 
||         LDDW    .D2T1   *+SP(152),A5:A4   ; |373| 

           ADDKPC  .S2     RL145,B3,0        ; |373| 
           STDW    .D2T1   A7:A6,*+B10(24)   ; |372| 
           NOP             1
           MVK     .S2     0x30,B4           ; |373| 
RL145:     ; CALL OCCURS {_ldexp}            ; |373| 
           MVKL    .S1     __fixdlli,A3      ; |373| 
           MVKH    .S1     __fixdlli,A3      ; |373| 
           NOP             1
           CALL    .S2X    A3                ; |373| 
           ADDKPC  .S2     RL146,B3,4        ; |373| 
RL146:     ; CALL OCCURS {__fixdlli}         ; |373| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |374| 
           MVKH    .S1     __mpyd,A3         ; |374| 
           LDDW    .D2T1   *+SP(168),A7:A6   ; |374| 
           CALL    .S2X    A3                ; |374| 
           ADDKPC  .S2     RL148,B3,0        ; |374| 
           ZERO    .L2     B4                ; |374| 
           MVKL    .S2     0x408f4000,B5     ; |374| 
           MVKH    .S2     0x408f4000,B5     ; |374| 

           STDW    .D2T1   A5:A4,*+B10(16)   ; |373| 
||         MV      .L1     A7,A5             ; |374| 
||         MV      .S1     A6,A4             ; |374| 

RL148:     ; CALL OCCURS {__mpyd}            ; |374| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fixdlli,A3      ; |374| 
           MVKH    .S1     __fixdlli,A3      ; |374| 
           NOP             1
           CALL    .S2X    A3                ; |374| 
           ADDKPC  .S2     RL149,B3,4        ; |374| 
RL149:     ; CALL OCCURS {__fixdlli}         ; |374| 
           MVKL    .S2     __mpyd,B6         ; |375| 
           MVKH    .S2     __mpyd,B6         ; |375| 

           LDDW    .D2T1   *+SP(128),A7:A6   ; |375| 
||         CALL    .S2     B6                ; |375| 

           ZERO    .L2     B4                ; |375| 
           STDW    .D2T1   A5:A4,*+B10(32)   ; |374| 
           MVKL    .S2     0x408f4000,B5     ; |375| 
           MVKH    .S2     0x408f4000,B5     ; |375| 

           MV      .S1     A6,A4             ; |375| 
||         MV      .L1     A7,A5             ; |375| 
||         ADDKPC  .S2     RL151,B3,0        ; |375| 

RL151:     ; CALL OCCURS {__mpyd}            ; |375| 
           MVKL    .S1     __fixdi,A3        ; |375| 
           MVKH    .S1     __fixdi,A3        ; |375| 
           NOP             1
           CALL    .S2X    A3                ; |375| 
           ADDKPC  .S2     RL152,B3,4        ; |375| 
RL152:     ; CALL OCCURS {__fixdi}           ; |375| 
           MVKL    .S1     __mpyd,A3         ; |376| 
           MVKH    .S1     __mpyd,A3         ; |376| 
           LDDW    .D2T1   *+SP(136),A7:A6   ; |376| 
           CALL    .S2X    A3                ; |376| 
           ADDKPC  .S2     RL154,B3,0        ; |376| 
           ZERO    .L2     B4                ; |376| 
           MVKL    .S2     0x408f4000,B5     ; |376| 
           MVKH    .S2     0x408f4000,B5     ; |376| 

           MV      .L1     A7,A5             ; |376| 
||         STW     .D2T1   A4,*+B10(40)      ; |375| 
||         MV      .S1     A6,A4             ; |376| 

RL154:     ; CALL OCCURS {__mpyd}            ; |376| 
           MVKL    .S1     __fixdi,A3        ; |376| 
           MVKH    .S1     __fixdi,A3        ; |376| 
           NOP             1
           CALL    .S2X    A3                ; |376| 
           ADDKPC  .S2     RL155,B3,4        ; |376| 
RL155:     ; CALL OCCURS {__fixdi}           ; |376| 
           MVKL    .S1     __mpyd,A3         ; |377| 
           MVKH    .S1     __mpyd,A3         ; |377| 
           LDDW    .D2T1   *+SP(144),A7:A6   ; |377| 
           CALL    .S2X    A3                ; |377| 
           STW     .D2T1   A4,*+B10(44)      ; |376| 
           ZERO    .L2     B4                ; |377| 
           MVKL    .S2     0x408f4000,B5     ; |377| 
           MVKH    .S2     0x408f4000,B5     ; |377| 

           MV      .L1     A7,A5             ; |377| 
||         ADDKPC  .S2     RL157,B3,0        ; |377| 
||         MV      .S1     A6,A4             ; |377| 

RL157:     ; CALL OCCURS {__mpyd}            ; |377| 
           MVKL    .S1     __fixdi,A3        ; |377| 
           MVKH    .S1     __fixdi,A3        ; |377| 
           NOP             1
           CALL    .S2X    A3                ; |377| 
           ADDKPC  .S2     RL158,B3,4        ; |377| 
RL158:     ; CALL OCCURS {__fixdi}           ; |377| 
           MVKL    .S1     __mpyd,A3         ; |378| 
           MVKH    .S1     __mpyd,A3         ; |378| 
           LDDW    .D2T2   *+SP(176),B7:B6   ; |378| 
           CALL    .S2X    A3                ; |378| 
           STW     .D2T1   A4,*+B10(48)      ; |377| 
           ADDKPC  .S2     RL160,B3,0        ; |378| 
           MVKL    .S2     0x408f4000,B5     ; |378| 

           MV      .L1X    B6,A4             ; |378| 
||         MVKH    .S2     0x408f4000,B5     ; |378| 

           MV      .L1X    B7,A5             ; |378| 
||         ZERO    .L2     B4                ; |378| 

RL160:     ; CALL OCCURS {__mpyd}            ; |378| 
           MVKL    .S1     __fixdlli,A3      ; |378| 
           MVKH    .S1     __fixdlli,A3      ; |378| 
           NOP             1
           CALL    .S2X    A3                ; |378| 
           ADDKPC  .S2     RL161,B3,4        ; |378| 
RL161:     ; CALL OCCURS {__fixdlli}         ; |378| 
           MVKL    .S1     _ldexp,A3         ; |379| 
           MVKH    .S1     _ldexp,A3         ; |379| 
           MV      .S1     A4,A6             ; |378| 
           CALL    .S2X    A3                ; |379| 

           MV      .L1     A5,A7             ; |378| 
||         LDDW    .D2T1   *+SP(200),A5:A4   ; |379| 

           STDW    .D2T1   A7:A6,*+B10(56)   ; |378| 
           ADDKPC  .S2     RL162,B3,1        ; |379| 
           MVK     .S2     0x30,B4           ; |379| 
RL162:     ; CALL OCCURS {_ldexp}            ; |379| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fixdlli,A3      ; |379| 
           MVKH    .S1     __fixdlli,A3      ; |379| 
           NOP             1
           CALL    .S2X    A3                ; |379| 
           ADDKPC  .S2     RL163,B3,4        ; |379| 
RL163:     ; CALL OCCURS {__fixdlli}         ; |379| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _ldexp,A3         ; |380| 
           MVKH    .S1     _ldexp,A3         ; |380| 
           MV      .L1     A5,A7             ; |379| 
           CALL    .S2X    A3                ; |380| 

           MV      .S1     A4,A6             ; |379| 
||         LDDW    .D2T1   *+SP(216),A5:A4   ; |380| 

           ADDKPC  .S2     RL164,B3,0        ; |380| 
           STDW    .D2T1   A7:A6,*+B10(64)   ; |379| 
           NOP             1
           MVK     .S2     0x30,B4           ; |380| 
RL164:     ; CALL OCCURS {_ldexp}            ; |380| 
           MVKL    .S1     __fixdlli,A3      ; |380| 
           MVKH    .S1     __fixdlli,A3      ; |380| 
           NOP             1
           CALL    .S2X    A3                ; |380| 
           ADDKPC  .S2     RL165,B3,4        ; |380| 
RL165:     ; CALL OCCURS {__fixdlli}         ; |380| 
           MVKL    .S1     _ldexp,A3         ; |381| 
           MVKH    .S1     _ldexp,A3         ; |381| 
           MV      .L1     A5,A7             ; |380| 
           CALL    .S2X    A3                ; |381| 

           MV      .S1     A4,A6             ; |380| 
||         LDDW    .D2T1   *+SP(192),A5:A4   ; |381| 

           ADDKPC  .S2     RL166,B3,0        ; |381| 
           STDW    .D2T1   A7:A6,*+B10(72)   ; |380| 
           NOP             1
           MVK     .S2     0x1d,B4           ; |381| 
RL166:     ; CALL OCCURS {_ldexp}            ; |381| 
           MVKL    .S1     __fixdi,A3        ; |381| 
           MVKH    .S1     __fixdi,A3        ; |381| 
           NOP             1
           CALL    .S2X    A3                ; |381| 
           ADDKPC  .S2     RL167,B3,4        ; |381| 
RL167:     ; CALL OCCURS {__fixdi}           ; |381| 
           MVKL    .S1     _ldexp,A3         ; |382| 
           MVKH    .S1     _ldexp,A3         ; |382| 
           MV      .L1     A4,A6             ; |381| 
           CALL    .S2X    A3                ; |382| 
           LDDW    .D2T1   *+SP(208),A5:A4   ; |382| 
           ADDKPC  .S2     RL168,B3,0        ; |382| 
           STW     .D2T1   A6,*+B10(80)      ; |381| 
           NOP             1
           MVK     .S2     0x1d,B4           ; |382| 
RL168:     ; CALL OCCURS {_ldexp}            ; |382| 
           MVKL    .S1     __fixdi,A3        ; |382| 
           MVKH    .S1     __fixdi,A3        ; |382| 
           NOP             1
           CALL    .S2X    A3                ; |382| 
           ADDKPC  .S2     RL169,B3,4        ; |382| 
RL169:     ; CALL OCCURS {__fixdi}           ; |382| 
           MVKL    .S1     _ldexp,A3         ; |383| 
           MVKH    .S1     _ldexp,A3         ; |383| 
           MV      .L1     A4,A6             ; |382| 
           CALL    .S2X    A3                ; |383| 
           LDDW    .D2T1   *+SP(184),A5:A4   ; |383| 
           ADDKPC  .S2     RL170,B3,0        ; |383| 
           STW     .D2T1   A6,*+B10(84)      ; |382| 
           NOP             1
           MVK     .S2     0x1f,B4           ; |383| 
RL170:     ; CALL OCCURS {_ldexp}            ; |383| 
           MVKL    .S1     __fixdi,A3        ; |383| 
           MVKH    .S1     __fixdi,A3        ; |383| 
           NOP             1
           CALL    .S2X    A3                ; |383| 
           ADDKPC  .S2     RL171,B3,4        ; |383| 
RL171:     ; CALL OCCURS {__fixdi}           ; |383| 
           MVKL    .S1     __mpyd,A3         ; |384| 
           MVKH    .S1     __mpyd,A3         ; |384| 
           LDDW    .D1T2   *+A10(24),B7:B6   ; |384| 
           CALL    .S2X    A3                ; |384| 
           MVKL    .S2     0xeb25f9db,B4     ; |384| 
           MVKL    .S2     0x3e2ca726,B5     ; |384| 
           MVKH    .S2     0xeb25f9db,B4     ; |384| 

           MV      .L1X    B6,A4             ; |384| 
||         MVKH    .S2     0x3e2ca726,B5     ; |384| 
||         MV      .S1     A4,A6             ; |383| 

           ADDKPC  .S2     RL175,B3,0        ; |384| 
||         MV      .L1X    B7,A5             ; |384| 
||         STW     .D2T1   A6,*+B10(88)      ; |383| 

RL175:     ; CALL OCCURS {__mpyd}            ; |384| 
           MVKL    .S1     __mpyd,A3         ; |384| 
           MVKH    .S1     __mpyd,A3         ; |384| 
           MVKL    .S2     0x426d1a94,B5     ; |384| 
           CALL    .S2X    A3                ; |384| 
           MVKH    .S2     0x426d1a94,B5     ; |384| 
           ZERO    .L2     B4                ; |384| 
           MVKH    .S2     0xa2000000,B4     ; |384| 
           ADDKPC  .S2     RL176,B3,1        ; |384| 
RL176:     ; CALL OCCURS {__mpyd}            ; |384| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fixdlli,A3      ; |384| 
           MVKH    .S1     __fixdlli,A3      ; |384| 
           NOP             1
           CALL    .S2X    A3                ; |384| 
           ADDKPC  .S2     RL177,B3,4        ; |384| 
RL177:     ; CALL OCCURS {__fixdlli}         ; |384| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |385| 
           MVKH    .S1     __mpyd,A3         ; |385| 
           LDDW    .D1T1   *+A10(32),A7:A6   ; |385| 
           CALL    .S2X    A3                ; |385| 
           MVKL    .S2     0x3e2ca726,B5     ; |385| 
           MVKL    .S2     0xeb25f9db,B4     ; |385| 

           MVKH    .S2     0x3e2ca726,B5     ; |385| 
||         MV      .L2X    A5,B7             ; |384| 

           MVKH    .S2     0xeb25f9db,B4     ; |385| 
||         MV      .L2X    A4,B6             ; |384| 

           ADDKPC  .S2     RL181,B3,0        ; |385| 
||         MV      .L1     A7,A5             ; |385| 
||         MV      .S1     A6,A4             ; |385| 
||         STDW    .D2T2   B7:B6,*+B10(96)   ; |384| 

RL181:     ; CALL OCCURS {__mpyd}            ; |385| 
           MVKL    .S1     __mpyd,A3         ; |385| 
           MVKH    .S1     __mpyd,A3         ; |385| 
           MVKL    .S2     0x426d1a94,B5     ; |385| 
           CALL    .S2X    A3                ; |385| 
           MVKH    .S2     0x426d1a94,B5     ; |385| 
           ZERO    .L2     B4                ; |385| 
           MVKH    .S2     0xa2000000,B4     ; |385| 
           ADDKPC  .S2     RL182,B3,1        ; |385| 
RL182:     ; CALL OCCURS {__mpyd}            ; |385| 
           MVKL    .S2     __fixdi,B4        ; |385| 
           MVKH    .S2     __fixdi,B4        ; |385| 
           CALL    .S2     B4                ; |385| 
           ADDKPC  .S2     RL183,B3,4        ; |385| 
RL183:     ; CALL OCCURS {__fixdi}           ; |385| 
;** --------------------------------------------------------------------------*
           MVK     .S1     19539,A3          ; |386| 
           LDB     .D1T1   *+A12[A3],A3      ; |386| 
           LDW     .D2T2   *+SP(248),B10     ; |397| 
           MVK     .S1     19537,A6          ; |395| 
           MV      .L2     B13,B3            ; |397| 
           LDW     .D2T2   *+SP(228),B13     ; |397| 

           MVK     .S1     19537,A3          ; |388| 
||         CMPEQ   .L1     A3,1,A0           ; |386| 

   [ A0]   LDB     .D1T1   *+A12[A3],A3      ; |388| 
           NOP             1
   [!A0]   LDB     .D1T1   *+A12[A6],A5      ; |395| 
           LDDW    .D2T1   *+SP(240),A13:A12 ; |397| 
           ZERO    .S2     B0                ; |391| 

           MVK     .S1     19448,A3          ; |385| 
|| [ A0]   CMPEQ   .L1     A3,2,A5           ; |388| 
|| [ A0]   CMPEQ   .L2X    A3,3,B4           ; |388| 
||         RET     .S2     B3                ; |397| 

           LDDW    .D2T1   *+SP(232),A11:A10 ; |397| 
||         ADD     .L1     A3,A11,A3         ; |385| 

   [ A0]   OR      .L2X    B4,A5,B4          ; |388| 

           MVK     .L1     1,A4              ; |391| 
||         STW     .D1T1   A4,*A3            ; |385| 
|| [ A0]   MV      .L2     B4,B0             ; |391| 

   [ B0]   STB     .D1T1   A4,*+A3(4)        ; |391| 
	.dwpsn	"OPManager.c",397,1

           ADDK    .S2     248,SP            ; |397| 
|| [!A0]   STB     .D1T1   A5,*+A3(4)        ; |395| 

           ; BRANCH OCCURS {B3}              ; |397| 
	.dwattr DW$85, DW_AT_end_file("OPManager.c")
	.dwattr DW$85, DW_AT_end_line(0x18d)
	.dwattr DW$85, DW_AT_end_column(0x01)
	.dwendtag DW$85

	.sect	".text"
	.global	_OPMgr_Process

DW$88	.dwtag  DW_TAG_subprogram, DW_AT_name("OPMgr_Process"), DW_AT_symbol_name("_OPMgr_Process")
	.dwattr DW$88, DW_AT_low_pc(_OPMgr_Process)
	.dwattr DW$88, DW_AT_high_pc(0x00)
	.dwattr DW$88, DW_AT_begin_file("OPManager.c")
	.dwattr DW$88, DW_AT_begin_line(0x199)
	.dwattr DW$88, DW_AT_begin_column(0x06)
	.dwattr DW$88, DW_AT_frame_base[DW_OP_breg31 552]
	.dwattr DW$88, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",418,1

;******************************************************************************
;* FUNCTION NAME: _OPMgr_Process                                              *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 504 Auto + 44 Save = 548 byte               *
;******************************************************************************
_OPMgr_Process:
;** --------------------------------------------------------------------------*
DW$89	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$89, DW_AT_type(*DW$T$189)
	.dwattr DW$89, DW_AT_location[DW_OP_reg4]
DW$90	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pTopSystem"), DW_AT_symbol_name("_pTopSystem")
	.dwattr DW$90, DW_AT_type(*DW$T$195)
	.dwattr DW$90, DW_AT_location[DW_OP_reg20]
DW$91	.dwtag  DW_TAG_formal_parameter, DW_AT_name("NavCond"), DW_AT_symbol_name("_NavCond")
	.dwattr DW$91, DW_AT_type(*DW$T$197)
	.dwattr DW$91, DW_AT_location[DW_OP_reg6]
DW$92	.dwtag  DW_TAG_formal_parameter, DW_AT_name("User"), DW_AT_symbol_name("_User")
	.dwattr DW$92, DW_AT_type(*DW$T$305)
	.dwattr DW$92, DW_AT_location[DW_OP_reg22]
DW$93	.dwtag  DW_TAG_formal_parameter, DW_AT_name("NavSolution"), DW_AT_symbol_name("_NavSolution")
	.dwattr DW$93, DW_AT_type(*DW$T$53)
	.dwattr DW$93, DW_AT_location[DW_OP_reg8]
DW$94	.dwtag  DW_TAG_formal_parameter, DW_AT_name("weekData"), DW_AT_symbol_name("_weekData")
	.dwattr DW$94, DW_AT_type(*DW$T$385)
	.dwattr DW$94, DW_AT_location[DW_OP_reg24]
DW$95	.dwtag  DW_TAG_formal_parameter, DW_AT_name("BinStrFlag"), DW_AT_symbol_name("_BinStrFlag")
	.dwattr DW$95, DW_AT_type(*DW$T$52)
	.dwattr DW$95, DW_AT_location[DW_OP_reg10]
DW$96	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataStr"), DW_AT_symbol_name("_dataStr")
	.dwattr DW$96, DW_AT_type(*DW$T$200)
	.dwattr DW$96, DW_AT_location[DW_OP_reg26]
DW$97	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DebugTerminal"), DW_AT_symbol_name("_DebugTerminal")
	.dwattr DW$97, DW_AT_type(*DW$T$202)
	.dwattr DW$97, DW_AT_location[DW_OP_reg12]
           MVKL    .S1     _memcpy,A3        ; |418| 

           MVKH    .S1     _memcpy,A3        ; |418| 
||         ADDK    .S2     -552,SP           ; |418| 
||         MV      .L1X    SP,A31            ; |418| 

           STW     .D2T2   B11,*+SP(540)

           CALL    .S2X    A3                ; |418| 
||         STW     .D2T2   B12,*+SP(544)

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B3,*+SP(532)

           STW     .D2T1   A15,*+SP(552)

           STW     .D2T2   B10,*+SP(536)
||         STW     .D1T1   A14,*-A31(24)

           MV      .L2     B8,B12            ; |418| 
||         MV      .L1X    B4,A14            ; |418| 
||         MV      .S1     A6,A13            ; |418| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         MV      .S2X    A4,B11            ; |418| 
||         STW     .D2T2   B13,*+SP(548)

           ADDKPC  .S2     RL184,B3,0        ; |418| 
||         STW     .D2T1   A14,*+SP(496)     ; |418| 
||         ADD     .L1X    8,SP,A4           ; |418| 
||         MV      .L2     B6,B4             ; |418| 
||         MVK     .S1     0x1d0,A6          ; |418| 
||         MV      .D1     A8,A11            ; |418| 

RL184:     ; CALL OCCURS {_memcpy}           ; |418| 
;** --------------------------------------------------------------------------*

           LDNDW   .D2T2   *B12,B7:B6        ; |418| 
||         MVK     .S1     472,A3            ; |418| 
||         MVK     .S2     11680,B8          ; |423| 

           ADD     .L2X    B8,A14,B4         ; |423| 
           ADD     .L2X    A3,SP,B5          ; |418| 
           MVKL    .S1     __fltid,A3        ; |425| 
           MVKH    .S1     __fltid,A3        ; |425| 
           STNDW   .D2T2   B7:B6,*B5         ; |418| 

           LDB     .D2T2   *B4,B0            ; |423| 
||         MVK     .S2     14456,B4          ; |425| 

           STW     .D2T1   A10,*+SP(500)     ; |418| 
||         ADD     .L2     B4,B11,B12        ; |425| 
||         MV      .L1     A12,A10           ; |418| 

           STW     .D2T2   B10,*+SP(504)     ; |418| 
           NOP             2

   [!B0]   B       .S2     L8                ; |423| 
|| [!B0]   MVKL    .S1     __subd,A3         ; |438| 
|| [!B0]   LDW     .D2T2   *+SP(476),B4      ; |430| 
||         SUB     .L1X    B0,13,A4          ; |425| 

   [ B0]   CALL    .S2X    A3                ; |425| 
|| [!B0]   MVKH    .S1     __subd,A3         ; |438| 
|| [!B0]   LDBU    .D2T2   *+SP(474),B6      ; |431| 

           NOP             3
   [!B0]   MV      .L1X    B4,A0
           ; BRANCHCC OCCURS {L8}            ; |423| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL185,B3,0        ; |425| 
RL185:     ; CALL OCCURS {__fltid}           ; |425| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(476),B4      ; |430| 
           LDBU    .D2T2   *+SP(474),B6      ; |431| 
           MVKL    .S1     __subd,A3         ; |438| 
           MVKH    .S1     __subd,A3         ; |438| 
           STDW    .D2T1   A5:A4,*B12        ; |425| 
           MV      .L1X    B4,A0
;** --------------------------------------------------------------------------*
L8:    

           LDDW    .D1T1   *+A13(24),A5:A4   ; |434| 
||         MVK     .S2     0x989,B5

   [!A0]   LDH     .D1T1   *+A13(16),A6      ; |431| 
   [ A0]   LDH     .D1T1   *+A13(16),A12     ; |433| 
           MV      .L1X    B4,A15            ; |430| 

           LDDW    .D2T2   *+B11[B5],B5:B4   ; |438| 
||         CALL    .S2X    A3                ; |438| 

           SHL     .S2     B6,10,B6          ; |431| 
||         STW     .D2T1   A4,*+SP(488)      ; |434| 

           STW     .D2T1   A5,*+SP(492)      ; |434| 

   [!A0]   ADD     .L1X    B6,A6,A14         ; |431| 
|| [ A0]   ADDK    .S1     1024,A12          ; |433| 

   [!A0]   STH     .D2T1   A14,*+SP(480)     ; |431| 

   [ A0]   STH     .D2T1   A12,*+SP(480)     ; |433| 
||         ADDKPC  .S2     RL190,B3,0        ; |438| 

RL190:     ; CALL OCCURS {__subd}            ; |438| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fltid,A6        ; |438| 

           MVKH    .S1     __fltid,A6        ; |438| 
||         MVK     .S2     19520,B4          ; |438| 

           ADD     .L2     B4,B11,B4         ; |438| 

           CALL    .S2X    A6                ; |438| 
||         LDH     .D2T2   *B4,B4            ; |438| 

           MV      .L1     A15,A0            ; |438| 
   [!A0]   MV      .L1     A14,A12           ; |433| 
           EXT     .S1     A12,16,16,A3      ; |431| 
           ADDKPC  .S2     RL191,B3,0        ; |438| 

           MV      .S1     A4,A12            ; |438| 
||         MV      .D1     A5,A14            ; |438| 
||         SUB     .L1X    A3,B4,A4          ; |438| 

RL191:     ; CALL OCCURS {__fltid}           ; |438| 
           MVKL    .S1     __mpyd,A3         ; |438| 
           MVKH    .S1     __mpyd,A3         ; |438| 
           MVKL    .S2     0x41227500,B5     ; |438| 
           CALL    .S2X    A3                ; |438| 
           MVKH    .S2     0x41227500,B5     ; |438| 
           ADDKPC  .S2     RL192,B3,2        ; |438| 
           ZERO    .L2     B4                ; |438| 
RL192:     ; CALL OCCURS {__mpyd}            ; |438| 
           MVKL    .S1     __addd,A3         ; |438| 
           MVKH    .S1     __addd,A3         ; |438| 
           MV      .L2X    A4,B4             ; |438| 
           CALL    .S2X    A3                ; |438| 
           MV      .L2X    A5,B5             ; |438| 
           MV      .S1     A12,A4            ; |438| 
           ADDKPC  .S2     RL193,B3,1        ; |438| 
           MV      .L1     A14,A5            ; |438| 
RL193:     ; CALL OCCURS {__addd}            ; |438| 
           MVKL    .S1     __cmpd,A3         ; |442| 
           MVKH    .S1     __cmpd,A3         ; |442| 
           MVKL    .S2     0x408c2000,B5     ; |442| 
           CALL    .S2X    A3                ; |442| 
           MVKH    .S2     0x408c2000,B5     ; |442| 
           ADDKPC  .S2     RL194,B3,0        ; |442| 
           MV      .L1     A4,A14            ; |438| 
           MV      .S1     A5,A12            ; |438| 
           ZERO    .L2     B4                ; |442| 
RL194:     ; CALL OCCURS {__cmpd}            ; |442| 
;** --------------------------------------------------------------------------*

           MVKL    .S2     _OP_ResetEKF_AfterOutage,B4 ; |443| 
||         CMPGT   .L1     A4,0,A0           ; |442| 
||         MV      .S1X    B11,A4            ; |443| 

           MVKH    .S2     _OP_ResetEKF_AfterOutage,B4 ; |443| 
|| [!A0]   MVKL    .S1     __cmpd,A3         ; |446| 

   [!A0]   B       .S2     L9                ; |442| 
|| [!A0]   MVKH    .S1     __cmpd,A3         ; |446| 

   [ A0]   CALL    .S2     B4                ; |443| 
   [!A0]   CALL    .S2X    A3                ; |446| 
   [!A0]   MVKL    .S2     0x40ac2000,B5     ; |446| 
           NOP             2
           ; BRANCHCC OCCURS {L9}            ; |442| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL195,B3,0        ; |443| 
RL195:     ; CALL OCCURS {_OP_ResetEKF_AfterOutage}  ; |443| 

           MVKL    .S1     __cmpd,A3         ; |446| 
||         MVKL    .S2     0x40ac2000,B5     ; |446| 

           MVKH    .S1     __cmpd,A3         ; |446| 
           NOP             1
           CALL    .S2X    A3                ; |446| 
           NOP             3
;** --------------------------------------------------------------------------*
L9:    
           MVKH    .S2     0x40ac2000,B5     ; |446| 

           ADDKPC  .S2     RL196,B3,0        ; |446| 
||         MV      .L1     A12,A5            ; |446| 
||         MV      .S1     A14,A4            ; |446| 
||         ZERO    .L2     B4                ; |446| 

RL196:     ; CALL OCCURS {__cmpd}            ; |446| 
;** --------------------------------------------------------------------------*

           CMPGT   .L1     A4,0,A0           ; |446| 
||         MVK     .S1     19537,A4          ; |446| 
||         MV      .D1X    B11,A3
||         LDW     .D2T2   *+SP(492),B6
||         MVK     .S2     19537,B5

           MV      .L1     A0,A1             ; guard predicate rewrite
||         MV      .S1     A0,A2             ; |446| branch predicate copy
||         LDW     .D2T2   *+SP(488),B7

   [!A0]   B       .S2     L10               ; |446| 
|| [ A0]   LDB     .D1T1   *+A3[A4],A0       ; |446| 
|| [!A1]   MVK     .L1     0x1,A0            ; nullify predicate
|| [ A1]   MVKL    .S1     _OPMgr_Message,A3 ; |451| 
|| [ A1]   LDW     .D2T2   *+SP(488),B7
|| [ A1]   ZERO    .L2     B4                ; |449| 

           LDH     .D2T1   *+SP(480),A12
|| [ A1]   MVKH    .S1     _OPMgr_Message,A3 ; |451| 
|| [ A1]   MVK     .S2     19538,B8          ; |450| 
|| [ A1]   MV      .L1X    B11,A4            ; |451| 

   [ A1]   LDH     .D2T1   *+SP(480),A12
   [ A1]   LDW     .D2T2   *+SP(492),B6
           NOP             1

   [!A2]   LDB     .D2T1   *+B11[B5],A0
|| [!A0]   B       .S1     L10

           ; BRANCHCC OCCURS {L10}           ; |446| 
;** --------------------------------------------------------------------------*
           NOP             5
           ; BRANCHCC OCCURS {L10} 
;** --------------------------------------------------------------------------*
           STB     .D2T2   B4,*+B11[B5]      ; |449| 

           CALL    .S2X    A3                ; |451| 
||         STB     .D2T2   B4,*+B8[B11]      ; |450| 

           LDH     .D1T2   *A13,B4           ; |451| 
           ADDKPC  .S2     RL197,B3,3        ; |451| 
RL197:     ; CALL OCCURS {_OPMgr_Message}    ; |451| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(532),B3      ; |523| 
           LDW     .D2T1   *+SP(516),A11     ; |523| 
           LDW     .D2T1   *+SP(512),A10     ; |523| 
           LDW     .D2T1   *+SP(524),A13     ; |523| 
           LDW     .D2T1   *+SP(520),A12     ; |523| 
           LDW     .D2T1   *+SP(528),A14     ; |523| 
           LDW     .D2T2   *+SP(540),B11     ; |523| 

           RET     .S2     B3                ; |523| 
||         LDW     .D2T1   *+SP(552),A15     ; |523| 

           LDW     .D2T2   *+SP(544),B12     ; |523| 
           NOP             3
           ADDK    .S2     552,SP            ; |523| 
           ; BRANCH OCCURS {B3}              ; |523| 
;** --------------------------------------------------------------------------*
L10:    

           MVK     .S1     472,A3            ; |459| 
||         MVKL    .S2     _OP_GetFirstSolution,B5 ; |459| 
||         MV      .L1     A11,A6            ; |459| 
||         MV      .D1X    B11,A4            ; |459| 
||         ADD     .L2     8,SP,B4           ; |459| 
||         ZERO    .D2     B8                ; |457| 

           MVKL    .S1     __cmpd,A11        ; |477| 
||         MVK     .S2     19504,B17         ; |477| 
||         MV      .L1X    B6,A5             ; |477| 

           MVKL    .S1     __subd,A7         ; |477| 
||         MVKL    .S2     __addd,B12        ; |477| 
||         ADD     .L2     B17,B11,B10       ; |477| 
||         ADD     .D2X    A3,SP,B6          ; |459| 

           MVKL    .S1     __mpyd,A14        ; |477| 
||         MVKL    .S2     __fltid,B13       ; |477| 

   [ A0]   B       .S2     L11               ; |454| 
||         MVKH    .S1     __cmpd,A11        ; |477| 

           MVKH    .S2     _OP_GetFirstSolution,B5 ; |459| 
||         MVKH    .S1     __subd,A7         ; |477| 

           MVKH    .S2     __addd,B12        ; |477| 
||         MVKH    .S1     __mpyd,A14        ; |477| 

           MVKH    .S2     __fltid,B13       ; |477| 
           MVK     .S2     19538,B9          ; |458| 
           MVK     .S2     19537,B16         ; |457| 
           ; BRANCHCC OCCURS {L11}           ; |454| 
;** --------------------------------------------------------------------------*
           CALL    .S2     B5                ; |459| 
           ADDKPC  .S2     RL198,B3,0        ; |459| 
           STB     .D2T2   B8,*+B11[B16]     ; |457| 
           STB     .D2T2   B8,*+B9[B11]      ; |458| 
           NOP             2
RL198:     ; CALL OCCURS {_OP_GetFirstSolution}  ; |459| 
;** --------------------------------------------------------------------------*

           MVK     .S2     19537,B4          ; |459| 
||         MV      .L1     A4,A0             ; |459| 

   [!A0]   LDB     .D2T2   *+B11[B4],B10     ; |459| 
||         MVK     .S2     19538,B6          ; |466| 
||         MVK     .L2     0x1,B4            ; |466| 

           NOP             2
           MVKL    .S2     _OPMgr_Message,B5 ; |468| 
           MVKH    .S2     _OPMgr_Message,B5 ; |468| 
           OR      .L1X    B10,A0,A0         ; |459| 

           MVK     .S2     19537,B6          ; |467| 
|| [ A0]   STB     .D2T2   B4,*+B6[B11]      ; |466| 
|| [!A0]   B       .S1     L19               ; |459| 

   [ A0]   STB     .D2T2   B4,*+B11[B6]      ; |467| 
|| [ A0]   CALL    .S2     B5                ; |468| 

   [ A0]   LDH     .D1T2   *A13,B4           ; |468| 
   [!A0]   LDW     .D2T2   *+SP(532),B3      ; |523| 
           NOP             2
           ; BRANCHCC OCCURS {L19}           ; |459| 
;** --------------------------------------------------------------------------*

           MV      .L1X    B11,A4            ; |468| 
||         ADDKPC  .S2     RL199,B3,0        ; |468| 

RL199:     ; CALL OCCURS {_OPMgr_Message}    ; |468| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(532),B3      ; |523| 
           LDW     .D2T1   *+SP(516),A11     ; |523| 
           LDW     .D2T1   *+SP(512),A10     ; |523| 
           LDW     .D2T1   *+SP(524),A13     ; |523| 
           LDW     .D2T1   *+SP(520),A12     ; |523| 
           LDW     .D2T1   *+SP(528),A14     ; |523| 
           LDW     .D2T2   *+SP(540),B11     ; |523| 
           LDW     .D2T2   *+SP(536),B10     ; |523| 
           LDW     .D2T2   *+SP(548),B13     ; |523| 

           RET     .S2     B3                ; |523| 
||         LDW     .D2T1   *+SP(552),A15     ; |523| 

           LDW     .D2T2   *+SP(544),B12     ; |523| 
           NOP             3
           ADDK    .S2     552,SP            ; |523| 
           ; BRANCH OCCURS {B3}              ; |523| 
;** --------------------------------------------------------------------------*
L11:    

           MVK     .S1     0x987,A4
||         MV      .L1X    B11,A3
||         CALL    .S2X    A7                ; |477| 

           LDDW    .D1T2   *+A3[A4],B5:B4    ; |477| 
||         MV      .L1X    B7,A4             ; |477| 

           ADDKPC  .S2     RL204,B3,3        ; |477| 
RL204:     ; CALL OCCURS {__subd}            ; |477| 
;** --------------------------------------------------------------------------*

           CALL    .S2     B13               ; |477| 
||         LDH     .D2T1   *B10,A3           ; |477| 

           ADDKPC  .S2     RL205,B3,2        ; |477| 
           MV      .D1     A4,A15            ; |477| 

           MV      .L1     A5,A12            ; |477| 
||         SUB     .S1     A12,A3,A4         ; |477| 

RL205:     ; CALL OCCURS {__fltid}           ; |477| 
           CALL    .S2X    A14               ; |477| 
           MVKL    .S2     0x3ee845c8,B5     ; |477| 
           MVKL    .S2     0xa0ce5129,B4     ; |477| 
           MVKH    .S2     0x3ee845c8,B5     ; |477| 
           MVKH    .S2     0xa0ce5129,B4     ; |477| 
           ADDKPC  .S2     RL206,B3,0        ; |477| 
RL206:     ; CALL OCCURS {__mpyd}            ; |477| 
           CALL    .S2     B12               ; |477| 
           MV      .L2X    A4,B4             ; |477| 
           MV      .L2X    A5,B5             ; |477| 
           MV      .S1     A15,A4            ; |477| 
           ADDKPC  .S2     RL207,B3,0        ; |477| 
           MV      .L1     A12,A5            ; |477| 
RL207:     ; CALL OCCURS {__addd}            ; |477| 
           CALL    .S2X    A11               ; |477| 
           ZERO    .L2     B5:B4             ; |477| 
           MV      .S1     A4,A14            ; |477| 
           MV      .L1     A5,A12            ; |477| 
           ADDKPC  .S2     RL208,B3,1        ; |477| 
RL208:     ; CALL OCCURS {__cmpd}            ; |477| 
;** --------------------------------------------------------------------------*

           CMPGT   .L1     A4,0,A0           ; |477| 
||         ZERO    .L2     B5                ; |481| 
||         MVKL    .S1     __cmpd,A3         ; |481| 
||         MV      .D1     A14,A4            ; |481| 
||         ZERO    .S2     B4                ; |481| 

   [!A0]   B       .S1     L18               ; |477| 
||         MVKH    .S2     0x403e0000,B5     ; |481| 
||         MV      .L1     A12,A5            ; |481| 

           MVKH    .S1     __cmpd,A3         ; |481| 
           NOP             1
   [ A0]   CALL    .S2X    A3                ; |481| 
           NOP             2
           ; BRANCHCC OCCURS {L18}           ; |477| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL209,B3,2        ; |481| 
RL209:     ; CALL OCCURS {__cmpd}            ; |481| 
;** --------------------------------------------------------------------------*

           MVK     .L2     3,B4              ; |486| 
||         MVK     .S2     19537,B5          ; |486| 
||         CMPGT   .L1     A4,0,A0           ; |481| 
||         MVK     .D2     2,B31             ; |483| 
||         MVK     .S1     480,A3            ; |493| 
||         MV      .D1X    B11,A4            ; |493| 

   [!A0]   STB     .D2T2   B4,*+B11[B5]      ; |486| 
||         MVK     .S2     19537,B30         ; |489| 
||         ADD     .L1X    A3,SP,A6          ; |493| 
||         MVKL    .S1     _OP_Propagation,A3 ; |493| 

   [ A0]   STB     .D2T2   B31,*+B11[B5]     ; |483| 
||         MVKH    .S1     _OP_Propagation,A3 ; |493| 

           LDB     .D2T2   *+B11[B30],B5     ; |489| 
           NOP             1
           LDW     .D2T2   *+SP(496),B4      ; |493| 
           NOP             2

           CMPEQ   .L2     B5,3,B0           ; |495| 
||         CMPEQ   .L1X    B5,2,A0           ; |489| 

   [!A0]   LDW     .D2T1   *+SP(500),A8      ; |498| 
|| [!A0]   MVKL    .S1     _OP_Estimation,A3 ; |498| 
|| [ A0]   MVK     .L2     0x1,B0            ; nullify predicate
|| [ A0]   B       .S2     L12               ; |489| 

   [!A0]   ZERO    .L2     B5                ; |503| 
|| [ A0]   CALL    .S2X    A3                ; |493| 
|| [!A0]   LDW     .D2T2   *+SP(504),B8      ; |498| 
|| [!A0]   MVKH    .S1     _OP_Estimation,A3 ; |498| 

   [!A0]   MVK     .S2     19537,B7          ; |503| 

   [!A0]   MVK     .S2     480,B31           ; |498| 
|| [!B0]   B       .S1     L13               ; |504| 

   [!A0]   MVK     .S2     19538,B6          ; |504| 
           NOP             1
           ; BRANCHCC OCCURS {L12}           ; |489| 
;** --------------------------------------------------------------------------*

   [ B0]   CALL    .S2X    A3                ; |498| 
|| [!B0]   STB     .D2T2   B5,*+B6[B11]      ; |504| 
||         ADD     .L2     B31,SP,B6         ; |498| 
||         MV      .L1     A13,A6            ; |498| 

   [!B0]   STB     .D2T2   B5,*+B11[B7]      ; |503| 
|| [!B0]   MVKL    .S2     _OPMgr_Message,B5 ; |507| 

   [!B0]   MVKH    .S2     _OPMgr_Message,B5 ; |507| 
           ; BRANCHCC OCCURS {L13}           ; |504| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL210,B3,2        ; |498| 
RL210:     ; CALL OCCURS {_OP_Estimation}    ; |498| 
;** --------------------------------------------------------------------------*

           B       .S1     L14               ; |499| 
||         MVKL    .S2     _OPMgr_Message,B5 ; |507| 

           MVKH    .S2     _OPMgr_Message,B5 ; |507| 
           CALL    .S2     B5                ; |507| 
           LDH     .D1T2   *A13,B4           ; |507| 
           NOP             2
           ; BRANCH OCCURS {L14}             ; |499| 
;** --------------------------------------------------------------------------*
L12:    
           ADDKPC  .S2     RL211,B3,0        ; |493| 
RL211:     ; CALL OCCURS {_OP_Propagation}   ; |493| 
           MVKL    .S2     _OPMgr_Message,B5 ; |507| 
           MVKH    .S2     _OPMgr_Message,B5 ; |507| 
;** --------------------------------------------------------------------------*
L13:    
           CALL    .S2     B5                ; |507| 
           LDH     .D1T2   *A13,B4           ; |507| 
           NOP             2
;** --------------------------------------------------------------------------*
L14:    
           ADDKPC  .S2     RL212,B3,0        ; |507| 
           MV      .L1X    B11,A4            ; |507| 
RL212:     ; CALL OCCURS {_OPMgr_Message}    ; |507| 
;** --------------------------------------------------------------------------*

           MVK     .S1     0x4bfc,A4
||         MV      .L1X    B11,A3

           LDB     .D1T1   *+A4[A3],A4       ; |509| 
||         MVK     .S1     19500,A5

           NOP             3
           ADD     .L1     A5,A3,A3
           CMPEQ   .L1     A4,3,A0           ; |509| 

   [!A0]   BNOP    .S1     L18,4             ; |509| 
|| [ A0]   LDBU    .D1T2   *A3,B4

           CMPEQ   .L2     B4,1,B0           ; |511| (P) <0,0>  ^ 
           ; BRANCHCC OCCURS {L18}           ; |509| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 511
;*      Loop closing brace source line   : 511
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 1
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        1*    
;*      .S units                     1*       0     
;*      .D units                     0        0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             0        0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1*       1*    
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop is interruptible
;*      Collapsed epilog stages     : 1
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L15:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L16:    ; PIPED LOOP KERNEL
DW$L$_OPMgr_Process$32$B:
   [ B0]   BNOP    .S1     L16,4             ; |511| <0,1> 
           CMPEQ   .L2     B4,1,B0           ; |511| <1,0>  ^ 
DW$L$_OPMgr_Process$32$E:
;** --------------------------------------------------------------------------*
L17:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*

           MVK     .L1     1,A5              ; |512| 
||         MVK     .S1     19496,A4          ; |513| 

           STB     .D1T1   A5,*A3            ; |512| 
||         ADD     .L1X    A4,B11,A4         ; |513| 

           LDW     .D1T1   *A4,A0            ; |513| 
           MVK     .S2     14672,B4          ; |515| 
           ADD     .L2     B4,B11,B7         ; |515| 
           MVK     .S1     19456,A31         ; |515| 
           MVK     .S2     0x981,B31
   [!A0]   LDDW    .D2T2   *B7,B5:B4         ; |515| 
           NOP             1
           ADD     .L2X    A31,B11,B6        ; |515| 
           MVK     .L1     1,A30             ; |517| 
           ZERO    .L1     A29               ; |519| 
   [!A0]   STDW    .D2T2   B5:B4,*B6         ; |515| 
   [!A0]   LDDW    .D2T2   *+B7(8),B7:B6     ; |516| 
           NOP             4
   [!A0]   STDW    .D2T2   B7:B6,*+B11[B31]  ; |516| 
   [!A0]   STW     .D1T1   A30,*A4           ; |517| 
           STB     .D1T1   A29,*A3           ; |519| 
;** --------------------------------------------------------------------------*
L18:    
           LDW     .D2T2   *+SP(532),B3      ; |523| 
;** --------------------------------------------------------------------------*
L19:    
           LDW     .D2T2   *+SP(544),B12     ; |523| 
           LDW     .D2T2   *+SP(548),B13     ; |523| 
           LDW     .D2T2   *+SP(536),B10     ; |523| 
           LDW     .D2T2   *+SP(540),B11     ; |523| 
           LDW     .D2T1   *+SP(528),A14     ; |523| 
           LDW     .D2T1   *+SP(520),A12     ; |523| 
           LDW     .D2T1   *+SP(524),A13     ; |523| 
           LDW     .D2T1   *+SP(512),A10     ; |523| 

           LDW     .D2T1   *+SP(516),A11     ; |523| 
||         RET     .S2     B3                ; |523| 

           LDW     .D2T1   *+SP(552),A15     ; |523| 
||         ADDK    .S2     552,SP            ; |523| 

	.dwpsn	"OPManager.c",523,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |523| 

DW$98	.dwtag  DW_TAG_loop
	.dwattr DW$98, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L16:1:1472539744")
	.dwattr DW$98, DW_AT_begin_file("OPManager.c")
	.dwattr DW$98, DW_AT_begin_line(0x1ff)
	.dwattr DW$98, DW_AT_end_line(0x1ff)
DW$99	.dwtag  DW_TAG_loop_range
	.dwattr DW$99, DW_AT_low_pc(DW$L$_OPMgr_Process$32$B)
	.dwattr DW$99, DW_AT_high_pc(DW$L$_OPMgr_Process$32$E)
	.dwendtag DW$98

	.dwattr DW$88, DW_AT_end_file("OPManager.c")
	.dwattr DW$88, DW_AT_end_line(0x20b)
	.dwattr DW$88, DW_AT_end_column(0x01)
	.dwendtag DW$88

	.sect	".text"

DW$100	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_ResetEKF"), DW_AT_symbol_name("_OP_ResetEKF")
	.dwattr DW$100, DW_AT_low_pc(_OP_ResetEKF)
	.dwattr DW$100, DW_AT_high_pc(0x00)
	.dwattr DW$100, DW_AT_begin_file("OPManager.c")
	.dwattr DW$100, DW_AT_begin_line(0x213)
	.dwattr DW$100, DW_AT_begin_column(0x0d)
	.dwattr DW$100, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr DW$100, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",532,1

;******************************************************************************
;* FUNCTION NAME: _OP_ResetEKF                                                *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 8 Save = 8 byte                    *
;******************************************************************************
_OP_ResetEKF:
;** --------------------------------------------------------------------------*
DW$101	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$101, DW_AT_type(*DW$T$189)
	.dwattr DW$101, DW_AT_location[DW_OP_reg4]
           MVKL    .S2     _CreateMatrixEx,B5 ; |534| 
           MVKH    .S2     _CreateMatrixEx,B5 ; |534| 
           CALL    .S2     B5                ; |534| 
           MVK     .S2     14688,B4          ; |534| 
           STW     .D2T1   A10,*SP--(8)      ; |532| 
           STW     .D2T2   B13,*+SP(4)       ; |532| 
           MV      .D2     B3,B13            ; |532| 

           MV      .S1     A4,A10            ; |532| 
||         MVK     .D1     0x8,A6            ; |534| 
||         ADDKPC  .S2     RL213,B3,0        ; |534| 
||         ADD     .L1X    B4,A4,A4          ; |534| 
||         MVK     .L2     0x8,B4            ; |534| 

RL213:     ; CALL OCCURS {_CreateMatrixEx}   ; |534| 
;** --------------------------------------------------------------------------*
           MVK     .S1     15888,A3          ; |541| 
           MVKL    .S1     0x412e8480,A7     ; |541| 
           MVKL    .S1     0x3fa47ae1,A5     ; |535| 
           MVKL    .S1     0x47ae147b,A4     ; |535| 

           MVK     .S1     15088,A8          ; |537| 
||         MVKL    .S2     0x408f4000,B5     ; |542| 

           MVKH    .S2     0x408f4000,B5     ; |542| 
||         MVKH    .S1     0x412e8480,A7     ; |541| 
||         ADD     .L1     A3,A10,A3         ; |541| 
||         ZERO    .D1     A6                ; |541| 
||         ZERO    .L2     B7                ; |538| 

           MVKH    .S2     0x40790000,B7     ; |538| 
||         STDW    .D1T1   A7:A6,*A3         ; |541| 
||         MVKH    .S1     0x3fa47ae1,A5     ; |535| 
||         ZERO    .L2     B4                ; |542| 

           MVK     .S2     14688,B5          ; |535| 
||         ADD     .L1     A8,A10,A3         ; |537| 
||         STDW    .D1T2   B5:B4,*+A3(200)   ; |542| 
||         MVKH    .S1     0x47ae147b,A4     ; |535| 
||         MV      .L2     B13,B3            ; |545| 

           STDW    .D1T1   A5:A4,*A3         ; |537| 
||         MVK     .S2     15488,B4          ; |539| 
||         LDW     .D2T2   *+SP(4),B13       ; |545| 
||         ZERO    .L2     B6                ; |538| 

           ADD     .L1X    B5,A10,A3         ; |535| 
||         STDW    .D1T2   B7:B6,*+A3(200)   ; |538| 
||         ADD     .L2X    B4,A10,B4         ; |539| 

           STDW    .D1T1   A5:A4,*A3         ; |535| 
||         STDW    .D2T2   B7:B6,*+B4(200)   ; |540| 

           RET     .S2     B3                ; |545| 
||         STDW    .D1T1   A5:A4,*+A3(200)   ; |536| 
||         STDW    .D2T2   B7:B6,*B4         ; |539| 

           LDW     .D2T1   *++SP(8),A10      ; |545| 
	.dwpsn	"OPManager.c",545,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |545| 
	.dwattr DW$100, DW_AT_end_file("OPManager.c")
	.dwattr DW$100, DW_AT_end_line(0x221)
	.dwattr DW$100, DW_AT_end_column(0x01)
	.dwendtag DW$100

	.sect	".text"

DW$102	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_ResetEKF_AfterOutage"), DW_AT_symbol_name("_OP_ResetEKF_AfterOutage")
	.dwattr DW$102, DW_AT_low_pc(_OP_ResetEKF_AfterOutage)
	.dwattr DW$102, DW_AT_high_pc(0x00)
	.dwattr DW$102, DW_AT_begin_file("OPManager.c")
	.dwattr DW$102, DW_AT_begin_line(0x229)
	.dwattr DW$102, DW_AT_begin_column(0x0d)
	.dwattr DW$102, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr DW$102, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",554,1

;******************************************************************************
;* FUNCTION NAME: _OP_ResetEKF_AfterOutage                                    *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 8 Save = 8 byte                    *
;******************************************************************************
_OP_ResetEKF_AfterOutage:
;** --------------------------------------------------------------------------*
DW$103	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$103, DW_AT_type(*DW$T$189)
	.dwattr DW$103, DW_AT_location[DW_OP_reg4]
           MVKL    .S2     _CreateMatrixEx,B5 ; |556| 
           MVKH    .S2     _CreateMatrixEx,B5 ; |556| 
           CALL    .S2     B5                ; |556| 
           MVK     .S2     14688,B4          ; |556| 
           STW     .D2T1   A10,*SP--(8)      ; |554| 
           STW     .D2T2   B13,*+SP(4)       ; |554| 
           MV      .D2     B3,B13            ; |554| 

           MV      .S1     A4,A10            ; |554| 
||         MVK     .D1     0x8,A6            ; |556| 
||         ADDKPC  .S2     RL214,B3,0        ; |556| 
||         ADD     .L1X    B4,A4,A4          ; |556| 
||         MVK     .L2     0x8,B4            ; |556| 

RL214:     ; CALL OCCURS {_CreateMatrixEx}   ; |556| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     0x412e8480,A7     ; |563| 
           MVK     .S1     15888,A3          ; |563| 

           MVKH    .S1     0x412e8480,A7     ; |563| 
||         MVKL    .S2     0x408f4000,B5     ; |564| 
||         ZERO    .L1     A5                ; |557| 

           MVKH    .S1     0x3fd00000,A5     ; |557| 
||         ADD     .L1     A3,A10,A4         ; |563| 
||         MVKH    .S2     0x408f4000,B5     ; |564| 
||         ZERO    .L2     B4                ; |564| 

           STDW    .D1T2   B5:B4,*+A4(200)   ; |564| 
||         MVK     .S1     15088,A3          ; |559| 
||         MVK     .S2     15488,B6          ; |561| 
||         ZERO    .L1     A6                ; |563| 

           ZERO    .L1     A4                ; |557| 
||         MVKL    .S2     0x40a38800,B5     ; |560| 
||         STDW    .D1T1   A7:A6,*A4         ; |563| 
||         ADD     .S1     A3,A10,A3         ; |559| 

           STDW    .D1T1   A5:A4,*A3         ; |559| 
||         MVKH    .S2     0x40a38800,B5     ; |560| 
||         MVK     .S1     14688,A6          ; |557| 
||         LDW     .D2T2   *+SP(4),B13       ; |567| 
||         MV      .L2     B13,B3            ; |567| 

           ADD     .L1     A6,A10,A3         ; |557| 
||         STDW    .D1T2   B5:B4,*+A3(200)   ; |560| 
||         ADD     .L2X    B6,A10,B6         ; |561| 

           STDW    .D1T1   A5:A4,*A3         ; |557| 
||         STDW    .D2T2   B5:B4,*+B6(200)   ; |562| 

           RET     .S2     B3                ; |567| 
||         STDW    .D1T1   A5:A4,*+A3(200)   ; |558| 
||         STDW    .D2T2   B5:B4,*B6         ; |561| 

           LDW     .D2T1   *++SP(8),A10      ; |567| 
	.dwpsn	"OPManager.c",567,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |567| 
	.dwattr DW$102, DW_AT_end_file("OPManager.c")
	.dwattr DW$102, DW_AT_end_line(0x237)
	.dwattr DW$102, DW_AT_end_column(0x01)
	.dwendtag DW$102

	.sect	".text"

DW$104	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_GetFirstSolution"), DW_AT_symbol_name("_OP_GetFirstSolution")
	.dwattr DW$104, DW_AT_low_pc(_OP_GetFirstSolution)
	.dwattr DW$104, DW_AT_high_pc(0x00)
	.dwattr DW$104, DW_AT_begin_file("OPManager.c")
	.dwattr DW$104, DW_AT_begin_line(0x242)
	.dwattr DW$104, DW_AT_begin_column(0x0e)
	.dwattr DW$104, DW_AT_frame_base[DW_OP_breg31 488]
	.dwattr DW$104, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",582,1

;******************************************************************************
;* FUNCTION NAME: _OP_GetFirstSolution                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,  *
;*                           A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31, *
;*                           B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26,B27, *
;*                           B28,B29,B30,B31                                  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,  *
;*                           A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31, *
;*                           B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26,B27, *
;*                           B28,B29,B30,B31                                  *
;*   Local Frame Size  : 0 Args + 468 Auto + 20 Save = 488 byte               *
;******************************************************************************
_OP_GetFirstSolution:
;** --------------------------------------------------------------------------*
DW$105	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$105, DW_AT_type(*DW$T$189)
	.dwattr DW$105, DW_AT_location[DW_OP_reg4]
DW$106	.dwtag  DW_TAG_formal_parameter, DW_AT_name("User"), DW_AT_symbol_name("_User")
	.dwattr DW$106, DW_AT_type(*DW$T$305)
	.dwattr DW$106, DW_AT_location[DW_OP_reg20]
DW$107	.dwtag  DW_TAG_formal_parameter, DW_AT_name("NavSolution"), DW_AT_symbol_name("_NavSolution")
	.dwattr DW$107, DW_AT_type(*DW$T$53)
	.dwattr DW$107, DW_AT_location[DW_OP_reg6]
DW$108	.dwtag  DW_TAG_formal_parameter, DW_AT_name("weekData"), DW_AT_symbol_name("_weekData")
	.dwattr DW$108, DW_AT_type(*DW$T$385)
	.dwattr DW$108, DW_AT_location[DW_OP_reg22]
           MVKL    .S1     _memcpy,A3        ; |582| 
           MVKH    .S1     _memcpy,A3        ; |582| 
           ADDK    .S2     -488,SP           ; |582| 
           CALL    .S2X    A3                ; |582| 
           STW     .D2T1   A13,*+SP(488)     ; |582| 
           STW     .D2T1   A11,*+SP(476)     ; |582| 
           STW     .D2T1   A10,*+SP(472)     ; |582| 

           MV      .S1X    B6,A13            ; |582| 
||         CMPEQ   .L1     A6,2,A11          ; |584| 
||         STW     .D2T2   B13,*+SP(484)     ; |582| 

           ADDKPC  .S2     RL215,B3,0        ; |582| 
||         ADD     .L1X    8,SP,A4           ; |582| 
||         MVK     .S1     0x1d0,A6          ; |582| 
||         MV      .L2     B3,B13            ; |582| 
||         MV      .D1     A4,A10            ; |582| 
||         STW     .D2T1   A12,*+SP(480)     ; |582| 

RL215:     ; CALL OCCURS {_memcpy}           ; |582| 
;** --------------------------------------------------------------------------*

           LDB     .D2T2   *+SP(8),B7        ; |584| 
||         MVK     .S2     14648,B8          ; |587| 
||         LDBU    .D1T1   *+A13(2),A3       ; |582| 
||         ZERO    .L1     A4                ; |628| 
||         MVKL    .S1     __mpyd,A7         ; |608| 

           LDB     .D2T2   *+SP(9),B6        ; |584| 
||         MVK     .S2     14672,B9          ; |597| 
||         LDW     .D1T1   *+A13(4),A0       ; |582| 
||         MVKH    .S1     __mpyd,A7         ; |608| 

           ADD     .L1X    B8,A10,A5         ; |587| 
||         LDDW    .D2T2   *+SP(168),B27:B26 ; |600| 

           ADD     .L1X    B9,A10,A12        ; |597| 
||         LDDW    .D2T2   *+SP(40),B21:B20  ; |587| 

           LDDW    .D2T2   *+SP(48),B25:B24  ; |588| 

           CMPEQ   .L2     B7,1,B7           ; |584| 
||         LDDW    .D2T2   *+SP(56),B23:B22  ; |589| 

           CMPEQ   .L2     B6,1,B6           ; |584| 
||         LDDW    .D2T2   *+SP(16),B19:B18  ; |592| 

           AND     .L2     B7,B6,B8          ; |584| 
||         LDDW    .D2T2   *+SP(160),B7:B6   ; |597| 

           LDDW    .D2T2   *+SP(24),B17:B16  ; |593| 

           AND     .L1X    A11,B8,A1         ; |584| 
||         LDDW    .D2T2   *+SP(32),B5:B4    ; |594| 

   [!A1]   BNOP    .S2     L24,1             ; |628| 
|| [ A1]   LDH     .D2T2   *+SP(176),B8      ; |607| 
|| [ A1]   MVK     .S1     19520,A4          ; |607| 

   [ A1]   LDH     .D2T2   *+SP(176),B9      ; |605| 
|| [ A1]   MVK     .S1     19504,A9          ; |607| 
|| [ A0]   ADD     .L1     A4,A10,A8         ; |607| 

   [ A1]   MVK     .S1     19520,A6          ; |605| 

   [ A1]   STDW    .D1T2   B17:B16,*-A5(16)  ; |593| 
|| [ A1]   MVK     .S2     19504,B16         ; |605| 
|| [ A1]   SHL     .S1     A3,10,A3          ; |605| 

   [ A1]   ADDK    .S2     1024,B8           ; |607| 
           ; BRANCHCC OCCURS {L24}           ; |628| 
;** --------------------------------------------------------------------------*
           STDW    .D1T2   B27:B26,*+A12(8)  ; |600| 
           STDW    .D1T2   B21:B20,*A5       ; |587| 
           STDW    .D1T2   B25:B24,*+A5(8)   ; |588| 
           STDW    .D1T2   B23:B22,*+A5(16)  ; |589| 

           STDW    .D1T2   B7:B6,*A12        ; |597| 
||         CALL    .S2X    A7                ; |608| 

           ADDKPC  .S2     RL217,B3,0        ; |608| 
|| [ A0]   STH     .D1T2   B8,*A8            ; |607| 

           MVKL    .S2     0xeb25f9db,B4     ; |608| 
||         STDW    .D1T2   B5:B4,*-A5(8)     ; |594| 
||         ADD     .L1X    A3,B9,A3          ; |605| 
||         ADD     .S1     A9,A10,A8         ; |607| 

           MVKL    .S2     0x3e2ca726,B5     ; |608| 
||         ADD     .L2X    B16,A10,B6        ; |605| 
||         MV      .L1X    B6,A4             ; |608| 
||         ADD     .S1     A6,A10,A6         ; |605| 
|| [ A0]   STH     .D1T2   B8,*A8            ; |607| 

           MVKH    .S2     0xeb25f9db,B4     ; |608| 
|| [!A0]   STH     .D2T1   A3,*B6            ; |605| 
||         STDW    .D1T2   B19:B18,*-A5(24)  ; |592| 

           MVKH    .S2     0x3e2ca726,B5     ; |608| 
|| [!A0]   STH     .D1T1   A3,*A6            ; |605| 
||         MV      .L1X    B7,A5             ; |608| 

RL217:     ; CALL OCCURS {__mpyd}            ; |608| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __subd,B6         ; |608| 

           LDDW    .D2T2   *+SP(184),B9:B8   ; |608| 
||         MVKH    .S2     __subd,B6         ; |608| 

           CALL    .S2     B6                ; |608| 
           MV      .L2X    A4,B4             ; |608| 
           ADDKPC  .S2     RL218,B3,0        ; |608| 
           MV      .L2X    A5,B5             ; |608| 
           MV      .L1X    B8,A4             ; |608| 
           MV      .L1X    B9,A5             ; |608| 
RL218:     ; CALL OCCURS {__subd}            ; |608| 
;** --------------------------------------------------------------------------*

           MVK     .S1     0x987,A3          ; |605| 
||         MVK     .S2     19500,B4          ; |612| 
||         MVK     .L2     1,B5              ; |610| 

           STDW    .D1T1   A5:A4,*+A10[A3]   ; |608| 
||         MVK     .S1     0x989,A31         ; |605| 
||         ADD     .L2X    B4,A10,B6         ; |612| 

           MVK     .S1     19537,A30         ; |610| 
||         STDW    .D1T1   A5:A4,*+A10[A31]  ; |608| 

           STB     .D1T2   B5,*+A10[A30]     ; |610| 
||         MVK     .S1     0x981,A6          ; |605| 

           LDBU    .D2T2   *B6,B4            ; |612| 
           NOP             4
           CMPEQ   .L2     B4,1,B0           ; |612| 

   [!B0]   BNOP    .S1     L23,5             ; |612| 
|| [ B0]   CMPEQ   .L2     B4,1,B0           ; |612| (P) <0,0>  ^ 

           ; BRANCHCC OCCURS {L23}           ; |612| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 612
;*      Loop closing brace source line   : 612
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 1
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        1*    
;*      .S units                     1*       0     
;*      .D units                     0        0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             0        0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1*       1*    
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop is interruptible
;*      Collapsed epilog stages     : 1
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L20:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L21:    ; PIPED LOOP KERNEL
DW$L$_OP_GetFirstSolution$7$B:
   [ B0]   BNOP    .S1     L21,4             ; |612| <0,1> 
           CMPEQ   .L2     B4,1,B0           ; |612| <1,0>  ^ 
DW$L$_OP_GetFirstSolution$7$E:
;** --------------------------------------------------------------------------*
L22:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
L23:    

           MVK     .L1     1,A4              ; |613| 
||         MVK     .S1     19496,A3          ; |614| 

           STB     .D2T1   A4,*B6            ; |613| 
||         ADD     .L1     A3,A10,A7         ; |614| 

           LDW     .D1T1   *A7,A0            ; |614| 
           NOP             1
           MVK     .S1     19456,A31         ; |616| 
           ADD     .L1     A31,A10,A3        ; |616| 
           MVK     .L1     1,A30             ; |618| 
   [!A0]   LDDW    .D1T2   *A12,B5:B4        ; |616| 
           NOP             4
   [!A0]   STDW    .D1T2   B5:B4,*A3         ; |616| 

   [!A0]   LDDW    .D1T1   *+A12(8),A5:A4    ; |617| 
||         MVKL    .S2     _OP_ResetEKF,B4   ; |623| 

           MVKH    .S2     _OP_ResetEKF,B4   ; |623| 
           CALL    .S2     B4                ; |623| 
           ZERO    .L1     A3                ; |620| 
           ADDKPC  .S2     RL219,B3,0        ; |623| 
   [!A0]   STDW    .D1T1   A5:A4,*+A10[A6]   ; |617| 
   [!A0]   STW     .D1T1   A30,*A7           ; |618| 

           STB     .D2T1   A3,*B6            ; |620| 
||         MV      .L1     A10,A4            ; |623| 

RL219:     ; CALL OCCURS {_OP_ResetEKF}      ; |623| 
;** --------------------------------------------------------------------------*
           MVK     .L1     0x1,A4            ; |625| 
;** --------------------------------------------------------------------------*
L24:    
           LDW     .D2T1   *+SP(476),A11     ; |629| 
           LDW     .D2T1   *+SP(472),A10     ; |629| 

           LDW     .D2T1   *+SP(480),A12     ; |629| 
||         MV      .L2     B13,B3            ; |629| 

           RET     .S2     B3                ; |629| 
||         LDW     .D2T1   *+SP(488),A13     ; |629| 

           LDW     .D2T2   *+SP(484),B13     ; |629| 
           NOP             3
	.dwpsn	"OPManager.c",629,1
           ADDK    .S2     488,SP            ; |629| 
           ; BRANCH OCCURS {B3}              ; |629| 

DW$109	.dwtag  DW_TAG_loop
	.dwattr DW$109, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L21:1:1472539744")
	.dwattr DW$109, DW_AT_begin_file("OPManager.c")
	.dwattr DW$109, DW_AT_begin_line(0x264)
	.dwattr DW$109, DW_AT_end_line(0x264)
DW$110	.dwtag  DW_TAG_loop_range
	.dwattr DW$110, DW_AT_low_pc(DW$L$_OP_GetFirstSolution$7$B)
	.dwattr DW$110, DW_AT_high_pc(DW$L$_OP_GetFirstSolution$7$E)
	.dwendtag DW$109

	.dwattr DW$104, DW_AT_end_file("OPManager.c")
	.dwattr DW$104, DW_AT_end_line(0x275)
	.dwattr DW$104, DW_AT_end_column(0x01)
	.dwendtag DW$104

	.sect	".text"

DW$111	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_Estimation"), DW_AT_symbol_name("_OP_Estimation")
	.dwattr DW$111, DW_AT_low_pc(_OP_Estimation)
	.dwattr DW$111, DW_AT_high_pc(0x00)
	.dwattr DW$111, DW_AT_begin_file("OPManager.c")
	.dwattr DW$111, DW_AT_begin_line(0x280)
	.dwattr DW$111, DW_AT_begin_column(0x0d)
	.dwattr DW$111, DW_AT_frame_base[DW_OP_breg31 160]
	.dwattr DW$111, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",647,1
DW$112	.dwtag  DW_TAG_variable, DW_AT_name("H"), DW_AT_symbol_name("_H$1")
	.dwattr DW$112, DW_AT_type(*DW$T$62)
	.dwattr DW$112, DW_AT_location[DW_OP_addr _H$1]
DW$113	.dwtag  DW_TAG_variable, DW_AT_name("Pp"), DW_AT_symbol_name("_Pp$2")
	.dwattr DW$113, DW_AT_type(*DW$T$62)
	.dwattr DW$113, DW_AT_location[DW_OP_addr _Pp$2]
DW$114	.dwtag  DW_TAG_variable, DW_AT_name("K"), DW_AT_symbol_name("_K$3")
	.dwattr DW$114, DW_AT_type(*DW$T$62)
	.dwattr DW$114, DW_AT_location[DW_OP_addr _K$3]
DW$115	.dwtag  DW_TAG_variable, DW_AT_name("X0"), DW_AT_symbol_name("_X0$4")
	.dwattr DW$115, DW_AT_type(*DW$T$61)
	.dwattr DW$115, DW_AT_location[DW_OP_addr _X0$4]
DW$116	.dwtag  DW_TAG_variable, DW_AT_name("Xp"), DW_AT_symbol_name("_Xp$5")
	.dwattr DW$116, DW_AT_type(*DW$T$61)
	.dwattr DW$116, DW_AT_location[DW_OP_addr _Xp$5]
DW$117	.dwtag  DW_TAG_variable, DW_AT_name("DX"), DW_AT_symbol_name("_DX$6")
	.dwattr DW$117, DW_AT_type(*DW$T$61)
	.dwattr DW$117, DW_AT_location[DW_OP_addr _DX$6]
DW$118	.dwtag  DW_TAG_variable, DW_AT_name("dX"), DW_AT_symbol_name("_dX$7")
	.dwattr DW$118, DW_AT_type(*DW$T$61)
	.dwattr DW$118, DW_AT_location[DW_OP_addr _dX$7]
DW$119	.dwtag  DW_TAG_variable, DW_AT_name("dYp"), DW_AT_symbol_name("_dYp$8")
	.dwattr DW$119, DW_AT_type(*DW$T$254)
	.dwattr DW$119, DW_AT_location[DW_OP_addr _dYp$8]
DW$120	.dwtag  DW_TAG_variable, DW_AT_name("dYv"), DW_AT_symbol_name("_dYv$9")
	.dwattr DW$120, DW_AT_type(*DW$T$254)
	.dwattr DW$120, DW_AT_location[DW_OP_addr _dYv$9]

;******************************************************************************
;* FUNCTION NAME: _OP_Estimation                                              *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 112 Auto + 44 Save = 156 byte               *
;******************************************************************************
_OP_Estimation:
;** --------------------------------------------------------------------------*
DW$121	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$121, DW_AT_type(*DW$T$189)
	.dwattr DW$121, DW_AT_location[DW_OP_reg4]
DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pTopSystem"), DW_AT_symbol_name("_pTopSystem")
	.dwattr DW$122, DW_AT_type(*DW$T$195)
	.dwattr DW$122, DW_AT_location[DW_OP_reg20]
DW$123	.dwtag  DW_TAG_formal_parameter, DW_AT_name("NavCond"), DW_AT_symbol_name("_NavCond")
	.dwattr DW$123, DW_AT_type(*DW$T$197)
	.dwattr DW$123, DW_AT_location[DW_OP_reg6]
DW$124	.dwtag  DW_TAG_formal_parameter, DW_AT_name("btUpdate"), DW_AT_symbol_name("_btUpdate")
	.dwattr DW$124, DW_AT_type(*DW$T$168)
	.dwattr DW$124, DW_AT_location[DW_OP_reg22]
DW$125	.dwtag  DW_TAG_formal_parameter, DW_AT_name("BinStrFlag"), DW_AT_symbol_name("_BinStrFlag")
	.dwattr DW$125, DW_AT_type(*DW$T$52)
	.dwattr DW$125, DW_AT_location[DW_OP_reg8]
DW$126	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataStr"), DW_AT_symbol_name("_dataStr")
	.dwattr DW$126, DW_AT_type(*DW$T$200)
	.dwattr DW$126, DW_AT_location[DW_OP_reg24]
DW$127	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DebugTerminal"), DW_AT_symbol_name("_DebugTerminal")
	.dwattr DW$127, DW_AT_type(*DW$T$202)
	.dwattr DW$127, DW_AT_location[DW_OP_reg10]

           ADDK    .S2     -160,SP           ; |647| 
||         MV      .L1X    SP,A31            ; |647| 

           STDW    .D2T2   B11:B10,*+SP(144)
||         MVKL    .S1     _MJD_From_UTCEx,A3 ; |658| 

           STW     .D2T1   A4,*+SP(16)       ; |647| 
||         MVKH    .S1     _MJD_From_UTCEx,A3 ; |658| 

           STW     .D2T1   A8,*+SP(28)       ; |647| 

           CALL    .S2X    A3                ; |658| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(140)

           STW     .D2T1   A6,*+SP(32)       ; |647| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B8,*+SP(20)       ; |647| 

           STW     .D2T1   A15,*+SP(160)

           STDW    .D2T2   B13:B12,*+SP(152)
||         MV      .L1X    B6,A11            ; |647| 
||         STW     .D1T1   A14,*-A31(24)
||         MV      .L2     B4,B11            ; |647| 
||         MVK     .S1     19504,A5          ; |658| 

           ADDKPC  .S2     RL223,B3,0        ; |658| 
||         ADD     .L1     A5,A4,A4          ; |658| 
||         STW     .D2T1   A11,*+SP(24)      ; |647| 
||         ADD     .L2     8,SP,B4           ; |658| 
||         MV      .S1     A4,A14            ; |647| 
||         MV      .D1     A4,A13            ; |647| 

RL223:     ; CALL OCCURS {_MJD_From_UTCEx}   ; |658| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __fltid,B4        ; |659| 
           MVK     .S2     11680,B5          ; |659| 
           MVKH    .S2     __fltid,B4        ; |659| 
           CALL    .S2     B4                ; |659| 
           LDB     .D2T1   *+B5[B11],A4      ; |659| 
           ADDKPC  .S2     RL227,B3,3        ; |659| 
RL227:     ; CALL OCCURS {__fltid}           ; |659| 
           MVKL    .S1     __mpyd,A3         ; |659| 
           MVKH    .S1     __mpyd,A3         ; |659| 
           MVKL    .S2     0x3ee845c8,B5     ; |659| 
           CALL    .S2X    A3                ; |659| 
           MVKL    .S2     0xa0ce5129,B4     ; |659| 
           MVKH    .S2     0x3ee845c8,B5     ; |659| 
           MVKH    .S2     0xa0ce5129,B4     ; |659| 
           ADDKPC  .S2     RL228,B3,1        ; |659| 
RL228:     ; CALL OCCURS {__mpyd}            ; |659| 
           MVKL    .S1     __subd,A3         ; |659| 
           MVKH    .S1     __subd,A3         ; |659| 
           LDDW    .D2T2   *+SP(8),B7:B6     ; |659| 
           CALL    .S2X    A3                ; |659| 
           MV      .L2X    A4,B4             ; |659| 
           ADDKPC  .S2     RL229,B3,0        ; |659| 
           MV      .L2X    A5,B5             ; |659| 
           MV      .L1X    B6,A4             ; |659| 
           MV      .L1X    B7,A5             ; |659| 
RL229:     ; CALL OCCURS {__subd}            ; |659| 
           MVK     .S1     19504,A3          ; |660| 
           ADD     .L1     A3,A14,A3         ; |660| 
           STW     .D2T1   A3,*+SP(36)       ; |660| 

           MVKL    .S1     __fltid,A3        ; |660| 
||         LDH     .D1T1   *A3,A6            ; |660| 

           MVKH    .S1     __fltid,A3        ; |660| 
           MV      .L2X    A11,B5            ; |660| 

           CALL    .S2X    A3                ; |660| 
||         LDH     .D2T2   *B5,B4            ; |660| 

           LDDW    .D2T2   *+B5(8),B13:B12   ; |660| 
           STW     .D2T1   A4,*+SP(40)       ; |659| 
           STW     .D2T1   A5,*+SP(44)       ; |659| 
           ADDKPC  .S2     RL231,B3,0        ; |660| 

           STDW    .D2T1   A5:A4,*+SP(8)     ; |659| 
||         SUB     .L1X    B4,A6,A4          ; |660| 

RL231:     ; CALL OCCURS {__fltid}           ; |660| 
           MVKL    .S2     __mpyd,B6         ; |660| 
           MVKH    .S2     __mpyd,B6         ; |660| 
           CALL    .S2     B6                ; |660| 
           MVKL    .S2     0x41227500,B5     ; |660| 
           MVKH    .S2     0x41227500,B5     ; |660| 
           ADDKPC  .S2     RL232,B3,1        ; |660| 
           ZERO    .L2     B4                ; |660| 
RL232:     ; CALL OCCURS {__mpyd}            ; |660| 
           MVKL    .S1     __subd,A3         ; |660| 

           MVK     .S2     19512,B4          ; |660| 
||         MVKH    .S1     __subd,A3         ; |660| 

           ADD     .L2X    B4,A14,B10        ; |660| 
           CALL    .S2X    A3                ; |660| 
           LDDW    .D2T2   *B10,B5:B4        ; |660| 
           MV      .S1     A4,A12            ; |660| 
           MV      .L1X    B12,A4            ; |660| 
           MV      .S1     A5,A15            ; |660| 

           ADDKPC  .S2     RL234,B3,0        ; |660| 
||         MV      .L1X    B13,A5            ; |660| 

RL234:     ; CALL OCCURS {__subd}            ; |660| 
           MVKL    .S2     __addd,B6         ; |660| 
           MVKH    .S2     __addd,B6         ; |660| 
           CALL    .S2     B6                ; |660| 
           MV      .L2X    A12,B4            ; |660| 
           MV      .L2X    A15,B5            ; |660| 
           ADDKPC  .S2     RL235,B3,2        ; |660| 
RL235:     ; CALL OCCURS {__addd}            ; |660| 
;** --------------------------------------------------------------------------*

           MVK     .S2     14664,B6          ; |40| 
||         STW     .D2T1   A5,*+SP(60)       ; |660| 
||         MVK     .S1     14656,A5          ; |40| 
||         MV      .L2X    A5,B5             ; |660| 

           STW     .D2T1   A4,*+SP(64)       ; |660| 
||         MV      .L2X    A4,B4             ; |660| 
||         MVK     .S2     14648,B7          ; |40| 
||         MVK     .S1     14680,A7          ; |681| 

           ADD     .L1X    B6,A13,A4         ; |40| 
||         MVK     .S2     14640,B6          ; |40| 
||         MVK     .S1     19496,A6          ; |680| 
||         ADD     .D1     A7,A13,A7         ; |681| 

           STW     .D2T1   A4,*+SP(68)       ; |40| 
||         MVK     .S1     14672,A8          ; |40| 
||         ADD     .L1     A6,A14,A9         ; |680| 
||         MVKL    .S2     _OP_CovMatrPropagation,B16 ; |692| 

           LDDW    .D1T1   *A4,A21:A20       ; |40| 
||         ADD     .L1     A5,A13,A4         ; |40| 
||         MVK     .S1     14632,A5          ; |40| 
||         MVKH    .S2     _OP_CovMatrPropagation,B16 ; |692| 

           STW     .D2T1   A4,*+SP(72)       ; |40| 
||         ADD     .L1     A8,A13,A16        ; |40| 
||         ADD     .D1     A8,A13,A18        ; |40| 
||         MVKL    .S1     _Xp$5,A31         ; |680| 

           LDDW    .D1T1   *A4,A23:A22       ; |40| 
||         ADD     .L1X    B7,A13,A4         ; |40| 
||         MVKH    .S1     _Xp$5,A31         ; |680| 
||         MVKL    .S2     _Pp$2,B7          ; |692| 

           STW     .D2T1   A4,*+SP(76)       ; |40| 
||         MVKL    .S1     _X0$4,A3          ; |40| 
||         MVKH    .S2     _Pp$2,B7          ; |692| 

           LDDW    .D1T1   *A4,A25:A24       ; |40| 
||         ADD     .L1X    B6,A13,A4         ; |40| 
||         MVK     .S2     14624,B6          ; |40| 
||         MVKH    .S1     _X0$4,A3          ; |40| 

           STW     .D2T1   A4,*+SP(80)       ; |40| 
||         LDDW    .D1T2   *A4,B9:B8         ; |40| 
||         ADD     .L1     A5,A13,A4         ; |40| 
||         MVKL    .S2     _rk4,B13          ; |689| 
||         MV      .S1X    B13,A5            ; |689| 

           STW     .D2T1   A4,*+SP(84)       ; |40| 
||         ADD     .D1     A3,24,A14         ; |40| 
||         MV      .L1     A3,A11            ; |40| 
||         MVKL    .S1     _DX$6,A30         ; |699| 
||         MVKH    .S2     _rk4,B13          ; |689| 

           MVKL    .S2     _Pp$2,B6          ; |702| 
||         LDDW    .D1T1   *A4,A27:A26       ; |40| 
||         ADD     .L1X    B6,A13,A4         ; |40| 
||         MVKH    .S1     _DX$6,A30         ; |699| 

           STW     .D2T1   A4,*+SP(88)       ; |40| 
||         MVKH    .S2     _Pp$2,B6          ; |702| 

           MVK     .S1     19536,A4          ; |684| 
||         LDDW    .D1T1   *A4,A29:A28       ; |40| 

           STW     .D2T1   A7,*+SP(48)       ; |681| 
||         STDW    .D1T2   B9:B8,*+A3(16)    ; |40| 

           STW     .D2T1   A9,*+SP(52)       ; |681| 
           STW     .D2T1   A16,*+SP(56)      ; |680| 

           MVKL    .S2     __addd,B12        ; |689| 
||         LDB     .D1T1   *+A4[A13],A0      ; |684| 
||         MV      .L1X    B12,A4            ; |689| 
||         STW     .D2T2   B6,*+SP(92)       ; |702| 

           LDDW    .D1T1   *A7,A7:A6         ; |681| 
||         MVKH    .S2     __addd,B12        ; |689| 
||         STW     .D2T2   B7,*+SP(96)       ; |692| 

           LDDW    .D1T1   *+A18(8),A17:A16  ; |680| 
||         STW     .D2T2   B7,*+SP(100)      ; |692| 
||         MVK     .S2     14688,B7          ; |702| 

           LDDW    .D1T1   *-A9(24),A9:A8    ; |680| 
||         STW     .D2T2   B16,*+SP(108)     ; |40| 

           LDDW    .D1T1   *A18,A19:A18      ; |40| 
           STDW    .D1T1   A29:A28,*A3       ; |40| 

           MVKL    .S1     _DX$6,A6          ; |689| 
||         STDW    .D1T1   A7:A6,*+A31(56)   ; |681| 

           MVK     .S1     14688,A16         ; |692| 
||         STDW    .D1T1   A17:A16,*+A3(56)  ; |680| 
||         MV      .L1     A6,A17            ; |40| 

           MVKH    .S1     _DX$6,A17         ; |689| 
||         STDW    .D1T1   A27:A26,*+A3(8)   ; |40| 
||         ZERO    .L1     A7:A6             ; |699| 

           STDW    .D1T1   A23:A22,*+A3(32)  ; |40| 

           STDW    .D1T1   A21:A20,*+A3(40)  ; |40| 
|| [ A0]   B       .S1     L25               ; |684| 

           MVKL    .S1     __subd,A8         ; |689| 
||         STDW    .D1T1   A9:A8,*+A31(48)   ; |680| 

           MVKL    .S1     _OP_CovMatrPropagation_Static,A9 ; |702| 
||         ADD     .L1     A16,A13,A3        ; |692| 
||         STDW    .D1T1   A19:A18,*+A3(48)  ; |40| 

           STW     .D2T1   A3,*+SP(112)      ; |40| 
||         MVKH    .S1     __subd,A8         ; |689| 

           STDW    .D1T1   A25:A24,*A14      ; |40| 
||         MVKH    .S1     _OP_CovMatrPropagation_Static,A9 ; |702| 

           STW     .D2T1   A17,*+SP(104)     ; |40| 
           ; BRANCHCC OCCURS {L25}           ; |684| 
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *+SP(40),A6

           LDW     .D2T1   *+SP(44),A7
||         CALL    .S2X    A8                ; |689| 

           LDDW    .D2T2   *B10,B5:B4        ; |689| 
           ADDKPC  .S2     RL240,B3,2        ; |689| 
           STDW    .D1T1   A7:A6,*+A11(48)   ; |688| 
RL240:     ; CALL OCCURS {__subd}            ; |689| 
;** --------------------------------------------------------------------------*
           CALL    .S2     B12               ; |689| 
           MV      .L2X    A12,B4            ; |689| 
           ADDKPC  .S2     RL241,B3,2        ; |689| 
           MV      .L2X    A15,B5            ; |689| 
RL241:     ; CALL OCCURS {__addd}            ; |689| 

           MVK     .S1     3634,A3           ; |689| 
||         CALL    .S2     B13               ; |689| 

           LDW     .D1T1   *+A13[A3],A4      ; |689| 
||         LDW     .D2T2   *+SP(104),B8      ; |689| 
||         MV      .L2X    A4,B6             ; |689| 

           MV      .L2X    A11,B4            ; |689| 
           MVK     .S1     0x7,A6            ; |689| 
           MV      .L1     A13,A8            ; |689| 

           MV      .L2X    A5,B7             ; |689| 
||         ADDKPC  .S2     RL242,B3,0        ; |689| 

RL242:     ; CALL OCCURS {_rk4}              ; |689| 
           LDW     .D2T2   *+SP(108),B5      ; |692| 
           LDW     .D2T1   *+SP(112),A8      ; |692| 
           LDW     .D2T2   *+SP(60),B7       ; |692| 
           LDW     .D2T2   *+SP(100),B8      ; |692| 
           LDW     .D2T2   *+SP(64),B6       ; |692| 
           CALL    .S2     B5                ; |692| 
           MV      .S1     A13,A4            ; |692| 
           MV      .L2X    A14,B4            ; |692| 
           MV      .L1     A11,A6            ; |692| 
           ADDKPC  .S2     RL243,B3,1        ; |692| 
RL243:     ; CALL OCCURS {_OP_CovMatrPropagation}  ; |692| 
;** --------------------------------------------------------------------------*

           MVKL    .S1     _DX$6,A3
||         MVKL    .S2     _Xp$5,B4          ; |680| 
||         MVK     .L2     0x3,B5            ; |706| 

           BNOP    .S2     L26,1             ; |693| 
||         MVKH    .S1     _DX$6,A3

           MVKH    .S2     _Xp$5,B4          ; |680| 

           MV      .L2X    A3,B12
||         MVKL    .S1     __addd,A3         ; |708| 

           MVKH    .S1     __addd,A3         ; |708| 
           NOP             1
           ; BRANCH OCCURS {L26}             ; |693| 
;** --------------------------------------------------------------------------*
L25:    
           STDW    .D1T1   A7:A6,*+A30(40)   ; |699| 

           STDW    .D1T1   A7:A6,*+A30(32)   ; |699| 
||         CALL    .S2X    A9                ; |702| 

           STDW    .D1T1   A7:A6,*+A30(24)   ; |699| 
           STDW    .D1T1   A7:A6,*+A30(16)   ; |699| 
           STDW    .D1T1   A7:A6,*+A30(8)    ; |699| 

           STDW    .D1T1   A7:A6,*A30        ; |699| 
||         ADD     .L1X    B7,A13,A6         ; |702| 

           ADDKPC  .S2     RL244,B3,0        ; |702| 
||         STW     .D2T1   A6,*+SP(112)      ; |702| 
||         MV      .L1     A13,A4            ; |702| 

RL244:     ; CALL OCCURS {_OP_CovMatrPropagation_Static}  ; |702| 
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *+SP(92),A3       ; |702| 
           NOP             2
           MVKL    .S2     _Xp$5,B4          ; |680| 
           MVKH    .S2     _Xp$5,B4          ; |680| 

           MVKL    .S1     _DX$6,A3
||         STW     .D2T1   A3,*+SP(96)       ; |702| 

           MVKH    .S1     _DX$6,A3
           MVK     .L2     0x3,B5            ; |706| 

           MVKL    .S1     __addd,A3         ; |708| 
||         MV      .L2X    A3,B12

           MVKH    .S1     __addd,A3         ; |708| 
           NOP             1
;** --------------------------------------------------------------------------*
L26:    

           CALL    .S2X    A3                ; |708| 
||         STW     .D2T2   B4,*+SP(64)       ; |680| 
||         MV      .L1X    B4,A14            ; |680| 

           LDDW    .D1T1   *+A11(24),A5:A4   ; |708| 
||         MV      .L1X    B5,A15
||         LDDW    .D2T2   *+B12(24),B5:B4   ; |708| 

	.dwpsn	"OPManager.c",706,0
           NOP             2
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L27:    
DW$L$_OP_Estimation$10$B:
	.dwpsn	"OPManager.c",707,0
           ADDKPC  .S2     RL245,B3,1        ; |708| 
RL245:     ; CALL OCCURS {__addd}            ; |708| 
DW$L$_OP_Estimation$10$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Estimation$11$B:
           MVKL    .S2     __addd,B6         ; |709| 
           MVKH    .S2     __addd,B6         ; |709| 

           CALL    .S2     B6                ; |709| 
||         LDDW    .D1T1   *A11++,A7:A6      ; |709| 

           LDDW    .D2T2   *B12++,B5:B4      ; |709| 
           ADDKPC  .S2     RL246,B3,0        ; |709| 
           STDW    .D1T1   A5:A4,*+A14(24)   ; |708| 
           NOP             1

           MV      .S1     A6,A4             ; |709| 
||         MV      .L1     A7,A5             ; |709| 

RL246:     ; CALL OCCURS {__addd}            ; |709| 
DW$L$_OP_Estimation$11$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Estimation$12$B:

           SUB     .L1     A15,1,A0          ; |706| 
||         MVKL    .S1     __addd,A3         ; |708| 
||         STDW    .D1T1   A5:A4,*A14++      ; |709| 

   [ A0]   B       .S2     L27               ; |706| 
||         MVKH    .S1     __addd,A3         ; |708| 
||         SUB     .L1     A15,1,A15         ; |706| 
|| [!A0]   LDW     .D2T2   *+SP(24),B4
|| [ A0]   LDDW    .D1T1   *+A11(24),A5:A4   ; |708| 

   [!A0]   LDW     .D2T1   *+SP(36),A4

   [ A0]   CALL    .S2X    A3                ; |708| 
|| [ A0]   LDDW    .D2T2   *+B12(24),B5:B4   ; |708| 

           NOP             2
	.dwpsn	"OPManager.c",710,0
   [!A0]   LDH     .D2T1   *B4,A3            ; |724| 
           ; BRANCHCC OCCURS {L27}           ; |706| 
DW$L$_OP_Estimation$12$E:
;** --------------------------------------------------------------------------*

           MVK     .S1     32,A31            ; |730| 
||         MVKL    .S2     _Xp$5,B9          ; |40| 
||         MV      .L2X    A10,B12           ; |741| 
||         MVK     .D2     2,B13             ; |735| 

           MVKL    .S1     _OP_HMatrixCalulation,A7 ; |741| 
||         MVKL    .S2     _dYv$9,B8         ; |741| 

           MVKH    .S1     _OP_HMatrixCalulation,A7 ; |741| 
||         MVKL    .S2     _CopyMatrixEx,B7  ; |734| 

           MVKL    .S1     _H$1,A8           ; |741| 
||         MVKH    .S2     _Xp$5,B9          ; |40| 

           STH     .D1T1   A3,*A4            ; |724| 
||         MVKH    .S1     _H$1,A8           ; |741| 
||         MV      .L1     A13,A4            ; |741| 
||         MVKH    .S2     _dYv$9,B8         ; |741| 

           LDDW    .D2T2   *+B4(8),B5:B4     ; |725| 
||         MV      .L1     A8,A11            ; |741| 
||         MVKH    .S2     _CopyMatrixEx,B7  ; |734| 

           LDW     .D2T1   *+SP(32),A6       ; |741| 
           NOP             3

           MV      .L2     B11,B4            ; |741| 
||         STDW    .D2T2   B5:B4,*B10        ; |725| 

           LDW     .D2T2   *+SP(64),B6       ; |741| 
||         MVKL    .S1     _dYp$8,A31        ; |741| 
||         LDB     .D1T1   *+A31[A6],A5      ; |730| 

           MVKH    .S1     _dYp$8,A31        ; |741| 
           LDW     .D2T1   *+SP(20),A12      ; |741| 
           MV      .L1     A31,A10           ; |741| 
           LDW     .D2T2   *+SP(28),B10      ; |741| 
           CMPLT   .L1     A5,2,A0           ; |730| 

   [ A0]   LDW     .D2T1   *+SP(88),A3
|| [ A0]   B       .S1     L33               ; |730| 

   [ A0]   LDDW    .D2T1   *B9,A5:A4         ; |40| 
|| [!A0]   CALL    .S2X    A7                ; |741| 

           NOP             4
           ; BRANCHCC OCCURS {L33}           ; |730| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL247,B3,0        ; |741| 
RL247:     ; CALL OCCURS {_OP_HMatrixCalulation}  ; |741| 
;** --------------------------------------------------------------------------*

           CMPLT   .L1     A4,2,A0           ; |741| 
||         MVKL    .S2     _OP_KF_Gain,B5    ; |759| 
||         LDW     .D2T1   *+SP(96),A6       ; |759| 
||         MVKL    .S1     _Xp$5,A3          ; |40| 
||         MV      .D1     A4,A8             ; |741| 
||         MV      .L2X    A11,B6            ; |741| 

   [ A0]   B       .S1     L32               ; |741| 
||         MVKH    .S2     _OP_KF_Gain,B5    ; |759| 
||         MV      .L1     A13,A4            ; |759| 
||         MVK     .D1     2,A10             ; |749| 
|| [ A0]   LDW     .D2T1   *+SP(88),A6
||         MV      .L2X    A4,B10            ; |741| 

           MVKL    .S2     _K$3,B4           ; |759| 
||         MVKL    .S1     _CopyMatrixEx,A7  ; |748| 
||         STW     .D2T1   A11,*+SP(36)      ; |741| 

           MVKH    .S2     _K$3,B4           ; |759| 
||         MVKH    .S1     _Xp$5,A3          ; |40| 

   [!A0]   CALL    .S2     B5                ; |759| 
||         MVKH    .S1     _CopyMatrixEx,A7  ; |748| 
||         MV      .L2     B4,B12            ; |759| 
|| [ A0]   LDDW    .D1T1   *A3,A5:A4         ; |40| 

           MVK     .S2     340,B13           ; |752| 
           MVK     .S1     19537,A12         ; |749| 
           ; BRANCHCC OCCURS {L32}           ; |741| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL248,B3,2        ; |759| 
RL248:     ; CALL OCCURS {_OP_KF_Gain}       ; |759| 
;** --------------------------------------------------------------------------*

           MVK     .S2     0x481,B4          ; |761| 
||         MVKL    .S1     _K$3,A14          ; |759| 
||         MVK     .L2     0x8,B7
||         ZERO    .L1     A15
||         MV      .D1     A13,A4            ; |766| 

           LDW     .D2T2   *+B12[B4],B0      ; |761| 
||         MVKL    .S2     _memset,B6        ; |777| 
||         MVKL    .S1     _OPMgr_Init,A5    ; |766| 
||         ZERO    .L2     B4                ; |777| 

           MVKH    .S2     _memset,B6        ; |777| 
||         STW     .D2T2   B7,*+SP(84)       ; |777| 
||         MVKH    .S1     _K$3,A14          ; |759| 

           MVKL    .S2     _dX$7,B5          ; |777| 
||         MVKH    .S1     _OPMgr_Init,A5    ; |766| 

           MVKH    .S2     _dX$7,B5          ; |777| 
||         MVK     .S1     0x40,A6           ; |777| 

           SHL     .S2     B10,3,B13
||         MVK     .S1     78,A7             ; |766| 

           MV      .L1X    B5,A10            ; |777| 
|| [!B0]   LDW     .D2T1   *+SP(32),A3
|| [ B0]   B       .S1     L28               ; |761| 

   [ B0]   CALL    .S2     B6                ; |777| 
           NOP             2
   [!B0]   CALL    .S2X    A5                ; |766| 
   [!B0]   LDB     .D1T2   *+A7[A3],B4       ; |766| 
           ; BRANCHCC OCCURS {L28}           ; |761| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL249,B3,3        ; |766| 
RL249:     ; CALL OCCURS {_OPMgr_Init}       ; |766| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(140),B3      ; |807| 
           LDDW    .D2T1   *+SP(120),A11:A10 ; |807| 
           LDDW    .D2T1   *+SP(128),A13:A12 ; |807| 
           LDW     .D2T1   *+SP(136),A14     ; |807| 
           LDDW    .D2T2   *+SP(144),B11:B10 ; |807| 

           RET     .S2     B3                ; |807| 
||         LDW     .D2T1   *+SP(160),A15     ; |807| 

           LDDW    .D2T2   *+SP(152),B13:B12 ; |807| 
           NOP             3
           ADDK    .S2     160,SP            ; |807| 
           ; BRANCH OCCURS {B3}              ; |807| 
;** --------------------------------------------------------------------------*
L28:    

           MV      .L1X    B5,A4             ; |777| 
||         ADDKPC  .S2     RL250,B3,0        ; |777| 

RL250:     ; CALL OCCURS {_memset}           ; |777| 
           MVKL    .S1     _dYv$9,A3
           MVKH    .S1     _dYv$9,A3
           NOP             1

           MVKL    .S1     __mpyd,A3         ; |781| 
||         MV      .L2X    A3,B11

           MVKH    .S1     __mpyd,A3         ; |781| 
	.dwpsn	"OPManager.c",778,0
           NOP             1
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L29
;** --------------------------------------------------------------------------*
L29:    
DW$L$_OP_Estimation$21$B:
	.dwpsn	"OPManager.c",779,0

           STW     .D2T2   B10,*+SP(80)
||         ADD     .L1     A15,A14,A11
||         CALL    .S2X    A3                ; |781| 
||         ADD     .S1X    B13,A14,A12

           LDDW    .D1T1   *A11++,A5:A4      ; |781| 
||         LDDW    .D2T2   *B11++,B5:B4      ; |781| 
||         MVKL    .S2     _dYp$8,B12

           NOP             2
           MVKH    .S2     _dYp$8,B12
DW$L$_OP_Estimation$21$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L30:    
DW$L$_OP_Estimation$22$B:
	.dwpsn	"OPManager.c",780,0
           ADDKPC  .S2     RL256,B3,0        ; |781| 
RL256:     ; CALL OCCURS {__mpyd}            ; |781| 
DW$L$_OP_Estimation$22$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Estimation$23$B:
           MVKL    .S1     __mpyd,A3         ; |781| 

           MVKH    .S1     __mpyd,A3         ; |781| 
||         LDDW    .D2T2   *B12++,B5:B4      ; |781| 

           STW     .D2T1   A5,*+SP(72)       ; |781| 

           CALL    .S2X    A3                ; |781| 
||         LDDW    .D1T1   *A12++,A7:A6      ; |781| 

           ADDKPC  .S2     RL257,B3,1        ; |781| 
           STW     .D2T1   A4,*+SP(76)       ; |781| 
           NOP             1

           MV      .L1     A6,A4             ; |781| 
||         MV      .S1     A7,A5             ; |781| 

RL257:     ; CALL OCCURS {__mpyd}            ; |781| 
           MVKL    .S1     __addd,A3         ; |781| 
           MVKH    .S1     __addd,A3         ; |781| 
           MV      .L2X    A5,B5             ; |781| 

           LDW     .D2T1   *+SP(72),A5       ; |781| 
||         CALL    .S2X    A3                ; |781| 

           LDW     .D2T1   *+SP(76),A4       ; |781| 
||         MV      .L2X    A4,B4             ; |781| 

           ADDKPC  .S2     RL258,B3,3        ; |781| 
RL258:     ; CALL OCCURS {__addd}            ; |781| 
           MVKL    .S2     __addd,B6         ; |781| 
           MVKH    .S2     __addd,B6         ; |781| 

           LDDW    .D1T1   *A10,A7:A6        ; |781| 
||         CALL    .S2     B6                ; |781| 

           MV      .L2X    A4,B4             ; |781| 
           MV      .L2X    A5,B5             ; |781| 
           ADDKPC  .S2     RL259,B3,1        ; |781| 

           MV      .L1     A7,A5             ; |781| 
||         MV      .S1     A6,A4             ; |781| 

RL259:     ; CALL OCCURS {__addd}            ; |781| 
DW$L$_OP_Estimation$23$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Estimation$24$B:

           LDW     .D2T2   *+SP(80),B4       ; |781| 
||         MVKL    .S1     __mpyd,A3         ; |781| 
||         STDW    .D1T1   A5:A4,*A10        ; |781| 

           NOP             3
           MVKH    .S1     __mpyd,A3         ; |781| 

           SUB     .L2     B4,1,B4           ; |779| 
||         SUB     .L1X    B4,1,A0           ; |779| 

   [!A0]   ADD     .L1     8,A10,A10         ; |778| 
|| [!A0]   ADDK    .S2     192,B13           ; |778| 
|| [!A0]   ADDK    .S1     192,A15           ; |778| 
||         STW     .D2T2   B4,*+SP(80)       ; |781| 

   [ A0]   B       .S1     L30               ; |779| 
|| [!A0]   LDW     .D2T2   *+SP(84),B4
|| [ A0]   LDDW    .D1T1   *A11++,A5:A4      ; |781| 

   [ A0]   CALL    .S2X    A3                ; |781| 
|| [ A0]   LDDW    .D2T2   *B11++,B5:B4      ; |781| 

           NOP             3
	.dwpsn	"OPManager.c",782,0
   [!A0]   SUB     .L1X    B4,1,A0           ; |778| 
           ; BRANCHCC OCCURS {L30}           ; |779| 
DW$L$_OP_Estimation$24$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Estimation$25$B:

           MVKL    .S1     _dYv$9,A3
|| [ A0]   B       .S2     L29               ; |778| 
||         SUB     .L2     B4,1,B4           ; |778| 

           MVKH    .S1     _dYv$9,A3
||         STW     .D2T2   B4,*+SP(84)       ; |778| 

   [!A0]   LDW     .D2T2   *+SP(24),B4

           MV      .L2X    A3,B11
||         MVKL    .S1     __mpyd,A3         ; |781| 

           MVKH    .S1     __mpyd,A3         ; |781| 
           NOP             1
           ; BRANCHCC OCCURS {L29}           ; |778| 
DW$L$_OP_Estimation$25$E:
;** --------------------------------------------------------------------------*

           LDW     .D2T1   *+SP(52),A3
||         MVKL    .S1     _dX$7,A31
||         MVKL    .S2     __addd,B6         ; |789| 

           LDH     .D2T2   *B4,B4            ; |785| 
||         MVKH    .S1     _dX$7,A31
||         MVKH    .S2     __addd,B6         ; |789| 

           NOP             3
           MV      .L2X    A31,B11
           STH     .D1T2   B4,*+A3(24)       ; |785| 
           LDW     .D2T2   *+SP(24),B31      ; |785| 
           NOP             3
           LDW     .D2T1   *+SP(52),A30
           LDDW    .D2T2   *+B31(8),B5:B4    ; |786| 
           NOP             4

           MVK     .L2     0x8,B4            ; |787| 
||         STDW    .D1T2   B5:B4,*+A30(32)   ; |786| 

           LDW     .D2T1   *+SP(64),A11
           MV      .L1X    B4,A12            ; |786| 
           LDDW    .D2T2   *B11++,B5:B4      ; |789| 
           LDW     .D2T1   *+SP(88),A10      ; |786| 
           CALL    .S2     B6                ; |789| 
           LDDW    .D1T1   *A11++,A5:A4      ; |789| 
	.dwpsn	"OPManager.c",787,0
           NOP             3
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L31:    
DW$L$_OP_Estimation$27$B:
	.dwpsn	"OPManager.c",788,0
           ADDKPC  .S2     RL260,B3,0        ; |789| 
RL260:     ; CALL OCCURS {__addd}            ; |789| 
DW$L$_OP_Estimation$27$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Estimation$28$B:

           MVKL    .S2     __addd,B6         ; |789| 
||         SUB     .L1     A12,1,A0          ; |787| 
||         STDW    .D1T1   A5:A4,*A10++      ; |789| 
||         SUB     .S1     A12,1,A12         ; |787| 

           MVKH    .S2     __addd,B6         ; |789| 
|| [!A0]   MVKL    .S1     _OP_CovMatrUpdate,A3 ; |805| 
|| [!A0]   LDW     .D2T2   *+SP(112),B4      ; |805| 
|| [ A0]   LDDW    .D1T1   *A11++,A5:A4      ; |789| 

   [ A0]   B       .S2     L31               ; |787| 
|| [!A0]   MVKH    .S1     _OP_CovMatrUpdate,A3 ; |805| 
|| [!A0]   LDW     .D2T1   *+SP(36),A8       ; |805| 

   [ A0]   CALL    .S2     B6                ; |789| 
|| [!A0]   LDW     .D2T2   *+SP(96),B6       ; |805| 

   [!A0]   CALL    .S2X    A3                ; |805| 
|| [ A0]   LDDW    .D2T2   *B11++,B5:B4      ; |789| 

	.dwpsn	"OPManager.c",790,0
           NOP             3
           ; BRANCHCC OCCURS {L31}           ; |787| 
DW$L$_OP_Estimation$28$E:
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL261,B3,0        ; |805| 

           MV      .L2     B10,B8            ; |805| 
||         MV      .L1     A14,A6            ; |805| 
||         MV      .S1     A13,A4            ; |805| 

RL261:     ; CALL OCCURS {_OP_CovMatrUpdate}  ; |805| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(140),B3      ; |807| 
           LDDW    .D2T1   *+SP(120),A11:A10 ; |807| 
           LDDW    .D2T1   *+SP(128),A13:A12 ; |807| 
           LDW     .D2T1   *+SP(136),A14     ; |807| 
           LDDW    .D2T2   *+SP(144),B11:B10 ; |807| 

           RET     .S2     B3                ; |807| 
||         LDW     .D2T1   *+SP(160),A15     ; |807| 

           LDDW    .D2T2   *+SP(152),B13:B12 ; |807| 
           NOP             3
           ADDK    .S2     160,SP            ; |807| 
           ; BRANCH OCCURS {B3}              ; |807| 
;** --------------------------------------------------------------------------*
L32:    
           NOP             1
           LDW     .D2T2   *+SP(96),B4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(8),A5:A4     ; |40| 
           LDW     .D2T1   *+SP(84),A6
           NOP             4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(16),A5:A4    ; |40| 
           LDW     .D2T1   *+SP(80),A6
           NOP             4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(24),A5:A4    ; |40| 
           LDW     .D2T1   *+SP(76),A6
           NOP             4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(32),A5:A4    ; |40| 
           LDW     .D2T1   *+SP(72),A6
           NOP             4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(40),A5:A4    ; |40| 
           LDW     .D2T1   *+SP(68),A6
           NOP             4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(48),A5:A4    ; |40| 
           LDW     .D2T1   *+SP(56),A6
           NOP             4
           STDW    .D1T1   A5:A4,*A6         ; |40| 
           LDDW    .D1T1   *+A3(56),A9:A8    ; |40| 

           CALL    .S2X    A7                ; |748| 
||         LDW     .D2T1   *+SP(48),A3       ; |748| 

           LDW     .D2T1   *+SP(112),A4      ; |748| 
           ADDKPC  .S2     RL262,B3,2        ; |748| 
           STDW    .D1T1   A9:A8,*A3         ; |40| 
RL262:     ; CALL OCCURS {_CopyMatrixEx}     ; |748| 
;** --------------------------------------------------------------------------*
           MVK     .S1     3092,A3           ; |752| 
           STB     .D1T1   A10,*+A13[A12]    ; |749| 

           MVK     .S1     2404,A3           ; |752| 
||         ADD     .L2X    A3,B11,B5         ; |752| 

           LDB     .D2T1   *+B13[B11],A1     ; |752| 

           ADD     .L2X    A3,B11,B18        ; |752| 
||         MVK     .S1     1372,A3           ; |752| 

           MVK     .S2     3436,B4           ; |752| 

           ADD     .L2X    A3,B11,B16        ; |752| 
||         MVK     .S1     3780,A3           ; |752| 

           ADD     .L2     B4,B11,B6         ; |752| 
||         MVK     .S1     2748,A4           ; |752| 

           LDB     .D2T2   *B6,B2            ; |752| 
||         ZERO    .L1     A3                ; |753| 
||         ADD     .L2X    A3,B11,B7         ; |752| 
||         MVK     .S2     334,B9            ; |753| 

   [ A1]   STB     .D2T1   A3,*+B9[B11]      ; |753| 
||         MVK     .S1     1716,A4           ; |752| 
||         ADD     .L2X    A4,B11,B19        ; |752| 

           LDB     .D2T1   *B5,A1            ; |752| 
           ADD     .L2X    A4,B11,B17        ; |752| 
           LDB     .D2T1   *B17,A0           ; |752| 
           ZERO    .L2     B9                ; |753| 

           ZERO    .L2     B6                ; |753| 
|| [ B2]   STB     .D2T2   B9,*-B6(6)        ; |753| 
||         MVK     .S2     4124,B8           ; |752| 

   [ A1]   STB     .D2T2   B6,*-B5(6)        ; |753| 
||         ADD     .L2     B8,B11,B8         ; |752| 

           LDB     .D2T2   *B8,B0            ; |752| 
||         ZERO    .L2     B5                ; |753| 

   [ A0]   STB     .D2T2   B5,*-B17(6)       ; |753| 
           LDB     .D2T1   *B18,A2           ; |752| 
           LDB     .D2T1   *B16,A0           ; |752| 
           LDB     .D2T2   *B7,B1            ; |752| 
           LDW     .D2T2   *+SP(140),B3      ; |807| 
           LDB     .D2T1   *B19,A1           ; |752| 
           LDW     .D2T1   *+SP(160),A15     ; |807| 
   [ A0]   STB     .D2T2   B5,*-B16(6)       ; |753| 
           LDW     .D2T1   *+SP(136),A14     ; |807| 
           LDDW    .D2T1   *+SP(120),A11:A10 ; |807| 

           LDDW    .D2T2   *+SP(152),B13:B12 ; |807| 
||         MVK     .S2     684,B4            ; |752| 

           LDDW    .D2T1   *+SP(128),A13:A12 ; |807| 
||         MVK     .S1     2060,A3           ; |752| 
||         ADD     .L2     B4,B11,B4         ; |752| 

           LDB     .D2T1   *B4,A0            ; |752| 
||         MVK     .S1     1028,A3           ; |752| 
||         ADD     .L1X    A3,B11,A4         ; |752| 

           LDB     .D1T1   *A4,A1            ; |752| 
|| [ A1]   STB     .D2T2   B5,*-B19(6)       ; |753| 
||         ADD     .L1X    A3,B11,A3         ; |752| 

           RET     .S2     B3                ; |807| 
||         LDB     .D1T1   *A3,A2            ; |752| 
|| [ A2]   STB     .D2T2   B5,*-B18(6)       ; |753| 

           LDDW    .D2T2   *+SP(144),B11:B10 ; |807| 
           ZERO    .L1     A5                ; |753| 

   [ A0]   STB     .D2T2   B5,*-B4(6)        ; |753| 
||         ZERO    .L2     B4                ; |753| 

   [ B0]   STB     .D2T2   B4,*-B8(6)        ; |753| 
||         ZERO    .L1     A4                ; |753| 
|| [ A1]   STB     .D1T1   A5,*-A4(6)        ; |753| 

   [ B1]   STB     .D2T2   B4,*-B7(6)        ; |753| 
|| [ A2]   STB     .D1T1   A4,*-A3(6)        ; |753| 
||         ADDK    .S2     160,SP            ; |807| 

           ; BRANCH OCCURS {B3}              ; |807| 
;** --------------------------------------------------------------------------*
L33:    
           STDW    .D1T1   A5:A4,*A3         ; |40| 
           LDDW    .D2T1   *+B9(8),A5:A4     ; |40| 
           LDW     .D2T1   *+SP(84),A3
           NOP             4
           STDW    .D1T1   A5:A4,*A3         ; |40| 
           LDDW    .D2T1   *+B9(16),A5:A4    ; |40| 
           LDW     .D2T1   *+SP(80),A3
           NOP             4
           STDW    .D1T1   A5:A4,*A3         ; |40| 
           LDDW    .D2T2   *+B9(24),B5:B4    ; |40| 
           LDW     .D2T1   *+SP(76),A3
           NOP             3
           LDW     .D2T1   *+SP(112),A4      ; |734| 

           LDW     .D2T1   *+SP(72),A3
||         STDW    .D1T2   B5:B4,*A3         ; |40| 

           LDDW    .D2T2   *+B9(32),B5:B4    ; |40| 
           NOP             4

           LDW     .D2T1   *+SP(68),A3
||         STDW    .D1T2   B5:B4,*A3         ; |40| 

           LDDW    .D2T2   *+B9(40),B5:B4    ; |40| 
           NOP             4

           LDW     .D2T1   *+SP(56),A3
||         STDW    .D1T2   B5:B4,*A3         ; |40| 

           LDDW    .D2T2   *+B9(48),B5:B4    ; |40| 
           NOP             4

           LDW     .D2T1   *+SP(48),A3       ; |734| 
||         STDW    .D1T2   B5:B4,*A3         ; |40| 

           LDDW    .D2T2   *+B9(56),B9:B8    ; |40| 
||         CALL    .S2     B7                ; |734| 

           LDW     .D2T2   *+SP(96),B4
           ADDKPC  .S2     RL263,B3,2        ; |734| 
           STDW    .D1T2   B9:B8,*A3         ; |40| 
RL263:     ; CALL OCCURS {_CopyMatrixEx}     ; |734| 
;** --------------------------------------------------------------------------*

           MV      .L1X    B13,A3
||         MVK     .S1     19537,A4          ; |735| 

           STB     .D1T1   A3,*+A13[A4]      ; |735| 
           LDW     .D2T2   *+SP(140),B3      ; |807| 
           LDW     .D2T1   *+SP(160),A15     ; |807| 
           LDDW    .D2T1   *+SP(120),A11:A10 ; |807| 
           LDW     .D2T1   *+SP(136),A14     ; |807| 
           LDDW    .D2T2   *+SP(144),B11:B10 ; |807| 

           RET     .S2     B3                ; |807| 
||         LDDW    .D2T1   *+SP(128),A13:A12 ; |807| 

           LDDW    .D2T2   *+SP(152),B13:B12 ; |807| 
           NOP             3
	.dwpsn	"OPManager.c",807,1
           ADDK    .S2     160,SP            ; |807| 
           ; BRANCH OCCURS {B3}              ; |807| 

DW$128	.dwtag  DW_TAG_loop
	.dwattr DW$128, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L31:1:1472539744")
	.dwattr DW$128, DW_AT_begin_file("OPManager.c")
	.dwattr DW$128, DW_AT_begin_line(0x313)
	.dwattr DW$128, DW_AT_end_line(0x316)
DW$129	.dwtag  DW_TAG_loop_range
	.dwattr DW$129, DW_AT_low_pc(DW$L$_OP_Estimation$27$B)
	.dwattr DW$129, DW_AT_high_pc(DW$L$_OP_Estimation$27$E)
DW$130	.dwtag  DW_TAG_loop_range
	.dwattr DW$130, DW_AT_low_pc(DW$L$_OP_Estimation$28$B)
	.dwattr DW$130, DW_AT_high_pc(DW$L$_OP_Estimation$28$E)
	.dwendtag DW$128


DW$131	.dwtag  DW_TAG_loop
	.dwattr DW$131, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L29:1:1472539744")
	.dwattr DW$131, DW_AT_begin_file("OPManager.c")
	.dwattr DW$131, DW_AT_begin_line(0x30a)
	.dwattr DW$131, DW_AT_end_line(0x30e)
DW$132	.dwtag  DW_TAG_loop_range
	.dwattr DW$132, DW_AT_low_pc(DW$L$_OP_Estimation$21$B)
	.dwattr DW$132, DW_AT_high_pc(DW$L$_OP_Estimation$21$E)
DW$133	.dwtag  DW_TAG_loop_range
	.dwattr DW$133, DW_AT_low_pc(DW$L$_OP_Estimation$25$B)
	.dwattr DW$133, DW_AT_high_pc(DW$L$_OP_Estimation$25$E)

DW$134	.dwtag  DW_TAG_loop
	.dwattr DW$134, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L30:2:1472539744")
	.dwattr DW$134, DW_AT_begin_file("OPManager.c")
	.dwattr DW$134, DW_AT_begin_line(0x30b)
	.dwattr DW$134, DW_AT_end_line(0x30e)
DW$135	.dwtag  DW_TAG_loop_range
	.dwattr DW$135, DW_AT_low_pc(DW$L$_OP_Estimation$22$B)
	.dwattr DW$135, DW_AT_high_pc(DW$L$_OP_Estimation$22$E)
DW$136	.dwtag  DW_TAG_loop_range
	.dwattr DW$136, DW_AT_low_pc(DW$L$_OP_Estimation$23$B)
	.dwattr DW$136, DW_AT_high_pc(DW$L$_OP_Estimation$23$E)
DW$137	.dwtag  DW_TAG_loop_range
	.dwattr DW$137, DW_AT_low_pc(DW$L$_OP_Estimation$24$B)
	.dwattr DW$137, DW_AT_high_pc(DW$L$_OP_Estimation$24$E)
	.dwendtag DW$134

	.dwendtag DW$131


DW$138	.dwtag  DW_TAG_loop
	.dwattr DW$138, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L27:1:1472539744")
	.dwattr DW$138, DW_AT_begin_file("OPManager.c")
	.dwattr DW$138, DW_AT_begin_line(0x2c2)
	.dwattr DW$138, DW_AT_end_line(0x2c6)
DW$139	.dwtag  DW_TAG_loop_range
	.dwattr DW$139, DW_AT_low_pc(DW$L$_OP_Estimation$10$B)
	.dwattr DW$139, DW_AT_high_pc(DW$L$_OP_Estimation$10$E)
DW$140	.dwtag  DW_TAG_loop_range
	.dwattr DW$140, DW_AT_low_pc(DW$L$_OP_Estimation$11$B)
	.dwattr DW$140, DW_AT_high_pc(DW$L$_OP_Estimation$11$E)
DW$141	.dwtag  DW_TAG_loop_range
	.dwattr DW$141, DW_AT_low_pc(DW$L$_OP_Estimation$12$B)
	.dwattr DW$141, DW_AT_high_pc(DW$L$_OP_Estimation$12$E)
	.dwendtag DW$138

	.dwattr DW$111, DW_AT_end_file("OPManager.c")
	.dwattr DW$111, DW_AT_end_line(0x327)
	.dwattr DW$111, DW_AT_end_column(0x01)
	.dwendtag DW$111

	.sect	".text"

DW$142	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_CovMatrPropagation"), DW_AT_symbol_name("_OP_CovMatrPropagation")
	.dwattr DW$142, DW_AT_low_pc(_OP_CovMatrPropagation)
	.dwattr DW$142, DW_AT_high_pc(0x00)
	.dwattr DW$142, DW_AT_begin_file("OPManager.c")
	.dwattr DW$142, DW_AT_begin_line(0x334)
	.dwattr DW$142, DW_AT_begin_column(0x0d)
	.dwattr DW$142, DW_AT_frame_base[DW_OP_breg31 4728]
	.dwattr DW$142, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",826,1

;******************************************************************************
;* FUNCTION NAME: _OP_CovMatrPropagation                                      *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 4684 Auto + 44 Save = 4728 byte             *
;******************************************************************************
_OP_CovMatrPropagation:
;** --------------------------------------------------------------------------*
DW$143	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$143, DW_AT_type(*DW$T$189)
	.dwattr DW$143, DW_AT_location[DW_OP_reg4]
DW$144	.dwtag  DW_TAG_formal_parameter, DW_AT_name("r"), DW_AT_symbol_name("_r")
	.dwattr DW$144, DW_AT_type(*DW$T$57)
	.dwattr DW$144, DW_AT_location[DW_OP_reg20]
DW$145	.dwtag  DW_TAG_formal_parameter, DW_AT_name("v"), DW_AT_symbol_name("_v")
	.dwattr DW$145, DW_AT_type(*DW$T$57)
	.dwattr DW$145, DW_AT_location[DW_OP_reg6]
DW$146	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dt"), DW_AT_symbol_name("_dt")
	.dwattr DW$146, DW_AT_type(*DW$T$20)
	.dwattr DW$146, DW_AT_location[DW_OP_reg22]
DW$147	.dwtag  DW_TAG_formal_parameter, DW_AT_name("P0"), DW_AT_symbol_name("_P0")
	.dwattr DW$147, DW_AT_type(*DW$T$171)
	.dwattr DW$147, DW_AT_location[DW_OP_reg8]
DW$148	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Pp"), DW_AT_symbol_name("_Pp")
	.dwattr DW$148, DW_AT_type(*DW$T$171)
	.dwattr DW$148, DW_AT_location[DW_OP_reg24]

           ADDK    .S2     -4728,SP          ; |826| 
||         MV      .L1X    SP,A31            ; |826| 

           STW     .D2T2   B4,*+SP(4624)     ; |826| 

           STW     .D2T1   A15,*+SP(4728)
||         MVKL    .S2     _CreateMatrixEx,B5 ; |840| 

           STW     .D2T2   B11,*+SP(4716)
||         MVKH    .S2     _CreateMatrixEx,B5 ; |840| 

           CALL    .S2     B5                ; |840| 
||         STW     .D2T2   B3,*+SP(4708)

           STW     .D2T2   B10,*+SP(4712)
           STW     .D2T1   A8,*+SP(4628)     ; |826| 

           STW     .D2T2   B13,*+SP(4724)
||         STW     .D1T1   A14,*-A31(24)

           MV      .L1X    B8,A4             ; |826| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         MVK     .L2     0x8,B4            ; |840| 
||         MV      .S1     A4,A15            ; |826| 
||         STW     .D2T2   B12,*+SP(4720)

           ADDKPC  .S2     RL264,B3,0        ; |840| 
||         MV      .L1X    B8,A14            ; |826| 
||         STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .L2     B7,B11            ; |826| 
||         MV      .D2     B6,B10            ; |826| 
||         MVK     .S1     0x8,A6            ; |840| 

RL264:     ; CALL OCCURS {_CreateMatrixEx}   ; |840| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4624),B6     ; |840| 
           NOP             2
           MVKL    .S1     __mpyd,A3         ; |842| 
           MVKH    .S1     __mpyd,A3         ; |842| 
           LDDW    .D2T2   *B6,B5:B4         ; |842| 
           LDDW    .D2T2   *+B6(16),B13:B12  ; |842| 
           LDDW    .D2T2   *+B6(8),B7:B6     ; |842| 
           CALL    .S2X    A3                ; |842| 
           ADDKPC  .S2     RL271,B3,0        ; |842| 
           MV      .L1X    B5,A5             ; |842| 
           MV      .L1X    B4,A4             ; |842| 
           MV      .L1X    B6,A11            ; |842| 
           MV      .L1X    B7,A10            ; |842| 
RL271:     ; CALL OCCURS {__mpyd}            ; |842| 
           MVKL    .S2     __mpyd,B6         ; |842| 
           MVKH    .S2     __mpyd,B6         ; |842| 
           CALL    .S2     B6                ; |842| 
           MV      .L1     A4,A13            ; |842| 
           MV      .L2X    A11,B4            ; |842| 
           MV      .D1     A5,A12            ; |842| 
           MV      .L2X    A10,B5            ; |842| 

           MV      .S1     A11,A4            ; |842| 
||         ADDKPC  .S2     RL272,B3,0        ; |842| 
||         MV      .L1     A10,A5            ; |842| 

RL272:     ; CALL OCCURS {__mpyd}            ; |842| 
           MVKL    .S1     __addd,A3         ; |842| 
           MVKH    .S1     __addd,A3         ; |842| 
           MV      .L2X    A4,B4             ; |842| 
           CALL    .S2X    A3                ; |842| 
           MV      .L2X    A5,B5             ; |842| 
           MV      .S1     A13,A4            ; |842| 
           ADDKPC  .S2     RL273,B3,1        ; |842| 
           MV      .L1     A12,A5            ; |842| 
RL273:     ; CALL OCCURS {__addd}            ; |842| 
           MVKL    .S2     __mpyd,B6         ; |842| 
           MVKH    .S2     __mpyd,B6         ; |842| 
           CALL    .S2     B6                ; |842| 
           MV      .S1     A4,A11            ; |842| 
           MV      .L1X    B12,A4            ; |842| 
           ADDKPC  .S2     RL274,B3,0        ; |842| 
           MV      .L2     B13,B5            ; |842| 

           MV      .D2     B12,B4            ; |842| 
||         MV      .S1     A5,A10            ; |842| 
||         MV      .L1X    B13,A5            ; |842| 

RL274:     ; CALL OCCURS {__mpyd}            ; |842| 
           MVKL    .S1     __addd,A3         ; |842| 
           MVKH    .S1     __addd,A3         ; |842| 
           MV      .L2X    A4,B4             ; |842| 
           CALL    .S2X    A3                ; |842| 
           ADDKPC  .S2     RL275,B3,1        ; |842| 
           MV      .S1     A11,A4            ; |842| 
           MV      .L2X    A5,B5             ; |842| 
           MV      .L1     A10,A5            ; |842| 
RL275:     ; CALL OCCURS {__addd}            ; |842| 
           MVKL    .S1     __mpyd,A3         ; |843| 
           MVKH    .S1     __mpyd,A3         ; |843| 
           STW     .D2T1   A4,*+SP(4636)     ; |842| 
           CALL    .S2X    A3                ; |843| 
           STW     .D2T1   A5,*+SP(4632)     ; |842| 
           MV      .L2X    A5,B5             ; |842| 
           ADDKPC  .S2     RL283,B3,1        ; |843| 
           MV      .L2X    A4,B4             ; |842| 
RL283:     ; CALL OCCURS {__mpyd}            ; |843| 
           MVKL    .S2     _sqrt,B4          ; |843| 
           MVKH    .S2     _sqrt,B4          ; |843| 

           LDW     .D2T1   *+SP(4632),A5     ; |843| 
||         MV      .L1     A5,A10            ; |843| 
||         CALL    .S2     B4                ; |843| 

           LDW     .D2T1   *+SP(4636),A4     ; |843| 
||         MV      .L1     A4,A11            ; |843| 

           ADDKPC  .S2     RL284,B3,3        ; |843| 
RL284:     ; CALL OCCURS {_sqrt}             ; |843| 
           MVKL    .S1     __mpyd,A3         ; |843| 
           MVKH    .S1     __mpyd,A3         ; |843| 
           MV      .L2X    A4,B4             ; |843| 
           CALL    .S2X    A3                ; |843| 
           MV      .L2X    A5,B5             ; |843| 
           MV      .S1     A11,A4            ; |843| 
           ADDKPC  .S2     RL285,B3,1        ; |843| 
           MV      .L1     A10,A5            ; |843| 
RL285:     ; CALL OCCURS {__mpyd}            ; |843| 
           MVKL    .S1     __divd,A3         ; |843| 
           MVKH    .S1     __divd,A3         ; |843| 
           MV      .L2X    A5,B5             ; |843| 
           CALL    .S2X    A3                ; |843| 
           ZERO    .L1     A5                ; |843| 
           ADDKPC  .S2     RL286,B3,0        ; |843| 
           MVKH    .S1     0x3ff00000,A5     ; |843| 
           MV      .L2X    A4,B4             ; |843| 
           ZERO    .L1     A4                ; |843| 
RL286:     ; CALL OCCURS {__divd}            ; |843| 
           MVKL    .S2     _CreateMatrixEx,B5 ; |845| 
           MVKH    .S2     _CreateMatrixEx,B5 ; |845| 
           CALL    .S2     B5                ; |845| 
           ADDKPC  .S2     RL287,B3,0        ; |845| 
           MVK     .L1     0x8,A6            ; |845| 
           STW     .D2T1   A5,*+SP(4644)     ; |843| 
           STW     .D2T1   A4,*+SP(4640)     ; |843| 

           ADD     .S1X    8,SP,A4           ; |845| 
||         MVK     .L2     0x8,B4            ; |845| 

RL287:     ; CALL OCCURS {_CreateMatrixEx}   ; |845| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |848| 
           MVKH    .S1     __mpyd,A3         ; |848| 
           MVKL    .S2     0x3f231da7,B5     ; |848| 
           CALL    .S2X    A3                ; |848| 
           MVKL    .S2     0xd7cb8d5b,B4     ; |848| 
           MVKH    .S2     0x3f231da7,B5     ; |848| 
           MVKH    .S2     0xd7cb8d5b,B4     ; |848| 
           MV      .L1X    B10,A4            ; |848| 

           ADDKPC  .S2     RL288,B3,0        ; |848| 
||         MV      .L1X    B11,A5            ; |848| 

RL288:     ; CALL OCCURS {__mpyd}            ; |848| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4624),B4     ; |848| 
           MVKL    .S1     __mpyd,A3         ; |848| 
           MVKH    .S1     __mpyd,A3         ; |848| 
           ZERO    .L2     B5                ; |848| 
           STW     .D2T1   A4,*+SP(4648)     ; |848| 
           LDDW    .D2T2   *B4,B13:B12       ; |848| 
           CALL    .S2X    A3                ; |848| 
           MVKH    .S2     0x40080000,B5     ; |848| 
           STW     .D2T1   A5,*+SP(4652)     ; |848| 
           MV      .S2X    A4,B6             ; |848| 

           MV      .L1X    B12,A4            ; |848| 
||         MV      .L2X    A5,B7             ; |848| 

           ADDKPC  .S2     RL289,B3,0        ; |848| 
||         MV      .L1X    B13,A5            ; |848| 
||         ZERO    .L2     B4                ; |848| 
||         STDW    .D2T2   B7:B6,*+SP(16)    ; |848| 

RL289:     ; CALL OCCURS {__mpyd}            ; |848| 
           MVKL    .S1     __negd,A3         ; |848| 
           MVKH    .S1     __negd,A3         ; |848| 
           STW     .D2T1   A4,*+SP(4660)     ; |848| 
           CALL    .S2X    A3                ; |848| 
           ADDKPC  .S2     RL290,B3,1        ; |848| 
           STW     .D2T1   A5,*+SP(4656)     ; |848| 
           MV      .L1X    B11,A5            ; |848| 
           MV      .L1X    B10,A4            ; |848| 
RL290:     ; CALL OCCURS {__negd}            ; |848| 
           MVKL    .S1     __mpyd,A3         ; |848| 
           MVKH    .S1     __mpyd,A3         ; |848| 
           MVKL    .S2     0x42f6a866,B5     ; |848| 
           CALL    .S2X    A3                ; |848| 
           MVKL    .S2     0x5bda5400,B4     ; |848| 
           MVKH    .S2     0x42f6a866,B5     ; |848| 
           STW     .D2T1   A4,*+SP(4668)     ; |848| 
           MVKH    .S2     0x5bda5400,B4     ; |848| 

           STW     .D2T1   A5,*+SP(4664)     ; |848| 
||         ADDKPC  .S2     RL291,B3,0        ; |848| 

RL291:     ; CALL OCCURS {__mpyd}            ; |848| 
           MVKL    .S1     __mpyd,A3         ; |848| 

           STW     .D2T1   A5,*+SP(4672)     ; |848| 
||         MVKH    .S1     __mpyd,A3         ; |848| 

           STW     .D2T1   A4,*+SP(4676)     ; |848| 

           LDW     .D2T1   *+SP(4656),A5     ; |848| 
||         CALL    .S2X    A3                ; |848| 

           LDW     .D2T1   *+SP(4660),A4     ; |848| 
           MV      .D2     B12,B4            ; |848| 
           MV      .L2     B13,B5            ; |848| 
           ADDKPC  .S2     RL302,B3,1        ; |848| 
RL302:     ; CALL OCCURS {__mpyd}            ; |848| 
           MVKL    .S1     __subd,A3         ; |848| 
           MVKH    .S1     __subd,A3         ; |848| 
           MV      .L2X    A5,B5             ; |848| 

           LDW     .D2T1   *+SP(4632),A5     ; |848| 
||         CALL    .S2X    A3                ; |848| 

           LDW     .D2T1   *+SP(4636),A4     ; |848| 
||         MV      .L2X    A4,B4             ; |848| 

           ADDKPC  .S2     RL303,B3,3        ; |848| 
RL303:     ; CALL OCCURS {__subd}            ; |848| 
           MVKL    .S1     __mpyd,A3         ; |848| 
           MVKH    .S1     __mpyd,A3         ; |848| 
           LDW     .D2T2   *+SP(4672),B5     ; |848| 
           CALL    .S2X    A3                ; |848| 
           LDW     .D2T2   *+SP(4676),B4     ; |848| 
           ADDKPC  .S2     RL304,B3,3        ; |848| 
RL304:     ; CALL OCCURS {__mpyd}            ; |848| 
           MVKL    .S1     __mpyd,A3         ; |848| 
           MVKH    .S1     __mpyd,A3         ; |848| 
           LDW     .D2T2   *+SP(4644),B5     ; |848| 
           CALL    .S2X    A3                ; |848| 
           LDW     .D2T2   *+SP(4640),B4     ; |848| 
           ADDKPC  .S2     RL305,B3,3        ; |848| 
RL305:     ; CALL OCCURS {__mpyd}            ; |848| 
           MVKL    .S2     __addd,B6         ; |848| 
           MVKH    .S2     __addd,B6         ; |848| 
           CALL    .S2     B6                ; |848| 
           MVKL    .S2     0x3e36d6a5,B5     ; |848| 
           MVKL    .S2     0x97d265b1,B4     ; |848| 
           MVKH    .S2     0x3e36d6a5,B5     ; |848| 
           MVKH    .S2     0x97d265b1,B4     ; |848| 
           ADDKPC  .S2     RL306,B3,0        ; |848| 
RL306:     ; CALL OCCURS {__addd}            ; |848| 
           LDW     .D2T2   *+SP(4624),B6     ; |848| 
           MVKL    .S1     __mpyd,A3         ; |850| 
           MVKH    .S1     __mpyd,A3         ; |850| 
           MVKL    .S2     0x5bda5400,B4     ; |850| 
           CALL    .S2X    A3                ; |850| 
           LDDW    .D2T1   *+B6(8),A11:A10   ; |850| 
           MVKL    .S2     0x42f6a866,B5     ; |850| 
           MVKH    .S2     0x5bda5400,B4     ; |850| 

           MVKH    .S2     0x42f6a866,B5     ; |850| 
||         MV      .L1X    B10,A4            ; |850| 
||         MV      .D1     A4,A6             ; |848| 
||         MV      .S1     A5,A7             ; |848| 

           MV      .L1X    B11,A5            ; |850| 
||         STDW    .D2T1   A7:A6,*+SP(32)    ; |848| 
||         ADDKPC  .S2     RL307,B3,0        ; |850| 

RL307:     ; CALL OCCURS {__mpyd}            ; |850| 
           MVKL    .S1     __mpyd,A3         ; |850| 

           STW     .D2T1   A5,*+SP(4680)     ; |850| 
||         MVKH    .S1     __mpyd,A3         ; |850| 

           STW     .D2T1   A4,*+SP(4684)     ; |850| 

           LDW     .D2T1   *+SP(4656),A5     ; |850| 
||         CALL    .S2X    A3                ; |850| 

           LDW     .D2T1   *+SP(4660),A4     ; |850| 
           MV      .L2X    A10,B4            ; |850| 
           MV      .L2X    A11,B5            ; |850| 
           ADDKPC  .S2     RL311,B3,1        ; |850| 
RL311:     ; CALL OCCURS {__mpyd}            ; |850| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |850| 
           MVKH    .S1     __mpyd,A3         ; |850| 
           LDW     .D2T2   *+SP(4680),B5     ; |850| 
           CALL    .S2X    A3                ; |850| 
           LDW     .D2T2   *+SP(4684),B4     ; |850| 
           ADDKPC  .S2     RL312,B3,3        ; |850| 
RL312:     ; CALL OCCURS {__mpyd}            ; |850| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |850| 
           MVKH    .S1     __mpyd,A3         ; |850| 
           LDW     .D2T2   *+SP(4644),B5     ; |850| 
           CALL    .S2X    A3                ; |850| 
           LDW     .D2T2   *+SP(4640),B4     ; |850| 
           ADDKPC  .S2     RL313,B3,3        ; |850| 
RL313:     ; CALL OCCURS {__mpyd}            ; |850| 
           LDW     .D2T2   *+SP(4624),B4     ; |850| 
           MVKL    .S2     __mpyd,B6         ; |852| 
           MVKH    .S2     __mpyd,B6         ; |852| 
           MV      .L1     A5,A13            ; |850| 
           LDW     .D2T1   *+SP(4656),A5     ; |850| 

           CALL    .S2     B6                ; |852| 
||         LDDW    .D2T2   *+B4(16),B13:B12  ; |852| 

           LDW     .D2T1   *+SP(4660),A4     ; |852| 
||         MV      .L1     A4,A12            ; |850| 

           STDW    .D2T1   A13:A12,*+SP(40)  ; |850| 
           ADDKPC  .S2     RL317,B3,1        ; |852| 

           MV      .D2     B13,B5            ; |852| 
||         MV      .L2     B12,B4            ; |852| 

RL317:     ; CALL OCCURS {__mpyd}            ; |852| 
           MVKL    .S1     __mpyd,A3         ; |852| 
           MVKH    .S1     __mpyd,A3         ; |852| 
           LDW     .D2T2   *+SP(4680),B5     ; |852| 
           CALL    .S2X    A3                ; |852| 
           LDW     .D2T2   *+SP(4684),B4     ; |852| 
           ADDKPC  .S2     RL318,B3,3        ; |852| 
RL318:     ; CALL OCCURS {__mpyd}            ; |852| 
           MVKL    .S1     __mpyd,A3         ; |852| 
           MVKH    .S1     __mpyd,A3         ; |852| 
           LDW     .D2T2   *+SP(4644),B5     ; |852| 
           CALL    .S2X    A3                ; |852| 
           LDW     .D2T2   *+SP(4640),B4     ; |852| 
           ADDKPC  .S2     RL319,B3,3        ; |852| 
RL319:     ; CALL OCCURS {__mpyd}            ; |852| 
           STW     .D2T1   A5,*+SP(4656)     ; |852| 
           STW     .D2T1   A4,*+SP(4660)     ; |852| 
           LDW     .D2T1   *+SP(4656),A7     ; |852| 

           LDW     .D2T2   *+SP(4652),B5     ; |852| 
||         MVKL    .S1     __mpyd,A3         ; |852| 

           LDW     .D2T2   *+SP(4648),B4     ; |852| 
||         MVKH    .S1     __mpyd,A3         ; |852| 

           LDW     .D2T1   *+SP(4664),A5     ; |852| 

           CALL    .S2X    A3                ; |852| 
||         LDW     .D2T1   *+SP(4660),A6     ; |852| 

           LDW     .D2T1   *+SP(4668),A4     ; |852| 
           ADDKPC  .S2     RL320,B3,2        ; |852| 
           STDW    .D2T1   A7:A6,*+SP(48)    ; |852| 
RL320:     ; CALL OCCURS {__mpyd}            ; |852| 
           MVKL    .S2     __mpyd,B6         ; |853| 
           MVKH    .S2     __mpyd,B6         ; |853| 
           CALL    .S2     B6                ; |853| 
           ADDKPC  .S2     RL321,B3,0        ; |853| 
           STDW    .D2T1   A5:A4,*+SP(200)   ; |852| 
           MV      .L1     A13,A5            ; |853| 
           MV      .L2     B10,B4            ; |853| 

           MV      .S1     A12,A4            ; |853| 
||         MV      .L2     B11,B5            ; |853| 

RL321:     ; CALL OCCURS {__mpyd}            ; |853| 
           MVKL    .S1     __mpyd,A3         ; |854| 
           MVKH    .S1     __mpyd,A3         ; |854| 
           ZERO    .L2     B5                ; |854| 
           CALL    .S2X    A3                ; |854| 
           MVKH    .S2     0x40080000,B5     ; |854| 
           MV      .L1     A5,A7             ; |853| 
           MV      .S1     A4,A6             ; |853| 
           ADDKPC  .S2     RL322,B3,0        ; |854| 

           STDW    .D2T1   A7:A6,*+SP(224)   ; |853| 
||         MV      .L1     A11,A5            ; |854| 
||         MV      .S1     A10,A4            ; |854| 
||         ZERO    .L2     B4                ; |854| 

RL322:     ; CALL OCCURS {__mpyd}            ; |854| 
           MVKL    .S1     __mpyd,A3         ; |854| 
           MVKH    .S1     __mpyd,A3         ; |854| 
           MV      .L2X    A11,B5            ; |854| 
           CALL    .S2X    A3                ; |854| 
           ADDKPC  .S2     RL333,B3,1        ; |854| 
           MV      .S1     A4,A11            ; |854| 
           MV      .L2X    A10,B4            ; |854| 
           MV      .L1     A5,A10            ; |854| 
RL333:     ; CALL OCCURS {__mpyd}            ; |854| 
           MVKL    .S1     __subd,A3         ; |854| 
           MVKH    .S1     __subd,A3         ; |854| 
           MV      .L2X    A5,B5             ; |854| 

           LDW     .D2T1   *+SP(4632),A5     ; |854| 
||         CALL    .S2X    A3                ; |854| 

           LDW     .D2T1   *+SP(4636),A4     ; |854| 
||         MV      .L2X    A4,B4             ; |854| 

           ADDKPC  .S2     RL334,B3,3        ; |854| 
RL334:     ; CALL OCCURS {__subd}            ; |854| 
           MVKL    .S1     __mpyd,A3         ; |854| 
           MVKH    .S1     __mpyd,A3         ; |854| 
           LDW     .D2T2   *+SP(4672),B5     ; |854| 
           CALL    .S2X    A3                ; |854| 
           LDW     .D2T2   *+SP(4676),B4     ; |854| 
           ADDKPC  .S2     RL335,B3,3        ; |854| 
RL335:     ; CALL OCCURS {__mpyd}            ; |854| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |854| 
           MVKH    .S1     __mpyd,A3         ; |854| 
           LDW     .D2T2   *+SP(4644),B5     ; |854| 
           CALL    .S2X    A3                ; |854| 
           LDW     .D2T2   *+SP(4640),B4     ; |854| 
           ADDKPC  .S2     RL336,B3,3        ; |854| 
RL336:     ; CALL OCCURS {__mpyd}            ; |854| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |854| 
           MVKH    .S1     __addd,A3         ; |854| 
           MVKL    .S2     0x3e36d6a5,B5     ; |854| 
           CALL    .S2X    A3                ; |854| 
           MVKL    .S2     0x97d265b1,B4     ; |854| 
           MVKH    .S2     0x3e36d6a5,B5     ; |854| 
           MVKH    .S2     0x97d265b1,B4     ; |854| 
           ADDKPC  .S2     RL337,B3,1        ; |854| 
RL337:     ; CALL OCCURS {__addd}            ; |854| 
           MVKL    .S2     __mpyd,B8         ; |857| 
           MVKH    .S2     __mpyd,B8         ; |857| 
           CALL    .S2     B8                ; |857| 
           MV      .L2     B12,B4            ; |857| 
           MV      .S2X    A5,B7             ; |854| 
           MV      .L2X    A4,B6             ; |854| 
           ADDKPC  .S2     RL341,B3,0        ; |857| 

           STDW    .D2T2   B7:B6,*+SP(232)   ; |854| 
||         MV      .L1     A10,A5            ; |857| 
||         MV      .S1     A11,A4            ; |857| 
||         MV      .L2     B13,B5            ; |857| 

RL341:     ; CALL OCCURS {__mpyd}            ; |857| 
           MVKL    .S1     __mpyd,A3         ; |857| 
           MVKH    .S1     __mpyd,A3         ; |857| 
           LDW     .D2T2   *+SP(4680),B5     ; |857| 
           CALL    .S2X    A3                ; |857| 
           LDW     .D2T2   *+SP(4684),B4     ; |857| 
           ADDKPC  .S2     RL342,B3,3        ; |857| 
RL342:     ; CALL OCCURS {__mpyd}            ; |857| 
           MVKL    .S1     __mpyd,A3         ; |857| 
           MVKH    .S1     __mpyd,A3         ; |857| 
           LDW     .D2T2   *+SP(4644),B5     ; |857| 
           CALL    .S2X    A3                ; |857| 
           LDW     .D2T2   *+SP(4640),B4     ; |857| 
           ADDKPC  .S2     RL343,B3,3        ; |857| 
RL343:     ; CALL OCCURS {__mpyd}            ; |857| 
           MVKL    .S1     __mpyd,A3         ; |857| 
           MVKH    .S1     __mpyd,A3         ; |857| 
           MV      .L1     A5,A11            ; |857| 

           CALL    .S2X    A3                ; |857| 
||         LDW     .D2T1   *+SP(4656),A5     ; |857| 

           LDW     .D2T1   *+SP(4660),A4     ; |857| 
||         MV      .L1     A4,A10            ; |857| 

           ADDAD   .D2     SP,30,B4          ; |857| 
           ADDKPC  .S2     RL344,B3,0        ; |857| 
           MV      .L1X    B4,A12            ; |857| Register A/B partition copy

           STDW    .D1T1   A11:A10,*A12      ; |857| 
||         MV      .L2     B10,B4            ; |857| 
||         MV      .D2     B11,B5            ; |857| 

RL344:     ; CALL OCCURS {__mpyd}            ; |857| 
           MVKL    .S1     __mpyd,A3         ; |858| 
           MVKH    .S1     __mpyd,A3         ; |858| 
           MV      .L1     A4,A6             ; |857| 
           CALL    .S2X    A3                ; |858| 
           ADDKPC  .S2     RL345,B3,0        ; |858| 
           MV      .S1     A10,A4            ; |858| 
           MV      .L2     B11,B5            ; |858| 
           MV      .S1     A5,A7             ; |857| 

           MV      .L1     A11,A5            ; |858| 
||         STDW    .D1T1   A7:A6,*+A12(176)  ; |857| 
||         MV      .D2     B10,B4            ; |858| 

RL345:     ; CALL OCCURS {__mpyd}            ; |858| 
           MVKL    .S1     __mpyd,A3         ; |859| 
           MVKH    .S1     __mpyd,A3         ; |859| 
           ZERO    .L2     B5                ; |859| 
           CALL    .S2X    A3                ; |859| 
           MVKH    .S2     0x40080000,B5     ; |859| 
           MV      .S1     A5,A7             ; |858| 
           MV      .D1     A4,A6             ; |858| 
           MV      .L1X    B12,A4            ; |859| 

           ADDKPC  .S2     RL356,B3,0        ; |859| 
||         STDW    .D1T1   A7:A6,*+A12(184)  ; |858| 
||         MV      .L1X    B13,A5            ; |859| 
||         ZERO    .L2     B4                ; |859| 

RL356:     ; CALL OCCURS {__mpyd}            ; |859| 
           MVKL    .S1     __mpyd,A3         ; |859| 
           MVKH    .S1     __mpyd,A3         ; |859| 
           MV      .L2     B13,B5            ; |859| 
           CALL    .S2X    A3                ; |859| 
           ADDKPC  .S2     RL357,B3,3        ; |859| 
           MV      .D2     B12,B4            ; |859| 
RL357:     ; CALL OCCURS {__mpyd}            ; |859| 
           MVKL    .S2     __subd,B6         ; |859| 
           MVKH    .S2     __subd,B6         ; |859| 

           LDW     .D2T1   *+SP(4632),A5     ; |859| 
||         MV      .L2X    A5,B5             ; |859| 
||         CALL    .S2     B6                ; |859| 

           LDW     .D2T1   *+SP(4636),A4     ; |859| 
||         MV      .L2X    A4,B4             ; |859| 

           ADDKPC  .S2     RL358,B3,3        ; |859| 
RL358:     ; CALL OCCURS {__subd}            ; |859| 
           MVKL    .S1     __mpyd,A3         ; |859| 
           MVKH    .S1     __mpyd,A3         ; |859| 
           LDW     .D2T2   *+SP(4672),B5     ; |859| 
           CALL    .S2X    A3                ; |859| 
           LDW     .D2T2   *+SP(4676),B4     ; |859| 
           ADDKPC  .S2     RL359,B3,3        ; |859| 
RL359:     ; CALL OCCURS {__mpyd}            ; |859| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |859| 
           MVKH    .S1     __mpyd,A3         ; |859| 
           LDW     .D2T2   *+SP(4644),B5     ; |859| 
           CALL    .S2X    A3                ; |859| 
           LDW     .D2T2   *+SP(4640),B4     ; |859| 
           ADDKPC  .S2     RL360,B3,3        ; |859| 
RL360:     ; CALL OCCURS {__mpyd}            ; |859| 
;** --------------------------------------------------------------------------*

           ADDAD   .D2     SP,26,B7          ; |868| 
||         MVK     .S2     584,B4            ; |861| 
||         ZERO    .L1     A6                ; |868| 

           STW     .D2T1   A6,*+SP(1408)     ; |868| 
||         ADD     .L2     B4,SP,B6          ; |861| 

           STDW    .D2T2   B11:B10,*+B6(200) ; |862| 
||         MVK     .S2     984,B5            ; |863| 

           STDW    .D2T2   B11:B10,*B6       ; |861| 
||         ADD     .L2     B5,SP,B5          ; |863| 

           STDW    .D2T2   B11:B10,*B5       ; |863| 
||         MV      .L1     A5,A9             ; |859| 

           STDW    .D2T2   B11:B10,*+B5(232) ; |865| 
||         MVKL    .S1     _MultMatrix_B_TC_Ex,A3 ; |872| 
||         ZERO    .L1     A7                ; |868| 
||         MV      .D1     A4,A8             ; |859| 

           STDW    .D1T1   A9:A8,*+A12(192)  ; |859| 
||         LDW     .D2T2   *+SP(4628),B4     ; |868| 
||         MVKH    .S1     0x3ff00000,A7     ; |868| 

           STDW    .D2T1   A7:A6,*+SP(8)     ; |868| 
||         MVKH    .S1     _MultMatrix_B_TC_Ex,A3 ; |872| 

           STDW    .D2T1   A7:A6,*+B5(24)    ; |868| 

           CALL    .S2X    A3                ; |872| 
||         STDW    .D2T1   A7:A6,*+B7(200)   ; |868| 

           STDW    .D2T1   A7:A6,*+B5(224)   ; |868| 
           STDW    .D2T1   A7:A6,*-B5(176)   ; |868| 
           STDW    .D2T1   A7:A6,*+B6(24)    ; |868| 
           STW     .D2T1   A7,*+SP(1412)     ; |868| 

           ADDKPC  .S2     RL361,B3,0        ; |872| 
||         ADD     .L1X    8,SP,A6           ; |872| 
||         STDW    .D2T1   A7:A6,*B7         ; |868| 
||         MV      .S1     A14,A4            ; |872| 

RL361:     ; CALL OCCURS {_MultMatrix_B_TC_Ex}  ; |872| 
           MVKL    .S2     _MultMatrix_SimResEx,B5 ; |873| 
           MVKH    .S2     _MultMatrix_SimResEx,B5 ; |873| 
           CALL    .S2     B5                ; |873| 
           ADDKPC  .S2     RL362,B3,1        ; |873| 
           MV      .L1     A14,A6            ; |873| 
           ADD     .L2     8,SP,B4           ; |873| 
           MV      .S1     A14,A4            ; |873| 
RL362:     ; CALL OCCURS {_MultMatrix_SimResEx}  ; |873| 
;** --------------------------------------------------------------------------*

           SUB     .L1     A15,A14,A4        ; |885| 
||         SUB     .D1     A14,A15,A3        ; |885| 
||         MVK     .S1     14680,A6          ; |885| 
||         MVK     .S2     -12968,B4         ; |885| 
||         MV      .L2X    A14,B12

           CMPLT   .L1     A3,A6,A31         ; |885| 
||         MVK     .S1     14560,A5
||         MVKL    .S2     __mpyd,B6         ; |886| 
||         MVK     .D1     0x8,A29
||         MV      .L2X    A14,B13

           CMPLT   .L2X    A4,B4,B4          ; |885| 
||         MVK     .S1     14560,A30
||         ADD     .L1     A5,A15,A12
||         MVKH    .S2     __mpyd,B6         ; |886| 
||         MV      .D1     A29,A10

           AND     .L2X    A31,B4,B0
||         MV      .L1     A29,A13
||         ADD     .S1     A30,A15,A11

   [!B0]   BNOP    .S1     L35,4             ; |885| 
|| [!B0]   MVKL    .S2     __mpyd,B6         ; |886| 

   [!B0]   MVKH    .S2     __mpyd,B6         ; |886| 
           ; BRANCHCC OCCURS {L35}           ; |885| 
;** --------------------------------------------------------------------------*

           CALL    .S2     B6                ; |886| 
||         LDDW    .D1T1   *A12++,A5:A4      ; |886| 

	.dwpsn	"OPManager.c",885,0
           NOP             4
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L34:    
DW$L$_OP_CovMatrPropagation$13$B:
	.dwpsn	"OPManager.c",886,0

           MV      .L2     B11,B5            ; |886| 
||         MV      .D2     B10,B4            ; |886| 
||         ADDKPC  .S2     RL364,B3,0        ; |886| 

RL364:     ; CALL OCCURS {__mpyd}            ; |886| 
DW$L$_OP_CovMatrPropagation$13$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation$14$B:
           MVKL    .S1     __addd,A3         ; |886| 
           MVKH    .S1     __addd,A3         ; |886| 
           LDDW    .D2T2   *B13,B7:B6        ; |886| 
           CALL    .S2X    A3                ; |886| 
           MV      .L2X    A4,B4             ; |886| 
           ADDKPC  .S2     RL365,B3,0        ; |886| 
           MV      .L2X    A5,B5             ; |886| 
           MV      .L1X    B6,A4             ; |886| 
           MV      .L1X    B7,A5             ; |886| 
RL365:     ; CALL OCCURS {__addd}            ; |886| 
DW$L$_OP_CovMatrPropagation$14$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation$15$B:

           MVKL    .S2     __mpyd,B6         ; |886| 
||         SUB     .L1     A10,1,A0          ; |885| 
||         STDW    .D2T1   A5:A4,*B13        ; |886| 
||         SUB     .S1     A10,1,A10         ; |885| 

           MVKH    .S2     __mpyd,B6         ; |886| 
|| [!A0]   LDW     .D2T2   *+SP(4708),B3     ; |889| 
|| [ A0]   LDDW    .D1T1   *A12++,A5:A4      ; |886| 
|| [ A0]   B       .S1     L34               ; |885| 

   [!A0]   LDW     .D2T1   *+SP(4728),A15    ; |889| 
|| [ A0]   CALL    .S2     B6                ; |886| 

           ADDK    .S2     200,B13           ; |885| 
|| [!A0]   LDW     .D2T1   *+SP(4692),A11    ; |889| 

   [!A0]   LDW     .D2T1   *+SP(4688),A10    ; |889| 
   [!A0]   LDW     .D2T1   *+SP(4700),A13    ; |889| 
   [!A0]   LDW     .D2T1   *+SP(4696),A12    ; |889| 
           ; BRANCHCC OCCURS {L34}           ; |885| 
DW$L$_OP_CovMatrPropagation$15$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4720),B12    ; |889| 
           LDW     .D2T2   *+SP(4724),B13    ; |889| 
           LDW     .D2T2   *+SP(4712),B10    ; |889| 

           LDW     .D2T2   *+SP(4716),B11    ; |889| 
||         RET     .S2     B3                ; |889| 

           LDW     .D2T1   *+SP(4704),A14    ; |889| 
||         ADDK    .S2     4728,SP           ; |889| 

           NOP             4
           ; BRANCH OCCURS {B3}              ; |889| 
;** --------------------------------------------------------------------------*
L35:    

           CALL    .S2     B6                ; |886| 
||         LDDW    .D1T1   *A11++,A5:A4      ; |886| 

	.dwpsn	"OPManager.c",885,0
           NOP             4
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L36:    
DW$L$_OP_CovMatrPropagation$18$B:
	.dwpsn	"OPManager.c",886,0

           MV      .L2     B11,B5            ; |886| 
||         MV      .D2     B10,B4            ; |886| 
||         ADDKPC  .S2     RL367,B3,0        ; |886| 

RL367:     ; CALL OCCURS {__mpyd}            ; |886| 
DW$L$_OP_CovMatrPropagation$18$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation$19$B:
           MVKL    .S1     __addd,A3         ; |886| 
           MVKH    .S1     __addd,A3         ; |886| 
           LDDW    .D2T2   *B12,B7:B6        ; |886| 
           CALL    .S2X    A3                ; |886| 
           MV      .L2X    A4,B4             ; |886| 
           ADDKPC  .S2     RL368,B3,0        ; |886| 
           MV      .L2X    A5,B5             ; |886| 
           MV      .L1X    B6,A4             ; |886| 
           MV      .L1X    B7,A5             ; |886| 
RL368:     ; CALL OCCURS {__addd}            ; |886| 
DW$L$_OP_CovMatrPropagation$19$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation$20$B:

           MVKL    .S2     __mpyd,B6         ; |886| 
||         SUB     .L1     A13,1,A0          ; |885| 
||         STDW    .D2T1   A5:A4,*B12        ; |886| 
||         SUB     .S1     A13,1,A13         ; |885| 

           MVKH    .S2     __mpyd,B6         ; |886| 
|| [!A0]   LDW     .D2T2   *+SP(4708),B3     ; |889| 
|| [ A0]   LDDW    .D1T1   *A11++,A5:A4      ; |886| 
|| [ A0]   B       .S1     L36               ; |885| 

   [!A0]   LDW     .D2T1   *+SP(4728),A15    ; |889| 
|| [ A0]   CALL    .S2     B6                ; |886| 

           ADDK    .S2     200,B12           ; |885| 
|| [!A0]   LDW     .D2T1   *+SP(4692),A11    ; |889| 

   [!A0]   LDW     .D2T1   *+SP(4688),A10    ; |889| 
   [!A0]   LDW     .D2T1   *+SP(4700),A13    ; |889| 
   [!A0]   LDW     .D2T1   *+SP(4696),A12    ; |889| 
           ; BRANCHCC OCCURS {L36}           ; |885| 
DW$L$_OP_CovMatrPropagation$20$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4720),B12    ; |889| 
           LDW     .D2T2   *+SP(4724),B13    ; |889| 
           LDW     .D2T2   *+SP(4712),B10    ; |889| 

           LDW     .D2T2   *+SP(4716),B11    ; |889| 
||         RET     .S2     B3                ; |889| 

           LDW     .D2T1   *+SP(4704),A14    ; |889| 
||         ADDK    .S2     4728,SP           ; |889| 

	.dwpsn	"OPManager.c",889,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |889| 

DW$149	.dwtag  DW_TAG_loop
	.dwattr DW$149, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L36:1:1472539744")
	.dwattr DW$149, DW_AT_begin_file("OPManager.c")
	.dwattr DW$149, DW_AT_begin_line(0x375)
	.dwattr DW$149, DW_AT_end_line(0x376)
DW$150	.dwtag  DW_TAG_loop_range
	.dwattr DW$150, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation$18$B)
	.dwattr DW$150, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation$18$E)
DW$151	.dwtag  DW_TAG_loop_range
	.dwattr DW$151, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation$19$B)
	.dwattr DW$151, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation$19$E)
DW$152	.dwtag  DW_TAG_loop_range
	.dwattr DW$152, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation$20$B)
	.dwattr DW$152, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation$20$E)
	.dwendtag DW$149


DW$153	.dwtag  DW_TAG_loop
	.dwattr DW$153, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L34:1:1472539744")
	.dwattr DW$153, DW_AT_begin_file("OPManager.c")
	.dwattr DW$153, DW_AT_begin_line(0x375)
	.dwattr DW$153, DW_AT_end_line(0x376)
DW$154	.dwtag  DW_TAG_loop_range
	.dwattr DW$154, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation$13$B)
	.dwattr DW$154, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation$13$E)
DW$155	.dwtag  DW_TAG_loop_range
	.dwattr DW$155, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation$14$B)
	.dwattr DW$155, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation$14$E)
DW$156	.dwtag  DW_TAG_loop_range
	.dwattr DW$156, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation$15$B)
	.dwattr DW$156, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation$15$E)
	.dwendtag DW$153

	.dwattr DW$142, DW_AT_end_file("OPManager.c")
	.dwattr DW$142, DW_AT_end_line(0x379)
	.dwattr DW$142, DW_AT_end_column(0x01)
	.dwendtag DW$142

	.sect	".text"

DW$157	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_CovMatrPropagation_Static"), DW_AT_symbol_name("_OP_CovMatrPropagation_Static")
	.dwattr DW$157, DW_AT_low_pc(_OP_CovMatrPropagation_Static)
	.dwattr DW$157, DW_AT_high_pc(0x00)
	.dwattr DW$157, DW_AT_begin_file("OPManager.c")
	.dwattr DW$157, DW_AT_begin_line(0x385)
	.dwattr DW$157, DW_AT_begin_column(0x0d)
	.dwattr DW$157, DW_AT_frame_base[DW_OP_breg31 4664]
	.dwattr DW$157, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",905,1

;******************************************************************************
;* FUNCTION NAME: _OP_CovMatrPropagation_Static                               *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,B0,*
;*                           B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,SP,   *
;*                           A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27, *
;*                           A28,A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23, *
;*                           B24,B25,B26,B27,B28,B29,B30,B31                  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,B0,*
;*                           B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,SP,   *
;*                           A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27, *
;*                           A28,A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23, *
;*                           B24,B25,B26,B27,B28,B29,B30,B31                  *
;*   Local Frame Size  : 0 Args + 4620 Auto + 40 Save = 4660 byte             *
;******************************************************************************
_OP_CovMatrPropagation_Static:
;** --------------------------------------------------------------------------*
DW$158	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$158, DW_AT_type(*DW$T$189)
	.dwattr DW$158, DW_AT_location[DW_OP_reg4]
DW$159	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dt"), DW_AT_symbol_name("_dt")
	.dwattr DW$159, DW_AT_type(*DW$T$20)
	.dwattr DW$159, DW_AT_location[DW_OP_reg20]
DW$160	.dwtag  DW_TAG_formal_parameter, DW_AT_name("P0"), DW_AT_symbol_name("_P0")
	.dwattr DW$160, DW_AT_type(*DW$T$171)
	.dwattr DW$160, DW_AT_location[DW_OP_reg6]
DW$161	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Pp"), DW_AT_symbol_name("_Pp")
	.dwattr DW$161, DW_AT_type(*DW$T$171)
	.dwattr DW$161, DW_AT_location[DW_OP_reg22]

           ADDK    .S2     -4664,SP          ; |905| 
||         MVKL    .S1     _CreateMatrixEx,A3 ; |912| 
||         MV      .L1X    SP,A31            ; |905| 

           STW     .D2T1   A14,*+SP(4664)
||         MVKH    .S1     _CreateMatrixEx,A3 ; |912| 

           STW     .D2T2   B11,*+SP(4652)

           CALL    .S2X    A3                ; |912| 
||         STW     .D2T2   B12,*+SP(4656)

           STW     .D2T2   B10,*+SP(4648)

           SUB     .L1X    A4,B6,A14         ; |925| 
||         STW     .D2T2   B13,*+SP(4660)

           SUB     .L1X    B6,A4,A13         ; |925| 
||         STDW    .D1T1   A13:A12,*-A31(24)
||         STW     .D2T2   B3,*+SP(4628)

           MV      .L1X    B6,A11            ; |905| 
||         MV      .S1     A6,A10            ; |905| 
||         STDW    .D1T1   A11:A10,*-A31(32)
||         MV      .L2     B6,B12            ; |905| 
||         MV      .S2     B5,B11            ; |905| 
||         MV      .D2     B4,B10            ; |905| 

           ADDKPC  .S2     RL369,B3,0        ; |912| 
||         MV      .L1X    B6,A4             ; |905| 
||         MVK     .S1     0x8,A6            ; |912| 
||         MV      .D1     A4,A12            ; |905| 
||         MVK     .L2     0x8,B4            ; |912| 
||         MV      .D2     B6,B13            ; |905| 

RL369:     ; CALL OCCURS {_CreateMatrixEx}   ; |912| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _CreateMatrixEx,A3 ; |913| 
           MVKH    .S1     _CreateMatrixEx,A3 ; |913| 
           MVK     .L1     0x8,A6            ; |913| 
           CALL    .S2X    A3                ; |913| 
           ADDKPC  .S2     RL370,B3,2        ; |913| 
           MVK     .L2     0x8,B4            ; |913| 
           ADD     .S1X    8,SP,A4           ; |913| 
RL370:     ; CALL OCCURS {_CreateMatrixEx}   ; |913| 
           ZERO    .L1     A7                ; |918| 

           ADDAD   .D2     SP,26,B6          ; |918| 
||         ZERO    .L1     A6                ; |918| 
||         MVKH    .S1     0x3ff00000,A7     ; |918| 

           MVKL    .S1     _MultMatrix_B_TC_Ex,A3 ; |922| 
||         STDW    .D2T1   A7:A6,*B6         ; |918| 

           MVK     .S2     1216,B4           ; |915| 
||         MVKH    .S1     _MultMatrix_B_TC_Ex,A3 ; |922| 
||         STDW    .D2T1   A7:A6,*+B6(200)   ; |918| 

           ADD     .L2     B4,SP,B5          ; |915| 
||         STDW    .D2T1   A7:A6,*+SP(8)     ; |918| 

           STDW    .D2T1   A7:A6,*-B5(208)   ; |918| 
||         CALL    .S2X    A3                ; |922| 

           MVK     .S2     608,B7            ; |918| 
||         STDW    .D2T1   A7:A6,*-B5(8)     ; |918| 

           ADD     .L2     B7,SP,B6          ; |918| 
||         STDW    .D2T1   A7:A6,*+B5(192)   ; |918| 

           STDW    .D2T1   A7:A6,*+B6(200)   ; |918| 
           STDW    .D2T2   B11:B10,*B5       ; |915| 

           MV      .S1     A11,A4            ; |922| 
||         MV      .L2X    A10,B4            ; |922| 
||         ADD     .L1X    8,SP,A6           ; |922| 
||         STDW    .D2T1   A7:A6,*B6         ; |918| 
||         ADDKPC  .S2     RL371,B3,0        ; |922| 

RL371:     ; CALL OCCURS {_MultMatrix_B_TC_Ex}  ; |922| 
           MVKL    .S2     _MultMatrix_SimResEx,B5 ; |923| 
           MVKH    .S2     _MultMatrix_SimResEx,B5 ; |923| 
           CALL    .S2     B5                ; |923| 
           MV      .S1     A11,A4            ; |923| 
           ADD     .L2     8,SP,B4           ; |923| 
           MV      .L1     A11,A6            ; |923| 
           ADDKPC  .S2     RL372,B3,1        ; |923| 
RL372:     ; CALL OCCURS {_MultMatrix_SimResEx}  ; |923| 
;** --------------------------------------------------------------------------*

           MVK     .S2     14680,B5          ; |925| 
||         MVK     .S1     -12968,A3         ; |925| 
||         MVK     .L1     0x8,A11

           CMPLT   .L2X    A13,B5,B4         ; |925| 
||         CMPLT   .L1     A14,A3,A4         ; |925| 
||         MVK     .S1     14560,A5
||         MVKL    .S2     __mpyd,B6         ; |926| 
||         MVK     .D1     0x8,A3

           MVK     .S1     14560,A6
||         MV      .L1     A3,A10
||         MVKH    .S2     __mpyd,B6         ; |926| 

           AND     .L1X    B4,A4,A0
||         ADD     .S1     A6,A12,A13
||         ADD     .D1     A5,A12,A12

   [!A0]   BNOP    .S1     L38,4             ; |925| 
|| [!A0]   MVKL    .S2     __mpyd,B6         ; |926| 

   [!A0]   MVKH    .S2     __mpyd,B6         ; |926| 
           ; BRANCHCC OCCURS {L38}           ; |925| 
;** --------------------------------------------------------------------------*

           CALL    .S2     B6                ; |926| 
||         LDDW    .D1T1   *A12++,A5:A4      ; |926| 

	.dwpsn	"OPManager.c",925,0
           NOP             4
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L37:    
DW$L$_OP_CovMatrPropagation_Static$5$B:
	.dwpsn	"OPManager.c",926,0

           MV      .L2     B11,B5            ; |926| 
||         MV      .D2     B10,B4            ; |926| 
||         ADDKPC  .S2     RL374,B3,0        ; |926| 

RL374:     ; CALL OCCURS {__mpyd}            ; |926| 
DW$L$_OP_CovMatrPropagation_Static$5$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation_Static$6$B:
           MVKL    .S1     __addd,A3         ; |926| 
           MVKH    .S1     __addd,A3         ; |926| 
           LDDW    .D2T2   *B13,B7:B6        ; |926| 
           CALL    .S2X    A3                ; |926| 
           MV      .L2X    A4,B4             ; |926| 
           ADDKPC  .S2     RL375,B3,0        ; |926| 
           MV      .L2X    A5,B5             ; |926| 
           MV      .L1X    B6,A4             ; |926| 
           MV      .L1X    B7,A5             ; |926| 
RL375:     ; CALL OCCURS {__addd}            ; |926| 
DW$L$_OP_CovMatrPropagation_Static$6$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation_Static$7$B:

           MVKL    .S2     __mpyd,B6         ; |926| 
||         SUB     .L1     A11,1,A0          ; |925| 
||         STDW    .D2T1   A5:A4,*B13        ; |926| 
||         SUB     .S1     A11,1,A11         ; |925| 

           MVKH    .S2     __mpyd,B6         ; |926| 
|| [!A0]   LDW     .D2T2   *+SP(4628),B3     ; |929| 
|| [ A0]   LDDW    .D1T1   *A12++,A5:A4      ; |926| 
|| [ A0]   B       .S1     L37               ; |925| 

   [!A0]   LDW     .D2T1   *+SP(4664),A14    ; |929| 
|| [ A0]   CALL    .S2     B6                ; |926| 

           ADDK    .S2     200,B13           ; |925| 
|| [!A0]   LDW     .D2T1   *+SP(4636),A11    ; |929| 

   [!A0]   LDW     .D2T1   *+SP(4632),A10    ; |929| 
   [!A0]   LDW     .D2T1   *+SP(4644),A13    ; |929| 
   [!A0]   LDW     .D2T1   *+SP(4640),A12    ; |929| 
           ; BRANCHCC OCCURS {L37}           ; |925| 
DW$L$_OP_CovMatrPropagation_Static$7$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4656),B12    ; |929| 
           LDW     .D2T2   *+SP(4660),B13    ; |929| 

           LDW     .D2T2   *+SP(4648),B10    ; |929| 
||         RET     .S2     B3                ; |929| 

           LDW     .D2T2   *+SP(4652),B11    ; |929| 
||         ADDK    .S2     4664,SP           ; |929| 

           NOP             4
           ; BRANCH OCCURS {B3}              ; |929| 
;** --------------------------------------------------------------------------*
L38:    

           CALL    .S2     B6                ; |926| 
||         LDDW    .D1T1   *A13++,A5:A4      ; |926| 

	.dwpsn	"OPManager.c",925,0
           NOP             4
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L39:    
DW$L$_OP_CovMatrPropagation_Static$10$B:
	.dwpsn	"OPManager.c",926,0

           MV      .L2     B11,B5            ; |926| 
||         MV      .D2     B10,B4            ; |926| 
||         ADDKPC  .S2     RL377,B3,0        ; |926| 

RL377:     ; CALL OCCURS {__mpyd}            ; |926| 
DW$L$_OP_CovMatrPropagation_Static$10$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation_Static$11$B:
           MVKL    .S1     __addd,A3         ; |926| 
           MVKH    .S1     __addd,A3         ; |926| 
           LDDW    .D2T2   *B12,B7:B6        ; |926| 
           CALL    .S2X    A3                ; |926| 
           MV      .L2X    A4,B4             ; |926| 
           ADDKPC  .S2     RL378,B3,0        ; |926| 
           MV      .L2X    A5,B5             ; |926| 
           MV      .L1X    B6,A4             ; |926| 
           MV      .L1X    B7,A5             ; |926| 
RL378:     ; CALL OCCURS {__addd}            ; |926| 
DW$L$_OP_CovMatrPropagation_Static$11$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrPropagation_Static$12$B:

           MVKL    .S2     __mpyd,B6         ; |926| 
||         SUB     .L1     A10,1,A0          ; |925| 
||         STDW    .D2T1   A5:A4,*B12        ; |926| 
||         SUB     .S1     A10,1,A10         ; |925| 

           MVKH    .S2     __mpyd,B6         ; |926| 
|| [!A0]   LDW     .D2T2   *+SP(4628),B3     ; |929| 
|| [ A0]   LDDW    .D1T1   *A13++,A5:A4      ; |926| 
|| [ A0]   B       .S1     L39               ; |925| 

   [!A0]   LDW     .D2T1   *+SP(4664),A14    ; |929| 
|| [ A0]   CALL    .S2     B6                ; |926| 

           ADDK    .S2     200,B12           ; |925| 
|| [!A0]   LDW     .D2T1   *+SP(4636),A11    ; |929| 

   [!A0]   LDW     .D2T1   *+SP(4632),A10    ; |929| 
   [!A0]   LDW     .D2T1   *+SP(4644),A13    ; |929| 
   [!A0]   LDW     .D2T1   *+SP(4640),A12    ; |929| 
           ; BRANCHCC OCCURS {L39}           ; |925| 
DW$L$_OP_CovMatrPropagation_Static$12$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4656),B12    ; |929| 
           LDW     .D2T2   *+SP(4660),B13    ; |929| 

           LDW     .D2T2   *+SP(4648),B10    ; |929| 
||         RET     .S2     B3                ; |929| 

           LDW     .D2T2   *+SP(4652),B11    ; |929| 
||         ADDK    .S2     4664,SP           ; |929| 

	.dwpsn	"OPManager.c",929,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |929| 

DW$162	.dwtag  DW_TAG_loop
	.dwattr DW$162, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L39:1:1472539744")
	.dwattr DW$162, DW_AT_begin_file("OPManager.c")
	.dwattr DW$162, DW_AT_begin_line(0x39d)
	.dwattr DW$162, DW_AT_end_line(0x39e)
DW$163	.dwtag  DW_TAG_loop_range
	.dwattr DW$163, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation_Static$10$B)
	.dwattr DW$163, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation_Static$10$E)
DW$164	.dwtag  DW_TAG_loop_range
	.dwattr DW$164, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation_Static$11$B)
	.dwattr DW$164, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation_Static$11$E)
DW$165	.dwtag  DW_TAG_loop_range
	.dwattr DW$165, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation_Static$12$B)
	.dwattr DW$165, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation_Static$12$E)
	.dwendtag DW$162


DW$166	.dwtag  DW_TAG_loop
	.dwattr DW$166, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L37:1:1472539744")
	.dwattr DW$166, DW_AT_begin_file("OPManager.c")
	.dwattr DW$166, DW_AT_begin_line(0x39d)
	.dwattr DW$166, DW_AT_end_line(0x39e)
DW$167	.dwtag  DW_TAG_loop_range
	.dwattr DW$167, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation_Static$5$B)
	.dwattr DW$167, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation_Static$5$E)
DW$168	.dwtag  DW_TAG_loop_range
	.dwattr DW$168, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation_Static$6$B)
	.dwattr DW$168, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation_Static$6$E)
DW$169	.dwtag  DW_TAG_loop_range
	.dwattr DW$169, DW_AT_low_pc(DW$L$_OP_CovMatrPropagation_Static$7$B)
	.dwattr DW$169, DW_AT_high_pc(DW$L$_OP_CovMatrPropagation_Static$7$E)
	.dwendtag DW$166

	.dwattr DW$157, DW_AT_end_file("OPManager.c")
	.dwattr DW$157, DW_AT_end_line(0x3a1)
	.dwattr DW$157, DW_AT_end_column(0x01)
	.dwendtag DW$157

	.sect	".text"

DW$170	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_HMatrixCalulation"), DW_AT_symbol_name("_OP_HMatrixCalulation")
	.dwattr DW$170, DW_AT_low_pc(_OP_HMatrixCalulation)
	.dwattr DW$170, DW_AT_high_pc(0x00)
	.dwattr DW$170, DW_AT_begin_file("OPManager.c")
	.dwattr DW$170, DW_AT_begin_line(0x3b1)
	.dwattr DW$170, DW_AT_begin_column(0x0f)
	.dwattr DW$170, DW_AT_frame_base[DW_OP_breg31 504]
	.dwattr DW$170, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",955,1

;******************************************************************************
;* FUNCTION NAME: _OP_HMatrixCalulation                                       *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 456 Auto + 44 Save = 500 byte               *
;******************************************************************************
_OP_HMatrixCalulation:
;** --------------------------------------------------------------------------*
DW$171	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$171, DW_AT_type(*DW$T$189)
	.dwattr DW$171, DW_AT_location[DW_OP_reg4]
DW$172	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pTopSystem"), DW_AT_symbol_name("_pTopSystem")
	.dwattr DW$172, DW_AT_type(*DW$T$195)
	.dwattr DW$172, DW_AT_location[DW_OP_reg20]
DW$173	.dwtag  DW_TAG_formal_parameter, DW_AT_name("NavCond"), DW_AT_symbol_name("_NavCond")
	.dwattr DW$173, DW_AT_type(*DW$T$197)
	.dwattr DW$173, DW_AT_location[DW_OP_reg6]
DW$174	.dwtag  DW_TAG_formal_parameter, DW_AT_name("X"), DW_AT_symbol_name("_X")
	.dwattr DW$174, DW_AT_type(*DW$T$57)
	.dwattr DW$174, DW_AT_location[DW_OP_reg22]
DW$175	.dwtag  DW_TAG_formal_parameter, DW_AT_name("H"), DW_AT_symbol_name("_H")
	.dwattr DW$175, DW_AT_type(*DW$T$171)
	.dwattr DW$175, DW_AT_location[DW_OP_reg8]
DW$176	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dYv"), DW_AT_symbol_name("_dYv")
	.dwattr DW$176, DW_AT_type(*DW$T$57)
	.dwattr DW$176, DW_AT_location[DW_OP_reg24]
DW$177	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dYp"), DW_AT_symbol_name("_dYp")
	.dwattr DW$177, DW_AT_type(*DW$T$57)
	.dwattr DW$177, DW_AT_location[DW_OP_reg10]
DW$178	.dwtag  DW_TAG_formal_parameter, DW_AT_name("BinStrFlag"), DW_AT_symbol_name("_BinStrFlag")
	.dwattr DW$178, DW_AT_type(*DW$T$52)
	.dwattr DW$178, DW_AT_location[DW_OP_reg26]
DW$179	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataStr"), DW_AT_symbol_name("_dataStr")
	.dwattr DW$179, DW_AT_type(*DW$T$200)
	.dwattr DW$179, DW_AT_location[DW_OP_reg12]
DW$180	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DebugTerminal"), DW_AT_symbol_name("_DebugTerminal")
	.dwattr DW$180, DW_AT_type(*DW$T$202)
	.dwattr DW$180, DW_AT_location[DW_OP_reg28]

           ADDK    .S2     -504,SP           ; |955| 
||         MV      .L1X    SP,A31            ; |955| 

           STW     .D2T1   A4,*+SP(352)      ; |955| 
           STW     .D2T2   B3,*+SP(484)

           STW     .D2T2   B10,*+SP(488)
||         MVKL    .S1     __cvtfd,A3        ; |973| 

           STW     .D2T1   A6,*+SP(356)      ; |955| 
||         MVKH    .S1     __cvtfd,A3        ; |973| 

           STW     .D2T1   A15,*+SP(504)

           CALL    .S2X    A3                ; |973| 
||         STW     .D2T1   A8,*+SP(368)      ; |955| 

           LDW     .D1T1   *+A6(72),A4       ; |973| 
||         STW     .D2T2   B13,*+SP(500)

           STW     .D2T2   B11,*+SP(492)
||         STDW    .D1T1   A11:A10,*-A31(40)

           STW     .D1T1   A14,*-A31(24)
||         STW     .D2T2   B12,*+SP(496)

           STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .L1X    B4,A11            ; |955| 
||         STW     .D2T2   B8,*+SP(360)      ; |955| 

           ADDKPC  .S2     RL414,B3,0        ; |973| 
||         STW     .D2T1   A11,*+SP(364)     ; |955| 
||         MV      .L2     B6,B10            ; |955| 

RL414:     ; CALL OCCURS {__cvtfd}           ; |973| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     _sin,B4           ; |973| 
           MVKH    .S2     _sin,B4           ; |973| 
           CALL    .S2     B4                ; |973| 
           ADDKPC  .S2     RL415,B3,4        ; |973| 
RL415:     ; CALL OCCURS {_sin}              ; |973| 
;** --------------------------------------------------------------------------*
           STW     .D2T1   A4,*+SP(388)      ; |973| 
           STW     .D2T1   A5,*+SP(384)      ; |973| 

           STW     .D2T1   A10,*+SP(372)     ; |955| 
||         MVK     .S1     344,A6
||         MVK     .L2     0xc,B4

           STW     .D2T2   B4,*+SP(376)
||         SUB     .L1     A11,A6,A3

	.dwpsn	"OPManager.c",975,0

           STW     .D2T1   A3,*+SP(380)
||         ZERO    .L1     A11               ; |975| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
L40:    
DW$L$_OP_HMatrixCalulation$4$B:
	.dwpsn	"OPManager.c",976,0

           LDW     .D2T1   *+SP(380),A3
||         MVK     .S1     340,A4            ; |980| 

           NOP             3
           LDW     .D2T2   *+SP(376),B4
           ADDK    .S1     344,A3            ; |980| 

           MV      .L1     A3,A12
||         STW     .D2T1   A3,*+SP(380)      ; |980| 

           LDB     .D1T1   *+A4[A3],A0       ; |980| 
           NOP             3
           ADDAD   .D1     A12,27,A13        ; |985| 

   [ A0]   MVK     .S1     0x77,A3           ; |985| 
|| [!A0]   B       .S2     L47               ; |980| 
||         MV      .L1     A0,A1             ; guard predicate rewrite

   [ A0]   LDBU    .D1T1   *+A3[A13],A3      ; |985| 
|| [ A0]   MVK     .S1     336,A4            ; |985| 

   [!A1]   SUB     .L2     B4,1,B4           ; |975| 
|| [!A0]   SUB     .L1X    B4,1,A0           ; |975| 
|| [ A0]   LDBU    .D1T1   *+A4[A12],A4      ; |985| 

   [ A1]   ZERO    .L1     A0                ; |975| nullify predicate
|| [!A1]   STW     .D2T2   B4,*+SP(376)      ; |975| 

   [ A0]   BNOP    .S1     L40,1             ; |975| 
           ; BRANCHCC OCCURS {L47}           ; |980| 
DW$L$_OP_HMatrixCalulation$4$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$5$B:
           CMPEQ   .L1     A3,1,A3           ; |985| 
           XOR     .L1     1,A3,A3           ; |985| 

           OR      .L1     A3,A4,A0          ; |985| 
||         MVKL    .S1     __subd,A3         ; |996| 

   [ A0]   LDW     .D2T2   *+SP(376),B4
|| [ A0]   B       .S2     L46               ; |985| 
|| [!A0]   LDDW    .D1T1   *+A13(16),A7:A6   ; |989| 
||         MVKH    .S1     __subd,A3         ; |996| 

   [!A0]   LDDW    .D1T1   *+A13(24),A5:A4   ; |990| 
|| [!A0]   LDDW    .D2T2   *+B10(40),B5:B4   ; |996| 

   [!A0]   LDDW    .D1T1   *+A13(8),A9:A8    ; |988| 
   [!A0]   CALL    .S2X    A3                ; |996| 
           NOP             1
   [ A0]   SUB     .L1X    B4,1,A0           ; |975| 
           ; BRANCHCC OCCURS {L46}           ; |985| 
DW$L$_OP_HMatrixCalulation$5$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$6$B:
           STDW    .D2T1   A7:A6,*+SP(40)    ; |989| 
           STDW    .D2T1   A5:A4,*+SP(48)    ; |990| 

           STDW    .D2T1   A9:A8,*+SP(32)    ; |988| 
||         ADDKPC  .S2     RL416,B3,0        ; |996| 

RL416:     ; CALL OCCURS {__subd}            ; |996| 
DW$L$_OP_HMatrixCalulation$6$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$7$B:
           MVKL    .S1     __subd,A3         ; |996| 
           MVKH    .S1     __subd,A3         ; |996| 
           LDDW    .D2T2   *+SP(40),B7:B6    ; |996| 
           CALL    .S2X    A3                ; |996| 
           LDDW    .D2T2   *+B10(32),B5:B4   ; |996| 
           MV      .S1     A4,A15            ; |996| 
           ADDKPC  .S2     RL417,B3,0        ; |996| 
           MV      .L1X    B6,A4             ; |996| 

           MV      .S1     A5,A14            ; |996| 
||         MV      .L1X    B7,A5             ; |996| 

RL417:     ; CALL OCCURS {__subd}            ; |996| 
           MVKL    .S1     __subd,A3         ; |996| 
           MVKH    .S1     __subd,A3         ; |996| 
           LDDW    .D2T2   *+SP(32),B7:B6    ; |996| 
           CALL    .S2X    A3                ; |996| 
           LDDW    .D2T2   *+B10(24),B5:B4   ; |996| 
           MV      .L2X    A4,B12            ; |996| 
           ADDKPC  .S2     RL418,B3,0        ; |996| 
           MV      .L1X    B6,A4             ; |996| 

           MV      .L2X    A5,B11            ; |996| 
||         MV      .L1X    B7,A5             ; |996| 

RL418:     ; CALL OCCURS {__subd}            ; |996| 
           MVKL    .S2     __mpyd,B6         ; |996| 
           MVKH    .S2     __mpyd,B6         ; |996| 
           CALL    .S2     B6                ; |996| 
           MV      .L2X    A4,B4             ; |996| 
           ADDKPC  .S2     RL442,B3,2        ; |996| 
           MV      .L2X    A5,B5             ; |996| 
RL442:     ; CALL OCCURS {__mpyd}            ; |996| 
           MVKL    .S1     __mpyd,A3         ; |996| 
           MVKH    .S1     __mpyd,A3         ; |996| 
           MV      .S1     A4,A10            ; |996| 
           CALL    .S2X    A3                ; |996| 
           MV      .L2     B12,B4            ; |996| 
           MV      .L1X    B12,A4            ; |996| 
           MV      .D2     B11,B5            ; |996| 
           MV      .L2X    A5,B13            ; |996| 

           MV      .L1X    B11,A5            ; |996| 
||         ADDKPC  .S2     RL443,B3,0        ; |996| 

RL443:     ; CALL OCCURS {__mpyd}            ; |996| 
           MVKL    .S2     __addd,B6         ; |996| 
           MVKH    .S2     __addd,B6         ; |996| 
           CALL    .S2     B6                ; |996| 
           MV      .L2X    A4,B4             ; |996| 
           MV      .L2X    A5,B5             ; |996| 
           MV      .S1     A10,A4            ; |996| 
           ADDKPC  .S2     RL444,B3,0        ; |996| 
           MV      .L1X    B13,A5            ; |996| 
RL444:     ; CALL OCCURS {__addd}            ; |996| 
           MVKL    .S2     __mpyd,B6         ; |996| 
           MVKH    .S2     __mpyd,B6         ; |996| 
           CALL    .S2     B6                ; |996| 
           MV      .L2X    A4,B12            ; |996| 
           MV      .L2X    A14,B5            ; |996| 
           MV      .L2X    A15,B4            ; |996| 
           MV      .L2X    A5,B11            ; |996| 

           MV      .S1     A15,A4            ; |996| 
||         ADDKPC  .S2     RL445,B3,0        ; |996| 
||         MV      .L1     A14,A5            ; |996| 

RL445:     ; CALL OCCURS {__mpyd}            ; |996| 
           MVKL    .S2     __addd,B6         ; |996| 
           MVKH    .S2     __addd,B6         ; |996| 
           CALL    .S2     B6                ; |996| 
           MV      .L2X    A4,B4             ; |996| 
           MV      .L1X    B12,A4            ; |996| 
           MV      .L2X    A5,B5             ; |996| 
           ADDKPC  .S2     RL446,B3,0        ; |996| 
           MV      .L1X    B11,A5            ; |996| 
RL446:     ; CALL OCCURS {__addd}            ; |996| 
           MVKL    .S1     _sqrt,A3          ; |996| 
           MVKH    .S1     _sqrt,A3          ; |996| 
           NOP             1
           CALL    .S2X    A3                ; |996| 
           ADDKPC  .S2     RL447,B3,4        ; |996| 
RL447:     ; CALL OCCURS {_sqrt}             ; |996| 
           MVKL    .S1     __mpyd,A3         ; |996| 
           MVKH    .S1     __mpyd,A3         ; |996| 
           MVKL    .S2     0x3e2ca726,B5     ; |996| 
           CALL    .S2X    A3                ; |996| 
           MVKL    .S2     0xeb25f9db,B4     ; |996| 
           MVKH    .S2     0x3e2ca726,B5     ; |996| 
           MVKH    .S2     0xeb25f9db,B4     ; |996| 
           ADDKPC  .S2     RL448,B3,1        ; |996| 
RL448:     ; CALL OCCURS {__mpyd}            ; |996| 
           MVKL    .S1     _Compensate_Rotation,A3 ; |1003| 

           LDDW    .D1T1   *+A13(32),A17:A16 ; |998| 
||         MVKH    .S1     _Compensate_Rotation,A3 ; |1003| 

           LDDW    .D1T1   *+A13(40),A9:A8   ; |999| 

           CALL    .S2X    A3                ; |1003| 
||         LDDW    .D1T1   *+A13(48),A19:A18 ; |1000| 

           ADD     .D2     SP,24,B4          ; |1003| 
           ADDKPC  .S2     RL449,B3,0        ; |1003| 
           STDW    .D2T1   A17:A16,*+SP(56)  ; |998| 
           STDW    .D2T1   A9:A8,*+SP(64)    ; |999| 

           STDW    .D2T1   A19:A18,*+SP(72)  ; |1000| 
||         MV      .S1     A5,A7             ; |996| 
||         MV      .D1     A4,A6             ; |996| 
||         MV      .L1X    B4,A4             ; |1003| 

RL449:     ; CALL OCCURS {_Compensate_Rotation}  ; |1003| 
DW$L$_OP_HMatrixCalulation$7$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$8$B:

           LDW     .D2T1   *+SP(356),A3      ; |1012| 
||         MVK     .S1     76,A4             ; |1012| 
||         MVK     .S2     11576,B4

           LDW     .D2T1   *+SP(364),A30
           NOP             1
           LDW     .D2T1   *+SP(360),A29
           LDW     .D2T1   *+SP(372),A31
           LDB     .D1T1   *+A4[A3],A0       ; |1012| 
           ADD     .L1X    B4,A30,A10
           MVKL    .S2     __cvtfd,B4        ; |1012| 

           ADDAD   .D1     A29,A11,A28
||         MVKH    .S2     __cvtfd,B4        ; |1012| 

           ADDAD   .D1     A31,A11,A15
||         STW     .D2T1   A28,*+SP(392)

           MV      .D1     A0,A13            ; |1012| 
|| [!A0]   MVK     .S1     77,A4             ; |1014| 
|| [ A0]   MV      .L1     A12,A3            ; |1012| 
|| [!A0]   B       .S2     L41               ; |1012| 
|| [!A0]   LDW     .D2T1   *+SP(356),A3      ; |1014| 

   [ A0]   LDW     .D1T1   *+A3(72),A4       ; |1012| 
|| [ A0]   CALL    .S2     B4                ; |1012| 

           NOP             3
   [!A0]   LDB     .D1T1   *+A3[A4],A0       ; |1014| 
           ; BRANCHCC OCCURS {L41}           ; |1012| 
DW$L$_OP_HMatrixCalulation$8$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$9$B:
           ADDKPC  .S2     RL451,B3,0        ; |1012| 
RL451:     ; CALL OCCURS {__cvtfd}           ; |1012| 
DW$L$_OP_HMatrixCalulation$9$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$10$B:
           MVKL    .S1     __addd,A3         ; |1012| 
           MVKH    .S1     __addd,A3         ; |1012| 
           ZERO    .L2     B5:B4             ; |1012| 
           CALL    .S2X    A3                ; |1012| 
           ADDKPC  .S2     RL452,B3,4        ; |1012| 
RL452:     ; CALL OCCURS {__addd}            ; |1012| 
DW$L$_OP_HMatrixCalulation$10$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$11$B:
           LDW     .D2T1   *+SP(356),A3      ; |1014| 
           NOP             1
           STW     .D2T1   A4,*+SP(400)      ; |1012| 
           MVK     .S1     77,A4             ; |1014| 
           STW     .D2T1   A5,*+SP(396)      ; |1012| 
           LDB     .D1T1   *+A3[A4],A0       ; |1014| 
DW$L$_OP_HMatrixCalulation$11$E:
;** --------------------------------------------------------------------------*
L41:    
DW$L$_OP_HMatrixCalulation$12$B:
           MV      .L1     A13,A1
           ZERO    .L2     B5:B4             ; |1012| 

   [!A1]   STW     .D2T2   B4,*+SP(400)      ; |1012| 
||         MVKL    .S2     __cvtfd,B4        ; |1019| 

           MVKH    .S2     __cvtfd,B4        ; |1019| 
|| [!A1]   STW     .D2T2   B5,*+SP(396)      ; |1012| 

   [ A0]   LDW     .D1T1   *+A12(76),A4      ; |1019| 
|| [!A0]   MVKL    .S2     __subd,B8         ; |1024| 
|| [!A0]   LDDW    .D2T2   *+B10(24),B5:B4   ; |1024| 

   [!A0]   MVKH    .S2     __subd,B8         ; |1024| 
|| [!A0]   B       .S1     L42               ; |1014| 

   [ A0]   CALL    .S2     B4                ; |1019| 
   [!A0]   LDDW    .D2T2   *+SP(32),B7:B6    ; |1024| 
   [!A0]   CALL    .S2     B8                ; |1024| 
           NOP             2
           ; BRANCHCC OCCURS {L42}           ; |1014| 
DW$L$_OP_HMatrixCalulation$12$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$13$B:
           ADDKPC  .S2     RL454,B3,0        ; |1019| 
RL454:     ; CALL OCCURS {__cvtfd}           ; |1019| 
DW$L$_OP_HMatrixCalulation$13$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$14$B:
           MVKL    .S1     __addd,A3         ; |1019| 
           MVKH    .S1     __addd,A3         ; |1019| 
           MV      .L2X    A5,B5             ; |1019| 

           CALL    .S2X    A3                ; |1019| 
||         LDW     .D2T1   *+SP(396),A5      ; |1019| 

           LDW     .D2T1   *+SP(400),A4      ; |1019| 
||         MV      .L2X    A4,B4             ; |1019| 

           ADDKPC  .S2     RL455,B3,3        ; |1019| 
RL455:     ; CALL OCCURS {__addd}            ; |1019| 
DW$L$_OP_HMatrixCalulation$14$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$15$B:
           MVKL    .S2     __subd,B8         ; |1024| 

           LDDW    .D2T2   *+SP(32),B7:B6    ; |1024| 
||         MVKH    .S2     __subd,B8         ; |1024| 

           CALL    .S2     B8                ; |1024| 
||         STW     .D2T1   A4,*+SP(400)      ; |1019| 

           LDDW    .D2T2   *+B10(24),B5:B4   ; |1024| 
           STW     .D2T1   A5,*+SP(396)      ; |1019| 
DW$L$_OP_HMatrixCalulation$15$E:
;** --------------------------------------------------------------------------*
L42:    
DW$L$_OP_HMatrixCalulation$16$B:
           ADDKPC  .S2     RL456,B3,0        ; |1024| 
           MV      .L1X    B6,A4             ; |1024| 
           MV      .L1X    B7,A5             ; |1024| 
RL456:     ; CALL OCCURS {__subd}            ; |1024| 
DW$L$_OP_HMatrixCalulation$16$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$17$B:
           MVKL    .S2     __subd,B8         ; |1025| 

           LDDW    .D2T2   *+SP(40),B7:B6    ; |1025| 
||         MVKH    .S2     __subd,B8         ; |1025| 

           CALL    .S2     B8                ; |1025| 
           LDDW    .D2T2   *+B10(32),B5:B4   ; |1025| 
           STW     .D2T1   A4,*+SP(408)      ; |1024| 
           ADDKPC  .S2     RL457,B3,0        ; |1025| 
           MV      .L1X    B6,A4             ; |1025| 

           STW     .D2T1   A5,*+SP(404)      ; |1024| 
||         MV      .L1X    B7,A5             ; |1025| 

RL457:     ; CALL OCCURS {__subd}            ; |1025| 
           MVKL    .S2     __subd,B8         ; |1026| 

           LDDW    .D2T2   *+SP(48),B7:B6    ; |1026| 
||         MVKH    .S2     __subd,B8         ; |1026| 

           CALL    .S2     B8                ; |1026| 
           LDDW    .D2T2   *+B10(40),B5:B4   ; |1026| 
           STW     .D2T1   A4,*+SP(416)      ; |1025| 
           ADDKPC  .S2     RL458,B3,0        ; |1026| 
           MV      .L1X    B6,A4             ; |1026| 

           STW     .D2T1   A5,*+SP(412)      ; |1025| 
||         MV      .L1X    B7,A5             ; |1026| 

RL458:     ; CALL OCCURS {__subd}            ; |1026| 
           MVKL    .S2     __subd,B8         ; |1028| 

           LDDW    .D2T2   *+SP(56),B7:B6    ; |1028| 
||         MVKH    .S2     __subd,B8         ; |1028| 

           CALL    .S2     B8                ; |1028| 
           LDDW    .D2T2   *B10,B5:B4        ; |1028| 
           MV      .L2X    A4,B11            ; |1026| 
           ADDKPC  .S2     RL459,B3,0        ; |1028| 
           MV      .L1X    B6,A4             ; |1028| 

           MV      .S1     A5,A13            ; |1026| 
||         MV      .L1X    B7,A5             ; |1028| 

RL459:     ; CALL OCCURS {__subd}            ; |1028| 
           MVKL    .S2     __subd,B8         ; |1029| 

           LDDW    .D2T2   *+SP(64),B7:B6    ; |1029| 
||         MVKH    .S2     __subd,B8         ; |1029| 

           CALL    .S2     B8                ; |1029| 
           LDDW    .D2T2   *+B10(8),B5:B4    ; |1029| 
           STW     .D2T1   A4,*+SP(424)      ; |1028| 
           ADDKPC  .S2     RL460,B3,0        ; |1029| 
           MV      .L1X    B6,A4             ; |1029| 

           STW     .D2T1   A5,*+SP(420)      ; |1028| 
||         MV      .L1X    B7,A5             ; |1029| 

RL460:     ; CALL OCCURS {__subd}            ; |1029| 
           MVKL    .S1     __subd,A3         ; |1030| 
           MVKH    .S1     __subd,A3         ; |1030| 
           LDDW    .D2T2   *+SP(72),B7:B6    ; |1030| 
           CALL    .S2X    A3                ; |1030| 
           LDDW    .D2T2   *+B10(16),B5:B4   ; |1030| 
           STW     .D2T1   A4,*+SP(432)      ; |1029| 
           ADDKPC  .S2     RL461,B3,0        ; |1030| 
           MV      .L1X    B6,A4             ; |1030| 

           STW     .D2T1   A5,*+SP(428)      ; |1029| 
||         MV      .L1X    B7,A5             ; |1030| 

RL461:     ; CALL OCCURS {__subd}            ; |1030| 

           STW     .D2T1   A4,*+SP(440)      ; |1030| 
||         MVKL    .S1     __mpyd,A3         ; |1037| 

           MVKH    .S1     __mpyd,A3         ; |1037| 
||         STW     .D2T1   A5,*+SP(436)      ; |1030| 

           LDW     .D2T1   *+SP(408),A4      ; |1037| 

           CALL    .S2X    A3                ; |1037| 
||         LDW     .D2T1   *+SP(404),A5      ; |1037| 

           ADDKPC  .S2     RL479,B3,2        ; |1037| 
           MV      .L2X    A4,B4             ; |1037| 
           MV      .L2X    A5,B5             ; |1037| 
RL479:     ; CALL OCCURS {__mpyd}            ; |1037| 
           MVKL    .S1     __mpyd,A3         ; |1037| 
           MVKH    .S1     __mpyd,A3         ; |1037| 

           LDW     .D2T1   *+SP(412),A5      ; |1037| 
||         MV      .L2X    A4,B12            ; |1037| 
||         MV      .L1     A5,A14            ; |1037| 

           CALL    .S2X    A3                ; |1037| 
||         LDW     .D2T1   *+SP(416),A4      ; |1037| 

           ADDKPC  .S2     RL480,B3,2        ; |1037| 
           MV      .L2X    A5,B5             ; |1037| 
           MV      .L2X    A4,B4             ; |1037| 
RL480:     ; CALL OCCURS {__mpyd}            ; |1037| 
           MVKL    .S1     __addd,A3         ; |1037| 
           MVKH    .S1     __addd,A3         ; |1037| 
           MV      .L2X    A5,B5             ; |1037| 
           CALL    .S2X    A3                ; |1037| 
           ADDKPC  .S2     RL481,B3,1        ; |1037| 
           MV      .S1     A14,A5            ; |1037| 
           MV      .L2X    A4,B4             ; |1037| 
           MV      .L1X    B12,A4            ; |1037| 
RL481:     ; CALL OCCURS {__addd}            ; |1037| 
           MVKL    .S2     __mpyd,B6         ; |1037| 
           MVKH    .S2     __mpyd,B6         ; |1037| 
           CALL    .S2     B6                ; |1037| 
           MV      .L2X    A13,B5            ; |1037| 
           MV      .D2     B11,B4            ; |1037| 
           MV      .D1     A5,A14            ; |1037| 
           MV      .S1     A13,A5            ; |1037| 

           MV      .L2X    A4,B12            ; |1037| 
||         MV      .L1X    B11,A4            ; |1037| 
||         ADDKPC  .S2     RL482,B3,0        ; |1037| 

RL482:     ; CALL OCCURS {__mpyd}            ; |1037| 
           MVKL    .S1     __addd,A3         ; |1037| 
           MVKH    .S1     __addd,A3         ; |1037| 
           MV      .L2X    A5,B5             ; |1037| 
           CALL    .S2X    A3                ; |1037| 
           MV      .L2X    A4,B4             ; |1037| 
           MV      .S1     A14,A5            ; |1037| 
           ADDKPC  .S2     RL483,B3,1        ; |1037| 
           MV      .L1X    B12,A4            ; |1037| 
RL483:     ; CALL OCCURS {__addd}            ; |1037| 
DW$L$_OP_HMatrixCalulation$17$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$18$B:
           MVKL    .S1     _sqrt,A3          ; |1037| 
           MVKH    .S1     _sqrt,A3          ; |1037| 
           NOP             1
           CALL    .S2X    A3                ; |1037| 
           ADDKPC  .S2     RL484,B3,4        ; |1037| 
RL484:     ; CALL OCCURS {_sqrt}             ; |1037| 
DW$L$_OP_HMatrixCalulation$18$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$19$B:
           MVK     .S1     272,A3            ; |1038| 
           ADD     .L1     A3,A12,A3         ; |1038| 

           MVKL    .S1     __mpyd,A3         ; |1038| 
||         LDDW    .D1T1   *A3,A7:A6         ; |1038| 

           MVKH    .S1     __mpyd,A3         ; |1038| 
           MVKL    .S2     0x41b1de78,B5     ; |1038| 
           CALL    .S2X    A3                ; |1038| 
           MVKH    .S2     0x41b1de78,B5     ; |1038| 
           ZERO    .L2     B4                ; |1038| 
           MVKH    .S2     0x4a000000,B4     ; |1038| 
           STW     .D2T1   A4,*+SP(448)      ; |1037| 

           ADDKPC  .S2     RL495,B3,0        ; |1038| 
||         MV      .L1     A7,A5             ; |1038| 
||         MV      .S1     A6,A4             ; |1038| 
||         STW     .D2T1   A5,*+SP(444)      ; |1037| 

RL495:     ; CALL OCCURS {__mpyd}            ; |1038| 
           MVKL    .S1     __addd,A3         ; |1038| 
           MVKH    .S1     __addd,A3         ; |1038| 

           LDW     .D2T1   *+SP(444),A5      ; |1038| 
||         MV      .L1     A5,A14            ; |1038| 

           CALL    .S2X    A3                ; |1038| 
||         LDDW    .D2T2   *+B10(48),B5:B4   ; |1038| 

           LDW     .D2T1   *+SP(448),A4      ; |1038| 
||         MV      .L2X    A4,B12            ; |1038| 

           ADDKPC  .S2     RL496,B3,3        ; |1038| 
RL496:     ; CALL OCCURS {__addd}            ; |1038| 
           MVKL    .S1     __mpyd,A3         ; |1038| 
           MVKH    .S1     __mpyd,A3         ; |1038| 
           LDDW    .D1T1   *+A12(216),A7:A6  ; |1038| 
           CALL    .S2X    A3                ; |1038| 
           MVKL    .S2     0x41b1de78,B5     ; |1038| 
           MVKH    .S2     0x41b1de78,B5     ; |1038| 
           ZERO    .L2     B4                ; |1038| 
           MVKH    .S2     0x4a000000,B4     ; |1038| 

           ADDKPC  .S2     RL497,B3,0        ; |1038| 
||         MV      .L1     A7,A5             ; |1038| 
||         MV      .S1     A6,A4             ; |1038| 
||         MV      .L2X    A5,B13            ; |1038| 
||         STW     .D2T1   A4,*+SP(452)      ; |1038| 

RL497:     ; CALL OCCURS {__mpyd}            ; |1038| 
           MVKL    .S1     __subd,A3         ; |1038| 
           MVKH    .S1     __subd,A3         ; |1038| 
           MV      .L2X    A4,B4             ; |1038| 
           CALL    .S2X    A3                ; |1038| 
           LDW     .D2T1   *+SP(452),A4      ; |1038| 
           ADDKPC  .S2     RL498,B3,1        ; |1038| 
           MV      .L2X    A5,B5             ; |1038| 
           MV      .L1X    B13,A5            ; |1038| 
RL498:     ; CALL OCCURS {__subd}            ; |1038| 
           MVKL    .S1     __addd,A3         ; |1038| 
           MVKH    .S1     __addd,A3         ; |1038| 
           LDW     .D2T2   *+SP(396),B5      ; |1038| 
           CALL    .S2X    A3                ; |1038| 
           LDW     .D2T2   *+SP(400),B4      ; |1038| 
           ADDKPC  .S2     RL499,B3,3        ; |1038| 
RL499:     ; CALL OCCURS {__addd}            ; |1038| 
           MVKL    .S2     __subd,B6         ; |1038| 
           MVKH    .S2     __subd,B6         ; |1038| 
           CALL    .S2     B6                ; |1038| 
           MV      .L2X    A5,B5             ; |1038| 
           MV      .L2X    A4,B4             ; |1038| 
           MV      .S1     A14,A5            ; |1038| 
           ADDKPC  .S2     RL500,B3,0        ; |1038| 
           MV      .L1X    B12,A4            ; |1038| 
RL500:     ; CALL OCCURS {__subd}            ; |1038| 
           MVKL    .S1     __mpyd,A3         ; |1041| 

           MVKH    .S1     __mpyd,A3         ; |1041| 
||         LDW     .D2T2   *+SP(420),B5      ; |1038| 

           LDW     .D2T2   *+SP(424),B4      ; |1041| 

           LDW     .D2T1   *+SP(404),A5      ; |1041| 
||         MV      .L1     A5,A7             ; |1038| 
||         CALL    .S2X    A3                ; |1041| 

           LDW     .D2T1   *+SP(408),A4      ; |1041| 
||         MV      .L1     A4,A6             ; |1038| 

           STDW    .D1T1   A7:A6,*A15        ; |1038| 
           ADDKPC  .S2     RL518,B3,2        ; |1041| 
RL518:     ; CALL OCCURS {__mpyd}            ; |1041| 

           MVKL    .S2     __mpyd,B6         ; |1041| 
||         LDW     .D2T2   *+SP(428),B5      ; |1041| 

           MVKH    .S2     __mpyd,B6         ; |1041| 
||         LDW     .D2T2   *+SP(432),B4      ; |1041| 

           LDW     .D2T1   *+SP(412),A5      ; |1041| 
||         MV      .L1     A5,A14            ; |1041| 
||         CALL    .S2     B6                ; |1041| 

           LDW     .D2T1   *+SP(416),A4      ; |1041| 
||         MV      .L2X    A4,B12            ; |1041| 

           ADDKPC  .S2     RL519,B3,3        ; |1041| 
RL519:     ; CALL OCCURS {__mpyd}            ; |1041| 
           MVKL    .S2     __addd,B6         ; |1041| 
           MVKH    .S2     __addd,B6         ; |1041| 
           CALL    .S2     B6                ; |1041| 
           MV      .L2X    A5,B5             ; |1041| 
           MV      .L2X    A4,B4             ; |1041| 
           MV      .S1     A14,A5            ; |1041| 
           ADDKPC  .S2     RL520,B3,0        ; |1041| 
           MV      .L1X    B12,A4            ; |1041| 
RL520:     ; CALL OCCURS {__addd}            ; |1041| 
           MVKL    .S1     __mpyd,A3         ; |1041| 
           MVKH    .S1     __mpyd,A3         ; |1041| 
           LDW     .D2T2   *+SP(436),B5      ; |1041| 
           CALL    .S2X    A3                ; |1041| 
           LDW     .D2T2   *+SP(440),B4      ; |1041| 
           MV      .D1     A5,A14            ; |1041| 
           MV      .L2X    A4,B12            ; |1041| 
           MV      .L1X    B11,A4            ; |1041| 

           ADDKPC  .S2     RL521,B3,0        ; |1041| 
||         MV      .S1     A13,A5            ; |1041| 

RL521:     ; CALL OCCURS {__mpyd}            ; |1041| 
DW$L$_OP_HMatrixCalulation$19$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$20$B:
           MVKL    .S1     __addd,A3         ; |1041| 
           MVKH    .S1     __addd,A3         ; |1041| 
           MV      .L2X    A4,B4             ; |1041| 
           CALL    .S2X    A3                ; |1041| 
           ADDKPC  .S2     RL522,B3,1        ; |1041| 
           MV      .S1X    B12,A4            ; |1041| 
           MV      .L2X    A5,B5             ; |1041| 
           MV      .L1     A14,A5            ; |1041| 
RL522:     ; CALL OCCURS {__addd}            ; |1041| 
DW$L$_OP_HMatrixCalulation$20$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$21$B:
           MVKL    .S1     __divd,A3         ; |1041| 
           MVKH    .S1     __divd,A3         ; |1041| 
           LDW     .D2T2   *+SP(444),B5      ; |1041| 
           CALL    .S2X    A3                ; |1041| 
           LDW     .D2T2   *+SP(448),B4      ; |1041| 
           ADDKPC  .S2     RL523,B3,3        ; |1041| 
RL523:     ; CALL OCCURS {__divd}            ; |1041| 
           MVK     .S1     307,A3            ; |1042| 

           ADD     .L1     A3,A12,A14        ; |1042| 
||         MVKL    .S2     __mpyd,B6         ; |1042| 

           SUB     .D1     A14,27,A3         ; |1042| 
||         MVKH    .S2     __mpyd,B6         ; |1042| 

           CALL    .S2     B6                ; |1042| 
||         LDDW    .D1T1   *A3,A7:A6         ; |1042| 

           MVKL    .S2     0x3fc85b8b,B5     ; |1042| 
           MVKL    .S2     0x6a70079,B4      ; |1042| 
           MVKH    .S2     0x3fc85b8b,B5     ; |1042| 

           MVKH    .S2     0x6a70079,B4      ; |1042| 
||         STW     .D2T1   A4,*+SP(452)      ; |1041| 

           ADDKPC  .S2     RL538,B3,0        ; |1042| 
||         MV      .L1     A7,A5             ; |1042| 
||         MV      .S1     A6,A4             ; |1042| 
||         STW     .D2T1   A5,*+SP(456)      ; |1041| 

RL538:     ; CALL OCCURS {__mpyd}            ; |1042| 
           MVKL    .S1     __addd,A3         ; |1042| 
           MVKH    .S1     __addd,A3         ; |1042| 

           LDDW    .D2T2   *+B10(56),B5:B4   ; |1042| 
||         MV      .L2X    A4,B13            ; |1042| 

           CALL    .S2X    A3                ; |1042| 
||         LDW     .D2T1   *+SP(452),A4      ; |1042| 

           LDW     .D2T1   *+SP(456),A5      ; |1042| 
||         MV      .L2X    A5,B12            ; |1042| 

           ADDKPC  .S2     RL539,B3,3        ; |1042| 
RL539:     ; CALL OCCURS {__addd}            ; |1042| 
           MVK     .S1     340,A3            ; |1042| 
           LDB     .D1T1   *+A3[A12],A3      ; |1042| 
           NOP             3
           MV      .D1     A4,A12            ; |1042| 
           SHL     .S1     A3,7,A6           ; |1042| 
           ADDAD   .D1     A6,A3,A3          ; |1042| 

           MVKL    .S1     __cvtfd,A3        ; |1042| 
||         ADD     .L1     A10,A3,A4         ; |1042| 

           MVKH    .S1     __cvtfd,A3        ; |1042| 
           LDW     .D1T1   *A4,A4            ; |1042| 
           CALL    .S2X    A3                ; |1042| 
           MV      .L1     A5,A10            ; |1042| 
           ADDKPC  .S2     RL540,B3,3        ; |1042| 
RL540:     ; CALL OCCURS {__cvtfd}           ; |1042| 
           MVKL    .S2     __mpyd,B6         ; |1042| 
           MVKH    .S2     __mpyd,B6         ; |1042| 
           CALL    .S2     B6                ; |1042| 
           MVKL    .S2     0x41b1de78,B5     ; |1042| 
           ZERO    .L2     B4                ; |1042| 
           MVKH    .S2     0x41b1de78,B5     ; |1042| 
           MVKH    .S2     0x4a000000,B4     ; |1042| 
           ADDKPC  .S2     RL541,B3,0        ; |1042| 
RL541:     ; CALL OCCURS {__mpyd}            ; |1042| 
           MVKL    .S2     __subd,B6         ; |1042| 
           MVKH    .S2     __subd,B6         ; |1042| 
           CALL    .S2     B6                ; |1042| 
           MV      .L2X    A4,B4             ; |1042| 
           MV      .L2X    A5,B5             ; |1042| 
           MV      .S1     A12,A4            ; |1042| 
           ADDKPC  .S2     RL542,B3,0        ; |1042| 
           MV      .L1     A10,A5            ; |1042| 
RL542:     ; CALL OCCURS {__subd}            ; |1042| 
           MVKL    .S1     __addd,A3         ; |1042| 
           MVKH    .S1     __addd,A3         ; |1042| 
           ZERO    .L2     B5:B4             ; |1042| 
           CALL    .S2X    A3                ; |1042| 
           ADDKPC  .S2     RL543,B3,4        ; |1042| 
RL543:     ; CALL OCCURS {__addd}            ; |1042| 
           MVKL    .S1     __subd,A3         ; |1042| 
           MVKH    .S1     __subd,A3         ; |1042| 
           MV      .L2X    A4,B4             ; |1042| 
           CALL    .S2X    A3                ; |1042| 
           MV      .L1X    B13,A4            ; |1042| 
           MV      .L2X    A5,B5             ; |1042| 
           ADDKPC  .S2     RL544,B3,1        ; |1042| 
           MV      .L1X    B12,A5            ; |1042| 
RL544:     ; CALL OCCURS {__subd}            ; |1042| 
DW$L$_OP_HMatrixCalulation$21$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$22$B:

           LDW     .D2T1   *+SP(392),A3      ; |1042| 
||         MVK     .S1     19539,A31         ; |1057| 

           LDW     .D2T1   *+SP(352),A30     ; |1042| 
           NOP             3
           STDW    .D1T1   A5:A4,*A3         ; |1042| 
           LDB     .D1T1   *+A30[A31],A3     ; |1057| 
           NOP             4

           MVKL    .S1     __cmpd,A3         ; |1059| 
||         CMPEQ   .L1     A3,2,A0           ; |1057| 

   [!A0]   B       .S2     L43               ; |1057| 
||         MVKH    .S1     __cmpd,A3         ; |1059| 
|| [ A0]   LDDW    .D1T1   *A15,A5:A4        ; |1059| 
|| [!A0]   LDDW    .D2T2   *+B10(32),B7:B6   ; |1067| 

   [ A0]   MVKL    .S2     0x40e86a00,B5     ; |1059| 
|| [!A0]   MVKL    .S1     __mpyd,A3         ; |1067| 
|| [!A0]   LDDW    .D2T2   *+B10(24),B5:B4   ; |1067| 

   [ A0]   CALL    .S2X    A3                ; |1059| 
|| [!A0]   MVKH    .S1     __mpyd,A3         ; |1067| 
|| [!A0]   LDDW    .D2T2   *+B10(40),B13:B12 ; |1067| 

           NOP             1
   [!A0]   CALL    .S2X    A3                ; |1067| 
           NOP             1
           ; BRANCHCC OCCURS {L43}           ; |1057| 
DW$L$_OP_HMatrixCalulation$22$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$23$B:
           MVKH    .S2     0x40e86a00,B5     ; |1059| 

           ADDKPC  .S2     RL545,B3,0        ; |1059| 
||         ZERO    .L2     B4                ; |1059| 
||         CLR     .S1     A5,31,31,A5       ; |1059| 

RL545:     ; CALL OCCURS {__cmpd}            ; |1059| 
DW$L$_OP_HMatrixCalulation$23$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$24$B:
           CMPGT   .L1     A4,0,A0           ; |1059| 

   [ A0]   LDW     .D2T2   *+SP(376),B4
|| [ A0]   B       .S2     L46               ; |1059| 
|| [!A0]   MVKL    .S1     __mpyd,A3         ; |1067| 
|| [ A0]   MVK     .L1     1,A3              ; |1061| 

   [!A0]   MVKH    .S1     __mpyd,A3         ; |1067| 
|| [!A0]   LDDW    .D2T2   *+B10(24),B5:B4   ; |1067| 
|| [ A0]   STB     .D1T1   A3,*+A14(25)      ; |1061| 

           NOP             3
   [ A0]   SUB     .L1X    B4,1,A0           ; |975| 
           ; BRANCHCC OCCURS {L46}           ; |1059| 
DW$L$_OP_HMatrixCalulation$24$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$25$B:

           LDDW    .D2T2   *+B10(32),B7:B6   ; |1067| 
||         CALL    .S2X    A3                ; |1067| 

           LDDW    .D2T2   *+B10(40),B13:B12 ; |1067| 
DW$L$_OP_HMatrixCalulation$25$E:
;** --------------------------------------------------------------------------*
L43:    
DW$L$_OP_HMatrixCalulation$26$B:
           MV      .L1X    B4,A4             ; |1067| 
           MV      .L1X    B5,A5             ; |1067| 
           ADDKPC  .S2     RL563,B3,0        ; |1067| 

           MV      .L1X    B7,A12            ; |1067| 
||         STW     .D2T2   B6,*+SP(392)      ; |1067| 

RL563:     ; CALL OCCURS {__mpyd}            ; |1067| 
DW$L$_OP_HMatrixCalulation$26$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$27$B:
           MVKL    .S2     __mpyd,B6         ; |1067| 
           MVKH    .S2     __mpyd,B6         ; |1067| 

           CALL    .S2     B6                ; |1067| 
||         LDW     .D2T1   *+SP(392),A4      ; |1067| 
||         MV      .L1     A4,A10            ; |1067| 

           MV      .L2X    A12,B5            ; |1067| 
           ADDKPC  .S2     RL564,B3,0        ; |1067| 
           MV      .S1     A5,A15            ; |1067| 
           MV      .L1     A12,A5            ; |1067| 
           MV      .L2X    A4,B4             ; |1067| 
RL564:     ; CALL OCCURS {__mpyd}            ; |1067| 
           MVKL    .S1     __addd,A3         ; |1067| 
           MVKH    .S1     __addd,A3         ; |1067| 
           MV      .L2X    A4,B4             ; |1067| 
           CALL    .S2X    A3                ; |1067| 
           ADDKPC  .S2     RL565,B3,1        ; |1067| 
           MV      .S1     A10,A4            ; |1067| 
           MV      .L2X    A5,B5             ; |1067| 
           MV      .L1     A15,A5            ; |1067| 
RL565:     ; CALL OCCURS {__addd}            ; |1067| 
           MVKL    .S1     __mpyd,A3         ; |1067| 
           MVKH    .S1     __mpyd,A3         ; |1067| 
           MV      .S1     A5,A12            ; |1067| 
           CALL    .S2X    A3                ; |1067| 
           MV      .L1X    B13,A5            ; |1067| 
           MV      .D2     B12,B4            ; |1067| 
           MV      .L2     B13,B5            ; |1067| 
           MV      .S1     A4,A15            ; |1067| 

           MV      .L1X    B12,A4            ; |1067| 
||         ADDKPC  .S2     RL566,B3,0        ; |1067| 

RL566:     ; CALL OCCURS {__mpyd}            ; |1067| 
           MVKL    .S1     __addd,A3         ; |1067| 
           MVKH    .S1     __addd,A3         ; |1067| 
           MV      .L2X    A4,B4             ; |1067| 
           CALL    .S2X    A3                ; |1067| 
           MV      .L2X    A5,B5             ; |1067| 
           MV      .S1     A15,A4            ; |1067| 
           ADDKPC  .S2     RL567,B3,1        ; |1067| 
           MV      .L1     A12,A5            ; |1067| 
RL567:     ; CALL OCCURS {__addd}            ; |1067| 
           MVKL    .S2     _sqrt,B4          ; |1067| 
           MVKH    .S2     _sqrt,B4          ; |1067| 
           CALL    .S2     B4                ; |1067| 
           ADDKPC  .S2     RL568,B3,4        ; |1067| 
RL568:     ; CALL OCCURS {_sqrt}             ; |1067| 

           LDW     .D2T2   *+SP(448),B4      ; |1069| 
||         MVKL    .S2     __mpyd,B6         ; |1069| 

           LDW     .D2T2   *+SP(444),B5      ; |1069| 
||         MVKH    .S2     __mpyd,B6         ; |1069| 

           LDW     .D2T1   *+SP(384),A5      ; |1069| 
||         CALL    .S2     B6                ; |1069| 
||         MV      .L1     A5,A12            ; |1067| 

           LDW     .D2T1   *+SP(388),A4      ; |1069| 
||         MV      .L1     A4,A15            ; |1067| 

           ADDKPC  .S2     RL583,B3,3        ; |1069| 
RL583:     ; CALL OCCURS {__mpyd}            ; |1069| 
           MVKL    .S1     __mpyd,A3         ; |1069| 
           MVKH    .S1     __mpyd,A3         ; |1069| 
           MV      .L2X    A15,B4            ; |1069| 
           CALL    .S2X    A3                ; |1069| 
           ADDKPC  .S2     RL584,B3,3        ; |1069| 
           MV      .L2X    A12,B5            ; |1069| 
RL584:     ; CALL OCCURS {__mpyd}            ; |1069| 
           MVKL    .S1     __mpyd,A3         ; |1069| 
           MVKH    .S1     __mpyd,A3         ; |1069| 
           LDW     .D2T2   *+SP(404),B5      ; |1069| 

           LDDW    .D2T1   *+B10(24),A7:A6   ; |1069| 
||         CALL    .S2X    A3                ; |1069| 

           LDW     .D2T2   *+SP(408),B4      ; |1069| 
           MV      .L2X    A4,B13            ; |1069| 
           MV      .L2X    A5,B12            ; |1069| 
           ADDKPC  .S2     RL585,B3,0        ; |1069| 

           MV      .S1     A6,A4             ; |1069| 
||         MV      .L1     A7,A5             ; |1069| 

RL585:     ; CALL OCCURS {__mpyd}            ; |1069| 
           MVKL    .S2     __mpyd,B6         ; |1069| 

           MVKH    .S2     __mpyd,B6         ; |1069| 
||         LDW     .D2T2   *+SP(412),B5      ; |1069| 

           LDDW    .D2T1   *+B10(32),A7:A6   ; |1069| 
||         CALL    .S2     B6                ; |1069| 

           LDW     .D2T2   *+SP(416),B4      ; |1069| 
           MV      .L1     A4,A15            ; |1069| 
           MV      .D1     A5,A12            ; |1069| 
           ADDKPC  .S2     RL586,B3,0        ; |1069| 

           MV      .S1     A6,A4             ; |1069| 
||         MV      .L1     A7,A5             ; |1069| 

RL586:     ; CALL OCCURS {__mpyd}            ; |1069| 
           MVKL    .S2     __addd,B6         ; |1069| 
           MVKH    .S2     __addd,B6         ; |1069| 
           CALL    .S2     B6                ; |1069| 
           MV      .L2X    A4,B4             ; |1069| 
           MV      .L2X    A5,B5             ; |1069| 
           MV      .S1     A15,A4            ; |1069| 
           ADDKPC  .S2     RL587,B3,0        ; |1069| 
           MV      .L1     A12,A5            ; |1069| 
RL587:     ; CALL OCCURS {__addd}            ; |1069| 
DW$L$_OP_HMatrixCalulation$27$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$28$B:
           MVKL    .S2     __mpyd,B8         ; |1069| 

           LDDW    .D2T2   *+B10(40),B7:B6   ; |1069| 
||         MVKH    .S2     __mpyd,B8         ; |1069| 

           CALL    .S2     B8                ; |1069| 
           MV      .S1     A4,A15            ; |1069| 
           ADDKPC  .S2     RL588,B3,0        ; |1069| 
           MV      .L2X    A13,B5            ; |1069| 
           MV      .L1X    B6,A4             ; |1069| 

           MV      .D2     B11,B4            ; |1069| 
||         MV      .S1     A5,A12            ; |1069| 
||         MV      .L1X    B7,A5             ; |1069| 

RL588:     ; CALL OCCURS {__mpyd}            ; |1069| 
DW$L$_OP_HMatrixCalulation$28$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$29$B:
           MVKL    .S1     __addd,A3         ; |1069| 
           MVKH    .S1     __addd,A3         ; |1069| 
           MV      .L2X    A4,B4             ; |1069| 
           CALL    .S2X    A3                ; |1069| 
           ADDKPC  .S2     RL589,B3,1        ; |1069| 
           MV      .S1     A15,A4            ; |1069| 
           MV      .L2X    A5,B5             ; |1069| 
           MV      .L1     A12,A5            ; |1069| 
RL589:     ; CALL OCCURS {__addd}            ; |1069| 
           MVKL    .S2     __cmpd,B6         ; |1069| 
           MVKH    .S2     __cmpd,B6         ; |1069| 
           CALL    .S2     B6                ; |1069| 
           MV      .L2X    A4,B4             ; |1069| 
           MV      .L1X    B13,A4            ; |1069| 
           MV      .L2X    A5,B5             ; |1069| 
           ADDKPC  .S2     RL590,B3,0        ; |1069| 
           MV      .L1X    B12,A5            ; |1069| 
RL590:     ; CALL OCCURS {__cmpd}            ; |1069| 
DW$L$_OP_HMatrixCalulation$29$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$30$B:

           MVKL    .S1     __divd,A3         ; |1080| 
||         CMPGT   .L1     A4,0,A0           ; |1069| 

           MVKH    .S1     __divd,A3         ; |1080| 
|| [ A0]   B       .S2     L44               ; |1069| 
|| [!A0]   LDW     .D2T2   *+SP(444),B5      ; |1080| 
|| [!A0]   ZERO    .L1     A5                ; |1080| 

   [!A0]   LDW     .D2T2   *+SP(448),B4      ; |1080| 
|| [ A0]   MVK     .L1     1,A3              ; |1071| 

   [!A0]   CALL    .S2X    A3                ; |1080| 
|| [ A0]   STB     .D1T1   A3,*+A14(26)      ; |1071| 

           NOP             3
           ; BRANCHCC OCCURS {L44}           ; |1069| 
DW$L$_OP_HMatrixCalulation$30$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$31$B:
           ADDKPC  .S2     RL591,B3,0        ; |1080| 

           ZERO    .L1     A4                ; |1080| 
||         MVKH    .S1     0x3ff00000,A5     ; |1080| 

RL591:     ; CALL OCCURS {__divd}            ; |1080| 
DW$L$_OP_HMatrixCalulation$31$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$32$B:
           MVKL    .S1     __negd,A3         ; |1080| 
           MVKH    .S1     __negd,A3         ; |1080| 
           MV      .L1     A5,A15            ; |1080| 

           CALL    .S2X    A3                ; |1080| 
||         LDW     .D2T1   *+SP(404),A5      ; |1080| 

           LDW     .D2T1   *+SP(408),A4      ; |1080| 
||         MV      .L2X    A4,B12            ; |1080| 

           ADDKPC  .S2     RL593,B3,3        ; |1080| 
RL593:     ; CALL OCCURS {__negd}            ; |1080| 
           MVKL    .S1     __mpyd,A3         ; |1080| 
           MVKH    .S1     __mpyd,A3         ; |1080| 
           MV      .L2X    A15,B5            ; |1080| 
           CALL    .S2X    A3                ; |1080| 
           ADDKPC  .S2     RL594,B3,3        ; |1080| 
           MV      .D2     B12,B4            ; |1080| 
RL594:     ; CALL OCCURS {__mpyd}            ; |1080| 

           SHL     .S2X    A11,7,B4          ; |1080| 
||         LDW     .D2T1   *+SP(368),A7      ; |1080| 

           SHL     .S1     A11,6,A3          ; |1080| 

           MVKL    .S1     __negd,A3         ; |1081| 
||         ADD     .L1X    A3,B4,A6          ; |1080| 

           MVKH    .S1     __negd,A3         ; |1081| 
           NOP             1

           LDW     .D2T1   *+SP(416),A4      ; |1081| 
||         MV      .L1     A4,A6             ; |1080| 
||         CALL    .S2X    A3                ; |1081| 
||         ADD     .S1     A7,A6,A8          ; |1080| 

           LDW     .D2T1   *+SP(412),A5      ; |1080| 
||         MV      .L1     A5,A7             ; |1080| 

           ADDKPC  .S2     RL596,B3,0        ; |1081| 
           STDW    .D1T1   A7:A6,*A8         ; |1080| 
           NOP             2
RL596:     ; CALL OCCURS {__negd}            ; |1081| 
           MVKL    .S1     __mpyd,A3         ; |1081| 
           MVKH    .S1     __mpyd,A3         ; |1081| 
           MV      .D2     B12,B4            ; |1081| 
           CALL    .S2X    A3                ; |1081| 
           MV      .L2X    A15,B5            ; |1081| 
           ADDKPC  .S2     RL597,B3,3        ; |1081| 
RL597:     ; CALL OCCURS {__mpyd}            ; |1081| 
           SHL     .S1     A11,6,A3          ; |1081| 
           SHL     .S2X    A11,7,B4          ; |1081| 

           MVKL    .S1     __negd,A3         ; |1082| 
||         ADD     .L2X    A3,B4,B4          ; |1081| 

           MVKH    .S1     __negd,A3         ; |1082| 
           LDW     .D2T1   *+SP(368),A6      ; |1082| 
           CALL    .S2X    A3                ; |1082| 
           MV      .D1     A5,A7             ; |1081| 
           MV      .S1     A13,A5            ; |1082| 
           ADDKPC  .S2     RL599,B3,0        ; |1082| 

           MV      .L1     A4,A6             ; |1081| 
||         ADD     .S1X    A6,B4,A12         ; |1081| 

           STDW    .D1T1   A7:A6,*+A12(8)    ; |1081| 
||         MV      .L1X    B11,A4            ; |1082| 

RL599:     ; CALL OCCURS {__negd}            ; |1082| 
           MVKL    .S2     __mpyd,B6         ; |1082| 
           MVKH    .S2     __mpyd,B6         ; |1082| 
           CALL    .S2     B6                ; |1082| 
           MV      .D2     B12,B4            ; |1082| 
           MV      .L2X    A15,B5            ; |1082| 
           ADDKPC  .S2     RL600,B3,2        ; |1082| 
RL600:     ; CALL OCCURS {__mpyd}            ; |1082| 
           MVKL    .S2     __mpyd,B6         ; |1084| 
           MVKH    .S2     __mpyd,B6         ; |1084| 

           LDW     .D2T2   *+SP(456),B5      ; |1082| 
||         CALL    .S2     B6                ; |1084| 

           LDW     .D2T2   *+SP(452),B4      ; |1084| 
           MV      .S1     A4,A6             ; |1082| 
           MV      .L1     A5,A7             ; |1082| 
           MV      .S1     A15,A5            ; |1084| 

           MV      .L1X    B12,A4            ; |1084| 
||         STDW    .D1T1   A7:A6,*+A12(16)   ; |1082| 
||         ADDKPC  .S2     RL601,B3,0        ; |1084| 

RL601:     ; CALL OCCURS {__mpyd}            ; |1084| 
           MVKL    .S2     __negd,B4         ; |1084| 
           MVKH    .S2     __negd,B4         ; |1084| 
           CALL    .S2     B4                ; |1084| 
           MV      .D1     A4,A10            ; |1084| 
           MV      .L2X    A5,B13            ; |1084| 
           MV      .L1     A15,A5            ; |1084| 
           ADDKPC  .S2     RL602,B3,0        ; |1084| 
           MV      .S1X    B12,A4            ; |1084| 
RL602:     ; CALL OCCURS {__negd}            ; |1084| 
           MVKL    .S2     __mpyd,B6         ; |1084| 
           MVKH    .S2     __mpyd,B6         ; |1084| 

           LDW     .D2T2   *+SP(408),B4      ; |1084| 
||         CALL    .S2     B6                ; |1084| 

           LDW     .D2T2   *+SP(404),B5      ; |1084| 
           MV      .D1     A5,A15            ; |1084| 
           MV      .L2X    A4,B12            ; |1084| 
           MV      .S1     A10,A4            ; |1084| 

           ADDKPC  .S2     RL606,B3,0        ; |1084| 
||         MV      .L1X    B13,A5            ; |1084| 

RL606:     ; CALL OCCURS {__mpyd}            ; |1084| 
           MVKL    .S2     __subd,B6         ; |1084| 
           MVKH    .S2     __subd,B6         ; |1084| 

           LDW     .D2T1   *+SP(420),A5      ; |1084| 
||         MV      .L2X    A5,B5             ; |1084| 
||         CALL    .S2     B6                ; |1084| 

           LDW     .D2T1   *+SP(424),A4      ; |1084| 
||         MV      .L2X    A4,B4             ; |1084| 

           ADDKPC  .S2     RL607,B3,3        ; |1084| 
RL607:     ; CALL OCCURS {__subd}            ; |1084| 
DW$L$_OP_HMatrixCalulation$32$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$33$B:
           MVKL    .S2     __mpyd,B6         ; |1084| 
           MVKH    .S2     __mpyd,B6         ; |1084| 
           CALL    .S2     B6                ; |1084| 
           ADDKPC  .S2     RL608,B3,2        ; |1084| 
           MV      .L2X    A15,B5            ; |1084| 
           MV      .D2     B12,B4            ; |1084| 
RL608:     ; CALL OCCURS {__mpyd}            ; |1084| 
DW$L$_OP_HMatrixCalulation$33$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$34$B:
           MVKL    .S1     __mpyd,A3         ; |1085| 
           MVKH    .S1     __mpyd,A3         ; |1085| 
           LDW     .D2T2   *+SP(412),B5      ; |1084| 
           CALL    .S2X    A3                ; |1085| 
           LDW     .D2T2   *+SP(416),B4      ; |1085| 
           MV      .L1     A4,A6             ; |1084| 
           MV      .S1     A5,A7             ; |1084| 
           ADDKPC  .S2     RL612,B3,0        ; |1085| 

           STDW    .D1T1   A7:A6,*+A12(24)   ; |1084| 
||         MV      .L1X    B13,A5            ; |1085| 
||         MV      .S1     A10,A4            ; |1085| 

RL612:     ; CALL OCCURS {__mpyd}            ; |1085| 
           MVKL    .S1     __subd,A3         ; |1085| 
           MVKH    .S1     __subd,A3         ; |1085| 
           MV      .L2X    A5,B5             ; |1085| 

           CALL    .S2X    A3                ; |1085| 
||         LDW     .D2T1   *+SP(428),A5      ; |1085| 

           LDW     .D2T1   *+SP(432),A4      ; |1085| 
||         MV      .L2X    A4,B4             ; |1085| 

           ADDKPC  .S2     RL613,B3,3        ; |1085| 
RL613:     ; CALL OCCURS {__subd}            ; |1085| 
           MVKL    .S1     __mpyd,A3         ; |1085| 
           MVKH    .S1     __mpyd,A3         ; |1085| 
           MV      .L2X    A15,B5            ; |1085| 
           CALL    .S2X    A3                ; |1085| 
           ADDKPC  .S2     RL614,B3,3        ; |1085| 
           MV      .D2     B12,B4            ; |1085| 
RL614:     ; CALL OCCURS {__mpyd}            ; |1085| 
           MVKL    .S1     __mpyd,A3         ; |1086| 
           MVKH    .S1     __mpyd,A3         ; |1086| 
           MV      .S1     A5,A7             ; |1085| 
           CALL    .S2X    A3                ; |1086| 
           MV      .D2     B11,B4            ; |1086| 
           MV      .L2X    A13,B5            ; |1086| 
           MV      .L1X    B13,A5            ; |1086| 
           MV      .L1     A4,A6             ; |1085| 

           MV      .S1     A10,A4            ; |1086| 
||         STDW    .D1T1   A7:A6,*+A12(32)   ; |1085| 
||         ADDKPC  .S2     RL618,B3,0        ; |1086| 

RL618:     ; CALL OCCURS {__mpyd}            ; |1086| 
           MVKL    .S2     __subd,B6         ; |1086| 
           MVKH    .S2     __subd,B6         ; |1086| 

           LDW     .D2T1   *+SP(436),A5      ; |1086| 
||         MV      .L2X    A5,B5             ; |1086| 
||         CALL    .S2     B6                ; |1086| 

           LDW     .D2T1   *+SP(440),A4      ; |1086| 
||         MV      .L2X    A4,B4             ; |1086| 

           ADDKPC  .S2     RL619,B3,3        ; |1086| 
RL619:     ; CALL OCCURS {__subd}            ; |1086| 
           MVKL    .S2     __mpyd,B6         ; |1086| 
           MVKH    .S2     __mpyd,B6         ; |1086| 
           CALL    .S2     B6                ; |1086| 
           MV      .D2     B12,B4            ; |1086| 
           MV      .L2X    A15,B5            ; |1086| 
           ADDKPC  .S2     RL620,B3,2        ; |1086| 
RL620:     ; CALL OCCURS {__mpyd}            ; |1086| 
DW$L$_OP_HMatrixCalulation$34$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_HMatrixCalulation$35$B:

           BNOP    .S2     L45,2             ; |1094| 
||         ZERO    .L1     A7                ; |1089| 
||         ZERO    .S1     A6                ; |1089| 
||         ZERO    .L2     B5:B4             ; |1088| 
||         STDW    .D1T1   A5:A4,*+A12(40)   ; |1086| 

           MVKH    .S1     0x3ff00000,A7     ; |1089| 
||         STDW    .D1T2   B5:B4,*+A12(48)   ; |1088| 
||         MVK     .L1     1,A3              ; |1092| 

           STDW    .D1T1   A7:A6,*+A12(56)   ; |1089| 
||         LDW     .D2T2   *+SP(376),B4
||         ADD     .L1     1,A11,A11         ; |1094| 

           STB     .D1T1   A3,*+A14(27)      ; |1092| 
           ; BRANCH OCCURS {L45}             ; |1094| 
DW$L$_OP_HMatrixCalulation$35$E:
;** --------------------------------------------------------------------------*
L44:    
DW$L$_OP_HMatrixCalulation$36$B:
           LDW     .D2T2   *+SP(376),B4
DW$L$_OP_HMatrixCalulation$36$E:
;** --------------------------------------------------------------------------*
L45:    
DW$L$_OP_HMatrixCalulation$37$B:
           NOP             4
           SUB     .L1X    B4,1,A0           ; |975| 
DW$L$_OP_HMatrixCalulation$37$E:
;** --------------------------------------------------------------------------*
L46:    
DW$L$_OP_HMatrixCalulation$38$B:

           SUB     .L2     B4,1,B4           ; |975| 
|| [ A0]   B       .S1     L40               ; |975| 

           STW     .D2T2   B4,*+SP(376)      ; |975| 
DW$L$_OP_HMatrixCalulation$38$E:
;** --------------------------------------------------------------------------*
L47:    
DW$L$_OP_HMatrixCalulation$39$B:
           NOP             1

   [!A0]   MVK     .S1     192,A3
|| [!A0]   LDW     .D2T1   *+SP(368),A6      ; |1102| 
||         CMPGT   .L1     A11,0,A0          ; |1100| 

	.dwpsn	"OPManager.c",1095,0
           NOP             2
           ; BRANCHCC OCCURS {L40}           ; |975| 
DW$L$_OP_HMatrixCalulation$39$E:
;** --------------------------------------------------------------------------*

   [!A0]   BNOP    .S2     L51,1             ; |1100| 
||         MPYLI   .M1     A3,A11,A5:A4
||         MVK     .S1     1152,A8           ; |1097| 
||         ZERO    .D2     B5
||         ADD     .L1     A11,A11,A7        ; |1097| 
||         MVK     .D1     8,A30             ; |1097| 
||         ZERO    .L2     B7:B6

           MVK     .S1     1153,A31          ; |1097| 
||         STW     .D1T1   A7,*+A6[A8]       ; |1097| 
||         MVKH    .S2     0x3ff00000,B5
||         ZERO    .L2     B4

           STW     .D1T1   A30,*+A6[A31]     ; |1097| 
           ADD     .L1     A6,A4,A4
           NOP             1
           ; BRANCHCC OCCURS {L51}           ; |1100| 
;** --------------------------------------------------------------------------*

           SUB     .L1     A11,1,A0
||         MVK     .S1     0x1,A1            ; init prolog collapse predicate
||         ADD     .D1     8,A4,A3
||         MV      .L2X    A4,B8

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 1100
;*      Loop opening brace source line   : 1101
;*      Loop closing brace source line   : 1112
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 18
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound(*)    : 6
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        1     
;*      .D units                     5        6*    
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             6*       5     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        1     
;*      Bound(.L .S .D .LS .LSD)     2        3     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 18 Address increment too large
;*         ii = 18 Address increment too large
;*         ii = 18 Address increment too large
;*         ii = 18 Did not find schedule
;*         ii = 19 Address increment too large
;*         ii = 19 Address increment too large
;*         ii = 19 Address increment too large
;*         ii = 19 Did not find schedule
;*         ii = 20 Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop is interruptible
;*      Epilog not removed
;*      Collapsed epilog stages     : 0
;*      Collapsed prolog stages     : 1
;*      Minimum required memory pad : 0 bytes
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L48:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L49:    ; PIPED LOOP KERNEL
DW$L$_OP_HMatrixCalulation$43$B:
	.dwpsn	"OPManager.c",1101,0
   [!A1]   STDW    .D2T2   B7:B6,*B8++(192)  ; |1102| <0,1> 
   [!A1]   STDW    .D1T2   B7:B6,*-A3(184)   ; |1104| <0,2> 
   [!A1]   LDDW    .D1T1   *A6++(192),A5:A4  ; |1106| <0,3>  ^ 
           NOP             4
   [!A1]   STDW    .D2T1   A5:A4,*-B8(168)   ; |1106| <0,8>  ^ 
   [!A1]   LDDW    .D1T1   *-A6(184),A5:A4   ; |1107| <0,9>  ^ 
           NOP             4
   [!A1]   STDW    .D2T1   A5:A4,*-B8(160)   ; |1107| <0,14>  ^ 

   [ A0]   BDEC    .S1     L49,A0            ; |1100| <0,15> 
|| [!A1]   LDDW    .D1T1   *-A6(176),A5:A4   ; |1108| <0,15>  ^ 

           NOP             2
   [!A1]   STDW    .D2T2   B5:B4,*-B8(144)   ; |1110| <0,18> 
   [!A1]   STDW    .D2T2   B7:B6,*-B8(136)   ; |1111| <0,19> 
	.dwpsn	"OPManager.c",1112,0

   [ A1]   SUB     .L1     A1,1,A1           ; <0,20> 
|| [!A1]   STDW    .D2T1   A5:A4,*-B8(152)   ; |1108| <0,20>  ^ 
||         STDW    .D1T2   B7:B6,*A3++(192)  ; |1103| <1,0> 

DW$L$_OP_HMatrixCalulation$43$E:
;** --------------------------------------------------------------------------*
L50:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
           STDW    .D2T2   B7:B6,*B8++(192)  ; |1102| (E) <1,1> 
           STDW    .D1T2   B7:B6,*-A3(184)   ; |1104| (E) <1,2> 
           LDDW    .D1T1   *A6++(192),A5:A4  ; |1106| (E) <1,3>  ^ 
           NOP             4
           STDW    .D2T1   A5:A4,*-B8(168)   ; |1106| (E) <1,8>  ^ 
           LDDW    .D1T1   *-A6(184),A5:A4   ; |1107| (E) <1,9>  ^ 
           NOP             4
           STDW    .D2T1   A5:A4,*-B8(160)   ; |1107| (E) <1,14>  ^ 

           STDW    .D2T2   B7:B6,*-B8(136)   ; |1111| (E) <1,19> 
||         LDDW    .D1T1   *-A6(176),A5:A4   ; |1108| (E) <1,15>  ^ 

           NOP             3
           STDW    .D2T2   B5:B4,*-B8(144)   ; |1110| (E) <1,18> 
           STDW    .D2T1   A5:A4,*-B8(152)   ; |1108| (E) <1,20>  ^ 
;** --------------------------------------------------------------------------*
L51:    
           LDW     .D2T2   *+SP(484),B3      ; |1115| 
           LDW     .D2T1   *+SP(464),A10     ; |1115| 
           LDW     .D2T1   *+SP(476),A13     ; |1115| 
           LDW     .D2T1   *+SP(472),A12     ; |1115| 
           LDW     .D2T1   *+SP(480),A14     ; |1115| 
           LDW     .D2T2   *+SP(492),B11     ; |1115| 
           LDW     .D2T2   *+SP(488),B10     ; |1115| 
           LDW     .D2T2   *+SP(500),B13     ; |1115| 
           LDW     .D2T2   *+SP(496),B12     ; |1115| 

           RET     .S2     B3                ; |1115| 
||         LDW     .D2T1   *+SP(504),A15     ; |1115| 

           LDW     .D2T1   *+SP(468),A11     ; |1115| 
||         MV      .L1     A11,A4            ; |1114| 

           NOP             3
	.dwpsn	"OPManager.c",1115,1
           ADDK    .S2     504,SP            ; |1115| 
           ; BRANCH OCCURS {B3}              ; |1115| 

DW$181	.dwtag  DW_TAG_loop
	.dwattr DW$181, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L49:1:1472539744")
	.dwattr DW$181, DW_AT_begin_file("OPManager.c")
	.dwattr DW$181, DW_AT_begin_line(0x44c)
	.dwattr DW$181, DW_AT_end_line(0x458)
DW$182	.dwtag  DW_TAG_loop_range
	.dwattr DW$182, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$43$B)
	.dwattr DW$182, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$43$E)
	.dwendtag DW$181


DW$183	.dwtag  DW_TAG_loop
	.dwattr DW$183, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L40:1:1472539744")
	.dwattr DW$183, DW_AT_begin_file("OPManager.c")
	.dwattr DW$183, DW_AT_begin_line(0x3cf)
	.dwattr DW$183, DW_AT_end_line(0x447)
DW$184	.dwtag  DW_TAG_loop_range
	.dwattr DW$184, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$4$B)
	.dwattr DW$184, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$4$E)
DW$185	.dwtag  DW_TAG_loop_range
	.dwattr DW$185, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$25$B)
	.dwattr DW$185, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$25$E)
DW$186	.dwtag  DW_TAG_loop_range
	.dwattr DW$186, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$26$B)
	.dwattr DW$186, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$26$E)
DW$187	.dwtag  DW_TAG_loop_range
	.dwattr DW$187, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$27$B)
	.dwattr DW$187, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$27$E)
DW$188	.dwtag  DW_TAG_loop_range
	.dwattr DW$188, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$28$B)
	.dwattr DW$188, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$28$E)
DW$189	.dwtag  DW_TAG_loop_range
	.dwattr DW$189, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$29$B)
	.dwattr DW$189, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$29$E)
DW$190	.dwtag  DW_TAG_loop_range
	.dwattr DW$190, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$30$B)
	.dwattr DW$190, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$30$E)
DW$191	.dwtag  DW_TAG_loop_range
	.dwattr DW$191, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$31$B)
	.dwattr DW$191, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$31$E)
DW$192	.dwtag  DW_TAG_loop_range
	.dwattr DW$192, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$32$B)
	.dwattr DW$192, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$32$E)
DW$193	.dwtag  DW_TAG_loop_range
	.dwattr DW$193, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$33$B)
	.dwattr DW$193, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$33$E)
DW$194	.dwtag  DW_TAG_loop_range
	.dwattr DW$194, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$34$B)
	.dwattr DW$194, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$34$E)
DW$195	.dwtag  DW_TAG_loop_range
	.dwattr DW$195, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$35$B)
	.dwattr DW$195, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$35$E)
DW$196	.dwtag  DW_TAG_loop_range
	.dwattr DW$196, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$36$B)
	.dwattr DW$196, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$36$E)
DW$197	.dwtag  DW_TAG_loop_range
	.dwattr DW$197, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$13$B)
	.dwattr DW$197, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$13$E)
DW$198	.dwtag  DW_TAG_loop_range
	.dwattr DW$198, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$14$B)
	.dwattr DW$198, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$14$E)
DW$199	.dwtag  DW_TAG_loop_range
	.dwattr DW$199, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$9$B)
	.dwattr DW$199, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$9$E)
DW$200	.dwtag  DW_TAG_loop_range
	.dwattr DW$200, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$10$B)
	.dwattr DW$200, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$10$E)
DW$201	.dwtag  DW_TAG_loop_range
	.dwattr DW$201, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$6$B)
	.dwattr DW$201, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$6$E)
DW$202	.dwtag  DW_TAG_loop_range
	.dwattr DW$202, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$7$B)
	.dwattr DW$202, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$7$E)
DW$203	.dwtag  DW_TAG_loop_range
	.dwattr DW$203, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$8$B)
	.dwattr DW$203, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$8$E)
DW$204	.dwtag  DW_TAG_loop_range
	.dwattr DW$204, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$11$B)
	.dwattr DW$204, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$11$E)
DW$205	.dwtag  DW_TAG_loop_range
	.dwattr DW$205, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$12$B)
	.dwattr DW$205, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$12$E)
DW$206	.dwtag  DW_TAG_loop_range
	.dwattr DW$206, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$15$B)
	.dwattr DW$206, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$15$E)
DW$207	.dwtag  DW_TAG_loop_range
	.dwattr DW$207, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$16$B)
	.dwattr DW$207, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$16$E)
DW$208	.dwtag  DW_TAG_loop_range
	.dwattr DW$208, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$17$B)
	.dwattr DW$208, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$17$E)
DW$209	.dwtag  DW_TAG_loop_range
	.dwattr DW$209, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$18$B)
	.dwattr DW$209, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$18$E)
DW$210	.dwtag  DW_TAG_loop_range
	.dwattr DW$210, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$19$B)
	.dwattr DW$210, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$19$E)
DW$211	.dwtag  DW_TAG_loop_range
	.dwattr DW$211, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$20$B)
	.dwattr DW$211, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$20$E)
DW$212	.dwtag  DW_TAG_loop_range
	.dwattr DW$212, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$21$B)
	.dwattr DW$212, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$21$E)
DW$213	.dwtag  DW_TAG_loop_range
	.dwattr DW$213, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$22$B)
	.dwattr DW$213, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$22$E)
DW$214	.dwtag  DW_TAG_loop_range
	.dwattr DW$214, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$23$B)
	.dwattr DW$214, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$23$E)
DW$215	.dwtag  DW_TAG_loop_range
	.dwattr DW$215, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$5$B)
	.dwattr DW$215, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$5$E)
DW$216	.dwtag  DW_TAG_loop_range
	.dwattr DW$216, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$24$B)
	.dwattr DW$216, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$24$E)
DW$217	.dwtag  DW_TAG_loop_range
	.dwattr DW$217, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$37$B)
	.dwattr DW$217, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$37$E)
DW$218	.dwtag  DW_TAG_loop_range
	.dwattr DW$218, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$38$B)
	.dwattr DW$218, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$38$E)
DW$219	.dwtag  DW_TAG_loop_range
	.dwattr DW$219, DW_AT_low_pc(DW$L$_OP_HMatrixCalulation$39$B)
	.dwattr DW$219, DW_AT_high_pc(DW$L$_OP_HMatrixCalulation$39$E)
	.dwendtag DW$183

	.dwattr DW$170, DW_AT_end_file("OPManager.c")
	.dwattr DW$170, DW_AT_end_line(0x45b)
	.dwattr DW$170, DW_AT_end_column(0x01)
	.dwendtag DW$170

	.sect	".text"

DW$220	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_KF_Gain"), DW_AT_symbol_name("_OP_KF_Gain")
	.dwattr DW$220, DW_AT_low_pc(_OP_KF_Gain)
	.dwattr DW$220, DW_AT_high_pc(0x00)
	.dwattr DW$220, DW_AT_begin_file("OPManager.c")
	.dwattr DW$220, DW_AT_begin_line(0x468)
	.dwattr DW$220, DW_AT_begin_column(0x0d)
	.dwattr DW$220, DW_AT_frame_base[DW_OP_breg31 9280]
	.dwattr DW$220, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",1133,1

;******************************************************************************
;* FUNCTION NAME: _OP_KF_Gain                                                 *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 9236 Auto + 44 Save = 9280 byte             *
;******************************************************************************
_OP_KF_Gain:
;** --------------------------------------------------------------------------*
DW$221	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$221, DW_AT_type(*DW$T$189)
	.dwattr DW$221, DW_AT_location[DW_OP_reg4]
DW$222	.dwtag  DW_TAG_formal_parameter, DW_AT_name("K"), DW_AT_symbol_name("_K")
	.dwattr DW$222, DW_AT_type(*DW$T$171)
	.dwattr DW$222, DW_AT_location[DW_OP_reg20]
DW$223	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Pp"), DW_AT_symbol_name("_Pp")
	.dwattr DW$223, DW_AT_type(*DW$T$171)
	.dwattr DW$223, DW_AT_location[DW_OP_reg6]
DW$224	.dwtag  DW_TAG_formal_parameter, DW_AT_name("H"), DW_AT_symbol_name("_H")
	.dwattr DW$224, DW_AT_type(*DW$T$171)
	.dwattr DW$224, DW_AT_location[DW_OP_reg22]
DW$225	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nMeas"), DW_AT_symbol_name("_nMeas")
	.dwattr DW$225, DW_AT_type(*DW$T$19)
	.dwattr DW$225, DW_AT_location[DW_OP_reg8]
           MVKL    .S1     _CreateMatrixEx,A3 ; |1141| 

           MVKH    .S1     _CreateMatrixEx,A3 ; |1141| 
||         ADDK    .S2     -9280,SP          ; |1133| 
||         MV      .L1X    SP,A31            ; |1133| 

           STW     .D2T2   B11,*+SP(9268)

           CALL    .S2X    A3                ; |1141| 
||         STW     .D2T1   A15,*+SP(9280)

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(9260)

           STW     .D2T2   B10,*+SP(9264)
||         STW     .D1T1   A14,*-A31(24)

           ADD     .L1     A8,A8,A6          ; |1141| 
||         STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .S1X    B4,A15            ; |1133| 
||         MV      .L2X    A6,B11            ; |1133| 
||         STW     .D2T2   B13,*+SP(9276)

           CMPGT   .L1     A8,0,A14          ; |1150| 
||         MV      .S1X    B6,A10            ; |1133| 
||         MV      .D1     A4,A11            ; |1133| 
||         STW     .D2T2   B12,*+SP(9272)

           ADDKPC  .S2     RL621,B3,0        ; |1141| 
||         ADD     .L1X    8,SP,A4           ; |1141| 
||         ADD     .S1     A8,A8,A12         ; |1141| 
||         MV      .D1     A8,A13            ; |1133| 
||         MVK     .L2     0x8,B4            ; |1141| 
||         MV      .D2X    A8,B10            ; |1133| 

RL621:     ; CALL OCCURS {_CreateMatrixEx}   ; |1141| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     _CreateMatrixEx,B5 ; |1142| 
           MVKH    .S2     _CreateMatrixEx,B5 ; |1142| 
           CALL    .S2     B5                ; |1142| 
           MVK     .S1     4624,A3           ; |1142| 
           ADDKPC  .S2     RL622,B3,0        ; |1142| 
           ADD     .L1X    A3,SP,A4          ; |1142| 
           MV      .S1     A12,A6            ; |1142| 
           MV      .L2X    A12,B4            ; |1142| 
RL622:     ; CALL OCCURS {_CreateMatrixEx}   ; |1142| 
           MVKL    .S1     _MultMatrix_B_TC_Ex,A3 ; |1145| 
           MVKH    .S1     _MultMatrix_B_TC_Ex,A3 ; |1145| 
           MV      .L1     A10,A6            ; |1145| 
           CALL    .S2X    A3                ; |1145| 
           ADDKPC  .S2     RL623,B3,2        ; |1145| 
           MV      .L2     B11,B4            ; |1145| 
           ADD     .S1X    8,SP,A4           ; |1145| 
RL623:     ; CALL OCCURS {_MultMatrix_B_TC_Ex}  ; |1145| 
           MVKL    .S2     _MultMatrix_SimResEx,B5 ; |1146| 
           MVKH    .S2     _MultMatrix_SimResEx,B5 ; |1146| 
           CALL    .S2     B5                ; |1146| 
           MVK     .S1     4624,A3           ; |1146| 
           ADD     .L1X    8,SP,A6           ; |1146| 
           MV      .L2X    A10,B4            ; |1146| 
           ADD     .L1X    A3,SP,A4          ; |1146| 
           ADDKPC  .S2     RL624,B3,0        ; |1146| 
RL624:     ; CALL OCCURS {_MultMatrix_SimResEx}  ; |1146| 
;** --------------------------------------------------------------------------*

           MV      .L1     A14,A0            ; |1149| 
||         MVK     .S1     4624,A3
||         MV      .L2X    A12,B11           ; |1149| 

           ADD     .L1X    A3,SP,A10
||         MVK     .S1     14552,A3          ; |1150| 
|| [ A0]   MVKL    .S2     __addd,B6         ; |1150| 

   [!A0]   BNOP    .S1     L53,4             ; |1150| 
||         ADD     .L1     A3,A11,A3         ; |1150| 
|| [ A0]   MVKH    .S2     __addd,B6         ; |1150| 

   [ A0]   LDDW    .D1T1   *A3,A7:A6         ; |1150| 
           ; BRANCHCC OCCURS {L53}           ; |1150| 
;** --------------------------------------------------------------------------*

           CALL    .S2     B6                ; |1150| 
||         LDDW    .D1T1   *A10,A5:A4        ; |1150| 

	.dwpsn	"OPManager.c",1150,0
           NOP             3
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L52:    
DW$L$_OP_KF_Gain$5$B:

           ADDKPC  .S2     RL625,B3,0        ; |1150| 
||         MV      .L2X    A6,B4             ; |1150| 

           MV      .L2X    A7,B5             ; |1150| 
RL625:     ; CALL OCCURS {__addd}            ; |1150| 
DW$L$_OP_KF_Gain$5$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_KF_Gain$6$B:

           STDW    .D1T1   A5:A4,*A10        ; |1150| 
||         SUB     .L2     B10,1,B10         ; |1150| 
||         MVKL    .S2     __addd,B6         ; |1150| 
||         MVK     .S1     14552,A3          ; |1150| 
||         SUB     .L1X    B10,1,A0          ; |1150| 

           ADDK    .S1     200,A10           ; |1150| 
||         MVKH    .S2     __addd,B6         ; |1150| 
||         ADD     .L1     A3,A11,A3         ; |1150| 

   [ A0]   B       .S1     L52               ; |1150| 
|| [ A0]   LDDW    .D1T1   *A3,A7:A6         ; |1150| 

   [ A0]   LDDW    .D1T1   *A10,A5:A4        ; |1150| 
   [ A0]   CALL    .S2     B6                ; |1150| 
           NOP             3
           ; BRANCHCC OCCURS {L52}           ; |1150| 
DW$L$_OP_KF_Gain$6$E:
;** --------------------------------------------------------------------------*
L53:    

           CMPLT   .L1X    A13,B11,A0        ; |1151| 
||         MVK     .S1     200,A3
||         MVK     .S2     4624,B4

   [!A0]   BNOP    .S1     L55,3             ; |1151| 
||         MPYLI   .M1     A3,A13,A5:A4
||         SUB     .L1X    B11,A13,A12       ; |1151| 

           ADD     .L1X    A4,SP,A3

           ADD     .L1X    B4,A3,A10
|| [ A0]   MVK     .S1     14544,A3          ; |1151| 

           ; BRANCHCC OCCURS {L55}           ; |1151| 
;** --------------------------------------------------------------------------*

           MVKL    .S2     __addd,B6         ; |1151| 
||         ADD     .L1     A3,A11,A3         ; |1151| 
||         LDDW    .D1T1   *A10,A5:A4        ; |1151| 

           MVKH    .S2     __addd,B6         ; |1151| 
||         LDDW    .D1T1   *A3,A7:A6         ; |1151| 

           CALL    .S2     B6                ; |1151| 
	.dwpsn	"OPManager.c",1151,0
           NOP             3
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L54:    
DW$L$_OP_KF_Gain$9$B:

           ADDKPC  .S2     RL626,B3,0        ; |1151| 
||         MV      .L2X    A6,B4             ; |1151| 

           MV      .L2X    A7,B5             ; |1151| 
RL626:     ; CALL OCCURS {__addd}            ; |1151| 
DW$L$_OP_KF_Gain$9$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_KF_Gain$10$B:

           STDW    .D1T1   A5:A4,*A10        ; |1151| 
||         MVKL    .S2     __addd,B6         ; |1151| 
||         MVK     .S1     14544,A3          ; |1151| 
||         SUB     .L1     A12,1,A0          ; |1151| 

           SUB     .L1     A12,1,A12         ; |1151| 
||         ADDK    .S1     200,A10           ; |1151| 
||         MVKH    .S2     __addd,B6         ; |1151| 
||         ADD     .D1     A3,A11,A3         ; |1151| 

   [ A0]   B       .S1     L54               ; |1151| 
|| [ A0]   LDDW    .D1T1   *A3,A7:A6         ; |1151| 

   [ A0]   LDDW    .D1T1   *A10,A5:A4        ; |1151| 
   [ A0]   CALL    .S2     B6                ; |1151| 
           NOP             3
           ; BRANCHCC OCCURS {L54}           ; |1151| 
DW$L$_OP_KF_Gain$10$E:
;** --------------------------------------------------------------------------*
L55:    
           MVKL    .S2     _InvSymmetricalMatrixEx,B5 ; |1154| 
           MVKH    .S2     _InvSymmetricalMatrixEx,B5 ; |1154| 
           CALL    .S2     B5                ; |1154| 
           MVK     .S1     4624,A3           ; |1154| 
           MVK     .S2     4624,B4           ; |1154| 
           ADDKPC  .S2     RL627,B3,0        ; |1154| 
           ADD     .L1X    A3,SP,A4          ; |1154| 
           ADD     .L2     B4,SP,B4          ; |1154| 
RL627:     ; CALL OCCURS {_InvSymmetricalMatrixEx}  ; |1154| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *+SP(9236),B0     ; |1156| 
||         MVKL    .S1     _DestroyMatrixEx,A5 ; |1158| 
||         MVKL    .S2     _DestroyMatrixEx,B10 ; |1160| 
||         ADD     .L1X    8,SP,A4           ; |1158| 
||         ADD     .L2     8,SP,B12          ; |1168| 

           MVKH    .S1     _DestroyMatrixEx,A5 ; |1158| 
||         MVK     .S2     4624,B4           ; |1159| 

           MVK     .S1     4624,A3           ; |1169| 
||         MVKL    .S2     _MultMatrixEx,B5  ; |1165| 
||         ADD     .L2     B4,SP,B13         ; |1159| 
||         ADD     .D2     8,SP,B4           ; |1165| 

           MVKL    .S1     _DestroyMatrixEx,A12 ; |1159| 
||         MVKH    .S2     _DestroyMatrixEx,B10 ; |1160| 

           MVKL    .S1     _DestroyMatrixEx,A11 ; |1168| 
||         ADD     .L2X    A3,SP,B11         ; |1169| 
||         MVKH    .S2     _MultMatrixEx,B5  ; |1165| 

   [ B0]   B       .S2     L56               ; |1156| 
||         MVK     .S1     4624,A6           ; |1165| 

           MVKL    .S1     _DestroyMatrixEx,A10 ; |1169| 
||         ADD     .L1X    A6,SP,A6          ; |1165| 
|| [ B0]   CALL    .S2     B5                ; |1165| 

           MVKH    .S1     _DestroyMatrixEx,A12 ; |1159| 
           MVKH    .S1     _DestroyMatrixEx,A11 ; |1168| 
           MVKH    .S1     _DestroyMatrixEx,A10 ; |1169| 
   [!B0]   CALL    .S2X    A5                ; |1158| 
           ; BRANCHCC OCCURS {L56}           ; |1156| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL628,B3,4        ; |1158| 
RL628:     ; CALL OCCURS {_DestroyMatrixEx}  ; |1158| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A12               ; |1159| 
           ADDKPC  .S2     RL629,B3,3        ; |1159| 
           MV      .L1X    B13,A4            ; |1159| 
RL629:     ; CALL OCCURS {_DestroyMatrixEx}  ; |1159| 
           CALL    .S2     B10               ; |1160| 
           MV      .L1     A15,A4            ; |1160| 
           ADDKPC  .S2     RL630,B3,3        ; |1160| 
RL630:     ; CALL OCCURS {_DestroyMatrixEx}  ; |1160| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(9260),B3     ; |1172| 
           LDW     .D2T1   *+SP(9244),A11    ; |1172| 
           LDW     .D2T1   *+SP(9240),A10    ; |1172| 
           LDW     .D2T1   *+SP(9252),A13    ; |1172| 
           LDW     .D2T1   *+SP(9248),A12    ; |1172| 
           LDW     .D2T1   *+SP(9256),A14    ; |1172| 
           LDW     .D2T2   *+SP(9268),B11    ; |1172| 
           LDW     .D2T2   *+SP(9264),B10    ; |1172| 
           LDW     .D2T2   *+SP(9276),B13    ; |1172| 

           RET     .S2     B3                ; |1172| 
||         LDW     .D2T1   *+SP(9280),A15    ; |1172| 

           LDW     .D2T2   *+SP(9272),B12    ; |1172| 
           NOP             3
           ADDK    .S2     9280,SP           ; |1172| 
           ; BRANCH OCCURS {B3}              ; |1172| 
;** --------------------------------------------------------------------------*
L56:    

           MV      .L1     A15,A4            ; |1165| 
||         ADDKPC  .S2     RL631,B3,0        ; |1165| 

RL631:     ; CALL OCCURS {_MultMatrixEx}     ; |1165| 
;** --------------------------------------------------------------------------*
           CALL    .S2X    A11               ; |1168| 
           ADDKPC  .S2     RL632,B3,3        ; |1168| 
           MV      .L1X    B12,A4            ; |1168| 
RL632:     ; CALL OCCURS {_DestroyMatrixEx}  ; |1168| 
           CALL    .S2X    A10               ; |1169| 
           MV      .L1X    B11,A4            ; |1169| 
           ADDKPC  .S2     RL633,B3,3        ; |1169| 
RL633:     ; CALL OCCURS {_DestroyMatrixEx}  ; |1169| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(9260),B3     ; |1172| 
           LDW     .D2T1   *+SP(9244),A11    ; |1172| 
           LDW     .D2T1   *+SP(9240),A10    ; |1172| 
           LDW     .D2T1   *+SP(9252),A13    ; |1172| 
           LDW     .D2T1   *+SP(9248),A12    ; |1172| 
           LDW     .D2T1   *+SP(9256),A14    ; |1172| 
           LDW     .D2T2   *+SP(9268),B11    ; |1172| 
           LDW     .D2T2   *+SP(9264),B10    ; |1172| 
           LDW     .D2T2   *+SP(9276),B13    ; |1172| 

           RET     .S2     B3                ; |1172| 
||         LDW     .D2T1   *+SP(9280),A15    ; |1172| 

           LDW     .D2T2   *+SP(9272),B12    ; |1172| 
           NOP             3
	.dwpsn	"OPManager.c",1172,1
           ADDK    .S2     9280,SP           ; |1172| 
           ; BRANCH OCCURS {B3}              ; |1172| 

DW$226	.dwtag  DW_TAG_loop
	.dwattr DW$226, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L54:1:1472539744")
	.dwattr DW$226, DW_AT_begin_file("OPManager.c")
	.dwattr DW$226, DW_AT_begin_line(0x47f)
	.dwattr DW$226, DW_AT_end_line(0x47f)
DW$227	.dwtag  DW_TAG_loop_range
	.dwattr DW$227, DW_AT_low_pc(DW$L$_OP_KF_Gain$9$B)
	.dwattr DW$227, DW_AT_high_pc(DW$L$_OP_KF_Gain$9$E)
DW$228	.dwtag  DW_TAG_loop_range
	.dwattr DW$228, DW_AT_low_pc(DW$L$_OP_KF_Gain$10$B)
	.dwattr DW$228, DW_AT_high_pc(DW$L$_OP_KF_Gain$10$E)
	.dwendtag DW$226


DW$229	.dwtag  DW_TAG_loop
	.dwattr DW$229, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L52:1:1472539744")
	.dwattr DW$229, DW_AT_begin_file("OPManager.c")
	.dwattr DW$229, DW_AT_begin_line(0x47e)
	.dwattr DW$229, DW_AT_end_line(0x47e)
DW$230	.dwtag  DW_TAG_loop_range
	.dwattr DW$230, DW_AT_low_pc(DW$L$_OP_KF_Gain$5$B)
	.dwattr DW$230, DW_AT_high_pc(DW$L$_OP_KF_Gain$5$E)
DW$231	.dwtag  DW_TAG_loop_range
	.dwattr DW$231, DW_AT_low_pc(DW$L$_OP_KF_Gain$6$B)
	.dwattr DW$231, DW_AT_high_pc(DW$L$_OP_KF_Gain$6$E)
	.dwendtag DW$229

	.dwattr DW$220, DW_AT_end_file("OPManager.c")
	.dwattr DW$220, DW_AT_end_line(0x494)
	.dwattr DW$220, DW_AT_end_column(0x01)
	.dwendtag DW$220

	.sect	".text"

DW$232	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_CovMatrUpdate"), DW_AT_symbol_name("_OP_CovMatrUpdate")
	.dwattr DW$232, DW_AT_low_pc(_OP_CovMatrUpdate)
	.dwattr DW$232, DW_AT_high_pc(0x00)
	.dwattr DW$232, DW_AT_begin_file("OPManager.c")
	.dwattr DW$232, DW_AT_begin_line(0x4a1)
	.dwattr DW$232, DW_AT_begin_column(0x0d)
	.dwattr DW$232, DW_AT_frame_base[DW_OP_breg31 4704]
	.dwattr DW$232, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",1191,1

;******************************************************************************
;* FUNCTION NAME: _OP_CovMatrUpdate                                           *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 4656 Auto + 44 Save = 4700 byte             *
;******************************************************************************
_OP_CovMatrUpdate:
;** --------------------------------------------------------------------------*
DW$233	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$233, DW_AT_type(*DW$T$189)
	.dwattr DW$233, DW_AT_location[DW_OP_reg4]
DW$234	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Pu"), DW_AT_symbol_name("_Pu")
	.dwattr DW$234, DW_AT_type(*DW$T$171)
	.dwattr DW$234, DW_AT_location[DW_OP_reg20]
DW$235	.dwtag  DW_TAG_formal_parameter, DW_AT_name("K"), DW_AT_symbol_name("_K")
	.dwattr DW$235, DW_AT_type(*DW$T$171)
	.dwattr DW$235, DW_AT_location[DW_OP_reg6]
DW$236	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Pp"), DW_AT_symbol_name("_Pp")
	.dwattr DW$236, DW_AT_type(*DW$T$171)
	.dwattr DW$236, DW_AT_location[DW_OP_reg22]
DW$237	.dwtag  DW_TAG_formal_parameter, DW_AT_name("H"), DW_AT_symbol_name("_H")
	.dwattr DW$237, DW_AT_type(*DW$T$171)
	.dwattr DW$237, DW_AT_location[DW_OP_reg8]
DW$238	.dwtag  DW_TAG_formal_parameter, DW_AT_name("n_meas"), DW_AT_symbol_name("_n_meas")
	.dwattr DW$238, DW_AT_type(*DW$T$19)
	.dwattr DW$238, DW_AT_location[DW_OP_reg24]

           ADDK    .S2     -4704,SP          ; |1191| 
||         MV      .L1X    SP,A31            ; |1191| 

           STW     .D2T2   B3,*+SP(4684)
||         MVKL    .S1     _MultMatrixEx,A3  ; |1198| 

           STW     .D2T2   B4,*+SP(4628)     ; |1191| 
||         MVKH    .S1     _MultMatrixEx,A3  ; |1198| 

           STW     .D2T2   B10,*+SP(4688)

           CALL    .S2X    A3                ; |1198| 
||         STW     .D2T1   A15,*+SP(4704)

           STW     .D2T2   B13,*+SP(4700)

           STW     .D2T2   B11,*+SP(4692)
||         STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .L1     A6,A5             ; |1191| 

           STW     .D2T1   A5,*+SP(4624)     ; |1191| 

           MV      .L1X    B6,A13            ; |1191| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B12,*+SP(4696)
||         MV      .S1     A8,A6             ; |1191| 

           ADDKPC  .S2     RL634,B3,0        ; |1198| 
||         ADD     .L1X    8,SP,A4           ; |1198| 
||         MV      .L2X    A5,B4             ; |1198| 
||         MV      .S1     A4,A12            ; |1191| 
||         STW     .D1T1   A14,*-A31(24)
||         MV      .D2     B8,B10            ; |1191| 

RL634:     ; CALL OCCURS {_MultMatrixEx}     ; |1198| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *+SP(4620),B4     ; |1199| 
||         MVKL    .S1     __subd,A3         ; |1199| 
||         ADD     .L1X    8,SP,A10
||         ZERO    .D1     A11               ; |1199| 

           NOP             3
           MVKH    .S1     __subd,A3         ; |1199| 
           CMPGT   .L2     B4,0,B0           ; |1199| 

   [!B0]   LDW     .D2T1   *+SP(4628),A10    ; |1202| 
|| [!B0]   MVKL    .S2     _MultMatrix_B_TC_Ex,B5 ; |1202| 
|| [!B0]   B       .S1     L58               ; |1199| 

   [!B0]   MVKH    .S2     _MultMatrix_B_TC_Ex,B5 ; |1202| 
   [!B0]   CALL    .S2     B5                ; |1202| 
           NOP             3
           ; BRANCHCC OCCURS {L58}           ; |1199| 
;** --------------------------------------------------------------------------*

           CALL    .S2X    A3                ; |1199| 
||         LDDW    .D1T1   *A10,A5:A4        ; |1199| 
||         ZERO    .L2     B5                ; |1199| 

	.dwpsn	"OPManager.c",1199,0
           NOP             3
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L57:    
DW$L$_OP_CovMatrUpdate$4$B:
           ADDKPC  .S2     RL635,B3,0        ; |1199| 

           MVKH    .S2     0x3ff00000,B5     ; |1199| 
||         ZERO    .L2     B4                ; |1199| 

RL635:     ; CALL OCCURS {__subd}            ; |1199| 
DW$L$_OP_CovMatrUpdate$4$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrUpdate$5$B:

           LDW     .D2T2   *+SP(4620),B4     ; |1199| 
||         ADD     .L1     1,A11,A11         ; |1199| 
||         MVKL    .S1     __subd,A3         ; |1199| 
||         STDW    .D1T1   A5:A4,*A10        ; |1199| 

           NOP             2
           MVKH    .S1     __subd,A3         ; |1199| 
           ADDK    .S1     200,A10           ; |1199| 
           CMPGT   .L2X    B4,A11,B0         ; |1199| 

   [!B0]   MVKL    .S2     _MultMatrix_B_TC_Ex,B5 ; |1202| 
|| [ B0]   ZERO    .L2     B5                ; |1199| 
|| [ B0]   LDDW    .D1T1   *A10,A5:A4        ; |1199| 
|| [ B0]   B       .S1     L57               ; |1199| 

   [!B0]   MVKH    .S2     _MultMatrix_B_TC_Ex,B5 ; |1202| 
   [ B0]   CALL    .S2X    A3                ; |1199| 
           NOP             3
           ; BRANCHCC OCCURS {L57}           ; |1199| 
DW$L$_OP_CovMatrUpdate$5$E:
;** --------------------------------------------------------------------------*

           LDW     .D2T1   *+SP(4628),A10    ; |1202| 
||         CALL    .S2     B5                ; |1202| 

           NOP             3
;** --------------------------------------------------------------------------*
L58:    
           ADDKPC  .S2     RL636,B3,0        ; |1202| 

           ADD     .L1X    8,SP,A6           ; |1202| 
||         MV      .L2X    A13,B4            ; |1202| 
||         MV      .S1     A10,A4            ; |1202| 

RL636:     ; CALL OCCURS {_MultMatrix_B_TC_Ex}  ; |1202| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _MultMatrix_SimResEx,A3 ; |1203| 
           MVKH    .S1     _MultMatrix_SimResEx,A3 ; |1203| 
           MV      .L1     A10,A6            ; |1203| 
           CALL    .S2X    A3                ; |1203| 
           ADDKPC  .S2     RL637,B3,2        ; |1203| 
           ADD     .L2     8,SP,B4           ; |1203| 
           MV      .S1     A10,A4            ; |1203| 
RL637:     ; CALL OCCURS {_MultMatrix_SimResEx}  ; |1203| 
;** --------------------------------------------------------------------------*
           ZERO    .L2     B5

           STW     .D2T2   B5,*+SP(4636)     ; |1209| 
||         MVK     .L1     0x1,A3            ; |1209| 

           STW     .D2T1   A3,*+SP(4640)     ; |1209| 
           STW     .D2T1   A3,*+SP(4648)     ; |1212| 
           LDW     .D2T1   *+SP(4636),A4

           LDW     .D2T1   *+SP(4628),A3     ; |1212| 
||         MVK     .L2     0x8,B4            ; |1209| 

           STW     .D2T2   B4,*+SP(4644)     ; |1209| 
||         ZERO    .L2     B6

	.dwpsn	"OPManager.c",1209,0

           ZERO    .S2     B4
||         STW     .D2T2   B6,*+SP(4632)     ; |1209| 
||         CMPGT   .L2     B10,0,B0          ; |1212| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L59
;** --------------------------------------------------------------------------*
L59:    
DW$L$_OP_CovMatrUpdate$10$B:
	.dwpsn	"OPManager.c",1210,0
           STW     .D2T2   B4,*+SP(4652)
           NOP             1

           ADD     .S1     A4,A3,A11
|| [ B0]   MVK     .S2     14552,B6          ; |1213| 
||         ADD     .L1X    B6,A3,A13

DW$L$_OP_CovMatrUpdate$10$E:
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L60
;** --------------------------------------------------------------------------*
L60:    
DW$L$_OP_CovMatrUpdate$11$B:
	.dwpsn	"OPManager.c",1211,0

   [!B0]   B       .S1     L62               ; |1212| 
||         LDW     .D2T2   *+SP(4624),B4
||         MV      .L2     B4,B5
|| [ B0]   MVKL    .S2     __mpyd,B8         ; |1213| 

           LDW     .D2T2   *+SP(4636),B31
|| [ B0]   ADD     .L2X    B6,A12,B6         ; |1213| 
|| [ B0]   MVKH    .S2     __mpyd,B8         ; |1213| 

   [ B0]   STW     .D2T2   B10,*+SP(4656)
   [ B0]   LDDW    .D2T2   *B6,B7:B6         ; |1213| 
           NOP             2
           ; BRANCHCC OCCURS {L62}           ; |1212| 
DW$L$_OP_CovMatrUpdate$11$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrUpdate$12$B:

           ADD     .L2     B31,B4,B11
||         ADD     .S2     B5,B4,B12

           ADDAD   .D2     B11,B10,B13

           LDDW    .D2T2   *B11++,B5:B4      ; |1213| 
||         CALL    .S2     B8                ; |1213| 

           ADDAD   .D2     B12,B10,B30
           NOP             1
	.dwpsn	"OPManager.c",1212,0
           MV      .L1X    B30,A10           ; Define a twin register
DW$L$_OP_CovMatrUpdate$12$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L61:    
DW$L$_OP_CovMatrUpdate$13$B:
	.dwpsn	"OPManager.c",1213,0

           ADDKPC  .S2     RL649,B3,0        ; |1213| 
||         MV      .L1X    B6,A4             ; |1213| 

           MV      .L1X    B7,A5             ; |1213| 
RL649:     ; CALL OCCURS {__mpyd}            ; |1213| 
DW$L$_OP_CovMatrUpdate$13$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrUpdate$14$B:
           MVKL    .S1     __mpyd,A3         ; |1213| 
           MVKH    .S1     __mpyd,A3         ; |1213| 
           LDDW    .D2T2   *B12++,B5:B4      ; |1213| 
           CALL    .S2X    A3                ; |1213| 
           ADDKPC  .S2     RL650,B3,4        ; |1213| 
RL650:     ; CALL OCCURS {__mpyd}            ; |1213| 

           MVK     .S1     14544,A3          ; |1213| 
||         MVKL    .S2     __mpyd,B6         ; |1213| 

           ADD     .L1     A3,A12,A3         ; |1213| 
||         MVKH    .S2     __mpyd,B6         ; |1213| 

           CALL    .S2     B6                ; |1213| 
||         LDDW    .D1T1   *A3,A7:A6         ; |1213| 

           LDDW    .D2T2   *B13++,B5:B4      ; |1213| 
           MV      .L1     A4,A15            ; |1213| 
           ADDKPC  .S2     RL651,B3,0        ; |1213| 
           MV      .D1     A5,A14            ; |1213| 

           MV      .S1     A6,A4             ; |1213| 
||         MV      .L1     A7,A5             ; |1213| 

RL651:     ; CALL OCCURS {__mpyd}            ; |1213| 
           MVKL    .S1     __mpyd,A3         ; |1213| 
           MVKH    .S1     __mpyd,A3         ; |1213| 
           LDDW    .D1T1   *A10++,A7:A6      ; |1213| 
           CALL    .S2X    A3                ; |1213| 
           ADDKPC  .S2     RL652,B3,2        ; |1213| 
           MV      .L2X    A6,B4             ; |1213| 
           MV      .L2X    A7,B5             ; |1213| 
RL652:     ; CALL OCCURS {__mpyd}            ; |1213| 
           MVKL    .S2     __addd,B6         ; |1213| 
           MVKH    .S2     __addd,B6         ; |1213| 
           CALL    .S2     B6                ; |1213| 
           MV      .L2X    A4,B4             ; |1213| 
           ADDKPC  .S2     RL653,B3,0        ; |1213| 
           MV      .S1     A15,A4            ; |1213| 
           MV      .L2X    A5,B5             ; |1213| 
           MV      .L1     A14,A5            ; |1213| 
RL653:     ; CALL OCCURS {__addd}            ; |1213| 
           MVKL    .S1     __addd,A3         ; |1213| 
           MVKH    .S1     __addd,A3         ; |1213| 
           LDDW    .D1T1   *A11,A7:A6        ; |1213| 
           CALL    .S2X    A3                ; |1213| 
           MV      .L2X    A4,B4             ; |1213| 
           ADDKPC  .S2     RL654,B3,0        ; |1213| 
           MV      .L2X    A5,B5             ; |1213| 
           MV      .L1     A7,A5             ; |1213| 
           MV      .S1     A6,A4             ; |1213| 
RL654:     ; CALL OCCURS {__addd}            ; |1213| 
DW$L$_OP_CovMatrUpdate$14$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrUpdate$15$B:

           LDW     .D2T1   *+SP(4656),A3     ; |1213| 
||         MVKL    .S2     __mpyd,B8         ; |1213| 

           STDW    .D1T1   A5:A4,*A11        ; |1213| 
           MVK     .S2     14552,B6          ; |1213| 
           MVKH    .S2     __mpyd,B8         ; |1213| 
           ADD     .L2X    B6,A12,B6         ; |1213| 
           SUB     .L1     A3,1,A0           ; |1212| 

   [ A0]   B       .S1     L61               ; |1212| 
|| [ A0]   LDDW    .D2T2   *B6,B7:B6         ; |1213| 

           STW     .D2T1   A0,*+SP(4656)     ; |1213| 

   [ A0]   LDDW    .D2T2   *B11++,B5:B4      ; |1213| 
|| [ A0]   CALL    .S2     B8                ; |1213| 

	.dwpsn	"OPManager.c",1214,0
           NOP             3
           ; BRANCHCC OCCURS {L61}           ; |1212| 
DW$L$_OP_CovMatrUpdate$15$E:
;** --------------------------------------------------------------------------*
L62:    
DW$L$_OP_CovMatrUpdate$16$B:

           LDW     .D2T2   *+SP(4648),B5
||         LDDW    .D1T1   *A11++,A5:A4      ; |1215| 
||         CMPGT   .L2     B10,0,B0          ; |1212| 

           LDW     .D2T2   *+SP(4652),B4
|| [ B0]   MVK     .S2     14552,B6          ; |1213| 

           NOP             3

           ADDK    .S1     192,A13           ; |1210| 
||         STDW    .D1T1   A5:A4,*A13        ; |1215| 
||         SUB     .L2     B5,1,B5           ; |1210| 
||         SUB     .L1X    B5,1,A0           ; |1210| 

   [!A0]   LDW     .D2T2   *+SP(4644),B6
||         ADDK    .S2     192,B4            ; |1210| 
|| [ A0]   B       .S1     L60               ; |1210| 

   [!A0]   LDW     .D2T1   *+SP(4640),A3
           STW     .D2T2   B5,*+SP(4648)     ; |1210| 
   [!A0]   LDW     .D2T2   *+SP(4632),B5
           STW     .D2T2   B4,*+SP(4652)     ; |1210| 
	.dwpsn	"OPManager.c",1216,0
   [!A0]   LDW     .D2T2   *+SP(4636),B4
           ; BRANCHCC OCCURS {L60}           ; |1210| 
DW$L$_OP_CovMatrUpdate$16$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_CovMatrUpdate$17$B:
           ADD     .L1     1,A3,A3           ; |1209| 
           STW     .D2T1   A3,*+SP(4640)     ; |1209| 

           ADD     .L2     8,B5,B6           ; |1209| 
||         SUB     .S2     B6,1,B5           ; |1209| 

           STW     .D2T2   B5,*+SP(4644)     ; |1209| 

           MV      .L1X    B5,A0             ; |1209| 
||         ADDK    .S2     192,B4            ; |1209| 
||         STW     .D2T2   B6,*+SP(4632)     ; |1209| 

   [ A0]   CMPGT   .L2     B10,0,B0          ; |1212| 
||         STW     .D2T2   B4,*+SP(4636)     ; |1209| 
|| [ A0]   ZERO    .S2     B4
|| [ A0]   B       .S1     L59               ; |1209| 

   [ A0]   LDW     .D2T1   *+SP(4636),A4
   [ A0]   STW     .D2T1   A3,*+SP(4648)     ; |1212| 
   [ A0]   LDW     .D2T1   *+SP(4628),A3     ; |1212| 
   [!A0]   LDW     .D2T2   *+SP(4684),B3     ; |1220| 
           NOP             1
           ; BRANCHCC OCCURS {L59}           ; |1209| 
DW$L$_OP_CovMatrUpdate$17$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4696),B12    ; |1220| 
           LDW     .D2T2   *+SP(4700),B13    ; |1220| 
           LDW     .D2T2   *+SP(4688),B10    ; |1220| 
           LDW     .D2T2   *+SP(4692),B11    ; |1220| 
           LDW     .D2T1   *+SP(4680),A14    ; |1220| 
           LDW     .D2T1   *+SP(4672),A12    ; |1220| 
           LDW     .D2T1   *+SP(4676),A13    ; |1220| 
           LDW     .D2T1   *+SP(4664),A10    ; |1220| 

           LDW     .D2T1   *+SP(4668),A11    ; |1220| 
||         RET     .S2     B3                ; |1220| 

           LDW     .D2T1   *+SP(4704),A15    ; |1220| 
||         ADDK    .S2     4704,SP           ; |1220| 

	.dwpsn	"OPManager.c",1220,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |1220| 

DW$239	.dwtag  DW_TAG_loop
	.dwattr DW$239, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L59:1:1472539744")
	.dwattr DW$239, DW_AT_begin_file("OPManager.c")
	.dwattr DW$239, DW_AT_begin_line(0x4b9)
	.dwattr DW$239, DW_AT_end_line(0x4c0)
DW$240	.dwtag  DW_TAG_loop_range
	.dwattr DW$240, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$10$B)
	.dwattr DW$240, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$10$E)
DW$241	.dwtag  DW_TAG_loop_range
	.dwattr DW$241, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$17$B)
	.dwattr DW$241, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$17$E)

DW$242	.dwtag  DW_TAG_loop
	.dwattr DW$242, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L60:2:1472539744")
	.dwattr DW$242, DW_AT_begin_file("OPManager.c")
	.dwattr DW$242, DW_AT_begin_line(0x4ba)
	.dwattr DW$242, DW_AT_end_line(0x4c0)
DW$243	.dwtag  DW_TAG_loop_range
	.dwattr DW$243, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$11$B)
	.dwattr DW$243, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$11$E)
DW$244	.dwtag  DW_TAG_loop_range
	.dwattr DW$244, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$12$B)
	.dwattr DW$244, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$12$E)
DW$245	.dwtag  DW_TAG_loop_range
	.dwattr DW$245, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$16$B)
	.dwattr DW$245, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$16$E)

DW$246	.dwtag  DW_TAG_loop
	.dwattr DW$246, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L61:3:1472539744")
	.dwattr DW$246, DW_AT_begin_file("OPManager.c")
	.dwattr DW$246, DW_AT_begin_line(0x4bc)
	.dwattr DW$246, DW_AT_end_line(0x4be)
DW$247	.dwtag  DW_TAG_loop_range
	.dwattr DW$247, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$13$B)
	.dwattr DW$247, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$13$E)
DW$248	.dwtag  DW_TAG_loop_range
	.dwattr DW$248, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$14$B)
	.dwattr DW$248, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$14$E)
DW$249	.dwtag  DW_TAG_loop_range
	.dwattr DW$249, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$15$B)
	.dwattr DW$249, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$15$E)
	.dwendtag DW$246

	.dwendtag DW$242

	.dwendtag DW$239


DW$250	.dwtag  DW_TAG_loop
	.dwattr DW$250, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L57:1:1472539744")
	.dwattr DW$250, DW_AT_begin_file("OPManager.c")
	.dwattr DW$250, DW_AT_begin_line(0x4af)
	.dwattr DW$250, DW_AT_end_line(0x4af)
DW$251	.dwtag  DW_TAG_loop_range
	.dwattr DW$251, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$4$B)
	.dwattr DW$251, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$4$E)
DW$252	.dwtag  DW_TAG_loop_range
	.dwattr DW$252, DW_AT_low_pc(DW$L$_OP_CovMatrUpdate$5$B)
	.dwattr DW$252, DW_AT_high_pc(DW$L$_OP_CovMatrUpdate$5$E)
	.dwendtag DW$250

	.dwattr DW$232, DW_AT_end_file("OPManager.c")
	.dwattr DW$232, DW_AT_end_line(0x4c4)
	.dwattr DW$232, DW_AT_end_column(0x01)
	.dwendtag DW$232

	.sect	".text"

DW$253	.dwtag  DW_TAG_subprogram, DW_AT_name("OP_Propagation"), DW_AT_symbol_name("_OP_Propagation")
	.dwattr DW$253, DW_AT_low_pc(_OP_Propagation)
	.dwattr DW$253, DW_AT_high_pc(0x00)
	.dwattr DW$253, DW_AT_begin_file("OPManager.c")
	.dwattr DW$253, DW_AT_begin_line(0x4ce)
	.dwattr DW$253, DW_AT_begin_column(0x0d)
	.dwattr DW$253, DW_AT_frame_base[DW_OP_breg31 152]
	.dwattr DW$253, DW_AT_skeletal(0x01)
	.dwpsn	"OPManager.c",1231,1

;******************************************************************************
;* FUNCTION NAME: _OP_Propagation                                             *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 104 Auto + 44 Save = 148 byte               *
;******************************************************************************
_OP_Propagation:
;** --------------------------------------------------------------------------*
DW$254	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pOPMgr"), DW_AT_symbol_name("_pOPMgr")
	.dwattr DW$254, DW_AT_type(*DW$T$189)
	.dwattr DW$254, DW_AT_location[DW_OP_reg4]
DW$255	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pTopSystem"), DW_AT_symbol_name("_pTopSystem")
	.dwattr DW$255, DW_AT_type(*DW$T$195)
	.dwattr DW$255, DW_AT_location[DW_OP_reg20]
DW$256	.dwtag  DW_TAG_formal_parameter, DW_AT_name("btUpdate"), DW_AT_symbol_name("_btUpdate")
	.dwattr DW$256, DW_AT_type(*DW$T$168)
	.dwattr DW$256, DW_AT_location[DW_OP_reg6]

           ADDK    .S2     -152,SP           ; |1231| 
||         MVK     .S1     19504,A3          ; |1236| 
||         MV      .L1X    SP,A31            ; |1231| 

           MVKL    .S1     __fltid,A3        ; |1236| 
||         ADD     .L1     A3,A4,A11         ; |1236| 
||         STDW    .D1T1   A11:A10,*-A31(40)

           MVKH    .S1     __fltid,A3        ; |1236| 
||         MV      .L1     A6,A13            ; |1231| 
||         STDW    .D1T1   A13:A12,*-A31(32)

           LDH     .D1T1   *A13,A6           ; |1236| 

           CALL    .S2X    A3                ; |1236| 
||         LDH     .D1T1   *A11,A5           ; |1236| 

           STW     .D2T2   B3,*+SP(132)
           STDW    .D2T2   B11:B10,*+SP(136)
           STW     .D1T1   A14,*-A31(24)

           STDW    .D2T2   B13:B12,*+SP(144)
||         MVK     .S2     19512,B5          ; |1236| 

           ADDKPC  .S2     RL659,B3,0        ; |1236| 
||         SUB     .L1     A6,A5,A4          ; |1236| 
||         MV      .S1X    B4,A12            ; |1231| 
||         MV      .D1     A4,A10            ; |1231| 
||         ADD     .L2X    B5,A4,B10         ; |1236| 
||         STW     .D2T1   A15,*+SP(152)

RL659:     ; CALL OCCURS {__fltid}           ; |1236| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |1236| 
           MVKH    .S1     __mpyd,A3         ; |1236| 
           MVKL    .S2     0x41227500,B5     ; |1236| 
           CALL    .S2X    A3                ; |1236| 
           MVKH    .S2     0x41227500,B5     ; |1236| 
           ADDKPC  .S2     RL660,B3,2        ; |1236| 
           ZERO    .L2     B4                ; |1236| 
RL660:     ; CALL OCCURS {__mpyd}            ; |1236| 
           MVKL    .S1     __subd,A3         ; |1236| 
           MVKH    .S1     __subd,A3         ; |1236| 
           LDDW    .D1T1   *+A13(8),A7:A6    ; |1236| 
           CALL    .S2X    A3                ; |1236| 
           LDDW    .D2T2   *B10,B5:B4        ; |1236| 
           MV      .L1     A5,A14            ; |1236| 
           MV      .D1     A4,A13            ; |1236| 
           MV      .S1     A7,A5             ; |1236| 

           ADDKPC  .S2     RL661,B3,0        ; |1236| 
||         MV      .L1     A6,A4             ; |1236| 

RL661:     ; CALL OCCURS {__subd}            ; |1236| 
           MVKL    .S1     __addd,A3         ; |1236| 
           MVKH    .S1     __addd,A3         ; |1236| 
           MV      .L2X    A4,B4             ; |1236| 
           CALL    .S2X    A3                ; |1236| 
           MV      .L2X    A5,B5             ; |1236| 
           MV      .S1     A13,A4            ; |1236| 
           ADDKPC  .S2     RL662,B3,1        ; |1236| 
           MV      .L1     A14,A5            ; |1236| 
RL662:     ; CALL OCCURS {__addd}            ; |1236| 
           MVKL    .S1     _MJD_From_UTCEx,A3 ; |1241| 
           MVKH    .S1     _MJD_From_UTCEx,A3 ; |1241| 
           MV      .L2X    A4,B13            ; |1236| 
           CALL    .S2X    A3                ; |1241| 
           ADDKPC  .S2     RL663,B3,0        ; |1241| 
           STW     .D2T2   B13,*+SP(80)      ; |1236| 
           MV      .S1     A5,A14            ; |1236| 
           MVK     .S1     19504,A6          ; |1241| 

           MV      .D1     A4,A15            ; |1236| 
||         ADD     .L1     A6,A10,A4         ; |1241| 
||         ADD     .L2     8,SP,B4           ; |1241| 

RL663:     ; CALL OCCURS {_MJD_From_UTCEx}   ; |1241| 
           MVKL    .S2     __fltid,B4        ; |1242| 
           MVKH    .S2     __fltid,B4        ; |1242| 

           CALL    .S2     B4                ; |1242| 
||         MVK     .S1     11680,A3          ; |1242| 

           LDB     .D1T1   *+A3[A12],A4      ; |1242| 
           ADDKPC  .S2     RL667,B3,3        ; |1242| 
RL667:     ; CALL OCCURS {__fltid}           ; |1242| 
           MVKL    .S1     __mpyd,A3         ; |1242| 
           MVKH    .S1     __mpyd,A3         ; |1242| 
           MVKL    .S2     0xa0ce5129,B4     ; |1242| 
           CALL    .S2X    A3                ; |1242| 
           MVKL    .S2     0x3ee845c8,B5     ; |1242| 
           MVKH    .S2     0xa0ce5129,B4     ; |1242| 
           MVKH    .S2     0x3ee845c8,B5     ; |1242| 
           ADDKPC  .S2     RL668,B3,1        ; |1242| 
RL668:     ; CALL OCCURS {__mpyd}            ; |1242| 
           MVKL    .S2     __subd,B8         ; |1242| 

           LDDW    .D2T2   *+SP(8),B7:B6     ; |1242| 
||         MVKH    .S2     __subd,B8         ; |1242| 

           CALL    .S2     B8                ; |1242| 
           MV      .L2X    A4,B4             ; |1242| 
           ADDKPC  .S2     RL669,B3,0        ; |1242| 
           MV      .L2X    A5,B5             ; |1242| 
           MV      .L1X    B6,A4             ; |1242| 
           MV      .L1X    B7,A5             ; |1242| 
RL669:     ; CALL OCCURS {__subd}            ; |1242| 
           MVKL    .S2     __cmpd,B6         ; |1247| 
           MVKH    .S2     __cmpd,B6         ; |1247| 
           CALL    .S2     B6                ; |1247| 
           MV      .L2X    A5,B11            ; |1242| 
           STDW    .D2T1   A5:A4,*+SP(8)     ; |1242| 
           ADDKPC  .S2     RL670,B3,0        ; |1247| 
           MV      .S1     A14,A5            ; |1247| 

           MV      .D2X    A4,B12            ; |1242| 
||         MV      .L1X    B13,A4            ; |1247| 
||         ZERO    .L2     B5:B4             ; |1247| 

RL670:     ; CALL OCCURS {__cmpd}            ; |1247| 
           MVKL    .S2     __cmpd,B6         ; |1247| 
           MVKH    .S2     __cmpd,B6         ; |1247| 
           CALL    .S2     B6                ; |1247| 
           ZERO    .L2     B5                ; |1247| 
           MVKH    .S2     0x403e0000,B5     ; |1247| 
           CLR     .S1     A14,31,31,A12     ; |1247| 
           ADDKPC  .S2     RL671,B3,0        ; |1247| 

           MV      .D1     A12,A5            ; |1247| 
||         CMPLT   .L1     A4,0,A13          ; |1247| 
||         ZERO    .L2     B4                ; |1247| 
||         MV      .S1X    B13,A4            ; |1247| 

RL671:     ; CALL OCCURS {__cmpd}            ; |1247| 
           MVKL    .S1     __mpyd,A3         ; |1247| 
           MVKH    .S1     __mpyd,A3         ; |1247| 
           CMPGT   .L1     A4,0,A0           ; |1247| 
           CALL    .S2X    A3                ; |1247| 
           ZERO    .S1     A5                ; |1247| 

   [ A0]   ZERO    .L1     A15               ; |1247| 
|| [ A0]   ZERO    .D1     A12               ; |1247| 
||         MVKH    .S1     0xbff00000,A5     ; |1247| 

   [ A0]   MVKH    .S1     0x403e0000,A12    ; |1247| 
||         MV      .L1     A13,A1            ; |1247| 

           MV      .L2X    A15,B4            ; |1247| 
|| [!A1]   ZERO    .L1     A5                ; |1247| 

           ADDKPC  .S2     RL672,B3,0        ; |1247| 
||         MV      .L2X    A12,B5            ; |1247| 
|| [!A1]   MVKH    .S1     0x3ff00000,A5     ; |1247| 
||         ZERO    .L1     A4                ; |1247| 

RL672:     ; CALL OCCURS {__mpyd}            ; |1247| 
;** --------------------------------------------------------------------------*
           MVK     .S1     14672,A3          ; |1250| 
           MVK     .S1     14672,A6          ; |1250| 

           ADD     .L1     A3,A10,A3         ; |1250| 
||         MVKL    .S1     __cmpd,A8         ; |1252| 

           LDDW    .D1T1   *A3,A7:A6         ; |1250| 
||         ADD     .L1     A6,A10,A3         ; |1250| 
||         MVKH    .S1     __cmpd,A8         ; |1252| 

           MVKL    .S2     0xa0b5ed8d,B4     ; |1252| 
           CALL    .S2X    A8                ; |1252| 
           STW     .D2T1   A3,*+SP(84)       ; |1252| 
           MVKL    .S2     0x3eb0c6f7,B5     ; |1252| 

           MV      .L1X    B11,A7            ; |1252| 
||         STW     .D2T1   A7,*+SP(92)       ; |1252| 
||         MVKH    .S2     0xa0b5ed8d,B4     ; |1252| 

           MV      .L1X    B12,A6            ; |1252| 
||         STW     .D2T1   A6,*+SP(88)       ; |1252| 
||         MVKH    .S2     0x3eb0c6f7,B5     ; |1252| 

           ADDKPC  .S2     RL673,B3,0        ; |1252| 
||         STDW    .D1T1   A7:A6,*A3         ; |1250| 
||         MV      .L2X    A4,B13            ; |1247| 
||         MV      .L1     A5,A13            ; |1247| 

RL673:     ; CALL OCCURS {__cmpd}            ; |1252| 
;** --------------------------------------------------------------------------*

           CMPGT   .L1     A4,0,A0           ; |1252| 
||         MVK     .S1     14680,A5
||         MVK     .S2     14688,B5

   [!A0]   B       .S2     L70               ; |1252| 
||         MVK     .S1     0x14,A3           ; |1244| 
||         ADD     .L2X    B5,A10,B12

           STW     .D2T1   A3,*+SP(96)       ; |1244| 
||         ADD     .L1     A5,A10,A3
|| [ A0]   MVKL    .S2     _OP_CovMatrPropagation,B5 ; |1259| 

           STW     .D2T1   A3,*+SP(100)
|| [ A0]   MVK     .S1     19536,A3          ; |1255| 
||         MVK     .S2     14624,B4

   [ A0]   LDB     .D1T1   *+A3[A10],A0      ; |1255| 
           NOP             1
           ADD     .L1X    B4,A10,A15
           ; BRANCHCC OCCURS {L70}           ; |1252| 
;** --------------------------------------------------------------------------*
           NOP             2
	.dwpsn	"OPManager.c",1252,0
   [ A0]   MVKL    .S1     _OP_CovMatrPropagation_Static,A3 ; |1268| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L63
;** --------------------------------------------------------------------------*
L63:    
DW$L$_OP_Propagation$6$B:
	.dwpsn	"OPManager.c",1253,0

   [ A0]   B       .S1     L64               ; |1255| 
|| [!A0]   MVK     .S2     14648,B4          ; |1259| 

           MVKH    .S2     _OP_CovMatrPropagation,B5 ; |1259| 
|| [ A0]   MVKH    .S1     _OP_CovMatrPropagation_Static,A3 ; |1268| 

   [!A0]   CALL    .S2     B5                ; |1259| 
   [ A0]   CALL    .S2X    A3                ; |1268| 
           NOP             2
           ; BRANCHCC OCCURS {L64}           ; |1255| 
DW$L$_OP_Propagation$6$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$7$B:

           ADD     .L2X    B4,A10,B4         ; |1259| 
||         MV      .S2     B13,B6            ; |1259| 

           MV      .L1X    B12,A8            ; |1259| 
||         ADDKPC  .S2     RL674,B3,0        ; |1259| 
||         MV      .L2     B12,B8            ; |1259| 
||         MV      .D2X    A13,B7            ; |1259| 
||         MV      .S1     A15,A6            ; |1259| 
||         MV      .D1     A10,A4            ; |1259| 

RL674:     ; CALL OCCURS {_OP_CovMatrPropagation}  ; |1259| 
DW$L$_OP_Propagation$7$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$8$B:
           MVKL    .S2     _rk4,B5           ; |1262| 
           MVKH    .S2     _rk4,B5           ; |1262| 

           CALL    .S2     B5                ; |1262| 
||         MVK     .S1     3634,A3           ; |1262| 

           LDW     .D1T1   *+A10[A3],A4      ; |1262| 
           MV      .L2X    A15,B4            ; |1262| 
           MV      .S2     B13,B6            ; |1262| 
           ADDKPC  .S2     RL675,B3,0        ; |1262| 

           ADD     .D2     SP,16,B8          ; |1262| 
||         MV      .L1     A10,A8            ; |1262| 
||         MV      .L2X    A13,B7            ; |1262| 
||         MVK     .S1     0x7,A6            ; |1262| 

RL675:     ; CALL OCCURS {_rk4}              ; |1262| 
DW$L$_OP_Propagation$8$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$9$B:

           BNOP    .S1     L65,4             ; |1263| 
||         MVKL    .S2     __addd,B6         ; |1278| 
||         MVK     .L2     0x7,B4            ; |1278| 

           MVKH    .S2     __addd,B6         ; |1278| 
           ; BRANCH OCCURS {L65}             ; |1263| 
DW$L$_OP_Propagation$9$E:
;** --------------------------------------------------------------------------*
L64:    
DW$L$_OP_Propagation$10$B:
           MV      .L2     B13,B4            ; |1268| 
           ADDKPC  .S2     RL676,B3,0        ; |1268| 

           MV      .L2     B12,B6            ; |1268| 
||         MV      .D2X    A13,B5            ; |1268| 
||         MV      .L1X    B12,A6            ; |1268| 
||         MV      .S1     A10,A4            ; |1268| 

RL676:     ; CALL OCCURS {_OP_CovMatrPropagation_Static}  ; |1268| 
DW$L$_OP_Propagation$10$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$11$B:

           ZERO    .L2     B5:B4             ; |1272| 
||         MVKL    .S2     __addd,B6         ; |1278| 

           MVK     .L2     0x7,B4            ; |1278| 
||         STDW    .D2T2   B5:B4,*+SP(16)    ; |1272| 
||         MVKH    .S2     __addd,B6         ; |1278| 

DW$L$_OP_Propagation$11$E:
;** --------------------------------------------------------------------------*
L65:    
DW$L$_OP_Propagation$12$B:

           MV      .L1     A15,A12
||         STW     .D2T2   B4,*+SP(104)      ; |1278| 

           ADD     .D2     SP,16,B11
||         CALL    .S2     B6                ; |1278| 
||         LDDW    .D1T1   *A12,A5:A4        ; |1278| 

           LDDW    .D2T2   *B11++,B5:B4      ; |1278| 
	.dwpsn	"OPManager.c",1278,0
           NOP             3
DW$L$_OP_Propagation$12$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
L66:    
DW$L$_OP_Propagation$13$B:
           ADDKPC  .S2     RL677,B3,0        ; |1278| 
RL677:     ; CALL OCCURS {__addd}            ; |1278| 
DW$L$_OP_Propagation$13$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$14$B:

           LDW     .D2T2   *+SP(104),B4      ; |1278| 
||         MVKL    .S2     __addd,B6         ; |1278| 
||         STDW    .D1T1   A5:A4,*A12++      ; |1278| 

           NOP             3
           MVKH    .S2     __addd,B6         ; |1278| 

           SUB     .L2     B4,1,B4           ; |1278| 
||         SUB     .L1X    B4,1,A0           ; |1278| 

   [ A0]   LDDW    .D1T1   *A12,A5:A4        ; |1278| 
||         STW     .D2T2   B4,*+SP(104)      ; |1278| 
|| [ A0]   B       .S1     L66               ; |1278| 

   [!A0]   LDW     .D2T1   *+SP(100),A3
|| [ A0]   CALL    .S2     B6                ; |1278| 

   [ A0]   LDDW    .D2T2   *B11++,B5:B4      ; |1278| 
           NOP             3
           ; BRANCHCC OCCURS {L66}           ; |1278| 
DW$L$_OP_Propagation$14$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$15$B:

           LDDW    .D1T1   *A3,A5:A4         ; |1279| 
||         MVKL    .S1     __mpyd,A3         ; |1279| 

           MVKH    .S1     __mpyd,A3         ; |1279| 
           MV      .L2     B13,B4            ; |1279| 
           CALL    .S2X    A3                ; |1279| 
           ADDKPC  .S2     RL679,B3,3        ; |1279| 
           MV      .D2X    A13,B5            ; |1279| 
RL679:     ; CALL OCCURS {__mpyd}            ; |1279| 
DW$L$_OP_Propagation$15$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$16$B:
           MVKL    .S1     __addd,A3         ; |1279| 
           MVKH    .S1     __addd,A3         ; |1279| 
           MV      .L2X    A5,B5             ; |1279| 

           CALL    .S2X    A3                ; |1279| 
||         LDW     .D2T1   *+SP(92),A5       ; |1279| 

           LDW     .D2T1   *+SP(88),A4       ; |1279| 
||         MV      .L2X    A4,B4             ; |1279| 

           ADDKPC  .S2     RL680,B3,3        ; |1279| 
RL680:     ; CALL OCCURS {__addd}            ; |1279| 
           MVKL    .S1     __addd,A3         ; |1282| 
           MVKH    .S1     __addd,A3         ; |1282| 
           LDDW    .D2T2   *B10,B7:B6        ; |1282| 
           CALL    .S2X    A3                ; |1282| 
           STW     .D2T1   A4,*+SP(88)       ; |1279| 
           MV      .L2     B13,B4            ; |1282| 
           ADDKPC  .S2     RL681,B3,0        ; |1282| 
           MV      .L1X    B6,A4             ; |1282| 

           STW     .D2T1   A5,*+SP(92)       ; |1279| 
||         MV      .L2X    A13,B5            ; |1282| 
||         MV      .L1X    B7,A5             ; |1282| 

RL681:     ; CALL OCCURS {__addd}            ; |1282| 
           MVKL    .S2     __cmpd,B6         ; |1283| 
           MVKH    .S2     __cmpd,B6         ; |1283| 
           CALL    .S2     B6                ; |1283| 
           MV      .L1     A5,A13            ; |1282| 
           MV      .S1     A4,A12            ; |1282| 
           MVKL    .S2     0x41227500,B5     ; |1283| 
           MVKH    .S2     0x41227500,B5     ; |1283| 

           ADDKPC  .S2     RL682,B3,0        ; |1283| 
||         STDW    .D2T1   A13:A12,*B10      ; |1282| 
||         ZERO    .L2     B4                ; |1283| 

RL682:     ; CALL OCCURS {__cmpd}            ; |1283| 
DW$L$_OP_Propagation$16$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$17$B:

           MVKL    .S2     __cmpd,B6         ; |1288| 
||         CMPLT   .L1     A4,0,A0           ; |1283| 

           MVKH    .S2     __cmpd,B6         ; |1288| 
|| [!A0]   MVKL    .S1     __subd,A3         ; |1285| 
|| [!A0]   LDDW    .D2T2   *B10,B7:B6        ; |1285| 

   [!A0]   MVKH    .S1     __subd,A3         ; |1285| 
|| [!A0]   B       .S2     L67               ; |1283| 

   [ A0]   CALL    .S2     B6                ; |1288| 
   [!A0]   CALL    .S2X    A3                ; |1285| 
   [!A0]   MVKL    .S2     0x41227500,B5     ; |1285| 
           NOP             2
           ; BRANCHCC OCCURS {L67}           ; |1283| 
DW$L$_OP_Propagation$17$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$18$B:

           ZERO    .L2     B5:B4             ; |1288| 
||         MV      .L1     A13,A5            ; |1288| 
||         MV      .S1     A12,A4            ; |1288| 
||         ADDKPC  .S2     RL683,B3,0        ; |1288| 

RL683:     ; CALL OCCURS {__cmpd}            ; |1288| 
DW$L$_OP_Propagation$18$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$19$B:

           MVKL    .S1     __addd,A3         ; |1290| 
||         CMPLT   .L1     A4,0,A0           ; |1288| 

           MVKH    .S1     __addd,A3         ; |1290| 
|| [!A0]   MVKL    .S2     __subd,B6         ; |1295| 
|| [!A0]   LDW     .D2T1   *+SP(80),A4       ; |1295| 
|| [!A0]   ZERO    .L2     B5                ; |1295| 

   [!A0]   BNOP    .S1     L69,1             ; |1288| 
|| [!A0]   MVKH    .S2     __subd,B6         ; |1295| 

   [ A0]   CALL    .S2X    A3                ; |1290| 
   [ A0]   MVKL    .S2     0x41227500,B5     ; |1290| 
   [!A0]   CALL    .S2     B6                ; |1295| 
           NOP             1
           ; BRANCHCC OCCURS {L69}           ; |1288| 
DW$L$_OP_Propagation$19$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$20$B:
           MVKH    .S2     0x41227500,B5     ; |1290| 

           ADDKPC  .S2     RL684,B3,0        ; |1290| 
||         MV      .L1     A13,A5            ; |1290| 
||         MV      .S1     A12,A4            ; |1290| 
||         ZERO    .L2     B4                ; |1290| 

RL684:     ; CALL OCCURS {__addd}            ; |1290| 
DW$L$_OP_Propagation$20$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$21$B:

           LDH     .D1T1   *A11,A3           ; |1291| 
||         MVKL    .S2     __subd,B6         ; |1295| 

           BNOP    .S1     L68,3             ; |1291| 
||         STDW    .D2T1   A5:A4,*B10        ; |1290| 
||         MVKH    .S2     __subd,B6         ; |1295| 

           SUB     .L1     A3,1,A3           ; |1291| 
           STH     .D1T1   A3,*A11           ; |1291| 
           ; BRANCH OCCURS {L68}             ; |1291| 
DW$L$_OP_Propagation$21$E:
;** --------------------------------------------------------------------------*
L67:    
DW$L$_OP_Propagation$22$B:

           MVKH    .S2     0x41227500,B5     ; |1285| 
||         MV      .L1X    B6,A4             ; |1285| 

           MV      .L1X    B7,A5             ; |1285| 
||         ADDKPC  .S2     RL685,B3,0        ; |1285| 
||         ZERO    .L2     B4                ; |1285| 

RL685:     ; CALL OCCURS {__subd}            ; |1285| 
DW$L$_OP_Propagation$22$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$23$B:
           LDH     .D1T1   *A11,A3           ; |1286| 
           NOP             1
           STDW    .D2T1   A5:A4,*B10        ; |1285| 
           MVKL    .S2     __subd,B6         ; |1295| 
           MVKH    .S2     __subd,B6         ; |1295| 
           ADD     .L1     1,A3,A3           ; |1286| 
           STH     .D1T1   A3,*A11           ; |1286| 
DW$L$_OP_Propagation$23$E:
;** --------------------------------------------------------------------------*
L68:    
DW$L$_OP_Propagation$24$B:

           CALL    .S2     B6                ; |1295| 
||         ZERO    .L2     B5                ; |1295| 
||         LDW     .D2T1   *+SP(80),A4       ; |1295| 

           NOP             1
DW$L$_OP_Propagation$24$E:
;** --------------------------------------------------------------------------*
L69:    
DW$L$_OP_Propagation$25$B:
           ADDKPC  .S2     RL686,B3,2        ; |1295| 

           MVKH    .S2     0x403e0000,B5     ; |1295| 
||         MV      .L1     A14,A5            ; |1295| 
||         ZERO    .L2     B4                ; |1295| 

RL686:     ; CALL OCCURS {__subd}            ; |1295| 
DW$L$_OP_Propagation$25$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$26$B:
           MVKL    .S1     __cmpd,A3         ; |1296| 
           MVKH    .S1     __cmpd,A3         ; |1296| 
           ZERO    .L2     B5:B4             ; |1296| 
           CALL    .S2X    A3                ; |1296| 
           ADDKPC  .S2     RL687,B3,0        ; |1296| 
           MV      .L2X    A4,B13            ; |1295| 
           MV      .L1     A5,A14            ; |1295| 
           MV      .L1     A4,A12            ; |1295| 
           STW     .D2T1   A12,*+SP(80)      ; |1295| 
RL687:     ; CALL OCCURS {__cmpd}            ; |1296| 
           MVKL    .S1     __cmpd,A3         ; |1296| 
           MVKH    .S1     __cmpd,A3         ; |1296| 
           ZERO    .L2     B5                ; |1296| 
           CALL    .S2X    A3                ; |1296| 
           MVKH    .S2     0x403e0000,B5     ; |1296| 
           CLR     .S1     A14,31,31,A13     ; |1296| 
           ADDKPC  .S2     RL688,B3,0        ; |1296| 
           MV      .S1     A13,A5            ; |1296| 

           CMPLT   .L2X    A4,0,B11          ; |1296| 
||         MV      .L1     A12,A4            ; |1296| 
||         ZERO    .D2     B4                ; |1296| 

RL688:     ; CALL OCCURS {__cmpd}            ; |1296| 
           MVKL    .S2     __mpyd,B6         ; |1296| 
           MVKH    .S2     __mpyd,B6         ; |1296| 

           CALL    .S2     B6                ; |1296| 
||         MV      .L1X    B11,A0            ; |1296| 

   [!A0]   ZERO    .L2     B5                ; |1296| 
||         CMPGT   .L1     A4,0,A1           ; |1296| 

   [!A0]   MVKH    .S2     0x3ff00000,B5     ; |1296| 
|| [!A1]   MV      .L1X    B13,A3            ; |1296| 
|| [ A1]   ZERO    .S1     A13               ; |1296| 
|| [ A0]   ZERO    .L2     B5                ; |1296| 
|| [ A1]   ZERO    .D1     A3                ; |1296| 

   [ A0]   MVKH    .S2     0xbff00000,B5     ; |1296| 
|| [ A1]   MVKH    .S1     0x403e0000,A13    ; |1296| 

           MV      .L2X    A3,B4             ; |1296| 

           ADDKPC  .S2     RL689,B3,0        ; |1296| 
||         MV      .L2X    A13,B5            ; |1296| 
||         MV      .L1X    B5,A5             ; |1296| 
|| [!A0]   ZERO    .S1     A4                ; |1296| 
|| [ A0]   ZERO    .D1     A4                ; |1296| 

RL689:     ; CALL OCCURS {__mpyd}            ; |1296| 
           NOP             1
DW$L$_OP_Propagation$26$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$27$B:

           LDW     .D2T1   *+SP(96),A3       ; |1296| 
||         MV      .L1     A5,A13            ; |1296| 
||         MV      .L2X    A4,B13            ; |1296| 

           NOP             4

           MVKL    .S1     __cmpd,A3         ; |1252| 
||         SUB     .L1     A3,1,A0           ; |1298| 

   [!A0]   BNOP    .S2     L70,1             ; |1298| 
||         STW     .D2T1   A0,*+SP(96)       ; |1298| 
||         MVKH    .S1     __cmpd,A3         ; |1252| 

   [!A0]   MVK     .S1     19537,A4          ; |1301| 
|| [!A0]   MVK     .L1     1,A3              ; |1301| 
|| [ A0]   MVKL    .S2     0x3eb0c6f7,B5     ; |1252| 

   [!A0]   STB     .D1T1   A3,*+A10[A4]      ; |1301| 
|| [ A0]   MVKL    .S2     0xa0b5ed8d,B4     ; |1252| 

   [ A0]   CALL    .S2X    A3                ; |1252| 
           NOP             1
           ; BRANCHCC OCCURS {L70}           ; |1298| 
DW$L$_OP_Propagation$27$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$28$B:
           MVKH    .S2     0xa0b5ed8d,B4     ; |1252| 
           ADDKPC  .S2     RL690,B3,1        ; |1252| 
           MVKH    .S2     0x3eb0c6f7,B5     ; |1252| 
RL690:     ; CALL OCCURS {__cmpd}            ; |1252| 
DW$L$_OP_Propagation$28$E:
;** --------------------------------------------------------------------------*
DW$L$_OP_Propagation$29$B:

           MVK     .S1     19536,A3          ; |1255| 
||         CMPGT   .L1     A4,0,A0           ; |1252| 

   [ A0]   BNOP    .S1     L63,4             ; |1252| 
|| [ A0]   MVKL    .S2     _OP_CovMatrPropagation,B5 ; |1259| 
|| [ A0]   LDB     .D1T1   *+A3[A10],A0      ; |1255| 

	.dwpsn	"OPManager.c",1304,0
   [ A0]   MVKL    .S1     _OP_CovMatrPropagation_Static,A3 ; |1268| 
           ; BRANCHCC OCCURS {L63}           ; |1252| 
DW$L$_OP_Propagation$29$E:
;** --------------------------------------------------------------------------*

           B       .S1     L71               ; |1252| 
||         LDW     .D2T1   *+SP(84),A3

           LDW     .D2T1   *+SP(92),A5
           LDW     .D2T1   *+SP(88),A4
           NOP             3
           ; BRANCH OCCURS {L71}             ; |1252| 
;** --------------------------------------------------------------------------*
L70:    
           LDW     .D2T1   *+SP(92),A5
           LDW     .D2T1   *+SP(88),A4
           LDW     .D2T1   *+SP(84),A3
           NOP             3
;** --------------------------------------------------------------------------*
L71:    
           NOP             1
           STDW    .D1T1   A5:A4,*A3         ; |1306| 
           LDW     .D2T2   *+SP(132),B3      ; |1309| 
           LDW     .D2T1   *+SP(152),A15     ; |1309| 
           LDDW    .D2T1   *+SP(112),A11:A10 ; |1309| 
           LDDW    .D2T1   *+SP(120),A13:A12 ; |1309| 
           LDW     .D2T1   *+SP(128),A14     ; |1309| 

           LDDW    .D2T2   *+SP(136),B11:B10 ; |1309| 
||         RET     .S2     B3                ; |1309| 

           ADDK    .S2     152,SP            ; |1309| 
||         LDDW    .D2T2   *+SP(144),B13:B12 ; |1309| 

	.dwpsn	"OPManager.c",1309,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |1309| 

DW$257	.dwtag  DW_TAG_loop
	.dwattr DW$257, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L63:1:1472539744")
	.dwattr DW$257, DW_AT_begin_file("OPManager.c")
	.dwattr DW$257, DW_AT_begin_line(0x4e4)
	.dwattr DW$257, DW_AT_end_line(0x518)
DW$258	.dwtag  DW_TAG_loop_range
	.dwattr DW$258, DW_AT_low_pc(DW$L$_OP_Propagation$6$B)
	.dwattr DW$258, DW_AT_high_pc(DW$L$_OP_Propagation$6$E)
DW$259	.dwtag  DW_TAG_loop_range
	.dwattr DW$259, DW_AT_low_pc(DW$L$_OP_Propagation$22$B)
	.dwattr DW$259, DW_AT_high_pc(DW$L$_OP_Propagation$22$E)
DW$260	.dwtag  DW_TAG_loop_range
	.dwattr DW$260, DW_AT_low_pc(DW$L$_OP_Propagation$20$B)
	.dwattr DW$260, DW_AT_high_pc(DW$L$_OP_Propagation$20$E)
DW$261	.dwtag  DW_TAG_loop_range
	.dwattr DW$261, DW_AT_low_pc(DW$L$_OP_Propagation$21$B)
	.dwattr DW$261, DW_AT_high_pc(DW$L$_OP_Propagation$21$E)
DW$262	.dwtag  DW_TAG_loop_range
	.dwattr DW$262, DW_AT_low_pc(DW$L$_OP_Propagation$23$B)
	.dwattr DW$262, DW_AT_high_pc(DW$L$_OP_Propagation$23$E)
DW$263	.dwtag  DW_TAG_loop_range
	.dwattr DW$263, DW_AT_low_pc(DW$L$_OP_Propagation$10$B)
	.dwattr DW$263, DW_AT_high_pc(DW$L$_OP_Propagation$10$E)
DW$264	.dwtag  DW_TAG_loop_range
	.dwattr DW$264, DW_AT_low_pc(DW$L$_OP_Propagation$7$B)
	.dwattr DW$264, DW_AT_high_pc(DW$L$_OP_Propagation$7$E)
DW$265	.dwtag  DW_TAG_loop_range
	.dwattr DW$265, DW_AT_low_pc(DW$L$_OP_Propagation$8$B)
	.dwattr DW$265, DW_AT_high_pc(DW$L$_OP_Propagation$8$E)
DW$266	.dwtag  DW_TAG_loop_range
	.dwattr DW$266, DW_AT_low_pc(DW$L$_OP_Propagation$9$B)
	.dwattr DW$266, DW_AT_high_pc(DW$L$_OP_Propagation$9$E)
DW$267	.dwtag  DW_TAG_loop_range
	.dwattr DW$267, DW_AT_low_pc(DW$L$_OP_Propagation$11$B)
	.dwattr DW$267, DW_AT_high_pc(DW$L$_OP_Propagation$11$E)
DW$268	.dwtag  DW_TAG_loop_range
	.dwattr DW$268, DW_AT_low_pc(DW$L$_OP_Propagation$12$B)
	.dwattr DW$268, DW_AT_high_pc(DW$L$_OP_Propagation$12$E)
DW$269	.dwtag  DW_TAG_loop_range
	.dwattr DW$269, DW_AT_low_pc(DW$L$_OP_Propagation$15$B)
	.dwattr DW$269, DW_AT_high_pc(DW$L$_OP_Propagation$15$E)
DW$270	.dwtag  DW_TAG_loop_range
	.dwattr DW$270, DW_AT_low_pc(DW$L$_OP_Propagation$16$B)
	.dwattr DW$270, DW_AT_high_pc(DW$L$_OP_Propagation$16$E)
DW$271	.dwtag  DW_TAG_loop_range
	.dwattr DW$271, DW_AT_low_pc(DW$L$_OP_Propagation$17$B)
	.dwattr DW$271, DW_AT_high_pc(DW$L$_OP_Propagation$17$E)
DW$272	.dwtag  DW_TAG_loop_range
	.dwattr DW$272, DW_AT_low_pc(DW$L$_OP_Propagation$18$B)
	.dwattr DW$272, DW_AT_high_pc(DW$L$_OP_Propagation$18$E)
DW$273	.dwtag  DW_TAG_loop_range
	.dwattr DW$273, DW_AT_low_pc(DW$L$_OP_Propagation$19$B)
	.dwattr DW$273, DW_AT_high_pc(DW$L$_OP_Propagation$19$E)
DW$274	.dwtag  DW_TAG_loop_range
	.dwattr DW$274, DW_AT_low_pc(DW$L$_OP_Propagation$24$B)
	.dwattr DW$274, DW_AT_high_pc(DW$L$_OP_Propagation$24$E)
DW$275	.dwtag  DW_TAG_loop_range
	.dwattr DW$275, DW_AT_low_pc(DW$L$_OP_Propagation$25$B)
	.dwattr DW$275, DW_AT_high_pc(DW$L$_OP_Propagation$25$E)
DW$276	.dwtag  DW_TAG_loop_range
	.dwattr DW$276, DW_AT_low_pc(DW$L$_OP_Propagation$26$B)
	.dwattr DW$276, DW_AT_high_pc(DW$L$_OP_Propagation$26$E)
DW$277	.dwtag  DW_TAG_loop_range
	.dwattr DW$277, DW_AT_low_pc(DW$L$_OP_Propagation$27$B)
	.dwattr DW$277, DW_AT_high_pc(DW$L$_OP_Propagation$27$E)
DW$278	.dwtag  DW_TAG_loop_range
	.dwattr DW$278, DW_AT_low_pc(DW$L$_OP_Propagation$28$B)
	.dwattr DW$278, DW_AT_high_pc(DW$L$_OP_Propagation$28$E)
DW$279	.dwtag  DW_TAG_loop_range
	.dwattr DW$279, DW_AT_low_pc(DW$L$_OP_Propagation$29$B)
	.dwattr DW$279, DW_AT_high_pc(DW$L$_OP_Propagation$29$E)

DW$280	.dwtag  DW_TAG_loop
	.dwattr DW$280, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\OPManager.asm:L66:2:1472539744")
	.dwattr DW$280, DW_AT_begin_file("OPManager.c")
	.dwattr DW$280, DW_AT_begin_line(0x4fe)
	.dwattr DW$280, DW_AT_end_line(0x4fe)
DW$281	.dwtag  DW_TAG_loop_range
	.dwattr DW$281, DW_AT_low_pc(DW$L$_OP_Propagation$13$B)
	.dwattr DW$281, DW_AT_high_pc(DW$L$_OP_Propagation$13$E)
DW$282	.dwtag  DW_TAG_loop_range
	.dwattr DW$282, DW_AT_low_pc(DW$L$_OP_Propagation$14$B)
	.dwattr DW$282, DW_AT_high_pc(DW$L$_OP_Propagation$14$E)
	.dwendtag DW$280

	.dwendtag DW$257

	.dwattr DW$253, DW_AT_end_file("OPManager.c")
	.dwattr DW$253, DW_AT_end_line(0x51d)
	.dwattr DW$253, DW_AT_end_column(0x01)
	.dwendtag DW$253

;******************************************************************************
;* STRINGS                                                                    *
;******************************************************************************
	.sect	".const"
SL1:	.string	"FIRST_SOL: dR:%lf, dV:%lf, Cnt:%d",13,10,0
;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_sprintf
	.global	_memset
	.global	_sqrt
	.global	_sin
	.global	_ldexp
	.global	_rk4
	.global	_assumedDynamics_Init
	.global	_assumedDynamics
	.global	_MJD_From_UTCEx
	.global	_CreateMatrixEx
	.global	_DestroyMatrixEx
	.global	_CopyMatrixEx
	.global	_MultMatrixEx
	.global	_MultMatrix_B_TC_Ex
	.global	_MultMatrix_SimResEx
	.global	_InvSymmetricalMatrixEx
	.global	_Compensate_Rotation
	.global	_ECEF_to_Geoid
	.global	__divd
	.global	__fltid
	.global	__subd
	.global	__mpyd
	.global	__addd
	.global	__cmpd
	.global	__fixdlli
	.global	__fixdi
	.global	_memcpy
	.global	__negd
	.global	__cvtfd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr DW$T$3, DW_AT_address_class(0x20)

DW$T$155	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$3)
	.dwattr DW$T$155, DW_AT_language(DW_LANG_C)
DW$283	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$284	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$285	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$154)
	.dwendtag DW$T$155


DW$T$58	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$58, DW_AT_language(DW_LANG_C)
DW$286	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$287	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$288	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$58

DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("tRightPart"), DW_AT_type(*DW$T$59)
	.dwattr DW$T$60, DW_AT_language(DW_LANG_C)

DW$T$162	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$162, DW_AT_language(DW_LANG_C)
DW$289	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$157)
DW$290	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$161)
DW$291	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$154)
	.dwendtag DW$T$162


DW$T$163	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$163, DW_AT_language(DW_LANG_C)
DW$292	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$60)
DW$293	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$294	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$295	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
DW$296	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$297	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
	.dwendtag DW$T$163


DW$T$166	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$166, DW_AT_language(DW_LANG_C)
DW$298	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$165)
	.dwendtag DW$T$166


DW$T$169	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$169, DW_AT_language(DW_LANG_C)
DW$299	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$168)
DW$300	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
	.dwendtag DW$T$169


DW$T$172	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$172, DW_AT_language(DW_LANG_C)
DW$301	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$302	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$303	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$172


DW$T$174	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$174, DW_AT_language(DW_LANG_C)
DW$304	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$T$174


DW$T$176	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$176, DW_AT_language(DW_LANG_C)
DW$305	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$306	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$T$176


DW$T$178	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$178, DW_AT_language(DW_LANG_C)
DW$307	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$308	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$309	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$T$178


DW$T$181	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$181, DW_AT_language(DW_LANG_C)
DW$310	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$180)
DW$311	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$180)
DW$312	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
	.dwendtag DW$T$181


DW$T$186	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$186, DW_AT_language(DW_LANG_C)
DW$313	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$183)
DW$314	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$184)
DW$315	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$185)
	.dwendtag DW$T$186


DW$T$190	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$190, DW_AT_language(DW_LANG_C)
DW$316	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$317	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$42)
	.dwendtag DW$T$190


DW$T$192	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$192, DW_AT_language(DW_LANG_C)
DW$318	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$319	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$31)
	.dwendtag DW$T$192


DW$T$203	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$203, DW_AT_language(DW_LANG_C)
DW$320	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$321	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$195)
DW$322	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$197)
DW$323	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$198)
DW$324	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$53)
DW$325	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$199)
DW$326	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$52)
DW$327	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$200)
DW$328	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$202)
	.dwendtag DW$T$203


DW$T$204	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$204, DW_AT_language(DW_LANG_C)
DW$329	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
	.dwendtag DW$T$204


DW$T$206	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$206, DW_AT_language(DW_LANG_C)
DW$330	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$331	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$195)
DW$332	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$197)
DW$333	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$168)
DW$334	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$52)
DW$335	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$200)
DW$336	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$202)
	.dwendtag DW$T$206


DW$T$208	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$208, DW_AT_language(DW_LANG_C)
DW$337	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$338	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$339	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$340	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
DW$341	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$342	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$T$208


DW$T$210	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$210, DW_AT_language(DW_LANG_C)
DW$343	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$344	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$20)
DW$345	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$346	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
	.dwendtag DW$T$210


DW$T$212	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$212, DW_AT_language(DW_LANG_C)
DW$347	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$348	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$349	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$350	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$351	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$212


DW$T$214	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$214, DW_AT_language(DW_LANG_C)
DW$352	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$353	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$354	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$355	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$356	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$357	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$214


DW$T$216	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$216, DW_AT_language(DW_LANG_C)
DW$358	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$359	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$195)
DW$360	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$168)
	.dwendtag DW$T$216

DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("uint8"), DW_AT_type(*DW$T$6)
	.dwattr DW$T$53, DW_AT_language(DW_LANG_C)
DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("int16"), DW_AT_type(*DW$T$8)
	.dwattr DW$T$31, DW_AT_language(DW_LANG_C)
DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("int32"), DW_AT_type(*DW$T$10)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)

DW$T$235	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$235, DW_AT_language(DW_LANG_C)
DW$361	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$362	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$198)
DW$363	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$53)
DW$364	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$199)
	.dwendtag DW$T$235


DW$T$236	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$236, DW_AT_language(DW_LANG_C)
DW$365	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$189)
DW$366	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$195)
DW$367	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$197)
DW$368	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$369	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$171)
DW$370	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$371	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$57)
DW$372	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$52)
DW$373	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$200)
DW$374	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$202)
	.dwendtag DW$T$236


DW$T$238	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$10)
	.dwattr DW$T$238, DW_AT_language(DW_LANG_C)
DW$375	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$73)
DW$376	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$77)
DW$377	.dwtag  DW_TAG_unspecified_parameters
	.dwendtag DW$T$238

DW$T$154	.dwtag  DW_TAG_typedef, DW_AT_name("size_t"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$154, DW_AT_language(DW_LANG_C)

DW$T$246	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$154)
	.dwattr DW$T$246, DW_AT_language(DW_LANG_C)
DW$378	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$77)
	.dwendtag DW$T$246

DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)
DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$20, DW_AT_language(DW_LANG_C)
DW$T$185	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$24)
	.dwattr DW$T$185, DW_AT_address_class(0x20)
DW$T$57	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$57, DW_AT_address_class(0x20)

DW$T$61	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$61, DW_AT_byte_size(0x40)
DW$379	.dwtag  DW_TAG_subrange_type
	.dwattr DW$379, DW_AT_upper_bound(0x07)
	.dwendtag DW$T$61


DW$T$254	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$254, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$254, DW_AT_byte_size(0x60)
DW$380	.dwtag  DW_TAG_subrange_type
	.dwattr DW$380, DW_AT_upper_bound(0x0b)
	.dwendtag DW$T$254


DW$T$255	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$255, DW_AT_language(DW_LANG_C)
DW$381	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$T$255


DW$T$257	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$257, DW_AT_language(DW_LANG_C)
DW$382	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
DW$383	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
	.dwendtag DW$T$257

DW$T$165	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$56)
	.dwattr DW$T$165, DW_AT_address_class(0x20)
DW$T$168	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$65)
	.dwattr DW$T$168, DW_AT_address_class(0x20)
DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("MatrixEx"), DW_AT_type(*DW$T$35)
	.dwattr DW$T$62, DW_AT_language(DW_LANG_C)
DW$T$171	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$62)
	.dwattr DW$T$171, DW_AT_address_class(0x20)
DW$T$180	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$100)
	.dwattr DW$T$180, DW_AT_address_class(0x20)
DW$T$183	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$45)
	.dwattr DW$T$183, DW_AT_address_class(0x20)
DW$T$184	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$44)
	.dwattr DW$T$184, DW_AT_address_class(0x20)
DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("int8"), DW_AT_type(*DW$T$41)
	.dwattr DW$T$42, DW_AT_language(DW_LANG_C)

DW$T$301	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$42)
	.dwattr DW$T$301, DW_AT_language(DW_LANG_C)
DW$384	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$219)
DW$385	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$221)
DW$386	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$222)
DW$387	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$52)
DW$388	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$52)
DW$389	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$200)
DW$390	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$202)
	.dwendtag DW$T$301


DW$T$302	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$42)
	.dwattr DW$T$302, DW_AT_language(DW_LANG_C)
DW$391	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$219)
DW$392	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$221)
DW$393	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$52)
DW$394	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$200)
DW$395	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$202)
	.dwendtag DW$T$302

DW$T$73	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$41)
	.dwattr DW$T$73, DW_AT_address_class(0x20)
DW$T$77	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$76)
	.dwattr DW$T$77, DW_AT_address_class(0x20)
DW$T$198	.dwtag  DW_TAG_typedef, DW_AT_name("USER"), DW_AT_type(*DW$T$49)
	.dwattr DW$T$198, DW_AT_language(DW_LANG_C)
DW$T$219	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$218)
	.dwattr DW$T$219, DW_AT_address_class(0x20)
DW$T$305	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$198)
	.dwattr DW$T$305, DW_AT_address_class(0x20)
DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("eFlag"), DW_AT_type(*DW$T$51)
	.dwattr DW$T$52, DW_AT_language(DW_LANG_C)
DW$T$222	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$66)
	.dwattr DW$T$222, DW_AT_address_class(0x20)
DW$T$189	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$188)
	.dwattr DW$T$189, DW_AT_address_class(0x20)
DW$T$221	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$220)
	.dwattr DW$T$221, DW_AT_address_class(0x20)
DW$T$200	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$96)
	.dwattr DW$T$200, DW_AT_address_class(0x20)
DW$T$202	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$201)
	.dwattr DW$T$202, DW_AT_address_class(0x20)
DW$T$195	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$194)
	.dwattr DW$T$195, DW_AT_address_class(0x20)
DW$T$197	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$196)
	.dwattr DW$T$197, DW_AT_address_class(0x20)
DW$T$199	.dwtag  DW_TAG_typedef, DW_AT_name("tWeekData"), DW_AT_type(*DW$T$153)
	.dwattr DW$T$199, DW_AT_language(DW_LANG_C)
DW$T$385	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$199)
	.dwattr DW$T$385, DW_AT_address_class(0x20)
DW$T$157	.dwtag  DW_TAG_restrict_type
	.dwattr DW$T$157, DW_AT_type(*DW$T$3)
DW$T$59	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$58)
	.dwattr DW$T$59, DW_AT_address_class(0x20)
DW$T$161	.dwtag  DW_TAG_restrict_type
	.dwattr DW$T$161, DW_AT_type(*DW$T$160)
DW$T$6	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned char")
	.dwattr DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr DW$T$6, DW_AT_byte_size(0x01)
DW$T$8	.dwtag  DW_TAG_base_type, DW_AT_name("short")
	.dwattr DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$8, DW_AT_byte_size(0x02)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)

DW$T$24	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$24, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$24, DW_AT_byte_size(0x18)
DW$396	.dwtag  DW_TAG_subrange_type
	.dwattr DW$396, DW_AT_upper_bound(0x02)
	.dwendtag DW$T$24

DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("tDynamicsModel"), DW_AT_type(*DW$T$30)
	.dwattr DW$T$56, DW_AT_language(DW_LANG_C)
DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("tGPSTime"), DW_AT_type(*DW$T$32)
	.dwattr DW$T$65, DW_AT_language(DW_LANG_C)

DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$35, DW_AT_byte_size(0x1208)
DW$397	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$34)
	.dwattr DW$397, DW_AT_name("Matr"), DW_AT_symbol_name("_Matr")
	.dwattr DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$397, DW_AT_accessibility(DW_ACCESS_public)
DW$398	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$398, DW_AT_name("mRow"), DW_AT_symbol_name("_mRow")
	.dwattr DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x1200]
	.dwattr DW$398, DW_AT_accessibility(DW_ACCESS_public)
DW$399	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$399, DW_AT_name("mCol"), DW_AT_symbol_name("_mCol")
	.dwattr DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x1204]
	.dwattr DW$399, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$35

DW$T$100	.dwtag  DW_TAG_typedef, DW_AT_name("tCoord"), DW_AT_type(*DW$T$36)
	.dwattr DW$T$100, DW_AT_language(DW_LANG_C)
DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("tLLA"), DW_AT_type(*DW$T$37)
	.dwattr DW$T$45, DW_AT_language(DW_LANG_C)
DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("tECEF"), DW_AT_type(*DW$T$38)
	.dwattr DW$T$44, DW_AT_language(DW_LANG_C)
DW$T$41	.dwtag  DW_TAG_base_type, DW_AT_name("signed char")
	.dwattr DW$T$41, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr DW$T$41, DW_AT_byte_size(0x01)
DW$T$76	.dwtag  DW_TAG_const_type
	.dwattr DW$T$76, DW_AT_type(*DW$T$41)

DW$T$49	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$49, DW_AT_byte_size(0x1d0)
DW$400	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$400, DW_AT_name("CoordValid"), DW_AT_symbol_name("_CoordValid")
	.dwattr DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$400, DW_AT_accessibility(DW_ACCESS_public)
DW$401	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$401, DW_AT_name("SolutionFlag"), DW_AT_symbol_name("_SolutionFlag")
	.dwattr DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr DW$401, DW_AT_accessibility(DW_ACCESS_public)
DW$402	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$44)
	.dwattr DW$402, DW_AT_name("ecef"), DW_AT_symbol_name("_ecef")
	.dwattr DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$402, DW_AT_accessibility(DW_ACCESS_public)
DW$403	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$45)
	.dwattr DW$403, DW_AT_name("geo"), DW_AT_symbol_name("_geo")
	.dwattr DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$403, DW_AT_accessibility(DW_ACCESS_public)
DW$404	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$46)
	.dwattr DW$404, DW_AT_name("oe"), DW_AT_symbol_name("_oe")
	.dwattr DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr DW$404, DW_AT_accessibility(DW_ACCESS_public)
DW$405	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$405, DW_AT_name("R"), DW_AT_symbol_name("_R")
	.dwattr DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr DW$405, DW_AT_accessibility(DW_ACCESS_public)
DW$406	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$406, DW_AT_name("Rdot"), DW_AT_symbol_name("_Rdot")
	.dwattr DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr DW$406, DW_AT_accessibility(DW_ACCESS_public)
DW$407	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$407, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr DW$407, DW_AT_accessibility(DW_ACCESS_public)
DW$408	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$408, DW_AT_name("t"), DW_AT_symbol_name("_t")
	.dwattr DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr DW$408, DW_AT_accessibility(DW_ACCESS_public)
DW$409	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$47)
	.dwattr DW$409, DW_AT_name("TOPO"), DW_AT_symbol_name("_TOPO")
	.dwattr DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr DW$409, DW_AT_accessibility(DW_ACCESS_public)
DW$410	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$410, DW_AT_name("GDOP"), DW_AT_symbol_name("_GDOP")
	.dwattr DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr DW$410, DW_AT_accessibility(DW_ACCESS_public)
DW$411	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$411, DW_AT_name("PDOP"), DW_AT_symbol_name("_PDOP")
	.dwattr DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr DW$411, DW_AT_accessibility(DW_ACCESS_public)
DW$412	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$412, DW_AT_name("TDOP"), DW_AT_symbol_name("_TDOP")
	.dwattr DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr DW$412, DW_AT_accessibility(DW_ACCESS_public)
DW$413	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$413, DW_AT_name("HDOP"), DW_AT_symbol_name("_HDOP")
	.dwattr DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x118]
	.dwattr DW$413, DW_AT_accessibility(DW_ACCESS_public)
DW$414	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$414, DW_AT_name("VDOP"), DW_AT_symbol_name("_VDOP")
	.dwattr DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x120]
	.dwattr DW$414, DW_AT_accessibility(DW_ACCESS_public)
DW$415	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$48)
	.dwattr DW$415, DW_AT_name("User_fix"), DW_AT_symbol_name("_User_fix")
	.dwattr DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x128]
	.dwattr DW$415, DW_AT_accessibility(DW_ACCESS_public)
DW$416	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$416, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c8]
	.dwattr DW$416, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$49

DW$T$218	.dwtag  DW_TAG_const_type
	.dwattr DW$T$218, DW_AT_type(*DW$T$198)

DW$T$51	.dwtag  DW_TAG_enumeration_type
	.dwattr DW$T$51, DW_AT_name("Flag")
	.dwattr DW$T$51, DW_AT_byte_size(0x04)
DW$417	.dwtag  DW_TAG_enumerator, DW_AT_name("fg_ON"), DW_AT_const_value(0x01)
DW$418	.dwtag  DW_TAG_enumerator, DW_AT_name("fg_OFF"), DW_AT_const_value(0x00)
	.dwendtag DW$T$51

DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("tSolutionStatus"), DW_AT_type(*DW$T$55)
	.dwattr DW$T$66, DW_AT_language(DW_LANG_C)
DW$T$188	.dwtag  DW_TAG_typedef, DW_AT_name("tOPManager"), DW_AT_type(*DW$T$67)
	.dwattr DW$T$188, DW_AT_language(DW_LANG_C)
DW$T$220	.dwtag  DW_TAG_const_type
	.dwattr DW$T$220, DW_AT_type(*DW$T$188)
DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("DATA_STRING"), DW_AT_type(*DW$T$69)
	.dwattr DW$T$96, DW_AT_language(DW_LANG_C)
DW$T$201	.dwtag  DW_TAG_typedef, DW_AT_name("tTerminal"), DW_AT_type(*DW$T$97)
	.dwattr DW$T$201, DW_AT_language(DW_LANG_C)
DW$T$194	.dwtag  DW_TAG_typedef, DW_AT_name("tTopSystem"), DW_AT_type(*DW$T$148)
	.dwattr DW$T$194, DW_AT_language(DW_LANG_C)
DW$T$196	.dwtag  DW_TAG_typedef, DW_AT_name("NAV_CONDITIONS"), DW_AT_type(*DW$T$152)
	.dwattr DW$T$196, DW_AT_language(DW_LANG_C)

DW$T$153	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$153, DW_AT_byte_size(0x08)
DW$419	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$419, DW_AT_name("wnFlash"), DW_AT_symbol_name("_wnFlash")
	.dwattr DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$419, DW_AT_accessibility(DW_ACCESS_public)
DW$420	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$420, DW_AT_name("cntOverFlow"), DW_AT_symbol_name("_cntOverFlow")
	.dwattr DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr DW$420, DW_AT_accessibility(DW_ACCESS_public)
DW$421	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$52)
	.dwattr DW$421, DW_AT_name("DataError"), DW_AT_symbol_name("_DataError")
	.dwattr DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$421, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$153

DW$T$160	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$159)
	.dwattr DW$T$160, DW_AT_address_class(0x20)

DW$T$47	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$47, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$47, DW_AT_byte_size(0x48)
DW$422	.dwtag  DW_TAG_subrange_type
	.dwattr DW$422, DW_AT_upper_bound(0x02)
DW$423	.dwtag  DW_TAG_subrange_type
	.dwattr DW$423, DW_AT_upper_bound(0x02)
	.dwendtag DW$T$47


DW$T$34	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$34, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$34, DW_AT_byte_size(0x1200)
DW$424	.dwtag  DW_TAG_subrange_type
	.dwattr DW$424, DW_AT_upper_bound(0x17)
DW$425	.dwtag  DW_TAG_subrange_type
	.dwattr DW$425, DW_AT_upper_bound(0x17)
	.dwendtag DW$T$34


DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$30, DW_AT_name("_tDynamicsModel")
	.dwattr DW$T$30, DW_AT_byte_size(0x38c8)
DW$426	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$27)
	.dwattr DW$426, DW_AT_name("GP"), DW_AT_symbol_name("_GP")
	.dwattr DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$426, DW_AT_accessibility(DW_ACCESS_public)
DW$427	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$28)
	.dwattr DW$427, DW_AT_name("SMD"), DW_AT_symbol_name("_SMD")
	.dwattr DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x3870]
	.dwattr DW$427, DW_AT_accessibility(DW_ACCESS_public)
DW$428	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$29)
	.dwattr DW$428, DW_AT_name("Atm"), DW_AT_symbol_name("_Atm")
	.dwattr DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x38a0]
	.dwattr DW$428, DW_AT_accessibility(DW_ACCESS_public)
DW$429	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$429, DW_AT_name("GP_N"), DW_AT_symbol_name("_GP_N")
	.dwattr DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x38b8]
	.dwattr DW$429, DW_AT_accessibility(DW_ACCESS_public)
DW$430	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$430, DW_AT_name("SM_fUsing"), DW_AT_symbol_name("_SM_fUsing")
	.dwattr DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x38bc]
	.dwattr DW$430, DW_AT_accessibility(DW_ACCESS_public)
DW$431	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$431, DW_AT_name("OBJ_Sigma"), DW_AT_symbol_name("_OBJ_Sigma")
	.dwattr DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x38c0]
	.dwattr DW$431, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$30


DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$32, DW_AT_name("__tGPSTime")
	.dwattr DW$T$32, DW_AT_byte_size(0x10)
DW$432	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$432, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$432, DW_AT_accessibility(DW_ACCESS_public)
DW$433	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$433, DW_AT_name("t"), DW_AT_symbol_name("_t")
	.dwattr DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$433, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$32


DW$T$36	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$36, DW_AT_byte_size(0x38)
DW$434	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$434, DW_AT_name("dt_SV"), DW_AT_symbol_name("_dt_SV")
	.dwattr DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$434, DW_AT_accessibility(DW_ACCESS_public)
DW$435	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$435, DW_AT_name("x"), DW_AT_symbol_name("_x")
	.dwattr DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$435, DW_AT_accessibility(DW_ACCESS_public)
DW$436	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$436, DW_AT_name("y"), DW_AT_symbol_name("_y")
	.dwattr DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$436, DW_AT_accessibility(DW_ACCESS_public)
DW$437	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$437, DW_AT_name("z"), DW_AT_symbol_name("_z")
	.dwattr DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$437, DW_AT_accessibility(DW_ACCESS_public)
DW$438	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$438, DW_AT_name("vx"), DW_AT_symbol_name("_vx")
	.dwattr DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$438, DW_AT_accessibility(DW_ACCESS_public)
DW$439	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$439, DW_AT_name("vy"), DW_AT_symbol_name("_vy")
	.dwattr DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$439, DW_AT_accessibility(DW_ACCESS_public)
DW$440	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$440, DW_AT_name("vz"), DW_AT_symbol_name("_vz")
	.dwattr DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$440, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$36


DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$37, DW_AT_byte_size(0x30)
DW$441	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$441, DW_AT_name("Ndot"), DW_AT_symbol_name("_Ndot")
	.dwattr DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$441, DW_AT_accessibility(DW_ACCESS_public)
DW$442	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$442, DW_AT_name("Edot"), DW_AT_symbol_name("_Edot")
	.dwattr DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$442, DW_AT_accessibility(DW_ACCESS_public)
DW$443	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$443, DW_AT_name("Udot"), DW_AT_symbol_name("_Udot")
	.dwattr DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$443, DW_AT_accessibility(DW_ACCESS_public)
DW$444	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$444, DW_AT_name("Lon"), DW_AT_symbol_name("_Lon")
	.dwattr DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$444, DW_AT_accessibility(DW_ACCESS_public)
DW$445	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$445, DW_AT_name("Lat"), DW_AT_symbol_name("_Lat")
	.dwattr DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$445, DW_AT_accessibility(DW_ACCESS_public)
DW$446	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$446, DW_AT_name("H"), DW_AT_symbol_name("_H")
	.dwattr DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$446, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$37


DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$38, DW_AT_byte_size(0x30)
DW$447	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$447, DW_AT_name("Xdot"), DW_AT_symbol_name("_Xdot")
	.dwattr DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$447, DW_AT_accessibility(DW_ACCESS_public)
DW$448	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$448, DW_AT_name("Ydot"), DW_AT_symbol_name("_Ydot")
	.dwattr DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$448, DW_AT_accessibility(DW_ACCESS_public)
DW$449	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$449, DW_AT_name("Zdot"), DW_AT_symbol_name("_Zdot")
	.dwattr DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$449, DW_AT_accessibility(DW_ACCESS_public)
DW$450	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$450, DW_AT_name("X"), DW_AT_symbol_name("_X")
	.dwattr DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$450, DW_AT_accessibility(DW_ACCESS_public)
DW$451	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$451, DW_AT_name("Y"), DW_AT_symbol_name("_Y")
	.dwattr DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$451, DW_AT_accessibility(DW_ACCESS_public)
DW$452	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$452, DW_AT_name("Z"), DW_AT_symbol_name("_Z")
	.dwattr DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$452, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$38

DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("tOE"), DW_AT_type(*DW$T$39)
	.dwattr DW$T$46, DW_AT_language(DW_LANG_C)
DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("USERfix"), DW_AT_type(*DW$T$43)
	.dwattr DW$T$48, DW_AT_language(DW_LANG_C)

DW$T$55	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$55, DW_AT_name("__tSolutionStatus")
	.dwattr DW$T$55, DW_AT_byte_size(0x18)
DW$453	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$453, DW_AT_name("SolutionCount"), DW_AT_symbol_name("_SolutionCount")
	.dwattr DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$453, DW_AT_accessibility(DW_ACCESS_public)
DW$454	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$454, DW_AT_name("dR"), DW_AT_symbol_name("_dR")
	.dwattr DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$454, DW_AT_accessibility(DW_ACCESS_public)
DW$455	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$455, DW_AT_name("dV"), DW_AT_symbol_name("_dV")
	.dwattr DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$455, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$55


DW$T$67	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$67, DW_AT_name("__tOPManager")
	.dwattr DW$T$67, DW_AT_byte_size(0x4c70)
DW$456	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$56)
	.dwattr DW$456, DW_AT_name("DM"), DW_AT_symbol_name("_DM")
	.dwattr DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$456, DW_AT_accessibility(DW_ACCESS_public)
DW$457	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$60)
	.dwattr DW$457, DW_AT_name("pfDM"), DW_AT_symbol_name("_pfDM")
	.dwattr DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x38c8]
	.dwattr DW$457, DW_AT_accessibility(DW_ACCESS_public)
DW$458	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$458, DW_AT_name("R_pr"), DW_AT_symbol_name("_R_pr")
	.dwattr DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x38d0]
	.dwattr DW$458, DW_AT_accessibility(DW_ACCESS_public)
DW$459	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$459, DW_AT_name("R_prr"), DW_AT_symbol_name("_R_prr")
	.dwattr DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x38d8]
	.dwattr DW$459, DW_AT_accessibility(DW_ACCESS_public)
DW$460	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$61)
	.dwattr DW$460, DW_AT_name("Qorb"), DW_AT_symbol_name("_Qorb")
	.dwattr DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x38e0]
	.dwattr DW$460, DW_AT_accessibility(DW_ACCESS_public)
DW$461	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$61)
	.dwattr DW$461, DW_AT_name("X"), DW_AT_symbol_name("_X")
	.dwattr DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x3920]
	.dwattr DW$461, DW_AT_accessibility(DW_ACCESS_public)
DW$462	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$62)
	.dwattr DW$462, DW_AT_name("P0"), DW_AT_symbol_name("_P0")
	.dwattr DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x3960]
	.dwattr DW$462, DW_AT_accessibility(DW_ACCESS_public)
DW$463	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$63)
	.dwattr DW$463, DW_AT_name("User_OP"), DW_AT_symbol_name("_User_OP")
	.dwattr DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x4b68]
	.dwattr DW$463, DW_AT_accessibility(DW_ACCESS_public)
DW$464	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$64)
	.dwattr DW$464, DW_AT_name("CorrectTimeOP"), DW_AT_symbol_name("_CorrectTimeOP")
	.dwattr DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c00]
	.dwattr DW$464, DW_AT_accessibility(DW_ACCESS_public)
DW$465	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$65)
	.dwattr DW$465, DW_AT_name("btLastUpdate"), DW_AT_symbol_name("_btLastUpdate")
	.dwattr DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c30]
	.dwattr DW$465, DW_AT_accessibility(DW_ACCESS_public)
DW$466	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$65)
	.dwattr DW$466, DW_AT_name("btLastEstimation"), DW_AT_symbol_name("_btLastEstimation")
	.dwattr DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c40]
	.dwattr DW$466, DW_AT_accessibility(DW_ACCESS_public)
DW$467	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$467, DW_AT_name("mOP_DynModelType"), DW_AT_symbol_name("_mOP_DynModelType")
	.dwattr DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c50]
	.dwattr DW$467, DW_AT_accessibility(DW_ACCESS_public)
DW$468	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$468, DW_AT_name("fOP_Condition"), DW_AT_symbol_name("_fOP_Condition")
	.dwattr DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c51]
	.dwattr DW$468, DW_AT_accessibility(DW_ACCESS_public)
DW$469	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$469, DW_AT_name("fOP_Validity"), DW_AT_symbol_name("_fOP_Validity")
	.dwattr DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c52]
	.dwattr DW$469, DW_AT_accessibility(DW_ACCESS_public)
DW$470	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$470, DW_AT_name("fSynchronization"), DW_AT_symbol_name("_fSynchronization")
	.dwattr DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c53]
	.dwattr DW$470, DW_AT_accessibility(DW_ACCESS_public)
DW$471	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$66)
	.dwattr DW$471, DW_AT_name("solutionStatus"), DW_AT_symbol_name("_solutionStatus")
	.dwattr DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c58]
	.dwattr DW$471, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$67


DW$T$69	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$69, DW_AT_byte_size(0x204)
DW$472	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$68)
	.dwattr DW$472, DW_AT_name("str"), DW_AT_symbol_name("_str")
	.dwattr DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$472, DW_AT_accessibility(DW_ACCESS_public)
DW$473	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$473, DW_AT_name("length"), DW_AT_symbol_name("_length")
	.dwattr DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x200]
	.dwattr DW$473, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$69


DW$T$97	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$97, DW_AT_name("tagTerminal")
	.dwattr DW$T$97, DW_AT_byte_size(0x2444)
DW$474	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$93)
	.dwattr DW$474, DW_AT_name("vtable"), DW_AT_symbol_name("_vtable")
	.dwattr DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$474, DW_AT_accessibility(DW_ACCESS_public)
DW$475	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$475, DW_AT_name("ParcelOfData"), DW_AT_symbol_name("_ParcelOfData")
	.dwattr DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$475, DW_AT_accessibility(DW_ACCESS_public)
DW$476	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$94)
	.dwattr DW$476, DW_AT_name("TransmitQueue"), DW_AT_symbol_name("_TransmitQueue")
	.dwattr DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr DW$476, DW_AT_accessibility(DW_ACCESS_public)
DW$477	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$94)
	.dwattr DW$477, DW_AT_name("ReceiveQueue"), DW_AT_symbol_name("_ReceiveQueue")
	.dwattr DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr DW$477, DW_AT_accessibility(DW_ACCESS_public)
DW$478	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$95)
	.dwattr DW$478, DW_AT_name("m_WriteBuffer"), DW_AT_symbol_name("_m_WriteBuffer")
	.dwattr DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr DW$478, DW_AT_accessibility(DW_ACCESS_public)
DW$479	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$95)
	.dwattr DW$479, DW_AT_name("m_ReadBuffer"), DW_AT_symbol_name("_m_ReadBuffer")
	.dwattr DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x1034]
	.dwattr DW$479, DW_AT_accessibility(DW_ACCESS_public)
DW$480	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$480, DW_AT_name("FlagRead"), DW_AT_symbol_name("_FlagRead")
	.dwattr DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x2034]
	.dwattr DW$480, DW_AT_accessibility(DW_ACCESS_public)
DW$481	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$96)
	.dwattr DW$481, DW_AT_name("stringWrite"), DW_AT_symbol_name("_stringWrite")
	.dwattr DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x2038]
	.dwattr DW$481, DW_AT_accessibility(DW_ACCESS_public)
DW$482	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$96)
	.dwattr DW$482, DW_AT_name("stringRead"), DW_AT_symbol_name("_stringRead")
	.dwattr DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x223c]
	.dwattr DW$482, DW_AT_accessibility(DW_ACCESS_public)
DW$483	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$52)
	.dwattr DW$483, DW_AT_name("FlagUART"), DW_AT_symbol_name("_FlagUART")
	.dwattr DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x2440]
	.dwattr DW$483, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$97


DW$T$148	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$148, DW_AT_byte_size(0x3eb8)
DW$484	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$140)
	.dwattr DW$484, DW_AT_name("navtask_params"), DW_AT_symbol_name("_navtask_params")
	.dwattr DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$484, DW_AT_accessibility(DW_ACCESS_public)
DW$485	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$141)
	.dwattr DW$485, DW_AT_name("usedSV"), DW_AT_symbol_name("_usedSV")
	.dwattr DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x1020]
	.dwattr DW$485, DW_AT_accessibility(DW_ACCESS_public)
DW$486	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$142)
	.dwattr DW$486, DW_AT_name("DecCommon"), DW_AT_symbol_name("_DecCommon")
	.dwattr DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x1040]
	.dwattr DW$486, DW_AT_accessibility(DW_ACCESS_public)
DW$487	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$144)
	.dwattr DW$487, DW_AT_name("AlmGPS"), DW_AT_symbol_name("_AlmGPS")
	.dwattr DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x2568]
	.dwattr DW$487, DW_AT_accessibility(DW_ACCESS_public)
DW$488	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$145)
	.dwattr DW$488, DW_AT_name("IonUTC"), DW_AT_symbol_name("_IonUTC")
	.dwattr DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d68]
	.dwattr DW$488, DW_AT_accessibility(DW_ACCESS_public)
DW$489	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$147)
	.dwattr DW$489, DW_AT_name("EphGPS"), DW_AT_symbol_name("_EphGPS")
	.dwattr DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x2da8]
	.dwattr DW$489, DW_AT_accessibility(DW_ACCESS_public)
DW$490	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$490, DW_AT_name("eph_set_flgs"), DW_AT_symbol_name("_eph_set_flgs")
	.dwattr DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x3ea8]
	.dwattr DW$490, DW_AT_accessibility(DW_ACCESS_public)
DW$491	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$491, DW_AT_name("alm_set_flgs"), DW_AT_symbol_name("_alm_set_flgs")
	.dwattr DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x3eac]
	.dwattr DW$491, DW_AT_accessibility(DW_ACCESS_public)
DW$492	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$492, DW_AT_name("health_flgs"), DW_AT_symbol_name("_health_flgs")
	.dwattr DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x3eb0]
	.dwattr DW$492, DW_AT_accessibility(DW_ACCESS_public)
DW$493	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$493, DW_AT_name("visibility_flgs"), DW_AT_symbol_name("_visibility_flgs")
	.dwattr DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x3eb4]
	.dwattr DW$493, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$148


DW$T$152	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$152, DW_AT_byte_size(0x68)
DW$494	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$494, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$494, DW_AT_accessibility(DW_ACCESS_public)
DW$495	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$495, DW_AT_name("t"), DW_AT_symbol_name("_t")
	.dwattr DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$495, DW_AT_accessibility(DW_ACCESS_public)
DW$496	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$496, DW_AT_name("wnOP"), DW_AT_symbol_name("_wnOP")
	.dwattr DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$496, DW_AT_accessibility(DW_ACCESS_public)
DW$497	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$497, DW_AT_name("tOP"), DW_AT_symbol_name("_tOP")
	.dwattr DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$497, DW_AT_accessibility(DW_ACCESS_public)
DW$498	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$151)
	.dwattr DW$498, DW_AT_name("NavSV"), DW_AT_symbol_name("_NavSV")
	.dwattr DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$498, DW_AT_accessibility(DW_ACCESS_public)
DW$499	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$151)
	.dwattr DW$499, DW_AT_name("NavSVOld"), DW_AT_symbol_name("_NavSVOld")
	.dwattr DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d]
	.dwattr DW$499, DW_AT_accessibility(DW_ACCESS_public)
DW$500	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$500, DW_AT_name("alm_set_flgs"), DW_AT_symbol_name("_alm_set_flgs")
	.dwattr DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr DW$500, DW_AT_accessibility(DW_ACCESS_public)
DW$501	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$501, DW_AT_name("health_flgs"), DW_AT_symbol_name("_health_flgs")
	.dwattr DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$501, DW_AT_accessibility(DW_ACCESS_public)
DW$502	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$502, DW_AT_name("visibility_flgs"), DW_AT_symbol_name("_visibility_flgs")
	.dwattr DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr DW$502, DW_AT_accessibility(DW_ACCESS_public)
DW$503	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$503, DW_AT_name("ElevMask"), DW_AT_symbol_name("_ElevMask")
	.dwattr DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr DW$503, DW_AT_accessibility(DW_ACCESS_public)
DW$504	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$504, DW_AT_name("IonosphereModel"), DW_AT_symbol_name("_IonosphereModel")
	.dwattr DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr DW$504, DW_AT_accessibility(DW_ACCESS_public)
DW$505	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$505, DW_AT_name("TroposphereModel"), DW_AT_symbol_name("_TroposphereModel")
	.dwattr DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr DW$505, DW_AT_accessibility(DW_ACCESS_public)
DW$506	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$506, DW_AT_name("OrbStatic"), DW_AT_symbol_name("_OrbStatic")
	.dwattr DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e]
	.dwattr DW$506, DW_AT_accessibility(DW_ACCESS_public)
DW$507	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$507, DW_AT_name("OPmode"), DW_AT_symbol_name("_OPmode")
	.dwattr DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x4f]
	.dwattr DW$507, DW_AT_accessibility(DW_ACCESS_public)
DW$508	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$508, DW_AT_name("DOP"), DW_AT_symbol_name("_DOP")
	.dwattr DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr DW$508, DW_AT_accessibility(DW_ACCESS_public)
DW$509	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$509, DW_AT_name("timeTracking"), DW_AT_symbol_name("_timeTracking")
	.dwattr DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr DW$509, DW_AT_accessibility(DW_ACCESS_public)
DW$510	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$510, DW_AT_name("RAIMKey"), DW_AT_symbol_name("_RAIMKey")
	.dwattr DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr DW$510, DW_AT_accessibility(DW_ACCESS_public)
DW$511	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$511, DW_AT_name("UERE"), DW_AT_symbol_name("_UERE")
	.dwattr DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr DW$511, DW_AT_accessibility(DW_ACCESS_public)
DW$512	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$512, DW_AT_name("CPU"), DW_AT_symbol_name("_CPU")
	.dwattr DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr DW$512, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$152

DW$T$159	.dwtag  DW_TAG_const_type
DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("uint32"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$89, DW_AT_language(DW_LANG_C)
DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("float32"), DW_AT_type(*DW$T$16)
	.dwattr DW$T$98, DW_AT_language(DW_LANG_C)
DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("tGeopotential"), DW_AT_type(*DW$T$23)
	.dwattr DW$T$27, DW_AT_language(DW_LANG_C)
DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("tSunMoonData"), DW_AT_type(*DW$T$25)
	.dwattr DW$T$28, DW_AT_language(DW_LANG_C)
DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("tAtmosphere"), DW_AT_type(*DW$T$26)
	.dwattr DW$T$29, DW_AT_language(DW_LANG_C)

DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$39, DW_AT_byte_size(0x30)
DW$513	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$513, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$513, DW_AT_accessibility(DW_ACCESS_public)
DW$514	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$514, DW_AT_name("Ecc"), DW_AT_symbol_name("_Ecc")
	.dwattr DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$514, DW_AT_accessibility(DW_ACCESS_public)
DW$515	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$515, DW_AT_name("I"), DW_AT_symbol_name("_I")
	.dwattr DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$515, DW_AT_accessibility(DW_ACCESS_public)
DW$516	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$516, DW_AT_name("Omega"), DW_AT_symbol_name("_Omega")
	.dwattr DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$516, DW_AT_accessibility(DW_ACCESS_public)
DW$517	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$517, DW_AT_name("OmegaP"), DW_AT_symbol_name("_OmegaP")
	.dwattr DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$517, DW_AT_accessibility(DW_ACCESS_public)
DW$518	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$518, DW_AT_name("M"), DW_AT_symbol_name("_M")
	.dwattr DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$518, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$39


DW$T$68	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$42)
	.dwattr DW$T$68, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$68, DW_AT_byte_size(0x200)
DW$519	.dwtag  DW_TAG_subrange_type
	.dwattr DW$519, DW_AT_upper_bound(0x1ff)
	.dwendtag DW$T$68


DW$T$141	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$42)
	.dwattr DW$T$141, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$141, DW_AT_byte_size(0x20)
DW$520	.dwtag  DW_TAG_subrange_type
	.dwattr DW$520, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$141


DW$T$95	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$41)
	.dwattr DW$T$95, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$95, DW_AT_byte_size(0x1000)
DW$521	.dwtag  DW_TAG_subrange_type
	.dwattr DW$521, DW_AT_upper_bound(0xfff)
	.dwendtag DW$T$95


DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$43, DW_AT_byte_size(0xa0)
DW$522	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$522, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$522, DW_AT_accessibility(DW_ACCESS_public)
DW$523	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$523, DW_AT_name("t"), DW_AT_symbol_name("_t")
	.dwattr DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$523, DW_AT_accessibility(DW_ACCESS_public)
DW$524	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$524, DW_AT_name("X"), DW_AT_symbol_name("_X")
	.dwattr DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$524, DW_AT_accessibility(DW_ACCESS_public)
DW$525	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$525, DW_AT_name("Y"), DW_AT_symbol_name("_Y")
	.dwattr DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$525, DW_AT_accessibility(DW_ACCESS_public)
DW$526	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$526, DW_AT_name("Z"), DW_AT_symbol_name("_Z")
	.dwattr DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$526, DW_AT_accessibility(DW_ACCESS_public)
DW$527	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$527, DW_AT_name("Xdot"), DW_AT_symbol_name("_Xdot")
	.dwattr DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$527, DW_AT_accessibility(DW_ACCESS_public)
DW$528	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$528, DW_AT_name("Ydot"), DW_AT_symbol_name("_Ydot")
	.dwattr DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr DW$528, DW_AT_accessibility(DW_ACCESS_public)
DW$529	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$529, DW_AT_name("Zdot"), DW_AT_symbol_name("_Zdot")
	.dwattr DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$529, DW_AT_accessibility(DW_ACCESS_public)
DW$530	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$530, DW_AT_name("Lon"), DW_AT_symbol_name("_Lon")
	.dwattr DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$530, DW_AT_accessibility(DW_ACCESS_public)
DW$531	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$531, DW_AT_name("Lat"), DW_AT_symbol_name("_Lat")
	.dwattr DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$531, DW_AT_accessibility(DW_ACCESS_public)
DW$532	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$532, DW_AT_name("H"), DW_AT_symbol_name("_H")
	.dwattr DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr DW$532, DW_AT_accessibility(DW_ACCESS_public)
DW$533	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$533, DW_AT_name("Ndot"), DW_AT_symbol_name("_Ndot")
	.dwattr DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr DW$533, DW_AT_accessibility(DW_ACCESS_public)
DW$534	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$534, DW_AT_name("Edot"), DW_AT_symbol_name("_Edot")
	.dwattr DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr DW$534, DW_AT_accessibility(DW_ACCESS_public)
DW$535	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$535, DW_AT_name("Udot"), DW_AT_symbol_name("_Udot")
	.dwattr DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr DW$535, DW_AT_accessibility(DW_ACCESS_public)
DW$536	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$536, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr DW$536, DW_AT_accessibility(DW_ACCESS_public)
DW$537	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$537, DW_AT_name("Omega"), DW_AT_symbol_name("_Omega")
	.dwattr DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr DW$537, DW_AT_accessibility(DW_ACCESS_public)
DW$538	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$538, DW_AT_name("M"), DW_AT_symbol_name("_M")
	.dwattr DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr DW$538, DW_AT_accessibility(DW_ACCESS_public)
DW$539	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$539, DW_AT_name("I"), DW_AT_symbol_name("_I")
	.dwattr DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr DW$539, DW_AT_accessibility(DW_ACCESS_public)
DW$540	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$540, DW_AT_name("OmegaP"), DW_AT_symbol_name("_OmegaP")
	.dwattr DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr DW$540, DW_AT_accessibility(DW_ACCESS_public)
DW$541	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$541, DW_AT_name("Ecc"), DW_AT_symbol_name("_Ecc")
	.dwattr DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr DW$541, DW_AT_accessibility(DW_ACCESS_public)
DW$542	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$542, DW_AT_name("R"), DW_AT_symbol_name("_R")
	.dwattr DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr DW$542, DW_AT_accessibility(DW_ACCESS_public)
DW$543	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$543, DW_AT_name("OscillOffset"), DW_AT_symbol_name("_OscillOffset")
	.dwattr DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr DW$543, DW_AT_accessibility(DW_ACCESS_public)
DW$544	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$544, DW_AT_name("GDOP"), DW_AT_symbol_name("_GDOP")
	.dwattr DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x94]
	.dwattr DW$544, DW_AT_accessibility(DW_ACCESS_public)
DW$545	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$545, DW_AT_name("PDOP"), DW_AT_symbol_name("_PDOP")
	.dwattr DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x96]
	.dwattr DW$545, DW_AT_accessibility(DW_ACCESS_public)
DW$546	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$546, DW_AT_name("TDOP"), DW_AT_symbol_name("_TDOP")
	.dwattr DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr DW$546, DW_AT_accessibility(DW_ACCESS_public)
DW$547	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$547, DW_AT_name("HDOP"), DW_AT_symbol_name("_HDOP")
	.dwattr DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x9a]
	.dwattr DW$547, DW_AT_accessibility(DW_ACCESS_public)
DW$548	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$548, DW_AT_name("VDOP"), DW_AT_symbol_name("_VDOP")
	.dwattr DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x9c]
	.dwattr DW$548, DW_AT_accessibility(DW_ACCESS_public)
DW$549	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$549, DW_AT_name("fNT_Condition"), DW_AT_symbol_name("_fNT_Condition")
	.dwattr DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x9e]
	.dwattr DW$549, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$43

DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("USERop"), DW_AT_type(*DW$T$50)
	.dwattr DW$T$63, DW_AT_language(DW_LANG_C)
DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("tCorrectTimeOP"), DW_AT_type(*DW$T$54)
	.dwattr DW$T$64, DW_AT_language(DW_LANG_C)
DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("tTerminalDrv"), DW_AT_type(*DW$T$88)
	.dwattr DW$T$93, DW_AT_language(DW_LANG_C)
DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("tFIFO"), DW_AT_type(*DW$T$92)
	.dwattr DW$T$94, DW_AT_language(DW_LANG_C)

DW$T$140	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$139)
	.dwattr DW$T$140, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$140, DW_AT_byte_size(0x1020)
DW$550	.dwtag  DW_TAG_subrange_type
	.dwattr DW$550, DW_AT_upper_bound(0x0b)
	.dwendtag DW$T$140

DW$T$142	.dwtag  DW_TAG_typedef, DW_AT_name("tDecoderCommon"), DW_AT_type(*DW$T$135)
	.dwattr DW$T$142, DW_AT_language(DW_LANG_C)

DW$T$144	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$143)
	.dwattr DW$T$144, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$144, DW_AT_byte_size(0x800)
DW$551	.dwtag  DW_TAG_subrange_type
	.dwattr DW$551, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$144

DW$T$145	.dwtag  DW_TAG_typedef, DW_AT_name("ION_UTC"), DW_AT_type(*DW$T$137)
	.dwattr DW$T$145, DW_AT_language(DW_LANG_C)

DW$T$147	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$146)
	.dwattr DW$T$147, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$147, DW_AT_byte_size(0x1100)
DW$552	.dwtag  DW_TAG_subrange_type
	.dwattr DW$552, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$147

DW$T$151	.dwtag  DW_TAG_typedef, DW_AT_name("LIST12"), DW_AT_type(*DW$T$150)
	.dwattr DW$T$151, DW_AT_language(DW_LANG_C)
DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("int64"), DW_AT_type(*DW$T$14)
	.dwattr DW$T$40, DW_AT_language(DW_LANG_C)
DW$T$16	.dwtag  DW_TAG_base_type, DW_AT_name("float")
	.dwattr DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$16, DW_AT_byte_size(0x04)

DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$23, DW_AT_name("__tGeopotential")
	.dwattr DW$T$23, DW_AT_byte_size(0x3870)
DW$553	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$553, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$553, DW_AT_accessibility(DW_ACCESS_public)
DW$554	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$554, DW_AT_name("NormFact"), DW_AT_symbol_name("_NormFact")
	.dwattr DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$554, DW_AT_accessibility(DW_ACCESS_public)
DW$555	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$555, DW_AT_name("C"), DW_AT_symbol_name("_C")
	.dwattr DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0xb50]
	.dwattr DW$555, DW_AT_accessibility(DW_ACCESS_public)
DW$556	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$556, DW_AT_name("S"), DW_AT_symbol_name("_S")
	.dwattr DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x1698]
	.dwattr DW$556, DW_AT_accessibility(DW_ACCESS_public)
DW$557	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$557, DW_AT_name("P"), DW_AT_symbol_name("_P")
	.dwattr DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x21e0]
	.dwattr DW$557, DW_AT_accessibility(DW_ACCESS_public)
DW$558	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$22)
	.dwattr DW$558, DW_AT_name("dPfi"), DW_AT_symbol_name("_dPfi")
	.dwattr DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d28]
	.dwattr DW$558, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$23


DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$25, DW_AT_name("__tSunMoonData")
	.dwattr DW$T$25, DW_AT_byte_size(0x30)
DW$559	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$559, DW_AT_name("mjdTau"), DW_AT_symbol_name("_mjdTau")
	.dwattr DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$559, DW_AT_accessibility(DW_ACCESS_public)
DW$560	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$560, DW_AT_name("dATex"), DW_AT_symbol_name("_dATex")
	.dwattr DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$560, DW_AT_accessibility(DW_ACCESS_public)
DW$561	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$561, DW_AT_name("mjdMean_t"), DW_AT_symbol_name("_mjdMean_t")
	.dwattr DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$561, DW_AT_accessibility(DW_ACCESS_public)
DW$562	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$24)
	.dwattr DW$562, DW_AT_name("Mean_g"), DW_AT_symbol_name("_Mean_g")
	.dwattr DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$562, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$25


DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$26, DW_AT_name("__tAtmosphere")
	.dwattr DW$T$26, DW_AT_byte_size(0x18)
DW$563	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$563, DW_AT_name("mjdMean_t"), DW_AT_symbol_name("_mjdMean_t")
	.dwattr DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$563, DW_AT_accessibility(DW_ACCESS_public)
DW$564	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$564, DW_AT_name("mjdTau"), DW_AT_symbol_name("_mjdTau")
	.dwattr DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$564, DW_AT_accessibility(DW_ACCESS_public)
DW$565	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$565, DW_AT_name("Mean_AF"), DW_AT_symbol_name("_Mean_AF")
	.dwattr DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$565, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$26


DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$50, DW_AT_byte_size(0x98)
DW$566	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$566, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$566, DW_AT_accessibility(DW_ACCESS_public)
DW$567	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$567, DW_AT_name("t"), DW_AT_symbol_name("_t")
	.dwattr DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$567, DW_AT_accessibility(DW_ACCESS_public)
DW$568	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$568, DW_AT_name("X"), DW_AT_symbol_name("_X")
	.dwattr DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$568, DW_AT_accessibility(DW_ACCESS_public)
DW$569	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$569, DW_AT_name("Y"), DW_AT_symbol_name("_Y")
	.dwattr DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$569, DW_AT_accessibility(DW_ACCESS_public)
DW$570	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$570, DW_AT_name("Z"), DW_AT_symbol_name("_Z")
	.dwattr DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$570, DW_AT_accessibility(DW_ACCESS_public)
DW$571	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$571, DW_AT_name("Xdot"), DW_AT_symbol_name("_Xdot")
	.dwattr DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$571, DW_AT_accessibility(DW_ACCESS_public)
DW$572	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$572, DW_AT_name("Ydot"), DW_AT_symbol_name("_Ydot")
	.dwattr DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr DW$572, DW_AT_accessibility(DW_ACCESS_public)
DW$573	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$573, DW_AT_name("Zdot"), DW_AT_symbol_name("_Zdot")
	.dwattr DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$573, DW_AT_accessibility(DW_ACCESS_public)
DW$574	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$574, DW_AT_name("Lon"), DW_AT_symbol_name("_Lon")
	.dwattr DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$574, DW_AT_accessibility(DW_ACCESS_public)
DW$575	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$575, DW_AT_name("Lat"), DW_AT_symbol_name("_Lat")
	.dwattr DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$575, DW_AT_accessibility(DW_ACCESS_public)
DW$576	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$576, DW_AT_name("H"), DW_AT_symbol_name("_H")
	.dwattr DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr DW$576, DW_AT_accessibility(DW_ACCESS_public)
DW$577	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$577, DW_AT_name("Ndot"), DW_AT_symbol_name("_Ndot")
	.dwattr DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr DW$577, DW_AT_accessibility(DW_ACCESS_public)
DW$578	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$578, DW_AT_name("Edot"), DW_AT_symbol_name("_Edot")
	.dwattr DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr DW$578, DW_AT_accessibility(DW_ACCESS_public)
DW$579	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$579, DW_AT_name("Udot"), DW_AT_symbol_name("_Udot")
	.dwattr DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr DW$579, DW_AT_accessibility(DW_ACCESS_public)
DW$580	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$580, DW_AT_name("A"), DW_AT_symbol_name("_A")
	.dwattr DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr DW$580, DW_AT_accessibility(DW_ACCESS_public)
DW$581	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$581, DW_AT_name("Omega"), DW_AT_symbol_name("_Omega")
	.dwattr DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr DW$581, DW_AT_accessibility(DW_ACCESS_public)
DW$582	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$582, DW_AT_name("M"), DW_AT_symbol_name("_M")
	.dwattr DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr DW$582, DW_AT_accessibility(DW_ACCESS_public)
DW$583	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$583, DW_AT_name("I"), DW_AT_symbol_name("_I")
	.dwattr DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr DW$583, DW_AT_accessibility(DW_ACCESS_public)
DW$584	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$584, DW_AT_name("OmegaP"), DW_AT_symbol_name("_OmegaP")
	.dwattr DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr DW$584, DW_AT_accessibility(DW_ACCESS_public)
DW$585	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$585, DW_AT_name("Ecc"), DW_AT_symbol_name("_Ecc")
	.dwattr DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr DW$585, DW_AT_accessibility(DW_ACCESS_public)
DW$586	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$586, DW_AT_name("R"), DW_AT_symbol_name("_R")
	.dwattr DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr DW$586, DW_AT_accessibility(DW_ACCESS_public)
DW$587	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$587, DW_AT_name("OscillOffset"), DW_AT_symbol_name("_OscillOffset")
	.dwattr DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr DW$587, DW_AT_accessibility(DW_ACCESS_public)
DW$588	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$588, DW_AT_name("fOP_Condition"), DW_AT_symbol_name("_fOP_Condition")
	.dwattr DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x94]
	.dwattr DW$588, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$50


DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$54, DW_AT_byte_size(0x30)
DW$589	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$589, DW_AT_name("R_nts"), DW_AT_symbol_name("_R_nts")
	.dwattr DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$589, DW_AT_accessibility(DW_ACCESS_public)
DW$590	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$590, DW_AT_name("Rdot_nts"), DW_AT_symbol_name("_Rdot_nts")
	.dwattr DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$590, DW_AT_accessibility(DW_ACCESS_public)
DW$591	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$591, DW_AT_name("Rnc"), DW_AT_symbol_name("_Rnc")
	.dwattr DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$591, DW_AT_accessibility(DW_ACCESS_public)
DW$592	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$592, DW_AT_name("R"), DW_AT_symbol_name("_R")
	.dwattr DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$592, DW_AT_accessibility(DW_ACCESS_public)
DW$593	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$593, DW_AT_name("Rdot"), DW_AT_symbol_name("_Rdot")
	.dwattr DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$593, DW_AT_accessibility(DW_ACCESS_public)
DW$594	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$52)
	.dwattr DW$594, DW_AT_name("FlagCorrectTime"), DW_AT_symbol_name("_FlagCorrectTime")
	.dwattr DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$594, DW_AT_accessibility(DW_ACCESS_public)
DW$595	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$595, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr DW$595, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$54


DW$T$88	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$88, DW_AT_name("tagTerminalDrv")
	.dwattr DW$T$88, DW_AT_byte_size(0x10)
DW$596	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$75)
	.dwattr DW$596, DW_AT_name("pfnRead"), DW_AT_symbol_name("_pfnRead")
	.dwattr DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$596, DW_AT_accessibility(DW_ACCESS_public)
DW$597	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$79)
	.dwattr DW$597, DW_AT_name("pfnWrite"), DW_AT_symbol_name("_pfnWrite")
	.dwattr DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$597, DW_AT_accessibility(DW_ACCESS_public)
DW$598	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$83)
	.dwattr DW$598, DW_AT_name("pfnReadBinary"), DW_AT_symbol_name("_pfnReadBinary")
	.dwattr DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$598, DW_AT_accessibility(DW_ACCESS_public)
DW$599	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$87)
	.dwattr DW$599, DW_AT_name("pfnWriteBinary"), DW_AT_symbol_name("_pfnWriteBinary")
	.dwattr DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$599, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$88


DW$T$92	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$92, DW_AT_name("tFIFO")
	.dwattr DW$T$92, DW_AT_byte_size(0x10)
DW$600	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$600, DW_AT_name("iHead"), DW_AT_symbol_name("_iHead")
	.dwattr DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$600, DW_AT_accessibility(DW_ACCESS_public)
DW$601	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$601, DW_AT_name("iTail"), DW_AT_symbol_name("_iTail")
	.dwattr DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$601, DW_AT_accessibility(DW_ACCESS_public)
DW$602	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$602, DW_AT_name("nSize"), DW_AT_symbol_name("_nSize")
	.dwattr DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$602, DW_AT_accessibility(DW_ACCESS_public)
DW$603	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$91)
	.dwattr DW$603, DW_AT_name("pData"), DW_AT_symbol_name("_pData")
	.dwattr DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$603, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$92

DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("tNavTaskParams"), DW_AT_type(*DW$T$112)
	.dwattr DW$T$139, DW_AT_language(DW_LANG_C)

DW$T$135	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$135, DW_AT_byte_size(0x1528)
DW$604	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$127)
	.dwattr DW$604, DW_AT_name("TempIonUTC"), DW_AT_symbol_name("_TempIonUTC")
	.dwattr DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$604, DW_AT_accessibility(DW_ACCESS_public)
DW$605	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$129)
	.dwattr DW$605, DW_AT_name("TempAlmGPS"), DW_AT_symbol_name("_TempAlmGPS")
	.dwattr DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr DW$605, DW_AT_accessibility(DW_ACCESS_public)
DW$606	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$130)
	.dwattr DW$606, DW_AT_name("RawAlmGPS"), DW_AT_symbol_name("_RawAlmGPS")
	.dwattr DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a4]
	.dwattr DW$606, DW_AT_accessibility(DW_ACCESS_public)
DW$607	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$132)
	.dwattr DW$607, DW_AT_name("TempEph"), DW_AT_symbol_name("_TempEph")
	.dwattr DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0xa24]
	.dwattr DW$607, DW_AT_accessibility(DW_ACCESS_public)
DW$608	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$608, DW_AT_name("LastSubFrame"), DW_AT_symbol_name("_LastSubFrame")
	.dwattr DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x13a4]
	.dwattr DW$608, DW_AT_accessibility(DW_ACCESS_public)
DW$609	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$134)
	.dwattr DW$609, DW_AT_name("preamble"), DW_AT_symbol_name("_preamble")
	.dwattr DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x13a8]
	.dwattr DW$609, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$135

DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("ALM_GPS"), DW_AT_type(*DW$T$136)
	.dwattr DW$T$143, DW_AT_language(DW_LANG_C)

DW$T$137	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$137, DW_AT_byte_size(0x40)
DW$610	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$610, DW_AT_name("alpha0"), DW_AT_symbol_name("_alpha0")
	.dwattr DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$610, DW_AT_accessibility(DW_ACCESS_public)
DW$611	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$611, DW_AT_name("alpha1"), DW_AT_symbol_name("_alpha1")
	.dwattr DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$611, DW_AT_accessibility(DW_ACCESS_public)
DW$612	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$612, DW_AT_name("alpha2"), DW_AT_symbol_name("_alpha2")
	.dwattr DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$612, DW_AT_accessibility(DW_ACCESS_public)
DW$613	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$613, DW_AT_name("alpha3"), DW_AT_symbol_name("_alpha3")
	.dwattr DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$613, DW_AT_accessibility(DW_ACCESS_public)
DW$614	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$614, DW_AT_name("beta0"), DW_AT_symbol_name("_beta0")
	.dwattr DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$614, DW_AT_accessibility(DW_ACCESS_public)
DW$615	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$615, DW_AT_name("beta1"), DW_AT_symbol_name("_beta1")
	.dwattr DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr DW$615, DW_AT_accessibility(DW_ACCESS_public)
DW$616	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$616, DW_AT_name("beta2"), DW_AT_symbol_name("_beta2")
	.dwattr DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$616, DW_AT_accessibility(DW_ACCESS_public)
DW$617	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$617, DW_AT_name("beta3"), DW_AT_symbol_name("_beta3")
	.dwattr DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr DW$617, DW_AT_accessibility(DW_ACCESS_public)
DW$618	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$618, DW_AT_name("A0"), DW_AT_symbol_name("_A0")
	.dwattr DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$618, DW_AT_accessibility(DW_ACCESS_public)
DW$619	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$619, DW_AT_name("A1"), DW_AT_symbol_name("_A1")
	.dwattr DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$619, DW_AT_accessibility(DW_ACCESS_public)
DW$620	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$620, DW_AT_name("tot"), DW_AT_symbol_name("_tot")
	.dwattr DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$620, DW_AT_accessibility(DW_ACCESS_public)
DW$621	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$621, DW_AT_name("WNt"), DW_AT_symbol_name("_WNt")
	.dwattr DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr DW$621, DW_AT_accessibility(DW_ACCESS_public)
DW$622	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$622, DW_AT_name("WNlsf"), DW_AT_symbol_name("_WNlsf")
	.dwattr DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr DW$622, DW_AT_accessibility(DW_ACCESS_public)
DW$623	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$623, DW_AT_name("dtls"), DW_AT_symbol_name("_dtls")
	.dwattr DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$623, DW_AT_accessibility(DW_ACCESS_public)
DW$624	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$624, DW_AT_name("dtlsf"), DW_AT_symbol_name("_dtlsf")
	.dwattr DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr DW$624, DW_AT_accessibility(DW_ACCESS_public)
DW$625	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$625, DW_AT_name("DN"), DW_AT_symbol_name("_DN")
	.dwattr DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr DW$625, DW_AT_accessibility(DW_ACCESS_public)
DW$626	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$626, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr DW$626, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$137

DW$T$146	.dwtag  DW_TAG_typedef, DW_AT_name("EPH_GPS"), DW_AT_type(*DW$T$138)
	.dwattr DW$T$146, DW_AT_language(DW_LANG_C)

DW$T$150	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$150, DW_AT_byte_size(0x0d)
DW$627	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$627, DW_AT_name("N"), DW_AT_symbol_name("_N")
	.dwattr DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$627, DW_AT_accessibility(DW_ACCESS_public)
DW$628	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$149)
	.dwattr DW$628, DW_AT_name("Cells"), DW_AT_symbol_name("_Cells")
	.dwattr DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr DW$628, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$150

DW$T$83	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$82)
	.dwattr DW$T$83, DW_AT_address_class(0x20)
DW$T$87	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$86)
	.dwattr DW$T$87, DW_AT_address_class(0x20)
DW$T$14	.dwtag  DW_TAG_base_type, DW_AT_name("long long")
	.dwattr DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$14, DW_AT_byte_size(0x08)

DW$T$22	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$20)
	.dwattr DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$22, DW_AT_byte_size(0xb48)
DW$629	.dwtag  DW_TAG_subrange_type
	.dwattr DW$629, DW_AT_upper_bound(0x12)
DW$630	.dwtag  DW_TAG_subrange_type
	.dwattr DW$630, DW_AT_upper_bound(0x12)
	.dwendtag DW$T$22


DW$T$149	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$42)
	.dwattr DW$T$149, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$149, DW_AT_byte_size(0x0c)
DW$631	.dwtag  DW_TAG_subrange_type
	.dwattr DW$631, DW_AT_upper_bound(0x0b)
	.dwendtag DW$T$149

DW$T$91	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$90)
	.dwattr DW$T$91, DW_AT_address_class(0x20)
DW$T$75	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$74)
	.dwattr DW$T$75, DW_AT_address_class(0x20)
DW$T$79	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$78)
	.dwattr DW$T$79, DW_AT_address_class(0x20)

DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$112, DW_AT_byte_size(0x158)
DW$632	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$109)
	.dwattr DW$632, DW_AT_name("sat_params"), DW_AT_symbol_name("_sat_params")
	.dwattr DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$632, DW_AT_accessibility(DW_ACCESS_public)
DW$633	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$110)
	.dwattr DW$633, DW_AT_name("sat_coord"), DW_AT_symbol_name("_sat_coord")
	.dwattr DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$633, DW_AT_accessibility(DW_ACCESS_public)
DW$634	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$634, DW_AT_name("range"), DW_AT_symbol_name("_range")
	.dwattr DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr DW$634, DW_AT_accessibility(DW_ACCESS_public)
DW$635	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$635, DW_AT_name("Vrad"), DW_AT_symbol_name("_Vrad")
	.dwattr DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr DW$635, DW_AT_accessibility(DW_ACCESS_public)
DW$636	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$24)
	.dwattr DW$636, DW_AT_name("Hmat"), DW_AT_symbol_name("_Hmat")
	.dwattr DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr DW$636, DW_AT_accessibility(DW_ACCESS_public)
DW$637	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$106)
	.dwattr DW$637, DW_AT_name("raw_data"), DW_AT_symbol_name("_raw_data")
	.dwattr DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr DW$637, DW_AT_accessibility(DW_ACCESS_public)
DW$638	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$107)
	.dwattr DW$638, DW_AT_name("raw_data_fix"), DW_AT_symbol_name("_raw_data_fix")
	.dwattr DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr DW$638, DW_AT_accessibility(DW_ACCESS_public)
DW$639	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$111)
	.dwattr DW$639, DW_AT_name("paramsOP"), DW_AT_symbol_name("_paramsOP")
	.dwattr DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0xd8]
	.dwattr DW$639, DW_AT_accessibility(DW_ACCESS_public)
DW$640	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$640, DW_AT_name("BAD_SAT"), DW_AT_symbol_name("_BAD_SAT")
	.dwattr DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x150]
	.dwattr DW$640, DW_AT_accessibility(DW_ACCESS_public)
DW$641	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$641, DW_AT_name("ELEV_MSK"), DW_AT_symbol_name("_ELEV_MSK")
	.dwattr DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x151]
	.dwattr DW$641, DW_AT_accessibility(DW_ACCESS_public)
DW$642	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$642, DW_AT_name("USED"), DW_AT_symbol_name("_USED")
	.dwattr DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x152]
	.dwattr DW$642, DW_AT_accessibility(DW_ACCESS_public)
DW$643	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$643, DW_AT_name("SNR_log"), DW_AT_symbol_name("_SNR_log")
	.dwattr DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x153]
	.dwattr DW$643, DW_AT_accessibility(DW_ACCESS_public)
DW$644	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$644, DW_AT_name("sv_num"), DW_AT_symbol_name("_sv_num")
	.dwattr DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x154]
	.dwattr DW$644, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$112

DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("TEMP_ION_UTC"), DW_AT_type(*DW$T$119)
	.dwattr DW$T$127, DW_AT_language(DW_LANG_C)

DW$T$130	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$121)
	.dwattr DW$T$130, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$130, DW_AT_byte_size(0x480)
DW$645	.dwtag  DW_TAG_subrange_type
	.dwattr DW$645, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$130


DW$T$129	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$128)
	.dwattr DW$T$129, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$129, DW_AT_byte_size(0x580)
DW$646	.dwtag  DW_TAG_subrange_type
	.dwattr DW$646, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$129


DW$T$132	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$131)
	.dwattr DW$T$132, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$132, DW_AT_byte_size(0x980)
DW$647	.dwtag  DW_TAG_subrange_type
	.dwattr DW$647, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$132


DW$T$134	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$133)
	.dwattr DW$T$134, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$134, DW_AT_byte_size(0x180)
DW$648	.dwtag  DW_TAG_subrange_type
	.dwattr DW$648, DW_AT_upper_bound(0x1f)
	.dwendtag DW$T$134


DW$T$136	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$136, DW_AT_byte_size(0x40)
DW$649	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$649, DW_AT_name("prn"), DW_AT_symbol_name("_prn")
	.dwattr DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$649, DW_AT_accessibility(DW_ACCESS_public)
DW$650	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$650, DW_AT_name("health"), DW_AT_symbol_name("_health")
	.dwattr DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr DW$650, DW_AT_accessibility(DW_ACCESS_public)
DW$651	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$651, DW_AT_name("wna"), DW_AT_symbol_name("_wna")
	.dwattr DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr DW$651, DW_AT_accessibility(DW_ACCESS_public)
DW$652	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$652, DW_AT_name("toa"), DW_AT_symbol_name("_toa")
	.dwattr DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$652, DW_AT_accessibility(DW_ACCESS_public)
DW$653	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$653, DW_AT_name("roota"), DW_AT_symbol_name("_roota")
	.dwattr DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$653, DW_AT_accessibility(DW_ACCESS_public)
DW$654	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$654, DW_AT_name("omega0"), DW_AT_symbol_name("_omega0")
	.dwattr DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$654, DW_AT_accessibility(DW_ACCESS_public)
DW$655	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$655, DW_AT_name("omega"), DW_AT_symbol_name("_omega")
	.dwattr DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$655, DW_AT_accessibility(DW_ACCESS_public)
DW$656	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$656, DW_AT_name("m0"), DW_AT_symbol_name("_m0")
	.dwattr DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$656, DW_AT_accessibility(DW_ACCESS_public)
DW$657	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$657, DW_AT_name("e"), DW_AT_symbol_name("_e")
	.dwattr DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$657, DW_AT_accessibility(DW_ACCESS_public)
DW$658	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$658, DW_AT_name("i0"), DW_AT_symbol_name("_i0")
	.dwattr DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr DW$658, DW_AT_accessibility(DW_ACCESS_public)
DW$659	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$659, DW_AT_name("omegadot"), DW_AT_symbol_name("_omegadot")
	.dwattr DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$659, DW_AT_accessibility(DW_ACCESS_public)
DW$660	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$660, DW_AT_name("af0"), DW_AT_symbol_name("_af0")
	.dwattr DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr DW$660, DW_AT_accessibility(DW_ACCESS_public)
DW$661	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$661, DW_AT_name("af1"), DW_AT_symbol_name("_af1")
	.dwattr DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$661, DW_AT_accessibility(DW_ACCESS_public)
DW$662	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$662, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr DW$662, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$136


DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$138, DW_AT_byte_size(0x88)
DW$663	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$663, DW_AT_name("prn"), DW_AT_symbol_name("_prn")
	.dwattr DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$663, DW_AT_accessibility(DW_ACCESS_public)
DW$664	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$664, DW_AT_name("health"), DW_AT_symbol_name("_health")
	.dwattr DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr DW$664, DW_AT_accessibility(DW_ACCESS_public)
DW$665	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$665, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr DW$665, DW_AT_accessibility(DW_ACCESS_public)
DW$666	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$666, DW_AT_name("tow"), DW_AT_symbol_name("_tow")
	.dwattr DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$666, DW_AT_accessibility(DW_ACCESS_public)
DW$667	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$667, DW_AT_name("tgd"), DW_AT_symbol_name("_tgd")
	.dwattr DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$667, DW_AT_accessibility(DW_ACCESS_public)
DW$668	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$668, DW_AT_name("iodc"), DW_AT_symbol_name("_iodc")
	.dwattr DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$668, DW_AT_accessibility(DW_ACCESS_public)
DW$669	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$669, DW_AT_name("toc"), DW_AT_symbol_name("_toc")
	.dwattr DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$669, DW_AT_accessibility(DW_ACCESS_public)
DW$670	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$670, DW_AT_name("af0"), DW_AT_symbol_name("_af0")
	.dwattr DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr DW$670, DW_AT_accessibility(DW_ACCESS_public)
DW$671	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$671, DW_AT_name("af1"), DW_AT_symbol_name("_af1")
	.dwattr DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$671, DW_AT_accessibility(DW_ACCESS_public)
DW$672	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$672, DW_AT_name("af2"), DW_AT_symbol_name("_af2")
	.dwattr DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr DW$672, DW_AT_accessibility(DW_ACCESS_public)
DW$673	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$673, DW_AT_name("iode"), DW_AT_symbol_name("_iode")
	.dwattr DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$673, DW_AT_accessibility(DW_ACCESS_public)
DW$674	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$674, DW_AT_name("deltan"), DW_AT_symbol_name("_deltan")
	.dwattr DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr DW$674, DW_AT_accessibility(DW_ACCESS_public)
DW$675	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$675, DW_AT_name("m0"), DW_AT_symbol_name("_m0")
	.dwattr DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$675, DW_AT_accessibility(DW_ACCESS_public)
DW$676	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$676, DW_AT_name("e"), DW_AT_symbol_name("_e")
	.dwattr DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$676, DW_AT_accessibility(DW_ACCESS_public)
DW$677	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$677, DW_AT_name("roota"), DW_AT_symbol_name("_roota")
	.dwattr DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$677, DW_AT_accessibility(DW_ACCESS_public)
DW$678	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$678, DW_AT_name("toe"), DW_AT_symbol_name("_toe")
	.dwattr DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$678, DW_AT_accessibility(DW_ACCESS_public)
DW$679	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$679, DW_AT_name("cic"), DW_AT_symbol_name("_cic")
	.dwattr DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr DW$679, DW_AT_accessibility(DW_ACCESS_public)
DW$680	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$680, DW_AT_name("cis"), DW_AT_symbol_name("_cis")
	.dwattr DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr DW$680, DW_AT_accessibility(DW_ACCESS_public)
DW$681	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$681, DW_AT_name("cuc"), DW_AT_symbol_name("_cuc")
	.dwattr DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr DW$681, DW_AT_accessibility(DW_ACCESS_public)
DW$682	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$682, DW_AT_name("cus"), DW_AT_symbol_name("_cus")
	.dwattr DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr DW$682, DW_AT_accessibility(DW_ACCESS_public)
DW$683	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$683, DW_AT_name("crc"), DW_AT_symbol_name("_crc")
	.dwattr DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr DW$683, DW_AT_accessibility(DW_ACCESS_public)
DW$684	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$684, DW_AT_name("crs"), DW_AT_symbol_name("_crs")
	.dwattr DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr DW$684, DW_AT_accessibility(DW_ACCESS_public)
DW$685	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$685, DW_AT_name("omega0"), DW_AT_symbol_name("_omega0")
	.dwattr DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr DW$685, DW_AT_accessibility(DW_ACCESS_public)
DW$686	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$686, DW_AT_name("omega"), DW_AT_symbol_name("_omega")
	.dwattr DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr DW$686, DW_AT_accessibility(DW_ACCESS_public)
DW$687	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$687, DW_AT_name("i0"), DW_AT_symbol_name("_i0")
	.dwattr DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr DW$687, DW_AT_accessibility(DW_ACCESS_public)
DW$688	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$688, DW_AT_name("omegadot"), DW_AT_symbol_name("_omegadot")
	.dwattr DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr DW$688, DW_AT_accessibility(DW_ACCESS_public)
DW$689	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$689, DW_AT_name("idot"), DW_AT_symbol_name("_idot")
	.dwattr DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr DW$689, DW_AT_accessibility(DW_ACCESS_public)
DW$690	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$690, DW_AT_name("accuracy"), DW_AT_symbol_name("_accuracy")
	.dwattr DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr DW$690, DW_AT_accessibility(DW_ACCESS_public)
DW$691	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$691, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x84]
	.dwattr DW$691, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$138


DW$T$82	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$80)
	.dwattr DW$T$82, DW_AT_language(DW_LANG_C)
DW$692	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$72)
DW$693	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$81)
DW$694	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$80)
	.dwendtag DW$T$82


DW$T$86	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$80)
	.dwattr DW$T$86, DW_AT_language(DW_LANG_C)
DW$695	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$72)
DW$696	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$85)
DW$697	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$80)
	.dwendtag DW$T$86

DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("tFifoItem"), DW_AT_type(*DW$T$41)
	.dwattr DW$T$90, DW_AT_language(DW_LANG_C)

DW$T$74	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$71)
	.dwattr DW$T$74, DW_AT_language(DW_LANG_C)
DW$698	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$72)
DW$699	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$73)
DW$700	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
	.dwendtag DW$T$74


DW$T$78	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$71)
	.dwattr DW$T$78, DW_AT_language(DW_LANG_C)
DW$701	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$72)
DW$702	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$77)
DW$703	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
	.dwendtag DW$T$78

DW$T$72	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$88)
	.dwattr DW$T$72, DW_AT_address_class(0x20)
DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("tSatParams"), DW_AT_type(*DW$T$99)
	.dwattr DW$T$109, DW_AT_language(DW_LANG_C)
DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("tSatCoord"), DW_AT_type(*DW$T$101)
	.dwattr DW$T$110, DW_AT_language(DW_LANG_C)
DW$T$106	.dwtag  DW_TAG_typedef, DW_AT_name("tRawData"), DW_AT_type(*DW$T$102)
	.dwattr DW$T$106, DW_AT_language(DW_LANG_C)
DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("tRawDataFix"), DW_AT_type(*DW$T$105)
	.dwattr DW$T$107, DW_AT_language(DW_LANG_C)
DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("tParamsOP"), DW_AT_type(*DW$T$108)
	.dwattr DW$T$111, DW_AT_language(DW_LANG_C)

DW$T$119	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$119, DW_AT_byte_size(0x24)
DW$704	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$118)
	.dwattr DW$704, DW_AT_name("TIonUTC"), DW_AT_symbol_name("_TIonUTC")
	.dwattr DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$704, DW_AT_accessibility(DW_ACCESS_public)
DW$705	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$705, DW_AT_name("flag"), DW_AT_symbol_name("_flag")
	.dwattr DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr DW$705, DW_AT_accessibility(DW_ACCESS_public)
DW$706	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$706, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$706, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$119

DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("RAW_ALM_GPS"), DW_AT_type(*DW$T$120)
	.dwattr DW$T$121, DW_AT_language(DW_LANG_C)
DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("TEMP_ALM_GPS"), DW_AT_type(*DW$T$122)
	.dwattr DW$T$128, DW_AT_language(DW_LANG_C)
DW$T$131	.dwtag  DW_TAG_typedef, DW_AT_name("TEMP_EPH_GPS"), DW_AT_type(*DW$T$125)
	.dwattr DW$T$131, DW_AT_language(DW_LANG_C)
DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("tPreamble"), DW_AT_type(*DW$T$126)
	.dwattr DW$T$133, DW_AT_language(DW_LANG_C)
DW$T$81	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$53)
	.dwattr DW$T$81, DW_AT_address_class(0x20)
DW$T$85	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$84)
	.dwattr DW$T$85, DW_AT_address_class(0x20)
DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("uint16"), DW_AT_type(*DW$T$9)
	.dwattr DW$T$80, DW_AT_language(DW_LANG_C)
DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("tIOstat"), DW_AT_type(*DW$T$70)
	.dwattr DW$T$71, DW_AT_language(DW_LANG_C)

DW$T$99	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$99, DW_AT_byte_size(0x0c)
DW$707	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$707, DW_AT_name("elev"), DW_AT_symbol_name("_elev")
	.dwattr DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$707, DW_AT_accessibility(DW_ACCESS_public)
DW$708	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$708, DW_AT_name("azim"), DW_AT_symbol_name("_azim")
	.dwattr DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$708, DW_AT_accessibility(DW_ACCESS_public)
DW$709	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$709, DW_AT_name("elev_i16"), DW_AT_symbol_name("_elev_i16")
	.dwattr DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$709, DW_AT_accessibility(DW_ACCESS_public)
DW$710	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$710, DW_AT_name("azim_i16"), DW_AT_symbol_name("_azim_i16")
	.dwattr DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr DW$710, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$99


DW$T$101	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$101, DW_AT_byte_size(0x60)
DW$711	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$100)
	.dwattr DW$711, DW_AT_name("coord"), DW_AT_symbol_name("_coord")
	.dwattr DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$711, DW_AT_accessibility(DW_ACCESS_public)
DW$712	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$712, DW_AT_name("dt_ION"), DW_AT_symbol_name("_dt_ION")
	.dwattr DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$712, DW_AT_accessibility(DW_ACCESS_public)
DW$713	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$98)
	.dwattr DW$713, DW_AT_name("dt_TRO"), DW_AT_symbol_name("_dt_TRO")
	.dwattr DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr DW$713, DW_AT_accessibility(DW_ACCESS_public)
DW$714	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$714, DW_AT_name("dt_IONQ17"), DW_AT_symbol_name("_dt_IONQ17")
	.dwattr DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$714, DW_AT_accessibility(DW_ACCESS_public)
DW$715	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$715, DW_AT_name("dt_TROQ17"), DW_AT_symbol_name("_dt_TROQ17")
	.dwattr DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr DW$715, DW_AT_accessibility(DW_ACCESS_public)
DW$716	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$716, DW_AT_name("x_los"), DW_AT_symbol_name("_x_los")
	.dwattr DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr DW$716, DW_AT_accessibility(DW_ACCESS_public)
DW$717	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$717, DW_AT_name("y_los"), DW_AT_symbol_name("_y_los")
	.dwattr DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr DW$717, DW_AT_accessibility(DW_ACCESS_public)
DW$718	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$40)
	.dwattr DW$718, DW_AT_name("z_los"), DW_AT_symbol_name("_z_los")
	.dwattr DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr DW$718, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$101


DW$T$102	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$102, DW_AT_byte_size(0x20)
DW$719	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$719, DW_AT_name("ps_delay"), DW_AT_symbol_name("_ps_delay")
	.dwattr DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$719, DW_AT_accessibility(DW_ACCESS_public)
DW$720	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$720, DW_AT_name("ps_freq"), DW_AT_symbol_name("_ps_freq")
	.dwattr DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$720, DW_AT_accessibility(DW_ACCESS_public)
DW$721	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$17)
	.dwattr DW$721, DW_AT_name("timeCoord"), DW_AT_symbol_name("_timeCoord")
	.dwattr DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$721, DW_AT_accessibility(DW_ACCESS_public)
DW$722	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$722, DW_AT_name("attempt"), DW_AT_symbol_name("_attempt")
	.dwattr DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$722, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$102


DW$T$105	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$105, DW_AT_byte_size(0x1c)
DW$723	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$104)
	.dwattr DW$723, DW_AT_name("ps_delay"), DW_AT_symbol_name("_ps_delay")
	.dwattr DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$723, DW_AT_accessibility(DW_ACCESS_public)
DW$724	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$104)
	.dwattr DW$724, DW_AT_name("ps_freq"), DW_AT_symbol_name("_ps_freq")
	.dwattr DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$724, DW_AT_accessibility(DW_ACCESS_public)
DW$725	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$104)
	.dwattr DW$725, DW_AT_name("timeCoord"), DW_AT_symbol_name("_timeCoord")
	.dwattr DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$725, DW_AT_accessibility(DW_ACCESS_public)
DW$726	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$42)
	.dwattr DW$726, DW_AT_name("attempt"), DW_AT_symbol_name("_attempt")
	.dwattr DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$726, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$105


DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$108, DW_AT_byte_size(0x78)
DW$727	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$100)
	.dwattr DW$727, DW_AT_name("coord"), DW_AT_symbol_name("_coord")
	.dwattr DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$727, DW_AT_accessibility(DW_ACCESS_public)
DW$728	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$106)
	.dwattr DW$728, DW_AT_name("raw_data"), DW_AT_symbol_name("_raw_data")
	.dwattr DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$728, DW_AT_accessibility(DW_ACCESS_public)
DW$729	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$107)
	.dwattr DW$729, DW_AT_name("raw_data_fix"), DW_AT_symbol_name("_raw_data_fix")
	.dwattr DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr DW$729, DW_AT_accessibility(DW_ACCESS_public)
DW$730	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$730, DW_AT_name("BAD_SAT_OP"), DW_AT_symbol_name("_BAD_SAT_OP")
	.dwattr DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr DW$730, DW_AT_accessibility(DW_ACCESS_public)
DW$731	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$731, DW_AT_name("ELEV_MSK_OP"), DW_AT_symbol_name("_ELEV_MSK_OP")
	.dwattr DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x75]
	.dwattr DW$731, DW_AT_accessibility(DW_ACCESS_public)
DW$732	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$732, DW_AT_name("USED_OP"), DW_AT_symbol_name("_USED_OP")
	.dwattr DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x76]
	.dwattr DW$732, DW_AT_accessibility(DW_ACCESS_public)
DW$733	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$53)
	.dwattr DW$733, DW_AT_name("COORDINATES"), DW_AT_symbol_name("_COORDINATES")
	.dwattr DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x77]
	.dwattr DW$733, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$108

DW$T$118	.dwtag  DW_TAG_typedef, DW_AT_name("RAW_ION_UTC"), DW_AT_type(*DW$T$117)
	.dwattr DW$T$118, DW_AT_language(DW_LANG_C)

DW$T$120	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$120, DW_AT_byte_size(0x24)
DW$734	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$734, DW_AT_name("prn"), DW_AT_symbol_name("_prn")
	.dwattr DW$734, DW_AT_bit_offset(0x18), DW_AT_bit_size(0x08)
	.dwattr DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$734, DW_AT_accessibility(DW_ACCESS_public)
DW$735	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$735, DW_AT_name("wna"), DW_AT_symbol_name("_wna")
	.dwattr DW$735, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x10)
	.dwattr DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$735, DW_AT_accessibility(DW_ACCESS_public)
DW$736	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$11)
	.dwattr DW$736, DW_AT_name("toa"), DW_AT_symbol_name("_toa")
	.dwattr DW$736, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$736, DW_AT_accessibility(DW_ACCESS_public)
DW$737	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$737, DW_AT_name("e"), DW_AT_symbol_name("_e")
	.dwattr DW$737, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$737, DW_AT_accessibility(DW_ACCESS_public)
DW$738	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$738, DW_AT_name("i0"), DW_AT_symbol_name("_i0")
	.dwattr DW$738, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$738, DW_AT_accessibility(DW_ACCESS_public)
DW$739	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$739, DW_AT_name("omegadot"), DW_AT_symbol_name("_omegadot")
	.dwattr DW$739, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$739, DW_AT_accessibility(DW_ACCESS_public)
DW$740	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$740, DW_AT_name("roota"), DW_AT_symbol_name("_roota")
	.dwattr DW$740, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$740, DW_AT_accessibility(DW_ACCESS_public)
DW$741	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$741, DW_AT_name("omega0"), DW_AT_symbol_name("_omega0")
	.dwattr DW$741, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$741, DW_AT_accessibility(DW_ACCESS_public)
DW$742	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$742, DW_AT_name("omega"), DW_AT_symbol_name("_omega")
	.dwattr DW$742, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr DW$742, DW_AT_accessibility(DW_ACCESS_public)
DW$743	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$743, DW_AT_name("m0"), DW_AT_symbol_name("_m0")
	.dwattr DW$743, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$743, DW_AT_accessibility(DW_ACCESS_public)
DW$744	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$744, DW_AT_name("af0"), DW_AT_symbol_name("_af0")
	.dwattr DW$744, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr DW$744, DW_AT_accessibility(DW_ACCESS_public)
DW$745	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$745, DW_AT_name("af1"), DW_AT_symbol_name("_af1")
	.dwattr DW$745, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr DW$745, DW_AT_accessibility(DW_ACCESS_public)
DW$746	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$746, DW_AT_name("health"), DW_AT_symbol_name("_health")
	.dwattr DW$746, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$746, DW_AT_accessibility(DW_ACCESS_public)
DW$747	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$747, DW_AT_name("valid"), DW_AT_symbol_name("_valid")
	.dwattr DW$747, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$747, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$120


DW$T$122	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$122, DW_AT_byte_size(0x2c)
DW$748	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$748, DW_AT_name("flag"), DW_AT_symbol_name("_flag")
	.dwattr DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$748, DW_AT_accessibility(DW_ACCESS_public)
DW$749	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$749, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$749, DW_AT_accessibility(DW_ACCESS_public)
DW$750	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$121)
	.dwattr DW$750, DW_AT_name("TAlmGPS"), DW_AT_symbol_name("_TAlmGPS")
	.dwattr DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$750, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$122


DW$T$125	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$125, DW_AT_byte_size(0x4c)
DW$751	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$124)
	.dwattr DW$751, DW_AT_name("TEphGPS"), DW_AT_symbol_name("_TEphGPS")
	.dwattr DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$751, DW_AT_accessibility(DW_ACCESS_public)
DW$752	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$752, DW_AT_name("flag"), DW_AT_symbol_name("_flag")
	.dwattr DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr DW$752, DW_AT_accessibility(DW_ACCESS_public)
DW$753	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$753, DW_AT_name("flag_bl"), DW_AT_symbol_name("_flag_bl")
	.dwattr DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr DW$753, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$125


DW$T$126	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$126, DW_AT_byte_size(0x0c)
DW$754	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$754, DW_AT_name("PreambleTime"), DW_AT_symbol_name("_PreambleTime")
	.dwattr DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$754, DW_AT_accessibility(DW_ACCESS_public)
DW$755	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$755, DW_AT_name("time"), DW_AT_symbol_name("_time")
	.dwattr DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$755, DW_AT_accessibility(DW_ACCESS_public)
DW$756	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$31)
	.dwattr DW$756, DW_AT_name("week"), DW_AT_symbol_name("_week")
	.dwattr DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$756, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$126

DW$T$84	.dwtag  DW_TAG_const_type
	.dwattr DW$T$84, DW_AT_type(*DW$T$53)
DW$T$9	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned short")
	.dwattr DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$9, DW_AT_byte_size(0x02)

DW$T$70	.dwtag  DW_TAG_enumeration_type
	.dwattr DW$T$70, DW_AT_byte_size(0x04)
DW$757	.dwtag  DW_TAG_enumerator, DW_AT_name("io_ERROR"), DW_AT_const_value(0x00)
DW$758	.dwtag  DW_TAG_enumerator, DW_AT_name("io_OK"), DW_AT_const_value(0x01)
	.dwendtag DW$T$70

DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("tLongFract"), DW_AT_type(*DW$T$103)
	.dwattr DW$T$104, DW_AT_language(DW_LANG_C)

DW$T$117	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$117, DW_AT_byte_size(0x1c)
DW$759	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$115)
	.dwattr DW$759, DW_AT_name("ION"), DW_AT_symbol_name("_ION")
	.dwattr DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$759, DW_AT_accessibility(DW_ACCESS_public)
DW$760	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$116)
	.dwattr DW$760, DW_AT_name("UTC"), DW_AT_symbol_name("_UTC")
	.dwattr DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$760, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$117

DW$T$124	.dwtag  DW_TAG_typedef, DW_AT_name("RAW_EPH_GPS"), DW_AT_type(*DW$T$123)
	.dwattr DW$T$124, DW_AT_language(DW_LANG_C)

DW$T$103	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$103, DW_AT_byte_size(0x08)
DW$761	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$761, DW_AT_name("integral"), DW_AT_symbol_name("_integral")
	.dwattr DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$761, DW_AT_accessibility(DW_ACCESS_public)
DW$762	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$89)
	.dwattr DW$762, DW_AT_name("fract"), DW_AT_symbol_name("_fract")
	.dwattr DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$762, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$103

DW$T$115	.dwtag  DW_TAG_typedef, DW_AT_name("RAW_ION"), DW_AT_type(*DW$T$113)
	.dwattr DW$T$115, DW_AT_language(DW_LANG_C)
DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("RAW_UTC"), DW_AT_type(*DW$T$114)
	.dwattr DW$T$116, DW_AT_language(DW_LANG_C)

DW$T$123	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$123, DW_AT_byte_size(0x44)
DW$763	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$763, DW_AT_name("prn"), DW_AT_symbol_name("_prn")
	.dwattr DW$763, DW_AT_bit_offset(0x18), DW_AT_bit_size(0x08)
	.dwattr DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$763, DW_AT_accessibility(DW_ACCESS_public)
DW$764	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$764, DW_AT_name("wn"), DW_AT_symbol_name("_wn")
	.dwattr DW$764, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x10)
	.dwattr DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$764, DW_AT_accessibility(DW_ACCESS_public)
DW$765	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$765, DW_AT_name("tgd"), DW_AT_symbol_name("_tgd")
	.dwattr DW$765, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$765, DW_AT_accessibility(DW_ACCESS_public)
DW$766	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$11)
	.dwattr DW$766, DW_AT_name("toc"), DW_AT_symbol_name("_toc")
	.dwattr DW$766, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$766, DW_AT_accessibility(DW_ACCESS_public)
DW$767	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$767, DW_AT_name("af1"), DW_AT_symbol_name("_af1")
	.dwattr DW$767, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$767, DW_AT_accessibility(DW_ACCESS_public)
DW$768	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$768, DW_AT_name("af0"), DW_AT_symbol_name("_af0")
	.dwattr DW$768, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$768, DW_AT_accessibility(DW_ACCESS_public)
DW$769	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$769, DW_AT_name("af2"), DW_AT_symbol_name("_af2")
	.dwattr DW$769, DW_AT_bit_offset(0x18), DW_AT_bit_size(0x08)
	.dwattr DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$769, DW_AT_accessibility(DW_ACCESS_public)
DW$770	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$11)
	.dwattr DW$770, DW_AT_name("iode"), DW_AT_symbol_name("_iode")
	.dwattr DW$770, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x08)
	.dwattr DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$770, DW_AT_accessibility(DW_ACCESS_public)
DW$771	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$771, DW_AT_name("deltan"), DW_AT_symbol_name("_deltan")
	.dwattr DW$771, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$771, DW_AT_accessibility(DW_ACCESS_public)
DW$772	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$772, DW_AT_name("m0"), DW_AT_symbol_name("_m0")
	.dwattr DW$772, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$772, DW_AT_accessibility(DW_ACCESS_public)
DW$773	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$11)
	.dwattr DW$773, DW_AT_name("e"), DW_AT_symbol_name("_e")
	.dwattr DW$773, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr DW$773, DW_AT_accessibility(DW_ACCESS_public)
DW$774	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$11)
	.dwattr DW$774, DW_AT_name("roota"), DW_AT_symbol_name("_roota")
	.dwattr DW$774, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$774, DW_AT_accessibility(DW_ACCESS_public)
DW$775	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$775, DW_AT_name("omega0"), DW_AT_symbol_name("_omega0")
	.dwattr DW$775, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr DW$775, DW_AT_accessibility(DW_ACCESS_public)
DW$776	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$776, DW_AT_name("i0"), DW_AT_symbol_name("_i0")
	.dwattr DW$776, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr DW$776, DW_AT_accessibility(DW_ACCESS_public)
DW$777	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$777, DW_AT_name("omega"), DW_AT_symbol_name("_omega")
	.dwattr DW$777, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr DW$777, DW_AT_accessibility(DW_ACCESS_public)
DW$778	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$778, DW_AT_name("omegadot"), DW_AT_symbol_name("_omegadot")
	.dwattr DW$778, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr DW$778, DW_AT_accessibility(DW_ACCESS_public)
DW$779	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$11)
	.dwattr DW$779, DW_AT_name("toe"), DW_AT_symbol_name("_toe")
	.dwattr DW$779, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr DW$779, DW_AT_accessibility(DW_ACCESS_public)
DW$780	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$780, DW_AT_name("idot"), DW_AT_symbol_name("_idot")
	.dwattr DW$780, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr DW$780, DW_AT_accessibility(DW_ACCESS_public)
DW$781	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$781, DW_AT_name("crc"), DW_AT_symbol_name("_crc")
	.dwattr DW$781, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$781, DW_AT_accessibility(DW_ACCESS_public)
DW$782	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$782, DW_AT_name("crs"), DW_AT_symbol_name("_crs")
	.dwattr DW$782, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr DW$782, DW_AT_accessibility(DW_ACCESS_public)
DW$783	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$783, DW_AT_name("cuc"), DW_AT_symbol_name("_cuc")
	.dwattr DW$783, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr DW$783, DW_AT_accessibility(DW_ACCESS_public)
DW$784	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$784, DW_AT_name("cus"), DW_AT_symbol_name("_cus")
	.dwattr DW$784, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr DW$784, DW_AT_accessibility(DW_ACCESS_public)
DW$785	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$785, DW_AT_name("cic"), DW_AT_symbol_name("_cic")
	.dwattr DW$785, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$785, DW_AT_accessibility(DW_ACCESS_public)
DW$786	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$786, DW_AT_name("cis"), DW_AT_symbol_name("_cis")
	.dwattr DW$786, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr DW$786, DW_AT_accessibility(DW_ACCESS_public)
DW$787	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$787, DW_AT_name("accuracy"), DW_AT_symbol_name("_accuracy")
	.dwattr DW$787, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr DW$787, DW_AT_accessibility(DW_ACCESS_public)
DW$788	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$788, DW_AT_name("health"), DW_AT_symbol_name("_health")
	.dwattr DW$788, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr DW$788, DW_AT_accessibility(DW_ACCESS_public)
DW$789	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$789, DW_AT_name("iodc"), DW_AT_symbol_name("_iodc")
	.dwattr DW$789, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$789, DW_AT_accessibility(DW_ACCESS_public)
DW$790	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$790, DW_AT_name("valid"), DW_AT_symbol_name("_valid")
	.dwattr DW$790, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr DW$790, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$123


DW$T$113	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$113, DW_AT_byte_size(0x08)
DW$791	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$791, DW_AT_name("alpha0"), DW_AT_symbol_name("_alpha0")
	.dwattr DW$791, DW_AT_bit_offset(0x18), DW_AT_bit_size(0x08)
	.dwattr DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$791, DW_AT_accessibility(DW_ACCESS_public)
DW$792	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$792, DW_AT_name("alpha1"), DW_AT_symbol_name("_alpha1")
	.dwattr DW$792, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x08)
	.dwattr DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$792, DW_AT_accessibility(DW_ACCESS_public)
DW$793	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$793, DW_AT_name("alpha2"), DW_AT_symbol_name("_alpha2")
	.dwattr DW$793, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$793, DW_AT_accessibility(DW_ACCESS_public)
DW$794	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$794, DW_AT_name("alpha3"), DW_AT_symbol_name("_alpha3")
	.dwattr DW$794, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$794, DW_AT_accessibility(DW_ACCESS_public)
DW$795	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$795, DW_AT_name("beta0"), DW_AT_symbol_name("_beta0")
	.dwattr DW$795, DW_AT_bit_offset(0x18), DW_AT_bit_size(0x08)
	.dwattr DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$795, DW_AT_accessibility(DW_ACCESS_public)
DW$796	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$796, DW_AT_name("beta1"), DW_AT_symbol_name("_beta1")
	.dwattr DW$796, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x08)
	.dwattr DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$796, DW_AT_accessibility(DW_ACCESS_public)
DW$797	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$797, DW_AT_name("beta2"), DW_AT_symbol_name("_beta2")
	.dwattr DW$797, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$797, DW_AT_accessibility(DW_ACCESS_public)
DW$798	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$798, DW_AT_name("beta3"), DW_AT_symbol_name("_beta3")
	.dwattr DW$798, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$798, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$113


DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$114, DW_AT_byte_size(0x14)
DW$799	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$799, DW_AT_name("A0"), DW_AT_symbol_name("_A0")
	.dwattr DW$799, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$799, DW_AT_accessibility(DW_ACCESS_public)
DW$800	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$800, DW_AT_name("A1"), DW_AT_symbol_name("_A1")
	.dwattr DW$800, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr DW$800, DW_AT_accessibility(DW_ACCESS_public)
DW$801	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$801, DW_AT_name("tot"), DW_AT_symbol_name("_tot")
	.dwattr DW$801, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$801, DW_AT_accessibility(DW_ACCESS_public)
DW$802	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$802, DW_AT_name("WNt"), DW_AT_symbol_name("_WNt")
	.dwattr DW$802, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$802, DW_AT_accessibility(DW_ACCESS_public)
DW$803	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$803, DW_AT_name("dtls"), DW_AT_symbol_name("_dtls")
	.dwattr DW$803, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr DW$803, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$803, DW_AT_accessibility(DW_ACCESS_public)
DW$804	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$804, DW_AT_name("WNlsf"), DW_AT_symbol_name("_WNlsf")
	.dwattr DW$804, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr DW$804, DW_AT_accessibility(DW_ACCESS_public)
DW$805	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$805, DW_AT_name("DN"), DW_AT_symbol_name("_DN")
	.dwattr DW$805, DW_AT_bit_offset(0x18), DW_AT_bit_size(0x08)
	.dwattr DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$805, DW_AT_accessibility(DW_ACCESS_public)
DW$806	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$806, DW_AT_name("dtlsf"), DW_AT_symbol_name("_dtlsf")
	.dwattr DW$806, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x08)
	.dwattr DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$806, DW_AT_accessibility(DW_ACCESS_public)
DW$807	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$10)
	.dwattr DW$807, DW_AT_name("WN"), DW_AT_symbol_name("_WN")
	.dwattr DW$807, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$807, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$114


	.dwattr DW$66, DW_AT_external(0x01)
	.dwattr DW$66, DW_AT_type(*DW$T$42)
	.dwattr DW$82, DW_AT_external(0x01)
	.dwattr DW$88, DW_AT_external(0x01)
	.dwattr DW$104, DW_AT_type(*DW$T$19)
	.dwattr DW$170, DW_AT_type(*DW$T$19)
	.dwattr DW$76, DW_AT_external(0x01)
	.dwattr DW$76, DW_AT_type(*DW$T$42)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$808	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$808, DW_AT_location[DW_OP_reg0]
DW$809	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$809, DW_AT_location[DW_OP_reg1]
DW$810	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$810, DW_AT_location[DW_OP_reg2]
DW$811	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$811, DW_AT_location[DW_OP_reg3]
DW$812	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$812, DW_AT_location[DW_OP_reg4]
DW$813	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$813, DW_AT_location[DW_OP_reg5]
DW$814	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$814, DW_AT_location[DW_OP_reg6]
DW$815	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$815, DW_AT_location[DW_OP_reg7]
DW$816	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$816, DW_AT_location[DW_OP_reg8]
DW$817	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$817, DW_AT_location[DW_OP_reg9]
DW$818	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$818, DW_AT_location[DW_OP_reg10]
DW$819	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$819, DW_AT_location[DW_OP_reg11]
DW$820	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$820, DW_AT_location[DW_OP_reg12]
DW$821	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$821, DW_AT_location[DW_OP_reg13]
DW$822	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$822, DW_AT_location[DW_OP_reg14]
DW$823	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$823, DW_AT_location[DW_OP_reg15]
DW$824	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$824, DW_AT_location[DW_OP_reg16]
DW$825	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$825, DW_AT_location[DW_OP_reg17]
DW$826	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$826, DW_AT_location[DW_OP_reg18]
DW$827	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$827, DW_AT_location[DW_OP_reg19]
DW$828	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$828, DW_AT_location[DW_OP_reg20]
DW$829	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$829, DW_AT_location[DW_OP_reg21]
DW$830	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$830, DW_AT_location[DW_OP_reg22]
DW$831	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$831, DW_AT_location[DW_OP_reg23]
DW$832	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$832, DW_AT_location[DW_OP_reg24]
DW$833	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$833, DW_AT_location[DW_OP_reg25]
DW$834	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$834, DW_AT_location[DW_OP_reg26]
DW$835	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$835, DW_AT_location[DW_OP_reg27]
DW$836	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$836, DW_AT_location[DW_OP_reg28]
DW$837	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$837, DW_AT_location[DW_OP_reg29]
DW$838	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$838, DW_AT_location[DW_OP_reg30]
DW$839	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$839, DW_AT_location[DW_OP_reg31]
DW$840	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$840, DW_AT_location[DW_OP_regx 0x20]
DW$841	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$841, DW_AT_location[DW_OP_regx 0x21]
DW$842	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$842, DW_AT_location[DW_OP_regx 0x22]
DW$843	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$843, DW_AT_location[DW_OP_regx 0x23]
DW$844	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$844, DW_AT_location[DW_OP_regx 0x24]
DW$845	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$845, DW_AT_location[DW_OP_regx 0x25]
DW$846	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$846, DW_AT_location[DW_OP_regx 0x26]
DW$847	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$847, DW_AT_location[DW_OP_regx 0x27]
DW$848	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$848, DW_AT_location[DW_OP_regx 0x28]
DW$849	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$849, DW_AT_location[DW_OP_regx 0x29]
DW$850	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$850, DW_AT_location[DW_OP_regx 0x2a]
DW$851	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$851, DW_AT_location[DW_OP_regx 0x2b]
DW$852	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$852, DW_AT_location[DW_OP_regx 0x2c]
DW$853	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$853, DW_AT_location[DW_OP_regx 0x2d]
DW$854	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$854, DW_AT_location[DW_OP_regx 0x2e]
DW$855	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$855, DW_AT_location[DW_OP_regx 0x2f]
DW$856	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$856, DW_AT_location[DW_OP_regx 0x30]
DW$857	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$857, DW_AT_location[DW_OP_regx 0x31]
DW$858	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$858, DW_AT_location[DW_OP_regx 0x32]
DW$859	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$859, DW_AT_location[DW_OP_regx 0x33]
DW$860	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$860, DW_AT_location[DW_OP_regx 0x34]
DW$861	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$861, DW_AT_location[DW_OP_regx 0x35]
DW$862	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$862, DW_AT_location[DW_OP_regx 0x36]
DW$863	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$863, DW_AT_location[DW_OP_regx 0x37]
DW$864	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$864, DW_AT_location[DW_OP_regx 0x38]
DW$865	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$865, DW_AT_location[DW_OP_regx 0x39]
DW$866	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$866, DW_AT_location[DW_OP_regx 0x3a]
DW$867	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$867, DW_AT_location[DW_OP_regx 0x3b]
DW$868	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$868, DW_AT_location[DW_OP_regx 0x3c]
DW$869	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$869, DW_AT_location[DW_OP_regx 0x3d]
DW$870	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$870, DW_AT_location[DW_OP_regx 0x3e]
DW$871	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$871, DW_AT_location[DW_OP_regx 0x3f]
DW$872	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$872, DW_AT_location[DW_OP_regx 0x40]
DW$873	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$873, DW_AT_location[DW_OP_regx 0x41]
DW$874	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$874, DW_AT_location[DW_OP_regx 0x42]
DW$875	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$875, DW_AT_location[DW_OP_regx 0x43]
DW$876	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$876, DW_AT_location[DW_OP_regx 0x44]
DW$877	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$877, DW_AT_location[DW_OP_regx 0x45]
DW$878	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$878, DW_AT_location[DW_OP_regx 0x46]
DW$879	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$879, DW_AT_location[DW_OP_regx 0x47]
DW$880	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$880, DW_AT_location[DW_OP_regx 0x48]
DW$881	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$881, DW_AT_location[DW_OP_regx 0x49]
DW$882	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$882, DW_AT_location[DW_OP_regx 0x4a]
DW$883	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$883, DW_AT_location[DW_OP_regx 0x4b]
DW$884	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$884, DW_AT_location[DW_OP_regx 0x4c]
DW$885	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$885, DW_AT_location[DW_OP_regx 0x4d]
DW$886	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$886, DW_AT_location[DW_OP_regx 0x4e]
DW$887	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$887, DW_AT_location[DW_OP_regx 0x4f]
DW$888	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$888, DW_AT_location[DW_OP_regx 0x50]
DW$889	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$889, DW_AT_location[DW_OP_regx 0x51]
DW$890	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$890, DW_AT_location[DW_OP_regx 0x52]
DW$891	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$891, DW_AT_location[DW_OP_regx 0x53]
DW$892	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$892, DW_AT_location[DW_OP_regx 0x54]
DW$893	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$893, DW_AT_location[DW_OP_regx 0x55]
DW$894	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$894, DW_AT_location[DW_OP_regx 0x56]
DW$895	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$895, DW_AT_location[DW_OP_regx 0x57]
DW$896	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$896, DW_AT_location[DW_OP_regx 0x58]
DW$897	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$897, DW_AT_location[DW_OP_regx 0x59]
DW$898	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$898, DW_AT_location[DW_OP_regx 0x5a]
DW$899	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$899, DW_AT_location[DW_OP_regx 0x5b]
DW$900	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$900, DW_AT_location[DW_OP_regx 0x5c]
DW$901	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$901, DW_AT_location[DW_OP_regx 0x5d]
DW$902	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$902, DW_AT_location[DW_OP_regx 0x5e]
DW$903	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$903, DW_AT_location[DW_OP_regx 0x5f]
DW$904	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$904, DW_AT_location[DW_OP_regx 0x60]
DW$905	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$905, DW_AT_location[DW_OP_regx 0x61]
DW$906	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$906, DW_AT_location[DW_OP_regx 0x62]
DW$907	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$907, DW_AT_location[DW_OP_regx 0x63]
DW$908	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$908, DW_AT_location[DW_OP_regx 0x64]
DW$909	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$909, DW_AT_location[DW_OP_regx 0x65]
DW$910	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$910, DW_AT_location[DW_OP_regx 0x66]
DW$911	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$911, DW_AT_location[DW_OP_regx 0x67]
DW$912	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$912, DW_AT_location[DW_OP_regx 0x68]
DW$913	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$913, DW_AT_location[DW_OP_regx 0x69]
DW$914	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$914, DW_AT_location[DW_OP_regx 0x6a]
DW$915	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$915, DW_AT_location[DW_OP_regx 0x6b]
DW$916	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$916, DW_AT_location[DW_OP_regx 0x6c]
DW$917	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$917, DW_AT_location[DW_OP_regx 0x6d]
DW$918	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$918, DW_AT_location[DW_OP_regx 0x6e]
DW$919	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$919, DW_AT_location[DW_OP_regx 0x6f]
DW$920	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$920, DW_AT_location[DW_OP_regx 0x70]
DW$921	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$921, DW_AT_location[DW_OP_regx 0x71]
DW$922	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$922, DW_AT_location[DW_OP_regx 0x72]
DW$923	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$923, DW_AT_location[DW_OP_regx 0x73]
DW$924	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$924, DW_AT_location[DW_OP_regx 0x74]
DW$925	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$925, DW_AT_location[DW_OP_regx 0x75]
DW$926	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$926, DW_AT_location[DW_OP_regx 0x76]
DW$927	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$927, DW_AT_location[DW_OP_regx 0x77]
DW$928	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$928, DW_AT_location[DW_OP_regx 0x78]
DW$929	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$929, DW_AT_location[DW_OP_regx 0x79]
DW$930	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$930, DW_AT_location[DW_OP_regx 0x7a]
DW$931	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$931, DW_AT_location[DW_OP_regx 0x7b]
DW$932	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$932, DW_AT_location[DW_OP_regx 0x7c]
DW$933	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$933, DW_AT_location[DW_OP_regx 0x7d]
DW$934	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$934, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

