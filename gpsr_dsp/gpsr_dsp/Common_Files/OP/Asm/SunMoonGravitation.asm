;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:49:10 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("SunMoonGravitation.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("memset"), DW_AT_symbol_name("_memset")
	.dwattr DW$1, DW_AT_type(*DW$T$3)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$23)
	.dwendtag DW$1


DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt"), DW_AT_symbol_name("_sqrt")
	.dwattr DW$5, DW_AT_type(*DW$T$17)
	.dwattr DW$5, DW_AT_declaration(0x01)
	.dwattr DW$5, DW_AT_external(0x01)
DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$5


DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("sin"), DW_AT_symbol_name("_sin")
	.dwattr DW$7, DW_AT_type(*DW$T$17)
	.dwattr DW$7, DW_AT_declaration(0x01)
	.dwattr DW$7, DW_AT_external(0x01)
DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$7


DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("cos"), DW_AT_symbol_name("_cos")
	.dwattr DW$9, DW_AT_type(*DW$T$17)
	.dwattr DW$9, DW_AT_declaration(0x01)
	.dwattr DW$9, DW_AT_external(0x01)
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$9

_l$1:	.usect	".far",8,8
_l1$2:	.usect	".far",8,8
_F$3:	.usect	".far",8,8
_D$4:	.usect	".far",8,8
_OM$5:	.usect	".far",8,8
_L$6:	.usect	".far",8,8
_lamda$7:	.usect	".far",8,8
_beta$8:	.usect	".far",8,8
_Q$9:	.usect	".far",8,8
_rm$10:	.usect	".far",8,8
_sin_l$11:	.usect	".far",8,8
_sin_l1$12:	.usect	".far",8,8
_sin_D$13:	.usect	".far",8,8
_sin_F$14:	.usect	".far",8,8
_sin_2l$15:	.usect	".far",8,8
_sin_2D$16:	.usect	".far",8,8
_sin_2F$17:	.usect	".far",8,8
_sin_4D$18:	.usect	".far",8,8
_cos_l$19:	.usect	".far",8,8
_cos_l1$20:	.usect	".far",8,8
_cos_D$21:	.usect	".far",8,8
_cos_F$22:	.usect	".far",8,8
_cos_2l$23:	.usect	".far",8,8
_cos_2D$24:	.usect	".far",8,8
_cos_2F$25:	.usect	".far",8,8
_cos_4D$26:	.usect	".far",8,8
_sin_lm2D$27:	.usect	".far",8,8
_cos_lm2D$28:	.usect	".far",8,8
_dtm1$29:	.usect	".far",8,8
_dtm2$30:	.usect	".far",8,8
;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI0922 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI0924 
	.sect	".text"
	.global	_SunMoonData_Init

DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("SunMoonData_Init"), DW_AT_symbol_name("_SunMoonData_Init")
	.dwattr DW$11, DW_AT_low_pc(_SunMoonData_Init)
	.dwattr DW$11, DW_AT_high_pc(0x00)
	.dwattr DW$11, DW_AT_begin_file("SunMoonGravitation.c")
	.dwattr DW$11, DW_AT_begin_line(0x48)
	.dwattr DW$11, DW_AT_begin_column(0x06)
	.dwattr DW$11, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr DW$11, DW_AT_skeletal(0x01)
	.dwpsn	"SunMoonGravitation.c",73,1

;******************************************************************************
;* FUNCTION NAME: _SunMoonData_Init                                           *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,B13,SP,A16,A17,A18,A19,A20,A21,A22,  *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 8 Save = 8 byte                    *
;******************************************************************************
_SunMoonData_Init:
;** --------------------------------------------------------------------------*
DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pSMD"), DW_AT_symbol_name("_pSMD")
	.dwattr DW$12, DW_AT_type(*DW$T$32)
	.dwattr DW$12, DW_AT_location[DW_OP_reg4]
           MVKL    .S2     _memset,B5        ; |75| 
           MVKH    .S2     _memset,B5        ; |75| 
           CALL    .S2     B5                ; |75| 
           ZERO    .L2     B4                ; |75| 
           MVK     .S1     0x30,A6           ; |75| 
           NOP             1
           STW     .D2T1   A10,*SP--(8)      ; |73| 

           ADDKPC  .S2     RL0,B3,0          ; |75| 
||         MV      .L2     B3,B13            ; |73| 
||         MV      .L1     A4,A10            ; |73| 
||         STW     .D2T2   B13,*+SP(4)       ; |73| 

RL0:       ; CALL OCCURS {_memset}           ; |75| 
;** --------------------------------------------------------------------------*
           ZERO    .L1     A5                ; |78| 

           MVKH    .S1     0x3ff00000,A5     ; |78| 
||         MV      .L2     B13,B3            ; |81| 
||         ZERO    .L1     A4                ; |78| 

           RET     .S2     B3                ; |81| 
||         STDW    .D1T1   A5:A4,*+A10(8)    ; |78| 
||         LDW     .D2T2   *+SP(4),B13       ; |81| 

           LDW     .D2T1   *++SP(8),A10      ; |81| 
	.dwpsn	"SunMoonGravitation.c",81,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |81| 
	.dwattr DW$11, DW_AT_end_file("SunMoonGravitation.c")
	.dwattr DW$11, DW_AT_end_line(0x51)
	.dwattr DW$11, DW_AT_end_column(0x01)
	.dwendtag DW$11

	.sect	".text"
	.global	_SunMoonData_Gravitation

DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("SunMoonData_Gravitation"), DW_AT_symbol_name("_SunMoonData_Gravitation")
	.dwattr DW$13, DW_AT_low_pc(_SunMoonData_Gravitation)
	.dwattr DW$13, DW_AT_high_pc(0x00)
	.dwattr DW$13, DW_AT_begin_file("SunMoonGravitation.c")
	.dwattr DW$13, DW_AT_begin_line(0x5f)
	.dwattr DW$13, DW_AT_begin_column(0x06)
	.dwattr DW$13, DW_AT_frame_base[DW_OP_breg31 192]
	.dwattr DW$13, DW_AT_skeletal(0x01)
	.dwpsn	"SunMoonGravitation.c",100,1

;******************************************************************************
;* FUNCTION NAME: _SunMoonData_Gravitation                                    *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,   *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Local Frame Size  : 0 Args + 144 Auto + 44 Save = 188 byte               *
;******************************************************************************
_SunMoonData_Gravitation:
;** --------------------------------------------------------------------------*
DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pSMD"), DW_AT_symbol_name("_pSMD")
	.dwattr DW$14, DW_AT_type(*DW$T$32)
	.dwattr DW$14, DW_AT_location[DW_OP_reg4]
DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("r"), DW_AT_symbol_name("_r")
	.dwattr DW$15, DW_AT_type(*DW$T$34)
	.dwattr DW$15, DW_AT_location[DW_OP_reg20]
DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mjd_u"), DW_AT_symbol_name("_mjd_u")
	.dwattr DW$16, DW_AT_type(*DW$T$19)
	.dwattr DW$16, DW_AT_location[DW_OP_reg6]
DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("fUsing"), DW_AT_symbol_name("_fUsing")
	.dwattr DW$17, DW_AT_type(*DW$T$35)
	.dwattr DW$17, DW_AT_location[DW_OP_reg22]
DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_name("g_sm"), DW_AT_symbol_name("_g_sm")
	.dwattr DW$18, DW_AT_type(*DW$T$34)
	.dwattr DW$18, DW_AT_location[DW_OP_reg8]

           MVKL    .S1     _memset,A3        ; |107| 
||         MV      .L1X    B6,A0             ; |107| 

           MVKH    .S1     _memset,A3        ; |107| 
||         MV      .L1X    SP,A31            ; |100| 
||         ADDK    .S2     -192,SP           ; |100| 

           STW     .D1T1   A14,*-A31(24)
||         STDW    .D2T2   B13:B12,*+SP(184)
||         MV      .L1     A8,A14            ; |100| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STDW    .D2T2   B11:B10,*+SP(176)
|| [ A0]   B       .S2     L1                ; |105| 
||         MV      .L2X    A6,B10            ; |100| 
||         MVK     .S1     0x18,A6           ; |107| 

           STDW    .D1T1   A11:A10,*-A31(40)
||         STW     .D2T2   B3,*+SP(172)
|| [!A0]   CALL    .S2X    A3                ; |107| 

           STW     .D2T1   A15,*+SP(192)
||         MV      .L2X    A7,B11            ; |100| 

           STW     .D2T1   A4,*+SP(96)       ; |100| 
||         MV      .L1     A8,A4             ; |100| 

           STW     .D2T2   B4,*+SP(100)      ; |100| 
||         ZERO    .L2     B4                ; |107| 

           STW     .D2T2   B6,*+SP(104)      ; |100| 
|| [ A0]   MVKL    .S2     __negd,B6         ; |114| 

           ; BRANCHCC OCCURS {L1}            ; |105| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL2,B3,0          ; |107| 
RL2:       ; CALL OCCURS {_memset}           ; |107| 
           MVKL    .S2     __negd,B6         ; |114| 
;** --------------------------------------------------------------------------*
L1:    

           LDW     .D2T1   *+SP(96),A0       ; |114| 
||         MVKH    .S2     __negd,B6         ; |114| 
||         MVKL    .S1     __subd,A3         ; |114| 
||         MV      .L2     B11,B5            ; |114| 
||         ZERO    .L1     A11               ; |114| 

           MVKH    .S1     __subd,A3         ; |114| 
||         MV      .L2     B10,B4            ; |114| 

           MV      .L1X    B6,A10            ; |114| 
||         MVKL    .S1     __cmpd,A12        ; |114| 

           MVKH    .S1     __cmpd,A12        ; |114| 
           NOP             1

   [!A0]   MVKL    .S1     _memset,A3        ; |126| 
||         MV      .L2X    A0,B6
|| [!A0]   B       .S2     L7                ; |111| 

   [!A0]   MVKH    .S1     _memset,A3        ; |126| 
|| [ A0]   LDDW    .D2T2   *+B6(16),B9:B8    ; |114| 

   [ A0]   LDDW    .D2T2   *B6,B13:B12       ; |114| 
|| [ A0]   CALL    .S2X    A3                ; |114| 

   [!A0]   CALL    .S2X    A3                ; |126| 
           NOP             2
           ; BRANCHCC OCCURS {L7}            ; |111| 
;** --------------------------------------------------------------------------*
           MV      .L1X    B9,A5             ; |114| 

           ADDKPC  .S2     RL3,B3,0          ; |114| 
||         MV      .L1X    B8,A4             ; |114| 

RL3:       ; CALL OCCURS {__subd}            ; |114| 
;** --------------------------------------------------------------------------*
           MV      .L2X    A10,B4            ; |114| 
           CALL    .S2     B4                ; |114| 
           MV      .S1     A4,A15            ; |114| 
           MV      .L1X    B12,A4            ; |114| 
           ADDKPC  .S2     RL5,B3,0          ; |114| 
           MV      .S1     A5,A13            ; |114| 
           MV      .L1X    B13,A5            ; |114| 
RL5:       ; CALL OCCURS {__negd}            ; |114| 
           CALL    .S2X    A12               ; |114| 
           MV      .L2X    A5,B5             ; |114| 
           ADDKPC  .S2     RL6,B3,0          ; |114| 
           MV      .S1     A13,A5            ; |114| 
           MV      .L2X    A4,B4             ; |114| 
           MV      .L1     A15,A4            ; |114| 
RL6:       ; CALL OCCURS {__cmpd}            ; |114| 
           MVKL    .S2     __cmpd,B6         ; |114| 
           MVKH    .S2     __cmpd,B6         ; |114| 
           CALL    .S2     B6                ; |114| 
           ADDKPC  .S2     RL7,B3,0          ; |114| 
           MV      .D1     A13,A5            ; |114| 
           CMPGT   .L1     A4,0,A10          ; |114| 
           MV      .S1     A15,A4            ; |114| 

           MV      .L2     B13,B5            ; |114| 
||         MV      .D2     B12,B4            ; |114| 

RL7:       ; CALL OCCURS {__cmpd}            ; |114| 
;** --------------------------------------------------------------------------*

           LDW     .D2T1   *+SP(96),A3       ; |114| 
||         MVK     .S1     48,A6             ; |38| 
||         MV      .D1     A10,A2            ; |114| 
||         CMPLT   .L1     A4,0,A0           ; |114| 

           NOP             3
           MV      .L1X    B10,A4            ; |121| 

           ZERO    .S1     A3                ; |114| 
||         MV      .L2X    A3,B4             ; |38| 
||         SUB     .L1     A14,A3,A5         ; |38| 

   [ A0]   MVK     .D1     0x1,A3            ; |114| 
||         SUB     .L2X    B4,A14,B5         ; |38| 
||         MVKL    .S1     __addd,A6         ; |121| 
||         CMPLT   .L1     A5,A6,A5          ; |38| 

           MVKH    .S1     __addd,A6         ; |121| 
||         CMPLT   .L2     B5,0,B5           ; |38| 

           MV      .L1X    B11,A5            ; |121| 
|| [ A2]   MVK     .S2     0x1,B5            ; |114| 
||         AND     .L2X    A5,B5,B6

           MV      .S2     B12,B4            ; |121| 
||         ADD     .D2     B4,24,B6          ; |36| 
||         XOR     .L2     1,B6,B0

   [ A2]   MV      .L1X    B5,A11            ; |114| 
           NOP             1
           MV      .L2X    A11,B5            ; |114| 

           MV      .S2     B13,B5            ; |121| 
||         AND     .L2X    B5,A3,B1          ; |114| 

   [!B1]   ZERO    .L2     B0                ; nullify predicate
|| [ B1]   LDW     .D2T2   *+SP(96),B5
|| [ B1]   MVK     .S2     0x18,B7
|| [!B1]   B       .S1     L6                ; |114| 

   [ B0]   B       .S1     L5                ; |38| 
|| [ B1]   MV      .L2X    A14,B4

   [!B1]   CALL    .S2X    A6                ; |121| 
           NOP             2
   [ B1]   ADD     .D2     B5,24,B5
           ; BRANCHCC OCCURS {L6}            ; |114| 
;** --------------------------------------------------------------------------*

   [!B0]   SUB     .L1X    B7,1,A0
|| [ B0]   LDB     .D2T2   *B6,B4            ; |40| 

           ; BRANCHCC OCCURS {L5}            ; |38| 
;** --------------------------------------------------------------------------*
           MVC     .S2     CSR,B7
           AND     .L2     -2,B7,B6
           MVC     .S2     B6,CSR            ; interrupts off
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 38
;*      Loop opening brace source line   : 39
;*      Loop closing brace source line   : 41
;*      Known Minimum Trip Count         : 24                    
;*      Known Maximum Trip Count         : 24                    
;*      Known Max Trip Count Factor      : 24
;*      Loop Carried Dependency Bound(^) : 6
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1        0     
;*      .D units                     0        2*    
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             0        2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 1 iterations in parallel
;*      Done
;*
;*      Collapsed epilog stages     : 0
;*      Collapsed prolog stages     : 0
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L2:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L3:    ; PIPED LOOP KERNEL
DW$L$_SunMoonData_Gravitation$10$B:
	.dwpsn	"SunMoonGravitation.c",39,0

   [ A0]   BDEC    .S1     L3,A0             ; |38| <0,0> 
||         LDB     .D2T2   *B5++,B6          ; |40| <0,0>  ^ 

           NOP             4
	.dwpsn	"SunMoonGravitation.c",41,0
           STB     .D2T2   B6,*B4++          ; |40| <0,5>  ^ 
DW$L$_SunMoonData_Gravitation$10$E:
;** --------------------------------------------------------------------------*
L4:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(172),B3      ; |205| 
           LDW     .D2T1   *+SP(192),A15     ; |205| 
           LDDW    .D2T1   *+SP(160),A13:A12 ; |205| 
           LDDW    .D2T2   *+SP(184),B13:B12 ; |205| 
           LDW     .D2T1   *+SP(168),A14     ; |205| 

           RET     .S2     B3                ; |205| 
||         LDDW    .D2T1   *+SP(152),A11:A10 ; |205| 

           LDDW    .D2T2   *+SP(176),B11:B10 ; |205| 
           NOP             1
           MVC     .S2     B7,CSR            ; interrupts on
           NOP             1
           ADDK    .S2     192,SP            ; |205| 
           ; BRANCH OCCURS {B3}              ; |205| 
;** --------------------------------------------------------------------------*
L5:    
           LDW     .D2T2   *+SP(172),B3      ; |205| 
           LDDW    .D2T2   *+SP(184),B13:B12 ; |205| 
           LDDW    .D2T2   *+SP(176),B11:B10 ; |205| 
           LDDW    .D2T1   *+SP(160),A13:A12 ; |205| 

           STB     .D1T2   B4,*A14           ; |40| 
||         LDDW    .D2T1   *+SP(152),A11:A10 ; |205| 

           LDB     .D2T2   *+B6(1),B4        ; |40| 
           NOP             3
           LDW     .D2T1   *+SP(192),A15     ; |205| 
           STB     .D1T2   B4,*+A14(1)       ; |40| 
           LDB     .D2T2   *+B6(2),B4        ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(2)       ; |40| 
           LDB     .D2T2   *+B6(3),B4        ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(3)       ; |40| 
           LDB     .D2T1   *+B6(4),A3        ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(4)       ; |40| 
           LDB     .D2T1   *+B6(5),A3        ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(5)       ; |40| 
           LDB     .D2T1   *+B6(6),A3        ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(6)       ; |40| 
           LDB     .D2T1   *+B6(7),A3        ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(7)       ; |40| 
           LDB     .D2T1   *+B6(8),A3        ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(8)       ; |40| 
           LDB     .D2T2   *+B6(9),B4        ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(9)       ; |40| 
           LDB     .D2T2   *+B6(10),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(10)      ; |40| 
           LDB     .D2T2   *+B6(11),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(11)      ; |40| 
           LDB     .D2T2   *+B6(12),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(12)      ; |40| 
           LDB     .D2T2   *+B6(13),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(13)      ; |40| 
           LDB     .D2T1   *+B6(14),A3       ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(14)      ; |40| 
           LDB     .D2T1   *+B6(15),A3       ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(15)      ; |40| 
           LDB     .D2T1   *+B6(16),A3       ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(16)      ; |40| 
           LDB     .D2T1   *+B6(17),A3       ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(17)      ; |40| 
           LDB     .D2T1   *+B6(18),A3       ; |40| 
           NOP             4
           STB     .D1T1   A3,*+A14(18)      ; |40| 
           LDB     .D2T2   *+B6(19),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(19)      ; |40| 
           LDB     .D2T2   *+B6(20),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(20)      ; |40| 
           LDB     .D2T2   *+B6(21),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(21)      ; |40| 
           LDB     .D2T2   *+B6(22),B4       ; |40| 
           NOP             4
           STB     .D1T2   B4,*+A14(22)      ; |40| 
           LDB     .D2T2   *+B6(23),B4       ; |40| 
           NOP             3
           RET     .S2     B3                ; |205| 

           ADDK    .S2     192,SP            ; |205| 
||         LDW     .D2T1   *+SP(168),A14     ; |205| 
||         STB     .D1T2   B4,*+A14(23)      ; |40| 

           NOP             4
           ; BRANCH OCCURS {B3}              ; |205| 
;** --------------------------------------------------------------------------*
L6:    
           ADDKPC  .S2     RL8,B3,1          ; |121| 
RL8:       ; CALL OCCURS {__addd}            ; |121| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *+SP(96),B4       ; |121| 
||         MVKL    .S1     _memset,A3        ; |126| 

           MVKH    .S1     _memset,A3        ; |126| 
           MV      .L2X    A5,B11            ; |121| 
           CALL    .S2X    A3                ; |126| 
           MV      .L2X    A4,B10            ; |121| 
           STDW    .D2T2   B11:B10,*+B4(16)  ; |122| 
;** --------------------------------------------------------------------------*
L7:    
           ADDKPC  .S2     RL9,B3,0          ; |126| 
           MVK     .S1     0x18,A6           ; |126| 

           ZERO    .L2     B4                ; |126| 
||         MV      .L1     A14,A4            ; |126| 

RL9:       ; CALL OCCURS {_memset}           ; |126| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(96),B4       ; |126| 
           NOP             2
           MVKL    .S1     __mpyd,A3         ; |130| 
           MVKH    .S1     __mpyd,A3         ; |130| 
           LDDW    .D2T2   *+B4(8),B7:B6     ; |130| 
           CALL    .S2X    A3                ; |130| 
           MVKL    .S2     0x3ee845c8,B5     ; |130| 
           MVKH    .S2     0x3ee845c8,B5     ; |130| 
           MVKL    .S2     0xa0ce5129,B4     ; |130| 

           MV      .L1X    B6,A4             ; |130| 
||         MVKH    .S2     0xa0ce5129,B4     ; |130| 

           MV      .L1X    B7,A5             ; |130| 
||         ADDKPC  .S2     RL13,B3,0         ; |130| 

RL13:      ; CALL OCCURS {__mpyd}            ; |130| 
           MVKL    .S1     __addd,A3         ; |130| 
           MVKH    .S1     __addd,A3         ; |130| 
           MV      .L2X    A4,B4             ; |130| 
           CALL    .S2X    A3                ; |130| 
           MV      .L1X    B10,A4            ; |130| 
           MV      .L2X    A5,B5             ; |130| 
           ADDKPC  .S2     RL14,B3,1         ; |130| 
           MV      .L1X    B11,A5            ; |130| 
RL14:      ; CALL OCCURS {__addd}            ; |130| 
           MVKL    .S1     __mpyd,A3         ; |130| 
           MVKH    .S1     __mpyd,A3         ; |130| 
           MVKL    .S2     0x3efcb55c,B5     ; |130| 
           CALL    .S2X    A3                ; |130| 
           MVKL    .S2     0xbc173dd3,B4     ; |130| 
           MVKH    .S2     0x3efcb55c,B5     ; |130| 
           MVKH    .S2     0xbc173dd3,B4     ; |130| 
           ADDKPC  .S2     RL15,B3,1         ; |130| 
RL15:      ; CALL OCCURS {__mpyd}            ; |130| 
           MVKL    .S1     _EclToCurrentEq_Matrix,A3 ; |133| 
           MVKH    .S1     _EclToCurrentEq_Matrix,A3 ; |133| 
           STW     .D2T1   A4,*+SP(112)      ; |130| 
           CALL    .S2X    A3                ; |133| 
           ADDKPC  .S2     RL16,B3,1         ; |133| 
           STW     .D2T1   A5,*+SP(108)      ; |130| 
           NOP             1
           ADD     .L2     8,SP,B4           ; |133| 
RL16:      ; CALL OCCURS {_EclToCurrentEq_Matrix}  ; |133| 
           MVKL    .S1     __mpyd,A3         ; |135| 
           MVKH    .S1     __mpyd,A3         ; |135| 
           MVKL    .S2     0xbc173dd3,B4     ; |135| 
           CALL    .S2X    A3                ; |135| 
           MVKL    .S2     0x3efcb55c,B5     ; |135| 
           MVKH    .S2     0xbc173dd3,B4     ; |135| 
           MV      .L1X    B10,A4            ; |135| 
           MVKH    .S2     0x3efcb55c,B5     ; |135| 

           MV      .L1X    B11,A5            ; |135| 
||         ADDKPC  .S2     RL18,B3,0         ; |135| 

RL18:      ; CALL OCCURS {__mpyd}            ; |135| 
           MVKL    .S1     _EqToECEF_Matrix,A3 ; |135| 
           MVKH    .S1     _EqToECEF_Matrix,A3 ; |135| 
           ADDAD   .D2     SP,5,B4           ; |135| 
           CALL    .S2X    A3                ; |135| 
           ADDKPC  .S2     RL19,B3,4         ; |135| 
RL19:      ; CALL OCCURS {_EqToECEF_Matrix}  ; |135| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *+SP(104),B4      ; |142| 
||         MVKL    .S1     _SunPos_GeoCenterEclFrame,A6 ; |142| 
||         MVKL    .S2     __mpyd,B12        ; |147| 

           MVKH    .S1     _SunPos_GeoCenterEclFrame,A6 ; |142| 
||         LDW     .D2T1   *+SP(112),A4      ; |142| 
||         MVKL    .S2     __mpyd,B13        ; |146| 

           LDW     .D2T1   *+SP(108),A5      ; |142| 
||         MVKL    .S1     __addd,A10        ; |150| 
||         MVKH    .S2     __mpyd,B12        ; |147| 

           MVKL    .S1     __mpyd,A13        ; |147| 
||         MVKH    .S2     __mpyd,B13        ; |146| 

           MVKL    .S1     __addd,A12        ; |147| 
||         MVKL    .S2     __mpyd,B10        ; |150| 

           AND     .L2     1,B4,B0           ; |139| 
||         ADDAD   .D2     SP,9,B4           ; |142| 
||         MVKL    .S1     __mpyd,A11        ; |150| 
||         MVKL    .S2     __addd,B11        ; |146| 

   [!B0]   MVKL    .S2     __mpyd,B4         ; |177| 
||         MVKH    .S1     __mpyd,A11        ; |150| 
|| [!B0]   LDW     .D2T2   *+SP(104),B31     ; |177| 

           MVKL    .S1     __mpyd,A3         ; |151| 
|| [!B0]   MVKL    .S2     __mpyd,B12        ; |178| 

   [!B0]   MVKH    .S2     __mpyd,B4         ; |177| 
||         MVKH    .S1     __addd,A10        ; |150| 

   [!B0]   STW     .D2T2   B4,*+SP(128)      ; |177| 
||         MVKH    .S1     __mpyd,A13        ; |147| 
|| [!B0]   MVKL    .S2     __addd,B13        ; |177| 

           MVKH    .S1     __addd,A12        ; |147| 
||         MVKH    .S2     __mpyd,B10        ; |150| 

           MVKH    .S1     __mpyd,A3         ; |151| 
||         MVKH    .S2     __addd,B11        ; |146| 

           MVKL    .S1     __mpyd,A15        ; |146| 
||         STW     .D2T1   A3,*+SP(116)      ; |142| 
|| [!B0]   B       .S2     L8                ; |139| 

   [!B0]   MVKL    .S1     __mpyd,A10        ; |182| 
|| [ B0]   CALL    .S2X    A6                ; |142| 

   [!B0]   MVKL    .S1     __mpyd,A13        ; |178| 
   [!B0]   MVKL    .S1     __mpyd,A11        ; |181| 
   [!B0]   MVKL    .S1     __addd,A12        ; |178| 
           MVKH    .S1     __mpyd,A15        ; |146| 
           ; BRANCHCC OCCURS {L8}            ; |139| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL20,B3,0         ; |142| 
RL20:      ; CALL OCCURS {_SunPos_GeoCenterEclFrame}  ; |142| 
;** --------------------------------------------------------------------------*
           LDDW    .D2T2   *+SP(8),B7:B6     ; |146| 
           NOP             1
           LDDW    .D2T2   *+SP(80),B5:B4    ; |146| 
           NOP             2
           MV      .L1X    B6,A4             ; |146| 

           LDDW    .D2T2   *+SP(72),B7:B6    ; |145| 
||         MV      .L1X    B7,A5             ; |146| 

           NOP             2
           STW     .D2T2   B4,*+SP(120)      ; |146| 
           STW     .D2T2   B5,*+SP(124)      ; |146| 
           STW     .D2T2   B7,*+SP(132)      ; |145| 
           STW     .D2T2   B6,*+SP(128)      ; |145| 
           LDDW    .D2T2   *+SP(88),B7:B6    ; |146| 
           CALL    .S2     B13               ; |146| 
           ADDKPC  .S2     RL23,B3,2         ; |146| 
           STW     .D2T2   B6,*+SP(136)      ; |146| 
           STW     .D2T2   B7,*+SP(140)      ; |146| 
RL23:      ; CALL OCCURS {__mpyd}            ; |146| 
           LDDW    .D2T2   *+SP(16),B7:B6    ; |146| 

           CALL    .S2X    A15               ; |146| 
||         LDW     .D2T2   *+SP(140),B5      ; |146| 

           LDW     .D2T2   *+SP(136),B4      ; |146| 
           STW     .D2T1   A4,*+SP(144)      ; |146| 
           ADDKPC  .S2     RL24,B3,0         ; |146| 
           MV      .L1X    B6,A4             ; |146| 

           MV      .L2X    A5,B13            ; |146| 
||         MV      .L1X    B7,A5             ; |146| 

RL24:      ; CALL OCCURS {__mpyd}            ; |146| 
           CALL    .S2     B11               ; |146| 

           LDW     .D2T1   *+SP(144),A4      ; |146| 
||         MV      .L2X    A4,B4             ; |146| 

           ADDKPC  .S2     RL25,B3,1         ; |146| 
           MV      .L2X    A5,B5             ; |146| 
           MV      .L1X    B13,A5            ; |146| 
RL25:      ; CALL OCCURS {__addd}            ; |146| 
           LDDW    .D2T2   *+SP(24),B7:B6    ; |147| 

           LDW     .D2T2   *+SP(124),B5      ; |147| 
||         CALL    .S2X    A13               ; |147| 

           LDW     .D2T2   *+SP(120),B4      ; |147| 
           MV      .L2X    A4,B11            ; |146| 
           MV      .S1     A5,A15            ; |146| 
           MV      .L1X    B6,A4             ; |147| 

           ADDKPC  .S2     RL28,B3,0         ; |147| 
||         MV      .L1X    B7,A5             ; |147| 

RL28:      ; CALL OCCURS {__mpyd}            ; |147| 
           LDDW    .D2T2   *+SP(32),B7:B6    ; |147| 

           LDW     .D2T2   *+SP(140),B5      ; |147| 
||         CALL    .S2     B12               ; |147| 

           LDW     .D2T2   *+SP(136),B4      ; |147| 
           MV      .S1     A5,A13            ; |147| 
           MV      .L2X    A4,B13            ; |147| 
           MV      .L1X    B7,A5             ; |147| 

           ADDKPC  .S2     RL29,B3,0         ; |147| 
||         MV      .L1X    B6,A4             ; |147| 

RL29:      ; CALL OCCURS {__mpyd}            ; |147| 
           CALL    .S2X    A12               ; |147| 
           MV      .L2X    A5,B5             ; |147| 
           MV      .L2X    A4,B4             ; |147| 
           MV      .S1     A13,A5            ; |147| 
           ADDKPC  .S2     RL30,B3,0         ; |147| 
           MV      .L1X    B13,A4            ; |147| 
RL30:      ; CALL OCCURS {__addd}            ; |147| 
           LDDW    .D2T2   *+SP(40),B7:B6    ; |150| 

           LDW     .D2T2   *+SP(132),B5      ; |150| 
||         CALL    .S2     B10               ; |150| 

           LDW     .D2T2   *+SP(128),B4      ; |150| 
           MV      .L2X    A4,B13            ; |147| 
           MV      .L2X    A5,B12            ; |147| 
           MV      .L1X    B6,A4             ; |150| 

           ADDKPC  .S2     RL33,B3,0         ; |150| 
||         MV      .L1X    B7,A5             ; |150| 

RL33:      ; CALL OCCURS {__mpyd}            ; |150| 
           LDDW    .D2T2   *+SP(48),B7:B6    ; |150| 
           CALL    .S2X    A11               ; |150| 
           MV      .S1     A4,A13            ; |150| 
           MV      .D2     B11,B4            ; |150| 
           MV      .L2X    A15,B5            ; |150| 
           MV      .L1X    B6,A4             ; |150| 

           MV      .S1     A5,A12            ; |150| 
||         MV      .L1X    B7,A5             ; |150| 
||         ADDKPC  .S2     RL34,B3,0         ; |150| 

RL34:      ; CALL OCCURS {__mpyd}            ; |150| 
           CALL    .S2X    A10               ; |150| 
           MV      .L2X    A4,B4             ; |150| 
           MV      .L2X    A5,B5             ; |150| 
           MV      .S1     A13,A4            ; |150| 
           ADDKPC  .S2     RL35,B3,0         ; |150| 
           MV      .L1     A12,A5            ; |150| 
RL35:      ; CALL OCCURS {__addd}            ; |150| 
           LDW     .D2T1   *+SP(116),A3      ; |151| 
           LDDW    .D2T2   *+SP(56),B7:B6    ; |151| 
           LDW     .D2T2   *+SP(132),B5      ; |151| 
           LDW     .D2T2   *+SP(128),B4      ; |151| 
           MV      .D1     A4,A12            ; |150| 
           CALL    .S2X    A3                ; |151| 
           MV      .S1     A5,A13            ; |150| 
           STDW    .D2T1   A13:A12,*+SP(72)  ; |151| 
           MV      .L1X    B7,A5             ; |151| 
           MV      .L1X    B6,A4             ; |151| 
           ADDKPC  .S2     RL38,B3,0         ; |151| 
RL38:      ; CALL OCCURS {__mpyd}            ; |151| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |151| 
           MVKH    .S1     __mpyd,A3         ; |151| 
           LDDW    .D2T2   *+SP(64),B7:B6    ; |151| 
           CALL    .S2X    A3                ; |151| 
           MV      .S1     A4,A11            ; |151| 
           ADDKPC  .S2     RL39,B3,0         ; |151| 
           MV      .L2X    A15,B5            ; |151| 
           MV      .L1X    B6,A4             ; |151| 

           MV      .D2     B11,B4            ; |151| 
||         MV      .S1     A5,A10            ; |151| 
||         MV      .L1X    B7,A5             ; |151| 

RL39:      ; CALL OCCURS {__mpyd}            ; |151| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |151| 
           MVKH    .S1     __addd,A3         ; |151| 
           MV      .L2X    A4,B4             ; |151| 
           CALL    .S2X    A3                ; |151| 
           ADDKPC  .S2     RL40,B3,1         ; |151| 
           MV      .S1     A11,A4            ; |151| 
           MV      .L2X    A5,B5             ; |151| 
           MV      .L1     A10,A5            ; |151| 
RL40:      ; CALL OCCURS {__addd}            ; |151| 

           LDW     .D2T2   *+SP(100),B12     ; |151| 
||         MV      .L2     B12,B11           ; |152| 

           MVKL    .S2     __subd,B6         ; |155| 
           MVKH    .S2     __subd,B6         ; |155| 
           MV      .L2     B13,B10           ; |152| 
           CALL    .S2     B6                ; |155| 
           LDDW    .D2T2   *B12,B5:B4        ; |155| 
           MV      .S1     A4,A10            ; |151| 
           MV      .L1     A5,A11            ; |151| 
           STDW    .D2T2   B11:B10,*+SP(88)  ; |155| 

           MV      .S1     A12,A4            ; |155| 
||         MV      .L1     A13,A5            ; |155| 
||         STDW    .D2T1   A11:A10,*+SP(80)  ; |152| 
||         ADDKPC  .S2     RL41,B3,0         ; |155| 

RL41:      ; CALL OCCURS {__subd}            ; |155| 
           MVKL    .S2     __subd,B6         ; |155| 
           MVKH    .S2     __subd,B6         ; |155| 
           CALL    .S2     B6                ; |155| 
           LDDW    .D2T2   *+B12(8),B5:B4    ; |155| 
           STW     .D2T1   A5,*+SP(136)      ; |155| 
           ADDKPC  .S2     RL42,B3,0         ; |155| 
           STW     .D2T1   A4,*+SP(124)      ; |155| 

           MV      .S1     A10,A4            ; |155| 
||         MV      .L1     A11,A5            ; |155| 

RL42:      ; CALL OCCURS {__subd}            ; |155| 
           MVKL    .S2     __subd,B6         ; |156| 
           MVKH    .S2     __subd,B6         ; |156| 

           MV      .L2     B12,B4            ; |155| 
||         CALL    .S2     B6                ; |156| 

           LDDW    .D2T2   *+B4(16),B5:B4    ; |156| 
           STW     .D2T1   A4,*+SP(120)      ; |155| 
           MV      .L1X    B10,A4            ; |156| 
           MV      .S1     A5,A12            ; |155| 

           ADDKPC  .S2     RL43,B3,0         ; |156| 
||         MV      .L1X    B11,A5            ; |156| 

RL43:      ; CALL OCCURS {__subd}            ; |156| 
           MVKL    .S2     __mpyd,B6         ; |156| 

           LDW     .D2T1   *+SP(124),A4      ; |156| 
||         MVKH    .S2     __mpyd,B6         ; |156| 
||         MV      .L1     A4,A15            ; |156| 

           LDW     .D2T1   *+SP(136),A5      ; |156| 
||         MV      .L1     A5,A13            ; |156| 
||         CALL    .S2     B6                ; |156| 

           ADDKPC  .S2     RL50,B3,2         ; |156| 
           MV      .L2X    A4,B4             ; |156| 
           MV      .L2X    A5,B5             ; |156| 
RL50:      ; CALL OCCURS {__mpyd}            ; |156| 
           MVKL    .S2     __mpyd,B6         ; |156| 
           MVKH    .S2     __mpyd,B6         ; |156| 

           LDW     .D2T1   *+SP(120),A4      ; |156| 
||         MV      .L1     A4,A11            ; |156| 
||         CALL    .S2     B6                ; |156| 

           MV      .L2X    A12,B5            ; |156| 
           MV      .S1     A5,A10            ; |156| 
           ADDKPC  .S2     RL51,B3,0         ; |156| 
           MV      .L1     A12,A5            ; |156| 
           MV      .L2X    A4,B4             ; |156| 
RL51:      ; CALL OCCURS {__mpyd}            ; |156| 
           MVKL    .S1     __addd,A3         ; |156| 
           MVKH    .S1     __addd,A3         ; |156| 
           MV      .L2X    A4,B4             ; |156| 
           CALL    .S2X    A3                ; |156| 
           MV      .L2X    A5,B5             ; |156| 
           MV      .S1     A11,A4            ; |156| 
           ADDKPC  .S2     RL52,B3,1         ; |156| 
           MV      .L1     A10,A5            ; |156| 
RL52:      ; CALL OCCURS {__addd}            ; |156| 
           MVKL    .S1     __mpyd,A3         ; |156| 
           MVKH    .S1     __mpyd,A3         ; |156| 
           MV      .L2X    A15,B4            ; |156| 
           CALL    .S2X    A3                ; |156| 
           MV      .L1     A4,A11            ; |156| 
           ADDKPC  .S2     RL53,B3,0         ; |156| 
           MV      .S1     A15,A4            ; |156| 
           MV      .D1     A5,A10            ; |156| 

           MV      .L1     A13,A5            ; |156| 
||         MV      .L2X    A13,B5            ; |156| 

RL53:      ; CALL OCCURS {__mpyd}            ; |156| 
           MVKL    .S2     __addd,B6         ; |156| 
           MVKH    .S2     __addd,B6         ; |156| 
           CALL    .S2     B6                ; |156| 
           MV      .L2X    A4,B4             ; |156| 
           ADDKPC  .S2     RL54,B3,0         ; |156| 
           MV      .S1     A11,A4            ; |156| 
           MV      .L2X    A5,B5             ; |156| 
           MV      .L1     A10,A5            ; |156| 
RL54:      ; CALL OCCURS {__addd}            ; |156| 
           MVKL    .S1     _sqrt,A3          ; |157| 
           MVKH    .S1     _sqrt,A3          ; |157| 
           MV      .L1     A5,A10            ; |156| 
           CALL    .S2X    A3                ; |157| 
           ADDKPC  .S2     RL59,B3,3         ; |157| 
           MV      .S1     A4,A11            ; |156| 
RL59:      ; CALL OCCURS {_sqrt}             ; |157| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B6         ; |157| 
           MVKH    .S2     __mpyd,B6         ; |157| 
           CALL    .S2     B6                ; |157| 
           MV      .L2X    A11,B4            ; |157| 
           ADDKPC  .S2     RL60,B3,2         ; |157| 
           MV      .L2X    A10,B5            ; |157| 
RL60:      ; CALL OCCURS {__mpyd}            ; |157| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __divd,A3         ; |157| 
           MVKH    .S1     __divd,A3         ; |157| 
           MV      .L2X    A5,B5             ; |157| 
           CALL    .S2X    A3                ; |157| 
           ZERO    .L1     A5                ; |157| 
           ADDKPC  .S2     RL61,B3,0         ; |157| 
           MVKH    .S1     0x3ff00000,A5     ; |157| 
           MV      .L2X    A4,B4             ; |157| 
           ZERO    .L1     A4                ; |157| 
RL61:      ; CALL OCCURS {__divd}            ; |157| 
           MVKL    .S2     __mpyd,B6         ; |160| 

           LDDW    .D2T2   *+SP(72),B5:B4    ; |160| 
||         MVKH    .S2     __mpyd,B6         ; |160| 

           CALL    .S2     B6                ; |160| 
||         LDDW    .D2T2   *+SP(88),B11:B10  ; |160| 

           LDDW    .D2T2   *+SP(80),B13:B12  ; |160| 
           MV      .S1     A4,A11            ; |157| 
           ADDKPC  .S2     RL68,B3,0         ; |160| 
           MV      .L1X    B4,A4             ; |160| 

           MV      .S1     A5,A10            ; |157| 
||         MV      .L1X    B5,A5             ; |160| 

RL68:      ; CALL OCCURS {__mpyd}            ; |160| 
           MVKL    .S1     __mpyd,A3         ; |160| 
           MVKH    .S1     __mpyd,A3         ; |160| 
           STW     .D2T1   A4,*+SP(140)      ; |160| 
           CALL    .S2X    A3                ; |160| 
           MV      .L1X    B12,A4            ; |160| 
           STW     .D2T1   A5,*+SP(128)      ; |160| 
           ADDKPC  .S2     RL69,B3,0         ; |160| 
           MV      .L1X    B13,A5            ; |160| 

           MV      .L2     B13,B5            ; |160| 
||         MV      .D2     B12,B4            ; |160| 

RL69:      ; CALL OCCURS {__mpyd}            ; |160| 
           MVKL    .S2     __addd,B6         ; |160| 
           MVKH    .S2     __addd,B6         ; |160| 

           CALL    .S2     B6                ; |160| 
||         LDW     .D2T1   *+SP(128),A5      ; |160| 
||         MV      .L2X    A5,B5             ; |160| 

           LDW     .D2T1   *+SP(140),A4      ; |160| 
||         MV      .L2X    A4,B4             ; |160| 

           ADDKPC  .S2     RL70,B3,3         ; |160| 
RL70:      ; CALL OCCURS {__addd}            ; |160| 
           MVKL    .S2     __mpyd,B6         ; |160| 
           MVKH    .S2     __mpyd,B6         ; |160| 
           CALL    .S2     B6                ; |160| 
           MV      .L2X    A4,B13            ; |160| 
           MV      .S2     B10,B4            ; |160| 
           MV      .L1X    B10,A4            ; |160| 
           ADDKPC  .S2     RL71,B3,0         ; |160| 

           MV      .L2X    A5,B12            ; |160| 
||         MV      .D2     B11,B5            ; |160| 
||         MV      .L1X    B11,A5            ; |160| 

RL71:      ; CALL OCCURS {__mpyd}            ; |160| 
           MVKL    .S1     __addd,A3         ; |160| 
           MVKH    .S1     __addd,A3         ; |160| 
           MV      .L2X    A4,B4             ; |160| 
           CALL    .S2X    A3                ; |160| 
           MV      .L1X    B13,A4            ; |160| 
           ADDKPC  .S2     RL72,B3,1         ; |160| 
           MV      .L2X    A5,B5             ; |160| 
           MV      .L1X    B12,A5            ; |160| 
RL72:      ; CALL OCCURS {__addd}            ; |160| 
           MVKL    .S1     _sqrt,A3          ; |161| 
           MVKH    .S1     _sqrt,A3          ; |161| 
           MV      .L2X    A4,B11            ; |160| 
           CALL    .S2X    A3                ; |161| 
           MV      .L2X    A5,B10            ; |160| 
           ADDKPC  .S2     RL77,B3,3         ; |161| 
RL77:      ; CALL OCCURS {_sqrt}             ; |161| 
           MVKL    .S2     __mpyd,B6         ; |161| 
           MVKH    .S2     __mpyd,B6         ; |161| 
           CALL    .S2     B6                ; |161| 
           MV      .D2     B11,B4            ; |161| 
           MV      .L2     B10,B5            ; |161| 
           ADDKPC  .S2     RL78,B3,2         ; |161| 
RL78:      ; CALL OCCURS {__mpyd}            ; |161| 
           MVKL    .S2     __divd,B6         ; |161| 
           MVKH    .S2     __divd,B6         ; |161| 
           CALL    .S2     B6                ; |161| 
           MV      .L2X    A5,B5             ; |161| 
           ZERO    .L1     A5                ; |161| 
           MV      .L2X    A4,B4             ; |161| 
           MVKH    .S1     0x3ff00000,A5     ; |161| 

           ADDKPC  .S2     RL79,B3,0         ; |161| 
||         ZERO    .L1     A4                ; |161| 

RL79:      ; CALL OCCURS {__divd}            ; |161| 
           MVKL    .S2     __mpyd,B6         ; |164| 
           MVKH    .S2     __mpyd,B6         ; |164| 

           LDDW    .D2T1   *+SP(72),A7:A6    ; |164| 
||         CALL    .S2     B6                ; |164| 

           MV      .L2X    A5,B5             ; |161| 
           MV      .L2X    A4,B4             ; |161| 
           MV      .L2X    A4,B11            ; |161| 
           MV      .L2X    A5,B10            ; |161| 

           MV      .S1     A6,A4             ; |164| 
||         ADDKPC  .S2     RL89,B3,0         ; |164| 
||         MV      .L1     A7,A5             ; |164| 

RL89:      ; CALL OCCURS {__mpyd}            ; |164| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |164| 
           MVKH    .S1     __mpyd,A3         ; |164| 
           MV      .L2X    A5,B12            ; |164| 

           CALL    .S2X    A3                ; |164| 
||         LDW     .D2T1   *+SP(136),A5      ; |164| 

           LDW     .D2T1   *+SP(124),A4      ; |164| 
||         MV      .L2X    A4,B13            ; |164| 

           MV      .L2X    A11,B4            ; |164| 
           ADDKPC  .S2     RL90,B3,1         ; |164| 
           MV      .L2X    A10,B5            ; |164| 
RL90:      ; CALL OCCURS {__mpyd}            ; |164| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |164| 
           MVKH    .S1     __subd,A3         ; |164| 
           MV      .L2X    A4,B4             ; |164| 
           CALL    .S2X    A3                ; |164| 
           MV      .L1X    B13,A4            ; |164| 
           ADDKPC  .S2     RL91,B3,1         ; |164| 
           MV      .L2X    A5,B5             ; |164| 
           MV      .L1X    B12,A5            ; |164| 
RL91:      ; CALL OCCURS {__subd}            ; |164| 
           MVKL    .S1     __mpyd,A3         ; |164| 
           MVKH    .S1     __mpyd,A3         ; |164| 
           MVKL    .S2     0x441cc705,B5     ; |164| 
           CALL    .S2X    A3                ; |164| 
           MVKL    .S2     0x2ce1a836,B4     ; |164| 
           MVKH    .S2     0x441cc705,B5     ; |164| 
           MVKH    .S2     0x2ce1a836,B4     ; |164| 
           ADDKPC  .S2     RL92,B3,1         ; |164| 
RL92:      ; CALL OCCURS {__mpyd}            ; |164| 
           MVKL    .S1     __addd,A3         ; |164| 
           MVKH    .S1     __addd,A3         ; |164| 
           LDDW    .D1T1   *A14,A7:A6        ; |164| 
           CALL    .S2X    A3                ; |164| 
           MV      .L2X    A4,B4             ; |164| 
           MV      .L2X    A5,B5             ; |164| 
           ADDKPC  .S2     RL93,B3,0         ; |164| 
           MV      .L1     A7,A5             ; |164| 
           MV      .S1     A6,A4             ; |164| 
RL93:      ; CALL OCCURS {__addd}            ; |164| 
           MVKL    .S1     __mpyd,A3         ; |165| 
           MVKH    .S1     __mpyd,A3         ; |165| 
           LDDW    .D2T2   *+SP(80),B7:B6    ; |165| 
           CALL    .S2X    A3                ; |165| 
           STDW    .D1T1   A5:A4,*A14        ; |164| 
           MV      .D2     B11,B4            ; |165| 
           MV      .L2     B10,B5            ; |165| 
           MV      .L1X    B6,A4             ; |165| 

           MV      .L1X    B7,A5             ; |165| 
||         ADDKPC  .S2     RL103,B3,0        ; |165| 

RL103:     ; CALL OCCURS {__mpyd}            ; |165| 
           MVKL    .S1     __mpyd,A3         ; |165| 
           MVKH    .S1     __mpyd,A3         ; |165| 
           MV      .L2X    A4,B13            ; |165| 
           CALL    .S2X    A3                ; |165| 
           LDW     .D2T1   *+SP(120),A4      ; |165| 
           MV      .L2X    A10,B5            ; |165| 
           MV      .L2X    A11,B4            ; |165| 
           MV      .L2X    A5,B12            ; |165| 

           ADDKPC  .S2     RL104,B3,0        ; |165| 
||         MV      .L1     A12,A5            ; |165| 

RL104:     ; CALL OCCURS {__mpyd}            ; |165| 
           MVKL    .S1     __subd,A3         ; |165| 
           MVKH    .S1     __subd,A3         ; |165| 
           MV      .L2X    A4,B4             ; |165| 
           CALL    .S2X    A3                ; |165| 
           MV      .L1X    B13,A4            ; |165| 
           MV      .L2X    A5,B5             ; |165| 
           ADDKPC  .S2     RL105,B3,1        ; |165| 
           MV      .L1X    B12,A5            ; |165| 
RL105:     ; CALL OCCURS {__subd}            ; |165| 
           MVKL    .S2     __mpyd,B6         ; |165| 
           MVKH    .S2     __mpyd,B6         ; |165| 
           CALL    .S2     B6                ; |165| 
           MVKL    .S2     0x441cc705,B5     ; |165| 
           MVKL    .S2     0x2ce1a836,B4     ; |165| 
           MVKH    .S2     0x441cc705,B5     ; |165| 
           MVKH    .S2     0x2ce1a836,B4     ; |165| 
           ADDKPC  .S2     RL106,B3,0        ; |165| 
RL106:     ; CALL OCCURS {__mpyd}            ; |165| 
           MVKL    .S2     __addd,B6         ; |165| 
           MVKH    .S2     __addd,B6         ; |165| 

           CALL    .S2     B6                ; |165| 
||         LDDW    .D1T1   *+A14(8),A7:A6    ; |165| 

           MV      .L2X    A4,B4             ; |165| 
           ADDKPC  .S2     RL107,B3,1        ; |165| 
           MV      .L2X    A5,B5             ; |165| 

           MV      .L1     A7,A5             ; |165| 
||         MV      .S1     A6,A4             ; |165| 

RL107:     ; CALL OCCURS {__addd}            ; |165| 
           MVKL    .S1     __mpyd,A3         ; |166| 
           MVKH    .S1     __mpyd,A3         ; |166| 
           LDDW    .D2T1   *+SP(88),A7:A6    ; |166| 
           CALL    .S2X    A3                ; |166| 
           MV      .D2     B11,B4            ; |166| 
           MV      .L2     B10,B5            ; |166| 
           STDW    .D1T1   A5:A4,*+A14(8)    ; |165| 
           MV      .S1     A6,A4             ; |166| 

           MV      .L1     A7,A5             ; |166| 
||         ADDKPC  .S2     RL117,B3,0        ; |166| 

RL117:     ; CALL OCCURS {__mpyd}            ; |166| 
           MVKL    .S1     __mpyd,A3         ; |166| 
           MVKH    .S1     __mpyd,A3         ; |166| 
           MV      .L2X    A10,B5            ; |166| 
           CALL    .S2X    A3                ; |166| 
           MV      .L2X    A11,B4            ; |166| 
           MV      .D1     A5,A12            ; |166| 
           MV      .L2X    A4,B10            ; |166| 
           MV      .L1     A15,A4            ; |166| 

           ADDKPC  .S2     RL118,B3,0        ; |166| 
||         MV      .S1     A13,A5            ; |166| 

RL118:     ; CALL OCCURS {__mpyd}            ; |166| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __subd,B6         ; |166| 
           MVKH    .S2     __subd,B6         ; |166| 
           CALL    .S2     B6                ; |166| 
           MV      .L2X    A4,B4             ; |166| 
           ADDKPC  .S2     RL119,B3,0        ; |166| 
           MV      .S1X    B10,A4            ; |166| 
           MV      .L2X    A5,B5             ; |166| 
           MV      .L1     A12,A5            ; |166| 
RL119:     ; CALL OCCURS {__subd}            ; |166| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |166| 
           MVKH    .S1     __mpyd,A3         ; |166| 
           MVKL    .S2     0x441cc705,B5     ; |166| 
           CALL    .S2X    A3                ; |166| 
           MVKL    .S2     0x2ce1a836,B4     ; |166| 
           MVKH    .S2     0x441cc705,B5     ; |166| 
           MVKH    .S2     0x2ce1a836,B4     ; |166| 
           ADDKPC  .S2     RL120,B3,1        ; |166| 
RL120:     ; CALL OCCURS {__mpyd}            ; |166| 
           MVKL    .S1     __addd,A3         ; |166| 
           MVKH    .S1     __addd,A3         ; |166| 
           LDDW    .D1T1   *+A14(16),A7:A6   ; |166| 
           CALL    .S2X    A3                ; |166| 
           MV      .L2X    A4,B4             ; |166| 
           ADDKPC  .S2     RL121,B3,0        ; |166| 
           MV      .L2X    A5,B5             ; |166| 
           MV      .L1     A7,A5             ; |166| 
           MV      .S1     A6,A4             ; |166| 
RL121:     ; CALL OCCURS {__addd}            ; |166| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(104),B31     ; |177| 

           MVKL    .S1     __mpyd,A13        ; |178| 
||         MVKL    .S2     __mpyd,B4         ; |177| 

           MVKL    .S1     __mpyd,A11        ; |181| 
||         MVKL    .S2     __mpyd,B12        ; |178| 

           MVKH    .S2     __mpyd,B4         ; |177| 
||         MVKL    .S1     __addd,A12        ; |178| 

           STW     .D2T2   B4,*+SP(128)      ; |177| 
||         MVKL    .S1     __mpyd,A10        ; |182| 
||         STDW    .D1T1   A5:A4,*+A14(16)   ; |166| 
||         MVKL    .S2     __addd,B13        ; |177| 

;** --------------------------------------------------------------------------*
L8:    

           MVKL    .S1     _MoonPos_GeoCenterEclFrame,A3 ; |173| 
||         AND     .L2     2,B31,B0          ; |170| 
||         LDW     .D2T1   *+SP(108),A5      ; |173| 
||         MVKL    .S2     __mpyd,B11        ; |181| 

           MVKH    .S1     _MoonPos_GeoCenterEclFrame,A3 ; |173| 
||         LDW     .D2T1   *+SP(112),A4      ; |173| 
||         MVKL    .S2     __addd,B10        ; |181| 

   [!B0]   B       .S2     L9                ; |170| 
||         MVKL    .S1     __mpyd,A15        ; |177| 
||         ADDAD   .D2     SP,9,B4           ; |173| 

   [ B0]   CALL    .S2X    A3                ; |173| 
||         MVKH    .S1     __mpyd,A15        ; |177| 
|| [!B0]   LDW     .D2T2   *+SP(96),B4

           MVKH    .S2     __mpyd,B11        ; |181| 
||         MVKH    .S1     __mpyd,A10        ; |182| 

           MVKH    .S2     __addd,B10        ; |181| 
||         MVKH    .S1     __mpyd,A11        ; |181| 

           MVKH    .S2     __addd,B13        ; |177| 
||         MVKH    .S1     __mpyd,A13        ; |178| 

           MVKH    .S2     __mpyd,B12        ; |178| 
||         MVKH    .S1     __addd,A12        ; |178| 

           ; BRANCHCC OCCURS {L9}            ; |170| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     RL122,B3,0        ; |173| 
RL122:     ; CALL OCCURS {_MoonPos_GeoCenterEclFrame}  ; |173| 
;** --------------------------------------------------------------------------*
           LDDW    .D2T2   *+SP(8),B7:B6     ; |177| 
           NOP             1
           LDDW    .D2T2   *+SP(80),B5:B4    ; |177| 
           NOP             2
           MV      .L1X    B6,A4             ; |177| 

           LDDW    .D2T2   *+SP(72),B7:B6    ; |176| 
||         MV      .L1X    B7,A5             ; |177| 

           NOP             2
           STW     .D2T2   B4,*+SP(108)      ; |177| 
           STW     .D2T2   B5,*+SP(104)      ; |177| 
           STW     .D2T2   B7,*+SP(140)      ; |176| 
           STW     .D2T2   B6,*+SP(136)      ; |176| 
           LDDW    .D2T2   *+SP(88),B7:B6    ; |177| 
           NOP             4
           STW     .D2T2   B6,*+SP(120)      ; |177| 
           LDW     .D2T2   *+SP(128),B6      ; |177| 
           NOP             3
           STW     .D2T2   B7,*+SP(124)      ; |177| 
           CALL    .S2     B6                ; |177| 
           ADDKPC  .S2     RL125,B3,4        ; |177| 
RL125:     ; CALL OCCURS {__mpyd}            ; |177| 
           LDDW    .D2T2   *+SP(16),B7:B6    ; |177| 

           CALL    .S2X    A15               ; |177| 
||         LDW     .D2T2   *+SP(124),B5      ; |177| 

           LDW     .D2T2   *+SP(120),B4      ; |177| 
           STW     .D2T1   A4,*+SP(112)      ; |177| 
           ADDKPC  .S2     RL126,B3,0        ; |177| 
           MV      .L1X    B6,A4             ; |177| 

           STW     .D2T1   A5,*+SP(128)      ; |177| 
||         MV      .L1X    B7,A5             ; |177| 

RL126:     ; CALL OCCURS {__mpyd}            ; |177| 
           MV      .L2X    A4,B4             ; |177| 

           LDW     .D2T1   *+SP(128),A5      ; |177| 
||         MV      .L2X    A5,B5             ; |177| 
||         CALL    .S2     B13               ; |177| 

           LDW     .D2T1   *+SP(112),A4      ; |177| 
           ADDKPC  .S2     RL127,B3,3        ; |177| 
RL127:     ; CALL OCCURS {__addd}            ; |177| 
           LDDW    .D2T2   *+SP(24),B7:B6    ; |178| 

           CALL    .S2X    A13               ; |178| 
||         LDW     .D2T2   *+SP(104),B5      ; |178| 

           LDW     .D2T2   *+SP(108),B4      ; |178| 
           STW     .D2T1   A4,*+SP(112)      ; |177| 
           ADDKPC  .S2     RL130,B3,0        ; |178| 
           MV      .L1X    B6,A4             ; |178| 

           MV      .S1     A5,A15            ; |177| 
||         MV      .L1X    B7,A5             ; |178| 

RL130:     ; CALL OCCURS {__mpyd}            ; |178| 
           LDDW    .D2T2   *+SP(32),B7:B6    ; |178| 

           CALL    .S2     B12               ; |178| 
||         LDW     .D2T2   *+SP(124),B5      ; |178| 

           LDW     .D2T2   *+SP(120),B4      ; |178| 
           MV      .S1     A5,A13            ; |178| 
           ADDKPC  .S2     RL131,B3,0        ; |178| 
           MV      .L1X    B7,A5             ; |178| 

           MV      .L2X    A4,B13            ; |178| 
||         MV      .L1X    B6,A4             ; |178| 

RL131:     ; CALL OCCURS {__mpyd}            ; |178| 
           CALL    .S2X    A12               ; |178| 
           MV      .L2X    A5,B5             ; |178| 
           ADDKPC  .S2     RL132,B3,0        ; |178| 
           MV      .S1     A13,A5            ; |178| 
           MV      .L2X    A4,B4             ; |178| 
           MV      .L1X    B13,A4            ; |178| 
RL132:     ; CALL OCCURS {__addd}            ; |178| 
           LDDW    .D2T2   *+SP(40),B7:B6    ; |181| 

           CALL    .S2     B11               ; |181| 
||         LDW     .D2T2   *+SP(140),B5      ; |181| 

           LDW     .D2T2   *+SP(136),B4      ; |181| 
           MV      .L2X    A4,B13            ; |178| 
           ADDKPC  .S2     RL135,B3,0        ; |181| 
           MV      .L1X    B6,A4             ; |181| 

           MV      .L2X    A5,B12            ; |178| 
||         MV      .L1X    B7,A5             ; |181| 

RL135:     ; CALL OCCURS {__mpyd}            ; |181| 
           LDDW    .D2T2   *+SP(48),B7:B6    ; |181| 
           CALL    .S2X    A11               ; |181| 
           LDW     .D2T2   *+SP(112),B4      ; |181| 
           MV      .S1     A4,A13            ; |181| 
           ADDKPC  .S2     RL136,B3,0        ; |181| 
           MV      .L1X    B6,A4             ; |181| 

           MV      .S1     A5,A12            ; |181| 
||         MV      .L2X    A15,B5            ; |181| 
||         MV      .L1X    B7,A5             ; |181| 

RL136:     ; CALL OCCURS {__mpyd}            ; |181| 
           CALL    .S2     B10               ; |181| 
           MV      .L2X    A4,B4             ; |181| 
           ADDKPC  .S2     RL137,B3,0        ; |181| 
           MV      .S1     A13,A4            ; |181| 
           MV      .L2X    A5,B5             ; |181| 
           MV      .L1     A12,A5            ; |181| 
RL137:     ; CALL OCCURS {__addd}            ; |181| 
           LDDW    .D2T2   *+SP(56),B7:B6    ; |182| 

           CALL    .S2X    A10               ; |182| 
||         LDW     .D2T2   *+SP(140),B5      ; |182| 

           LDW     .D2T2   *+SP(136),B4      ; |182| 
           MV      .S1     A5,A13            ; |181| 
           MV      .D1     A4,A12            ; |181| 
           MV      .L1X    B6,A4             ; |182| 

           ADDKPC  .S2     RL140,B3,0        ; |182| 
||         MV      .L1X    B7,A5             ; |182| 
||         STDW    .D2T1   A13:A12,*+SP(72)  ; |182| 

RL140:     ; CALL OCCURS {__mpyd}            ; |182| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |182| 
           MVKH    .S1     __mpyd,A3         ; |182| 
           LDDW    .D2T2   *+SP(64),B7:B6    ; |182| 
           CALL    .S2X    A3                ; |182| 
           LDW     .D2T2   *+SP(112),B4      ; |182| 
           MV      .S1     A4,A11            ; |182| 
           ADDKPC  .S2     RL141,B3,0        ; |182| 
           MV      .L1X    B6,A4             ; |182| 

           MV      .L2X    A15,B5            ; |182| 
||         MV      .S1     A5,A10            ; |182| 
||         MV      .L1X    B7,A5             ; |182| 

RL141:     ; CALL OCCURS {__mpyd}            ; |182| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |182| 
           MVKH    .S1     __addd,A3         ; |182| 
           MV      .L2X    A4,B4             ; |182| 
           CALL    .S2X    A3                ; |182| 
           ADDKPC  .S2     RL142,B3,1        ; |182| 
           MV      .S1     A11,A4            ; |182| 
           MV      .L2X    A5,B5             ; |182| 
           MV      .L1     A10,A5            ; |182| 
RL142:     ; CALL OCCURS {__addd}            ; |182| 

           LDW     .D2T2   *+SP(100),B12     ; |182| 
||         MV      .L2     B12,B11           ; |183| 

           MVKL    .S2     __subd,B6         ; |186| 
           MVKH    .S2     __subd,B6         ; |186| 
           MV      .L2     B13,B10           ; |183| 
           CALL    .S2     B6                ; |186| 
           LDDW    .D2T2   *B12,B5:B4        ; |186| 
           MV      .S1     A4,A10            ; |182| 
           MV      .L1     A5,A11            ; |182| 
           STDW    .D2T2   B11:B10,*+SP(88)  ; |186| 

           MV      .S1     A12,A4            ; |186| 
||         MV      .L1     A13,A5            ; |186| 
||         STDW    .D2T1   A11:A10,*+SP(80)  ; |183| 
||         ADDKPC  .S2     RL143,B3,0        ; |186| 

RL143:     ; CALL OCCURS {__subd}            ; |186| 
           MVKL    .S2     __subd,B6         ; |186| 
           MVKH    .S2     __subd,B6         ; |186| 
           CALL    .S2     B6                ; |186| 
           LDDW    .D2T2   *+B12(8),B5:B4    ; |186| 
           STW     .D2T1   A5,*+SP(104)      ; |186| 
           ADDKPC  .S2     RL144,B3,0        ; |186| 
           STW     .D2T1   A4,*+SP(108)      ; |186| 

           MV      .S1     A10,A4            ; |186| 
||         MV      .L1     A11,A5            ; |186| 

RL144:     ; CALL OCCURS {__subd}            ; |186| 
           MVKL    .S2     __subd,B6         ; |187| 
           MVKH    .S2     __subd,B6         ; |187| 

           MV      .L2     B12,B4            ; |186| 
||         CALL    .S2     B6                ; |187| 

           LDDW    .D2T2   *+B4(16),B5:B4    ; |187| 
           STW     .D2T1   A4,*+SP(112)      ; |186| 
           MV      .L1X    B10,A4            ; |187| 
           MV      .S1     A5,A12            ; |186| 

           ADDKPC  .S2     RL145,B3,0        ; |187| 
||         MV      .L1X    B11,A5            ; |187| 

RL145:     ; CALL OCCURS {__subd}            ; |187| 
           MVKL    .S2     __mpyd,B6         ; |187| 

           LDW     .D2T1   *+SP(108),A4      ; |187| 
||         MVKH    .S2     __mpyd,B6         ; |187| 
||         MV      .L1     A4,A15            ; |187| 

           LDW     .D2T1   *+SP(104),A5      ; |187| 
||         MV      .L1     A5,A13            ; |187| 
||         CALL    .S2     B6                ; |187| 

           ADDKPC  .S2     RL152,B3,2        ; |187| 
           MV      .L2X    A4,B4             ; |187| 
           MV      .L2X    A5,B5             ; |187| 
RL152:     ; CALL OCCURS {__mpyd}            ; |187| 
           MVKL    .S2     __mpyd,B6         ; |187| 
           MVKH    .S2     __mpyd,B6         ; |187| 

           LDW     .D2T1   *+SP(112),A4      ; |187| 
||         MV      .L1     A4,A11            ; |187| 
||         CALL    .S2     B6                ; |187| 

           MV      .L2X    A12,B5            ; |187| 
           MV      .S1     A5,A10            ; |187| 
           ADDKPC  .S2     RL153,B3,0        ; |187| 
           MV      .L1     A12,A5            ; |187| 
           MV      .L2X    A4,B4             ; |187| 
RL153:     ; CALL OCCURS {__mpyd}            ; |187| 
           MVKL    .S1     __addd,A3         ; |187| 
           MVKH    .S1     __addd,A3         ; |187| 
           MV      .L2X    A4,B4             ; |187| 
           CALL    .S2X    A3                ; |187| 
           MV      .L2X    A5,B5             ; |187| 
           MV      .S1     A11,A4            ; |187| 
           ADDKPC  .S2     RL154,B3,1        ; |187| 
           MV      .L1     A10,A5            ; |187| 
RL154:     ; CALL OCCURS {__addd}            ; |187| 
           MVKL    .S1     __mpyd,A3         ; |187| 
           MVKH    .S1     __mpyd,A3         ; |187| 
           MV      .L2X    A15,B4            ; |187| 
           CALL    .S2X    A3                ; |187| 
           MV      .L1     A4,A11            ; |187| 
           ADDKPC  .S2     RL155,B3,0        ; |187| 
           MV      .S1     A15,A4            ; |187| 
           MV      .D1     A5,A10            ; |187| 

           MV      .L1     A13,A5            ; |187| 
||         MV      .L2X    A13,B5            ; |187| 

RL155:     ; CALL OCCURS {__mpyd}            ; |187| 
           MVKL    .S2     __addd,B6         ; |187| 
           MVKH    .S2     __addd,B6         ; |187| 
           CALL    .S2     B6                ; |187| 
           MV      .L2X    A4,B4             ; |187| 
           ADDKPC  .S2     RL156,B3,0        ; |187| 
           MV      .S1     A11,A4            ; |187| 
           MV      .L2X    A5,B5             ; |187| 
           MV      .L1     A10,A5            ; |187| 
RL156:     ; CALL OCCURS {__addd}            ; |187| 
           MVKL    .S1     _sqrt,A3          ; |188| 
           MVKH    .S1     _sqrt,A3          ; |188| 
           MV      .L1     A5,A10            ; |187| 
           CALL    .S2X    A3                ; |188| 
           ADDKPC  .S2     RL161,B3,3        ; |188| 
           MV      .S1     A4,A11            ; |187| 
RL161:     ; CALL OCCURS {_sqrt}             ; |188| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B6         ; |188| 
           MVKH    .S2     __mpyd,B6         ; |188| 
           CALL    .S2     B6                ; |188| 
           MV      .L2X    A11,B4            ; |188| 
           ADDKPC  .S2     RL162,B3,2        ; |188| 
           MV      .L2X    A10,B5            ; |188| 
RL162:     ; CALL OCCURS {__mpyd}            ; |188| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __divd,A3         ; |188| 
           MVKH    .S1     __divd,A3         ; |188| 
           MV      .L2X    A5,B5             ; |188| 
           CALL    .S2X    A3                ; |188| 
           ZERO    .L1     A5                ; |188| 
           ADDKPC  .S2     RL163,B3,0        ; |188| 
           MVKH    .S1     0x3ff00000,A5     ; |188| 
           MV      .L2X    A4,B4             ; |188| 
           ZERO    .L1     A4                ; |188| 
RL163:     ; CALL OCCURS {__divd}            ; |188| 
           MVKL    .S2     __mpyd,B6         ; |191| 

           LDDW    .D2T2   *+SP(72),B5:B4    ; |191| 
||         MVKH    .S2     __mpyd,B6         ; |191| 

           CALL    .S2     B6                ; |191| 
||         LDDW    .D2T2   *+SP(88),B11:B10  ; |191| 

           LDDW    .D2T2   *+SP(80),B13:B12  ; |191| 
           MV      .S1     A4,A11            ; |188| 
           ADDKPC  .S2     RL170,B3,0        ; |191| 
           MV      .L1X    B4,A4             ; |191| 

           MV      .S1     A5,A10            ; |188| 
||         MV      .L1X    B5,A5             ; |191| 

RL170:     ; CALL OCCURS {__mpyd}            ; |191| 
           MVKL    .S1     __mpyd,A3         ; |191| 
           MVKH    .S1     __mpyd,A3         ; |191| 
           STW     .D2T1   A4,*+SP(100)      ; |191| 
           CALL    .S2X    A3                ; |191| 
           MV      .L1X    B12,A4            ; |191| 
           STW     .D2T1   A5,*+SP(120)      ; |191| 
           ADDKPC  .S2     RL171,B3,0        ; |191| 
           MV      .L1X    B13,A5            ; |191| 

           MV      .L2     B13,B5            ; |191| 
||         MV      .D2     B12,B4            ; |191| 

RL171:     ; CALL OCCURS {__mpyd}            ; |191| 
           MVKL    .S2     __addd,B6         ; |191| 
           MVKH    .S2     __addd,B6         ; |191| 

           CALL    .S2     B6                ; |191| 
||         LDW     .D2T1   *+SP(120),A5      ; |191| 
||         MV      .L2X    A5,B5             ; |191| 

           LDW     .D2T1   *+SP(100),A4      ; |191| 
||         MV      .L2X    A4,B4             ; |191| 

           ADDKPC  .S2     RL172,B3,3        ; |191| 
RL172:     ; CALL OCCURS {__addd}            ; |191| 
           MVKL    .S2     __mpyd,B6         ; |191| 
           MVKH    .S2     __mpyd,B6         ; |191| 
           CALL    .S2     B6                ; |191| 
           MV      .L2X    A4,B13            ; |191| 
           MV      .S2     B10,B4            ; |191| 
           MV      .L1X    B10,A4            ; |191| 
           ADDKPC  .S2     RL173,B3,0        ; |191| 

           MV      .L2X    A5,B12            ; |191| 
||         MV      .D2     B11,B5            ; |191| 
||         MV      .L1X    B11,A5            ; |191| 

RL173:     ; CALL OCCURS {__mpyd}            ; |191| 
           MVKL    .S1     __addd,A3         ; |191| 
           MVKH    .S1     __addd,A3         ; |191| 
           MV      .L2X    A4,B4             ; |191| 
           CALL    .S2X    A3                ; |191| 
           MV      .L1X    B13,A4            ; |191| 
           ADDKPC  .S2     RL174,B3,1        ; |191| 
           MV      .L2X    A5,B5             ; |191| 
           MV      .L1X    B12,A5            ; |191| 
RL174:     ; CALL OCCURS {__addd}            ; |191| 
           MVKL    .S1     _sqrt,A3          ; |192| 
           MVKH    .S1     _sqrt,A3          ; |192| 
           MV      .L2X    A4,B11            ; |191| 
           CALL    .S2X    A3                ; |192| 
           MV      .L2X    A5,B10            ; |191| 
           ADDKPC  .S2     RL179,B3,3        ; |192| 
RL179:     ; CALL OCCURS {_sqrt}             ; |192| 
           MVKL    .S2     __mpyd,B6         ; |192| 
           MVKH    .S2     __mpyd,B6         ; |192| 
           CALL    .S2     B6                ; |192| 
           MV      .D2     B11,B4            ; |192| 
           MV      .L2     B10,B5            ; |192| 
           ADDKPC  .S2     RL180,B3,2        ; |192| 
RL180:     ; CALL OCCURS {__mpyd}            ; |192| 
           MVKL    .S2     __divd,B6         ; |192| 
           MVKH    .S2     __divd,B6         ; |192| 
           CALL    .S2     B6                ; |192| 
           MV      .L2X    A5,B5             ; |192| 
           ZERO    .L1     A5                ; |192| 
           MV      .L2X    A4,B4             ; |192| 
           MVKH    .S1     0x3ff00000,A5     ; |192| 

           ADDKPC  .S2     RL181,B3,0        ; |192| 
||         ZERO    .L1     A4                ; |192| 

RL181:     ; CALL OCCURS {__divd}            ; |192| 
           MVKL    .S2     __mpyd,B6         ; |195| 
           MVKH    .S2     __mpyd,B6         ; |195| 

           LDDW    .D2T1   *+SP(72),A7:A6    ; |195| 
||         CALL    .S2     B6                ; |195| 

           MV      .L2X    A5,B5             ; |192| 
           MV      .L2X    A4,B4             ; |192| 
           MV      .L2X    A4,B11            ; |192| 
           MV      .L2X    A5,B10            ; |192| 

           MV      .S1     A6,A4             ; |195| 
||         ADDKPC  .S2     RL191,B3,0        ; |195| 
||         MV      .L1     A7,A5             ; |195| 

RL191:     ; CALL OCCURS {__mpyd}            ; |195| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |195| 
           MVKH    .S1     __mpyd,A3         ; |195| 
           MV      .L2X    A5,B12            ; |195| 

           CALL    .S2X    A3                ; |195| 
||         LDW     .D2T1   *+SP(104),A5      ; |195| 

           LDW     .D2T1   *+SP(108),A4      ; |195| 
||         MV      .L2X    A4,B13            ; |195| 

           MV      .L2X    A11,B4            ; |195| 
           ADDKPC  .S2     RL192,B3,1        ; |195| 
           MV      .L2X    A10,B5            ; |195| 
RL192:     ; CALL OCCURS {__mpyd}            ; |195| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |195| 
           MVKH    .S1     __subd,A3         ; |195| 
           MV      .L2X    A4,B4             ; |195| 
           CALL    .S2X    A3                ; |195| 
           MV      .L1X    B13,A4            ; |195| 
           ADDKPC  .S2     RL193,B3,1        ; |195| 
           MV      .L2X    A5,B5             ; |195| 
           MV      .L1X    B12,A5            ; |195| 
RL193:     ; CALL OCCURS {__subd}            ; |195| 
           MVKL    .S1     __mpyd,A3         ; |195| 
           MVKH    .S1     __mpyd,A3         ; |195| 
           MVKL    .S2     0x4291d615,B5     ; |195| 
           CALL    .S2X    A3                ; |195| 
           MVKL    .S2     0x99b1358,B4      ; |195| 
           MVKH    .S2     0x4291d615,B5     ; |195| 
           MVKH    .S2     0x99b1358,B4      ; |195| 
           ADDKPC  .S2     RL194,B3,1        ; |195| 
RL194:     ; CALL OCCURS {__mpyd}            ; |195| 
           MVKL    .S1     __addd,A3         ; |195| 
           MVKH    .S1     __addd,A3         ; |195| 
           LDDW    .D1T1   *A14,A7:A6        ; |195| 
           CALL    .S2X    A3                ; |195| 
           MV      .L2X    A4,B4             ; |195| 
           MV      .L2X    A5,B5             ; |195| 
           ADDKPC  .S2     RL195,B3,0        ; |195| 
           MV      .L1     A7,A5             ; |195| 
           MV      .S1     A6,A4             ; |195| 
RL195:     ; CALL OCCURS {__addd}            ; |195| 
           MVKL    .S1     __mpyd,A3         ; |196| 
           MVKH    .S1     __mpyd,A3         ; |196| 
           LDDW    .D2T2   *+SP(80),B7:B6    ; |196| 
           CALL    .S2X    A3                ; |196| 
           STDW    .D1T1   A5:A4,*A14        ; |195| 
           MV      .D2     B11,B4            ; |196| 
           MV      .L2     B10,B5            ; |196| 
           MV      .L1X    B6,A4             ; |196| 

           MV      .L1X    B7,A5             ; |196| 
||         ADDKPC  .S2     RL205,B3,0        ; |196| 

RL205:     ; CALL OCCURS {__mpyd}            ; |196| 
           MVKL    .S1     __mpyd,A3         ; |196| 
           MVKH    .S1     __mpyd,A3         ; |196| 
           MV      .L2X    A4,B13            ; |196| 
           CALL    .S2X    A3                ; |196| 
           LDW     .D2T1   *+SP(112),A4      ; |196| 
           MV      .L2X    A10,B5            ; |196| 
           MV      .L2X    A11,B4            ; |196| 
           MV      .L2X    A5,B12            ; |196| 

           ADDKPC  .S2     RL206,B3,0        ; |196| 
||         MV      .L1     A12,A5            ; |196| 

RL206:     ; CALL OCCURS {__mpyd}            ; |196| 
           MVKL    .S1     __subd,A3         ; |196| 
           MVKH    .S1     __subd,A3         ; |196| 
           MV      .L2X    A4,B4             ; |196| 
           CALL    .S2X    A3                ; |196| 
           MV      .L1X    B13,A4            ; |196| 
           MV      .L2X    A5,B5             ; |196| 
           ADDKPC  .S2     RL207,B3,1        ; |196| 
           MV      .L1X    B12,A5            ; |196| 
RL207:     ; CALL OCCURS {__subd}            ; |196| 
           MVKL    .S2     __mpyd,B6         ; |196| 
           MVKH    .S2     __mpyd,B6         ; |196| 
           CALL    .S2     B6                ; |196| 
           MVKL    .S2     0x4291d615,B5     ; |196| 
           MVKL    .S2     0x99b1358,B4      ; |196| 
           MVKH    .S2     0x4291d615,B5     ; |196| 
           MVKH    .S2     0x99b1358,B4      ; |196| 
           ADDKPC  .S2     RL208,B3,0        ; |196| 
RL208:     ; CALL OCCURS {__mpyd}            ; |196| 
           MVKL    .S2     __addd,B6         ; |196| 
           MVKH    .S2     __addd,B6         ; |196| 

           CALL    .S2     B6                ; |196| 
||         LDDW    .D1T1   *+A14(8),A7:A6    ; |196| 

           MV      .L2X    A4,B4             ; |196| 
           ADDKPC  .S2     RL209,B3,1        ; |196| 
           MV      .L2X    A5,B5             ; |196| 

           MV      .L1     A7,A5             ; |196| 
||         MV      .S1     A6,A4             ; |196| 

RL209:     ; CALL OCCURS {__addd}            ; |196| 
           MVKL    .S1     __mpyd,A3         ; |197| 
           MVKH    .S1     __mpyd,A3         ; |197| 
           LDDW    .D2T1   *+SP(88),A7:A6    ; |197| 
           CALL    .S2X    A3                ; |197| 
           MV      .D2     B11,B4            ; |197| 
           MV      .L2     B10,B5            ; |197| 
           STDW    .D1T1   A5:A4,*+A14(8)    ; |196| 
           MV      .S1     A6,A4             ; |197| 

           MV      .L1     A7,A5             ; |197| 
||         ADDKPC  .S2     RL219,B3,0        ; |197| 

RL219:     ; CALL OCCURS {__mpyd}            ; |197| 
           MVKL    .S1     __mpyd,A3         ; |197| 
           MVKH    .S1     __mpyd,A3         ; |197| 
           MV      .L2X    A10,B5            ; |197| 
           CALL    .S2X    A3                ; |197| 
           MV      .L2X    A11,B4            ; |197| 
           MV      .D1     A5,A12            ; |197| 
           MV      .L2X    A4,B10            ; |197| 
           MV      .L1     A15,A4            ; |197| 

           ADDKPC  .S2     RL220,B3,0        ; |197| 
||         MV      .S1     A13,A5            ; |197| 

RL220:     ; CALL OCCURS {__mpyd}            ; |197| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __subd,B6         ; |197| 
           MVKH    .S2     __subd,B6         ; |197| 
           CALL    .S2     B6                ; |197| 
           MV      .L2X    A4,B4             ; |197| 
           ADDKPC  .S2     RL221,B3,0        ; |197| 
           MV      .S1X    B10,A4            ; |197| 
           MV      .L2X    A5,B5             ; |197| 
           MV      .L1     A12,A5            ; |197| 
RL221:     ; CALL OCCURS {__subd}            ; |197| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |197| 
           MVKH    .S1     __mpyd,A3         ; |197| 
           MVKL    .S2     0x4291d615,B5     ; |197| 
           CALL    .S2X    A3                ; |197| 
           MVKL    .S2     0x99b1358,B4      ; |197| 
           MVKH    .S2     0x4291d615,B5     ; |197| 
           MVKH    .S2     0x99b1358,B4      ; |197| 
           ADDKPC  .S2     RL222,B3,1        ; |197| 
RL222:     ; CALL OCCURS {__mpyd}            ; |197| 
           MVKL    .S1     __addd,A3         ; |197| 
           MVKH    .S1     __addd,A3         ; |197| 
           LDDW    .D1T1   *+A14(16),A7:A6   ; |197| 
           CALL    .S2X    A3                ; |197| 
           MV      .L2X    A4,B4             ; |197| 
           ADDKPC  .S2     RL223,B3,0        ; |197| 
           MV      .L2X    A5,B5             ; |197| 
           MV      .L1     A7,A5             ; |197| 
           MV      .S1     A6,A4             ; |197| 
RL223:     ; CALL OCCURS {__addd}            ; |197| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(96),B4
           NOP             3
           STDW    .D1T1   A5:A4,*+A14(16)   ; |197| 
;** --------------------------------------------------------------------------*
L9:    

           MV      .L1X    B4,A0
||         MVK     .S1     48,A4             ; |38| 
||         SUB     .L2X    B4,A14,B5         ; |38| 
||         ADD     .D2     B4,24,B6          ; |37| 

   [!A0]   BNOP    .S1     L14,1             ; |201| 
||         SUB     .L1X    A14,B4,A3         ; |38| 
||         CMPLT   .L2     B5,0,B5           ; |38| 

           CMPLT   .L1     A3,A4,A3          ; |38| 
           NOP             1
           AND     .L2X    B5,A3,B5
           XOR     .L2     1,B5,B0
           ; BRANCHCC OCCURS {L14}           ; |201| 
;** --------------------------------------------------------------------------*

   [ B0]   BNOP    .S1     L13,3             ; |38| 
||         MVK     .S2     0x18,B7
||         ADD     .D2     B4,24,B4
||         MV      .L2X    A14,B5
|| [ B0]   LDB     .D1T1   *A14,A3           ; |40| 

   [!B0]   SUB     .L1X    B7,1,A0
   [ B0]   STB     .D2T1   A3,*B6            ; |40| 
           ; BRANCHCC OCCURS {L13}           ; |38| 
;** --------------------------------------------------------------------------*
           MVC     .S2     CSR,B7
           AND     .L2     -2,B7,B6
           MVC     .S2     B6,CSR            ; interrupts off
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 38
;*      Loop opening brace source line   : 39
;*      Loop closing brace source line   : 41
;*      Known Minimum Trip Count         : 24                    
;*      Known Maximum Trip Count         : 24                    
;*      Known Max Trip Count Factor      : 24
;*      Loop Carried Dependency Bound(^) : 6
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1        0     
;*      .D units                     0        2*    
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             0        2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 1 iterations in parallel
;*      Done
;*
;*      Collapsed epilog stages     : 0
;*      Collapsed prolog stages     : 0
;*
;*      Minimum safe trip count     : 1
;*----------------------------------------------------------------------------*
L10:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L11:    ; PIPED LOOP KERNEL
DW$L$_SunMoonData_Gravitation$46$B:
	.dwpsn	"SunMoonGravitation.c",39,0

   [ A0]   BDEC    .S1     L11,A0            ; |38| <0,0> 
||         LDB     .D2T2   *B5++,B6          ; |40| <0,0>  ^ 

           NOP             4
	.dwpsn	"SunMoonGravitation.c",41,0
           STB     .D2T2   B6,*B4++          ; |40| <0,5>  ^ 
DW$L$_SunMoonData_Gravitation$46$E:
;** --------------------------------------------------------------------------*
L12:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(172),B3      ; |205| 
           LDW     .D2T1   *+SP(192),A15     ; |205| 
           LDDW    .D2T1   *+SP(160),A13:A12 ; |205| 
           LDDW    .D2T2   *+SP(184),B13:B12 ; |205| 
           LDW     .D2T1   *+SP(168),A14     ; |205| 

           RET     .S2     B3                ; |205| 
||         LDDW    .D2T1   *+SP(152),A11:A10 ; |205| 

           LDDW    .D2T2   *+SP(176),B11:B10 ; |205| 
           NOP             1
           MVC     .S2     B7,CSR            ; interrupts on
           NOP             1
           ADDK    .S2     192,SP            ; |205| 
           ; BRANCH OCCURS {B3}              ; |205| 
;** --------------------------------------------------------------------------*
L13:    
           LDB     .D1T1   *+A14(1),A3       ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(1)        ; |40| 
           LDB     .D1T1   *+A14(2),A3       ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(2)        ; |40| 
           LDB     .D1T1   *+A14(3),A3       ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(3)        ; |40| 
           LDB     .D1T2   *+A14(4),B4       ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(4)        ; |40| 
           LDB     .D1T2   *+A14(5),B4       ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(5)        ; |40| 
           LDB     .D1T2   *+A14(6),B4       ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(6)        ; |40| 
           LDB     .D1T2   *+A14(7),B4       ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(7)        ; |40| 
           LDB     .D1T2   *+A14(8),B4       ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(8)        ; |40| 
           LDB     .D1T1   *+A14(9),A3       ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(9)        ; |40| 
           LDB     .D1T1   *+A14(10),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(10)       ; |40| 
           LDB     .D1T1   *+A14(11),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(11)       ; |40| 
           LDB     .D1T1   *+A14(12),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(12)       ; |40| 
           LDB     .D1T1   *+A14(13),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(13)       ; |40| 
           LDB     .D1T2   *+A14(14),B4      ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(14)       ; |40| 
           LDB     .D1T2   *+A14(15),B4      ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(15)       ; |40| 
           LDB     .D1T2   *+A14(16),B4      ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(16)       ; |40| 
           LDB     .D1T2   *+A14(17),B4      ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(17)       ; |40| 
           LDB     .D1T2   *+A14(18),B4      ; |40| 
           NOP             4
           STB     .D2T2   B4,*+B6(18)       ; |40| 
           LDB     .D1T1   *+A14(19),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(19)       ; |40| 
           LDB     .D1T1   *+A14(20),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(20)       ; |40| 
           LDB     .D1T1   *+A14(21),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(21)       ; |40| 
           LDB     .D1T1   *+A14(22),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(22)       ; |40| 
           LDB     .D1T1   *+A14(23),A3      ; |40| 
           NOP             4
           STB     .D2T1   A3,*+B6(23)       ; |40| 
;** --------------------------------------------------------------------------*
L14:    
           LDW     .D2T2   *+SP(172),B3      ; |205| 
           LDDW    .D2T1   *+SP(152),A11:A10 ; |205| 
           LDDW    .D2T1   *+SP(160),A13:A12 ; |205| 
           LDW     .D2T1   *+SP(168),A14     ; |205| 
           LDDW    .D2T2   *+SP(176),B11:B10 ; |205| 

           RET     .S2     B3                ; |205| 
||         LDW     .D2T1   *+SP(192),A15     ; |205| 

           LDDW    .D2T2   *+SP(184),B13:B12 ; |205| 
           NOP             3
	.dwpsn	"SunMoonGravitation.c",205,1
           ADDK    .S2     192,SP            ; |205| 
           ; BRANCH OCCURS {B3}              ; |205| 

DW$19	.dwtag  DW_TAG_loop
	.dwattr DW$19, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\SunMoonGravitation.asm:L11:1:1472539751")
	.dwattr DW$19, DW_AT_begin_file("../Sources/baseop.h")
	.dwattr DW$19, DW_AT_begin_line(0x26)
	.dwattr DW$19, DW_AT_end_line(0x29)
DW$20	.dwtag  DW_TAG_loop_range
	.dwattr DW$20, DW_AT_low_pc(DW$L$_SunMoonData_Gravitation$46$B)
	.dwattr DW$20, DW_AT_high_pc(DW$L$_SunMoonData_Gravitation$46$E)
	.dwendtag DW$19


DW$21	.dwtag  DW_TAG_loop
	.dwattr DW$21, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\SunMoonGravitation.asm:L3:1:1472539751")
	.dwattr DW$21, DW_AT_begin_file("../Sources/baseop.h")
	.dwattr DW$21, DW_AT_begin_line(0x26)
	.dwattr DW$21, DW_AT_end_line(0x29)
DW$22	.dwtag  DW_TAG_loop_range
	.dwattr DW$22, DW_AT_low_pc(DW$L$_SunMoonData_Gravitation$10$B)
	.dwattr DW$22, DW_AT_high_pc(DW$L$_SunMoonData_Gravitation$10$E)
	.dwendtag DW$21

	.dwattr DW$13, DW_AT_end_file("SunMoonGravitation.c")
	.dwattr DW$13, DW_AT_end_line(0xcd)
	.dwattr DW$13, DW_AT_end_column(0x01)
	.dwendtag DW$13

	.sect	".text"

DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("EclToCurrentEq_Matrix"), DW_AT_symbol_name("_EclToCurrentEq_Matrix")
	.dwattr DW$23, DW_AT_low_pc(_EclToCurrentEq_Matrix)
	.dwattr DW$23, DW_AT_high_pc(0x00)
	.dwattr DW$23, DW_AT_begin_file("SunMoonGravitation.c")
	.dwattr DW$23, DW_AT_begin_line(0xd8)
	.dwattr DW$23, DW_AT_begin_column(0x0d)
	.dwattr DW$23, DW_AT_frame_base[DW_OP_breg31 32]
	.dwattr DW$23, DW_AT_skeletal(0x01)
	.dwpsn	"SunMoonGravitation.c",217,1

;******************************************************************************
;* FUNCTION NAME: _EclToCurrentEq_Matrix                                      *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B13,SP,A16,*
;*                           A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28, *
;*                           A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23,B24, *
;*                           B25,B26,B27,B28,B29,B30,B31                      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B13,SP,A16,*
;*                           A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28, *
;*                           A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23,B24, *
;*                           B25,B26,B27,B28,B29,B30,B31                      *
;*   Local Frame Size  : 0 Args + 0 Auto + 32 Save = 32 byte                  *
;******************************************************************************
_EclToCurrentEq_Matrix:
;** --------------------------------------------------------------------------*
DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("T"), DW_AT_symbol_name("_T")
	.dwattr DW$24, DW_AT_type(*DW$T$19)
	.dwattr DW$24, DW_AT_location[DW_OP_reg4]
DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("M"), DW_AT_symbol_name("_M")
	.dwattr DW$25, DW_AT_type(*DW$T$38)
	.dwattr DW$25, DW_AT_location[DW_OP_reg20]
           MVKL    .S1     __mpyd,A3         ; |228| 
           MVKH    .S1     __mpyd,A3         ; |228| 
           MVKL    .S2     0x3f2dbfb3,B5     ; |228| 
           CALL    .S2X    A3                ; |228| 
           MVKH    .S2     0x3f2dbfb3,B5     ; |228| 

           MV      .L1X    SP,A31            ; |217| 
||         STW     .D2T2   B10,*SP--(32)     ; |217| 

           MVKL    .S2     0x344d66c,B4      ; |228| 
||         MV      .L1X    B4,A12            ; |217| 
||         STDW    .D1T1   A13:A12,*-A31(16)

           MVKH    .S2     0x344d66c,B4      ; |228| 
||         STDW    .D2T1   A11:A10,*+SP(8)

           ADDKPC  .S2     RL224,B3,0        ; |228| 
||         STDW    .D1T1   A15:A14,*-A31(8)
||         MV      .L2     B3,B13
||         STW     .D2T2   B13,*+SP(4)

RL224:     ; CALL OCCURS {__mpyd}            ; |228| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |228| 
           MVKH    .S1     __mpyd,A3         ; |228| 
           ZERO    .L2     B5                ; |228| 
           CALL    .S2X    A3                ; |228| 
           MVKH    .S2     0x3fe00000,B5     ; |228| 
           ADDKPC  .S2     RL228,B3,0        ; |228| 
           MV      .L1     A5,A13            ; |228| 
           MV      .S1     A4,A14            ; |228| 
           ZERO    .L2     B4                ; |228| 
RL228:     ; CALL OCCURS {__mpyd}            ; |228| 
           MVKL    .S1     __mpyd,A3         ; |228| 
           MVKH    .S1     __mpyd,A3         ; |228| 
           MV      .L2X    A14,B4            ; |228| 
           CALL    .S2X    A3                ; |228| 
           ADDKPC  .S2     RL229,B3,3        ; |228| 
           MV      .L2X    A13,B5            ; |228| 
RL229:     ; CALL OCCURS {__mpyd}            ; |228| 
           MVKL    .S1     __subd,A3         ; |228| 
           MVKH    .S1     __subd,A3         ; |228| 
           MV      .L2X    A5,B5             ; |228| 
           CALL    .S2X    A3                ; |228| 
           ZERO    .L1     A5                ; |228| 
           MV      .L2X    A4,B4             ; |228| 
           MVKH    .S1     0x3ff00000,A5     ; |228| 
           ADDKPC  .S2     RL230,B3,0        ; |228| 
           ZERO    .L1     A4                ; |228| 
RL230:     ; CALL OCCURS {__subd}            ; |228| 
           MVKL    .S1     __mpyd,A3         ; |228| 
           MVKH    .S1     __mpyd,A3         ; |228| 
           MVKL    .S2     0x3fed5c03,B5     ; |228| 
           CALL    .S2X    A3                ; |228| 
           MVKL    .S2     0x80d209ee,B4     ; |228| 
           MVKH    .S2     0x3fed5c03,B5     ; |228| 
           MVKH    .S2     0x80d209ee,B4     ; |228| 
           MV      .L1     A5,A15            ; |228| 

           MV      .L2X    A4,B10            ; |228| 
||         ADDKPC  .S2     RL233,B3,0        ; |228| 

RL233:     ; CALL OCCURS {__mpyd}            ; |228| 
           MVKL    .S1     __mpyd,A3         ; |228| 
           MVKH    .S1     __mpyd,A3         ; |228| 
           MVKL    .S2     0x3fd9752d,B5     ; |228| 
           CALL    .S2X    A3                ; |228| 
           MVKL    .S2     0x91e98eec,B4     ; |228| 
           MVKH    .S2     0x3fd9752d,B5     ; |228| 
           MV      .L1     A4,A11            ; |228| 
           MVKH    .S2     0x91e98eec,B4     ; |228| 

           MV      .L1     A13,A5            ; |228| 
||         MV      .D1     A5,A10            ; |228| 
||         MV      .S1     A14,A4            ; |228| 
||         ADDKPC  .S2     RL234,B3,0        ; |228| 

RL234:     ; CALL OCCURS {__mpyd}            ; |228| 
           MVKL    .S1     __addd,A3         ; |228| 
           MVKH    .S1     __addd,A3         ; |228| 
           MV      .L2X    A4,B4             ; |228| 
           CALL    .S2X    A3                ; |228| 
           MV      .L2X    A5,B5             ; |228| 
           MV      .S1     A11,A4            ; |228| 
           ADDKPC  .S2     RL235,B3,1        ; |228| 
           MV      .L1     A10,A5            ; |228| 
RL235:     ; CALL OCCURS {__addd}            ; |228| 
           MVKL    .S1     __mpyd,A3         ; |229| 
           MVKH    .S1     __mpyd,A3         ; |229| 
           MVKL    .S2     0xbfd9752d,B5     ; |229| 
           CALL    .S2X    A3                ; |229| 
           MVKL    .S2     0x91e98eec,B4     ; |229| 
           MVKH    .S2     0xbfd9752d,B5     ; |229| 
           MVKH    .S2     0x91e98eec,B4     ; |229| 

           MV      .L1     A5,A11            ; |228| 
||         MV      .S1     A4,A10            ; |228| 

           ADDKPC  .S2     RL238,B3,0        ; |229| 
||         MV      .L1X    B10,A4            ; |229| 
||         STDW    .D1T1   A11:A10,*A12      ; |228| 
||         MV      .S1     A15,A5            ; |229| 

RL238:     ; CALL OCCURS {__mpyd}            ; |229| 
           MVKL    .S2     __mpyd,B6         ; |229| 
           MVKH    .S2     __mpyd,B6         ; |229| 
           CALL    .S2     B6                ; |229| 
           MVKL    .S2     0x3fed5c03,B5     ; |229| 
           MVKL    .S2     0x80d209ee,B4     ; |229| 
           MVKH    .S2     0x3fed5c03,B5     ; |229| 
           MVKH    .S2     0x80d209ee,B4     ; |229| 

           MV      .S1     A13,A5            ; |229| 
||         ADDKPC  .S2     RL239,B3,0        ; |229| 
||         MV      .L1     A14,A4            ; |229| 
||         MV      .L2X    A4,B10            ; |229| 
||         MV      .D1     A5,A15            ; |229| 

RL239:     ; CALL OCCURS {__mpyd}            ; |229| 
           MVKL    .S1     __addd,A3         ; |229| 
           MVKH    .S1     __addd,A3         ; |229| 
           MV      .L2X    A5,B5             ; |229| 
           CALL    .S2X    A3                ; |229| 
           ADDKPC  .S2     RL240,B3,1        ; |229| 
           MV      .S1     A15,A5            ; |229| 
           MV      .L2X    A4,B4             ; |229| 
           MV      .L1X    B10,A4            ; |229| 
RL240:     ; CALL OCCURS {__addd}            ; |229| 
           MVKL    .S1     __negd,A3         ; |231| 
           MVKH    .S1     __negd,A3         ; |231| 
           STDW    .D1T1   A5:A4,*+A12(8)    ; |229| 
           CALL    .S2X    A3                ; |231| 
           ADDKPC  .S2     RL241,B3,4        ; |231| 
RL241:     ; CALL OCCURS {__negd}            ; |231| 
;** --------------------------------------------------------------------------*
           STDW    .D1T1   A11:A10,*+A12(24) ; |231| 

           STDW    .D1T1   A5:A4,*+A12(16)   ; |231| 
||         MV      .L1X    SP,A31            ; |238| 

           LDDW    .D1T1   *+A31(24),A15:A14 ; |238| 
||         LDW     .D2T2   *+SP(4),B13       ; |238| 
||         MV      .L2     B13,B3            ; |238| 

           RET     .S2     B3                ; |238| 
||         LDDW    .D2T1   *+SP(8),A11:A10   ; |238| 

           LDW     .D2T2   *++SP(32),B10     ; |238| 
||         LDDW    .D1T1   *+A31(16),A13:A12 ; |238| 

	.dwpsn	"SunMoonGravitation.c",238,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |238| 
	.dwattr DW$23, DW_AT_end_file("SunMoonGravitation.c")
	.dwattr DW$23, DW_AT_end_line(0xee)
	.dwattr DW$23, DW_AT_end_column(0x01)
	.dwendtag DW$23

	.sect	".text"

DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("EqToECEF_Matrix"), DW_AT_symbol_name("_EqToECEF_Matrix")
	.dwattr DW$26, DW_AT_low_pc(_EqToECEF_Matrix)
	.dwattr DW$26, DW_AT_high_pc(0x00)
	.dwattr DW$26, DW_AT_begin_file("SunMoonGravitation.c")
	.dwattr DW$26, DW_AT_begin_line(0xf9)
	.dwattr DW$26, DW_AT_begin_column(0x0d)
	.dwattr DW$26, DW_AT_frame_base[DW_OP_breg31 24]
	.dwattr DW$26, DW_AT_skeletal(0x01)
	.dwpsn	"SunMoonGravitation.c",250,1

;******************************************************************************
;* FUNCTION NAME: _EqToECEF_Matrix                                            *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,B0,*
;*                           B1,B2,B3,B4,B5,B6,B7,B8,B9,B13,SP,A16,A17,A18,   *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,B0,*
;*                           B1,B2,B3,B4,B5,B6,B7,B8,B9,B13,SP,A16,A17,A18,   *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 24 Save = 24 byte                  *
;******************************************************************************
_EqToECEF_Matrix:
;** --------------------------------------------------------------------------*
DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Tu"), DW_AT_symbol_name("_Tu")
	.dwattr DW$27, DW_AT_type(*DW$T$19)
	.dwattr DW$27, DW_AT_location[DW_OP_reg4]
DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("M"), DW_AT_symbol_name("_M")
	.dwattr DW$28, DW_AT_type(*DW$T$38)
	.dwattr DW$28, DW_AT_location[DW_OP_reg20]
           MVKL    .S1     __mpyd,A3         ; |269| 
           MVKH    .S1     __mpyd,A3         ; |269| 
           MVKL    .S2     0x41e7939d,B5     ; |269| 
           CALL    .S2X    A3                ; |269| 
           STW     .D2T1   A14,*SP--(24)     ; |250| 

           MVKL    .S2     0x471a0300,B4     ; |269| 
||         MV      .L1X    B4,A10            ; |250| 
||         STDW    .D2T1   A11:A10,*+SP(8)   ; |250| 

           MVKH    .S2     0x41e7939d,B5     ; |269| 

           MVKH    .S2     0x471a0300,B4     ; |269| 
||         STW     .D2T2   B13,*+SP(4)       ; |250| 

           ADDKPC  .S2     RL248,B3,0        ; |269| 
||         MV      .L1     A5,A11            ; |250| 
||         MV      .S1     A4,A12            ; |250| 
||         MV      .L2     B3,B13            ; |250| 
||         STDW    .D2T1   A13:A12,*+SP(16)  ; |250| 

RL248:     ; CALL OCCURS {__mpyd}            ; |269| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |269| 
           MVKH    .S1     __addd,A3         ; |269| 
           MVKL    .S2     0x40f06ee8,B5     ; |269| 
           CALL    .S2X    A3                ; |269| 
           MVKL    .S2     0xc649906d,B4     ; |269| 
           MVKH    .S2     0x40f06ee8,B5     ; |269| 
           MVKH    .S2     0xc649906d,B4     ; |269| 
           ADDKPC  .S2     RL249,B3,1        ; |269| 
RL249:     ; CALL OCCURS {__addd}            ; |269| 
           MVKL    .S1     __mpyd,A3         ; |269| 
           MVKH    .S1     __mpyd,A3         ; |269| 
           MVKL    .S2     0x3fb7d5a9,B5     ; |269| 
           CALL    .S2X    A3                ; |269| 
           MVKL    .S2     0xeb2074eb,B4     ; |269| 
           MVKH    .S2     0x3fb7d5a9,B5     ; |269| 
           MVKH    .S2     0xeb2074eb,B4     ; |269| 
           MV      .L1     A4,A14            ; |269| 

           ADDKPC  .S2     RL250,B3,0        ; |269| 
||         MV      .L1     A11,A5            ; |269| 
||         MV      .S1     A12,A4            ; |269| 
||         MV      .D1     A5,A13            ; |269| 

RL250:     ; CALL OCCURS {__mpyd}            ; |269| 
           MVKL    .S1     __mpyd,A3         ; |269| 
           MVKH    .S1     __mpyd,A3         ; |269| 
           MV      .L2X    A12,B4            ; |269| 
           CALL    .S2X    A3                ; |269| 
           ADDKPC  .S2     RL251,B3,3        ; |269| 
           MV      .L2X    A11,B5            ; |269| 
RL251:     ; CALL OCCURS {__mpyd}            ; |269| 
           MVKL    .S2     __addd,B6         ; |269| 
           MVKH    .S2     __addd,B6         ; |269| 
           CALL    .S2     B6                ; |269| 
           MV      .L2X    A4,B4             ; |269| 
           MV      .L2X    A5,B5             ; |269| 
           MV      .S1     A14,A4            ; |269| 
           ADDKPC  .S2     RL252,B3,0        ; |269| 
           MV      .L1     A13,A5            ; |269| 
RL252:     ; CALL OCCURS {__addd}            ; |269| 
           MVKL    .S2     __fixdu,B4        ; |269| 
           MVKH    .S2     __fixdu,B4        ; |269| 
           CALL    .S2     B4                ; |269| 
           MV      .S1     A5,A12            ; |269| 
           MV      .L1     A4,A13            ; |269| 
           ADDKPC  .S2     RL253,B3,2        ; |269| 
RL253:     ; CALL OCCURS {__fixdu}           ; |269| 
           MVKL    .S1     __fltud,A3        ; |269| 
           MVKH    .S1     __fltud,A3        ; |269| 
           MV      .L1     A4,A11            ; |269| 
           CALL    .S2X    A3                ; |269| 
           ADDKPC  .S2     RL270,B3,4        ; |269| 
RL270:     ; CALL OCCURS {__fltud}           ; |269| 
           MVKL    .S1     __subd,A3         ; |269| 
           MVKH    .S1     __subd,A3         ; |269| 
           MV      .L2X    A4,B4             ; |269| 
           CALL    .S2X    A3                ; |269| 
           MV      .L2X    A5,B5             ; |269| 
           MV      .S1     A13,A4            ; |269| 
           ADDKPC  .S2     RL271,B3,1        ; |269| 
           MV      .L1     A12,A5            ; |269| 
RL271:     ; CALL OCCURS {__subd}            ; |269| 
           MVKL    .S1     __remu,A3         ; |269| 
           MVKH    .S1     __remu,A3         ; |269| 
           MVKL    .S2     0x15180,B4        ; |269| 
           CALL    .S2X    A3                ; |269| 
           MVKH    .S2     0x15180,B4        ; |269| 
           ADDKPC  .S2     RL272,B3,0        ; |269| 
           MV      .D1     A4,A12            ; |269| 
           MV      .S1     A11,A4            ; |269| 
           MV      .L1     A5,A11            ; |269| 
RL272:     ; CALL OCCURS {__remu}            ; |269| 
           MVKL    .S1     __fltud,A3        ; |269| 
           MVKH    .S1     __fltud,A3        ; |269| 
           NOP             1
           CALL    .S2X    A3                ; |269| 
           ADDKPC  .S2     RL273,B3,4        ; |269| 
RL273:     ; CALL OCCURS {__fltud}           ; |269| 
           MVKL    .S1     __addd,A3         ; |269| 
           MVKH    .S1     __addd,A3         ; |269| 
           MV      .L2X    A5,B5             ; |269| 
           CALL    .S2X    A3                ; |269| 
           MV      .L2X    A4,B4             ; |269| 
           MV      .S1     A11,A5            ; |269| 
           ADDKPC  .S2     RL274,B3,1        ; |269| 
           MV      .L1     A12,A4            ; |269| 
RL274:     ; CALL OCCURS {__addd}            ; |269| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |269| 
           MVKH    .S1     __mpyd,A3         ; |269| 
           MVKL    .S2     0x3fefe9a1,B5     ; |269| 
           CALL    .S2X    A3                ; |269| 
           MVKL    .S2     0xdd95be9e,B4     ; |269| 
           MVKH    .S2     0x3fefe9a1,B5     ; |269| 
           MVKH    .S2     0xdd95be9e,B4     ; |269| 
           ADDKPC  .S2     RL275,B3,1        ; |269| 
RL275:     ; CALL OCCURS {__mpyd}            ; |269| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |269| 
           MVKH    .S1     __mpyd,A3         ; |269| 
           MVKL    .S2     0x3f131da7,B5     ; |269| 
           CALL    .S2X    A3                ; |269| 
           MVKL    .S2     0xd7cb8d5b,B4     ; |269| 
           MVKH    .S2     0x3f131da7,B5     ; |269| 
           MVKH    .S2     0xd7cb8d5b,B4     ; |269| 
           ADDKPC  .S2     RL276,B3,1        ; |269| 
RL276:     ; CALL OCCURS {__mpyd}            ; |269| 
           MVKL    .S1     _cos,A3           ; |269| 
           MVKH    .S1     _cos,A3           ; |269| 
           MV      .L1     A5,A11            ; |269| 
           CALL    .S2X    A3                ; |269| 
           ADDKPC  .S2     RL277,B3,3        ; |269| 
           MV      .S1     A4,A12            ; |269| 
RL277:     ; CALL OCCURS {_cos}              ; |269| 
           MVKL    .S1     _sin,A3           ; |269| 
           MVKH    .S1     _sin,A3           ; |269| 
           STDW    .D1T1   A5:A4,*A10        ; |269| 
           CALL    .S2X    A3                ; |269| 
           ADDKPC  .S2     RL278,B3,2        ; |269| 
           MV      .S1     A12,A4            ; |269| 
           MV      .L1     A11,A5            ; |269| 
RL278:     ; CALL OCCURS {_sin}              ; |269| 
           MVKL    .S1     __negd,A3         ; |270| 
           MVKH    .S1     __negd,A3         ; |270| 
           STDW    .D1T1   A5:A4,*+A10(8)    ; |269| 
           CALL    .S2X    A3                ; |270| 
           ADDKPC  .S2     RL279,B3,4        ; |270| 
RL279:     ; CALL OCCURS {__negd}            ; |270| 
;** --------------------------------------------------------------------------*
           LDDW    .D1T1   *A10,A7:A6        ; |270| 
           STDW    .D1T1   A5:A4,*+A10(16)   ; |270| 
           LDDW    .D2T1   *+SP(16),A13:A12  ; |273| 
           MV      .L2     B13,B3            ; |273| 
           LDW     .D2T2   *+SP(4),B13       ; |273| 
           STDW    .D1T1   A7:A6,*+A10(24)   ; |270| 

           RET     .S2     B3                ; |273| 
||         LDDW    .D2T1   *+SP(8),A11:A10   ; |273| 

           LDW     .D2T1   *++SP(24),A14     ; |273| 
	.dwpsn	"SunMoonGravitation.c",273,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |273| 
	.dwattr DW$26, DW_AT_end_file("SunMoonGravitation.c")
	.dwattr DW$26, DW_AT_end_line(0x111)
	.dwattr DW$26, DW_AT_end_column(0x01)
	.dwendtag DW$26

	.sect	".text"

DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("SunPos_GeoCenterEclFrame"), DW_AT_symbol_name("_SunPos_GeoCenterEclFrame")
	.dwattr DW$29, DW_AT_low_pc(_SunPos_GeoCenterEclFrame)
	.dwattr DW$29, DW_AT_high_pc(0x00)
	.dwattr DW$29, DW_AT_begin_file("SunMoonGravitation.c")
	.dwattr DW$29, DW_AT_begin_line(0x11c)
	.dwattr DW$29, DW_AT_begin_column(0x0d)
	.dwattr DW$29, DW_AT_frame_base[DW_OP_breg31 48]
	.dwattr DW$29, DW_AT_skeletal(0x01)
	.dwpsn	"SunMoonGravitation.c",285,1

;******************************************************************************
;* FUNCTION NAME: _SunPos_GeoCenterEclFrame                                   *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 0 Auto + 44 Save = 44 byte                  *
;******************************************************************************
_SunPos_GeoCenterEclFrame:
;** --------------------------------------------------------------------------*
DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("T"), DW_AT_symbol_name("_T")
	.dwattr DW$30, DW_AT_type(*DW$T$19)
	.dwattr DW$30, DW_AT_location[DW_OP_reg4]
DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Rs"), DW_AT_symbol_name("_Rs")
	.dwattr DW$31, DW_AT_type(*DW$T$34)
	.dwattr DW$31, DW_AT_location[DW_OP_reg20]
           MVKL    .S1     __mpyd,A3         ; |301| 
           MVKH    .S1     __mpyd,A3         ; |301| 
           MVKL    .S2     0x419ee5f2,B5     ; |301| 
           CALL    .S2X    A3                ; |301| 
           MVKH    .S2     0x419ee5f2,B5     ; |301| 

           MV      .L1X    SP,A31            ; |285| 
||         STW     .D2T1   A15,*SP--(48)     ; |285| 

           MVKL    .S2     0x94e56042,B4     ; |301| 
||         MV      .L1X    B4,A10            ; |285| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         STDW    .D2T2   B13:B12,*+SP(40)

           MVKH    .S2     0x94e56042,B4     ; |301| 
||         STDW    .D1T1   A13:A12,*-A31(32)
||         MV      .L2X    A4,B12            ; |285| 
||         STDW    .D2T2   B11:B10,*+SP(32)

           ADDKPC  .S2     RL281,B3,0        ; |301| 
||         STW     .D1T1   A14,*-A31(24)
||         MV      .L2X    A5,B11            ; |285| 
||         STW     .D2T2   B3,*+SP(28)

RL281:     ; CALL OCCURS {__mpyd}            ; |301| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |301| 
           MVKH    .S1     __addd,A3         ; |301| 
           MVKL    .S2     0x4133a3bb,B5     ; |301| 
           CALL    .S2X    A3                ; |301| 
           MVKL    .S2     0xcdd2f1aa,B4     ; |301| 
           MVKH    .S2     0x4133a3bb,B5     ; |301| 
           MVKH    .S2     0xcdd2f1aa,B4     ; |301| 
           ADDKPC  .S2     RL282,B3,1        ; |301| 
RL282:     ; CALL OCCURS {__addd}            ; |301| 
           MVKL    .S1     __fixdu,A3        ; |301| 
           MVKH    .S1     __fixdu,A3        ; |301| 
           MV      .L1     A4,A12            ; |301| 
           CALL    .S2X    A3                ; |301| 
           ADDKPC  .S2     RL283,B3,3        ; |301| 
           MV      .S1     A5,A11            ; |301| 
RL283:     ; CALL OCCURS {__fixdu}           ; |301| 
           MVKL    .S1     __remu,A5         ; |301| 
           MVKH    .S1     __remu,A5         ; |301| 
           MVKL    .S2     0x13c680,B4       ; |301| 
           CALL    .S2X    A5                ; |301| 
           MVKH    .S2     0x13c680,B4       ; |301| 
           MV      .L1     A4,A3             ; |301| 
           ADDKPC  .S2     RL289,B3,2        ; |301| 
RL289:     ; CALL OCCURS {__remu}            ; |301| 
           MVKL    .S1     __fltud,A5        ; |301| 
           MVKH    .S1     __fltud,A5        ; |301| 
           SUB     .L1     A3,A4,A4          ; |301| 
           CALL    .S2X    A5                ; |301| 
           ADDKPC  .S2     RL290,B3,4        ; |301| 
RL290:     ; CALL OCCURS {__fltud}           ; |301| 
           MVKL    .S2     __subd,B6         ; |301| 
           MVKH    .S2     __subd,B6         ; |301| 
           CALL    .S2     B6                ; |301| 
           MV      .L2X    A4,B4             ; |301| 
           MV      .L2X    A5,B5             ; |301| 
           MV      .S1     A12,A4            ; |301| 
           ADDKPC  .S2     RL291,B3,0        ; |301| 
           MV      .L1     A11,A5            ; |301| 
RL291:     ; CALL OCCURS {__subd}            ; |301| 
           MVKL    .S1     __mpyd,A3         ; |301| 
           MVKH    .S1     __mpyd,A3         ; |301| 
           MVKL    .S2     0x3ed455a5,B5     ; |301| 
           CALL    .S2X    A3                ; |301| 
           MVKL    .S2     0xb2ff8f9d,B4     ; |301| 
           MVKH    .S2     0x3ed455a5,B5     ; |301| 
           MVKH    .S2     0xb2ff8f9d,B4     ; |301| 
           ADDKPC  .S2     RL292,B3,1        ; |301| 
RL292:     ; CALL OCCURS {__mpyd}            ; |301| 
           MVKL    .S1     _cos,A3           ; |301| 
           MVKH    .S1     _cos,A3           ; |301| 
           MV      .S1     A4,A12            ; |301| 
           CALL    .S2X    A3                ; |301| 
           MV      .L1     A5,A11            ; |301| 
           ADDKPC  .S2     RL293,B3,3        ; |301| 
RL293:     ; CALL OCCURS {_cos}              ; |301| 
           MVKL    .S1     _sin,A3           ; |301| 
           MVKH    .S1     _sin,A3           ; |301| 
           MV      .L1     A4,A15            ; |301| 
           CALL    .S2X    A3                ; |301| 
           MV      .D1     A5,A14            ; |301| 
           MV      .S1     A12,A4            ; |301| 
           ADDKPC  .S2     RL294,B3,1        ; |301| 
           MV      .L1     A11,A5            ; |301| 
RL294:     ; CALL OCCURS {_sin}              ; |301| 
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MVKL    .S2     0x3f9121ab,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0x4b72c519,B4     ; |309| 
           MVKH    .S2     0x3f9121ab,B5     ; |309| 
           MVKH    .S2     0x4b72c519,B4     ; |309| 
           MV      .L1     A4,A12            ; |301| 

           MV      .L1     A14,A5            ; |309| 
||         ADDKPC  .S2     RL312,B3,0        ; |309| 
||         MV      .S1     A15,A4            ; |309| 
||         MV      .D1     A5,A11            ; |301| 

RL312:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __subd,A3         ; |309| 
           MVKH    .S1     __subd,A3         ; |309| 
           MV      .L2X    A4,B4             ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S1     0xccf6be38,A4     ; |309| 
           MV      .L2X    A5,B5             ; |309| 
           MVKL    .S1     0x3ff00092,A5     ; |309| 
           MVKH    .S1     0xccf6be38,A4     ; |309| 

           ADDKPC  .S2     RL313,B3,0        ; |309| 
||         MVKH    .S1     0x3ff00092,A5     ; |309| 

RL313:     ; CALL OCCURS {__subd}            ; |309| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MVKL    .S2     0x3f32599e,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0xd7c6fbd2,B4     ; |309| 
           MVKH    .S2     0x3f32599e,B5     ; |309| 
           MVKH    .S2     0xd7c6fbd2,B4     ; |309| 
           ADDKPC  .S2     RL314,B3,0        ; |309| 

           MV      .D1     A5,A13            ; |309| 
||         MV      .L2X    A4,B10            ; |309| 
||         MV      .L1     A14,A5            ; |309| 
||         MV      .S1     A15,A4            ; |309| 

RL314:     ; CALL OCCURS {__mpyd}            ; |309| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MV      .L2X    A15,B4            ; |309| 
           CALL    .S2X    A3                ; |309| 
           ADDKPC  .S2     RL315,B3,3        ; |309| 
           MV      .L2X    A14,B5            ; |309| 
RL315:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __subd,A3         ; |309| 
           MVKH    .S1     __subd,A3         ; |309| 
           MV      .L2X    A4,B4             ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L2X    A5,B5             ; |309| 
           MV      .S1X    B10,A4            ; |309| 
           ADDKPC  .S2     RL316,B3,1        ; |309| 
           MV      .L1     A13,A5            ; |309| 
RL316:     ; CALL OCCURS {__subd}            ; |309| 
           MVKL    .S1     __addd,A3         ; |309| 
           MVKH    .S1     __addd,A3         ; |309| 
           MVKL    .S2     0x3f22599e,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0xd7c6fbd2,B4     ; |309| 
           MVKH    .S2     0x3f22599e,B5     ; |309| 
           MVKH    .S2     0xd7c6fbd2,B4     ; |309| 
           ADDKPC  .S2     RL317,B3,1        ; |309| 
RL317:     ; CALL OCCURS {__addd}            ; |309| 
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MVKL    .S2     0x42416a5d,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKH    .S2     0x42416a5d,B5     ; |309| 
           ZERO    .L2     B4                ; |309| 
           MVKH    .S2     0x2bd80000,B4     ; |309| 
           ADDKPC  .S2     RL318,B3,1        ; |309| 
RL318:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MVKL    .S2     0xeb1bdd2b,B4     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0x40e193eb,B5     ; |309| 
           MVKH    .S2     0xeb1bdd2b,B4     ; |309| 
           MV      .S1     A5,A13            ; |309| 

           MVKH    .S2     0x40e193eb,B5     ; |309| 
||         MV      .L1X    B11,A5            ; |309| 

           MV      .L1X    B12,A4            ; |309| 
||         ADDKPC  .S2     RL320,B3,0        ; |309| 
||         MV      .L2X    A4,B10            ; |309| 

RL320:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __addd,A3         ; |309| 
           MVKH    .S1     __addd,A3         ; |309| 
           MVKL    .S2     0x4071876e,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0xe2b16593,B4     ; |309| 
           MVKH    .S2     0x4071876e,B5     ; |309| 
           MVKH    .S2     0xe2b16593,B4     ; |309| 
           ADDKPC  .S2     RL321,B3,1        ; |309| 
RL321:     ; CALL OCCURS {__addd}            ; |309| 
           MVKL    .S2     __fixdu,B4        ; |309| 
           MVKH    .S2     __fixdu,B4        ; |309| 
           CALL    .S2     B4                ; |309| 
           MV      .L2X    A4,B13            ; |309| 
           ADDKPC  .S2     RL322,B3,2        ; |309| 
           MV      .L2X    A5,B12            ; |309| 
RL322:     ; CALL OCCURS {__fixdu}           ; |309| 
           MVKL    .S1     __fltud,A3        ; |309| 
           MVKH    .S1     __fltud,A3        ; |309| 
           MV      .L2X    A4,B11            ; |309| 
           CALL    .S2X    A3                ; |309| 
           ADDKPC  .S2     RL368,B3,4        ; |309| 
RL368:     ; CALL OCCURS {__fltud}           ; |309| 
           MVKL    .S1     __subd,A3         ; |309| 
           MVKH    .S1     __subd,A3         ; |309| 
           MV      .L2X    A4,B4             ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L1X    B13,A4            ; |309| 
           MV      .L2X    A5,B5             ; |309| 
           ADDKPC  .S2     RL369,B3,1        ; |309| 
           MV      .L1X    B12,A5            ; |309| 
RL369:     ; CALL OCCURS {__subd}            ; |309| 
           MVKL    .S1     __remu,A3         ; |309| 
           MVKH    .S1     __remu,A3         ; |309| 
           MVK     .S2     0x168,B4          ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L2X    A4,B13            ; |309| 
           ADDKPC  .S2     RL370,B3,1        ; |309| 
           MV      .L1X    B11,A4            ; |309| 
           MV      .L2X    A5,B12            ; |309| 
RL370:     ; CALL OCCURS {__remu}            ; |309| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __fltud,A3        ; |309| 
           MVKH    .S1     __fltud,A3        ; |309| 
           NOP             1
           CALL    .S2X    A3                ; |309| 
           ADDKPC  .S2     RL371,B3,4        ; |309| 
RL371:     ; CALL OCCURS {__fltud}           ; |309| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |309| 
           MVKH    .S1     __addd,A3         ; |309| 
           MV      .L2X    A4,B4             ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L1X    B13,A4            ; |309| 
           ADDKPC  .S2     RL372,B3,1        ; |309| 
           MV      .L2X    A5,B5             ; |309| 
           MV      .L1X    B12,A5            ; |309| 
RL372:     ; CALL OCCURS {__addd}            ; |309| 
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MVKL    .S2     0x3fa47ae1,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0x47ae147b,B4     ; |309| 
           MVKH    .S2     0x3fa47ae1,B5     ; |309| 
           MV      .L2X    A5,B11            ; |309| 
           MVKH    .S2     0x47ae147b,B4     ; |309| 

           MV      .L1     A15,A4            ; |309| 
||         MV      .L2X    A4,B12            ; |309| 
||         MV      .S1     A14,A5            ; |309| 
||         ADDKPC  .S2     RL373,B3,0        ; |309| 

RL373:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __addd,A3         ; |309| 
           MVKH    .S1     __addd,A3         ; |309| 
           MVKL    .S2     0x3ffeac71,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0xcb295ea,B4      ; |309| 
           MVKH    .S2     0x3ffeac71,B5     ; |309| 
           MVKH    .S2     0xcb295ea,B4      ; |309| 
           ADDKPC  .S2     RL374,B3,1        ; |309| 
RL374:     ; CALL OCCURS {__addd}            ; |309| 
           MVKL    .S1     __addd,A3         ; |309| 
           MVKH    .S1     __addd,A3         ; |309| 
           MVKL    .S2     0x3f4d7dbf,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0x487fcb92,B4     ; |309| 
           MVKH    .S2     0x3f4d7dbf,B5     ; |309| 
           MVKH    .S2     0x487fcb92,B4     ; |309| 
           ADDKPC  .S2     RL375,B3,1        ; |309| 
RL375:     ; CALL OCCURS {__addd}            ; |309| 
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MVKL    .S2     0x3f53a92a,B5     ; |309| 
           CALL    .S2X    A3                ; |309| 
           MVKL    .S2     0x30553261,B4     ; |309| 
           MVKH    .S2     0x3f53a92a,B5     ; |309| 
           MV      .L1     A4,A15            ; |309| 
           MVKH    .S2     0x30553261,B4     ; |309| 

           MV      .L1     A11,A5            ; |309| 
||         MV      .D1     A5,A14            ; |309| 
||         MV      .S1     A12,A4            ; |309| 
||         ADDKPC  .S2     RL376,B3,0        ; |309| 

RL376:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MV      .L2X    A12,B4            ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L2X    A11,B5            ; |309| 
           ADDKPC  .S2     RL377,B3,3        ; |309| 
RL377:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __subd,A3         ; |309| 
           MVKH    .S1     __subd,A3         ; |309| 
           MV      .L2X    A4,B4             ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L2X    A5,B5             ; |309| 
           MV      .S1     A15,A4            ; |309| 
           ADDKPC  .S2     RL378,B3,1        ; |309| 
           MV      .L1     A14,A5            ; |309| 
RL378:     ; CALL OCCURS {__subd}            ; |309| 
           MVKL    .S2     __mpyd,B6         ; |309| 
           MVKH    .S2     __mpyd,B6         ; |309| 
           CALL    .S2     B6                ; |309| 
           MV      .L2X    A12,B4            ; |309| 
           ADDKPC  .S2     RL379,B3,2        ; |309| 
           MV      .L2X    A11,B5            ; |309| 
RL379:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S1     __addd,A3         ; |309| 
           MVKH    .S1     __addd,A3         ; |309| 
           MV      .L2X    A4,B4             ; |309| 
           CALL    .S2X    A3                ; |309| 
           MV      .L1X    B12,A4            ; |309| 
           MV      .L2X    A5,B5             ; |309| 
           ADDKPC  .S2     RL380,B3,1        ; |309| 
           MV      .L1X    B11,A5            ; |309| 
RL380:     ; CALL OCCURS {__addd}            ; |309| 
           MVKL    .S2     __mpyd,B6         ; |309| 
           MVKH    .S2     __mpyd,B6         ; |309| 
           CALL    .S2     B6                ; |309| 
           MVKL    .S2     0x3f91df46,B5     ; |309| 
           MVKL    .S2     0xa2529d39,B4     ; |309| 
           MVKH    .S2     0x3f91df46,B5     ; |309| 
           MVKH    .S2     0xa2529d39,B4     ; |309| 
           ADDKPC  .S2     RL381,B3,0        ; |309| 
RL381:     ; CALL OCCURS {__mpyd}            ; |309| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _cos,A3           ; |309| 
           MVKH    .S1     _cos,A3           ; |309| 
           MV      .L1     A5,A11            ; |309| 
           CALL    .S2X    A3                ; |309| 
           ADDKPC  .S2     RL383,B3,3        ; |309| 
           MV      .S1     A4,A12            ; |309| 
RL383:     ; CALL OCCURS {_cos}              ; |309| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |309| 
           MVKH    .S1     __mpyd,A3         ; |309| 
           MV      .L2X    A13,B5            ; |309| 
           CALL    .S2X    A3                ; |309| 
           ADDKPC  .S2     RL384,B3,3        ; |309| 
           MV      .D2     B10,B4            ; |309| 
RL384:     ; CALL OCCURS {__mpyd}            ; |309| 
           MVKL    .S2     _sin,B4           ; |310| 
           MVKH    .S2     _sin,B4           ; |310| 
           CALL    .S2     B4                ; |310| 
           STDW    .D1T1   A5:A4,*A10        ; |309| 
           ADDKPC  .S2     RL386,B3,1        ; |310| 
           MV      .S1     A12,A4            ; |310| 
           MV      .L1     A11,A5            ; |310| 
RL386:     ; CALL OCCURS {_sin}              ; |310| 
           MVKL    .S1     __mpyd,A3         ; |310| 
           MVKH    .S1     __mpyd,A3         ; |310| 
           MV      .L2X    A13,B5            ; |310| 
           CALL    .S2X    A3                ; |310| 
           ADDKPC  .S2     RL387,B3,3        ; |310| 
           MV      .D2     B10,B4            ; |310| 
RL387:     ; CALL OCCURS {__mpyd}            ; |310| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(28),B3       ; |314| 

           STDW    .D1T1   A5:A4,*+A10(8)    ; |310| 
||         MV      .L1X    SP,A31            ; |314| 

           LDDW    .D1T1   *+A31(16),A13:A12 ; |314| 
||         ZERO    .L2     B5:B4             ; |311| 

           STDW    .D1T2   B5:B4,*+A10(16)   ; |311| 

           LDW     .D1T1   *+A31(24),A14     ; |314| 
||         LDDW    .D2T2   *+SP(40),B13:B12  ; |314| 

           RET     .S2     B3                ; |314| 
||         LDDW    .D1T1   *+A31(8),A11:A10  ; |314| 
||         LDDW    .D2T2   *+SP(32),B11:B10  ; |314| 

           LDW     .D2T1   *++SP(48),A15     ; |314| 
	.dwpsn	"SunMoonGravitation.c",314,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |314| 
	.dwattr DW$29, DW_AT_end_file("SunMoonGravitation.c")
	.dwattr DW$29, DW_AT_end_line(0x13a)
	.dwattr DW$29, DW_AT_end_column(0x01)
	.dwendtag DW$29

	.sect	".text"

DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("MoonPos_GeoCenterEclFrame"), DW_AT_symbol_name("_MoonPos_GeoCenterEclFrame")
	.dwattr DW$32, DW_AT_low_pc(_MoonPos_GeoCenterEclFrame)
	.dwattr DW$32, DW_AT_high_pc(0x00)
	.dwattr DW$32, DW_AT_begin_file("SunMoonGravitation.c")
	.dwattr DW$32, DW_AT_begin_line(0x145)
	.dwattr DW$32, DW_AT_begin_column(0x0d)
	.dwattr DW$32, DW_AT_frame_base[DW_OP_breg31 152]
	.dwattr DW$32, DW_AT_skeletal(0x01)
	.dwpsn	"SunMoonGravitation.c",326,1
DW$33	.dwtag  DW_TAG_variable, DW_AT_name("l"), DW_AT_symbol_name("_l$1")
	.dwattr DW$33, DW_AT_type(*DW$T$19)
	.dwattr DW$33, DW_AT_location[DW_OP_addr _l$1]
DW$34	.dwtag  DW_TAG_variable, DW_AT_name("l1"), DW_AT_symbol_name("_l1$2")
	.dwattr DW$34, DW_AT_type(*DW$T$19)
	.dwattr DW$34, DW_AT_location[DW_OP_addr _l1$2]
DW$35	.dwtag  DW_TAG_variable, DW_AT_name("F"), DW_AT_symbol_name("_F$3")
	.dwattr DW$35, DW_AT_type(*DW$T$19)
	.dwattr DW$35, DW_AT_location[DW_OP_addr _F$3]
DW$36	.dwtag  DW_TAG_variable, DW_AT_name("D"), DW_AT_symbol_name("_D$4")
	.dwattr DW$36, DW_AT_type(*DW$T$19)
	.dwattr DW$36, DW_AT_location[DW_OP_addr _D$4]
DW$37	.dwtag  DW_TAG_variable, DW_AT_name("OM"), DW_AT_symbol_name("_OM$5")
	.dwattr DW$37, DW_AT_type(*DW$T$19)
	.dwattr DW$37, DW_AT_location[DW_OP_addr _OM$5]
DW$38	.dwtag  DW_TAG_variable, DW_AT_name("L"), DW_AT_symbol_name("_L$6")
	.dwattr DW$38, DW_AT_type(*DW$T$19)
	.dwattr DW$38, DW_AT_location[DW_OP_addr _L$6]
DW$39	.dwtag  DW_TAG_variable, DW_AT_name("lamda"), DW_AT_symbol_name("_lamda$7")
	.dwattr DW$39, DW_AT_type(*DW$T$19)
	.dwattr DW$39, DW_AT_location[DW_OP_addr _lamda$7]
DW$40	.dwtag  DW_TAG_variable, DW_AT_name("beta"), DW_AT_symbol_name("_beta$8")
	.dwattr DW$40, DW_AT_type(*DW$T$19)
	.dwattr DW$40, DW_AT_location[DW_OP_addr _beta$8]
DW$41	.dwtag  DW_TAG_variable, DW_AT_name("Q"), DW_AT_symbol_name("_Q$9")
	.dwattr DW$41, DW_AT_type(*DW$T$19)
	.dwattr DW$41, DW_AT_location[DW_OP_addr _Q$9]
DW$42	.dwtag  DW_TAG_variable, DW_AT_name("rm"), DW_AT_symbol_name("_rm$10")
	.dwattr DW$42, DW_AT_type(*DW$T$19)
	.dwattr DW$42, DW_AT_location[DW_OP_addr _rm$10]
DW$43	.dwtag  DW_TAG_variable, DW_AT_name("sin_l"), DW_AT_symbol_name("_sin_l$11")
	.dwattr DW$43, DW_AT_type(*DW$T$19)
	.dwattr DW$43, DW_AT_location[DW_OP_addr _sin_l$11]
DW$44	.dwtag  DW_TAG_variable, DW_AT_name("sin_l1"), DW_AT_symbol_name("_sin_l1$12")
	.dwattr DW$44, DW_AT_type(*DW$T$19)
	.dwattr DW$44, DW_AT_location[DW_OP_addr _sin_l1$12]
DW$45	.dwtag  DW_TAG_variable, DW_AT_name("sin_D"), DW_AT_symbol_name("_sin_D$13")
	.dwattr DW$45, DW_AT_type(*DW$T$19)
	.dwattr DW$45, DW_AT_location[DW_OP_addr _sin_D$13]
DW$46	.dwtag  DW_TAG_variable, DW_AT_name("sin_F"), DW_AT_symbol_name("_sin_F$14")
	.dwattr DW$46, DW_AT_type(*DW$T$19)
	.dwattr DW$46, DW_AT_location[DW_OP_addr _sin_F$14]
DW$47	.dwtag  DW_TAG_variable, DW_AT_name("sin_2l"), DW_AT_symbol_name("_sin_2l$15")
	.dwattr DW$47, DW_AT_type(*DW$T$19)
	.dwattr DW$47, DW_AT_location[DW_OP_addr _sin_2l$15]
DW$48	.dwtag  DW_TAG_variable, DW_AT_name("sin_2D"), DW_AT_symbol_name("_sin_2D$16")
	.dwattr DW$48, DW_AT_type(*DW$T$19)
	.dwattr DW$48, DW_AT_location[DW_OP_addr _sin_2D$16]
DW$49	.dwtag  DW_TAG_variable, DW_AT_name("sin_2F"), DW_AT_symbol_name("_sin_2F$17")
	.dwattr DW$49, DW_AT_type(*DW$T$19)
	.dwattr DW$49, DW_AT_location[DW_OP_addr _sin_2F$17]
DW$50	.dwtag  DW_TAG_variable, DW_AT_name("sin_4D"), DW_AT_symbol_name("_sin_4D$18")
	.dwattr DW$50, DW_AT_type(*DW$T$19)
	.dwattr DW$50, DW_AT_location[DW_OP_addr _sin_4D$18]
DW$51	.dwtag  DW_TAG_variable, DW_AT_name("cos_l"), DW_AT_symbol_name("_cos_l$19")
	.dwattr DW$51, DW_AT_type(*DW$T$19)
	.dwattr DW$51, DW_AT_location[DW_OP_addr _cos_l$19]
DW$52	.dwtag  DW_TAG_variable, DW_AT_name("cos_l1"), DW_AT_symbol_name("_cos_l1$20")
	.dwattr DW$52, DW_AT_type(*DW$T$19)
	.dwattr DW$52, DW_AT_location[DW_OP_addr _cos_l1$20]
DW$53	.dwtag  DW_TAG_variable, DW_AT_name("cos_D"), DW_AT_symbol_name("_cos_D$21")
	.dwattr DW$53, DW_AT_type(*DW$T$19)
	.dwattr DW$53, DW_AT_location[DW_OP_addr _cos_D$21]
DW$54	.dwtag  DW_TAG_variable, DW_AT_name("cos_F"), DW_AT_symbol_name("_cos_F$22")
	.dwattr DW$54, DW_AT_type(*DW$T$19)
	.dwattr DW$54, DW_AT_location[DW_OP_addr _cos_F$22]
DW$55	.dwtag  DW_TAG_variable, DW_AT_name("cos_2l"), DW_AT_symbol_name("_cos_2l$23")
	.dwattr DW$55, DW_AT_type(*DW$T$19)
	.dwattr DW$55, DW_AT_location[DW_OP_addr _cos_2l$23]
DW$56	.dwtag  DW_TAG_variable, DW_AT_name("cos_2D"), DW_AT_symbol_name("_cos_2D$24")
	.dwattr DW$56, DW_AT_type(*DW$T$19)
	.dwattr DW$56, DW_AT_location[DW_OP_addr _cos_2D$24]
DW$57	.dwtag  DW_TAG_variable, DW_AT_name("cos_2F"), DW_AT_symbol_name("_cos_2F$25")
	.dwattr DW$57, DW_AT_type(*DW$T$19)
	.dwattr DW$57, DW_AT_location[DW_OP_addr _cos_2F$25]
DW$58	.dwtag  DW_TAG_variable, DW_AT_name("cos_4D"), DW_AT_symbol_name("_cos_4D$26")
	.dwattr DW$58, DW_AT_type(*DW$T$19)
	.dwattr DW$58, DW_AT_location[DW_OP_addr _cos_4D$26]
DW$59	.dwtag  DW_TAG_variable, DW_AT_name("sin_lm2D"), DW_AT_symbol_name("_sin_lm2D$27")
	.dwattr DW$59, DW_AT_type(*DW$T$19)
	.dwattr DW$59, DW_AT_location[DW_OP_addr _sin_lm2D$27]
DW$60	.dwtag  DW_TAG_variable, DW_AT_name("cos_lm2D"), DW_AT_symbol_name("_cos_lm2D$28")
	.dwattr DW$60, DW_AT_type(*DW$T$19)
	.dwattr DW$60, DW_AT_location[DW_OP_addr _cos_lm2D$28]
DW$61	.dwtag  DW_TAG_variable, DW_AT_name("dtm1"), DW_AT_symbol_name("_dtm1$29")
	.dwattr DW$61, DW_AT_type(*DW$T$19)
	.dwattr DW$61, DW_AT_location[DW_OP_addr _dtm1$29]
DW$62	.dwtag  DW_TAG_variable, DW_AT_name("dtm2"), DW_AT_symbol_name("_dtm2$30")
	.dwattr DW$62, DW_AT_type(*DW$T$19)
	.dwattr DW$62, DW_AT_location[DW_OP_addr _dtm2$30]

;******************************************************************************
;* FUNCTION NAME: _MoonPos_GeoCenterEclFrame                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 108 Auto + 44 Save = 152 byte               *
;******************************************************************************
_MoonPos_GeoCenterEclFrame:
;** --------------------------------------------------------------------------*
DW$63	.dwtag  DW_TAG_formal_parameter, DW_AT_name("T"), DW_AT_symbol_name("_T")
	.dwattr DW$63, DW_AT_type(*DW$T$19)
	.dwattr DW$63, DW_AT_location[DW_OP_reg4]
DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Rm"), DW_AT_symbol_name("_Rm")
	.dwattr DW$64, DW_AT_type(*DW$T$34)
	.dwattr DW$64, DW_AT_location[DW_OP_reg20]
           MVKL    .S1     __mpyd,A3         ; |336| 
           MVKH    .S1     __mpyd,A3         ; |336| 

           ADDK    .S2     -152,SP           ; |326| 
||         MV      .L1X    SP,A31            ; |326| 

           CALL    .S2X    A3                ; |336| 

           STW     .D2T2   B4,*+SP(4)        ; |326| 
||         MVKL    .S2     0x403f4f5c,B5     ; |336| 

           STDW    .D1T1   A13:A12,*-A31(32)
||         STW     .D2T2   B3,*+SP(132)
||         MVKH    .S2     0x403f4f5c,B5     ; |336| 

           STW     .D2T1   A15,*+SP(152)
||         MVKL    .S2     0x28f5c28f,B4     ; |336| 

           MVKH    .S2     0x28f5c28f,B4     ; |336| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         STDW    .D2T2   B11:B10,*+SP(136)

           ADDKPC  .S2     RL398,B3,0        ; |336| 
||         MV      .L1     A5,A12            ; |326| 
||         MV      .S1     A4,A13            ; |326| 
||         STW     .D1T1   A14,*-A31(24)
||         STDW    .D2T2   B13:B12,*+SP(144)

RL398:     ; CALL OCCURS {__mpyd}            ; |336| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |336| 
           MVKH    .S1     __addd,A3         ; |336| 
           MVKL    .S2     0x41d99954,B5     ; |336| 
           CALL    .S2X    A3                ; |336| 
           MVKL    .S2     0x44a88312,B4     ; |336| 
           MVKH    .S2     0x41d99954,B5     ; |336| 
           MVKH    .S2     0x44a88312,B4     ; |336| 
           ADDKPC  .S2     RL399,B3,1        ; |336| 
RL399:     ; CALL OCCURS {__addd}            ; |336| 
           MVKL    .S1     __mpyd,A3         ; |336| 
           MVKH    .S1     __mpyd,A3         ; |336| 
           MV      .L2X    A13,B4            ; |336| 
           CALL    .S2X    A3                ; |336| 
           ADDKPC  .S2     RL400,B3,3        ; |336| 
           MV      .L2X    A12,B5            ; |336| 
RL400:     ; CALL OCCURS {__mpyd}            ; |336| 
           MVKL    .S1     __addd,A3         ; |336| 
           MVKH    .S1     __addd,A3         ; |336| 
           MVKL    .S2     0x411da7aa,B5     ; |336| 
           CALL    .S2X    A3                ; |336| 
           MVKL    .S2     0xee978d50,B4     ; |336| 
           MVKH    .S2     0x411da7aa,B5     ; |336| 
           MVKH    .S2     0xee978d50,B4     ; |336| 
           ADDKPC  .S2     RL401,B3,1        ; |336| 
RL401:     ; CALL OCCURS {__addd}            ; |336| 
           MVKL    .S1     __mpyd,A3         ; |336| 
           MVKH    .S1     __mpyd,A3         ; |336| 
           MVKL    .S2     0x3ed455a5,B5     ; |336| 
           CALL    .S2X    A3                ; |336| 
           MVKL    .S2     0xb2ff8f9d,B4     ; |336| 
           MVKH    .S2     0x3ed455a5,B5     ; |336| 
           MVKH    .S2     0xb2ff8f9d,B4     ; |336| 
           ADDKPC  .S2     RL402,B3,1        ; |336| 
RL402:     ; CALL OCCURS {__mpyd}            ; |336| 
           MVKL    .S1     __mpyd,A6         ; |337| 
           MVKH    .S1     __mpyd,A6         ; |337| 
           MVKL    .S2     0x3fe276c8,B5     ; |337| 
           CALL    .S2X    A6                ; |337| 
           MVKL    .S2     0xb4395810,B4     ; |337| 
           MVKL    .S1     _l$1,A3           ; |336| 
           MVKH    .S2     0x3fe276c8,B5     ; |337| 

           MVKH    .S1     _l$1,A3           ; |336| 
||         MV      .D1     A4,A10            ; |336| 
||         MV      .L1     A5,A11            ; |336| 
||         MVKH    .S2     0xb4395810,B4     ; |337| 

           MV      .S1     A13,A4            ; |337| 
||         MV      .L1     A12,A5            ; |337| 
||         STDW    .D1T1   A11:A10,*A3       ; |336| 
||         ADDKPC  .S2     RL413,B3,0        ; |337| 

RL413:     ; CALL OCCURS {__mpyd}            ; |337| 
           MVKL    .S1     __subd,A3         ; |337| 
           MVKH    .S1     __subd,A3         ; |337| 
           MV      .L2X    A4,B4             ; |337| 
           CALL    .S2X    A3                ; |337| 
           MVKL    .S1     0x94e56042,A4     ; |337| 
           MV      .L2X    A5,B5             ; |337| 
           MVKL    .S1     0x419ee5f2,A5     ; |337| 
           MVKH    .S1     0x94e56042,A4     ; |337| 

           MVKH    .S1     0x419ee5f2,A5     ; |337| 
||         ADDKPC  .S2     RL414,B3,0        ; |337| 

RL414:     ; CALL OCCURS {__subd}            ; |337| 
           MVKL    .S1     __mpyd,A3         ; |337| 
           MVKH    .S1     __mpyd,A3         ; |337| 
           MV      .L2X    A13,B4            ; |337| 
           CALL    .S2X    A3                ; |337| 
           MV      .L2X    A12,B5            ; |337| 
           ADDKPC  .S2     RL415,B3,3        ; |337| 
RL415:     ; CALL OCCURS {__mpyd}            ; |337| 
           MVKL    .S1     __addd,A3         ; |337| 
           MVKH    .S1     __addd,A3         ; |337| 
           MVKL    .S2     0x4133a3bb,B5     ; |337| 
           CALL    .S2X    A3                ; |337| 
           MVKL    .S2     0xcdd2f1aa,B4     ; |337| 
           MVKH    .S2     0x4133a3bb,B5     ; |337| 
           MVKH    .S2     0xcdd2f1aa,B4     ; |337| 
           ADDKPC  .S2     RL416,B3,1        ; |337| 
RL416:     ; CALL OCCURS {__addd}            ; |337| 
           MVKL    .S1     __mpyd,A3         ; |337| 
           MVKH    .S1     __mpyd,A3         ; |337| 
           MVKL    .S2     0x3ed455a5,B5     ; |337| 
           CALL    .S2X    A3                ; |337| 
           MVKL    .S2     0xb2ff8f9d,B4     ; |337| 
           MVKH    .S2     0x3ed455a5,B5     ; |337| 
           MVKH    .S2     0xb2ff8f9d,B4     ; |337| 
           ADDKPC  .S2     RL417,B3,1        ; |337| 
RL417:     ; CALL OCCURS {__mpyd}            ; |337| 
           MVKL    .S1     __mpyd,A8         ; |338| 
           MVKH    .S1     __mpyd,A8         ; |338| 
           MVKL    .S2     0x402a8395,B5     ; |338| 
           CALL    .S2X    A8                ; |338| 
           MVKL    .S2     0x810624dd,B4     ; |338| 
           MVKL    .S1     _l1$2,A3          ; |337| 
           MVKH    .S2     0x402a8395,B5     ; |338| 

           MVKH    .S1     _l1$2,A3          ; |337| 
||         MV      .D1     A4,A6             ; |337| 
||         MV      .L1     A5,A7             ; |337| 
||         MVKH    .S2     0x810624dd,B4     ; |338| 

           MV      .S1     A13,A4            ; |338| 
||         MV      .L1     A12,A5            ; |338| 
||         STDW    .D1T1   A7:A6,*A3         ; |337| 
||         ADDKPC  .S2     RL428,B3,0        ; |338| 

RL428:     ; CALL OCCURS {__mpyd}            ; |338| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |338| 
           MVKH    .S1     __subd,A3         ; |338| 
           MV      .L2X    A4,B4             ; |338| 
           CALL    .S2X    A3                ; |338| 
           MVKL    .S1     0x17c8c49c,A4     ; |338| 
           MV      .L2X    A5,B5             ; |338| 
           MVKL    .S1     0x41d9ebc5,A5     ; |338| 
           MVKH    .S1     0x17c8c49c,A4     ; |338| 

           ADDKPC  .S2     RL429,B3,0        ; |338| 
||         MVKH    .S1     0x41d9ebc5,A5     ; |338| 

RL429:     ; CALL OCCURS {__subd}            ; |338| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |338| 
           MVKH    .S1     __mpyd,A3         ; |338| 
           MV      .L2X    A13,B4            ; |338| 
           CALL    .S2X    A3                ; |338| 
           ADDKPC  .S2     RL430,B3,3        ; |338| 
           MV      .L2X    A12,B5            ; |338| 
RL430:     ; CALL OCCURS {__mpyd}            ; |338| 
           MVKL    .S1     __addd,A3         ; |338| 
           MVKH    .S1     __addd,A3         ; |338| 
           MVKL    .S2     0x41147e8b,B5     ; |338| 
           CALL    .S2X    A3                ; |338| 
           MVKL    .S2     0x820c49ba,B4     ; |338| 
           MVKH    .S2     0x41147e8b,B5     ; |338| 
           MVKH    .S2     0x820c49ba,B4     ; |338| 
           ADDKPC  .S2     RL431,B3,1        ; |338| 
RL431:     ; CALL OCCURS {__addd}            ; |338| 
           MVKL    .S1     __mpyd,A3         ; |338| 
           MVKH    .S1     __mpyd,A3         ; |338| 
           MVKL    .S2     0x3ed455a5,B5     ; |338| 
           CALL    .S2X    A3                ; |338| 
           MVKL    .S2     0xb2ff8f9d,B4     ; |338| 
           MVKH    .S2     0x3ed455a5,B5     ; |338| 
           MVKH    .S2     0xb2ff8f9d,B4     ; |338| 
           ADDKPC  .S2     RL432,B3,1        ; |338| 
RL432:     ; CALL OCCURS {__mpyd}            ; |338| 
           MVKL    .S1     __mpyd,A6         ; |339| 
           MVKH    .S1     __mpyd,A6         ; |339| 
           MVKL    .S2     0x401b9062,B5     ; |339| 
           CALL    .S2X    A6                ; |339| 
           MVKL    .S2     0x4dd2f1aa,B4     ; |339| 
           MVKL    .S1     _F$3,A3           ; |338| 

           MV      .L2X    A4,B10            ; |338| 
||         MVKH    .S2     0x401b9062,B5     ; |339| 

           MVKH    .S1     _F$3,A3           ; |338| 
||         MV      .L2X    A5,B11            ; |338| 
||         MVKH    .S2     0x4dd2f1aa,B4     ; |339| 

           MV      .S1     A13,A4            ; |339| 
||         MV      .L1     A12,A5            ; |339| 
||         STDW    .D1T2   B11:B10,*A3       ; |338| 
||         ADDKPC  .S2     RL443,B3,0        ; |339| 

RL443:     ; CALL OCCURS {__mpyd}            ; |339| 
           MVKL    .S1     __subd,A3         ; |339| 
           MVKH    .S1     __subd,A3         ; |339| 
           MV      .L2X    A4,B4             ; |339| 
           CALL    .S2X    A3                ; |339| 
           MVKL    .S1     0x3054fdf4,A4     ; |339| 
           MV      .L2X    A5,B5             ; |339| 
           MVKL    .S1     0x41d7e2d0,A5     ; |339| 
           MVKH    .S1     0x3054fdf4,A4     ; |339| 

           MVKH    .S1     0x41d7e2d0,A5     ; |339| 
||         ADDKPC  .S2     RL444,B3,0        ; |339| 

RL444:     ; CALL OCCURS {__subd}            ; |339| 
           MVKL    .S1     __mpyd,A3         ; |339| 
           MVKH    .S1     __mpyd,A3         ; |339| 
           MV      .L2X    A13,B4            ; |339| 
           CALL    .S2X    A3                ; |339| 
           MV      .L2X    A12,B5            ; |339| 
           ADDKPC  .S2     RL445,B3,3        ; |339| 
RL445:     ; CALL OCCURS {__mpyd}            ; |339| 
           MVKL    .S1     __addd,A3         ; |339| 
           MVKH    .S1     __addd,A3         ; |339| 
           MVKL    .S2     0x41305c85,B5     ; |339| 
           CALL    .S2X    A3                ; |339| 
           MVKL    .S2     0x4e978d50,B4     ; |339| 
           MVKH    .S2     0x41305c85,B5     ; |339| 
           MVKH    .S2     0x4e978d50,B4     ; |339| 
           ADDKPC  .S2     RL446,B3,1        ; |339| 
RL446:     ; CALL OCCURS {__addd}            ; |339| 
           MVKL    .S1     __mpyd,A3         ; |339| 
           MVKH    .S1     __mpyd,A3         ; |339| 
           MVKL    .S2     0x3ed455a5,B5     ; |339| 
           CALL    .S2X    A3                ; |339| 
           MVKL    .S2     0xb2ff8f9d,B4     ; |339| 
           MVKH    .S2     0x3ed455a5,B5     ; |339| 
           MVKH    .S2     0xb2ff8f9d,B4     ; |339| 
           ADDKPC  .S2     RL447,B3,1        ; |339| 
RL447:     ; CALL OCCURS {__mpyd}            ; |339| 
           MVKL    .S1     __mpyd,A8         ; |340| 
           MVKH    .S1     __mpyd,A8         ; |340| 
           MVKL    .S2     0x401dd1eb,B5     ; |340| 
           CALL    .S2X    A8                ; |340| 
           MVKL    .S2     0x851eb852,B4     ; |340| 
           MVKL    .S1     _D$4,A3           ; |339| 
           MVKH    .S2     0x401dd1eb,B5     ; |340| 

           MVKH    .S1     _D$4,A3           ; |339| 
||         MV      .D1     A4,A6             ; |339| 
||         MV      .L1     A5,A7             ; |339| 
||         MVKH    .S2     0x851eb852,B4     ; |340| 

           MV      .S1     A13,A4            ; |340| 
||         MV      .L1     A12,A5            ; |340| 
||         STDW    .D1T1   A7:A6,*A3         ; |339| 
||         ADDKPC  .S2     RL458,B3,0        ; |340| 

RL458:     ; CALL OCCURS {__mpyd}            ; |340| 
           MVKL    .S1     __addd,A3         ; |340| 
           MVKH    .S1     __addd,A3         ; |340| 
           MVKL    .S2     0x415a8fb2,B5     ; |340| 
           CALL    .S2X    A3                ; |340| 
           MVKL    .S2     0xa27ef9db,B4     ; |340| 
           MVKH    .S2     0x415a8fb2,B5     ; |340| 
           MVKH    .S2     0xa27ef9db,B4     ; |340| 
           ADDKPC  .S2     RL459,B3,1        ; |340| 
RL459:     ; CALL OCCURS {__addd}            ; |340| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B6         ; |340| 
           MVKH    .S2     __mpyd,B6         ; |340| 
           CALL    .S2     B6                ; |340| 
           MV      .L2X    A13,B4            ; |340| 
           ADDKPC  .S2     RL460,B3,2        ; |340| 
           MV      .L2X    A12,B5            ; |340| 
RL460:     ; CALL OCCURS {__mpyd}            ; |340| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |340| 
           MVKH    .S1     __subd,A3         ; |340| 
           MV      .L2X    A4,B4             ; |340| 
           CALL    .S2X    A3                ; |340| 
           MVKL    .S1     0x1eb851ec,A4     ; |340| 
           MV      .L2X    A5,B5             ; |340| 
           MVKL    .S1     0x411b79c1,A5     ; |340| 
           MVKH    .S1     0x1eb851ec,A4     ; |340| 

           ADDKPC  .S2     RL461,B3,0        ; |340| 
||         MVKH    .S1     0x411b79c1,A5     ; |340| 

RL461:     ; CALL OCCURS {__subd}            ; |340| 
           MVKL    .S1     __mpyd,A3         ; |340| 
           MVKH    .S1     __mpyd,A3         ; |340| 
           MVKL    .S2     0x3ed455a5,B5     ; |340| 
           CALL    .S2X    A3                ; |340| 
           MVKL    .S2     0xb2ff8f9d,B4     ; |340| 
           MVKH    .S2     0x3ed455a5,B5     ; |340| 
           MVKH    .S2     0xb2ff8f9d,B4     ; |340| 
           ADDKPC  .S2     RL462,B3,1        ; |340| 
RL462:     ; CALL OCCURS {__mpyd}            ; |340| 
           MVKL    .S1     __addd,A6         ; |343| 
           MVKH    .S1     __addd,A6         ; |343| 
           MVKL    .S1     _OM$5,A3          ; |340| 
           CALL    .S2X    A6                ; |343| 
           ADDKPC  .S2     RL464,B3,0        ; |343| 
           MV      .L2     B11,B5            ; |343| 
           MV      .D2     B10,B4            ; |343| 
           MVKH    .S1     _OM$5,A3          ; |340| 
           STDW    .D1T1   A5:A4,*A3         ; |340| 
RL464:     ; CALL OCCURS {__addd}            ; |343| 
           MVKL    .S1     __mpyd,A3         ; |343| 
           MVKH    .S1     __mpyd,A3         ; |343| 
           MVKL    .S2     0x404ca5dc,B5     ; |343| 
           CALL    .S2X    A3                ; |343| 
           MVKL    .S2     0x1a63c1f8,B4     ; |343| 
           MVKH    .S2     0x404ca5dc,B5     ; |343| 
           MVKH    .S2     0x1a63c1f8,B4     ; |343| 
           ADDKPC  .S2     RL465,B3,1        ; |343| 
RL465:     ; CALL OCCURS {__mpyd}            ; |343| 
           MVKL    .S1     __fixdu,A3        ; |343| 
           MVKH    .S1     __fixdu,A3        ; |343| 
           MV      .L1     A4,A14            ; |343| 
           CALL    .S2X    A3                ; |343| 
           ADDKPC  .S2     RL466,B3,3        ; |343| 
           MV      .S1     A5,A13            ; |343| 
RL466:     ; CALL OCCURS {__fixdu}           ; |343| 
           MVKL    .S2     __fltud,B4        ; |343| 
           MVKH    .S2     __fltud,B4        ; |343| 
           CALL    .S2     B4                ; |343| 
           MV      .L1     A4,A12            ; |343| 
           ADDKPC  .S2     RL472,B3,3        ; |343| 
RL472:     ; CALL OCCURS {__fltud}           ; |343| 
           MVKL    .S2     __subd,B6         ; |343| 
           MVKH    .S2     __subd,B6         ; |343| 
           CALL    .S2     B6                ; |343| 
           MV      .L2X    A4,B4             ; |343| 
           MV      .L2X    A5,B5             ; |343| 
           MV      .S1     A14,A4            ; |343| 
           ADDKPC  .S2     RL473,B3,0        ; |343| 
           MV      .L1     A13,A5            ; |343| 
RL473:     ; CALL OCCURS {__subd}            ; |343| 
           MVKL    .S1     __remu,A3         ; |343| 
           MVKH    .S1     __remu,A3         ; |343| 
           MVK     .S2     0x168,B4          ; |343| 
           CALL    .S2X    A3                ; |343| 
           MV      .D1     A4,A13            ; |343| 
           ADDKPC  .S2     RL474,B3,1        ; |343| 
           MV      .S1     A12,A4            ; |343| 
           MV      .L1     A5,A12            ; |343| 
RL474:     ; CALL OCCURS {__remu}            ; |343| 
           MVKL    .S1     __fltud,A3        ; |343| 
           MVKH    .S1     __fltud,A3        ; |343| 
           NOP             1
           CALL    .S2X    A3                ; |343| 
           ADDKPC  .S2     RL475,B3,4        ; |343| 
RL475:     ; CALL OCCURS {__fltud}           ; |343| 
           MVKL    .S1     __addd,A3         ; |343| 
           MVKH    .S1     __addd,A3         ; |343| 
           MV      .L2X    A5,B5             ; |343| 
           CALL    .S2X    A3                ; |343| 
           MV      .L2X    A4,B4             ; |343| 
           MV      .S1     A12,A5            ; |343| 
           ADDKPC  .S2     RL476,B3,1        ; |343| 
           MV      .L1     A13,A4            ; |343| 
RL476:     ; CALL OCCURS {__addd}            ; |343| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _sin,A3           ; |346| 
           MVKH    .S1     _sin,A3           ; |346| 
           MV      .L2X    A4,B4             ; |343| 
           CALL    .S2X    A3                ; |346| 
           MV      .L2X    A5,B5             ; |343| 
           ADDKPC  .S2     RL477,B3,0        ; |346| 
           MVKL    .S1     _L$6,A6           ; |343| 
           MVKH    .S1     _L$6,A6           ; |343| 

           STDW    .D1T2   B5:B4,*A6         ; |343| 
||         MV      .L1     A11,A5            ; |346| 
||         MV      .S1     A10,A4            ; |346| 

RL477:     ; CALL OCCURS {_sin}              ; |346| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     _cos,B5           ; |346| 
           MVKL    .S2     _l$1,B6           ; |346| 
           MVKH    .S2     _cos,B5           ; |346| 
           MVKH    .S2     _l$1,B6           ; |346| 
           CALL    .S2     B5                ; |346| 

           LDDW    .D2T1   *B6,A5:A4         ; |346| 
||         MV      .L1     A5,A7             ; |346| 
||         MV      .S1     A4,A6             ; |346| 

           MVKL    .S2     _sin_l$11,B4      ; |346| 
           MVKH    .S2     _sin_l$11,B4      ; |346| 
           ADDKPC  .S2     RL478,B3,0        ; |346| 
           STDW    .D2T1   A7:A6,*B4         ; |346| 
RL478:     ; CALL OCCURS {_cos}              ; |346| 
           MVKL    .S1     _sin,A8           ; |347| 

           MVKH    .S1     _sin,A8           ; |347| 
||         MVKL    .S2     _l1$2,B4          ; |347| 

           MVKH    .S2     _l1$2,B4          ; |347| 
           CALL    .S2X    A8                ; |347| 

           LDDW    .D2T1   *B4,A5:A4         ; |347| 
||         MV      .L1     A5,A7             ; |346| 
||         MV      .S1     A4,A6             ; |346| 

           MVKL    .S1     _cos_l$19,A3      ; |346| 
           MVKH    .S1     _cos_l$19,A3      ; |346| 
           ADDKPC  .S2     RL479,B3,0        ; |347| 
           STDW    .D1T1   A7:A6,*A3         ; |346| 
RL479:     ; CALL OCCURS {_sin}              ; |347| 
           MVKL    .S1     _cos,A8           ; |347| 

           MVKH    .S1     _cos,A8           ; |347| 
||         MVKL    .S2     _l1$2,B4          ; |347| 

           MVKH    .S2     _l1$2,B4          ; |347| 
           CALL    .S2X    A8                ; |347| 

           LDDW    .D2T1   *B4,A5:A4         ; |347| 
||         MV      .L1     A5,A7             ; |347| 
||         MV      .S1     A4,A6             ; |347| 

           MVKL    .S1     _sin_l1$12,A3     ; |347| 
           MVKH    .S1     _sin_l1$12,A3     ; |347| 
           ADDKPC  .S2     RL480,B3,0        ; |347| 
           STDW    .D1T1   A7:A6,*A3         ; |347| 
RL480:     ; CALL OCCURS {_cos}              ; |347| 
           MVKL    .S1     _sin,A8           ; |348| 

           MVKH    .S1     _sin,A8           ; |348| 
||         MVKL    .S2     _D$4,B4           ; |348| 

           MVKH    .S2     _D$4,B4           ; |348| 
           CALL    .S2X    A8                ; |348| 

           LDDW    .D2T1   *B4,A5:A4         ; |348| 
||         MV      .L1     A5,A7             ; |347| 
||         MV      .S1     A4,A6             ; |347| 

           MVKL    .S1     _cos_l1$20,A3     ; |347| 
           MVKH    .S1     _cos_l1$20,A3     ; |347| 
           ADDKPC  .S2     RL481,B3,0        ; |348| 
           STDW    .D1T1   A7:A6,*A3         ; |347| 
RL481:     ; CALL OCCURS {_sin}              ; |348| 
           MVKL    .S2     _cos,B4           ; |348| 
           MVKL    .S2     _D$4,B5           ; |348| 
           MVKH    .S2     _cos,B4           ; |348| 
           MVKH    .S2     _D$4,B5           ; |348| 
           CALL    .S2     B4                ; |348| 

           LDDW    .D2T1   *B5,A5:A4         ; |348| 
||         MV      .L1     A5,A7             ; |348| 
||         MV      .S1     A4,A6             ; |348| 

           MVKL    .S1     _sin_D$13,A3      ; |348| 
           MVKH    .S1     _sin_D$13,A3      ; |348| 
           ADDKPC  .S2     RL482,B3,0        ; |348| 
           STDW    .D1T1   A7:A6,*A3         ; |348| 
RL482:     ; CALL OCCURS {_cos}              ; |348| 
           MVKL    .S1     _sin,A3           ; |349| 
           MVKH    .S1     _sin,A3           ; |349| 
           MVKL    .S1     _F$3,A8           ; |349| 

           CALL    .S2X    A3                ; |349| 
||         MVKH    .S1     _F$3,A8           ; |349| 

           LDDW    .D1T1   *A8,A5:A4         ; |349| 
||         MV      .L1     A5,A7             ; |348| 
||         MV      .S1     A4,A6             ; |348| 

           MVKL    .S1     _cos_D$21,A9      ; |348| 
           MVKH    .S1     _cos_D$21,A9      ; |348| 
           ADDKPC  .S2     RL483,B3,0        ; |349| 
           STDW    .D1T1   A7:A6,*A9         ; |348| 
RL483:     ; CALL OCCURS {_sin}              ; |349| 
           MVKL    .S1     _cos,A3           ; |349| 

           MVKH    .S1     _cos,A3           ; |349| 
||         MVKL    .S2     _F$3,B4           ; |349| 

           MVKH    .S2     _F$3,B4           ; |349| 
           CALL    .S2X    A3                ; |349| 

           LDDW    .D2T1   *B4,A5:A4         ; |349| 
||         MV      .L1     A5,A7             ; |349| 
||         MV      .S1     A4,A6             ; |349| 

           MVKL    .S2     _sin_F$14,B5      ; |349| 
           MVKH    .S2     _sin_F$14,B5      ; |349| 
           ADDKPC  .S2     RL484,B3,0        ; |349| 
           STDW    .D2T1   A7:A6,*B5         ; |349| 
RL484:     ; CALL OCCURS {_cos}              ; |349| 
           MVKL    .S1     __addd,A3         ; |350| 

           MVKH    .S1     __addd,A3         ; |350| 
||         MVKL    .S2     _sin_l$11,B6      ; |350| 

           MVKH    .S2     _sin_l$11,B6      ; |350| 
||         MV      .L2X    A4,B8             ; |350| 
||         STW     .D2T1   A4,*+SP(12)       ; |349| 
||         MVKL    .S1     _sin_l$11,A6      ; |350| 

           CALL    .S2X    A3                ; |350| 
||         MVKH    .S1     _sin_l$11,A6      ; |350| 
||         STW     .D2T1   A5,*+SP(8)        ; |349| 

           LDDW    .D2T1   *B6,A5:A4         ; |350| 
||         MV      .L2X    A5,B9             ; |350| 
||         LDDW    .D1T2   *A6,B5:B4         ; |350| 

           MVKL    .S1     _cos_F$22,A7      ; |349| 
           MVKH    .S1     _cos_F$22,A7      ; |349| 
           ADDKPC  .S2     RL486,B3,0        ; |350| 
           STDW    .D1T2   B9:B8,*A7         ; |349| 
RL486:     ; CALL OCCURS {__addd}            ; |350| 
           MVKL    .S2     __mpyd,B6         ; |350| 

           MVKH    .S2     __mpyd,B6         ; |350| 
||         MVKL    .S1     _cos_l$19,A3      ; |350| 

           CALL    .S2     B6                ; |350| 
||         MVKH    .S1     _cos_l$19,A3      ; |350| 

           LDDW    .D1T2   *A3,B5:B4         ; |350| 
           ADDKPC  .S2     RL487,B3,3        ; |350| 
RL487:     ; CALL OCCURS {__mpyd}            ; |350| 
           MVKL    .S1     __addd,A3         ; |350| 

           MVKH    .S1     __addd,A3         ; |350| 
||         MVKL    .S2     _cos_l$19,B4      ; |350| 

           MVKH    .S2     _cos_l$19,B4      ; |350| 
||         MVKL    .S1     _cos_l$19,A8      ; |350| 

           CALL    .S2X    A3                ; |350| 
||         MVKH    .S1     _cos_l$19,A8      ; |350| 

           LDDW    .D1T1   *A8,A5:A4         ; |350| 
||         LDDW    .D2T2   *B4,B5:B4         ; |350| 
||         MV      .L1     A5,A7             ; |350| 
||         MV      .S1     A4,A6             ; |350| 

           STW     .D2T1   A7,*+SP(16)       ; |350| 
           MVKL    .S2     _sin_2l$15,B6     ; |350| 

           STW     .D2T1   A6,*+SP(20)       ; |350| 
||         MVKH    .S2     _sin_2l$15,B6     ; |350| 

           ADDKPC  .S2     RL491,B3,0        ; |350| 
||         STDW    .D2T1   A7:A6,*B6         ; |350| 

RL491:     ; CALL OCCURS {__addd}            ; |350| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |350| 

           MVKH    .S1     __mpyd,A3         ; |350| 
||         MVKL    .S2     _cos_l$19,B4      ; |350| 

           MVKH    .S2     _cos_l$19,B4      ; |350| 
           CALL    .S2X    A3                ; |350| 
           LDDW    .D2T2   *B4,B5:B4         ; |350| 
           ADDKPC  .S2     RL492,B3,3        ; |350| 
RL492:     ; CALL OCCURS {__mpyd}            ; |350| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |350| 
           MVKH    .S1     __subd,A3         ; |350| 
           ZERO    .L2     B5                ; |350| 
           CALL    .S2X    A3                ; |350| 
           MVKH    .S2     0x3ff00000,B5     ; |350| 
           ADDKPC  .S2     RL493,B3,2        ; |350| 
           ZERO    .L2     B4                ; |350| 
RL493:     ; CALL OCCURS {__subd}            ; |350| 
           MVKL    .S2     __addd,B6         ; |351| 
           MVKL    .S2     _sin_D$13,B4      ; |351| 
           MVKH    .S2     __addd,B6         ; |351| 

           MVKL    .S1     _sin_D$13,A8      ; |351| 
||         MVKH    .S2     _sin_D$13,B4      ; |351| 

           MVKH    .S1     _sin_D$13,A8      ; |351| 
||         CALL    .S2     B6                ; |351| 

           LDDW    .D1T2   *A8,B5:B4         ; |351| 
||         LDDW    .D2T1   *B4,A5:A4         ; |351| 
||         MV      .S1     A4,A6             ; |350| 
||         MV      .L1     A5,A7             ; |350| 

           MVKL    .S1     _cos_2l$23,A3     ; |350| 
           STW     .D2T1   A7,*+SP(24)       ; |350| 

           MVKH    .S1     _cos_2l$23,A3     ; |350| 
||         STW     .D2T1   A6,*+SP(28)       ; |350| 

           STDW    .D1T1   A7:A6,*A3         ; |350| 
||         ADDKPC  .S2     RL495,B3,0        ; |351| 

RL495:     ; CALL OCCURS {__addd}            ; |351| 
           MVKL    .S1     __mpyd,A3         ; |351| 
           MVKH    .S1     __mpyd,A3         ; |351| 
           MVKL    .S1     _cos_D$21,A6      ; |351| 

           MVKH    .S1     _cos_D$21,A6      ; |351| 
||         CALL    .S2X    A3                ; |351| 

           LDDW    .D1T2   *A6,B5:B4         ; |351| 
           ADDKPC  .S2     RL496,B3,3        ; |351| 
RL496:     ; CALL OCCURS {__mpyd}            ; |351| 
           MVKL    .S1     __addd,A3         ; |351| 

           MVKL    .S2     _cos_D$21,B4      ; |351| 
||         MVKH    .S1     __addd,A3         ; |351| 

           MVKL    .S1     _cos_D$21,A6      ; |351| 
||         MVKH    .S2     _cos_D$21,B4      ; |351| 
||         MV      .L2X    A4,B6             ; |351| 

           MVKH    .S1     _cos_D$21,A6      ; |351| 
||         CALL    .S2X    A3                ; |351| 

           LDDW    .D1T2   *A6,B5:B4         ; |351| 
||         LDDW    .D2T1   *B4,A5:A4         ; |351| 
||         MV      .L2X    A5,B7             ; |351| 

           MVKL    .S2     _sin_2D$16,B8     ; |351| 
           STW     .D2T2   B6,*+SP(36)       ; |351| 

           MVKH    .S2     _sin_2D$16,B8     ; |351| 
||         STW     .D2T2   B7,*+SP(32)       ; |351| 

           STDW    .D2T2   B7:B6,*B8         ; |351| 
||         ADDKPC  .S2     RL500,B3,0        ; |351| 

RL500:     ; CALL OCCURS {__addd}            ; |351| 
           MVKL    .S1     __mpyd,A3         ; |351| 
           MVKH    .S1     __mpyd,A3         ; |351| 
           MVKL    .S1     _cos_D$21,A6      ; |351| 

           MVKH    .S1     _cos_D$21,A6      ; |351| 
||         CALL    .S2X    A3                ; |351| 

           LDDW    .D1T2   *A6,B5:B4         ; |351| 
           ADDKPC  .S2     RL501,B3,3        ; |351| 
RL501:     ; CALL OCCURS {__mpyd}            ; |351| 
           MVKL    .S2     __subd,B6         ; |351| 
           MVKH    .S2     __subd,B6         ; |351| 
           CALL    .S2     B6                ; |351| 
           ZERO    .L2     B5                ; |351| 
           MVKH    .S2     0x3ff00000,B5     ; |351| 
           ZERO    .L2     B4                ; |351| 
           ADDKPC  .S2     RL502,B3,1        ; |351| 
RL502:     ; CALL OCCURS {__subd}            ; |351| 
           MVKL    .S1     __addd,A3         ; |352| 

           MVKL    .S2     _sin_F$14,B4      ; |352| 
||         MVKH    .S1     __addd,A3         ; |352| 

           MVKL    .S1     _sin_F$14,A6      ; |352| 
||         MVKH    .S2     _sin_F$14,B4      ; |352| 
||         MV      .L2X    A4,B10            ; |351| 

           MVKH    .S1     _sin_F$14,A6      ; |352| 
||         CALL    .S2X    A3                ; |352| 

           LDDW    .D1T2   *A6,B5:B4         ; |352| 
||         LDDW    .D2T1   *B4,A5:A4         ; |352| 
||         MV      .L2X    A5,B11            ; |351| 

           MVKL    .S2     _cos_2D$24,B6     ; |351| 
           MVKH    .S2     _cos_2D$24,B6     ; |351| 
           STDW    .D2T2   B11:B10,*B6       ; |351| 
           ADDKPC  .S2     RL504,B3,0        ; |352| 
RL504:     ; CALL OCCURS {__addd}            ; |352| 
           MVKL    .S1     __mpyd,A3         ; |352| 
           MVKH    .S1     __mpyd,A3         ; |352| 
           LDW     .D2T1   *+SP(12),A11      ; |352| 

           LDW     .D2T1   *+SP(8),A10       ; |352| 
||         CALL    .S2X    A3                ; |352| 

           ADDKPC  .S2     RL505,B3,2        ; |352| 
           MV      .L2X    A11,B4            ; |352| 
           MV      .L2X    A10,B5            ; |352| 
RL505:     ; CALL OCCURS {__mpyd}            ; |352| 
           MVKL    .S2     __addd,B6         ; |352| 
           MVKH    .S2     __addd,B6         ; |352| 
           CALL    .S2     B6                ; |352| 
           MV      .L1     A4,A6             ; |352| 
           MVKL    .S1     _sin_2F$17,A3     ; |352| 

           MV      .L1     A5,A7             ; |352| 
||         STW     .D2T1   A6,*+SP(44)       ; |352| 

           MVKH    .S1     _sin_2F$17,A3     ; |352| 
||         MV      .L2X    A11,B4            ; |352| 
||         STW     .D2T1   A7,*+SP(40)       ; |352| 

           MV      .L2X    A10,B5            ; |352| 
||         MV      .S1     A11,A4            ; |352| 
||         STDW    .D1T1   A7:A6,*A3         ; |352| 
||         MV      .L1     A10,A5            ; |352| 
||         ADDKPC  .S2     RL509,B3,0        ; |352| 

RL509:     ; CALL OCCURS {__addd}            ; |352| 
           MVKL    .S1     __mpyd,A3         ; |352| 
           MVKH    .S1     __mpyd,A3         ; |352| 
           MV      .L2X    A11,B4            ; |352| 
           CALL    .S2X    A3                ; |352| 
           MV      .L2X    A10,B5            ; |352| 
           ADDKPC  .S2     RL510,B3,3        ; |352| 
RL510:     ; CALL OCCURS {__mpyd}            ; |352| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __subd,B6         ; |352| 
           MVKH    .S2     __subd,B6         ; |352| 
           CALL    .S2     B6                ; |352| 
           ZERO    .L2     B5                ; |352| 
           MVKH    .S2     0x3ff00000,B5     ; |352| 
           ADDKPC  .S2     RL511,B3,1        ; |352| 
           ZERO    .L2     B4                ; |352| 
RL511:     ; CALL OCCURS {__subd}            ; |352| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *+SP(36),B4       ; |353| 
||         MVKL    .S1     __addd,A3         ; |353| 

           MVKH    .S1     __addd,A3         ; |353| 
||         LDW     .D2T2   *+SP(32),B5       ; |352| 

           MV      .L2X    A4,B6             ; |352| 
           CALL    .S2X    A3                ; |353| 
           STW     .D2T2   B6,*+SP(52)       ; |352| 
           MV      .L1X    B4,A11            ; |353| 

           MV      .L1X    B5,A10            ; |353| 
||         MVKL    .S1     _cos_2F$25,A6     ; |352| 
||         MV      .L2X    A5,B7             ; |352| 

           MV      .L1X    B4,A4             ; |353| 
||         STW     .D2T2   B7,*+SP(48)       ; |352| 
||         MVKH    .S1     _cos_2F$25,A6     ; |352| 

           ADDKPC  .S2     RL513,B3,0        ; |353| 
||         STDW    .D1T2   B7:B6,*A6         ; |352| 
||         MV      .L1X    B5,A5             ; |353| 

RL513:     ; CALL OCCURS {__addd}            ; |353| 
           MVKL    .S1     __mpyd,A3         ; |353| 
           MVKH    .S1     __mpyd,A3         ; |353| 
           MV      .L2     B11,B5            ; |353| 
           CALL    .S2X    A3                ; |353| 
           ADDKPC  .S2     RL514,B3,3        ; |353| 
           MV      .D2     B10,B4            ; |353| 
RL514:     ; CALL OCCURS {__mpyd}            ; |353| 
           MVKL    .S1     __addd,A3         ; |353| 
           MVKH    .S1     __addd,A3         ; |353| 
           MV      .L2X    A4,B6             ; |353| 
           CALL    .S2X    A3                ; |353| 
           MVKL    .S1     _sin_4D$18,A6     ; |353| 
           MV      .L2X    A5,B7             ; |353| 
           STW     .D2T2   B6,*+SP(60)       ; |353| 

           MVKH    .S1     _sin_4D$18,A6     ; |353| 
||         STW     .D2T2   B7,*+SP(56)       ; |353| 
||         MV      .L1X    B10,A4            ; |353| 

           MV      .D2     B10,B4            ; |353| 
||         MV      .L2     B11,B5            ; |353| 
||         MV      .L1X    B11,A5            ; |353| 
||         STDW    .D1T2   B7:B6,*A6         ; |353| 
||         ADDKPC  .S2     RL518,B3,0        ; |353| 

RL518:     ; CALL OCCURS {__addd}            ; |353| 
           MVKL    .S1     __mpyd,A3         ; |353| 
           MVKH    .S1     __mpyd,A3         ; |353| 
           MV      .D2     B10,B4            ; |353| 
           CALL    .S2X    A3                ; |353| 
           MV      .L2     B11,B5            ; |353| 
           ADDKPC  .S2     RL519,B3,3        ; |353| 
RL519:     ; CALL OCCURS {__mpyd}            ; |353| 
           MVKL    .S1     __subd,A3         ; |353| 
           MVKH    .S1     __subd,A3         ; |353| 
           ZERO    .L2     B5                ; |353| 
           CALL    .S2X    A3                ; |353| 
           MVKH    .S2     0x3ff00000,B5     ; |353| 
           ZERO    .L2     B4                ; |353| 
           ADDKPC  .S2     RL520,B3,2        ; |353| 
RL520:     ; CALL OCCURS {__subd}            ; |353| 
           MVKL    .S1     __mpyd,A6         ; |354| 
           MVKH    .S1     __mpyd,A6         ; |354| 

           MVKL    .S1     _cos_l$19,A7      ; |354| 
||         MV      .L2X    A4,B6             ; |353| 

           MVKH    .S1     _cos_l$19,A7      ; |354| 
||         CALL    .S2X    A6                ; |354| 

           LDDW    .D1T1   *A7,A5:A4         ; |354| 
||         MV      .L2X    A5,B7             ; |353| 

           MVKL    .S1     _cos_4D$26,A3     ; |353| 
           STW     .D2T2   B7,*+SP(64)       ; |353| 

           MVKH    .S1     _cos_4D$26,A3     ; |353| 
||         MV      .L2X    A11,B4            ; |354| 
||         STW     .D2T2   B6,*+SP(68)       ; |353| 

           MV      .L2X    A10,B5            ; |354| 
||         STDW    .D1T2   B7:B6,*A3         ; |353| 
||         ADDKPC  .S2     RL521,B3,0        ; |354| 

RL521:     ; CALL OCCURS {__mpyd}            ; |354| 
           MVKL    .S1     __mpyd,A3         ; |354| 

           MVKL    .S2     _sin_l$11,B4      ; |354| 
||         MVKH    .S1     __mpyd,A3         ; |354| 

           MVKH    .S2     _sin_l$11,B4      ; |354| 
           CALL    .S2X    A3                ; |354| 

           LDDW    .D2T1   *B4,A5:A4         ; |354| 
||         MV      .S1     A4,A13            ; |354| 
||         MV      .L1     A5,A12            ; |354| 

           MV      .D2     B11,B5            ; |354| 
           MV      .L2     B10,B4            ; |354| 
           ADDKPC  .S2     RL522,B3,1        ; |354| 
RL522:     ; CALL OCCURS {__mpyd}            ; |354| 
           MVKL    .S2     __subd,B6         ; |354| 
           MVKH    .S2     __subd,B6         ; |354| 
           CALL    .S2     B6                ; |354| 
           STW     .D2T1   A4,*+SP(76)       ; |354| 
           STW     .D2T1   A5,*+SP(72)       ; |354| 
           MV      .L2X    A12,B5            ; |354| 
           MV      .L2X    A13,B4            ; |354| 
           ADDKPC  .S2     RL523,B3,0        ; |354| 
RL523:     ; CALL OCCURS {__subd}            ; |354| 
           MVKL    .S2     __mpyd,B8         ; |355| 
           MVKL    .S2     _cos_l$19,B9      ; |355| 
           MVKH    .S2     __mpyd,B8         ; |355| 

           STW     .D2T1   A4,*+SP(84)       ; |354| 
||         MVKH    .S2     _cos_l$19,B9      ; |355| 

           STW     .D2T1   A5,*+SP(80)       ; |354| 
||         MV      .L2X    A4,B6             ; |355| 
||         CALL    .S2     B8                ; |355| 

           LDDW    .D2T1   *B9,A5:A4         ; |355| 
||         MV      .L2X    A5,B7             ; |355| 

           MVKL    .S1     _sin_lm2D$27,A3   ; |354| 
           MVKH    .S1     _sin_lm2D$27,A3   ; |354| 
           MV      .D2     B10,B4            ; |355| 

           MV      .L2     B11,B5            ; |355| 
||         STDW    .D1T2   B7:B6,*A3         ; |354| 
||         ADDKPC  .S2     RL524,B3,0        ; |355| 

RL524:     ; CALL OCCURS {__mpyd}            ; |355| 
           MVKL    .S2     __mpyd,B6         ; |355| 

           MVKL    .S1     _sin_l$11,A3      ; |355| 
||         MVKH    .S2     __mpyd,B6         ; |355| 
||         STW     .D2T1   A4,*+SP(92)       ; |355| 

           STW     .D2T1   A5,*+SP(88)       ; |355| 
||         MVKH    .S1     _sin_l$11,A3      ; |355| 
||         CALL    .S2     B6                ; |355| 

           LDDW    .D1T1   *A3,A5:A4         ; |355| 
           MV      .L2X    A11,B4            ; |355| 
           MV      .L2X    A10,B5            ; |355| 
           ADDKPC  .S2     RL526,B3,1        ; |355| 
RL526:     ; CALL OCCURS {__mpyd}            ; |355| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |355| 
           MVKH    .S1     __addd,A3         ; |355| 
           MV      .L2X    A5,B5             ; |355| 

           CALL    .S2X    A3                ; |355| 
||         LDW     .D2T1   *+SP(88),A5       ; |355| 

           LDW     .D2T1   *+SP(92),A4       ; |355| 
||         MV      .L2X    A4,B4             ; |355| 

           ADDKPC  .S2     RL527,B3,3        ; |355| 
RL527:     ; CALL OCCURS {__addd}            ; |355| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKH    .S1     __mpyd,A3         ; |359| 
||         MVKL    .S2     _cos_l$19,B4      ; |359| 

           MVKH    .S2     _cos_l$19,B4      ; |359| 
||         MV      .L2X    A4,B6             ; |355| 
||         MVKL    .S1     _sin_l1$12,A7     ; |359| 

           CALL    .S2X    A3                ; |359| 
||         MVKH    .S1     _sin_l1$12,A7     ; |359| 

           LDDW    .D1T1   *A7,A5:A4         ; |359| 
||         LDDW    .D2T2   *B4,B5:B4         ; |359| 
||         MV      .L2X    A5,B7             ; |355| 

           STW     .D2T2   B7,*+SP(96)       ; |355| 
           MVKL    .S1     _cos_lm2D$28,A6   ; |355| 

           STW     .D2T2   B6,*+SP(100)      ; |355| 
||         MVKH    .S1     _cos_lm2D$28,A6   ; |355| 

           ADDKPC  .S2     RL528,B3,0        ; |359| 
||         STDW    .D1T2   B7:B6,*A6         ; |355| 

RL528:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKH    .S1     __mpyd,A3         ; |359| 
||         MVKL    .S2     _cos_l1$20,B4     ; |359| 

           MVKH    .S2     _cos_l1$20,B4     ; |359| 
||         MVKL    .S1     _sin_l$11,A6      ; |359| 

           CALL    .S2X    A3                ; |359| 
||         MVKH    .S1     _sin_l$11,A6      ; |359| 

           LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         LDDW    .D2T2   *B4,B5:B4         ; |359| 
||         MV      .L1     A5,A10            ; |359| 
||         MV      .S1     A4,A11            ; |359| 

           ADDKPC  .S2     RL529,B3,3        ; |359| 
RL529:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 

           STW     .D2T1   A4,*+SP(108)      ; |359| 
||         MVKL    .S1     _sin_l$11,A6      ; |359| 

           CALL    .S2X    A3                ; |359| 
||         MVKH    .S1     _sin_l$11,A6      ; |359| 
||         STW     .D2T1   A5,*+SP(104)      ; |359| 

           LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         MVKL    .S2     0x401927ef,B5     ; |359| 

           MVKL    .S2     0x9db22d0e,B4     ; |359| 
           MVKH    .S2     0x401927ef,B5     ; |359| 
           MVKH    .S2     0x9db22d0e,B4     ; |359| 
           ADDKPC  .S2     RL924,B3,0        ; |359| 
RL924:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 

           MV      .L2X    A4,B4             ; |359| 
||         MVKL    .S1     _L$6,A6           ; |359| 

           CALL    .S2X    A3                ; |359| 
||         MVKH    .S1     _L$6,A6           ; |359| 

           LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         MV      .L2X    A5,B5             ; |359| 

           ADDKPC  .S2     RL925,B3,3        ; |359| 
RL925:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S2     __mpyd,B6         ; |359| 
           MVKH    .S2     __mpyd,B6         ; |359| 

           CALL    .S2     B6                ; |359| 
||         LDW     .D2T1   *+SP(80),A5       ; |359| 
||         MV      .L1     A5,A14            ; |359| 

           LDW     .D2T1   *+SP(84),A4       ; |359| 
||         MV      .L1     A4,A15            ; |359| 
||         MVKL    .S2     0xbff4624d,B5     ; |359| 

           MVKL    .S2     0xd2f1a9fc,B4     ; |359| 
           MVKH    .S2     0xbff4624d,B5     ; |359| 
           MVKH    .S2     0xd2f1a9fc,B4     ; |359| 
           ADDKPC  .S2     RL926,B3,0        ; |359| 
RL926:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL927,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL927:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MV      .L1     A5,A14            ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(32),A5       ; |359| 

           LDW     .D2T1   *+SP(36),A4       ; |359| 
||         MVKL    .S2     0x3fe50e56,B5     ; |359| 
||         MV      .L1     A4,A15            ; |359| 

           MVKL    .S2     0x4189375,B4      ; |359| 
           MVKH    .S2     0x3fe50e56,B5     ; |359| 
           MVKH    .S2     0x4189375,B4      ; |359| 
           ADDKPC  .S2     RL928,B3,0        ; |359| 
RL928:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A15,A4            ; |359| 
           ADDKPC  .S2     RL929,B3,1        ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL929:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MV      .L1     A5,A14            ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(16),A5       ; |359| 

           LDW     .D2T1   *+SP(20),A4       ; |359| 
||         MV      .L1     A4,A15            ; |359| 
||         MVKL    .S2     0x3fcb645a,B5     ; |359| 

           MVKL    .S2     0x1cac0831,B4     ; |359| 
           MVKH    .S2     0x3fcb645a,B5     ; |359| 
           MVKH    .S2     0x1cac0831,B4     ; |359| 
           ADDKPC  .S2     RL930,B3,0        ; |359| 
RL930:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL931,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL931:     ; CALL OCCURS {__addd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S1     _sin_l1$12,A6     ; |359| 

           CALL    .S2X    A3                ; |359| 
||         MVKH    .S1     _sin_l1$12,A6     ; |359| 

           LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         MVKL    .S2     0xbfc7ced9,B5     ; |359| 
||         MV      .L1     A5,A14            ; |359| 
||         MV      .S1     A4,A15            ; |359| 

           MVKL    .S2     0x16872b02,B4     ; |359| 
           MVKH    .S2     0xbfc7ced9,B5     ; |359| 
           MVKH    .S2     0x16872b02,B4     ; |359| 
           ADDKPC  .S2     RL932,B3,0        ; |359| 
RL932:     ; CALL OCCURS {__mpyd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL933,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL933:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MV      .L1     A5,A14            ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(40),A5       ; |359| 

           LDW     .D2T1   *+SP(44),A4       ; |359| 
||         MVKL    .S2     0xbfbd2f1a,B5     ; |359| 
||         MV      .L1     A4,A15            ; |359| 

           MVKL    .S2     0x9fbe76c9,B4     ; |359| 
           MVKH    .S2     0xbfbd2f1a,B5     ; |359| 
           MVKH    .S2     0x9fbe76c9,B4     ; |359| 
           ADDKPC  .S2     RL934,B3,0        ; |359| 
RL934:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A15,A4            ; |359| 
           ADDKPC  .S2     RL935,B3,1        ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL935:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MV      .L1     A5,A14            ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(16),A5       ; |359| 

           LDW     .D2T1   *+SP(20),A4       ; |359| 
||         MV      .L1     A4,A15            ; |359| 

           ADDKPC  .S2     RL936,B3,1        ; |359| 
           MV      .L2     B11,B5            ; |359| 
           MV      .D2     B10,B4            ; |359| 
RL936:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKH    .S1     __mpyd,A3         ; |359| 
||         LDW     .D2T2   *+SP(24),B5       ; |359| 

           MV      .L2X    A5,B12            ; |359| 
||         LDW     .D2T2   *+SP(28),B4       ; |359| 

           LDW     .D2T1   *+SP(32),A5       ; |359| 
||         CALL    .S2X    A3                ; |359| 

           LDW     .D2T1   *+SP(36),A4       ; |359| 
||         MV      .L2X    A4,B13            ; |359| 

           ADDKPC  .S2     RL937,B3,3        ; |359| 
RL937:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __subd,A3         ; |359| 
           MVKH    .S1     __subd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L1X    B13,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           ADDKPC  .S2     RL938,B3,1        ; |359| 
           MV      .L1X    B12,A5            ; |359| 
RL938:     ; CALL OCCURS {__subd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0xbfae353f,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0x7ced9168,B4     ; |359| 
           MVKH    .S2     0xbfae353f,B5     ; |359| 
           MVKH    .S2     0x7ced9168,B4     ; |359| 
           ADDKPC  .S2     RL939,B3,1        ; |359| 
RL939:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL940,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL940:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKL    .S2     _cos_l1$20,B4     ; |359| 
||         MVKH    .S1     __mpyd,A3         ; |359| 

           MVKH    .S2     _cos_l1$20,B4     ; |359| 
||         LDW     .D2T2   *+SP(80),B5       ; |359| 

           LDDW    .D2T1   *B4,A5:A4         ; |359| 
||         MV      .S1     A4,A15            ; |359| 
||         MV      .L1     A5,A14            ; |359| 
||         CALL    .S2X    A3                ; |359| 

           LDW     .D2T2   *+SP(84),B4       ; |359| 
           ADDKPC  .S2     RL941,B3,3        ; |359| 
RL941:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKL    .S2     _sin_l1$12,B4     ; |359| 
||         MVKH    .S1     __mpyd,A3         ; |359| 
||         MV      .L2X    A4,B13            ; |359| 

           MVKH    .S2     _sin_l1$12,B4     ; |359| 
||         MV      .L2X    A5,B12            ; |359| 
||         LDW     .D2T2   *+SP(96),B5       ; |359| 

           LDDW    .D2T1   *B4,A5:A4         ; |359| 
||         CALL    .S2X    A3                ; |359| 

           LDW     .D2T2   *+SP(100),B4      ; |359| 
           ADDKPC  .S2     RL942,B3,3        ; |359| 
RL942:     ; CALL OCCURS {__mpyd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L1X    B13,A4            ; |359| 
           ADDKPC  .S2     RL943,B3,1        ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1X    B12,A5            ; |359| 
RL943:     ; CALL OCCURS {__addd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0xbfad2f1a,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0x9fbe76c9,B4     ; |359| 
           MVKH    .S2     0xbfad2f1a,B5     ; |359| 
           MVKH    .S2     0x9fbe76c9,B4     ; |359| 
           ADDKPC  .S2     RL944,B3,1        ; |359| 
RL944:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL945,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL945:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L1     A5,A14            ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(72),A5       ; |359| 

           LDW     .D2T1   *+SP(76),A4       ; |359| 
||         MV      .L1     A4,A15            ; |359| 

           MV      .L2X    A13,B4            ; |359| 
           MV      .L2X    A12,B5            ; |359| 
           ADDKPC  .S2     RL946,B3,1        ; |359| 
RL946:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0x3fab22d0,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0xe5604189,B4     ; |359| 
           MVKH    .S2     0x3fab22d0,B5     ; |359| 
           MVKH    .S2     0xe5604189,B4     ; |359| 
           ADDKPC  .S2     RL947,B3,1        ; |359| 
RL947:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A15,A4            ; |359| 
           ADDKPC  .S2     RL948,B3,1        ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL948:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKH    .S1     __mpyd,A3         ; |359| 
||         MVKL    .S2     _sin_l1$12,B4     ; |359| 

           MVKH    .S2     _sin_l1$12,B4     ; |359| 
           CALL    .S2X    A3                ; |359| 

           LDDW    .D2T1   *B4,A5:A4         ; |359| 
||         MV      .L1     A5,A12            ; |359| 
||         MV      .S1     A4,A13            ; |359| 

           ADDKPC  .S2     RL949,B3,1        ; |359| 
           MV      .L2     B10,B4            ; |359| 
           MV      .D2     B11,B5            ; |359| 
RL949:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S1     _cos_l1$20,A6     ; |359| 

           LDW     .D2T2   *+SP(36),B4       ; |359| 
||         MVKH    .S1     _cos_l1$20,A6     ; |359| 
||         CALL    .S2X    A3                ; |359| 

           LDW     .D2T2   *+SP(32),B5       ; |359| 
||         LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         MV      .S1     A4,A15            ; |359| 
||         MV      .L1     A5,A14            ; |359| 

           ADDKPC  .S2     RL950,B3,3        ; |359| 
RL950:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __subd,A3         ; |359| 
           MVKH    .S1     __subd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A15,A4            ; |359| 
           ADDKPC  .S2     RL951,B3,1        ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL951:     ; CALL OCCURS {__subd}            ; |359| 
           MVKL    .S2     __mpyd,B6         ; |359| 
           MVKH    .S2     __mpyd,B6         ; |359| 
           CALL    .S2     B6                ; |359| 
           MVKL    .S2     0xbfa78d4f,B5     ; |359| 
           MVKL    .S2     0xdf3b645a,B4     ; |359| 
           MVKH    .S2     0xbfa78d4f,B5     ; |359| 
           MVKH    .S2     0xdf3b645a,B4     ; |359| 
           ADDKPC  .S2     RL952,B3,0        ; |359| 
RL952:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S2     __addd,B6         ; |359| 
           MVKH    .S2     __addd,B6         ; |359| 
           CALL    .S2     B6                ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           ADDKPC  .S2     RL953,B3,0        ; |359| 
           MV      .S1     A13,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A12,A5            ; |359| 
RL953:     ; CALL OCCURS {__addd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __subd,A3         ; |359| 
           MVKH    .S1     __subd,A3         ; |359| 
           LDW     .D2T1   *+SP(104),A12     ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(108),A13     ; |359| 

           MV      .L2X    A11,B4            ; |359| 
           MV      .L1     A4,A15            ; |359| 
           ADDKPC  .S2     RL954,B3,0        ; |359| 
           MV      .L2X    A10,B5            ; |359| 

           MV      .S1     A13,A4            ; |359| 
||         MV      .D1     A5,A14            ; |359| 
||         MV      .L1     A12,A5            ; |359| 

RL954:     ; CALL OCCURS {__subd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0x3fa4fdf3,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0xb645a1cb,B4     ; |359| 
           MVKH    .S2     0x3fa4fdf3,B5     ; |359| 
           MVKH    .S2     0xb645a1cb,B4     ; |359| 
           ADDKPC  .S2     RL955,B3,1        ; |359| 
RL955:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL956,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL956:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S1     _sin_D$13,A6      ; |359| 

           MVKH    .S1     _sin_D$13,A6      ; |359| 
||         CALL    .S2X    A3                ; |359| 

           MVKL    .S2     0xbfa1eb85,B5     ; |359| 
||         LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         MV      .S1     A4,A15            ; |359| 
||         MV      .L1     A5,A14            ; |359| 

           MVKL    .S2     0x1eb851ec,B4     ; |359| 
           MVKH    .S2     0xbfa1eb85,B5     ; |359| 
           MVKH    .S2     0x1eb851ec,B4     ; |359| 
           ADDKPC  .S2     RL957,B3,0        ; |359| 
RL957:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A15,A4            ; |359| 
           ADDKPC  .S2     RL958,B3,1        ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL958:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A11,B4            ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L1     A4,A15            ; |359| 
           ADDKPC  .S2     RL959,B3,0        ; |359| 
           MV      .S1     A13,A4            ; |359| 
           MV      .D1     A5,A14            ; |359| 

           MV      .L1     A12,A5            ; |359| 
||         MV      .L2X    A10,B5            ; |359| 

RL959:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0xbf9fbe76,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0xc8b43958,B4     ; |359| 
           MVKH    .S2     0xbf9fbe76,B5     ; |359| 
           MVKH    .S2     0xc8b43958,B4     ; |359| 
           ADDKPC  .S2     RL960,B3,1        ; |359| 
RL960:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL961,B3,1        ; |359| 
           MV      .S1     A15,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A14,A5            ; |359| 
RL961:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MV      .L1     A5,A10            ; |359| 

           CALL    .S2X    A3                ; |359| 
||         LDW     .D2T1   *+SP(40),A5       ; |359| 

           LDW     .D2T1   *+SP(44),A4       ; |359| 
||         MV      .L1     A4,A11            ; |359| 

           MV      .D2     B10,B4            ; |359| 
           MV      .L2     B11,B5            ; |359| 
           ADDKPC  .S2     RL962,B3,1        ; |359| 
RL962:     ; CALL OCCURS {__mpyd}            ; |359| 

           MVKL    .S2     __mpyd,B6         ; |359| 
||         LDW     .D2T2   *+SP(48),B5       ; |359| 

           MVKH    .S2     __mpyd,B6         ; |359| 
||         LDW     .D2T2   *+SP(52),B4       ; |359| 

           LDW     .D2T1   *+SP(32),A5       ; |359| 
||         MV      .L1     A5,A12            ; |359| 
||         CALL    .S2     B6                ; |359| 

           LDW     .D2T1   *+SP(36),A4       ; |359| 
||         MV      .L1     A4,A13            ; |359| 

           ADDKPC  .S2     RL963,B3,3        ; |359| 
RL963:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S2     __subd,B6         ; |359| 
           MVKH    .S2     __subd,B6         ; |359| 
           CALL    .S2     B6                ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A13,A4            ; |359| 
           ADDKPC  .S2     RL964,B3,0        ; |359| 
           MV      .L1     A12,A5            ; |359| 
RL964:     ; CALL OCCURS {__subd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B6         ; |359| 
           MVKH    .S2     __mpyd,B6         ; |359| 
           CALL    .S2     B6                ; |359| 
           MVKL    .S2     0xbf8eb851,B5     ; |359| 
           MVKL    .S2     0xeb851eb8,B4     ; |359| 
           MVKH    .S2     0xbf8eb851,B5     ; |359| 
           MVKH    .S2     0xeb851eb8,B4     ; |359| 
           ADDKPC  .S2     RL965,B3,0        ; |359| 
RL965:     ; CALL OCCURS {__mpyd}            ; |359| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL966,B3,1        ; |359| 
           MV      .S1     A11,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A10,A5            ; |359| 
RL966:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 

           MVKL    .S2     _sin_l$11,B4      ; |359| 
||         MVKH    .S1     __mpyd,A3         ; |359| 

           MVKH    .S2     _sin_l$11,B4      ; |359| 
||         LDW     .D2T2   *+SP(64),B5       ; |359| 

           LDDW    .D2T1   *B4,A5:A4         ; |359| 
||         MV      .S1     A4,A11            ; |359| 
||         MV      .L1     A5,A10            ; |359| 
||         CALL    .S2X    A3                ; |359| 

           LDW     .D2T2   *+SP(68),B4       ; |359| 
           ADDKPC  .S2     RL967,B3,3        ; |359| 
RL967:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S1     _cos_l$19,A6      ; |359| 

           LDW     .D2T2   *+SP(60),B4       ; |359| 
||         MVKH    .S1     _cos_l$19,A6      ; |359| 
||         CALL    .S2X    A3                ; |359| 

           LDDW    .D1T1   *A6,A5:A4         ; |359| 
||         MV      .S1     A4,A13            ; |359| 
||         MV      .L1     A5,A12            ; |359| 
||         LDW     .D2T2   *+SP(56),B5       ; |359| 

           ADDKPC  .S2     RL968,B3,3        ; |359| 
RL968:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __subd,A3         ; |359| 
           MVKH    .S1     __subd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .S1     A13,A4            ; |359| 
           ADDKPC  .S2     RL969,B3,1        ; |359| 
           MV      .L1     A12,A5            ; |359| 
RL969:     ; CALL OCCURS {__subd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0x3f86872b,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0x20c49ba,B4      ; |359| 
           MVKH    .S2     0x3f86872b,B5     ; |359| 
           MVKH    .S2     0x20c49ba,B4      ; |359| 
           ADDKPC  .S2     RL970,B3,1        ; |359| 
RL970:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __addd,A3         ; |359| 
           MVKH    .S1     __addd,A3         ; |359| 
           MV      .L2X    A4,B4             ; |359| 
           CALL    .S2X    A3                ; |359| 
           ADDKPC  .S2     RL971,B3,1        ; |359| 
           MV      .S1     A11,A4            ; |359| 
           MV      .L2X    A5,B5             ; |359| 
           MV      .L1     A10,A5            ; |359| 
RL971:     ; CALL OCCURS {__addd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |359| 
           MVKH    .S1     __mpyd,A3         ; |359| 
           MVKL    .S2     0x3f91df46,B5     ; |359| 
           CALL    .S2X    A3                ; |359| 
           MVKL    .S2     0xa2529d39,B4     ; |359| 
           MVKH    .S2     0x3f91df46,B5     ; |359| 
           MVKH    .S2     0xa2529d39,B4     ; |359| 
           ADDKPC  .S2     RL972,B3,1        ; |359| 
RL972:     ; CALL OCCURS {__mpyd}            ; |359| 
           MVKL    .S1     __mpyd,A3         ; |401| 

           MVKH    .S1     __mpyd,A3         ; |401| 
||         LDW     .D2T2   *+SP(8),B5        ; |359| 

           LDW     .D2T2   *+SP(12),B4       ; |401| 

           LDW     .D2T1   *+SP(84),A4       ; |401| 
||         MV      .L1     A4,A6             ; |359| 
||         CALL    .S2X    A3                ; |401| 

           LDW     .D2T1   *+SP(80),A5       ; |401| 
||         MV      .L1     A5,A7             ; |359| 

           MVKL    .S1     _lamda$7,A8       ; |359| 
           MVKH    .S1     _lamda$7,A8       ; |359| 
           STDW    .D1T1   A7:A6,*A8         ; |359| 
           ADDKPC  .S2     RL973,B3,0        ; |401| 
RL973:     ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S1     __mpyd,A3         ; |401| 
           MVKH    .S1     __mpyd,A3         ; |401| 
           MVKL    .S1     _sin_F$14,A6      ; |401| 

           MVKH    .S1     _sin_F$14,A6      ; |401| 
||         LDW     .D2T2   *+SP(100),B4      ; |401| 
||         CALL    .S2X    A3                ; |401| 

           LDW     .D2T2   *+SP(96),B5       ; |401| 
||         LDDW    .D1T1   *A6,A5:A4         ; |401| 
||         MV      .S1     A4,A15            ; |401| 
||         MV      .L1     A5,A14            ; |401| 

           ADDKPC  .S2     RL974,B3,3        ; |401| 
RL974:     ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S2     __mpyd,B6         ; |401| 

           MVKL    .S1     _sin_l$11,A3      ; |401| 
||         MVKH    .S2     __mpyd,B6         ; |401| 

           MVKH    .S1     _sin_l$11,A3      ; |401| 
||         LDW     .D2T2   *+SP(12),B4       ; |401| 
||         MV      .L2X    A4,B13            ; |401| 
||         CALL    .S2     B6                ; |401| 

           LDDW    .D1T1   *A3,A5:A4         ; |401| 
||         LDW     .D2T2   *+SP(8),B5        ; |401| 
||         MV      .L2X    A5,B12            ; |401| 

           ADDKPC  .S2     RL975,B3,3        ; |401| 
RL975:     ; CALL OCCURS {__mpyd}            ; |401| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |401| 

           MVKH    .S1     __mpyd,A3         ; |401| 
||         MVKL    .S2     _sin_F$14,B4      ; |401| 

           MVKH    .S2     _sin_F$14,B4      ; |401| 
||         MVKL    .S1     _cos_l$19,A6      ; |401| 

           CALL    .S2X    A3                ; |401| 
||         MVKH    .S1     _cos_l$19,A6      ; |401| 

           LDDW    .D1T2   *A6,B5:B4         ; |401| 
||         LDDW    .D2T1   *B4,A5:A4         ; |401| 
||         MV      .L1     A5,A10            ; |401| 
||         MV      .S1     A4,A11            ; |401| 

           ADDKPC  .S2     RL976,B3,3        ; |401| 
RL976:     ; CALL OCCURS {__mpyd}            ; |401| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A6         ; |401| 
           MVKH    .S1     __mpyd,A6         ; |401| 
           MVKL    .S1     _sin_F$14,A3      ; |401| 

           CALL    .S2X    A6                ; |401| 
||         MVKH    .S1     _sin_F$14,A3      ; |401| 

           LDDW    .D1T1   *A3,A5:A4         ; |401| 
||         MVKL    .S2     0x40148312,B5     ; |401| 
||         MV      .L1     A5,A12            ; |401| 
||         MV      .S1     A4,A13            ; |401| 

           MVKL    .S2     0x6e978d50,B4     ; |401| 
           MVKH    .S2     0x40148312,B5     ; |401| 
           MVKH    .S2     0x6e978d50,B4     ; |401| 
           ADDKPC  .S2     RL1011,B3,0       ; |401| 
RL1011:    ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S1     __addd,A3         ; |401| 
           MVKH    .S1     __addd,A3         ; |401| 
           MV      .L2X    A11,B4            ; |401| 
           CALL    .S2X    A3                ; |401| 
           ADDKPC  .S2     RL1012,B3,0       ; |401| 
           STW     .D2T1   A5,*+SP(40)       ; |401| 
           MV      .L1     A12,A5            ; |401| 
           STW     .D2T1   A4,*+SP(44)       ; |401| 

           MV      .S1     A13,A4            ; |401| 
||         MV      .L2X    A10,B5            ; |401| 

RL1012:    ; CALL OCCURS {__addd}            ; |401| 
           MVKL    .S1     __mpyd,A3         ; |401| 
           MVKH    .S1     __mpyd,A3         ; |401| 
           MVKL    .S2     0x3fd1fbe7,B5     ; |401| 
           CALL    .S2X    A3                ; |401| 
           MVKL    .S2     0x6c8b4396,B4     ; |401| 
           MVKH    .S2     0x3fd1fbe7,B5     ; |401| 
           MVKH    .S2     0x6c8b4396,B4     ; |401| 
           ADDKPC  .S2     RL1013,B3,1       ; |401| 
RL1013:    ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S1     __addd,A3         ; |401| 
           MVKH    .S1     __addd,A3         ; |401| 
           MV      .L2X    A5,B5             ; |401| 

           CALL    .S2X    A3                ; |401| 
||         LDW     .D2T1   *+SP(40),A5       ; |401| 

           LDW     .D2T1   *+SP(44),A4       ; |401| 
||         MV      .L2X    A4,B4             ; |401| 

           ADDKPC  .S2     RL1014,B3,3       ; |401| 
RL1014:    ; CALL OCCURS {__addd}            ; |401| 
           MVKL    .S1     __subd,A3         ; |401| 
           MVKH    .S1     __subd,A3         ; |401| 
           MV      .L2X    A11,B4            ; |401| 
           CALL    .S2X    A3                ; |401| 
           ADDKPC  .S2     RL1015,B3,0       ; |401| 
           STW     .D2T1   A5,*+SP(40)       ; |401| 
           MV      .L1     A12,A5            ; |401| 
           STW     .D2T1   A4,*+SP(44)       ; |401| 

           MV      .S1     A13,A4            ; |401| 
||         MV      .L2X    A10,B5            ; |401| 

RL1015:    ; CALL OCCURS {__subd}            ; |401| 
           MVKL    .S1     __mpyd,A3         ; |401| 
           MVKH    .S1     __mpyd,A3         ; |401| 
           MVKL    .S2     0xbfd1cac0,B5     ; |401| 
           CALL    .S2X    A3                ; |401| 
           MVKL    .S2     0x83126e98,B4     ; |401| 
           MVKH    .S2     0xbfd1cac0,B5     ; |401| 
           MVKH    .S2     0x83126e98,B4     ; |401| 
           ADDKPC  .S2     RL1016,B3,1       ; |401| 
RL1016:    ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S1     __addd,A3         ; |401| 
           MVKH    .S1     __addd,A3         ; |401| 
           MV      .L2X    A5,B5             ; |401| 

           CALL    .S2X    A3                ; |401| 
||         LDW     .D2T1   *+SP(40),A5       ; |401| 

           LDW     .D2T1   *+SP(44),A4       ; |401| 
||         MV      .L2X    A4,B4             ; |401| 

           ADDKPC  .S2     RL1017,B3,3       ; |401| 
RL1017:    ; CALL OCCURS {__addd}            ; |401| 
           MVKL    .S2     __subd,B6         ; |401| 
           MVKH    .S2     __subd,B6         ; |401| 
           CALL    .S2     B6                ; |401| 
           MV      .S1     A4,A11            ; |401| 
           MV      .L2X    A15,B4            ; |401| 
           MV      .L1X    B13,A4            ; |401| 
           ADDKPC  .S2     RL1018,B3,0       ; |401| 

           MV      .S1     A5,A10            ; |401| 
||         MV      .L2X    A14,B5            ; |401| 
||         MV      .L1X    B12,A5            ; |401| 

RL1018:    ; CALL OCCURS {__subd}            ; |401| 
           MVKL    .S1     __mpyd,A3         ; |401| 
           MVKH    .S1     __mpyd,A3         ; |401| 
           MVKL    .S2     0x3fac28f5,B5     ; |401| 
           CALL    .S2X    A3                ; |401| 
           MVKL    .S2     0xc28f5c29,B4     ; |401| 
           MVKH    .S2     0x3fac28f5,B5     ; |401| 
           MVKH    .S2     0xc28f5c29,B4     ; |401| 
           ADDKPC  .S2     RL1019,B3,1       ; |401| 
RL1019:    ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S1     __addd,A3         ; |401| 
           MVKH    .S1     __addd,A3         ; |401| 
           MV      .L2X    A4,B4             ; |401| 
           CALL    .S2X    A3                ; |401| 
           ADDKPC  .S2     RL1020,B3,1       ; |401| 
           MV      .S1     A11,A4            ; |401| 
           MV      .L2X    A5,B5             ; |401| 
           MV      .L1     A10,A5            ; |401| 
RL1020:    ; CALL OCCURS {__addd}            ; |401| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |401| 
           MVKH    .S1     __addd,A3         ; |401| 
           MV      .L2X    A15,B4            ; |401| 
           CALL    .S2X    A3                ; |401| 
           MV      .S1     A4,A11            ; |401| 
           MV      .L1X    B13,A4            ; |401| 
           ADDKPC  .S2     RL1021,B3,0       ; |401| 
           MV      .L2X    A14,B5            ; |401| 

           MV      .S1     A5,A10            ; |401| 
||         MV      .L1X    B12,A5            ; |401| 

RL1021:    ; CALL OCCURS {__addd}            ; |401| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |401| 
           MVKH    .S1     __mpyd,A3         ; |401| 
           MVKL    .S2     0xbfa78d4f,B5     ; |401| 
           CALL    .S2X    A3                ; |401| 
           MVKL    .S2     0xdf3b645a,B4     ; |401| 
           MVKH    .S2     0xbfa78d4f,B5     ; |401| 
           MVKH    .S2     0xdf3b645a,B4     ; |401| 
           ADDKPC  .S2     RL1022,B3,1       ; |401| 
RL1022:    ; CALL OCCURS {__mpyd}            ; |401| 
           MVKL    .S1     __addd,A3         ; |401| 
           MVKH    .S1     __addd,A3         ; |401| 
           MV      .L2X    A4,B4             ; |401| 
           CALL    .S2X    A3                ; |401| 
           ADDKPC  .S2     RL1023,B3,1       ; |401| 
           MV      .S1     A11,A4            ; |401| 
           MV      .L2X    A5,B5             ; |401| 
           MV      .L1     A10,A5            ; |401| 
RL1023:    ; CALL OCCURS {__addd}            ; |401| 
           MVKL    .S1     __mpyd,A3         ; |405| 
           MVKH    .S1     __mpyd,A3         ; |405| 
           MVKL    .S1     _sin_F$14,A6      ; |405| 

           MVKH    .S1     _sin_F$14,A6      ; |405| 
||         CALL    .S2X    A3                ; |405| 

           LDDW    .D1T1   *A6,A5:A4         ; |405| 
||         MV      .S1     A4,A15            ; |401| 
||         MV      .L1     A5,A14            ; |401| 

           MV      .D2     B10,B4            ; |405| 
           MV      .L2     B11,B5            ; |405| 
           ADDKPC  .S2     RL1024,B3,1       ; |405| 
RL1024:    ; CALL OCCURS {__mpyd}            ; |405| 
           MVKL    .S1     __mpyd,A3         ; |405| 

           MVKH    .S1     __mpyd,A3         ; |405| 
||         LDW     .D2T2   *+SP(8),B5        ; |405| 

           LDW     .D2T2   *+SP(12),B4       ; |405| 

           LDW     .D2T1   *+SP(32),A5       ; |405| 
||         MV      .L1     A5,A11            ; |405| 
||         CALL    .S2X    A3                ; |405| 

           LDW     .D2T1   *+SP(36),A4       ; |405| 
||         MV      .L1     A4,A10            ; |405| 

           MVKL    .S2     _dtm1$29,B6       ; |405| 
           MVKH    .S2     _dtm1$29,B6       ; |405| 
           STDW    .D2T1   A11:A10,*B6       ; |405| 
           ADDKPC  .S2     RL1025,B3,0       ; |405| 
RL1025:    ; CALL OCCURS {__mpyd}            ; |405| 
           MVKL    .S1     __addd,A3         ; |406| 
           MVKH    .S1     __addd,A3         ; |406| 
           MVKL    .S1     _dtm2$30,A6       ; |405| 
           CALL    .S2X    A3                ; |406| 
           MV      .L2X    A4,B4             ; |405| 
           MV      .D1     A4,A12            ; |405| 
           MV      .L1     A5,A13            ; |405| 
           MVKH    .S1     _dtm2$30,A6       ; |405| 

           MV      .L1     A11,A5            ; |406| 
||         MV      .L2X    A5,B5             ; |405| 
||         MV      .S1     A10,A4            ; |406| 
||         STDW    .D1T1   A13:A12,*A6       ; |405| 
||         ADDKPC  .S2     RL1061,B3,0       ; |406| 

RL1061:    ; CALL OCCURS {__addd}            ; |406| 
           MVKL    .S1     __mpyd,A3         ; |406| 
           MVKH    .S1     __mpyd,A3         ; |406| 
           MVKL    .S2     0x3fa0e560,B5     ; |406| 
           CALL    .S2X    A3                ; |406| 
           MVKL    .S2     0x4189374c,B4     ; |406| 
           MVKH    .S2     0x3fa0e560,B5     ; |406| 
           MVKH    .S2     0x4189374c,B4     ; |406| 
           ADDKPC  .S2     RL1062,B3,1       ; |406| 
RL1062:    ; CALL OCCURS {__mpyd}            ; |406| 
           MVKL    .S1     __addd,A3         ; |406| 
           MVKH    .S1     __addd,A3         ; |406| 
           MV      .L2X    A4,B4             ; |406| 
           CALL    .S2X    A3                ; |406| 
           MV      .L2X    A5,B5             ; |406| 
           MV      .S1     A15,A4            ; |406| 
           ADDKPC  .S2     RL1063,B3,1       ; |406| 
           MV      .L1     A14,A5            ; |406| 
RL1063:    ; CALL OCCURS {__addd}            ; |406| 
           MVKL    .S1     __subd,A3         ; |406| 
           MVKH    .S1     __subd,A3         ; |406| 
           MV      .L2X    A12,B4            ; |406| 
           CALL    .S2X    A3                ; |406| 
           MV      .L1     A4,A15            ; |406| 
           ADDKPC  .S2     RL1064,B3,0       ; |406| 
           MV      .S1     A10,A4            ; |406| 
           MV      .D1     A5,A14            ; |406| 

           MV      .L1     A11,A5            ; |406| 
||         MV      .L2X    A13,B5            ; |406| 

RL1064:    ; CALL OCCURS {__subd}            ; |406| 
           MVKL    .S1     __mpyd,A3         ; |406| 
           MVKH    .S1     __mpyd,A3         ; |406| 
           MVKL    .S2     0xbfc624dd,B5     ; |406| 
           CALL    .S2X    A3                ; |406| 
           MVKL    .S2     0x2f1a9fbe,B4     ; |406| 
           MVKH    .S2     0xbfc624dd,B5     ; |406| 
           MVKH    .S2     0x2f1a9fbe,B4     ; |406| 
           ADDKPC  .S2     RL1065,B3,1       ; |406| 
RL1065:    ; CALL OCCURS {__mpyd}            ; |406| 
           MVKL    .S2     __addd,B6         ; |406| 
           MVKH    .S2     __addd,B6         ; |406| 
           CALL    .S2     B6                ; |406| 
           MV      .L2X    A4,B4             ; |406| 
           ADDKPC  .S2     RL1066,B3,0       ; |406| 
           MV      .S1     A15,A4            ; |406| 
           MV      .L2X    A5,B5             ; |406| 
           MV      .L1     A14,A5            ; |406| 
RL1066:    ; CALL OCCURS {__addd}            ; |406| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |406| 

           LDW     .D2T2   *+SP(8),B5        ; |406| 
||         MVKH    .S1     __mpyd,A3         ; |406| 

           LDW     .D2T2   *+SP(12),B4       ; |406| 

           CALL    .S2X    A3                ; |406| 
||         LDW     .D2T1   *+SP(16),A5       ; |406| 
||         MV      .L1     A5,A10            ; |406| 

           LDW     .D2T1   *+SP(20),A4       ; |406| 
||         MV      .L1     A4,A11            ; |406| 

           ADDKPC  .S2     RL1067,B3,3       ; |406| 
RL1067:    ; CALL OCCURS {__mpyd}            ; |406| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |406| 
           MVKH    .S1     __mpyd,A3         ; |406| 
           MVKL    .S1     _sin_F$14,A6      ; |406| 

           CALL    .S2X    A3                ; |406| 
||         MVKH    .S1     _sin_F$14,A6      ; |406| 
||         LDW     .D2T2   *+SP(28),B4       ; |406| 

           LDDW    .D1T1   *A6,A5:A4         ; |406| 
||         LDW     .D2T2   *+SP(24),B5       ; |406| 
||         MV      .L1     A5,A12            ; |406| 
||         MV      .S1     A4,A13            ; |406| 

           ADDKPC  .S2     RL1068,B3,3       ; |406| 
RL1068:    ; CALL OCCURS {__mpyd}            ; |406| 
           MVKL    .S1     __addd,A3         ; |406| 
           MVKH    .S1     __addd,A3         ; |406| 
           MV      .L2X    A4,B4             ; |406| 
           CALL    .S2X    A3                ; |406| 
           ADDKPC  .S2     RL1069,B3,1       ; |406| 
           MV      .S1     A13,A4            ; |406| 
           MV      .L2X    A5,B5             ; |406| 
           MV      .L1     A12,A5            ; |406| 
RL1069:    ; CALL OCCURS {__addd}            ; |406| 
           MVKL    .S1     __mpyd,A3         ; |406| 
           MVKH    .S1     __mpyd,A3         ; |406| 
           MVKL    .S2     0x3f916872,B5     ; |406| 
           CALL    .S2X    A3                ; |406| 
           MVKL    .S2     0xb020c49c,B4     ; |406| 
           MVKH    .S2     0x3f916872,B5     ; |406| 
           MVKH    .S2     0xb020c49c,B4     ; |406| 
           ADDKPC  .S2     RL1070,B3,1       ; |406| 
RL1070:    ; CALL OCCURS {__mpyd}            ; |406| 
           MVKL    .S1     __addd,A3         ; |406| 
           MVKH    .S1     __addd,A3         ; |406| 
           MV      .L2X    A4,B4             ; |406| 
           CALL    .S2X    A3                ; |406| 
           MV      .L2X    A5,B5             ; |406| 
           MV      .S1     A11,A4            ; |406| 
           ADDKPC  .S2     RL1071,B3,1       ; |406| 
           MV      .L1     A10,A5            ; |406| 
RL1071:    ; CALL OCCURS {__addd}            ; |406| 
           MVKL    .S1     __mpyd,A3         ; |406| 
           MVKH    .S1     __mpyd,A3         ; |406| 
           MVKL    .S2     0x3f91df46,B5     ; |406| 
           CALL    .S2X    A3                ; |406| 
           MVKL    .S2     0xa2529d39,B4     ; |406| 
           MVKH    .S2     0x3f91df46,B5     ; |406| 
           MVKH    .S2     0xa2529d39,B4     ; |406| 
           ADDKPC  .S2     RL1072,B3,1       ; |406| 
RL1072:    ; CALL OCCURS {__mpyd}            ; |406| 
           MVKL    .S1     __mpyd,A3         ; |416| 

           MVKH    .S1     __mpyd,A3         ; |416| 
||         MVKL    .S2     _cos_l$19,B6      ; |416| 

           MVKH    .S2     _cos_l$19,B6      ; |416| 
           CALL    .S2X    A3                ; |416| 

           LDDW    .D2T1   *B6,A5:A4         ; |416| 
||         MV      .L1     A5,A11            ; |406| 
||         MV      .S1     A4,A10            ; |406| 
||         MVKL    .S2     0x3fabe76c,B5     ; |416| 

           MVKL    .S2     0x8b439581,B4     ; |416| 

           MVKH    .S2     0x3fabe76c,B5     ; |416| 
||         MVKL    .S1     _beta$8,A6        ; |406| 

           MVKH    .S2     0x8b439581,B4     ; |416| 
||         MVKH    .S1     _beta$8,A6        ; |406| 

           ADDKPC  .S2     RL1181,B3,0       ; |416| 
||         STDW    .D1T1   A11:A10,*A6       ; |406| 

RL1181:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           ZERO    .L2     B5                ; |416| 
           CALL    .S2X    A3                ; |416| 
           MVKH    .S2     0x3ff00000,B5     ; |416| 
           ADDKPC  .S2     RL1182,B3,2       ; |416| 
           ZERO    .L2     B4                ; |416| 
RL1182:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 
           MV      .L1     A5,A12            ; |416| 

           CALL    .S2X    A3                ; |416| 
||         LDW     .D2T1   *+SP(96),A5       ; |416| 

           LDW     .D2T1   *+SP(100),A4      ; |416| 
||         MVKL    .S2     0x3f847ae1,B5     ; |416| 
||         MV      .L1     A4,A13            ; |416| 

           MVKL    .S2     0x47ae147b,B4     ; |416| 
           MVKH    .S2     0x3f847ae1,B5     ; |416| 
           MVKH    .S2     0x47ae147b,B4     ; |416| 
           ADDKPC  .S2     RL1183,B3,0       ; |416| 
RL1183:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .S1     A13,A4            ; |416| 
           ADDKPC  .S2     RL1184,B3,1       ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1184:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 
           MVKL    .S2     0x5e9e1b09,B4     ; |416| 
           CALL    .S2X    A3                ; |416| 
           MVKL    .S2     0x3f80cb29,B5     ; |416| 
           MVKH    .S2     0x5e9e1b09,B4     ; |416| 
           MV      .S1     A4,A13            ; |416| 

           MVKH    .S2     0x3f80cb29,B5     ; |416| 
||         MV      .L1X    B10,A4            ; |416| 

           MV      .L1X    B11,A5            ; |416| 
||         ADDKPC  .S2     RL1185,B3,0       ; |416| 
||         MV      .S1     A5,A12            ; |416| 

RL1185:    ; CALL OCCURS {__mpyd}            ; |416| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           ADDKPC  .S2     RL1186,B3,1       ; |416| 
           MV      .S1     A13,A4            ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1186:    ; CALL OCCURS {__addd}            ; |416| 
;** --------------------------------------------------------------------------*
           MVKL    .S2     __mpyd,B6         ; |416| 
           MVKH    .S2     __mpyd,B6         ; |416| 

           CALL    .S2     B6                ; |416| 
||         LDW     .D2T1   *+SP(24),A5       ; |416| 
||         MV      .L1     A5,A12            ; |416| 

           LDW     .D2T1   *+SP(28),A4       ; |416| 
||         MVKL    .S2     0x3f689374,B5     ; |416| 
||         MV      .L1     A4,A13            ; |416| 

           MVKL    .S2     0xbc6a7efa,B4     ; |416| 
           MVKH    .S2     0x3f689374,B5     ; |416| 
           MVKH    .S2     0xbc6a7efa,B4     ; |416| 
           ADDKPC  .S2     RL1187,B3,0       ; |416| 
RL1187:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           ADDKPC  .S2     RL1188,B3,1       ; |416| 
           MV      .S1     A13,A4            ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1188:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 

           MVKL    .S1     _sin_l$11,A6      ; |416| 
||         LDW     .D2T1   *+SP(36),A15      ; |416| 

           LDW     .D2T1   *+SP(32),A14      ; |416| 
||         MVKH    .S1     _sin_l$11,A6      ; |416| 
||         CALL    .S2X    A3                ; |416| 

           LDDW    .D1T1   *A6,A5:A4         ; |416| 
||         MV      .S1     A4,A13            ; |416| 
||         MV      .L1     A5,A12            ; |416| 

           ADDKPC  .S2     RL1189,B3,1       ; |416| 
           MV      .L2X    A15,B4            ; |416| 
           MV      .L2X    A14,B5            ; |416| 
RL1189:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __subd,A3         ; |416| 
           MVKH    .S1     __subd,A3         ; |416| 
           MV      .L2X    A5,B5             ; |416| 

           LDW     .D2T1   *+SP(88),A5       ; |416| 
||         CALL    .S2X    A3                ; |416| 

           LDW     .D2T1   *+SP(92),A4       ; |416| 
||         MV      .L2X    A4,B4             ; |416| 

           ADDKPC  .S2     RL1190,B3,3       ; |416| 
RL1190:    ; CALL OCCURS {__subd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 
           MVKL    .S2     0x3f4d7dbf,B5     ; |416| 
           CALL    .S2X    A3                ; |416| 
           MVKL    .S2     0x487fcb92,B4     ; |416| 
           MVKH    .S2     0x3f4d7dbf,B5     ; |416| 
           MVKH    .S2     0x487fcb92,B4     ; |416| 
           ADDKPC  .S2     RL1191,B3,1       ; |416| 
RL1191:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .S1     A13,A4            ; |416| 
           ADDKPC  .S2     RL1192,B3,1       ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1192:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 
           MVKL    .S1     _cos_l1$20,A6     ; |416| 

           CALL    .S2X    A3                ; |416| 
||         MVKH    .S1     _cos_l1$20,A6     ; |416| 

           LDDW    .D1T1   *A6,A5:A4         ; |416| 
||         MV      .L1     A5,A12            ; |416| 
||         MV      .S1     A4,A13            ; |416| 

           ADDKPC  .S2     RL1193,B3,1       ; |416| 
           MV      .L2     B11,B5            ; |416| 
           MV      .D2     B10,B4            ; |416| 
RL1193:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __mpyd,A6         ; |416| 
           MVKH    .S1     __mpyd,A6         ; |416| 

           MVKL    .S1     _sin_l1$12,A3     ; |416| 
||         MV      .L2X    A4,B11            ; |416| 

           MVKH    .S1     _sin_l1$12,A3     ; |416| 
||         CALL    .S2X    A6                ; |416| 

           LDDW    .D1T1   *A3,A5:A4         ; |416| 
||         MV      .L2X    A5,B10            ; |416| 

           MV      .L2X    A15,B4            ; |416| 
           MV      .L2X    A14,B5            ; |416| 
           ADDKPC  .S2     RL1194,B3,1       ; |416| 
RL1194:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S2     __addd,B6         ; |416| 
           MVKH    .S2     __addd,B6         ; |416| 
           CALL    .S2     B6                ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           MV      .L1X    B11,A4            ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           ADDKPC  .S2     RL1195,B3,0       ; |416| 
           MV      .L1X    B10,A5            ; |416| 
RL1195:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S2     __mpyd,B6         ; |416| 
           MVKH    .S2     __mpyd,B6         ; |416| 
           CALL    .S2     B6                ; |416| 
           MVKL    .S2     0x3f43a92a,B5     ; |416| 
           MVKL    .S2     0x30553261,B4     ; |416| 
           MVKH    .S2     0x3f43a92a,B5     ; |416| 
           MVKH    .S2     0x30553261,B4     ; |416| 
           ADDKPC  .S2     RL1196,B3,0       ; |416| 
RL1196:    ; CALL OCCURS {__mpyd}            ; |416| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           ADDKPC  .S2     RL1197,B3,1       ; |416| 
           MV      .S1     A13,A4            ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1197:    ; CALL OCCURS {__addd}            ; |416| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __mpyd,A3         ; |416| 

           MVKH    .S1     __mpyd,A3         ; |416| 
||         MVKL    .S2     _cos_l1$20,B4     ; |416| 

           LDW     .D2T2   *+SP(96),B5       ; |416| 
||         MVKH    .S2     _cos_l1$20,B4     ; |416| 

           CALL    .S2X    A3                ; |416| 
||         LDDW    .D2T1   *B4,A5:A4         ; |416| 
||         MV      .L1     A5,A12            ; |416| 
||         MV      .S1     A4,A13            ; |416| 

           LDW     .D2T2   *+SP(100),B4      ; |416| 
           ADDKPC  .S2     RL1198,B3,3       ; |416| 
RL1198:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 
           MVKL    .S1     _sin_l1$12,A6     ; |416| 

           CALL    .S2X    A3                ; |416| 
||         LDW     .D2T2   *+SP(84),B4       ; |416| 
||         MVKH    .S1     _sin_l1$12,A6     ; |416| 

           LDDW    .D1T1   *A6,A5:A4         ; |416| 
||         LDW     .D2T2   *+SP(80),B5       ; |416| 
||         MV      .L1     A5,A14            ; |416| 
||         MV      .S1     A4,A15            ; |416| 

           ADDKPC  .S2     RL1199,B3,3       ; |416| 
RL1199:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __subd,A3         ; |416| 
           MVKH    .S1     __subd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           ADDKPC  .S2     RL1200,B3,1       ; |416| 
           MV      .S1     A15,A4            ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .L1     A14,A5            ; |416| 
RL1200:    ; CALL OCCURS {__subd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 
           MVKH    .S1     __mpyd,A3         ; |416| 
           MVKL    .S2     0x3f3a36e2,B5     ; |416| 
           CALL    .S2X    A3                ; |416| 
           MVKL    .S2     0xeb1c432d,B4     ; |416| 
           MVKH    .S2     0x3f3a36e2,B5     ; |416| 
           MVKH    .S2     0xeb1c432d,B4     ; |416| 
           ADDKPC  .S2     RL1201,B3,1       ; |416| 
RL1201:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .S1     A13,A4            ; |416| 
           ADDKPC  .S2     RL1202,B3,1       ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1202:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S2     __mpyd,B6         ; |416| 
           MVKL    .S2     _cos_l1$20,B4     ; |416| 
           MVKH    .S2     __mpyd,B6         ; |416| 

           MVKH    .S2     _cos_l1$20,B4     ; |416| 
||         MVKL    .S1     _cos_l$19,A3      ; |416| 

           CALL    .S2     B6                ; |416| 
||         MVKH    .S1     _cos_l$19,A3      ; |416| 

           LDDW    .D2T2   *B4,B5:B4         ; |416| 
||         LDDW    .D1T1   *A3,A5:A4         ; |416| 
||         MV      .L1     A5,A12            ; |416| 
||         MV      .S1     A4,A13            ; |416| 

           ADDKPC  .S2     RL1203,B3,3       ; |416| 
RL1203:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __mpyd,A3         ; |416| 

           MVKH    .S1     __mpyd,A3         ; |416| 
||         MVKL    .S2     _sin_l1$12,B4     ; |416| 

           MVKH    .S2     _sin_l1$12,B4     ; |416| 
||         MVKL    .S1     _sin_l$11,A6      ; |416| 

           CALL    .S2X    A3                ; |416| 
||         MVKH    .S1     _sin_l$11,A6      ; |416| 

           LDDW    .D2T2   *B4,B5:B4         ; |416| 
||         LDDW    .D1T1   *A6,A5:A4         ; |416| 
||         MV      .L1     A5,A14            ; |416| 
||         MV      .S1     A4,A15            ; |416| 

           ADDKPC  .S2     RL1204,B3,3       ; |416| 
RL1204:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S1     __addd,A3         ; |416| 
           MVKH    .S1     __addd,A3         ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |416| 
           ADDKPC  .S2     RL1205,B3,1       ; |416| 
           MV      .S1     A15,A4            ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .L1     A14,A5            ; |416| 
RL1205:    ; CALL OCCURS {__addd}            ; |416| 
           MVKL    .S2     __mpyd,B6         ; |416| 
           MVKH    .S2     __mpyd,B6         ; |416| 
           CALL    .S2     B6                ; |416| 
           MVKL    .S2     0x3f33a92a,B5     ; |416| 
           MVKL    .S2     0x30553261,B4     ; |416| 
           MVKH    .S2     0x3f33a92a,B5     ; |416| 
           MVKH    .S2     0x30553261,B4     ; |416| 
           ADDKPC  .S2     RL1206,B3,0       ; |416| 
RL1206:    ; CALL OCCURS {__mpyd}            ; |416| 
           MVKL    .S2     __addd,B6         ; |416| 
           MVKH    .S2     __addd,B6         ; |416| 
           CALL    .S2     B6                ; |416| 
           MV      .L2X    A4,B4             ; |416| 
           MV      .L2X    A5,B5             ; |416| 
           MV      .S1     A13,A4            ; |416| 
           ADDKPC  .S2     RL1207,B3,0       ; |416| 
           MV      .L1     A12,A5            ; |416| 
RL1207:    ; CALL OCCURS {__addd}            ; |416| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     __divd,A3         ; |432| 
           MVKH    .S1     __divd,A3         ; |432| 
           MV      .L2X    A4,B4             ; |416| 
           CALL    .S2X    A3                ; |432| 
           MV      .L1     A4,A6             ; |416| 
           ZERO    .L1     A4                ; |432| 

           MVKL    .S2     _Q$9,B6           ; |416| 
||         MV      .D1     A5,A7             ; |416| 
||         MV      .L2X    A5,B5             ; |416| 
||         MVKL    .S1     0x41b6e98a,A5     ; |432| 

           MVKH    .S1     0x20000000,A4     ; |432| 
||         MVKH    .S2     _Q$9,B6           ; |416| 

           ADDKPC  .S2     RL1208,B3,0       ; |432| 
||         STDW    .D2T1   A7:A6,*B6         ; |416| 
||         MVKH    .S1     0x41b6e98a,A5     ; |432| 

RL1208:    ; CALL OCCURS {__divd}            ; |432| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _cos,A3           ; |437| 
           MVKH    .S1     _cos,A3           ; |437| 
           MVKL    .S2     _rm$10,B4         ; |432| 
           CALL    .S2X    A3                ; |437| 
           MVKH    .S2     _rm$10,B4         ; |432| 
           MV      .L1     A5,A7             ; |432| 
           MV      .S1     A4,A6             ; |432| 
           ADDKPC  .S2     RL1210,B3,0       ; |437| 

           STDW    .D2T1   A7:A6,*B4         ; |432| 
||         MV      .L1     A11,A5            ; |437| 
||         MV      .S1     A10,A4            ; |437| 

RL1210:    ; CALL OCCURS {_cos}              ; |437| 
           MVKL    .S1     __mpyd,A3         ; |437| 

           MVKH    .S1     __mpyd,A3         ; |437| 
||         MVKL    .S2     _rm$10,B4         ; |437| 

           MVKH    .S2     _rm$10,B4         ; |437| 
           CALL    .S2X    A3                ; |437| 
           LDDW    .D2T2   *B4,B5:B4         ; |437| 
           ADDKPC  .S2     RL1211,B3,3       ; |437| 
RL1211:    ; CALL OCCURS {__mpyd}            ; |437| 
           MVKL    .S2     _cos,B4           ; |438| 
           MVKL    .S2     _lamda$7,B5       ; |438| 
           MVKH    .S2     _cos,B4           ; |438| 
           MVKH    .S2     _lamda$7,B5       ; |438| 
           CALL    .S2     B4                ; |438| 

           LDDW    .D2T1   *B5,A5:A4         ; |438| 
||         MV      .L1     A5,A7             ; |437| 
||         MV      .S1     A4,A6             ; |437| 

           MVKL    .S1     _dtm1$29,A3       ; |437| 
           MVKH    .S1     _dtm1$29,A3       ; |437| 
           ADDKPC  .S2     RL1213,B3,0       ; |438| 
           STDW    .D1T1   A7:A6,*A3         ; |437| 
RL1213:    ; CALL OCCURS {_cos}              ; |438| 
           MVKL    .S1     __mpyd,A3         ; |438| 
           MVKH    .S1     __mpyd,A3         ; |438| 
           MVKL    .S1     _dtm1$29,A6       ; |438| 

           CALL    .S2X    A3                ; |438| 
||         MVKH    .S1     _dtm1$29,A6       ; |438| 

           LDDW    .D1T2   *A6,B5:B4         ; |438| 
           ADDKPC  .S2     RL1214,B3,3       ; |438| 
RL1214:    ; CALL OCCURS {__mpyd}            ; |438| 
           LDW     .D2T1   *+SP(4),A10       ; |438| 
           MVKL    .S1     _sin,A3           ; |439| 
           MVKH    .S1     _sin,A3           ; |439| 
           MVKL    .S2     _lamda$7,B4       ; |439| 
           MVKH    .S2     _lamda$7,B4       ; |439| 

           CALL    .S2X    A3                ; |439| 
||         STDW    .D1T1   A5:A4,*A10        ; |438| 

           LDDW    .D2T1   *B4,A5:A4         ; |439| 
           ADDKPC  .S2     RL1216,B3,3       ; |439| 
RL1216:    ; CALL OCCURS {_sin}              ; |439| 
           MVKL    .S2     __mpyd,B6         ; |439| 

           MVKH    .S2     __mpyd,B6         ; |439| 
||         MVKL    .S1     _dtm1$29,A3       ; |439| 

           CALL    .S2     B6                ; |439| 
||         MVKH    .S1     _dtm1$29,A3       ; |439| 

           LDDW    .D1T2   *A3,B5:B4         ; |439| 
           ADDKPC  .S2     RL1217,B3,3       ; |439| 
RL1217:    ; CALL OCCURS {__mpyd}            ; |439| 
           MVKL    .S1     _sin,A3           ; |440| 
           MVKH    .S1     _sin,A3           ; |440| 
           MVKL    .S1     _beta$8,A8        ; |440| 

           CALL    .S2X    A3                ; |440| 
||         MVKH    .S1     _beta$8,A8        ; |440| 

           LDDW    .D1T1   *A8,A5:A4         ; |440| 
||         MV      .L1     A5,A7             ; |439| 
||         MV      .S1     A4,A6             ; |439| 

           ADDKPC  .S2     RL1219,B3,0       ; |440| 
           STDW    .D1T1   A7:A6,*+A10(8)    ; |439| 
           NOP             2
RL1219:    ; CALL OCCURS {_sin}              ; |440| 
           MVKL    .S2     __mpyd,B6         ; |440| 
           MVKL    .S2     _rm$10,B4         ; |440| 
           MVKH    .S2     __mpyd,B6         ; |440| 
           MVKH    .S2     _rm$10,B4         ; |440| 
           CALL    .S2     B6                ; |440| 
           LDDW    .D2T2   *B4,B5:B4         ; |440| 
           ADDKPC  .S2     RL1220,B3,3       ; |440| 
RL1220:    ; CALL OCCURS {__mpyd}            ; |440| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(132),B3      ; |442| 
           LDDW    .D2T1   *+SP(120),A13:A12 ; |442| 
           LDW     .D2T1   *+SP(128),A14     ; |442| 
           LDDW    .D2T2   *+SP(136),B11:B10 ; |442| 
           LDDW    .D2T2   *+SP(144),B13:B12 ; |442| 

           RET     .S2     B3                ; |442| 
||         LDW     .D2T1   *+SP(152),A15     ; |442| 

           LDDW    .D2T1   *+SP(112),A11:A10 ; |442| 
||         MV      .L1     A10,A3            ; |440| 

           NOP             2
           ADDK    .S2     152,SP            ; |442| 
	.dwpsn	"SunMoonGravitation.c",442,1
           STDW    .D1T1   A5:A4,*+A3(16)    ; |440| 
           ; BRANCH OCCURS {B3}              ; |442| 
	.dwattr DW$32, DW_AT_end_file("SunMoonGravitation.c")
	.dwattr DW$32, DW_AT_end_line(0x1ba)
	.dwattr DW$32, DW_AT_end_column(0x01)
	.dwendtag DW$32

;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_memset
	.global	_sqrt
	.global	_sin
	.global	_cos
	.global	__mpyd
	.global	__subd
	.global	__cmpd
	.global	__negd
	.global	__addd
	.global	__divd
	.global	__fixdu
	.global	__fltud
	.global	__remu

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr DW$T$3, DW_AT_address_class(0x20)

DW$T$24	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$3)
	.dwattr DW$T$24, DW_AT_language(DW_LANG_C)
DW$65	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$66	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$67	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$23)
	.dwendtag DW$T$24


DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$30, DW_AT_language(DW_LANG_C)
DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$22)
DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$70	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$23)
	.dwendtag DW$T$30


DW$T$33	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$33, DW_AT_language(DW_LANG_C)
DW$71	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
	.dwendtag DW$T$33


DW$T$36	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$36, DW_AT_language(DW_LANG_C)
DW$72	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$32)
DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$34)
DW$74	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$75	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$35)
DW$76	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$34)
	.dwendtag DW$T$36


DW$T$39	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$39, DW_AT_language(DW_LANG_C)
DW$77	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$38)
	.dwendtag DW$T$39


DW$T$41	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$41, DW_AT_language(DW_LANG_C)
DW$79	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
DW$80	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$34)
	.dwendtag DW$T$41

DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("size_t"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$23, DW_AT_language(DW_LANG_C)
DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("uint32"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$35, DW_AT_language(DW_LANG_C)
DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)
DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)
DW$T$34	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$34, DW_AT_address_class(0x20)
DW$T$38	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$37)
	.dwattr DW$T$38, DW_AT_address_class(0x20)

DW$T$53	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$17)
	.dwattr DW$T$53, DW_AT_language(DW_LANG_C)
DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$17)
	.dwendtag DW$T$53

DW$T$32	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$31)
	.dwattr DW$T$32, DW_AT_address_class(0x20)
DW$T$22	.dwtag  DW_TAG_restrict_type
	.dwattr DW$T$22, DW_AT_type(*DW$T$3)
DW$T$29	.dwtag  DW_TAG_restrict_type
	.dwattr DW$T$29, DW_AT_type(*DW$T$28)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)

DW$T$37	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$37, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$37, DW_AT_byte_size(0x10)
DW$82	.dwtag  DW_TAG_subrange_type
	.dwattr DW$82, DW_AT_upper_bound(0x01)
	.dwendtag DW$T$37

DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("tSunMoonData"), DW_AT_type(*DW$T$21)
	.dwattr DW$T$31, DW_AT_language(DW_LANG_C)
DW$T$28	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$27)
	.dwattr DW$T$28, DW_AT_address_class(0x20)

DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr DW$T$21, DW_AT_name("__tSunMoonData")
	.dwattr DW$T$21, DW_AT_byte_size(0x30)
DW$83	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$83, DW_AT_name("mjdTau"), DW_AT_symbol_name("_mjdTau")
	.dwattr DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr DW$83, DW_AT_accessibility(DW_ACCESS_public)
DW$84	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$84, DW_AT_name("dATex"), DW_AT_symbol_name("_dATex")
	.dwattr DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr DW$84, DW_AT_accessibility(DW_ACCESS_public)
DW$85	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$19)
	.dwattr DW$85, DW_AT_name("mjdMean_t"), DW_AT_symbol_name("_mjdMean_t")
	.dwattr DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr DW$85, DW_AT_accessibility(DW_ACCESS_public)
DW$86	.dwtag  DW_TAG_member, DW_AT_type(*DW$T$20)
	.dwattr DW$86, DW_AT_name("Mean_g"), DW_AT_symbol_name("_Mean_g")
	.dwattr DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr DW$86, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag DW$T$21

DW$T$27	.dwtag  DW_TAG_const_type

DW$T$20	.dwtag  DW_TAG_array_type, DW_AT_type(*DW$T$19)
	.dwattr DW$T$20, DW_AT_language(DW_LANG_C)
	.dwattr DW$T$20, DW_AT_byte_size(0x18)
DW$87	.dwtag  DW_TAG_subrange_type
	.dwattr DW$87, DW_AT_upper_bound(0x02)
	.dwendtag DW$T$20


	.dwattr DW$13, DW_AT_external(0x01)
	.dwattr DW$11, DW_AT_external(0x01)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$88	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$88, DW_AT_location[DW_OP_reg0]
DW$89	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$89, DW_AT_location[DW_OP_reg1]
DW$90	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$90, DW_AT_location[DW_OP_reg2]
DW$91	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$91, DW_AT_location[DW_OP_reg3]
DW$92	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$92, DW_AT_location[DW_OP_reg4]
DW$93	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$93, DW_AT_location[DW_OP_reg5]
DW$94	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$94, DW_AT_location[DW_OP_reg6]
DW$95	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$95, DW_AT_location[DW_OP_reg7]
DW$96	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$96, DW_AT_location[DW_OP_reg8]
DW$97	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$97, DW_AT_location[DW_OP_reg9]
DW$98	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$98, DW_AT_location[DW_OP_reg10]
DW$99	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$99, DW_AT_location[DW_OP_reg11]
DW$100	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$100, DW_AT_location[DW_OP_reg12]
DW$101	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$101, DW_AT_location[DW_OP_reg13]
DW$102	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$102, DW_AT_location[DW_OP_reg14]
DW$103	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$103, DW_AT_location[DW_OP_reg15]
DW$104	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$104, DW_AT_location[DW_OP_reg16]
DW$105	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$105, DW_AT_location[DW_OP_reg17]
DW$106	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$106, DW_AT_location[DW_OP_reg18]
DW$107	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$107, DW_AT_location[DW_OP_reg19]
DW$108	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$108, DW_AT_location[DW_OP_reg20]
DW$109	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$109, DW_AT_location[DW_OP_reg21]
DW$110	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$110, DW_AT_location[DW_OP_reg22]
DW$111	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$111, DW_AT_location[DW_OP_reg23]
DW$112	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$112, DW_AT_location[DW_OP_reg24]
DW$113	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$113, DW_AT_location[DW_OP_reg25]
DW$114	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$114, DW_AT_location[DW_OP_reg26]
DW$115	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$115, DW_AT_location[DW_OP_reg27]
DW$116	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$116, DW_AT_location[DW_OP_reg28]
DW$117	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$117, DW_AT_location[DW_OP_reg29]
DW$118	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$118, DW_AT_location[DW_OP_reg30]
DW$119	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$119, DW_AT_location[DW_OP_reg31]
DW$120	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$120, DW_AT_location[DW_OP_regx 0x20]
DW$121	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$121, DW_AT_location[DW_OP_regx 0x21]
DW$122	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$122, DW_AT_location[DW_OP_regx 0x22]
DW$123	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$123, DW_AT_location[DW_OP_regx 0x23]
DW$124	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$124, DW_AT_location[DW_OP_regx 0x24]
DW$125	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$125, DW_AT_location[DW_OP_regx 0x25]
DW$126	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$126, DW_AT_location[DW_OP_regx 0x26]
DW$127	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$127, DW_AT_location[DW_OP_regx 0x27]
DW$128	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$128, DW_AT_location[DW_OP_regx 0x28]
DW$129	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$129, DW_AT_location[DW_OP_regx 0x29]
DW$130	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$130, DW_AT_location[DW_OP_regx 0x2a]
DW$131	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$131, DW_AT_location[DW_OP_regx 0x2b]
DW$132	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$132, DW_AT_location[DW_OP_regx 0x2c]
DW$133	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$133, DW_AT_location[DW_OP_regx 0x2d]
DW$134	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$134, DW_AT_location[DW_OP_regx 0x2e]
DW$135	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$135, DW_AT_location[DW_OP_regx 0x2f]
DW$136	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$136, DW_AT_location[DW_OP_regx 0x30]
DW$137	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$137, DW_AT_location[DW_OP_regx 0x31]
DW$138	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$138, DW_AT_location[DW_OP_regx 0x32]
DW$139	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$139, DW_AT_location[DW_OP_regx 0x33]
DW$140	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$140, DW_AT_location[DW_OP_regx 0x34]
DW$141	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$141, DW_AT_location[DW_OP_regx 0x35]
DW$142	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$142, DW_AT_location[DW_OP_regx 0x36]
DW$143	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$143, DW_AT_location[DW_OP_regx 0x37]
DW$144	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$144, DW_AT_location[DW_OP_regx 0x38]
DW$145	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$145, DW_AT_location[DW_OP_regx 0x39]
DW$146	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$146, DW_AT_location[DW_OP_regx 0x3a]
DW$147	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$147, DW_AT_location[DW_OP_regx 0x3b]
DW$148	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$148, DW_AT_location[DW_OP_regx 0x3c]
DW$149	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$149, DW_AT_location[DW_OP_regx 0x3d]
DW$150	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$150, DW_AT_location[DW_OP_regx 0x3e]
DW$151	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$151, DW_AT_location[DW_OP_regx 0x3f]
DW$152	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$152, DW_AT_location[DW_OP_regx 0x40]
DW$153	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$153, DW_AT_location[DW_OP_regx 0x41]
DW$154	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$154, DW_AT_location[DW_OP_regx 0x42]
DW$155	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$155, DW_AT_location[DW_OP_regx 0x43]
DW$156	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$156, DW_AT_location[DW_OP_regx 0x44]
DW$157	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$157, DW_AT_location[DW_OP_regx 0x45]
DW$158	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$158, DW_AT_location[DW_OP_regx 0x46]
DW$159	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$159, DW_AT_location[DW_OP_regx 0x47]
DW$160	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$160, DW_AT_location[DW_OP_regx 0x48]
DW$161	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$161, DW_AT_location[DW_OP_regx 0x49]
DW$162	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$162, DW_AT_location[DW_OP_regx 0x4a]
DW$163	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$163, DW_AT_location[DW_OP_regx 0x4b]
DW$164	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$164, DW_AT_location[DW_OP_regx 0x4c]
DW$165	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$165, DW_AT_location[DW_OP_regx 0x4d]
DW$166	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$166, DW_AT_location[DW_OP_regx 0x4e]
DW$167	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$167, DW_AT_location[DW_OP_regx 0x4f]
DW$168	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$168, DW_AT_location[DW_OP_regx 0x50]
DW$169	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$169, DW_AT_location[DW_OP_regx 0x51]
DW$170	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$170, DW_AT_location[DW_OP_regx 0x52]
DW$171	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$171, DW_AT_location[DW_OP_regx 0x53]
DW$172	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$172, DW_AT_location[DW_OP_regx 0x54]
DW$173	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$173, DW_AT_location[DW_OP_regx 0x55]
DW$174	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$174, DW_AT_location[DW_OP_regx 0x56]
DW$175	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$175, DW_AT_location[DW_OP_regx 0x57]
DW$176	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$176, DW_AT_location[DW_OP_regx 0x58]
DW$177	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$177, DW_AT_location[DW_OP_regx 0x59]
DW$178	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$178, DW_AT_location[DW_OP_regx 0x5a]
DW$179	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$179, DW_AT_location[DW_OP_regx 0x5b]
DW$180	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$180, DW_AT_location[DW_OP_regx 0x5c]
DW$181	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$181, DW_AT_location[DW_OP_regx 0x5d]
DW$182	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$182, DW_AT_location[DW_OP_regx 0x5e]
DW$183	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$183, DW_AT_location[DW_OP_regx 0x5f]
DW$184	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$184, DW_AT_location[DW_OP_regx 0x60]
DW$185	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$185, DW_AT_location[DW_OP_regx 0x61]
DW$186	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$186, DW_AT_location[DW_OP_regx 0x62]
DW$187	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$187, DW_AT_location[DW_OP_regx 0x63]
DW$188	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$188, DW_AT_location[DW_OP_regx 0x64]
DW$189	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$189, DW_AT_location[DW_OP_regx 0x65]
DW$190	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$190, DW_AT_location[DW_OP_regx 0x66]
DW$191	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$191, DW_AT_location[DW_OP_regx 0x67]
DW$192	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$192, DW_AT_location[DW_OP_regx 0x68]
DW$193	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$193, DW_AT_location[DW_OP_regx 0x69]
DW$194	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$194, DW_AT_location[DW_OP_regx 0x6a]
DW$195	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$195, DW_AT_location[DW_OP_regx 0x6b]
DW$196	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$196, DW_AT_location[DW_OP_regx 0x6c]
DW$197	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$197, DW_AT_location[DW_OP_regx 0x6d]
DW$198	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$198, DW_AT_location[DW_OP_regx 0x6e]
DW$199	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$199, DW_AT_location[DW_OP_regx 0x6f]
DW$200	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$200, DW_AT_location[DW_OP_regx 0x70]
DW$201	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$201, DW_AT_location[DW_OP_regx 0x71]
DW$202	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$202, DW_AT_location[DW_OP_regx 0x72]
DW$203	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$203, DW_AT_location[DW_OP_regx 0x73]
DW$204	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$204, DW_AT_location[DW_OP_regx 0x74]
DW$205	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$205, DW_AT_location[DW_OP_regx 0x75]
DW$206	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$206, DW_AT_location[DW_OP_regx 0x76]
DW$207	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$207, DW_AT_location[DW_OP_regx 0x77]
DW$208	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$208, DW_AT_location[DW_OP_regx 0x78]
DW$209	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$209, DW_AT_location[DW_OP_regx 0x79]
DW$210	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$210, DW_AT_location[DW_OP_regx 0x7a]
DW$211	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$211, DW_AT_location[DW_OP_regx 0x7b]
DW$212	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$212, DW_AT_location[DW_OP_regx 0x7c]
DW$213	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$213, DW_AT_location[DW_OP_regx 0x7d]
DW$214	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$214, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

