;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.1.0 *
;* Date/Time created: Tue Aug 30 14:48:57 2016                                *
;******************************************************************************
	.compiler_opts --endian=little --mem_model:code=far --mem_model:data=far --predefine_memory_model_macros --quiet --silicon_version=6400 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C64xx                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : 8000                                                 *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : Optimized w/Profiling Info                           *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr DW$CU, DW_AT_name("integration.c")
	.dwattr DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v5.1.0 Copyright (c) 1996-2005 Texas Instruments Incorporated")
	.dwattr DW$CU, DW_AT_stmt_list(0x00)
	.dwattr DW$CU, DW_AT_TI_VERSION(0x01)

DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("memset"), DW_AT_symbol_name("_memset")
	.dwattr DW$1, DW_AT_type(*DW$T$3)
	.dwattr DW$1, DW_AT_declaration(0x01)
	.dwattr DW$1, DW_AT_external(0x01)
DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$1

;	C:\CCStudio_v3.1\C6000\cgtools\bin\opt6x.exe C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI9042 C:\DOCUME~1\XPMUser\LOCALS~1\Temp\TI9044 
	.sect	".text"
	.global	_rk4

DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("rk4"), DW_AT_symbol_name("_rk4")
	.dwattr DW$5, DW_AT_low_pc(_rk4)
	.dwattr DW$5, DW_AT_high_pc(0x00)
	.dwattr DW$5, DW_AT_begin_file("integration.c")
	.dwattr DW$5, DW_AT_begin_line(0x1f)
	.dwattr DW$5, DW_AT_begin_column(0x06)
	.dwattr DW$5, DW_AT_frame_base[DW_OP_breg31 184]
	.dwattr DW$5, DW_AT_skeletal(0x01)
	.dwpsn	"integration.c",32,1

;******************************************************************************
;* FUNCTION NAME: _rk4                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 136 Auto + 44 Save = 180 byte               *
;******************************************************************************
_rk4:
;** --------------------------------------------------------------------------*
DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("fp"), DW_AT_symbol_name("_fp")
	.dwattr DW$6, DW_AT_type(*DW$T$33)
	.dwattr DW$6, DW_AT_location[DW_OP_reg4]
DW$7	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Q"), DW_AT_symbol_name("_Q")
	.dwattr DW$7, DW_AT_type(*DW$T$29)
	.dwattr DW$7, DW_AT_location[DW_OP_reg20]
DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_name("cnt"), DW_AT_symbol_name("_cnt")
	.dwattr DW$8, DW_AT_type(*DW$T$30)
	.dwattr DW$8, DW_AT_location[DW_OP_reg6]
DW$9	.dwtag  DW_TAG_formal_parameter, DW_AT_name("h"), DW_AT_symbol_name("_h")
	.dwattr DW$9, DW_AT_type(*DW$T$28)
	.dwattr DW$9, DW_AT_location[DW_OP_reg22]
DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pParam"), DW_AT_symbol_name("_pParam")
	.dwattr DW$10, DW_AT_type(*DW$T$30)
	.dwattr DW$10, DW_AT_location[DW_OP_reg8]
DW$11	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dQ"), DW_AT_symbol_name("_dQ")
	.dwattr DW$11, DW_AT_type(*DW$T$29)
	.dwattr DW$11, DW_AT_location[DW_OP_reg24]

           MV      .L1X    SP,A31            ; |32| 
||         ADDK    .S2     -184,SP           ; |32| 

           STDW    .D2T2   B11:B10,*+SP(168)
||         MV      .L2X    A6,B11            ; |32| 
||         ADD     .S2     8,SP,B5
||         MV      .L1X    B4,A3             ; |32| 
||         STW     .D1T1   A14,*-A31(24)

           STW     .D2T2   B4,*+SP(124)      ; |32| 
||         EXT     .S2     B11,3,3,B4        ; |40| 
||         STDW    .D1T1   A13:A12,*-A31(32)

           SHL     .S1X    B11,3,A0          ; |38| 
||         STDW    .D1T1   A11:A10,*-A31(40)
||         STDW    .D2T2   B13:B12,*+SP(176)
||         MV      .L2     B6,B13            ; |32| 

   [!A0]   B       .S2     L4                ; |38| 
|| [ A0]   MV      .L1X    B5,A6
||         STW     .D2T1   A15,*+SP(184)
|| [ A0]   SUB     .L2     B4,1,B0
|| [!A0]   MVKL    .S1     _memset,A3        ; |38| 

           STW     .D2T2   B3,*+SP(164)
||         MV      .L1X    B7,A15            ; |32| 
|| [!A0]   MVKH    .S1     _memset,A3        ; |38| 

           STW     .D2T1   A4,*+SP(120)      ; |32| 
           STW     .D2T1   A8,*+SP(128)      ; |32| 

           STW     .D2T2   B8,*+SP(132)      ; |32| 
|| [!A0]   CALL    .S2X    A3                ; |38| 

   [!A0]   LDW     .D2T1   *+SP(132),A4      ; |38| 
           ; BRANCHCC OCCURS {L4}            ; |38| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 38
;*      Loop opening brace source line   : 39
;*      Loop closing brace source line   : 41
;*      Loop Unroll Multiple             : 8x
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        1     
;*      .D units                     2*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             2*       0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        1     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 1 iterations in parallel
;*      Done
;*
;*      Loop is interruptible
;*      Collapsed epilog stages     : 0
;*      Collapsed prolog stages     : 0
;*
;*      Minimum safe trip count     : 1 (after unrolling)
;*----------------------------------------------------------------------------*
L1:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
L2:    ; PIPED LOOP KERNEL
DW$L$_rk4$3$B:
	.dwpsn	"integration.c",39,0

   [ B0]   BDEC    .S2     L2,B0             ; |38| <0,0> 
||         LDDW    .D1T1   *A3++,A5:A4       ; |40| <0,0> 

           NOP             4
	.dwpsn	"integration.c",41,0
           STDW    .D1T1   A5:A4,*A6++       ; |40| <0,5> 
DW$L$_rk4$3$E:
;** --------------------------------------------------------------------------*
L3:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
           MVKL    .S1     _memset,A3        ; |38| 
           MVKH    .S1     _memset,A3        ; |38| 
           LDW     .D2T1   *+SP(132),A4      ; |38| 
           CALL    .S2X    A3                ; |38| 
           NOP             1
;** --------------------------------------------------------------------------*
L4:    
           ADDKPC  .S2     RL0,B3,1          ; |38| 
           ZERO    .L2     B4                ; |38| 
           MV      .L1     A0,A6             ; |38| 
RL0:       ; CALL OCCURS {_memset}           ; |38| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(120),B5      ; |42| 
           NOP             4
           CALL    .S2     B5                ; |42| 
           LDW     .D2T1   *+SP(128),A6
	.dwpsn	"integration.c",40,0
           MVK     .L2     0x1,B12           ; |40| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP L5
;** --------------------------------------------------------------------------*
L5:    
DW$L$_rk4$8$B:
	.dwpsn	"integration.c",41,0
           ADDKPC  .S2     RL1,B3,0          ; |42| 
           ADD     .L1X    8,SP,A4           ; |42| 
           ADDAD   .D2     SP,8,B4           ; |42| 
RL1:       ; CALL OCCURS {VR132}             ; |42| 
DW$L$_rk4$8$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$9$B:

           CMPGT   .L2     B11,0,B0          ; |44| 
||         MVKL    .S2     __mpyd,B6         ; |46| 
||         ADDAD   .D2     SP,7,B4

   [!B0]   BNOP    .S1     L11,5             ; |44| 
||         MVKH    .S2     __mpyd,B6         ; |46| 

           ; BRANCHCC OCCURS {L11}           ; |44| 
DW$L$_rk4$9$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$10$B:

           MV      .L1X    B4,A14            ; Register A/B partition copy
||         STW     .D2T2   B11,*+SP(136)     ; |46| 

           LDDW    .D1T1   *++A14,A5:A4      ; |46| 
||         CALL    .S2     B6                ; |46| 
||         LDW     .D2T2   *+SP(124),B10
||         ADD     .L1X    8,SP,A11

           LDW     .D2T1   *+SP(132),A10
	.dwpsn	"integration.c",44,0
           NOP             2
DW$L$_rk4$10$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
L6:    
DW$L$_rk4$11$B:
	.dwpsn	"integration.c",45,0
           MV      .L2X    A15,B5            ; |46| 

           ADDKPC  .S2     RL2,B3,0          ; |46| 
||         MV      .D2     B13,B4            ; |46| 

RL2:       ; CALL OCCURS {__mpyd}            ; |46| 
DW$L$_rk4$11$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$12$B:

           MVKL    .S1     __divd,A3         ; |48| 
||         CMPEQ   .L1X    B12,2,A0
||         MV      .D1     A5,A12            ; |46| 

           MVKH    .S1     __divd,A3         ; |48| 
|| [ A0]   B       .S2     L7
|| [!A0]   ZERO    .L2     B5                ; |48| 
||         MV      .L1     A4,A13            ; |46| 
|| [ A0]   ZERO    .D2     B5                ; |48| 

   [ A0]   MVKL    .S1     __divd,A3         ; |48| 

   [!A0]   CALL    .S2X    A3                ; |48| 
|| [ A0]   MVKH    .S1     __divd,A3         ; |48| 

           NOP             1
   [ A0]   CALL    .S2X    A3                ; |48| 
           NOP             1
           ; BRANCHCC OCCURS {L7} 
DW$L$_rk4$12$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$13$B:
           MVKH    .S2     0x40180000,B5     ; |48| 

           ADDKPC  .S2     RL4,B3,0          ; |48| 
||         ZERO    .L2     B4                ; |48| 

RL4:       ; CALL OCCURS {__divd}            ; |48| 
DW$L$_rk4$13$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$14$B:
           MVKL    .S1     __addd,A3         ; |48| 
           MVKH    .S1     __addd,A3         ; |48| 
           LDDW    .D1T1   *A10,A7:A6        ; |48| 
           CALL    .S2X    A3                ; |48| 
           MV      .L2X    A4,B4             ; |48| 
           ADDKPC  .S2     RL5,B3,0          ; |48| 
           MV      .L2X    A5,B5             ; |48| 
           MV      .L1     A7,A5             ; |48| 
           MV      .S1     A6,A4             ; |48| 
RL5:       ; CALL OCCURS {__addd}            ; |48| 
DW$L$_rk4$14$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$15$B:

           B       .S1     L8
||         STDW    .D1T1   A5:A4,*A10        ; |48| 
||         CMPEQ   .L2     B12,3,B0

   [ B0]   BNOP    .S1     L9,4
           ; BRANCH OCCURS {L8} 
DW$L$_rk4$15$E:
;** --------------------------------------------------------------------------*
L7:    
DW$L$_rk4$16$B:
           MVKH    .S2     0x40080000,B5     ; |48| 
           ADDKPC  .S2     RL7,B3,1          ; |48| 
           ZERO    .L2     B4                ; |48| 
RL7:       ; CALL OCCURS {__divd}            ; |48| 
DW$L$_rk4$16$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$17$B:
           MVKL    .S1     __addd,A3         ; |48| 
           MVKH    .S1     __addd,A3         ; |48| 
           LDDW    .D1T1   *A10,A7:A6        ; |48| 
           CALL    .S2X    A3                ; |48| 
           MV      .L2X    A4,B4             ; |48| 
           ADDKPC  .S2     RL8,B3,0          ; |48| 
           MV      .L2X    A5,B5             ; |48| 
           MV      .L1     A7,A5             ; |48| 
           MV      .S1     A6,A4             ; |48| 
RL8:       ; CALL OCCURS {__addd}            ; |48| 
DW$L$_rk4$17$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$18$B:
           CMPEQ   .L2     B12,3,B0
   [ B0]   BNOP    .S1     L9,3
           STDW    .D1T1   A5:A4,*A10        ; |48| 
DW$L$_rk4$18$E:
;** --------------------------------------------------------------------------*
L8:    
DW$L$_rk4$19$B:
   [!B0]   MVKL    .S1     __mpyd,A3         ; |55| 
           ; BRANCHCC OCCURS {L9} 
DW$L$_rk4$19$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$20$B:

           MVKH    .S1     __mpyd,A3         ; |55| 
||         ZERO    .L2     B5                ; |55| 

           MVKH    .S2     0x3fe00000,B5     ; |55| 
           CALL    .S2X    A3                ; |55| 
           ADDKPC  .S2     RL10,B3,1         ; |55| 
           MV      .L1     A12,A5            ; |55| 
           MV      .S1     A13,A4            ; |55| 
           ZERO    .L2     B4                ; |55| 
RL10:      ; CALL OCCURS {__mpyd}            ; |55| 
DW$L$_rk4$20$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$21$B:
           MVKL    .S1     __addd,A3         ; |55| 
           MVKH    .S1     __addd,A3         ; |55| 
           LDDW    .D2T2   *B10,B7:B6        ; |55| 
           CALL    .S2X    A3                ; |55| 
           MV      .L2X    A4,B4             ; |55| 
           ADDKPC  .S2     RL11,B3,0         ; |55| 
           MV      .L2X    A5,B5             ; |55| 
           MV      .L1X    B6,A4             ; |55| 
           MV      .L1X    B7,A5             ; |55| 
RL11:      ; CALL OCCURS {__addd}            ; |55| 
DW$L$_rk4$21$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$22$B:

           BNOP    .S1     L10,4
||         STDW    .D1T1   A5:A4,*A11        ; |55| 
||         LDW     .D2T2   *+SP(136),B4

           SUB     .L1X    B4,1,A0           ; |44| 
           ; BRANCH OCCURS {L10} 
DW$L$_rk4$22$E:
;** --------------------------------------------------------------------------*
L9:    
DW$L$_rk4$23$B:

           MVKL    .S1     __divd,A3         ; |54| 
||         ZERO    .L2     B5                ; |54| 

           MVKH    .S1     __divd,A3         ; |54| 
||         MVKH    .S2     0x40180000,B5     ; |54| 

           MV      .L1     A12,A5            ; |54| 
           CALL    .S2X    A3                ; |54| 
           ADDKPC  .S2     RL13,B3,2         ; |54| 
           MV      .S1     A13,A4            ; |54| 
           ZERO    .L2     B4                ; |54| 
RL13:      ; CALL OCCURS {__divd}            ; |54| 
DW$L$_rk4$23$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$24$B:
           MVKL    .S2     __addd,B6         ; |54| 
           MVKH    .S2     __addd,B6         ; |54| 

           CALL    .S2     B6                ; |54| 
||         LDDW    .D1T1   *A10,A7:A6        ; |54| 

           MV      .L2X    A4,B4             ; |54| 
           ADDKPC  .S2     RL14,B3,1         ; |54| 
           MV      .L2X    A5,B5             ; |54| 

           MV      .L1     A7,A5             ; |54| 
||         MV      .S1     A6,A4             ; |54| 

RL14:      ; CALL OCCURS {__addd}            ; |54| 
           MVKL    .S1     __addd,A3         ; |55| 

           STDW    .D1T1   A5:A4,*A10        ; |54| 
||         MVKH    .S1     __addd,A3         ; |55| 

           LDDW    .D2T2   *B10,B7:B6        ; |55| 
           CALL    .S2X    A3                ; |55| 
           MV      .L2X    A13,B4            ; |55| 
           MV      .L2X    A12,B5            ; |55| 
           ADDKPC  .S2     RL15,B3,0         ; |55| 
           MV      .L1X    B6,A4             ; |55| 
           MV      .L1X    B7,A5             ; |55| 
RL15:      ; CALL OCCURS {__addd}            ; |55| 
DW$L$_rk4$24$E:
;** --------------------------------------------------------------------------*
DW$L$_rk4$25$B:
           LDW     .D2T2   *+SP(136),B4
           NOP             3
           STDW    .D1T1   A5:A4,*A11        ; |55| 
           SUB     .L1X    B4,1,A0           ; |44| 
DW$L$_rk4$25$E:
;** --------------------------------------------------------------------------*
L10:    
DW$L$_rk4$26$B:

   [ A0]   B       .S1     L6                ; |44| 
||         SUB     .L2     B4,1,B4           ; |44| 
||         ADD     .L1     8,A10,A10         ; |44| 
||         ADD     .D2     8,B10,B10         ; |44| 
||         ADD     .D1     8,A11,A11         ; |44| 
||         MVKL    .S2     __mpyd,B6         ; |46| 

           STW     .D2T2   B4,*+SP(136)      ; |44| 
||         MVKH    .S2     __mpyd,B6         ; |46| 

   [ A0]   CALL    .S2     B6                ; |46| 
|| [ A0]   LDDW    .D1T1   *++A14,A5:A4      ; |46| 

	.dwpsn	"integration.c",59,0
           NOP             3
           ; BRANCHCC OCCURS {L6}            ; |44| 
DW$L$_rk4$26$E:
;** --------------------------------------------------------------------------*
L11:    
DW$L$_rk4$27$B:

           LDW     .D2T2   *+SP(120),B5      ; |42| 
||         ADD     .L2     1,B12,B12         ; |40| 

           CMPGT   .L2     B12,4,B0          ; |40| 

   [!B0]   B       .S1     L5                ; |40| 
|| [ B0]   LDW     .D2T2   *+SP(164),B3      ; |61| 

   [ B0]   LDDW    .D2T2   *+SP(176),B13:B12 ; |61| 
   [ B0]   LDW     .D2T1   *+SP(184),A15     ; |61| 

   [!B0]   CALL    .S2     B5                ; |42| 
|| [ B0]   LDDW    .D2T1   *+SP(144),A11:A10 ; |61| 

   [!B0]   LDW     .D2T1   *+SP(128),A6
	.dwpsn	"integration.c",60,0
   [ B0]   LDDW    .D2T1   *+SP(152),A13:A12 ; |61| 
           ; BRANCHCC OCCURS {L5}            ; |40| 
DW$L$_rk4$27$E:
;** --------------------------------------------------------------------------*

           RET     .S2     B3                ; |61| 
||         LDDW    .D2T2   *+SP(168),B11:B10 ; |61| 

           LDW     .D2T1   *+SP(160),A14     ; |61| 
||         ADDK    .S2     184,SP            ; |61| 

	.dwpsn	"integration.c",61,1
           NOP             4
           ; BRANCH OCCURS {B3}              ; |61| 

DW$12	.dwtag  DW_TAG_loop
	.dwattr DW$12, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\integration.asm:L5:1:1472539737")
	.dwattr DW$12, DW_AT_begin_file("integration.c")
	.dwattr DW$12, DW_AT_begin_line(0x28)
	.dwattr DW$12, DW_AT_end_line(0x3c)
DW$13	.dwtag  DW_TAG_loop_range
	.dwattr DW$13, DW_AT_low_pc(DW$L$_rk4$8$B)
	.dwattr DW$13, DW_AT_high_pc(DW$L$_rk4$8$E)
DW$14	.dwtag  DW_TAG_loop_range
	.dwattr DW$14, DW_AT_low_pc(DW$L$_rk4$10$B)
	.dwattr DW$14, DW_AT_high_pc(DW$L$_rk4$10$E)
DW$15	.dwtag  DW_TAG_loop_range
	.dwattr DW$15, DW_AT_low_pc(DW$L$_rk4$9$B)
	.dwattr DW$15, DW_AT_high_pc(DW$L$_rk4$9$E)
DW$16	.dwtag  DW_TAG_loop_range
	.dwattr DW$16, DW_AT_low_pc(DW$L$_rk4$27$B)
	.dwattr DW$16, DW_AT_high_pc(DW$L$_rk4$27$E)

DW$17	.dwtag  DW_TAG_loop
	.dwattr DW$17, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\integration.asm:L6:2:1472539737")
	.dwattr DW$17, DW_AT_begin_file("integration.c")
	.dwattr DW$17, DW_AT_begin_line(0x2c)
	.dwattr DW$17, DW_AT_end_line(0x3b)
DW$18	.dwtag  DW_TAG_loop_range
	.dwattr DW$18, DW_AT_low_pc(DW$L$_rk4$11$B)
	.dwattr DW$18, DW_AT_high_pc(DW$L$_rk4$11$E)
DW$19	.dwtag  DW_TAG_loop_range
	.dwattr DW$19, DW_AT_low_pc(DW$L$_rk4$23$B)
	.dwattr DW$19, DW_AT_high_pc(DW$L$_rk4$23$E)
DW$20	.dwtag  DW_TAG_loop_range
	.dwattr DW$20, DW_AT_low_pc(DW$L$_rk4$24$B)
	.dwattr DW$20, DW_AT_high_pc(DW$L$_rk4$24$E)
DW$21	.dwtag  DW_TAG_loop_range
	.dwattr DW$21, DW_AT_low_pc(DW$L$_rk4$16$B)
	.dwattr DW$21, DW_AT_high_pc(DW$L$_rk4$16$E)
DW$22	.dwtag  DW_TAG_loop_range
	.dwattr DW$22, DW_AT_low_pc(DW$L$_rk4$17$B)
	.dwattr DW$22, DW_AT_high_pc(DW$L$_rk4$17$E)
DW$23	.dwtag  DW_TAG_loop_range
	.dwattr DW$23, DW_AT_low_pc(DW$L$_rk4$12$B)
	.dwattr DW$23, DW_AT_high_pc(DW$L$_rk4$12$E)
DW$24	.dwtag  DW_TAG_loop_range
	.dwattr DW$24, DW_AT_low_pc(DW$L$_rk4$13$B)
	.dwattr DW$24, DW_AT_high_pc(DW$L$_rk4$13$E)
DW$25	.dwtag  DW_TAG_loop_range
	.dwattr DW$25, DW_AT_low_pc(DW$L$_rk4$14$B)
	.dwattr DW$25, DW_AT_high_pc(DW$L$_rk4$14$E)
DW$26	.dwtag  DW_TAG_loop_range
	.dwattr DW$26, DW_AT_low_pc(DW$L$_rk4$15$B)
	.dwattr DW$26, DW_AT_high_pc(DW$L$_rk4$15$E)
DW$27	.dwtag  DW_TAG_loop_range
	.dwattr DW$27, DW_AT_low_pc(DW$L$_rk4$18$B)
	.dwattr DW$27, DW_AT_high_pc(DW$L$_rk4$18$E)
DW$28	.dwtag  DW_TAG_loop_range
	.dwattr DW$28, DW_AT_low_pc(DW$L$_rk4$19$B)
	.dwattr DW$28, DW_AT_high_pc(DW$L$_rk4$19$E)
DW$29	.dwtag  DW_TAG_loop_range
	.dwattr DW$29, DW_AT_low_pc(DW$L$_rk4$20$B)
	.dwattr DW$29, DW_AT_high_pc(DW$L$_rk4$20$E)
DW$30	.dwtag  DW_TAG_loop_range
	.dwattr DW$30, DW_AT_low_pc(DW$L$_rk4$21$B)
	.dwattr DW$30, DW_AT_high_pc(DW$L$_rk4$21$E)
DW$31	.dwtag  DW_TAG_loop_range
	.dwattr DW$31, DW_AT_low_pc(DW$L$_rk4$22$B)
	.dwattr DW$31, DW_AT_high_pc(DW$L$_rk4$22$E)
DW$32	.dwtag  DW_TAG_loop_range
	.dwattr DW$32, DW_AT_low_pc(DW$L$_rk4$25$B)
	.dwattr DW$32, DW_AT_high_pc(DW$L$_rk4$25$E)
DW$33	.dwtag  DW_TAG_loop_range
	.dwattr DW$33, DW_AT_low_pc(DW$L$_rk4$26$B)
	.dwattr DW$33, DW_AT_high_pc(DW$L$_rk4$26$E)
	.dwendtag DW$17

	.dwendtag DW$12


DW$34	.dwtag  DW_TAG_loop
	.dwattr DW$34, DW_AT_name("\\tsclient\E\Alex\DSP GPSR\Dual Image+GNSSR\Project_SGR_V1_newICD_DualImage_GNSSR_ShortNAV_20160323\Common_Files\OP\Asm\integration.asm:L2:1:1472539737")
	.dwattr DW$34, DW_AT_begin_file("../Sources/baseop.h")
	.dwattr DW$34, DW_AT_begin_line(0x26)
	.dwattr DW$34, DW_AT_end_line(0x29)
DW$35	.dwtag  DW_TAG_loop_range
	.dwattr DW$35, DW_AT_low_pc(DW$L$_rk4$3$B)
	.dwattr DW$35, DW_AT_high_pc(DW$L$_rk4$3$E)
	.dwendtag DW$34

	.dwattr DW$5, DW_AT_end_file("integration.c")
	.dwattr DW$5, DW_AT_end_line(0x3d)
	.dwattr DW$5, DW_AT_end_column(0x01)
	.dwendtag DW$5

;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************
	.global	_memset
	.global	__mpyd
	.global	__divd
	.global	__addd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr DW$T$3, DW_AT_address_class(0x20)

DW$T$20	.dwtag  DW_TAG_subroutine_type, DW_AT_type(*DW$T$3)
	.dwattr DW$T$20, DW_AT_language(DW_LANG_C)
DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$3)
DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$10)
DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$20


DW$T$27	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$27, DW_AT_language(DW_LANG_C)
DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$22)
DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$26)
DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$19)
	.dwendtag DW$T$27

DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("tRightPart"), DW_AT_type(*DW$T$32)
	.dwattr DW$T$33, DW_AT_language(DW_LANG_C)

DW$T$36	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$36, DW_AT_language(DW_LANG_C)
DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$33)
DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$30)
DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$28)
DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$30)
DW$47	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
	.dwendtag DW$T$36

DW$T$10	.dwtag  DW_TAG_base_type, DW_AT_name("int")
	.dwattr DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr DW$T$10, DW_AT_byte_size(0x04)
DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("int32"), DW_AT_type(*DW$T$10)
	.dwattr DW$T$30, DW_AT_language(DW_LANG_C)
DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("size_t"), DW_AT_type(*DW$T$11)
	.dwattr DW$T$19, DW_AT_language(DW_LANG_C)
DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("float64"), DW_AT_type(*DW$T$17)
	.dwattr DW$T$28, DW_AT_language(DW_LANG_C)
DW$T$29	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$28)
	.dwattr DW$T$29, DW_AT_address_class(0x20)
DW$T$22	.dwtag  DW_TAG_restrict_type
	.dwattr DW$T$22, DW_AT_type(*DW$T$3)
DW$T$26	.dwtag  DW_TAG_restrict_type
	.dwattr DW$T$26, DW_AT_type(*DW$T$25)
DW$T$32	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$31)
	.dwattr DW$T$32, DW_AT_address_class(0x20)
DW$T$11	.dwtag  DW_TAG_base_type, DW_AT_name("unsigned int")
	.dwattr DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr DW$T$11, DW_AT_byte_size(0x04)
DW$T$17	.dwtag  DW_TAG_base_type, DW_AT_name("double")
	.dwattr DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr DW$T$17, DW_AT_byte_size(0x08)
DW$T$25	.dwtag  DW_TAG_pointer_type, DW_AT_type(*DW$T$24)
	.dwattr DW$T$25, DW_AT_address_class(0x20)

DW$T$31	.dwtag  DW_TAG_subroutine_type
	.dwattr DW$T$31, DW_AT_language(DW_LANG_C)
DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$49	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$29)
DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_type(*DW$T$30)
	.dwendtag DW$T$31

DW$T$24	.dwtag  DW_TAG_const_type

	.dwattr DW$5, DW_AT_external(0x01)
	.dwattr DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

DW$51	.dwtag  DW_TAG_assign_register, DW_AT_name("A0")
	.dwattr DW$51, DW_AT_location[DW_OP_reg0]
DW$52	.dwtag  DW_TAG_assign_register, DW_AT_name("A1")
	.dwattr DW$52, DW_AT_location[DW_OP_reg1]
DW$53	.dwtag  DW_TAG_assign_register, DW_AT_name("A2")
	.dwattr DW$53, DW_AT_location[DW_OP_reg2]
DW$54	.dwtag  DW_TAG_assign_register, DW_AT_name("A3")
	.dwattr DW$54, DW_AT_location[DW_OP_reg3]
DW$55	.dwtag  DW_TAG_assign_register, DW_AT_name("A4")
	.dwattr DW$55, DW_AT_location[DW_OP_reg4]
DW$56	.dwtag  DW_TAG_assign_register, DW_AT_name("A5")
	.dwattr DW$56, DW_AT_location[DW_OP_reg5]
DW$57	.dwtag  DW_TAG_assign_register, DW_AT_name("A6")
	.dwattr DW$57, DW_AT_location[DW_OP_reg6]
DW$58	.dwtag  DW_TAG_assign_register, DW_AT_name("A7")
	.dwattr DW$58, DW_AT_location[DW_OP_reg7]
DW$59	.dwtag  DW_TAG_assign_register, DW_AT_name("A8")
	.dwattr DW$59, DW_AT_location[DW_OP_reg8]
DW$60	.dwtag  DW_TAG_assign_register, DW_AT_name("A9")
	.dwattr DW$60, DW_AT_location[DW_OP_reg9]
DW$61	.dwtag  DW_TAG_assign_register, DW_AT_name("A10")
	.dwattr DW$61, DW_AT_location[DW_OP_reg10]
DW$62	.dwtag  DW_TAG_assign_register, DW_AT_name("A11")
	.dwattr DW$62, DW_AT_location[DW_OP_reg11]
DW$63	.dwtag  DW_TAG_assign_register, DW_AT_name("A12")
	.dwattr DW$63, DW_AT_location[DW_OP_reg12]
DW$64	.dwtag  DW_TAG_assign_register, DW_AT_name("A13")
	.dwattr DW$64, DW_AT_location[DW_OP_reg13]
DW$65	.dwtag  DW_TAG_assign_register, DW_AT_name("A14")
	.dwattr DW$65, DW_AT_location[DW_OP_reg14]
DW$66	.dwtag  DW_TAG_assign_register, DW_AT_name("A15")
	.dwattr DW$66, DW_AT_location[DW_OP_reg15]
DW$67	.dwtag  DW_TAG_assign_register, DW_AT_name("B0")
	.dwattr DW$67, DW_AT_location[DW_OP_reg16]
DW$68	.dwtag  DW_TAG_assign_register, DW_AT_name("B1")
	.dwattr DW$68, DW_AT_location[DW_OP_reg17]
DW$69	.dwtag  DW_TAG_assign_register, DW_AT_name("B2")
	.dwattr DW$69, DW_AT_location[DW_OP_reg18]
DW$70	.dwtag  DW_TAG_assign_register, DW_AT_name("B3")
	.dwattr DW$70, DW_AT_location[DW_OP_reg19]
DW$71	.dwtag  DW_TAG_assign_register, DW_AT_name("B4")
	.dwattr DW$71, DW_AT_location[DW_OP_reg20]
DW$72	.dwtag  DW_TAG_assign_register, DW_AT_name("B5")
	.dwattr DW$72, DW_AT_location[DW_OP_reg21]
DW$73	.dwtag  DW_TAG_assign_register, DW_AT_name("B6")
	.dwattr DW$73, DW_AT_location[DW_OP_reg22]
DW$74	.dwtag  DW_TAG_assign_register, DW_AT_name("B7")
	.dwattr DW$74, DW_AT_location[DW_OP_reg23]
DW$75	.dwtag  DW_TAG_assign_register, DW_AT_name("B8")
	.dwattr DW$75, DW_AT_location[DW_OP_reg24]
DW$76	.dwtag  DW_TAG_assign_register, DW_AT_name("B9")
	.dwattr DW$76, DW_AT_location[DW_OP_reg25]
DW$77	.dwtag  DW_TAG_assign_register, DW_AT_name("B10")
	.dwattr DW$77, DW_AT_location[DW_OP_reg26]
DW$78	.dwtag  DW_TAG_assign_register, DW_AT_name("B11")
	.dwattr DW$78, DW_AT_location[DW_OP_reg27]
DW$79	.dwtag  DW_TAG_assign_register, DW_AT_name("B12")
	.dwattr DW$79, DW_AT_location[DW_OP_reg28]
DW$80	.dwtag  DW_TAG_assign_register, DW_AT_name("B13")
	.dwattr DW$80, DW_AT_location[DW_OP_reg29]
DW$81	.dwtag  DW_TAG_assign_register, DW_AT_name("DP")
	.dwattr DW$81, DW_AT_location[DW_OP_reg30]
DW$82	.dwtag  DW_TAG_assign_register, DW_AT_name("SP")
	.dwattr DW$82, DW_AT_location[DW_OP_reg31]
DW$83	.dwtag  DW_TAG_assign_register, DW_AT_name("FP")
	.dwattr DW$83, DW_AT_location[DW_OP_regx 0x20]
DW$84	.dwtag  DW_TAG_assign_register, DW_AT_name("PC")
	.dwattr DW$84, DW_AT_location[DW_OP_regx 0x21]
DW$85	.dwtag  DW_TAG_assign_register, DW_AT_name("IRP")
	.dwattr DW$85, DW_AT_location[DW_OP_regx 0x22]
DW$86	.dwtag  DW_TAG_assign_register, DW_AT_name("IFR")
	.dwattr DW$86, DW_AT_location[DW_OP_regx 0x23]
DW$87	.dwtag  DW_TAG_assign_register, DW_AT_name("NRP")
	.dwattr DW$87, DW_AT_location[DW_OP_regx 0x24]
DW$88	.dwtag  DW_TAG_assign_register, DW_AT_name("A16")
	.dwattr DW$88, DW_AT_location[DW_OP_regx 0x25]
DW$89	.dwtag  DW_TAG_assign_register, DW_AT_name("A17")
	.dwattr DW$89, DW_AT_location[DW_OP_regx 0x26]
DW$90	.dwtag  DW_TAG_assign_register, DW_AT_name("A18")
	.dwattr DW$90, DW_AT_location[DW_OP_regx 0x27]
DW$91	.dwtag  DW_TAG_assign_register, DW_AT_name("A19")
	.dwattr DW$91, DW_AT_location[DW_OP_regx 0x28]
DW$92	.dwtag  DW_TAG_assign_register, DW_AT_name("A20")
	.dwattr DW$92, DW_AT_location[DW_OP_regx 0x29]
DW$93	.dwtag  DW_TAG_assign_register, DW_AT_name("A21")
	.dwattr DW$93, DW_AT_location[DW_OP_regx 0x2a]
DW$94	.dwtag  DW_TAG_assign_register, DW_AT_name("A22")
	.dwattr DW$94, DW_AT_location[DW_OP_regx 0x2b]
DW$95	.dwtag  DW_TAG_assign_register, DW_AT_name("A23")
	.dwattr DW$95, DW_AT_location[DW_OP_regx 0x2c]
DW$96	.dwtag  DW_TAG_assign_register, DW_AT_name("A24")
	.dwattr DW$96, DW_AT_location[DW_OP_regx 0x2d]
DW$97	.dwtag  DW_TAG_assign_register, DW_AT_name("A25")
	.dwattr DW$97, DW_AT_location[DW_OP_regx 0x2e]
DW$98	.dwtag  DW_TAG_assign_register, DW_AT_name("A26")
	.dwattr DW$98, DW_AT_location[DW_OP_regx 0x2f]
DW$99	.dwtag  DW_TAG_assign_register, DW_AT_name("A27")
	.dwattr DW$99, DW_AT_location[DW_OP_regx 0x30]
DW$100	.dwtag  DW_TAG_assign_register, DW_AT_name("A28")
	.dwattr DW$100, DW_AT_location[DW_OP_regx 0x31]
DW$101	.dwtag  DW_TAG_assign_register, DW_AT_name("A29")
	.dwattr DW$101, DW_AT_location[DW_OP_regx 0x32]
DW$102	.dwtag  DW_TAG_assign_register, DW_AT_name("A30")
	.dwattr DW$102, DW_AT_location[DW_OP_regx 0x33]
DW$103	.dwtag  DW_TAG_assign_register, DW_AT_name("A31")
	.dwattr DW$103, DW_AT_location[DW_OP_regx 0x34]
DW$104	.dwtag  DW_TAG_assign_register, DW_AT_name("B16")
	.dwattr DW$104, DW_AT_location[DW_OP_regx 0x35]
DW$105	.dwtag  DW_TAG_assign_register, DW_AT_name("B17")
	.dwattr DW$105, DW_AT_location[DW_OP_regx 0x36]
DW$106	.dwtag  DW_TAG_assign_register, DW_AT_name("B18")
	.dwattr DW$106, DW_AT_location[DW_OP_regx 0x37]
DW$107	.dwtag  DW_TAG_assign_register, DW_AT_name("B19")
	.dwattr DW$107, DW_AT_location[DW_OP_regx 0x38]
DW$108	.dwtag  DW_TAG_assign_register, DW_AT_name("B20")
	.dwattr DW$108, DW_AT_location[DW_OP_regx 0x39]
DW$109	.dwtag  DW_TAG_assign_register, DW_AT_name("B21")
	.dwattr DW$109, DW_AT_location[DW_OP_regx 0x3a]
DW$110	.dwtag  DW_TAG_assign_register, DW_AT_name("B22")
	.dwattr DW$110, DW_AT_location[DW_OP_regx 0x3b]
DW$111	.dwtag  DW_TAG_assign_register, DW_AT_name("B23")
	.dwattr DW$111, DW_AT_location[DW_OP_regx 0x3c]
DW$112	.dwtag  DW_TAG_assign_register, DW_AT_name("B24")
	.dwattr DW$112, DW_AT_location[DW_OP_regx 0x3d]
DW$113	.dwtag  DW_TAG_assign_register, DW_AT_name("B25")
	.dwattr DW$113, DW_AT_location[DW_OP_regx 0x3e]
DW$114	.dwtag  DW_TAG_assign_register, DW_AT_name("B26")
	.dwattr DW$114, DW_AT_location[DW_OP_regx 0x3f]
DW$115	.dwtag  DW_TAG_assign_register, DW_AT_name("B27")
	.dwattr DW$115, DW_AT_location[DW_OP_regx 0x40]
DW$116	.dwtag  DW_TAG_assign_register, DW_AT_name("B28")
	.dwattr DW$116, DW_AT_location[DW_OP_regx 0x41]
DW$117	.dwtag  DW_TAG_assign_register, DW_AT_name("B29")
	.dwattr DW$117, DW_AT_location[DW_OP_regx 0x42]
DW$118	.dwtag  DW_TAG_assign_register, DW_AT_name("B30")
	.dwattr DW$118, DW_AT_location[DW_OP_regx 0x43]
DW$119	.dwtag  DW_TAG_assign_register, DW_AT_name("B31")
	.dwattr DW$119, DW_AT_location[DW_OP_regx 0x44]
DW$120	.dwtag  DW_TAG_assign_register, DW_AT_name("AMR")
	.dwattr DW$120, DW_AT_location[DW_OP_regx 0x45]
DW$121	.dwtag  DW_TAG_assign_register, DW_AT_name("CSR")
	.dwattr DW$121, DW_AT_location[DW_OP_regx 0x46]
DW$122	.dwtag  DW_TAG_assign_register, DW_AT_name("ISR")
	.dwattr DW$122, DW_AT_location[DW_OP_regx 0x47]
DW$123	.dwtag  DW_TAG_assign_register, DW_AT_name("ICR")
	.dwattr DW$123, DW_AT_location[DW_OP_regx 0x48]
DW$124	.dwtag  DW_TAG_assign_register, DW_AT_name("IER")
	.dwattr DW$124, DW_AT_location[DW_OP_regx 0x49]
DW$125	.dwtag  DW_TAG_assign_register, DW_AT_name("ISTP")
	.dwattr DW$125, DW_AT_location[DW_OP_regx 0x4a]
DW$126	.dwtag  DW_TAG_assign_register, DW_AT_name("IN")
	.dwattr DW$126, DW_AT_location[DW_OP_regx 0x4b]
DW$127	.dwtag  DW_TAG_assign_register, DW_AT_name("OUT")
	.dwattr DW$127, DW_AT_location[DW_OP_regx 0x4c]
DW$128	.dwtag  DW_TAG_assign_register, DW_AT_name("ACR")
	.dwattr DW$128, DW_AT_location[DW_OP_regx 0x4d]
DW$129	.dwtag  DW_TAG_assign_register, DW_AT_name("ADR")
	.dwattr DW$129, DW_AT_location[DW_OP_regx 0x4e]
DW$130	.dwtag  DW_TAG_assign_register, DW_AT_name("FADCR")
	.dwattr DW$130, DW_AT_location[DW_OP_regx 0x4f]
DW$131	.dwtag  DW_TAG_assign_register, DW_AT_name("FAUCR")
	.dwattr DW$131, DW_AT_location[DW_OP_regx 0x50]
DW$132	.dwtag  DW_TAG_assign_register, DW_AT_name("FMCR")
	.dwattr DW$132, DW_AT_location[DW_OP_regx 0x51]
DW$133	.dwtag  DW_TAG_assign_register, DW_AT_name("GFPGFR")
	.dwattr DW$133, DW_AT_location[DW_OP_regx 0x52]
DW$134	.dwtag  DW_TAG_assign_register, DW_AT_name("DIER")
	.dwattr DW$134, DW_AT_location[DW_OP_regx 0x53]
DW$135	.dwtag  DW_TAG_assign_register, DW_AT_name("REP")
	.dwattr DW$135, DW_AT_location[DW_OP_regx 0x54]
DW$136	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCL")
	.dwattr DW$136, DW_AT_location[DW_OP_regx 0x55]
DW$137	.dwtag  DW_TAG_assign_register, DW_AT_name("TSCH")
	.dwattr DW$137, DW_AT_location[DW_OP_regx 0x56]
DW$138	.dwtag  DW_TAG_assign_register, DW_AT_name("ARP")
	.dwattr DW$138, DW_AT_location[DW_OP_regx 0x57]
DW$139	.dwtag  DW_TAG_assign_register, DW_AT_name("ILC")
	.dwattr DW$139, DW_AT_location[DW_OP_regx 0x58]
DW$140	.dwtag  DW_TAG_assign_register, DW_AT_name("RILC")
	.dwattr DW$140, DW_AT_location[DW_OP_regx 0x59]
DW$141	.dwtag  DW_TAG_assign_register, DW_AT_name("DNUM")
	.dwattr DW$141, DW_AT_location[DW_OP_regx 0x5a]
DW$142	.dwtag  DW_TAG_assign_register, DW_AT_name("SSR")
	.dwattr DW$142, DW_AT_location[DW_OP_regx 0x5b]
DW$143	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYA")
	.dwattr DW$143, DW_AT_location[DW_OP_regx 0x5c]
DW$144	.dwtag  DW_TAG_assign_register, DW_AT_name("GPLYB")
	.dwattr DW$144, DW_AT_location[DW_OP_regx 0x5d]
DW$145	.dwtag  DW_TAG_assign_register, DW_AT_name("TSR")
	.dwattr DW$145, DW_AT_location[DW_OP_regx 0x5e]
DW$146	.dwtag  DW_TAG_assign_register, DW_AT_name("ITSR")
	.dwattr DW$146, DW_AT_location[DW_OP_regx 0x5f]
DW$147	.dwtag  DW_TAG_assign_register, DW_AT_name("NTSR")
	.dwattr DW$147, DW_AT_location[DW_OP_regx 0x60]
DW$148	.dwtag  DW_TAG_assign_register, DW_AT_name("EFR")
	.dwattr DW$148, DW_AT_location[DW_OP_regx 0x61]
DW$149	.dwtag  DW_TAG_assign_register, DW_AT_name("ECR")
	.dwattr DW$149, DW_AT_location[DW_OP_regx 0x62]
DW$150	.dwtag  DW_TAG_assign_register, DW_AT_name("IERR")
	.dwattr DW$150, DW_AT_location[DW_OP_regx 0x63]
DW$151	.dwtag  DW_TAG_assign_register, DW_AT_name("DMSG")
	.dwattr DW$151, DW_AT_location[DW_OP_regx 0x64]
DW$152	.dwtag  DW_TAG_assign_register, DW_AT_name("CMSG")
	.dwattr DW$152, DW_AT_location[DW_OP_regx 0x65]
DW$153	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr DW$153, DW_AT_location[DW_OP_regx 0x66]
DW$154	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr DW$154, DW_AT_location[DW_OP_regx 0x67]
DW$155	.dwtag  DW_TAG_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr DW$155, DW_AT_location[DW_OP_regx 0x68]
DW$156	.dwtag  DW_TAG_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr DW$156, DW_AT_location[DW_OP_regx 0x69]
DW$157	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr DW$157, DW_AT_location[DW_OP_regx 0x6a]
DW$158	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr DW$158, DW_AT_location[DW_OP_regx 0x6b]
DW$159	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr DW$159, DW_AT_location[DW_OP_regx 0x6c]
DW$160	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr DW$160, DW_AT_location[DW_OP_regx 0x6d]
DW$161	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr DW$161, DW_AT_location[DW_OP_regx 0x6e]
DW$162	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr DW$162, DW_AT_location[DW_OP_regx 0x6f]
DW$163	.dwtag  DW_TAG_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr DW$163, DW_AT_location[DW_OP_regx 0x70]
DW$164	.dwtag  DW_TAG_assign_register, DW_AT_name("MFREG0")
	.dwattr DW$164, DW_AT_location[DW_OP_regx 0x71]
DW$165	.dwtag  DW_TAG_assign_register, DW_AT_name("DBG_STAT")
	.dwattr DW$165, DW_AT_location[DW_OP_regx 0x72]
DW$166	.dwtag  DW_TAG_assign_register, DW_AT_name("BRK_EN")
	.dwattr DW$166, DW_AT_location[DW_OP_regx 0x73]
DW$167	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr DW$167, DW_AT_location[DW_OP_regx 0x74]
DW$168	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP0")
	.dwattr DW$168, DW_AT_location[DW_OP_regx 0x75]
DW$169	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP1")
	.dwattr DW$169, DW_AT_location[DW_OP_regx 0x76]
DW$170	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP2")
	.dwattr DW$170, DW_AT_location[DW_OP_regx 0x77]
DW$171	.dwtag  DW_TAG_assign_register, DW_AT_name("HWBP3")
	.dwattr DW$171, DW_AT_location[DW_OP_regx 0x78]
DW$172	.dwtag  DW_TAG_assign_register, DW_AT_name("OVERLAY")
	.dwattr DW$172, DW_AT_location[DW_OP_regx 0x79]
DW$173	.dwtag  DW_TAG_assign_register, DW_AT_name("PC_PROF")
	.dwattr DW$173, DW_AT_location[DW_OP_regx 0x7a]
DW$174	.dwtag  DW_TAG_assign_register, DW_AT_name("ATSR")
	.dwattr DW$174, DW_AT_location[DW_OP_regx 0x7b]
DW$175	.dwtag  DW_TAG_assign_register, DW_AT_name("TRR")
	.dwattr DW$175, DW_AT_location[DW_OP_regx 0x7c]
DW$176	.dwtag  DW_TAG_assign_register, DW_AT_name("TCRR")
	.dwattr DW$176, DW_AT_location[DW_OP_regx 0x7d]
DW$177	.dwtag  DW_TAG_assign_register, DW_AT_name("CIE_RETA")
	.dwattr DW$177, DW_AT_location[DW_OP_regx 0x7e]
	.dwendtag DW$CU

