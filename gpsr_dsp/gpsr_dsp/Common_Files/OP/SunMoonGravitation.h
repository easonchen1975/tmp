/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  
 *   @file                   SunMoonGravitation.h
 *   @author                 Dmitry Churikov
 *   @date                   14.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#ifndef __SUNMOONGRAVITATION_H
#define __SUNMOONGRAVITATION_H

#ifdef USE_DSPBIOS
#include "comtypes.h"
#else
#include "../Sources/comtypes.h"
#endif

/// Using flag values
#define SMG_USE_NOTHING (0)
#define SMG_USE_SUN     (1)
#define SMG_USE_MOON    (2)
#define SMG_USE_ALL     (0xF)

///  Physical constants
#define SUN_MU               (1.32712438e20)             ///< Sun gravitational constant, m^3/s^2
#define MOON_MU              (0.01230002*3986004.418e8)  ///< Moon gravitational constant, m^3/s^2
#define EARTH_OMEGA          (7.2921151467e-5)           ///< Angular Velocity of the Earth rad/sec
#define AU                   (1.49597870e11)             ///< Astronomical Unit, m
#define MOON_ORBITAL_RADIUS  (384404000.)                ///< Mean distance between Earth and Moon centers

///  Mathematical constants
#define M_PI                   (3.1415926535897932384626433832795e0)
#define MC_ANG_SEC_TO_RADIANS  (M_PI/(180*3600))    
#define MC_RADIANS_TO_DEGREES  (180./M_PI)      
#define MC_DEGREES_TO_RADIANS  (M_PI/180.)       


// -----------------------------------------------------------------------------
///  @brief   Specific data object to Sun&Moon pertrubation calculation
// -----------------------------------------------------------------------------
typedef struct __tSunMoonData
{
  /// parameters to setting
  float64 mjdTau;     ///< Duration of fixing of Sun&Moon pertrubation

  /// special parameters
  float64 dATex;      ///< It is (dAT - dAT(J2000)) = (dGPS - dGPS(J2000)); 
                      ///  dAT = (TAI - UTC); dGPS = (GPS_Time - UTC)
  /// special data
  float64 mjdMean_t;  ///< Julian date (in J2000) of point of pertrubation fixing
  float64 Mean_g[3];  ///< Current values (approaches) of Sun&Moon pertrubations

} tSunMoonData;


//===================================================================================
///  @brief   Initialization Sun&Moon Data object
/// 
///  @param[in] pSMD - pointer of Sun&Moon Data object
//===================================================================================
EXTERN_C void SunMoonData_Init(tSunMoonData* pSMD);


//===================================================================================
///  @brief   The function calculates the perturbations due to Sun and Moon 
///           gravity acting on the object
///
///  @param[in] pSMD   - pointer to Sun&Moon Data object (can be 0)
///  @param[in] r      - pointer to ECEF state vector
///  @param[in] mjd_u  - current time from J2000.0 (in days)  
///                      mjd_u = JT(UTC) - JT(UTC(J2000.))
///  @param[in] fUsing - gravity pertrubation account flag
///  @param[in] g_sm   - pointer to gravity pertrubation vector (it is calculated)
//===================================================================================
EXTERN_C void SunMoonData_Gravitation(tSunMoonData *pSMD,
                                      float64      *r,
                                      float64       mjd_u,
                                      uint32        fUsing,
                                      float64      *g_sm);

#endif
