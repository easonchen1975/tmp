/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains functions to atmospheric drag calculation
 *   @file                   atmosphere.c
 *   @author                 Dmitry Churikov
 *   @date                   22.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#include "atmosphere.h"

#include <string.h>
#include <math.h>

//===================================================================================
///  @brief		Initialization Atmosphere object
///
///  @param[in]	pAtm - pointer to Atmosphere object
//===================================================================================
void Atmosphere_Init(tAtmosphere* pAtm)
{
  // zero object
  memset (pAtm, 0, sizeof (tAtmosphere));

  return;
}
// Atmosphere_Init

//===================================================================================
///  @brief  
/// 
///  @param[in] pAtm  - pointer to Atmosphere object
///  @param[in] r     - pointer to radius vector for drag factor calculation
///  @param[in] mjd_u - current time from J2000.0 (in days)  
///                     mjd_u = JT(UTC) - JT(UTC(J2000.))
///  @param[in] sigma - ballistic coefficient
///  @return    drag factor = Q/(V^2)
//===================================================================================
float64 Atmosphere_Drag_Factor(tAtmosphere *pAtm,
                               float64     *r,
                               float64      mjd_u,
                               float64      sigma)
{
  float64 h, hh, ro;

  h = sqrt(r[0]*r[0] + r[1]*r[1] + r[2]*r[2]) - ATM_EARTH_R0;  ///< current height

  hh = 70000.+0.075*(h - ATM_H1);                              ///< height scale 
  ro = ATM_RO1 * exp(-(h - ATM_H1)/hh);                        ///< density
  
  return (0.5*sigma*ro);

} 
// Atmosphere_Drag_Factor
