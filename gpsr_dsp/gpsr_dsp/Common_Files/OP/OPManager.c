/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains the functions to OPManager object support
 *   @file                   OPManager.c
 *   @author                 Dmitry Churikov
 *   @date                   25.12.2006
 *   @version                1.0
 */
/*****************************************************************************/

#include "../sources/comfuncs.h"
#include "../sources/DataForm.h"
#include "../sources/Control.h"
#include "../sources/Framework.h"
#include "../sources/DataForm.h"
#include "../sources/DebugPrintf.h"
#include "../sources/NavTask.h"
#include "../sources/DataConversion.h"
#include "OPManager.h"
#include "TimeEx.h"
#include "integration.h"
#include "matrix2.h"

#include <string.h>
#include <math.h>
#include <stdio.h>

//===========================================================================
///  @brief   Reset EFK parameters
///
///  @param[in] pOPMgr - pointer to OP Manager object
//===========================================================================
static void OP_ResetEKF(tOPManager *pOPMgr);

//===========================================================================
///  @brief   Reset EFK parameters after large GPS outages
///
///  @param[in] pOPMgr - pointer to OP Manager object
//===========================================================================
static void OP_ResetEKF_AfterOutage(tOPManager *pOPMgr);

//=================================================================================
///  @brief     Update first estimation (solution) for EFK 
///
///  @param[in] pOPMgr      - pointer to OP Manager object
///  @param[in] User        - User parameters
///  @param[in] NavSolution - navigation solution is good
///  @param[in] weekData    - week data structure
///  @return    
//=================================================================================
static int32 OP_GetFirstSolution(tOPManager *pOPMgr, 
                                 USER        User, 
                                 uint8       NavSolution, 
                                 tWeekData   weekData);

//===========================================================================
///  @brief 
///
///  @param[in] pOPMgr     - pointer to OP Manager object
///  @param[in] pTopSystem - pointer to top system parameters
///  @param[in] NavCond    - pointer to navigation conditions
///  @param[in] btUpdate   - pointer to GPS time 
//===========================================================================
static void OP_Estimation(tOPManager     *pOPMgr,
                          tTopSystem     *pTopSystem, 
                          NAV_CONDITIONS *NavCond,
                          tGPSTime       *btUpdate,
                          eFlag           BinStrFlag,
                          DATA_STRING    *dataStr,
                          tTerminal      *DebugTerminal);

//===========================================================================
///  @brief   The function calculates Covariance matrix propagation
///
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] r      - pointer to 
///  @param[in] v      - pointer to 
///  @param[in] dt     - 
///  @param[in] P0     - pointer to 
///  @param[in] Pp     - pointer to 
//===========================================================================
static void OP_CovMatrPropagation(tOPManager *pOPMgr,
                                  float64    *r, 
                                  float64    *v,
                                  float64     dt,
                                  MatrixEx   *P0,
                                  MatrixEx   *Pp);

//===========================================================================
///  @brief   The function calculates Covariance matrix propagation
///
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] dt     - 
///  @param[in] P0     - pointer to 
///  @param[in] Pp     - pointer to 
//===========================================================================
static void OP_CovMatrPropagation_Static(tOPManager *pOPMgr,
                                         float64     dt,
                                         MatrixEx   *P0,
                                         MatrixEx   *Pp);


//===========================================================================
///  @brief   
///
///  @param[in] pOPMgr     - pointer to OP Manager object
///  @param[in] pTopSystem - pointer to Top Systems parameters
///  @param[in] NavCond    - pointer to navigation conditions
///  @param[in] X          - pointer to 
///  @param[in] H          - pointer to 
///  @param[in] dYv        - pointer to 
///  @param[in] dYp        - pointer to
///  @return    number of NSV
//===========================================================================
static int32  OP_HMatrixCalulation(tOPManager *pOPMgr,
                                   tTopSystem     *pTopSystem,
                                   NAV_CONDITIONS *NavCond,
                                   float64        *X,
                                   MatrixEx       *H, 
                                   float64        *dYv,
                                   float64        *dYp,
                                   eFlag           BinStrFlag,
                                   DATA_STRING    *dataStr,
                                   tTerminal      *DebugTerminal);

//===========================================================================
///  @brief     The function calculates the gain matrix of Kalman Filter
///
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] K      - pointer to gain matrix (to calculation)
///  @param[in] Pp     - pointer to a priori covariance matrix
///  @param[in] H      - pointer to H matrix (2*nMeas x 8)
///  @param[in] nMeas  - number of independent measurements 
//                       (must be identical to Hpp (and Hvv) string count)
//===========================================================================
static void OP_KF_Gain(tOPManager *pOPMgr,
                       MatrixEx   *K,
                       MatrixEx   *Pp,
                       MatrixEx   *H,
                       int32       nMeas);

//===========================================================================
///  @brief   
///
///  @param[in] pOPMgr - pointer to OPManager object
///  @param[in] Pu     - pointer to 
///  @param[in] K      - pointer to 
///  @param[in] Pp     - pointer to 
///  @param[in] H      - pointer to 
///  @param[in] n_meas - 
//===========================================================================
static void OP_CovMatrUpdate(tOPManager *pOPMgr,
                             MatrixEx   *Pu,
                             MatrixEx   *K,
                             MatrixEx   *Pp,
                             MatrixEx   *H,
                             int32       n_meas);

//===========================================================================
///  @brief 
///   
///  @param[in] pOPMgr   - pointer to OP Manager object
///  @param[in] btUpdate - on-board time to state vector calculation 
///                        (it must be from GPS.0)
//===========================================================================
static void OP_Propagation(tOPManager *pOPMgr, tTopSystem *pTopSystem, tGPSTime *btUpdate);

//===========================================================================
///  @brief OP message forming
///   
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] wn     - GPS week number 
//===========================================================================
static void OPMgr_Message(tOPManager *pOPMgr, int16 wn);


// -----------------------------------------------------------------------------
///  @brief		  The function analyzes  validity of OP and navigation solutions 
///             by integral difference of them
///  @param[in]	const USER *pUser - pointer of USER object
///  @param[in]	const tOPManager *pOPMgr - pointer of OPManager object
///  @param[in]	tSolutionsStatus *pSolVal - pointer of tSolutionsStatus object
///  @param[in]	eFlag fProcessing - flag of analysis state 
///                                (fg_ON - permit analysis)
///                                (fg_OFF - stop analysis and reset all integral criterion)
///  @return     -1 - if analysis isn't complete
///               0 - if analysis is done and both solutions aren't valid 
///               1 - if analysis is done and both solutions are valid 
// -----------------------------------------------------------------------------
int8 FirstSolutionValidity( const USER       *pUser,
                            const tOPManager *pOPMgr,
                            tSolutionStatus  *pSolVal,
                            eFlag             fProcessing,
                            eFlag              BinStrFlag,
                            DATA_STRING       *dataStr,
                            tTerminal         *DebugTerminal)
{
  float64 flTm, dR, dV;

  if (fProcessing == fg_ON)
  {// processing
   
    flTm = pUser->ecef.X - pOPMgr->X[I_X];        dR  = flTm*flTm;
    flTm = pUser->ecef.Y - pOPMgr->X[I_Y];        dR += flTm*flTm;
    flTm = pUser->ecef.Z - pOPMgr->X[I_Z];        dR += flTm*flTm;

    flTm = pUser->ecef.Xdot - pOPMgr->X[I_VX];    dV  = flTm*flTm;
    flTm = pUser->ecef.Ydot - pOPMgr->X[I_VY];    dV += flTm*flTm;
    flTm = pUser->ecef.Zdot - pOPMgr->X[I_VZ];    dV += flTm*flTm;

    pSolVal->dR += dR;
    pSolVal->dV += dV;
    pSolVal->SolutionCount++;
  }

  
  if ((fProcessing == fg_OFF) || (pSolVal->SolutionCount <= 0))
  {
    /// Validity analyze
    if (pSolVal->SolutionCount)
    {
      dR = sqrt(pSolVal->dR / (float64)pSolVal->SolutionCount);
      dV = sqrt(pSolVal->dV / (float64)pSolVal->SolutionCount);
    }
    else
    {
      dR = MAX_AVER_DR + MAX_AVER_DR;
      dV = MAX_AVER_DV + MAX_AVER_DV;
    }

    if (BinStrFlag == fg_OFF)
    {
      sprintf(dataStr->str, "FIRST_SOL: dR:%lf, dV:%lf, Cnt:%d\r\n", dR, dV, pSolVal->SolutionCount);
      DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
    }

    pSolVal->SolutionCount = 0;
    pSolVal->dR = 0;
    pSolVal->dV = 0;
    
    return ((dR <= MAX_AVER_DR) && (dV <= MAX_AVER_DV));
  }

  return -1;
}


// -----------------------------------------------------------------------------
///  @brief		  The function analyzes  validity navigation solutions 
///             by current difference with OP solution
///  @param[in]	const USER *pUser - pointer of USER object
///  @param[in]	const tOPManager *pOPMgr - pointer of OPManager object
///  @return      0 - navigation solutions isn't valid 
///               1 - navigation solutions is valid 
// -----------------------------------------------------------------------------
int8 SolutionValidity( const USER *pUser,
                       const tOPManager *pOPMgr,
                       eFlag              BinStrFlag,
                       DATA_STRING       *dataStr,
                       tTerminal         *DebugTerminal)
{
  float64 flTm, dR, dV;

  flTm = pUser->ecef.X - pOPMgr->X[I_X];        dR  = flTm*flTm;
  flTm = pUser->ecef.Y - pOPMgr->X[I_Y];        dR += flTm*flTm;
  flTm = pUser->ecef.Z - pOPMgr->X[I_Z];        dR += flTm*flTm;

  flTm = pUser->ecef.Xdot - pOPMgr->X[I_VX];    dV  = flTm*flTm;
  flTm = pUser->ecef.Ydot - pOPMgr->X[I_VY];    dV += flTm*flTm;
  flTm = pUser->ecef.Zdot - pOPMgr->X[I_VZ];    dV += flTm*flTm;

  dR = sqrt(dR);
  dV = sqrt(dV);

/*	Spirit's debugging information
	Turn it off for released image
	
  if (BinStrFlag == fg_OFF)
  {
    sprintf(dataStr->str, "SOL_VALID: dR:%lf, dV:%lf\r\n", dR, dV);
    DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
  }
*/

  return ((dR <= MAX_DIFF_DR) && (dV <= MAX_DIFF_DV));
}


//===================================================================================
///  @brief     Initialization Orbit Propagation Manager object
///
///  @param[in] pOPMgr        - pointer to instance
///  @param[in] mDynModelType - Dynamic model type
//===================================================================================
void OPMgr_Init(tOPManager        *pOPMgr, int8 mDynModelType)
{
  memset(pOPMgr, 0, sizeof(tOPManager));

  /// dynamic model settings
  assumedDynamics_Init( &pOPMgr->DM); 
  pOPMgr->pfDM = assumedDynamics;  ///< right part functions
  
  /// OP parameters settings
  /// Q - matrix
  pOPMgr->Qorb[0] = OP_DEF_STATE_DISP_VL;     ///< along directional velocity dispersion
  pOPMgr->Qorb[1] = OP_DEF_STATE_DISP_VR;     ///< radial direction velocity dispersion
  pOPMgr->Qorb[2] = OP_DEF_STATE_DISP_VBN;    ///< binormal direction velocity dispersion
  pOPMgr->Qorb[3] = OP_DEF_STATE_DISP_L;      ///< along directional position dispersion
  pOPMgr->Qorb[4] = OP_DEF_STATE_DISP_R;      ///< radial direction position dispersion
  pOPMgr->Qorb[5] = OP_DEF_STATE_DISP_BN;     ///< binormal direction position dispersion
  pOPMgr->Qorb[6] = OP_DEF_STATE_DISP_BIAS;   ///< receiver clock bias dispersion
  pOPMgr->Qorb[7] = OP_DEF_STATE_DISP_DRIFT;  ///< receiver clock drift dispersion
  
  /// R - matrix
  pOPMgr->R_pr  = OP_DEF_PR_NOISE;
  pOPMgr->R_prr = OP_DEF_DOPLER_NOISE;

  /// OP condition
  pOPMgr->fOP_Condition = OP_COND_NO_SOLUTION;
  
  /// OP solution validation
  pOPMgr->fOP_Validity = OP_FG_NOT_VALID;

  /// Covariance matrix
  OP_ResetEKF(pOPMgr);
  
  /// Dynamic model type
  pOPMgr->mOP_DynModelType = mDynModelType;
  
  return;
} 
// OPMgr_Init

//===========================================================================
///  @brief OP message forming
///   
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] wn     - GPS week number 
//===========================================================================
static void OPMgr_Message(tOPManager *pOPMgr, int16 wn)
{
  /// convert ECEF data to LLA and OE format
  float64 TOPO[3][3];
  tECEF    ecef;
  tLLA     geo;
  tOE      oe;

  ecef.X    = pOPMgr->X[I_X];
  ecef.Y    = pOPMgr->X[I_Y];
  ecef.Z    = pOPMgr->X[I_Z];
  ecef.Xdot = pOPMgr->X[I_VX];
  ecef.Ydot = pOPMgr->X[I_VY];
  ecef.Zdot = pOPMgr->X[I_VZ];

	memset(&geo, 0, sizeof(tLLA));
	memset(&oe, 0, sizeof(tOE));

  ECEF_to_Geoid(&geo, &ecef, TOPO);
  //ECEF_to_OE(&oe, &ecef);
  /// Convert double User data to integer format
  pOPMgr->User_OP.wn     = wn;
  pOPMgr->User_OP.t      = (int64)(((pOPMgr->btLastUpdate.t))*1.e+6 + 0.5);  /// s->mks
  pOPMgr->User_OP.X      = (int64)(ecef.X*1.e+3);                        /// m->mm
  pOPMgr->User_OP.Y      = (int64)(ecef.Y*1.e+3);
  pOPMgr->User_OP.Z      = (int64)(ecef.Z*1.e+3);
  pOPMgr->User_OP.Xdot   = (int32)(ecef.Xdot*1.e+3);                     /// m/s->mm/s
  pOPMgr->User_OP.Ydot   = (int32)(ecef.Ydot*1.e+3);
  pOPMgr->User_OP.Zdot   = (int32)(ecef.Zdot*1.e+3);
  pOPMgr->User_OP.Lat    = (int64)(ldexp(geo.Lat, 48));                  /// 2^48 rad.
  pOPMgr->User_OP.Lon    = (int64)(ldexp(geo.Lon, 48));
  pOPMgr->User_OP.H      = (int64)(geo.H*1.e+3);                         /// m->mm
  pOPMgr->User_OP.Ndot   = (int32)(geo.Ndot*1.e+3);                      /// m/s->mm/s
  pOPMgr->User_OP.Edot   = (int32)(geo.Edot*1.e+3);
  pOPMgr->User_OP.Udot   = (int32)(geo.Udot*1.e+3);
  pOPMgr->User_OP.A      = (int64)(oe.A*1.e+3);                          /// m -> mm
  pOPMgr->User_OP.Omega  = (int64)(ldexp(oe.Omega, 48));                 /// 2^48 rad.
  pOPMgr->User_OP.M      = (int64)(ldexp(oe.M, 48));                     /// 2^48 rad.
  pOPMgr->User_OP.I      = (int32)(ldexp(oe.I, 29));                     /// 2^29 rad.
  pOPMgr->User_OP.OmegaP = (int32)(ldexp(oe.OmegaP, 29));                /// 2^29 rad.
  pOPMgr->User_OP.Ecc    = (int32)(ldexp(oe.Ecc, 31));                   /// 2^31
  pOPMgr->User_OP.R      = (int64)(pOPMgr->X[I_B]*l_cLight*1.e+12);       /// 10^-12 sec
  pOPMgr->User_OP.OscillOffset = (int32)(pOPMgr->X[I_D]*l_cLight*1.e+12);/// 10^-6 ppm : (Rdot*f0_GPS*10^6)/(cLight*1575.42)    
  if (pOPMgr->fSynchronization == SYNC_PROCESS)
  {
    if ((pOPMgr->fOP_Condition == OP_COND_PROPAGATION) ||
        (pOPMgr->fOP_Condition == OP_COND_ESTIMATION))
    {
      pOPMgr->User_OP.fOP_Condition = OP_COND_PROPAGATION_PROC;
    }
  }
  else
    pOPMgr->User_OP.fOP_Condition = pOPMgr->fOP_Condition;

}

//===========================================================================
///  @brief     Calculates current OP solution
///
///  @param[in] pOPMgr      - pointer to OP Manager object
///  @param[in] pTopSystem  - pointer to top system parameters
///  @param[in] NavCond     - pointer to navigation conditions
///  @param[in] User        - user parameters
///  @param[in] NavSolution - navigation solution is good
///  @param[in] weekData    - week data structure
//===========================================================================
void OPMgr_Process(tOPManager     *pOPMgr, 
                   tTopSystem     *pTopSystem, 
                   NAV_CONDITIONS *NavCond,
                   USER            User,
                   uint8           NavSolution,
                   tWeekData       weekData,
                   eFlag           BinStrFlag,
                   DATA_STRING    *dataStr,
                   tTerminal      *DebugTerminal)
{
  float64  t_from_evn;
  tGPSTime btNextUpdate;    // on-bord time of next update (estimation or propagation)
 
  ///  Calculation of current TAI correction
  if (pTopSystem->IonUTC.dtls)
  {
    pOPMgr->DM.SMD.dATex = pTopSystem->IonUTC.dtls - DT_UTC_GPS_2000;
  }

  ///  Calculation of next (current) on-bord time to update
  //week_current = week + WEEK*n, where n reading from FLASH 
  if (weekData.DataError == fg_OFF)
    btNextUpdate.wn = (NavCond->wnOP + WEEK*weekData.cntOverFlow);
  else
    btNextUpdate.wn = (NavCond->wnOP + WEEK);
  btNextUpdate.t  = NavCond->tOP;

  /// OP solution type/action determination 
  /// time from last estimation
  t_from_evn  = btNextUpdate.t - pOPMgr->btLastEstimation.t;
  t_from_evn += (btNextUpdate.wn - pOPMgr->btLastEstimation.wn) * T_SECONDS_PER_WEEK; 

  /// covariance matrix reset
  if (t_from_evn > OP_PRP_TIME_EKF_RESET)
    OP_ResetEKF_AfterOutage(pOPMgr);
  
  /// solution availability checking
  if ((t_from_evn > OP_PRP_TIME_LIMIT) && (pOPMgr->fOP_Condition != OP_COND_NO_SOLUTION))
  {
    /// no solution is available from OP data
    pOPMgr->fOP_Condition = OP_COND_NO_SOLUTION;
    pOPMgr->fOP_Validity = OP_FG_NOT_VALID;
    OPMgr_Message(pOPMgr, NavCond->wn);
    return;
  }
  if (pOPMgr->fOP_Condition == OP_COND_NO_SOLUTION)
  { 
    /// no solution is available from OP data
    pOPMgr->fOP_Condition = OP_COND_NO_SOLUTION;
    pOPMgr->fOP_Validity = OP_FG_NOT_VALID;
    if (!OP_GetFirstSolution(pOPMgr, User, NavSolution, weekData) && (pOPMgr->fOP_Condition == OP_COND_NO_SOLUTION))
    {
      /// No First Solution is available
      return;
    }
    else 
    {
      pOPMgr->fOP_Validity = OP_FG_VALID;
      pOPMgr->fOP_Condition = OP_COND_PROPAGATION_PROC;
      OPMgr_Message(pOPMgr, NavCond->wn);
      return;
    }
  }

  /// time from last update
  t_from_evn  =  btNextUpdate.t  - pOPMgr->btLastUpdate.t;
  t_from_evn += (btNextUpdate.wn - pOPMgr->btLastUpdate.wn) * T_DAYS_PER_SECOND; 
  
  if (t_from_evn <= 0 )
  {
    return;
  }
  else if (t_from_evn > OP_PRP_MAX_STEP)    
  {
    pOPMgr->fOP_Condition = OP_COND_PROPAGATION;
  }
  else 
    pOPMgr->fOP_Condition = OP_COND_ESTIMATION;
  
  /// OP solution 
  if (pOPMgr->fOP_Condition == OP_COND_PROPAGATION)
  {
    /// OP propagation
    /// State vector and EKF parameters propagation
    OP_Propagation(pOPMgr, pTopSystem, &btNextUpdate);
  }
  else if (pOPMgr->fOP_Condition == OP_COND_ESTIMATION)
  {
    /// OP estimation
    OP_Estimation(pOPMgr, pTopSystem, NavCond, &btNextUpdate, BinStrFlag, dataStr, DebugTerminal);
  }
  else
  {
    /// No OP solution 
    pOPMgr->fOP_Condition = OP_COND_NO_SOLUTION;
    pOPMgr->fOP_Validity = OP_FG_NOT_VALID;
  }
  
  OPMgr_Message(pOPMgr, NavCond->wn);

  if (pOPMgr->User_OP.fOP_Condition == OP_COND_ESTIMATION)
  {
    while (pOPMgr->CorrectTimeOP.flag_bl == 1);
    pOPMgr->CorrectTimeOP.flag_bl = 1;
    if (pOPMgr->CorrectTimeOP.FlagCorrectTime == fg_OFF)
    {
      pOPMgr->CorrectTimeOP.R_nts           = pOPMgr->X[I_B];
      pOPMgr->CorrectTimeOP.Rdot_nts        = pOPMgr->X[I_D];
      pOPMgr->CorrectTimeOP.FlagCorrectTime = fg_ON;
    }
    pOPMgr->CorrectTimeOP.flag_bl = 0;
  }    

  return;
} 
//OPMgr_Process

//===========================================================================
///  @brief   Reset EFK parameters
///
///  @param[in] pOPMgr - pointer to OP Manager object
//===========================================================================
static void OP_ResetEKF(tOPManager *pOPMgr)
{
  /// Covariance matrix
  CreateMatrixEx(&pOPMgr->P0, EST_VAR_COUNT, EST_VAR_COUNT);
  pOPMgr->P0.Matr[I_VX][I_VX] = OP_DEF_COV_DISP_V;
  pOPMgr->P0.Matr[I_VY][I_VY] = OP_DEF_COV_DISP_V;
  pOPMgr->P0.Matr[I_VZ][I_VZ] = OP_DEF_COV_DISP_V;
  pOPMgr->P0.Matr[I_X] [I_X]  = OP_DEF_COV_DISP_P;
  pOPMgr->P0.Matr[I_Y] [I_Y]  = OP_DEF_COV_DISP_P;
  pOPMgr->P0.Matr[I_Z] [I_Z]  = OP_DEF_COV_DISP_P;
  pOPMgr->P0.Matr[I_B] [I_B]  = OP_DEF_COV_DISP_B;
  pOPMgr->P0.Matr[I_D] [I_D]  = OP_DEF_COV_DISP_D;

  return;
} 
// OP_ResetEKF

//===========================================================================
///  @brief   Reset EFK parameters after large GPS outages
///
///  @param[in] pOPMgr - pointer to OP Manager object
//===========================================================================
static void OP_ResetEKF_AfterOutage(tOPManager *pOPMgr)
{
  /// Covariance matrix
  CreateMatrixEx(&pOPMgr->P0, EST_VAR_COUNT, EST_VAR_COUNT);
  pOPMgr->P0.Matr[I_VX][I_VX] = OP_LARGE_COV_DISP_V;
  pOPMgr->P0.Matr[I_VY][I_VY] = OP_LARGE_COV_DISP_V;
  pOPMgr->P0.Matr[I_VZ][I_VZ] = OP_LARGE_COV_DISP_V;
  pOPMgr->P0.Matr[I_X] [I_X]  = OP_LARGE_COV_DISP_P;
  pOPMgr->P0.Matr[I_Y] [I_Y]  = OP_LARGE_COV_DISP_P;
  pOPMgr->P0.Matr[I_Z] [I_Z]  = OP_LARGE_COV_DISP_P;
  pOPMgr->P0.Matr[I_B] [I_B]  = OP_LARGE_COV_DISP_B;
  pOPMgr->P0.Matr[I_D] [I_D]  = OP_LARGE_COV_DISP_D;

  return;
} 
// OP_ResetEKF
//=================================================================================
///  @brief     Update first estimation (solution) for EFK 
///
///  @param[in] pOPMgr      - pointer to OP Manager object
///  @param[in] User        - User parameters
///  @param[in] NavSolution - navigation solution is good
///  @param[in] weekData    - week data structure
///  @return    
//=================================================================================
static int32 OP_GetFirstSolution(tOPManager *pOPMgr, 
                                 USER        User, 
                                 uint8       NavSolution,
                                 tWeekData   weekData)
{

  if ((User.SolutionFlag == 1) && (User.CoordValid == OK_NAV_TASK) && (NavSolution == 2))
  {
    /// position
    pOPMgr->X[I_X] = User.ecef.X;
    pOPMgr->X[I_Y] = User.ecef.Y;
    pOPMgr->X[I_Z] = User.ecef.Z;

    /// velocity
    pOPMgr->X[I_VX] = User.ecef.Xdot;
    pOPMgr->X[I_VY] = User.ecef.Ydot;
    pOPMgr->X[I_VZ] = User.ecef.Zdot;
  
    /// clock shift (bias)
    pOPMgr->X[I_B] = User.R;    // in m

    /// clock drift 
    pOPMgr->X[I_D] = User.Rdot; // in m/s

    /// on-bord time update
    // week_current = week + WEEK*n, where n reading from FLASH
    if (weekData.DataError == fg_OFF)
      pOPMgr->btLastEstimation.wn = pOPMgr->btLastUpdate.wn = (User.wn + WEEK*weekData.cntOverFlow);
    else
      pOPMgr->btLastEstimation.wn = pOPMgr->btLastUpdate.wn = (User.wn + WEEK);
    pOPMgr->btLastEstimation.t  = pOPMgr->btLastUpdate.t  = (User.t - User.R*l_cLight);

    pOPMgr->fOP_Condition = OP_COND_PROPAGATION_PROC;

    while (pOPMgr->CorrectTimeOP.flag_bl == 1);
    pOPMgr->CorrectTimeOP.flag_bl = 1;
    if (pOPMgr->CorrectTimeOP.FlagCorrectTime == fg_OFF)
    {
      pOPMgr->CorrectTimeOP.R_nts           = pOPMgr->X[I_B];
      pOPMgr->CorrectTimeOP.Rdot_nts        = pOPMgr->X[I_D];
      pOPMgr->CorrectTimeOP.FlagCorrectTime = fg_ON;
    }
    pOPMgr->CorrectTimeOP.flag_bl = 0;

    /// EKF reset
    OP_ResetEKF(pOPMgr);

    return 1;
  }
  else 
    return 0;
}
// OP_GetFirstSolution

//===========================================================================
///  @brief 
///  
///  @param[in] pOPMgr     - pointer to OP Manager object
///  @param[in] pTopSystem - pointer to top system parameters
///  @param[in] NavCond    - pointer to navigation conditions
///  @param[in] btUpdate   - pointer to GPS time 
//===========================================================================
static void OP_Estimation(tOPManager     *pOPMgr,
                          tTopSystem     *pTopSystem, 
                          NAV_CONDITIONS *NavCond,
                          tGPSTime       *btUpdate,
                          eFlag           BinStrFlag,
                          DATA_STRING    *dataStr,
                          tTerminal      *DebugTerminal)
{       
  int32 i, j, n_meas;
  float64  mjd0, dt;
  static MatrixEx H, Pp, K;
  static float64  X0[EST_VAR_COUNT],           Xp[EST_VAR_COUNT];
  static float64  DX[EST_VAR_COUNT],           dX[EST_VAR_COUNT];
  static float64  dYp[GPS_SAT_CHANNELS_COUNT], dYv[GPS_SAT_CHANNELS_COUNT];

  /// Last estimation time and time from previous estimation determination
  //mjd0 = MJD_From_UTCEx( &pOPMgr->btLastUpdate) + 
  //  (l_cLight*pOPMgr->X[I_B] - pTopSystem->IonUTC.dtls) * T_DAYS_PER_SECOND;
  MJD_From_UTCEx(&pOPMgr->btLastUpdate, &mjd0);
  mjd0 -= (pTopSystem->IonUTC.dtls * T_DAYS_PER_SECOND);
  dt  = btUpdate->t - pOPMgr->btLastUpdate.t;
  dt +=  (btUpdate->wn - pOPMgr->btLastUpdate.wn) * T_SECONDS_PER_WEEK; 
  
/*	Spirit's debugging code
	Turn it off for released code  
  
  if (BinStrFlag == fg_OFF)
  {
    sprintf(dataStr->str, "START:   X:%12.3lf, Y:%12.3lf, Z:%12.3lf, VX:%8.3lf, VY:%8.3lf, VZ:%8.3lf, R:%12.6lf, Rd:%9.6lf, timeUp:%12.6lf, timeLUp:%12.6lf\r\n", 
      pOPMgr->X[I_X], pOPMgr->X[I_Y], pOPMgr->X[I_Z], pOPMgr->X[I_VX], pOPMgr->X[I_VY], pOPMgr->X[I_VZ], 
      (pOPMgr->X[I_B]*l_cLight*1E+6), (pOPMgr->X[I_D]*l_cLight*1E+6), btUpdate->t, pOPMgr->btLastUpdate.t);
    DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
  }  
*/

  _memcpy(X0, pOPMgr->X, sizeof(pOPMgr->X));

  /// Stage A
  
  //Xp[I_B] = X0[I_B] + X0[I_D]*dt; ///< bias propagation
  Xp[I_B] = pOPMgr->CorrectTimeOP.Rnc;
  Xp[I_D] = X0[I_D];              ///< drift propagation
  
  /// KF state propagation 
  switch (pOPMgr->mOP_DynModelType)
  {
  case OP_DM_ORBITAL_MOTION:
    // state vector increments
    X0[I_B] = mjd0;                 ///< X0[B] is used for MJD in assumed dynamic
    rk4( pOPMgr->pfDM, X0, 6+1, dt, (int32)&pOPMgr->DM, DX);

    /// Covariance matrix propagation
    OP_CovMatrPropagation( pOPMgr, &X0[I_X], &X0[I_VX], dt, &(pOPMgr->P0), &Pp);
    break;

  case OP_DM_STATIC:
  default:
    // state vector increments
    for(i=0;i<6;i++)
      DX[i] = 0;

    /// Covariance matrix propagation
    OP_CovMatrPropagation_Static( pOPMgr, dt, &(pOPMgr->P0), &Pp);
    break;
  }

  for (i = 0; i < 3; i++)   
  {
    Xp[I_X  + i ] = X0[I_X  + i ] + DX[I_X  + i];
    Xp[I_VX + i]  = X0[I_VX + i]  + DX[I_VX + i];
  }

/*	Spirit's debugging code
	Turn it off for released image
	
  if (BinStrFlag == fg_OFF)
  {
    sprintf(dataStr->str, "PRG_SOL: X:%12.3lf, Y:%12.3lf, Z:%12.3lf, VX:%8.3lf, VY:%8.3lf, VZ:%8.3lf, R:%12.6lf, Rd:%9.6lf\r\n", 
      Xp[I_X], Xp[I_Y], Xp[I_Z], Xp[I_VX], Xp[I_VY], Xp[I_VZ], (Xp[I_B]*l_cLight*1E+6), (Xp[I_D]*l_cLight*1E+6));
    DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
  }  
*/
  
  /// Set update time (estimation or propagation)
  pOPMgr->btLastUpdate.wn = btUpdate->wn;
  pOPMgr->btLastUpdate.t  = btUpdate->t;

  /// Stage B
  
  /// Estimation availability checking
  if (NavCond->NavSV.N < OP_EST_NSV_MIN)
  { 
    /// No data for estimation
    _memcpy( pOPMgr->X, Xp, sizeof(pOPMgr->X));
    CopyMatrixEx( &pOPMgr->P0, &Pp);
    pOPMgr->fOP_Condition = OP_COND_PROPAGATION;
    return;
  }

  /// Sensitivity matrix H and measurements estimation (dY) calculation
  //n_meas = OP_HMatrixCalulation(pTopSystem, Xp, &H, dYv, dYp, pOPMgr->mOP_DynModelType);
  n_meas = OP_HMatrixCalulation(pOPMgr, pTopSystem, NavCond, Xp, &H, dYv, dYp, BinStrFlag, dataStr, DebugTerminal);

  /// Estimation availability checking
  if (n_meas < OP_EST_NSV_MIN)
  { 
    /// No data for estimation
    _memcpy( pOPMgr->X, Xp, sizeof(pOPMgr->X));
    CopyMatrixEx( &pOPMgr->P0, &Pp);
    pOPMgr->fOP_Condition = OP_COND_PROPAGATION;
    for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
    {
      if (pTopSystem->navtask_params[i].sv_num != SATELLITE_INVALID_ID)
        pTopSystem->navtask_params[i].paramsOP.USED_OP = 0;
    }
    return;
  }

  /// KF Gain calculation
  OP_KF_Gain(pOPMgr, &K, &Pp, &H, n_meas);
  /// Singularity of matrix checking
  if (!K.mCol)
  { 
    /// K is singular
    //pOPMgr->fOP_Condition = OP_COND_PROPAGATION;
    //OP_ResetEKF( pOPMgr);  ///<EFK reset is required
    OPMgr_Init(pOPMgr, NavCond->OrbStatic);
    return;
  }

  /// Corrections of state vector calculation
  // dX = dQ = K*dY
  // K =  [ Kv | Kp ]   size(Kv) size(Kp) = n_meas x (6+1)
  //    [ dYv ]     size(dYv) = n_meas x 1; dYv - is residuals of pseudo range rate
  // dY = [ --- ]                 
  //    [ dYp ]     size(dYp) = n_meas x 1; dYp - is residuals of pseudo range
  // dX = [ Kv*dYv + Kp*dYp]  size(dX) size(Kp) = (6+1) x 1
  memset (dX, 0, sizeof(dX));
  for ( i = 0; i < EST_VAR_COUNT; i++)
    for ( j = 0; j < n_meas; j++)
    {
      dX[i] += K.Matr[i][j]*dYv[j] + K.Matr[i][n_meas + j]*dYp[j];
    }

  /// State update
  pOPMgr->btLastEstimation.wn = btUpdate->wn;
  pOPMgr->btLastEstimation.t  = btUpdate->t;
  for ( i = 0; i < EST_VAR_COUNT; i++)
  {
    pOPMgr->X[i] = Xp[i] + dX[i];
  }

/*	Spirit's debugging information
	Turn it off for released image

  if (BinStrFlag == fg_OFF)
  {
    sprintf(dataStr->str, "EST_SOL: X:%12.3lf, Y:%12.3lf, Z:%12.3lf, VX:%8.3lf, VY:%8.3lf, VZ:%8.3lf, R:%12.6lf, Rd:%9.6lf\r\n", 
      pOPMgr->X[I_X], pOPMgr->X[I_Y], pOPMgr->X[I_Z], pOPMgr->X[I_VX], pOPMgr->X[I_VY], pOPMgr->X[I_VZ], 
      (pOPMgr->X[I_B]*l_cLight*1E+6), (pOPMgr->X[I_D]*l_cLight*1E+6));
    DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
  }  
*/

  /// Covariance update - Joseph method
  OP_CovMatrUpdate(pOPMgr, &(pOPMgr->P0), &K, &Pp, &H, n_meas);

} 
//OP_Estimation

//===========================================================================
///  @brief   The function calculates Covariance matrix propagation
///
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] r    - pointer to 
///  @param[in] v    - pointer to 
///  @param[in] dt   - 
///  @param[in] P0   - pointer to 
///  @param[in] Pp   - pointer to 
//===========================================================================
static void OP_CovMatrPropagation(tOPManager *pOPMgr,
                                  float64    *r, 
                                  float64    *v,
                                  float64     dt,
                                  MatrixEx   *P0,
                                  MatrixEx   *Pp)
{ 
  MatrixEx F;
  float64 R2, ro5;
  int i;
  
  /// Jacobian matrix F calculation
  //    [  Fvv   | Fclp | 0 0 ]                          [   0     F_VxVy 0 ]        
  //    [ ------ | ---- | . . ]                    Fvv = [ -F_VxVy    0   0 ]
  //  F = [ I(3,3) |   O  | 0 0 ] *dt + I(8,8);          [   0        0   0 ]
  //    [ ------------- | --- ]   
  //    [ 0   . . .  0  | 0 1 ]       sizeof(Fclp) = (3,3)
  //    [ 0   . . .  0  | 0 0 ]       O - is null-matrix, sizeof(O) = (3,3)

  /// Matrix initialization
  CreateMatrixEx( Pp, EST_VAR_COUNT, EST_VAR_COUNT);

  R2 = r[0]*r[0] + r[1]*r[1] + r[2]*r[2];
  ro5 = 1./(R2*R2*sqrt(R2));  // 1/|r|^5
  
  CreateMatrixEx(&F, EST_VAR_COUNT, EST_VAR_COUNT);

  F.Matr[I_VX][I_VY] =   dt * (EARTH_OMEGA + EARTH_OMEGA);
  F.Matr[I_VX][I_X]  = - dt * EARTH_MU * (R2 - 3.*r[0]*r[0]) * ro5 + EARTH_OMEGA*EARTH_OMEGA;
  F.Matr[I_VX][I_Y]  =   dt * EARTH_MU * (     3.*r[0]*r[1]) * ro5;
  F.Matr[I_VX][I_Z]  =   dt * EARTH_MU * (     3.*r[0]*r[2]) * ro5;

  F.Matr[I_VY][I_VX] = - dt * F.Matr[I_VX][I_VY];
  F.Matr[I_VY][I_X]  =   dt * F.Matr[I_VX][I_Y];
  F.Matr[I_VY][I_Y]  = - dt * EARTH_MU * (R2 - 3.*r[1]*r[1]) * ro5 + EARTH_OMEGA*EARTH_OMEGA;
  F.Matr[I_VY][I_Z]  =   dt * EARTH_MU * (     3.*r[1]*r[2]) * ro5;

  F.Matr[I_VZ][I_X]  =   dt * F.Matr[I_VX][I_Z];
  F.Matr[I_VZ][I_Y]  =   dt * F.Matr[I_VY][I_Z];
  F.Matr[I_VZ][I_Z]  = - dt * EARTH_MU * (R2 - 3.*r[2]*r[2]) * ro5;

  F.Matr[I_X][I_VX]  =  dt;
  F.Matr[I_Y][I_VY]  =  dt;
  F.Matr[I_Z][I_VZ]  =  dt;
  
  F.Matr[I_B][I_D]   =  dt;

  for(i=0;i<EST_VAR_COUNT;i++)
    F.Matr[i][i] = 1;

  /// Pp matrix calculation
  // Pp = F*P0*F`
  MultMatrix_B_TC_Ex(Pp, P0, &F);
  MultMatrix_SimResEx(Pp, &F, Pp);


  // Pp = F*P0*F` + Q*dt; Q = M'*Qorb*M;  
  // Qorb - diagonal matrix of dispersions
  // M - ECEF to Orbital Frame transform matrix
  //OrbitalTranformMatrix( r, v, M);
  //
  //#if defined( __OP_DBG_STATIC)
  // M[0][0] = M[1][1] = M[2][2] = 1;
  // #endif  

  for(i=0;i<EST_VAR_COUNT;i++)
    Pp->Matr[i][i] += pOPMgr->Qorb[i]*dt;
  
  return;
}
// OP_CovMatrPropagation


//===========================================================================
///  @brief   The function calculates Covariance matrix propagation for static
///
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] dt   - 
///  @param[in] P0   - pointer to 
///  @param[in] Pp   - pointer to 
//===========================================================================
static void OP_CovMatrPropagation_Static(tOPManager *pOPMgr,
                                         float64     dt,
                                         MatrixEx   *P0,
                                         MatrixEx   *Pp)
{ 
  MatrixEx F;
  int i;
  
  /// Jacobian matrix F calculation
  
  /// Matrix initialization
  CreateMatrixEx( Pp, EST_VAR_COUNT, EST_VAR_COUNT);  
  CreateMatrixEx(&F, EST_VAR_COUNT, EST_VAR_COUNT);

  F.Matr[I_B][I_D]  =  dt;

  for(i=0;i<EST_VAR_COUNT;i++)
    F.Matr[i][i] = 1;

  /// Pp matrix calculation
  // Pp = F*P0*F` + Q*dt; 
  MultMatrix_B_TC_Ex(Pp, P0, &F);
  MultMatrix_SimResEx(Pp, &F, Pp);

  for(i=0;i<EST_VAR_COUNT;i++)
    Pp->Matr[i][i] += pOPMgr->Qorb[i]*dt;

  return;
}
// OP_CovMatrPropagation_Static


//===========================================================================
///  @brief   
///
///  @param[in] pOPMgr     - pointer to OP Manager object
///  @param[in] pTopSystem - pointer to Top Systems parameters
///  @param[in] NavCond    - pointer to navigation conditions
///  @param[in] X          - pointer to 
///  @param[in] H          - pointer to 
///  @param[in] dYv        - pointer to 
///  @param[in] dYp        - pointer to
///  @return    number of NSV
//===========================================================================
static int32  OP_HMatrixCalulation(tOPManager     *pOPMgr,
                                   tTopSystem     *pTopSystem,
                                   NAV_CONDITIONS *NavCond,
                                   float64        *X,
                                   MatrixEx       *H, 
                                   float64        *dYv,
                                   float64        *dYp,
                                   eFlag           BinStrFlag,
                                   DATA_STRING    *dataStr,
                                   tTerminal      *DebugTerminal)
{
  tNavTaskParams SatTmp, *pSat;
  float64        elev_limit;
  float64        dx, dy, dz, dVx, dVy, dVz;
  float64        dtmp1, dtmp2;
  float64        delay, YpCorr, YvCorr;
  float64        Yp, Yv, CarrFreq, usR;
  int32          n_meas, i;

  //    [ Hv  ]   Hv = [ Hpp | Hvp | o ]    size(Hpp) = n_meas x 3    size(O) = n_meas x 1
  //  H = [ --- ]                 size(Hvp) = n_meas x 3  
  //    [ Hp  ]   Hp = [  O  | Hpp | 1 ]    size(o) = n_meas x 1
  //                          size(1) = n_meas x 1
  //    [ dYv ]   size(dYv) = n_meas x 1    dYv - is residuals of pseudo range rate
  // dY = [ --- ]                 
  //    [ dYp ]   size(dYp) = n_meas x 1    dYp - is residuals of pseudo range

  n_meas = 0;
  elev_limit = sin(NavCond->ElevMask);  // cos(pi/2 - NavCond->ElevMask);

  for (i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
  {
    /// measurements: range - in meters
    /// measurements: dopler = +dv*x0 , m/sec (then Hvp = Hpp)
    if (pTopSystem->navtask_params[i].sv_num == SATELLITE_INVALID_ID)
      continue;

    pSat = &pTopSystem->navtask_params[i];
    
    if ((pSat->BAD_SAT != 0) || (pSat->paramsOP.COORDINATES != 1)) /// RAIM || coordinates
      continue;
    
      
    SatTmp.sat_coord.coord.x = pSat->paramsOP.coord.x; 
    SatTmp.sat_coord.coord.y = pSat->paramsOP.coord.y; 
    SatTmp.sat_coord.coord.z = pSat->paramsOP.coord.z;

    /// rough signal delay estimation
    dx = SatTmp.sat_coord.coord.x - X[I_X];
    dy = SatTmp.sat_coord.coord.y - X[I_Y]; 
    dz = SatTmp.sat_coord.coord.z - X[I_Z];
    delay = l_cLight * sqrt(dx * dx + dy * dy + dz * dz);

    SatTmp.sat_coord.coord.vx = pSat->paramsOP.coord.vx; 
    SatTmp.sat_coord.coord.vy = pSat->paramsOP.coord.vy; 
    SatTmp.sat_coord.coord.vz = pSat->paramsOP.coord.vz;

    /// Earth rotation compensation
    Compensate_Rotation(&SatTmp.sat_coord.coord, &SatTmp.sat_coord.coord, delay);        

    /// Range and range-rate corrections
    YpCorr = 0;
    YvCorr = 0; 

    /// Computing of signal delay in atmosphere: @todo: atmosphere delay calculation

    // ionosphere correction
    if (NavCond->IonosphereModel != 0)
    { 
      YpCorr += (float64)(pSat->sat_coord.dt_ION); 
    }
    // troposphere correction
    if (NavCond->TroposphereModel != 0)
    { 
      YpCorr += (float64)(pSat->sat_coord.dt_TRO); 

    }
      
    /// LOS and DV calculation
    dx = SatTmp.sat_coord.coord.x - X[I_X];
    dy = SatTmp.sat_coord.coord.y - X[I_Y];
    dz = SatTmp.sat_coord.coord.z - X[I_Z];

    dVx = SatTmp.sat_coord.coord.vx - X[I_VX];
    dVy = SatTmp.sat_coord.coord.vy - X[I_VY];
    dVz = SatTmp.sat_coord.coord.vz - X[I_VZ];

    /// measurements estimation calculation
    // dYp = Yp - hp(Xp)  // residuals of pseudo range 
    // dYv = Yv - hv(Xp)  // residuals of pseudo range rate
    CarrFreq = f0_GPS; // + cLight*pTopSystem->EphGPS[pSat->sv_num-1].af1;
    
    Yp = sqrt(dx*dx + dy*dy + dz*dz);      ///< geometric range
    dYp[n_meas] = cLight*pSat->paramsOP.raw_data.ps_delay - 
      (Yp + X[I_B] - cLight*pSat->paramsOP.coord.dt_SV + YpCorr);

    Yv = (dx*dVx + dy*dVy + dz*dVz) / Yp;  ///< geometric range rate
    dYv[n_meas] = cLight/CarrFreq*pSat->paramsOP.raw_data.ps_freq - 
      (Yv + X[I_D] - cLight*pTopSystem->EphGPS[pSat->sv_num-1].af1 + YvCorr);

/*	Spirit's debugging information
	Turn it off for release image 
	
    if (BinStrFlag == fg_OFF)
    {
      sprintf(dataStr->str, "RAW: SV:%2d, dYp:%8.3lf, dYv:%8.3lf, psd:%12.3lf, Yp:%12.3lf, dt_SV:%12.3lf, psf:%9.3lf, Yv:%9.3lf\r\n", 
        pSat->sv_num, dYp[n_meas], dYv[n_meas], (cLight*pSat->paramsOP.raw_data.ps_delay), Yp, 
        (cLight*pSat->paramsOP.coord.dt_SV), (cLight/CarrFreq*pSat->paramsOP.raw_data.ps_freq), Yv);
      DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
    }  
*/
     // OP raw data filter: used for all visible SV
    if(pOPMgr->fSynchronization == SYNC_OK)
    {
      if (fabs(dYp[n_meas]) > OP_RAW_FILTER_RANGE) // || (fabs(dYv[n_meas]) > OP_RAW_FILTER_VRAD))
      {
         pSat->paramsOP.BAD_SAT_OP = 1;
         continue;
      }
    }

    // Elevation analyzes
    usR = sqrt(X[I_X]*X[I_X] + X[I_Y]*X[I_Y] + X[I_Z]*X[I_Z]);      ///< user state vector magnitude;
    dtmp1 = dx*X[I_X] + dy*X[I_Y] + dz*X[I_Z];   ///< dot product of user state vector & LOS
    if( elev_limit*Yp*usR > dtmp1)
    {// screening by elevation mask
        pSat->paramsOP.ELEV_MSK_OP = 1;
        continue;
    }

    /// String of sensitivity matrix H calculation
    dtmp1 = 1./(Yp);     ///  dtmp1 = 1/(ro)
    dtmp2 = Yv * dtmp1;  /// for Hv calculation (see below)

    // Hv calculation
    H->Matr[n_meas][I_VX] = -dx*dtmp1;  
    H->Matr[n_meas][I_VY] = -dy*dtmp1;  /// dtmp1 = 1/(ro)
    H->Matr[n_meas][I_VZ] = -dz*dtmp1;  

    H->Matr[n_meas][I_X] = -dtmp1 * ( dVx - dx*dtmp2);  /// dtmp1 = /*ro;  dtmp2 - see above
    H->Matr[n_meas][I_Y] = -dtmp1 * ( dVy - dy*dtmp2);  /// dtmp1 = /*ro;  dtmp2 - see above
    H->Matr[n_meas][I_Z] = -dtmp1 * ( dVz - dz*dtmp2);  /// dtmp1 = /*ro;  dtmp2 - see above
  
    H->Matr[n_meas][I_B] = 0;
    H->Matr[n_meas][I_D] = 1;

    /// set Used_OP flag
    pSat->paramsOP.USED_OP = 1;

    n_meas ++;
  }
  
  H->mRow = 2*n_meas;   H->mCol = EST_VAR_COUNT;

  /// Hp calculation
  for (i = 0; i < n_meas; i++)
  {
    H->Matr[n_meas+i][I_VX] = 0;
    H->Matr[n_meas+i][I_VY] = 0;
    H->Matr[n_meas+i][I_VZ] = 0;

    H->Matr[n_meas+i][I_X]  = H->Matr[i][I_VX];
    H->Matr[n_meas+i][I_Y]  = H->Matr[i][I_VY];
    H->Matr[n_meas+i][I_Z]  = H->Matr[i][I_VZ];

    H->Matr[n_meas+i][I_B]  = 1;
    H->Matr[n_meas+i][I_D]  = 0;
  }

  return n_meas;
}
// OP_HMatrixCalulation

//===========================================================================
///  @brief     The function calculates the gain matrix of Kalman Filter
///
///  @param[in] pOPMgr - pointer to OP Manager object
///  @param[in] K      - pointer to gain matrix (to calculation)
///  @param[in] Pp     - pointer to a priori covariance matrix
///  @param[in] H      - pointer to H matrix (2*nMeas x 8)
///  @param[in] nMeas  - number of independent measurements 
//                       (must be identical to Hpp (and Hvv) string count)
//===========================================================================
static void OP_KF_Gain(tOPManager *pOPMgr,
                       MatrixEx   *K,
                       MatrixEx   *Pp,
                       MatrixEx   *H,
                       int32       nMeas)
{
  int32 i, n;
  MatrixEx A, B;  
  
  // K = A*Inv(B);
  // A = Pp*H';   
  // B = H*Pp*H' + R = H*A + R;

  CreateMatrixEx(&A,EST_VAR_COUNT,2*nMeas);
  CreateMatrixEx(&B,2*nMeas,2*nMeas);

  /// A and B matrix calculation
  MultMatrix_B_TC_Ex( &A, Pp, H);
  MultMatrix_SimResEx( &B, H, &A);

  // B = B+R  
  n = nMeas+nMeas;
  for (i = 0; i < nMeas; i++)   B.Matr[i][i] += pOPMgr->R_prr;
  for (i = nMeas; i < n; i++)   B.Matr[i][i] += pOPMgr->R_pr;

  /// Inversion B ( B is symmetrical)
  InvSymmetricalMatrixEx(&B, &B);
  /// Singularity of matrix checking
  if (!B.mCol)
  {
    DestroyMatrixEx(&A);
    DestroyMatrixEx(&B);
    DestroyMatrixEx(K);
    return;
  }
  
  /// Gain calculation
  MultMatrixEx(K, &A, &B);

  /// Destroy Matrices
  DestroyMatrixEx(&A);
  DestroyMatrixEx(&B);

  return;
}
// OP_KF_Gain

//===========================================================================
///  @brief   
///
///  @param[in] pOPMgr - pointer to OPManager object
///  @param[in] Pu     - pointer to 
///  @param[in] K      - pointer to 
///  @param[in] Pp     - pointer to 
///  @param[in] H      - pointer to 
///  @param[in] n_meas - 
//===========================================================================
static void OP_CovMatrUpdate(tOPManager *pOPMgr,
                             MatrixEx   *Pu,
                             MatrixEx   *K,
                             MatrixEx   *Pp,
                             MatrixEx   *H,
                             int32       n_meas)
{
  int32 i, j, l;
  MatrixEx A;

  // Pu = (K*H - I)*Pp*(K*H - I)' + K*R*K' = A*Pp*A' + K*R*K';

  // A = K*H - I
  MultMatrixEx( &A, K, H);
  for (i = 0; i < A.mCol; i++)  A.Matr[i][i] -= 1.;
  
  // Pu = (K*H - I)*Pp*(K*H - I)'
  MultMatrix_B_TC_Ex( Pu, Pp, &A);
  MultMatrix_SimResEx( Pu, &A, Pu);

  // Pu += K*R*K'
  //    [ Rprr * I(nMeas) |       0         ]
  // R =  [ --------------- | --------------- ]
  //    [       0         | Rpr * I(nMeas)  ]
  for (i = 0; i < EST_VAR_COUNT; i++) 
    for (j = 0; j <= i; j++)
    {
      for (l = 0; l < n_meas; l++)
        Pu->Matr[i][j] += (pOPMgr->R_prr * K->Matr[i][l] * K->Matr[j][l] + 
            pOPMgr->R_pr * K->Matr[i][n_meas + l] * K->Matr[j][n_meas + l]);
      Pu->Matr[j][i] = Pu->Matr[i][j];
    }

  return;

} 
// OP_CovMatrUpdate

//===========================================================================
///  @brief 
///   
///  @param[in] pOPMgr   - pointer to OP Manager object
///  @param[in] btUpdate - on-board time to state vector calculation 
///                        (it must be from GPS.0)
//===========================================================================
static void OP_Propagation(tOPManager *pOPMgr, tTopSystem *pTopSystem, tGPSTime *btUpdate)
{
  int32 i, itrCount ;
  float64 dt, tau, DQ[EST_VAR_COUNT], bias, mjd0;

  /// Different between current and last time in seconds 
  tau = (btUpdate->wn - pOPMgr->btLastUpdate.wn) * T_SECONDS_PER_WEEK;
  tau += (btUpdate->t - pOPMgr->btLastUpdate.t);
  
  // to do:!!! �������� �������� ������ ����� (��������)
  /// Last MJD calculation
  MJD_From_UTCEx(&pOPMgr->btLastUpdate, &mjd0);
  mjd0 -= (pTopSystem->IonUTC.dtls * T_DAYS_PER_SECOND); // + pOPMgr->X[I_B];

  itrCount = OP_PRP_ITERATION_LIMIT;

  /// integration step calculation
  dt = fSign(tau)*Min(Abs(tau) , Abs(OP_PRP_MAX_STEP));
  
  /// save receiver bias 
  bias = pOPMgr->X[I_B];  pOPMgr->X[I_B] = mjd0;

  while (dt > OP_PRP_TIME_RESOLUTION)
  {
    /// State update
    switch (pOPMgr->mOP_DynModelType)
    {
    case OP_DM_ORBITAL_MOTION:
      /// Covariance matrix propagation
      OP_CovMatrPropagation( pOPMgr, &pOPMgr->X[I_X], &pOPMgr->X[I_VX], dt, &(pOPMgr->P0), &(pOPMgr->P0));
 
      // state vector increments
      rk4( pOPMgr->pfDM, pOPMgr->X, (6+1), dt, (int32)&pOPMgr->DM, DQ);
      break;

    case OP_DM_STATIC:
    default:
      /// Covariance matrix propagation
      OP_CovMatrPropagation_Static( pOPMgr, dt, &(pOPMgr->P0), &(pOPMgr->P0));

      // state vector increments
      for(i=0;i<6;i++)
        DQ[0] = 0;

      break;
    }

    /// state propagation
    for (i = 0; i < 7; i++)   pOPMgr->X[i] += DQ[i];
    bias += pOPMgr->X[I_D]*dt;

    /// time update 
    pOPMgr->btLastUpdate.t += dt;
    if (pOPMgr->btLastUpdate.t >= T_SECONDS_PER_WEEK)
    {
      pOPMgr->btLastUpdate.t -= T_SECONDS_PER_WEEK;
      pOPMgr->btLastUpdate.wn ++;
    }
    else if (pOPMgr->btLastUpdate.t < 0)
    {
      pOPMgr->btLastUpdate.t += T_SECONDS_PER_WEEK;
      pOPMgr->btLastUpdate.wn --;
    }
    
    /// next integration step calculation
    tau -= OP_PRP_MAX_STEP;
    dt = fSign(tau)*Min(Abs(tau) , Abs(OP_PRP_MAX_STEP));
    
    if (!(--itrCount))  
    { 
      /// out of calculation time resource
      pOPMgr->fOP_Condition = OP_COND_PROPAGATION_PROC;
      break;
    }
  }

  pOPMgr->X[I_B] = bias;
  
  return;
} 
// OP_Propagation



