/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains functions to Sun and Moon gravitation 
 *                           calculation
 *   @file                   SunMoonGravitation.c
 *   @author                 Dmitry Churikov
 *   @date                   14.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#include "SunMoonGravitation.h"
#include "TimeEx.h"
#include "../Sources/baseop.h"

#include <string.h>
#include <math.h>

/// Indexes of phisical vectors in Cartesian frame
#define X  (0)
#define Y  (1)
#define Z  (2)

//===================================================================================
///  @brief   The function calculates varying part of transfer matrix 
///           from Ecliptic-Equinox Frame to current Equatorial-Equinox Frame
///
///  @param[in] T - Julian time from J2000 (in centuries)
///                 T = JT(TDB) - JT(TDB(J2000))
///  @param[in] M - buffer to matrix placing
//===================================================================================
static void EclToCurrentEq_Matrix(float64 T, float64 M[2][2]);

//===================================================================================
///  @brief   The function calculates varying part of transfer matrix 
///           from current Equatorial-Equinox Frame to ECEF
/// 
///  @param[in] Tu - Julian time from J2000 (in centuries)
///                  Tu = JT(UT1) - JT(UT1(J2000))
///  @param[in] M  - buffer to matrix placing
//===================================================================================
static void EqToECEF_Matrix(float64 Tu, float64 M[2][2]);

//======================================================================================
///  @brief   The function calculates Sun center position in Geocentric Ecliptic Frame 
///           on J2000. Accuracy of Rs vector calculation is about 0.05 deg, 0.00001 AU
///
///  @param[in] T  - Julian time from J2000 (in centuries)
///                  T = JT(TDB) - JT(TDB(J2000))
///  @param[in] Rs - pointer to buffer to calculating vector placing
//======================================================================================
static void SunPos_GeoCenterEclFrame(float64 T, float64 *Rs);

//======================================================================================
///  @brief   The function calculates Moon center position in Geocentric Ecliptic 
///           Frame on J2000. Accuracy of Rs vector calculation is about 0.07 deg, 
///           0.0003  mean radius of Moon orbit
///  @param[in] T  - Julian time from J2000 (in centuries)
///                  T = JT(TDB) - JT(TDB(J2000))
///  @param[in] Rm - pointer to buffer to calculating vector placing
//===================================================================================
static void MoonPos_GeoCenterEclFrame(float64 T, float64 *Rm);


//===================================================================================
///  @brief   Initialization Sun&Moon Data object
/// 
///  @param[in] pSMD - pointer of Sun&Moon Data object
//===================================================================================
void SunMoonData_Init(tSunMoonData* pSMD)
{
  // zero object
	memset (pSMD, 0, sizeof (tSunMoonData));  

  // special parameters
  pSMD->dATex = DT_UTC_GPS_2005 - DT_UTC_GPS_2000;  ///< On December 2006

  return;
} 
// SunMoonData_Init

//===================================================================================
///  @brief   The function calculates the perturbations due to Sun and Moon 
///           gravity acting on the object
///
///  @param[in] pSMD   - pointer to Sun&Moon Data object (can be 0)
///  @param[in] r      - pointer to ECEF state vector
///  @param[in] mjd_u  - current time from J2000.0 (in days)  
///                      mjd_u = JT(UTC) - JT(UTC(J2000.))
///  @param[in] fUsing - gravity pertrubation account flag
///  @param[in] g_sm   - pointer to gravity pertrubation vector (it is calculated)
//===================================================================================
void SunMoonData_Gravitation(tSunMoonData *pSMD,
                             float64      *r,
                             float64       mjd_u,
                             uint32        fUsing,
                             float64      *g_sm)
{
  float64 mjc_u, mjc_tai, ro3, rob3;
  float64 M1_yz[2][2], M2_xy[2][2];
  float64 R_tm[3], Rb[3];

  if (!fUsing)
  { // no obbjects to multybody pertrubation calculation
    memset(g_sm, 0, 3*sizeof(float64));
  }
  
  // check of recalculation necessity
  if (pSMD)
  {
    mjc_u = (pSMD->mjdMean_t - mjd_u);  // mjc_u as temporary variable
    if ((mjc_u < pSMD->mjdTau) && (mjc_u > (-pSMD->mjdTau)))
    {// Mean gravitation force is aplied
      _memcpy( g_sm, pSMD->Mean_g, sizeof(pSMD->Mean_g));
      return;
    }
    
    // init of update of mean gravitation force
    mjd_u += pSMD->mjdTau;
    pSMD->mjdMean_t = mjd_u;
  }

  // Sun and Moon pertrubations calculation
  memset(g_sm, 0, 3*sizeof(float64));

  // Centuries from J2000 calculation
  mjc_u = mjd_u * T_CENTURIES_PER_DAY; // mjc_u = JT(UTC) - JT(UTC(J2000.))
  mjc_tai = (mjd_u + pSMD->dATex*T_DAYS_PER_SECOND)*T_CENTURIES_PER_DAY;  // mjc_tai = JT(TAI) - JT(TAI(J2000.))

  // Ecliptic-Equinox Frame to current Equatorial-Equinox Frame matrix calculation
  EclToCurrentEq_Matrix(mjc_tai, M1_yz);  // 2x2 matrix = {{yy, yz},{zy zz}}
  // Current Equatorial-Equinox Frame to ECEF matrix calculation
  EqToECEF_Matrix(mjc_u, M2_xy);    // 2x2 matrix = {{xx, xy},{yx yy}}
  

  // Sun pertrubations calculation
  if (fUsing & SMG_USE_SUN)
  {
    // Sun pozition in Ecliptical Frame (Rb - is Sun mass center radius vector)
    SunPos_GeoCenterEclFrame(mjc_tai, Rb);

    // Sun pozition in Current Equatorial-Equinox Frame calculation
    R_tm[X] = Rb[X];
    R_tm[Y] =           M1_yz[0][0]*Rb[Y] + M1_yz[0][1]*Rb[Z];
    R_tm[Z] =           M1_yz[1][0]*Rb[Y] + M1_yz[1][1]*Rb[Z];

    // Sun pozition in ECEF calculation (Rb - is Sun mass center radius vector in ECEF)
    Rb[X] = M2_xy[0][0]*R_tm[X] + M2_xy[0][1]*R_tm[Y];
    Rb[Y] = M2_xy[1][0]*R_tm[X] + M2_xy[1][1]*R_tm[Y];
    Rb[Z] =                                   R_tm[Z];

    // (Rb - r) vector and (1/|Rb - r|^3) calculation
    R_tm[X] = Rb[X] - r[X];   R_tm[Y] = Rb[Y] - r[Y];   R_tm[Z] = Rb[Z] - r[Z];
    ro3 = R_tm[X]*R_tm[X] + R_tm[Y]*R_tm[Y] + R_tm[Z]*R_tm[Z];
    ro3 = 1. / (ro3 * sqrt(ro3));

    // 1/|Rb|^3) calculation
    rob3 = Rb[X]*Rb[X] + Rb[Y]*Rb[Y] + Rb[Z]*Rb[Z];
    rob3 = 1. / (rob3 * sqrt(rob3));

    // Sun pertrubations calculation
    g_sm[X] += SUN_MU*(rob3*Rb[X] - ro3*R_tm[X]);
    g_sm[Y] += SUN_MU*(rob3*Rb[Y] - ro3*R_tm[Y]);
    g_sm[Z] += SUN_MU*(rob3*Rb[Z] - ro3*R_tm[Z]);
  }

  // Moon pertrubations calculation
  if (fUsing & SMG_USE_MOON)
  {
    // Moon pozition in Ecliptical Frame (Rb - is Sun mass center radius vector)
    MoonPos_GeoCenterEclFrame(mjc_tai, Rb);

    // Moon pozition in Current Equatorial-Equinox Frame calculation
    R_tm[X] = Rb[X];
    R_tm[Y] =           M1_yz[0][0]*Rb[Y] + M1_yz[0][1]*Rb[Z];
    R_tm[Z] =           M1_yz[1][0]*Rb[Y] + M1_yz[1][1]*Rb[Z];

    // Moon pozition in ECEF calculation (Rb - is Moon mass center radius vector)
    Rb[X] = M2_xy[0][0]*R_tm[X] + M2_xy[0][1]*R_tm[Y];
    Rb[Y] = M2_xy[1][0]*R_tm[X] + M2_xy[1][1]*R_tm[Y];
    Rb[Z] =                                   R_tm[Z];

    // (Rb - r) vector and (1/|Rb - r|^3) calculation
    R_tm[X] = Rb[X] - r[X];   R_tm[Y] = Rb[Y] - r[Y];   R_tm[Z] = Rb[Z] - r[Z];
    ro3 = R_tm[X]*R_tm[X] + R_tm[Y]*R_tm[Y] + R_tm[Z]*R_tm[Z];
    ro3 = 1. / (ro3 * sqrt(ro3));

    // 1/|Rb|^3) calculation
    rob3 = Rb[X]*Rb[X] + Rb[Y]*Rb[Y] + Rb[Z]*Rb[Z];
    rob3 = 1. / (rob3 * sqrt(rob3));

    // Sun pertrubations calculation
    g_sm[X] += MOON_MU*(rob3*Rb[X] - ro3*R_tm[X]);
    g_sm[Y] += MOON_MU*(rob3*Rb[Y] - ro3*R_tm[Y]);
    g_sm[Z] += MOON_MU*(rob3*Rb[Z] - ro3*R_tm[Z]);
  }

  // Calculated data saving
  if (pSMD) _memcpy( pSMD->Mean_g, g_sm, sizeof(pSMD->Mean_g));
  

  return;
} 
// SunMoonData_Gravitation

//===================================================================================
///  @brief   The function calculates varying part of transfer matrix 
///           from Ecliptic-Equinox Frame to current Equatorial-Equinox Frame
///
///  @param[in] T - Julian time from J2000 (in centuries)
///                 T = JT(TDB) - JT(TDB(J2000))
///  @param[in] M - buffer to matrix placing
//===================================================================================
static void EclToCurrentEq_Matrix(float64 T, float64 M[2][2])
{
  #define CE0  (0.917482139208287)    ///<  cos(23.43928*PI/180) = cos(ecl0)
  #define SE0  (0.397776978008763)    ///<  sin(23.43928*PI/180) = sin(ecl0)
  #define C2   (2.26965524811429e-4)  ///<  (46.8150/3600)*M_PI/180 = de

  float64 sde, cde;
  
  sde = C2*T;   cde = 1. - 0.5*sde*sde;  ///< sin(de); cos(de)

  // ecl = C1 - C2*T; // (23.43928 - 46.8150/3600 *T)*M_PI/180;
  
  M[0][0] =  CE0*cde + SE0*sde;  ///<  cos(ecl0 - de);
  M[0][1] = -SE0*cde + CE0*sde;  ///< -sin(ecl0 - de);
  
  M[1][0] = -M[0][1];  M[1][1] =  M[0][0];

  #undef CE0
  #undef SE0  
  #undef C2

  return;
} 
// EclToCurrentEq_Matrix

//===================================================================================
///  @brief   The function calculates varying part of transfer matrix 
///           from current Equatorial-Equinox Frame to ECEF
/// 
///  @param[in] Tu - Julian time from J2000 (in centuries)
///                  Tu = JT(UT1) - JT(UT1(J2000))
///  @param[in] M  - buffer to matrix placing
//===================================================================================
static void EqToECEF_Matrix(float64 Tu, float64 M[2][2])
{
  float64 S;
  uint32 ulS;
  
  /// GMST calculation
  S = 67310.54841 + (876600.*3600. + 8640184.812866)*Tu +  0.093104*Tu*Tu;
  // S = 67310.54841 + (876600*3600 + 8640184.812866)*T + 0.093104*T*T -6.2e-6*T*T*T;
  
  /// Siderial seconds from current siderial day calculation
  ulS = (uint32)S;  ///< this convertion is valid up to 2100 year
  S = S - (float64)ulS + (float64)(ulS % 86400); 

  /// JT(UT1) seconds from the begin of current sideral day calculation
  S *= GSMT_TO_UT; // sec

  /// ECEF rotation angle calculation
  S *= EARTH_OMEGA;

  // Transfer matrix calculation
  M[0][0] =  cos(S);    M[0][1] = sin(S);
  M[1][0] = -M[0][1];   M[1][1] = M[0][0];

  return;
}
// EqToECEF_Matrix

//======================================================================================
///  @brief   The function calculates Sun center position in Geocentric Ecliptic Frame 
///           on J2000. Accuracy of Rs vector calculation is about 0.05 deg, 0.00001 AU
///
///  @param[in] T  - Julian time from J2000 (in centuries)
///                  T = JT(TDB) - JT(TDB(J2000))
///  @param[in] Rs - pointer to buffer to calculating vector placing
//======================================================================================
static void SunPos_GeoCenterEclFrame(float64 T, float64 *Rs)
{
  uint32 ultm;
  float64 l1, L1, lamda, r;
  float64 cosl1, sinl1;

  /// Calculates mean orbital elements of Sun
  // l1 = 1287099.804 + (99*r +  1292581.224)*T - 0.577*T*T  - 0.012*T*T*T;// in ang.sec
  l1 = 1287099.804 + (128304000 + 1292581.224)*T;  /// in ang.sec
  ultm = (uint32)l1;
  l1 = (l1 - (float64)(ultm - ultm % 1296000)) * MC_ANG_SEC_TO_RADIANS;  /// in rad.

    L1 = 280.46457166 + 35999.37244981*T;         /// in deg
  ultm = (uint32)L1;
  L1 = L1 - (float64)ultm + (float64)(ultm%360);  /// in deg
   
  // lamda = L1 + 1.9171*sin(l1) + 0.02*sin(2*l1) + 0.0003*sin(3*l1);      /// in deg
  cosl1 = cos(l1);    sinl1 = sin(l1);
  lamda = L1 + (1.9171 + 0.04*cosl1 + 0.0009 - 0.0012*sinl1*sinl1)*sinl1;  /// in deg
  lamda *= MC_DEGREES_TO_RADIANS;
  
  // r = (1.00014 - 0.01673*cos(l1) - 0.00014*cos(2*l1)) * AU; /// in meters
  r = (1.00014 - 0.01673*cosl1 - 0.00028*cosl1*cosl1 + 0.00014) * AU; 
    
  // ecliptic geocenter coordinates calculation (for beta = 0)
  Rs[X] = r *cos(lamda);
  Rs[Y] = r *sin(lamda);
  Rs[Z] = 0;

  return;
} 
// SunPos_GeoCenterEclFrame

//======================================================================================
///  @brief   The function calculates Moon center position in Geocentric Ecliptic 
///           Frame on J2000. Accuracy of Rs vector calculation is about 0.07 deg, 
///           0.0003  mean radius of Moon orbit
///  @param[in] T  - Julian time from J2000 (in centuries)
///                  T = JT(TDB) - JT(TDB(J2000))
///  @param[in] Rm - pointer to buffer to calculating vector placing
//===================================================================================
static void MoonPos_GeoCenterEclFrame(float64 T, float64 *Rm)
{
  static float64 l, l1, F, D, OM, L;
  static float64 lamda, beta, Q, rm;
  static float64 sin_l, sin_l1, sin_D, sin_F, sin_2l, sin_2D, sin_2F, sin_4D;
  static float64 cos_l, cos_l1, cos_D, cos_F, cos_2l, cos_2D, cos_2F, cos_4D;
  static float64 sin_lm2D, cos_lm2D;
  static float64 dtm1, dtm2;
  uint32 ultm;

  /// Fundamental arguments calculation
  l = (485866.733  + (1717915922.633 + 31.310*T)*T)* MC_ANG_SEC_TO_RADIANS;  /// rad
  l1= (1287099.804 + ( 129596581.224 -  0.577*T)*T)* MC_ANG_SEC_TO_RADIANS;  /// rad
  F = (335778.877  + (1739527263.137 - 13.257*T)*T)* MC_ANG_SEC_TO_RADIANS;  /// rad
  D = (1072261.307 + (1602961601.328 -  6.891*T)*T)* MC_ANG_SEC_TO_RADIANS;  /// rad
  OM= (450160.280  - (   6962890.539 +  7.455*T)*T)* MC_ANG_SEC_TO_RADIANS;  /// rad
  L = (OM + F)* MC_RADIANS_TO_DEGREES;  /// deg
  ultm = (uint32)L;
  L = L - (float64)ultm + (float64)(ultm%360);  /// deg

  /// sin and cos calculation
  sin_l  = sin(l);        cos_l  = cos(l);
  sin_l1 = sin(l1);       cos_l1 = cos(l1);
  sin_D  = sin(D);        cos_D  = cos(D);
  sin_F  = sin(F);        cos_F  = cos(F);
  sin_2l = 2.*sin_l*cos_l;    cos_2l = 2.*cos_l*cos_l - 1.;
  sin_2D = 2.*sin_D*cos_D;    cos_2D = 2.*cos_D*cos_D - 1.;
  sin_2F = 2.*sin_F*cos_F;    cos_2F = 2.*cos_F*cos_F - 1.;
  sin_4D = 2.*sin_2D*cos_2D;    cos_4D = 2.*cos_2D*cos_2D - 1.;
  sin_lm2D = sin_l*cos_2D - sin_2D*cos_l;
  cos_lm2D = cos_l*cos_2D + sin_2D*sin_l;

  /// geocentric ecliptic parameters calculation
  // lamda = L + 6.289*sin(l);
  lamda = L + 6.289 * sin_l;
  // lamda = lamda -1.274 * sin(l - 2*D);
  lamda += -1.274 * sin_lm2D;
  // lamda = lamda + .658 * sin(2*D);
  lamda +=   .658 * sin_2D;
  // lamda = lamda + .214 * sin(2*l);
  lamda +=   .214 * sin_2l;
  // lamda = lamda - .186 * sin(l1);
  lamda += - .186 * sin_l1;
  // lamda = lamda - .114 * sin(2*F);
  lamda += - .114 * sin_2F;
  // lamda = lamda - .059 * sin(2*l - 2*D);
  lamda += - .059 * (sin_2l*cos_2D - sin_2D*cos_2l);
  // lamda = lamda - .057 * sin(l - 2*D + l1);
  lamda += - .057 * (sin_lm2D*cos_l1 + sin_l1*cos_lm2D);
  // lamda = lamda + .053 * sin(l + 2*D);
  lamda +=   .053 * (sin_l*cos_2D + sin_2D*cos_l);
  // lamda = lamda - .046 * sin(l1 - 2*D);
  lamda += - .046 * (sin_l1*cos_2D - sin_2D*cos_l1);
  // lamda = lamda + .041 * sin(l - l1);
  lamda +=   .041 * (sin_l*cos_l1 - sin_l1*cos_l);
  // lamda = lamda - .035 * sin(D);
  lamda += - .035 * sin_D;
  // lamda = lamda - .031 * sin(l + l1);
  lamda += - .031 * (sin_l*cos_l1 + sin_l1*cos_l);
  // lamda = lamda - .015 * sin(2*F - 2*D);
  lamda += - .015 * (sin_2F*cos_2D - sin_2D*cos_2F);
  // lamda = lamda + .011 * sin(l - 4*D);
  lamda +=   .011 * (sin_l*cos_4D - sin_4D*cos_l);

  lamda = lamda * MC_DEGREES_TO_RADIANS;  /// to radians


  // beta = 5.128 * sin(F);
  beta = 5.128 * sin_F;
  // beta = beta + .281 * sin(F + l);
  dtm1 = sin_F*cos_l;   dtm2 = sin_l*cos_F;
  beta +=   .281 * (dtm1 + dtm2);
  // beta = beta - .278 * sin(F - l);
  beta += - .278 * (dtm1 - dtm2);
  // beta = beta + .055 * sin(F - l + 2*D );
  dtm1 = sin_F*cos_lm2D;    dtm2 = sin_lm2D*cos_F;
  beta +=   .055 * (dtm1 - dtm2);
  // beta = beta - .046 * sin(F + l - 2*D);
  beta += - .046 * (dtm1 + dtm2);
  // beta = beta + .033 * sin(F + 2*D);
  dtm1 = sin_F*cos_2D;    dtm2 = sin_2D*cos_F;
  beta +=   .033 * (dtm1 + dtm2);
  // beta = beta - .173 * sin(F - 2*D);
  beta += - .173 * (dtm1 - dtm2);
  // beta = beta + .017 * sin(2*l + F);
  beta +=   .017 * (sin_2l*cos_F + sin_F*cos_2l);

  beta = beta * MC_DEGREES_TO_RADIANS;  /// to radians


  // Q = 1 + .0545 * cos(l);
  Q = 1. + .0545 * cos_l;
  // Q = Q + .0100 * cos(l - 2*D);
  Q +=  .0100 * cos_lm2D;
  // Q = Q + .0082 * cos(2*D);
  Q +=  .0082 * cos_2D;
  // Q = Q + .0030 * cos(2*l);
  Q +=  .0030 * cos_2l;
  // Q = Q + .0009 * cos(l + 2*D);
  Q +=  .0009 * (cos_l*cos_2D - sin_l*sin_2D);
  // Q = Q + .0006 * cos(l1 - 2*D);
  Q +=  .0006 * (cos_l1*cos_2D + sin_l1*sin_2D);
  // Q = Q + .0004 * cos(l1 + l - 2*D);
  Q +=  .0004 * (cos_l1*cos_lm2D - sin_l1*sin_lm2D);
  // Q = Q + .0003 * cos(l - l1);
  Q +=  .0003 * (cos_l*cos_l1 + sin_l*sin_l1);

  rm = MOON_ORBITAL_RADIUS / Q;       // to meters


  /// Calculate the cartesian coordinates
  /// of the geocentric ecliptic lunar position
  dtm1 = rm * cos(beta);
  Rm[X] = dtm1 * cos(lamda);
  Rm[Y] = dtm1 * sin(lamda);
  Rm[Z] = rm * sin(beta);

} 
// MoonPos_GeoCenterEclFrame
