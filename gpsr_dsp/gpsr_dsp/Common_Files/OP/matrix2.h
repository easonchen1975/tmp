/*****************************************************************************
 *    Copyright (c) 2007 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  for OP task (is used large matrixes)
 *   @file                   matrix2.h
 *   @author                 Dmitry Churikov
 *   @date                   10.01.2007
 *   @version                1.1
 */
/*****************************************************************************/
#ifndef _MATRIX2_H_
#define _MATRIX2_H_

#include "../sources/comtypes.h"
#include "../sources/matrix.h"


#define MATRIXEX_MAX_N  (24)  ///< number row
#define MATRIXEX_MAX_M  (24)  ///< number column

/// matrix extension 
typedef struct
{ 
  float64 Matr[MATRIXEX_MAX_N][MATRIXEX_MAX_M];
  int32 mRow, mCol; // row, column
} 
MatrixEx;

//===================================================================================
///  BLOCK FOR IMPLEMENTATION OF MATRIX FUNCTIONS 
//===================================================================================

//===================================================================================
///  @brief     Matrix initialization
///
///  @param[in] A - pointer to instance
///  @param[in] N - row
///  @param[in] M - column
//===================================================================================
EXTERN_C void CreateMatrixEx(MatrixEx *A, int32 N, int32 M);

//===================================================================================
///  @brief     Free matrix dynamic allocation memory
///
///  @param[in] A - pointer to instance
//===================================================================================
EXTERN_C void DestroyMatrixEx(MatrixEx * A);

//===================================================================================
///  @brief     Matrix copy A = B
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
//===================================================================================
EXTERN_C void CopyMatrixEx(MatrixEx *A, MatrixEx *B);

//===================================================================================
///  @brief     Multiplication of matrices A = B*C
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
///  @param[in] C - pointer to instance
//===================================================================================
EXTERN_C void MultMatrixEx(MatrixEx *A, MatrixEx *B, MatrixEx *C);

//===================================================================================
///  @brief     Multiplication of matrices A = B*T(C) = B*C'
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
///  @param[in] C - pointer to instance
//===================================================================================
EXTERN_C void MultMatrix_B_TC_Ex(MatrixEx *A, MatrixEx *B, MatrixEx *C);

//===================================================================================
///  @brief     Multiplication of matrices A = B*C, and A is a symmetrical
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
///  @param[in] C - pointer to instance
//===================================================================================
EXTERN_C void MultMatrix_SimResEx(MatrixEx *A, MatrixEx *B, MatrixEx *C);

//===================================================================================
///  @brief     Inverse  symmetrical matrix
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
//===================================================================================
EXTERN_C void InvSymmetricalMatrixEx( MatrixEx *A, MatrixEx *B);


//===================================================================================
///  @brief     Inverse upper triangular squarte matrix 
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
//===================================================================================
EXTERN_C void InvUpTriangularMatrixEx( MatrixEx *A, MatrixEx *B);

//===================================================================================
///  @brief     QR-decomposition of the matrix A
///             Resultant matrices are contained in R and Q correspondingly
///
///  @param[in] A - pointer to instance
///  @param[in] Q - pointer to instance
///  @param[in] R - pointer to instance
//===================================================================================
EXTERN_C void QR_DecompositionEx(MatrixEx *A, MatrixEx *Q, MatrixEx *R);

#endif //_MATRIX2_H_
