/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 ****************************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains atmosphere object to atmospheric drag calculation
 *   @file                   atmosphere.h
 *   @author                 Dmitry Churikov
 *   @date                   22.12.2006
 *   @version                1.0
 */
/**************************************************************************************/
#ifndef __ATMOSPHERE_H
#define __ATMOSPHERE_H

#ifdef USE_DSPBIOS
#include "comtypes.h"
#else
#include "../Sources/comtypes.h"
#endif

#define ATM_H1       (500000.)    ///< Base hight for atmospheric model, m
#define ATM_RO1      (2.e-13)     ///< Density value on base hight, kg/m/m/m
#define ATM_EARTH_R0 (6378137.0)  ///< Earth's mean radius, it is level when h = 0, m 

// -----------------------------------------------------------------------------
///  @brief   Specific data object to Sun&Moon pertrubation calculation
// -----------------------------------------------------------------------------
typedef struct __tAtmosphere
{
  /// special data
  float64 mjdMean_t;  ///< Julian date (in J2000) of point of atmospheric drag fixing
  float64 mjdTau;     ///< Duration of fixing of atmospheric drag
  float64 Mean_AF;    ///< Current values (approaches) of Drag Factor = Q/(V^2)

} tAtmosphere;

//===================================================================================
///  @brief		Initialization Atmosphere object
///
///  @param[in]	pAtm - pointer to Atmosphere object
//===================================================================================
EXTERN_C void Atmosphere_Init(tAtmosphere* pAtm);

//===================================================================================
///  @brief  
/// 
///  @param[in] pAtm  - pointer to Atmosphere object
///  @param[in] r     - pointer to radius vector of point to drag factor calculation
///  @param[in] mjd_u - current time from J2000.0 (in days)  
///                     mjd_u = JT(UTC) - JT(UTC(J2000.))
///  @param[in] sigma - ballistic coefficient
///  @return    drag factor = Q/(V^2)
//===================================================================================
EXTERN_C float64 Atmosphere_Drag_Factor(tAtmosphere *pAtm,
                                        float64     *r,
                                        float64      mjd_u,
                                        float64      sigma);


#endif
