/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Contains OPManager object definition
 *   @file                   OPManager.h
 *   @author                 Dmitry Churikov
 *   @date                   25.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#ifndef __OPMANAGER_H
#define __OPMANAGER_H

#include "integration.h"
#include "DynamicsModel.h"
#include "TimeEx.h"
#include "Matrix2.h"
#include "ControlOP.h"
#include "../sources/SatelliteManager.h"
#include "../sources/Framework.h"

/// OP dynamic model types
#define OP_DM_ORBITAL_MOTION     (0)   ///<  Orbital motion model (GP(18x18), Sun&Moon, Athmosphere; 500km <= h <= 1200km)
#define OP_DM_STATIC             (1)   ///<  Static model ( 0km <= h <= 20km)

/// OP condition flags
#define OP_COND_NO_SOLUTION      (0)   ///<  estimation and propagation are not imposible
#define OP_COND_PROPAGATION_PROC (1)   ///<  propagation is processed (is not compleate)
#define OP_COND_PROPAGATION      (2)   ///<  propagation is realized (is compleate)
#define OP_COND_ESTIMATION       (3)   ///<  estimation is realized

/// OP validation flags
#define OP_FG_NOT_VALID          (0)
#define OP_FG_VALID              (1)


/// OP propagation/estimation settings
#define OP_PRP_MAX_STEP          (30.)     ///< maximal integration time step for propagation, sec
#define OP_PRP_TIME_RESOLUTION   (1e-6)    ///< minimal integration time step for propagation, sec 
#define OP_PRP_TIME_LIMIT        (3600.)   ///< maximal propagation time interval, sec
#define OP_PRP_TIME_EKF_RESET    (900.)    ///< maximal filtering time interval, sec        
#define OP_PRP_ITERATION_LIMIT   (20)      ///< max RK4 iteration on one function call
#define OP_EST_NSV_MIN           (2)       ///< minimal Navigation SV count for estimation calculation

/// OP screening parameters
#define OP_RAW_FILTER_RANGE      (50000.)  ///< maximal differencó between pseudo- & propagated range, m
#define OP_RAW_FILTER_VRAD       (10.)     ///< maximal differencó between pseudo- & propagated range rate, m/sec
//#define MAX_AVER_DR              (60.)   ///< [m]
//#define MAX_AVER_DV              (0.3)   ///< [m/s]
//#define MAX_AVER_DR              (15.)     ///< [m]
//#define MAX_AVER_DV              (0.25)    ///< [m/s]
//#define MAX_DIFF_DR              (2500.)   ///< [m]
//#define MAX_DIFF_DV              (1.5)     ///< [m/s]


// Tom: for testing only
#define MAX_AVER_DR              (240.)     ///< [m]
#define MAX_AVER_DV              (3.0)    ///< [m/s]
#define MAX_DIFF_DR              (2500.0)   ///< [m]
#define MAX_DIFF_DV              (6.0)     ///< [m/s]

/// OP structure settings
#define EST_VAR_COUNT 8



// -----------------------------------------------------------------------------
// Default OP EFK parameters

//  Q matrix parameters
#define OP_DEF_STATE_DISP_VL      (2e-7)*0.5 //2e-7 // m*m/(s*s)
#define OP_DEF_STATE_DISP_VR      (2e-7)*0.5 // m*m/(s*s)
#define OP_DEF_STATE_DISP_VBN     (2e-7)*0.5 // m*m/(s*s)
#define OP_DEF_STATE_DISP_L       (6e-2)*0.5 // m*m
#define OP_DEF_STATE_DISP_R       (6e-2)*0.5 // m*m
#define OP_DEF_STATE_DISP_BN      (6e-2)*0.5 // m*m
#define OP_DEF_STATE_DISP_BIAS    (5e-1)*0.5 // m*m
#define OP_DEF_STATE_DISP_DRIFT   (5e-1)*0.5 // m*m/(s*s)

//  Covariance (P0) matrix parameters for start
#define OP_DEF_COV_DISP_P         400       // m*m
#define OP_DEF_COV_DISP_V         0.04      // m*m/(s*s)
#define OP_DEF_COV_DISP_B         1.e6      // m*m
#define OP_DEF_COV_DISP_D         1.e3      // m*m

//  Covariance (P0) matrix parameters after large GPS outages
#define OP_LARGE_COV_DISP_P       25e2      // m*m
#define OP_LARGE_COV_DISP_V       0.25      // m*m/(s*s)
#define OP_LARGE_COV_DISP_B       1.e6      // m*m
#define OP_LARGE_COV_DISP_D       1.e3      // m*m

//  R matrix parameters
// nominal models
//#define OP_DEF_PR_NOISE         9*(2.*2. + 1.*1. + 1.1*1.1)   // m*m
//#define OP_DEF_DOPLER_NOISE     9*(0.05*0.05 + 0.04*0.04)   // m*m/(s*s)
// simulation mode
#define OP_DEF_PR_NOISE         9*(2.*2.)           // m*m
#define OP_DEF_DOPLER_NOISE     9*(0.04*0.04)       // m*m/(s*s)


typedef struct __tSolutionStatus
{
  /// Start synchronization 
  int16 SolutionCount;      ///< Count of solution for validity analyzes
  float64 dR;               ///< Magnitude of different of OP and Navigation positions
  float64 dV;               ///< Magnitude of different of OP and Navigation velocity vectors
}
tSolutionStatus;

// -----------------------------------------------------------------------------
///  @brief:  tOPManager object
// -----------------------------------------------------------------------------
typedef struct __tOPManager
{
  /// Dynamics model parameters
  tDynamicsModel     DM;                   ///< Dynamic model object
  tRightPart         pfDM;                 ///< Pointer of the function to right part calculation
  
  /// OP settings - EFK parameters
  float64            R_pr;                 ///< Pseudorange measurement noise, m 
  float64            R_prr;                ///< Pseudorange rate (doppler) measurement noise, m/sec
  float64            Qorb[EST_VAR_COUNT];  ///< Main diagonal of Q matrix (vector of dispersions)
                                           
  /// OP processing
  float64            X[EST_VAR_COUNT];     ///< Last calculated state vector
  MatrixEx           P0;                   ///< Last calculated covariance matrix of state vector

  USERop             User_OP;              ///< OP user parameter
  tCorrectTimeOP     CorrectTimeOP;        ///< OP correct time
  
  tGPSTime           btLastUpdate;         ///< On-board time of last update (estimation or propagation) of state vector
  tGPSTime           btLastEstimation;     ///< On-board time of last estimation of state vector

  int8               mOP_DynModelType;     ///< type of Dynamic model for prediction/propogation
  int8               fOP_Condition;        ///< last OP solution condition // no sol; prop; est
  int8               fOP_Validity;         ///< last OP solution validation // valid; not valid
  int8               fSynchronization;     ///< 

  tSolutionStatus    solutionStatus;
  
} tOPManager;

// ------------------------------------------------------------------------
///  @brief   Specific OP functions

//===================================================================================
///  @brief     Initialization Orbit Propagation Manager object
///
///  @param[in] pOPMgr        - pointer to instance
///  @param[in] mDynModelType - Dynamic model type
//===================================================================================
EXTERN_C void OPMgr_Init(tOPManager        *pOPMgr, int8 mDynModelType);

//===========================================================================
///  @brief     Calculates current OP solution
///
///  @param[in] pOPMgr      - pointer to OP Manager object
///  @param[in] pTopSystem  - pointer to top system parameters
///  @param[in] NavCond     - pointer to navigation conditions
///  @param[in] User        - user parameters
///  @param[in] NavSolution - navigation solution is good
///  @param[in] weekData    - week data structure
//===========================================================================
EXTERN_C void OPMgr_Process(tOPManager     *pOPMgr, 
                            tTopSystem     *pTopSystem, 
                            NAV_CONDITIONS *NavCond,
                            USER            User,
                            uint8           NavSolution,
                            tWeekData       weekData,
                            eFlag           BinStrFlag,
                            DATA_STRING    *dataStr,
                            tTerminal      *DebugTerminal);

// -----------------------------------------------------------------------------
///  @brief		  The function analyzes  validity of OP and navigation solutions 
///             by integral difference of them
///  @param[in]	const USER *pUser - pointer of USER object
///  @param[in]	const tOPManager *pOPMgr - pointer of OPManager object
///  @param[in]	tSolutionsStatus *pSolVal - pointer of tSolutionsStatus object
///  @param[in]	eFlag fProcessing - flag of analysis state 
///                                (fg_ON - permit analysis)
///                                (fg_OFF - stop analysis and reset all integral criterion)
///  @return     -1 - if analysis isn't complete
///               0 - if analysis is done and both solutions aren't valid 
///               1 - if analysis is done and both solutions are valid 
// -----------------------------------------------------------------------------
EXTERN_C int8 FirstSolutionValidity(const USER *pUser,
                                    const tOPManager *pOPMgr,
                                    tSolutionStatus *pSolVal,
                                    eFlag fProcessing,
                                    eFlag              BinStrFlag,
                                    DATA_STRING       *dataStr,
                                    tTerminal         *DebugTerminal);

// -----------------------------------------------------------------------------
///  @brief		  The function analyzes  validity navigation solutions 
///             by current difference with OP solution
///  @param[in]	const USER *pUser - pointer of USER object
///  @param[in]	const tOPManager *pOPMgr - pointer of OPManager object
///  @return      0 - if analysis is done and both solutions aren't valid 
///               1 - if analysis is done and both solutions are valid 
// -----------------------------------------------------------------------------
EXTERN_C int8 SolutionValidity( const USER *pUser,
                                const tOPManager *pOPMgr,
                                eFlag              BinStrFlag,
                                DATA_STRING       *dataStr,
                                tTerminal         *DebugTerminal);

#endif
