/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Dynamics Model object functions
 *   @file                   DynamicsModel.c
 *   @author                 Dmitry Churikov
 *   @date                   08.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#include "DynamicsModel.h"

#include <string.h>

//===================================================================================
///  @brief   Initialization Dynamics Model object
///
///  @param[in] pDM - pointer to Dynamics Model object
//===================================================================================
void DynamicsModel_Init(tDynamicsModel* pDM)
{
  // zero object
  memset(pDM, 0, sizeof (tDynamicsModel));

  // subobjects initialization
  Atmosphere_Init(&pDM->Atm);
  Geopotential_Init(&pDM->GP);
  SunMoonData_Init(&pDM->SMD);

  return;
} 
// DynamicsModel_Init
