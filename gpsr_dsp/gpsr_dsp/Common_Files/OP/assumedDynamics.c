/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Assumed dynamics model equation
 *   @file                   assumedDynamics.c
 *   @author                 Dmitry Churikov
 *   @date                   08.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#include "DynamicsModel.h"
#include "TimeEx.h"
#include "ControlOP.h"

#include <math.h>


//===================================================================================
///  @brief   Assumed dynamics model parameters initialization
///
///  @param[in] pDM - pointer to Dynamics Model object
//===================================================================================
void assumedDynamics_Init(tDynamicsModel* pDM)
{
  DynamicsModel_Init(pDM);
    
  /// assumed model settings 

  // geopotential
  pDM->GP_N = 15; // 15x15
  //pDM->GP_N = 4;    // 4x4
  //pDM->GP_N  = 0;   // 0 or 1 - central terrestrial gravitational field
  
  // Sun&Moon
  //pDM->SM_fUsing = SMG_USE_ALL; // Sun&Moon
  //pDM->SM_fUsing = SMG_USE_SUN; // Sun
  //pDM->SM_fUsing = SMG_USE_MOON; //Moon
  pDM->SM_fUsing = SMG_USE_NOTHING;  // nothing
    
  pDM->SMD.mjdTau = 10*T_DAYS_PER_SECOND;
  

  //pDM->OBJ_Sigma = 0.01;  // ballistic factor of object
  pDM->OBJ_Sigma = 0.0;     // atmosphere model not used
  
  /// assumed model special settings 
  pDM->SMD.dATex = DT_UTC_GPS_2005 - DT_UTC_GPS_2000;  ///< on December 2006 (for model witch is used from 01.01.2005)
  
  return;
} 
// assumedDynamics_Init

//==========================================================================================
///  @brief   The assumed dynamics model equation
///
///  @param[in] Q         - pointer to current satellite vector {Vx,Vy,Vz,x,y,z, time_drift}
///  @param[in] dQ        - pointer to state vector derivatives (it is calculated)
///  @param[in] ptrParams - pointer of tDynamicsModel object
//==========================================================================================
void assumedDynamics(float64 *Q, float64 *dQ, int32 ptrParams)
{
  tDynamicsModel* DM; 
  float64 g[3], g_mb[3], xa[3];
  float64 V, Drag_Factor;

  DM = (tDynamicsModel*)ptrParams;

  /// Atmosphere
  V = sqrt(Q[I_VX]*Q[I_VX] + Q[I_VY]*Q[I_VY] + Q[I_VZ]*Q[I_VZ]);
  Drag_Factor = Atmosphere_Drag_Factor( &DM->Atm, &Q[I_X], Q[I_T], DM->OBJ_Sigma);
  Drag_Factor *= V;
  xa[0] = Drag_Factor * Q[I_VX];
  xa[1] = Drag_Factor * Q[I_VY];
  xa[2] = Drag_Factor * Q[I_VZ];

  /// Earth gravitation
  Geopotential_Gravitation( &DM->GP, &Q[I_X], DM->GP_N, g);

  /// Multi body gravitation
  SunMoonData_Gravitation(&DM->SMD, &Q[I_X], Q[I_T], DM->SM_fUsing, g_mb);

  /// Right part of dynamics      
  dQ[I_X]  = Q[I_VX];
  dQ[I_Y]  = Q[I_VY];
  dQ[I_Z]  = Q[I_VZ];
  dQ[I_VX] = g[0] - xa[0] + g_mb[0] + EARTH_OMEGA*EARTH_OMEGA*Q[I_X] + 2.*EARTH_OMEGA * Q[I_VY];
  dQ[I_VY] = g[1] - xa[1] + g_mb[1] + EARTH_OMEGA*EARTH_OMEGA*Q[I_Y] - 2.*EARTH_OMEGA * Q[I_VX];
  dQ[I_VZ] = g[2] - xa[2] + g_mb[2];
  dQ[I_T]  = T_DAYS_PER_SECOND;

  return; 
} 
// assumedDynamics
