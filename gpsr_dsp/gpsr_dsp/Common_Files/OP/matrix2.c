/*****************************************************************************
 *    Copyright (c) 2007 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  for OP task
 *   @file                   matrix2.�
 *   @author                 Dmitry Churikov
 *   @date                   10.01.2007
 *   @version                1.1
 */
/*****************************************************************************/
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "matrix2.h"
#include "../sources/comtypes.h"

//===================================================================================
///  BLOCK FOR IMPLEMENTATION OF MATRIX FUNCTIONS 
//===================================================================================

//===================================================================================
///  @brief     Matrix initialization
///
///  @param[in] A - pointer to instance
///  @param[in] N - row
///  @param[in] M - column
//===================================================================================
void CreateMatrixEx(MatrixEx *A, int32 N, int32 M)
{ 

  A->mRow = N;
  A->mCol = M;
  memset(A->Matr, 0, sizeof(A->Matr));
  return;
}

//===================================================================================
///  @brief     Free matrix dynamic allocation memory
///
///  @param[in] A - pointer to instance
//===================================================================================
void DestroyMatrixEx(MatrixEx* A)
{ 
  A->mRow = 0;
  A->mCol = 0;
  
  return;
}

//===================================================================================
///  @brief     Matrix copy A = B
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
//===================================================================================
void CopyMatrixEx(MatrixEx *A, MatrixEx *B)
{ 
  int32 i, j, N = B->mRow, M = B->mCol;
  
  if(A->mRow != N || A->mCol != M) 
    CreateMatrixEx(A,N,M);
  
  if(A != B)
    for(i = 0; i < N; i++)
      for(j = 0; j < M; j++)
        A->Matr[i][j] = B->Matr[i][j];
  
  return;
}

//===================================================================================
///  @brief     Multiplication of matrices A = B*C
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
///  @param[in] C - pointer to instance
//===================================================================================
void MultMatrixEx(MatrixEx *A, MatrixEx *B, MatrixEx *C)
{ 
  float64 K;
  int32 i, j, l;
  MatrixEx D, *Ptr;
  
  D.mCol = 0;
  D.mRow = 0;
  
  if(B->mCol != C->mRow)
  {
    DestroyMatrixEx(A);
    return;
  }
  
  Ptr = ((A == B) || (A == C)) ? &D : A;
  CreateMatrixEx(Ptr,B->mRow,C->mCol);
  for(i = 0; i < B->mRow; i++)
    for(j = 0; j < C->mCol; j++)
    { 
      for(l = 0, K = 0; l < B->mCol; l++)
        K += B->Matr[i][l] * C->Matr[l][j];
      Ptr->Matr[i][j] = K;
    }
  if((A == B) || (A == C)) 
  { 
    CopyMatrixEx(A,&D);
    DestroyMatrixEx(&D); 
  }
  
  return;
}


//===================================================================================
///  @brief     Multiplication of matrices A = B*T(C) = B*C'
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
///  @param[in] C - pointer to instance
//===================================================================================
void MultMatrix_B_TC_Ex(MatrixEx *A, MatrixEx *B, MatrixEx *C)
{ 
  float64 K;
  int32 i, j, l;
  MatrixEx D, *Ptr;
  
  D.mCol = 0;
  D.mRow = 0;
  
  if(B->mCol != C->mCol)
  {
    DestroyMatrixEx(A);
    return;
  }

  Ptr = ((A == B) || (A == C)) ? &D : A;
  CreateMatrixEx(Ptr,B->mRow,C->mRow);

  for(i = 0; i < B->mRow; i++)
    for(j = 0; j < C->mRow; j++)
    { 
      for(l = 0, K = 0; l < B->mCol; l++)
        K += B->Matr[i][l] * C->Matr[j][l];
      
      Ptr->Matr[i][j] = K;
    }
  
  if(A != Ptr)
  { 
    CopyMatrixEx(A,&D);
    DestroyMatrixEx(&D); 
  }
  
  return;
}


//===================================================================================
///  @brief     Multiplication of matrices A = B*C, and A is a symmetrical
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
///  @param[in] C - pointer to instance
//===================================================================================
void MultMatrix_SimResEx(MatrixEx *A, MatrixEx *B, MatrixEx *C)
{ 
  float64 K;
  int32 i, j, l;
  MatrixEx D, *Ptr;
  
  D.mCol = 0;
  D.mRow = 0;
  
  if(B->mCol != C->mRow)
  {
    DestroyMatrixEx(A);
    return;
  }
  
  Ptr = ((A == B) || (A == C)) ? &D : A;
  CreateMatrixEx(Ptr,B->mRow,C->mCol);
  for(i = 0; i < B->mRow; i++)
    for(j = 0; j <= i; j++)
    { 
      for(l = 0, K = 0; l < B->mCol; l++)
        K += B->Matr[i][l] * C->Matr[l][j];
      Ptr->Matr[i][j] = Ptr->Matr[j][i] = K;

    }
  if(A != Ptr) 
  { 
    CopyMatrixEx(A,&D);
    DestroyMatrixEx(&D); 
  }
  
  return;
}

//===================================================================================
///  @brief     Inverse  symmetrical matrix
///
///  @param[in] A - pointer to instance of inversion matrix
///  @param[in] B - pointer to instance of matrix to inversion
//===================================================================================
void InvSymmetricalMatrixEx(MatrixEx *A, MatrixEx *B)
{
  int32 i, j, l;
  MatrixEx C, Q, R, *Ptr; 

  if ((!B->mCol) || (B->mCol != B->mRow))
  {
    DestroyMatrixEx(A);
    return;
  }
  
  Ptr = (A == B) ? &C : A;
  CreateMatrixEx(Ptr, B->mRow, B->mCol);
  

  /// QR decomposition
  QR_DecompositionEx( B, &Q, &R);
  
  /// R matrix inversion (L is up triangular matrix)
  InvUpTriangularMatrixEx( &R, &R);  /// R = Inv(R)
  if (!R.mCol)  /// singularity checking
  {
    DestroyMatrixEx(A);
    return;
  }
  
  /// Inversion of B calculation (Inv(B) = Inv(R)*Q')
  /// Inv(B) - is symmetrical
  for (i = 0; i < R.mRow; i++)
    for (j = i; j < R.mCol; j++ )
    {
      Ptr->Matr[i][j] = 0;
      for (l = i; l < R.mCol; l++)
        Ptr->Matr[i][j] += R.Matr[i][l] * Q.Matr[j][l];
      
      Ptr->Matr[j][i] = Ptr->Matr[i][j];
    }

  if (Ptr != A)
  { 
    CopyMatrixEx(A,&C);
    DestroyMatrixEx(&C); 
  }

  return;
}


//===================================================================================
///  @brief     Inverse upper triangular squarte matrix 
///
///  @param[in] A - pointer to instance
///  @param[in] B - pointer to instance
//===================================================================================
void InvUpTriangularMatrixEx( MatrixEx *A, MatrixEx *B)
{
  int32 i, j, l;
  MatrixEx C, *Ptr; 

  if ((!B->mCol) || (B->mCol != B->mRow))
  {
    DestroyMatrixEx(A);
    return;
    }
  
  // check of matrix singularity 
  for (i = 0; i < B->mCol; i++)
    if (B->Matr[i][i] == 0)
    {
      DestroyMatrixEx(A);
      return;
    }
  
  Ptr = (A == B) ? &C : A;
  CreateMatrixEx(Ptr, B->mRow, B->mCol);
  
  Ptr->Matr[0][0] = 1. / B->Matr[0][0];

  for (j = 1; j < B->mCol; j++)
  {
    Ptr->Matr[j][j] =  1. / B->Matr[j][j];
    for(i = 0; i < j; i++)
    {
      Ptr->Matr[i][j] = 0;
      for ( l = i; l < j; l++)
        Ptr->Matr[i][j] += Ptr->Matr[i][l] * B->Matr[l][j];

      Ptr->Matr[i][j] *= - Ptr->Matr[j][j];
    }
  }

  if (Ptr != A)
  { 
    CopyMatrixEx(A,&C);
    DestroyMatrixEx(&C); 
  }

  return;
}

//===================================================================================
///  @brief     QR-decomposition of the matrix A
///             Resultant matrices are contained in R and Q correspondingly
///
///  @param[in] A - pointer to instance
///  @param[in] Q - pointer to instance
///  @param[in] R - pointer to instance
//===================================================================================
void QR_DecompositionEx(MatrixEx *A, MatrixEx *Q, MatrixEx *R)
{ 
  int32 i, j, k;
  int32 N = A->mRow, M = A->mCol;
  float64 temp;

  CopyMatrixEx(Q,A); 
  CreateMatrixEx(R,M,M);

  for (k = 0; k < M; k++)
  { 
    for (i = 0, temp = 0.; i < N; i++) 
    {
      temp += Q->Matr[i][k] * Q->Matr[i][k];
    }

    R->Matr[k][k] = sqrt(temp);
    temp = 1. / R->Matr[k][k];

    for (i = 0; i < N; i++) 
    {
      Q->Matr[i][k] *= temp;
    }

    for (j = k + 1; j < M; j++)
    { 
      for (i = 0, temp = 0.; i < N; i++) 
      {
        temp += Q->Matr[i][k] * Q->Matr[i][j];
      }
      
      for (i = 0; i < N; i++) 
      {
        Q->Matr[i][j] -= temp * Q->Matr[i][k];
      }
      
      R->Matr[k][j] = temp;
    }
  }
}



