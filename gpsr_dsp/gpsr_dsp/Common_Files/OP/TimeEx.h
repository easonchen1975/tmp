/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Defines types, constants and functions to time 
 *                           processing and conversion
 *   @file                   TimeEx.h
 *   @author                 Dmitry Churikov
 *   @date                   18.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#ifndef __TIME_EX_H
#define __TIME_EX_H

#include "../sources/comtypes.h"

///  Reference constants
#define JD_2000_0       (2451545.0)  ///< J2000.0 in JD, days
#define JD_UT_OF_GPS_0  (2444244.5)  ///< GPS time origin (1980.01.06 00:00:00) in JD, days
#define DT_UTC_GPS_2000 (13)         ///< (GPS time - UTC) on  J2000.0, leap seconds
#define DT_UTC_GPS_2005 (14)         ///< (GPS time - UTC) on  UTC2005.0, leap seconds

/// Time units ratio
#define T_CENTURIES_PER_DAY  ( 1./36525.)
#define T_DAYS_PER_CENTURIE  (36525.)
#define T_DAYS_PER_SECOND    (1./86400.)
#define T_SECONDS_PER_WEEK   (86400.*7.)
#define T_SECONDS_PER_DAY    (86400.)
#define T_DAYS_PER_WEEK      (7.)

/// GPS time limites
#define T_GPS_HALF_WEEK      (86400.*3.5)
#define T_GPS_HALF_ROLLOVER  (86400.*3.5*1024.)

/// Time scales ratio
#define GSMT_TO_UT  (0.997269566329084)  ///< GSMT to UT scale factor (seconds to seconds)

// ------------------------------------------------------------------------
///  @brief   GPS system time format
// ------------------------------------------------------------------------
typedef struct __tGPSTime
{
  int16   wn;  ///< week number
  float64 t;   ///< time of week, sec
} 
tGPSTime;


//===================================================================================
///  @brief   The function calculates MJT from J2000.0 by UTC
///
///  @param[in]  utc - pointer to tGPSTime structure of UTC
///  @param[out] mjd - pointer to calculated MJD from J2000.0 as days.part_of_day
//===================================================================================
EXTERN_C void MJD_From_UTCEx(tGPSTime * utc, float64 * mjd);


#endif
