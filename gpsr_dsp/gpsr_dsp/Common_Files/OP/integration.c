/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Digital algorithms to Cauchy problem solution
 *   @file                   integration.c
 *   @author                 Dmitry Churikov
 *   @date                   11.12.2006
 *   @version                1.0
 */
/*****************************************************************************/
#include "integration.h"

#include <string.h>
#include "../Sources/baseop.h"


//===========================================================================================
///  @brief   
///
///  @param[in] fp     - TRightPart-function pointer
///  @param[in] Q      - pointer to start vector to Cauchy problem solution
///  @param[in] cnt    - count of Q-vector components, it must be no more than 
///                      CP_AVAL_VECTOR_SIZE. 
///                      This condition is not checked into the function body.
///  @param[in] h      - integration step
///  @param[in] pParam - addition parameter of RightPart-function
///  @param[in] dQ     - pointer to Q vector increase
//===========================================================================================
void rk4(tRightPart fp, float64 *Q, int32 cnt, float64 h, int32 pParam, float64 *dQ)
{
  int32 i, j;
  float64 Y1[CP_AVAL_VECTOR_SIZE], F[CP_AVAL_VECTOR_SIZE];
  float64 k;
  
  _memcpy( Y1, Q, sizeof(float64)*cnt);
  memset ( dQ, 0, sizeof(float64)*cnt);
  
  for (i = 1; i <= 4; i++)
  {
    (*fp)( Y1, F, pParam);

    for (j = 0; j < cnt; j++)
    {
      k = h * F[j];
      if (i == 2)
        dQ[j] += k / 3.;
      else 
        dQ[j] += k / 6.;
      
      if(i == 3)
      {
        dQ[j] += k / 6.;
        Y1[j] = Q[j] + k;
      }
      else 
        Y1[j] = Q[j] + 0.5*k;
    }
  }
}
