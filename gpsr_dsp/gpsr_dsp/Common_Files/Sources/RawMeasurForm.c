/**************************************************************************************
*    Copyright (c) 2005 Spirit Corp.
***************************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  raw measurements forming
*    @file                   RawMeasurForm.c
*    @author                 M. Zhokhova
*    @date                   25.06.2005
*    @version                1.0
*/
/*************************************************************************************/

#include "satellite.h"
#include "RawMeasurForm.h"
#include "Control.h"
#include "LongFract.h"

#define CONVERT_MS_S_Q40  (0x4189374BL)      ///< 1e-3 in Q40
#define CONV_PLL2DLL_Q61  (0x573D540CL)      ///< CONV_PLL2DLL*2M_PI in Q61
#define CONV_PLL2DLLS_Q71 (0x59555404L)      ///< CONV_PLL2DLL*2M_PI*1e-3 in Q71
#define CONV_PHASE2DELAY_Q54  (0x6C793731L)  ///< 1000/(2pi*f0_GPS) in Q54

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SatGPS_RawMeasurementsFormingFix, "fast_text_3")
	#endif
#endif

//=====================================================================================
///  Forming raw measurements
///
///  @param[in]  dopplerQ15         - Doppler frequency in Q15
///  @param[in]  dopplerPrimeQ10    - Doppler rate frequency in Q10
///  @param[in]  delay              - delay
///  @param[in]  tauQ               - quantized delay
///  @param[in]  phase_err_avrQ14   - filtered phase error in Q14 [rad.]
///  @param[in]  epochInBit         - number of ms between bits (0�19)
///  @param[in]  time               - time system in millisecond
///  @param[in]  epoch_control      - epoch control: reiteration or skip
///  @param[in]  R                  - clock shift
///  @param[in]  decoder            - pointer to satellite decoder structure
///  @param[in]  pPreamble          - pointer to structure with saved preamble time
///  @param[in]  Nframe_add         - number of additional frames in reacquisition mode
///  @param[in]  CorrTimeByPreamble - time corrected by preamble
///  @param[out] raw_data           - pointer to raw data structure
//=====================================================================================
void SatGPS_RawMeasurementsFormingFix(int32         dopplerQ15,
                                      int32         dopplerPrimeQ10,
                                      tLongFract48  delay,
                                      tLongFract48  tauQ,
                                      int16         phase_err_avrQ14,
                                      int           epochInBit,
                                      const uint32  time,
                                      int8          epoch_control,
                                      tLongFract    R,
                                      tDecoder     *decoder,
                                      tPreamble    *pPreamble,
                                      int32         Nframe_add,
                                      eFlag         CorrTimeByPreamble,
                                      tRawDataFix  *raw_data)
{
  raw_data->attempt = 0;

  /// start forming raw data if receiver time is corrected
  if (CorrTimeByPreamble == fg_ON) 
  {
    /// check synchronization of decoder
    if (SatGPS_GetDecoderStatus(decoder) == fg_ON)
    {
      int32        mult;         ///< result of multiplication
      tLongFract   timeLF,       ///< time in tLongFract format
                   timeNavTask,  ///< time for navigation task solution
                   extr_del,     ///< temporary extrapolation of delay in tLongFract format
                   ps_delay,     ///< temporary pseudo delay
                   dt_tmp,       ///< temporary time of extrapolation
                   dt,           ///< time of extrapolation
                   dll_state,    ///< DLL state
                   multLF,       ///< result of multiplication in tLongFract format
                   sqLFdt;       ///< square time of extrapolation in tLongFract format
      tLongFract48 tmp_extr,     ///< temporary extrapolation of delay in tLongFract48 format
                   dop,          ///< temporary doppler
                   dopPrime;     ///< temporary doppler prime

      /// set timeNavTask = time + (R*l_cLight*1000.0) in tLongFract format
      LongFractSet(time, 0, &timeLF);
      LongFractAdd(&timeLF, &R, &timeNavTask); 

      /// calculation fractional part delay
      {
        tLongFract48 diff1, diff2, d_delay48; 
        tLongFract   d_delay;
        
        LongFract48Sub(&delay, &tauQ, &diff1);
        tauQ.integral = 0;
        LongFract48Add(&diff1, &tauQ, &diff2);
        /// add filtered phase error 
        d_delay.integral = phase_err_avrQ14;  // in Q14
        d_delay.fract    = 0;
        LongFractShr(&d_delay, 16);  // Q14 -> Q-2
        LongFractMPY(&d_delay, CONV_PHASE2DELAY_Q54, &d_delay);  // Q-2*Q54 -> Q36
        LongFractShr(&d_delay, 20);  // Q36 -> Q16
        LongFract48SetLongFractQ48(&d_delay, &d_delay48);         // Q16 -> Q0
        LongFract48Sub(&diff2, &d_delay48, &diff2);
        /// convert to tLongFract
        LongFractSetLongFract48(&diff2, &dll_state);
      }

      // signal pseudodelay calculation
      {
        tLongFract prTime, Nbits, PrNbits, timeTrt, twoMs;
        // PreambleTime [ms] + number of bits after preamble *20[ms] + number of RPN periods * 1[ms]
        LongFractSet((int32)(decoder->PreambleTime * 1000), 0, &prTime);
        LongFractSet((int32)(decoder->Nframe * 20), 0, &Nbits);

        // timeTrt = PreambleTime * 1000 + Nframe * 20 + epochInBit
        // timeRec = timeNavTask = number ms + fractional part  -1
        LongFractAdd(&prTime, &Nbits, &PrNbits);
        LongFractSet(epochInBit, 0, &Nbits);
        LongFractAdd(&PrNbits, &Nbits, &timeTrt);  // ms

        if (epoch_control == EC_SKIP)
        {
          LongFractSet(1, 0, &twoMs);
        }
        else
          LongFractSet(2, 0, &twoMs);

        LongFractAdd(&twoMs, &R, &dt_tmp);

        LongFractSub(&dt_tmp, &dll_state, &dt);

        LongFractAdd(&timeTrt, &dt, &raw_data->timeCoord);

        LongFractSub(&timeNavTask, &raw_data->timeCoord, &ps_delay);
        
        if (ps_delay.integral > MS_HALF_WEEK)
          ps_delay.integral -= MS_WEEK;
        else if (ps_delay.integral < -MS_HALF_WEEK)
          ps_delay.integral += MS_WEEK;
      }

      mult = L_mpy_ll(dopplerPrimeQ10, CONVERT_MS_S_Q40); // Q10*Q40 -> Q19

      LongFractMPY(&dt, mult, &multLF);  // Q19-Q16 -> Q3

      LongFractShr(&multLF, 3);

      // Pseudofrequency raw measurement forming
      // ps_freq = -(doppler + dopplerPrime*dt*1.0e-3)
      {
        tLongFract dop;
        LongFractSetInvQ15(dopplerQ15, &dop);
        LongFractSub(&dop, &multLF, &raw_data->ps_freq);
      }

      mult = L_mpy_ll(dopplerQ15, CONV_PLL2DLL_Q61); // Q15*Q61 -> Q45

      LongFractMPY(&dt, mult, &multLF); // Q45 - Q16 -> Q29

      LongFractShr(&multLF, 13); // Q(29+32) - Q13  in Q48

      LongFract48SetLongFractQ48(&multLF, &dop);

      LongFractSquare(&dt, &sqLFdt);

      mult = L_mpy_ll(dopplerPrimeQ10, CONV_PLL2DLLS_Q71)>>3;  // Q10*Q71 -> Q50 *0.5 -> Q48

      LongFractMPY(&sqLFdt, mult, &multLF);  // Q48 - Q16 -> Q32

      LongFractShr(&multLF, 16); // Q(32+32) - Q16 -> Q48

      LongFract48SetLongFractQ48(&multLF, &dopPrime);

      LongFract48Add(&dop, &dopPrime, &tmp_extr);

      LongFractSetLongFract48(&tmp_extr, &extr_del);

      // resulting pseudodelay calculation
      LongFractSub(&ps_delay, &extr_del, &raw_data->ps_delay); // msec

      raw_data->attempt = 1;
    }
    else  /// if !(SatGPS_GetDecoderStatus(decoder) == fg_ON) is not synchronization
    {
      if (pPreamble->PreambleTime != -1)
      {
        tLongFract  timeLF, timeNavTask, tmp_phase, ps_delay;  // ms
        tLongFract  dt_tmp, dt, dll_state, AccLF, AccLFdt;     // ms
        int32 Acc;
        tLongFract48 ps_phase, dop, dopPrime;

        LongFractSet(time, 0, &timeLF);
        LongFractAdd(&timeLF, &R, &timeNavTask); 

        // calculation fractional part delay
        {
          tLongFract48 diff1, diff2, d_delay48; 
          tLongFract   d_delay;
        
          LongFract48Sub(&delay, &tauQ, &diff1);
          tauQ.integral = 0;
          LongFract48Add(&diff1, &tauQ, &diff2);
          /// add filtered phase error 
          d_delay.integral = phase_err_avrQ14;  // in Q14
          d_delay.fract    = 0;
          LongFractShr(&d_delay, 16);  // Q14 -> Q-2
          LongFractMPY(&d_delay, CONV_PHASE2DELAY_Q54, &d_delay);  // Q-2*Q54 -> Q36
          LongFractShr(&d_delay, 20);  // Q36 -> Q16
          LongFract48SetLongFractQ48(&d_delay, &d_delay48);         // Q16 -> Q0
          LongFract48Sub(&diff2, &d_delay48, &diff2);
          /// convert to tLongFract
          LongFractSetLongFract48(&diff2, &dll_state);
        }
        // signal pseudodelay calculation
        {
          tLongFract prTime, Nbits, PrNbits, PrNbitsNadd, timeTrt, twoMs;
          // PreambleTime [ms] + number of bits after preamble *20[ms] + number of RPN periods * 1[ms]
          LongFractSet((int32)(pPreamble->PreambleTime * 1000), 0, &prTime);
          LongFractSet((int32)(decoder->Nframe * 20), 0, &Nbits);
          LongFractAdd(&prTime, &Nbits, &PrNbits);
          LongFractSet((int32)(Nframe_add*20), 0, &Nbits);
          LongFractAdd(&PrNbits, &Nbits, &PrNbitsNadd);  // ms
          LongFractSet(epochInBit, 0, &Nbits);
          LongFractAdd(&PrNbitsNadd, &Nbits, &timeTrt);  // ms
 
          if (epoch_control == EC_SKIP)
          {
            LongFractSet(1, 0, &twoMs);
          }
          else
            LongFractSet(2, 0, &twoMs);

          LongFractAdd(&twoMs, &R, &dt_tmp);

          LongFractSub(&dt_tmp, &dll_state, &dt);

          LongFractAdd(&timeTrt, &dt, &raw_data->timeCoord);

          LongFractSub(&timeNavTask, &raw_data->timeCoord, &ps_delay);

          if (ps_delay.integral > MS_HALF_WEEK)
            ps_delay.integral -= MS_WEEK;
          else if (ps_delay.integral < -MS_HALF_WEEK)
            ps_delay.integral += MS_WEEK;
        }

        Acc = L_mpy_ll(dopplerPrimeQ10, CONVERT_MS_S_Q40); // Q10*Q40 -> Q19

        LongFractMPY(&dt, Acc, &AccLF);  // Q19-Q16 -> Q3

        LongFractShr(&AccLF, 3);


        // Pseudofrequency raw measurement forming
        {
          tLongFract dop;
          LongFractSetInvQ15(dopplerQ15, &dop);
          LongFractSub(&dop, &AccLF, &raw_data->ps_freq);
        }

        Acc = L_mpy_ll(dopplerQ15, CONV_PLL2DLL_Q61); // Q15*Q61 -> Q45

        LongFractMPY(&dt, Acc, &AccLF); // Q45 - Q16 -> Q29

        LongFractShr(&AccLF, 13); // Q(29+32) - Q13  in Q48

        LongFract48SetLongFractQ48(&AccLF, &dop);

        LongFractSquare(&dt, &AccLFdt);

        Acc = L_mpy_ll(dopplerPrimeQ10, CONV_PLL2DLLS_Q71)>>3;  // Q10*Q71 -> Q50 *0.5 -> Q48

        LongFractMPY(&AccLFdt, Acc, &AccLF);  // Q48 - Q16 -> Q32

        LongFractShr(&AccLF, 16); // Q(32+32) - Q16 -> Q48

        // Pseudophase raw measurement forming
        // ps_phase = CONV_PLL2DLL*raw_data->ps_freq*(2*M_PI)*dt*1.0e-3;
        LongFract48SetLongFractQ48(&AccLF, &dopPrime);

        LongFract48Add(&dop, &dopPrime, &ps_phase);

        LongFractSetLongFract48(&ps_phase, &tmp_phase);

        // resulting pseudodelay calculation
        // ps_delay = ps_delay*1.0e-3 + ps_phase
        LongFractSub(&ps_delay, &tmp_phase, &raw_data->ps_delay); // msec

        raw_data->attempt = 1;

      }  /// if (pPreamble->PreambleTime != -1)
    }    /// if !(SatGPS_GetDecoderStatus(decoder) == fg_ON)
  }      /// if (CorrTimeByPreamble == fg_ON) 

  return;
}

