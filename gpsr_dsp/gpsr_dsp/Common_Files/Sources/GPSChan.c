/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS 3x12 Receiver
 *    @brief                  GPS search-tracking channel uses correlator
 *                            with decimation working either on 8184 MHz
 *    @file                   GPSChan.c
 *    @author                 A. Nazarov
 *    @date                   18.08.2005
 *    @version                1.0
 */
/*****************************************************************************/

#include "GPSchan.h"
#include "FindBit.h"
#include "expTbl.h"
#include "Div31Fast.h"
#include "Log2.h"
#include "comfuncs.h"
#include "L_sqrt_l.h"
#include "SampleDriver.h"

#include "Framework.h"

/// General definitions affection to the software behaviour
#define CN_LONG_BB      (4864)         ///< SNR value in Q7 for long bit boundary test
#define CN_TIME         (300)          ///< for normal startup
#define STARTUP_TIME    (500)          ///< delay for normal navigation solution
#define STARTUP_LONG    (2340)         ///< time for long bit boundary test and two bits time
#define BIT_BOUND_TIME  (460)          ///< time for bit boundary test
#define BB_LONG_TIME    (2300)         ///< time for long bit boundary test
#define MAX_TRIALS      (9)            ///< maximum number of bits to find a boundary
#define TRIANGLE        (9)            ///< triangle points number

/// GPSCHAN_SAMPLERATE should be defined in GPS.H
#if     GPSCHAN_SAMPLERATE==8184
#define INVGPSCHAN_SAMPLERATE_Q41 (0x10040100)  ///< (1/GPSCHAN_SAMPLERATE) Q41
#define INVGPSCHAN_SAMPLERATE_Q43 (0x40100401)  ///< (1/GPSCHAN_SAMPLERATE) Q43
#define EPOCH_INC  (16384)                      ///< 4092./GPSCHAN_SAMPLERATE, Q15
#define PHASE_INC  (16384)                      ///< (int16)((1023./(GPSCHAN_SAMPLERATE/decimation factor of correlator))*32768.)
#else
#error This code supports sample rate 8184 ksps only
#endif

//================================================================================
/// start DLL
///
/// @param[in]  pGPSchan - pointer to instance
/// @param[in]  pos      - position
//================================================================================
static void DLLstart(tGPSchan *pGPSchan, int pos);

//================================================================================
/// call DLL only
///
/// @param[in]  pGPSchan   - pointer to instance
/// @param[in]  pPrompt    - pointer to prompt correlation
/// @param[in]  pEarly     - pointer to early correlation
/// @param[in]  pLate      - pointer to late correlation
/// @param[in]  coefPllDll - structure with coefficient for PLL/DLL
//================================================================================
static void DLLonly(tGPSchan         *pGPSchan,
                    const tComplex32 *pPrompt,
                    const tComplex32 *pEarly,
                    const tComplex32 *pLate,
                    const tCoefPLLDLL coefPllDll);

//================================================================================
/// Update sampling position
///
/// @param[in]  pGPSchan - pointer to instance
/// @param[in]  delay    - delay [sec]
/// @return     the process flag value:
///             0  - we are still in the current epoch
///             +1 - we are moved left and need to process epoch again, because
///                   it already is present in the delay line
///             -1 - we are moved right and we need to skip the next epoch
//================================================================================
static epfGPSChanProcessFlag updateDelay(tGPSchan *pGPSchan, tLongFract48 delay);

//===========================================================================================
/// Update LO (calculates phase increment)
///
/// @param[in]  pGPSchan         - pointer to instance
/// @param[in]  doppler          - doppler frequency, [Hz]
/// @param[in]  dopplerPrimeQ10  - doppler frequency, [Hz/s]
//===========================================================================================
static void updateReplicaDoppler(tGPSchan *pGPSchan, int32 dopplerQ15, int32 dopplerPrimeQ10);

//================================================================================
/// Position refine
///
/// @param[in]  pResult      - pointer to result structure
/// @param[in]  epochCounter - epoch counter
/// @param[in]  position     - position in buffer
/// @param[in]  array        - array for correlations accumulator
/// @param[in]  pPrompt      - current correlation
//================================================================================
static void refinePos(tEstPosResult    *pResult,
                      int               epochCounter,
                      int               position,
                      int32            *array,
                      const tComplex32 *pPrompt);

//================================================================================
/// Maximum module find
///
/// @param[in]  x - pointer to correlator outputs for ARRAY_SIZE epochs
/// @param[in]  N - noise estimate
/// @return     returns maximum index
//================================================================================
static int findMaxMod(const int32* x, int N);


#ifdef EXT_SRAM
#pragma CODE_SECTION (correlator, "fast_text")
#endif

//=========================================================================================
/// Correlator: calculates prompt and early, late correlations
///
/// @param[in]  pSamples                - pointer to samples at UPSAMPLE_RATIOxchip rate
/// @param[in]  phase                   - actual phase of LO 
/// @param[in]  phaseInc                - phase increment for LO (at chip rate)
/// @param[in]  pPRcode                 - pointer to PN code : +/-1
/// @param[out] pPrompt, pEarly, pLate  - pointer to output correlation results for prompt,
///                                       early, late channels
/// @return     phase of LO: phase += 1023*phaseInc
//=========================================================================================
static uint32 correlator(const int16* restrict pSamples,
                         uint32                phase,
                         int32                 phaseInc,
                         const int8 * restrict pPRcode,
                         tComplex32 * restrict pPrompt,
                         tComplex32 * restrict pEarly,
                         tComplex32 * restrict pLate)
{
  tDoubleWord sg;                      ///< variable for 4 samples
  int32       codePR,                  ///< PN code
              repl,                    ///< replica: cos/sin
              sgE,                     ///< signal for early correlation
              sg0;                     ///< temporary variable for two samples
  uint32      PhaseAcc = phase,        ///< phase accumulator
              phase0;                  ///< return phase
  int32       resReP = 0, resImP = 0,  ///< real and imaginary prompt correlation accumulators
              resReE = 0, resImE = 0,  ///< real and imaginary early correlation accumulators
              resReL = 0, resImL = 0;  ///< real and imaginary late correlation accumulators
  int32       sumRe0, sumIm0,          ///< real and imaginary temporary accumulators
              sumRe1, sumIm1;          ///< real and imaginary temporary accumulators
  int32      *pSmp;                    ///<  pointer to samples
  int         i;

  /// for target
  #ifdef _TMS320C6X
    sg = 0;
  #else  /// for PC model
    sg.hi = 0;
    sg.lo = 0;
  #endif

  /// return phase
  phase0 = phase + (phaseInc*CODE_LENGTH);

  phaseInc = L_mpy_ls(phaseInc,PHASE_INC);

  /// two samples for early
  pSmp = ((int32*)pSamples) - 1;
  sgE = *(int32*)pSmp;
  repl = expTbl[PhaseAcc>>22];
  PhaseAcc+= phaseInc;
  /// sm1.im+sm2.im/sm1.re+sm2.re
  sg0 = _ADD2(_LO(_MPYSU4(sgE, 0x01010101)), _HI(_MPYSU4(sgE, 0x01010101)));
  /// sm.im*sin + sm.re*cos
  sumRe1 = _DOTP2(sg0, repl);
  /// sm.im*cos - sm.re*sin
  sumIm1 = _DOTPN2(sg0, _PACKLH2(repl, repl));

  codePR = (int32)(*pPRcode++);
  codePR = _PACK2(codePR, codePR);

  Get_memd8((void*)pSamples, sg);
  pSamples += 4;

  repl = expTbl[PhaseAcc>>22];
  PhaseAcc+= phaseInc;
  sg0 = _ADD2(_LO(_MPYSU4(_LO(sg), 0x01010101)), _HI(_MPYSU4(_LO(sg), 0x01010101)));
  sumRe0 = _DOTP2(sg0, repl);
  sumIm0 = _DOTPN2(sg0, _PACKLH2(repl, repl));

  /// (sm1+sm2)*code
  resReE += _LO(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
  resImE += _HI(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));

  sg0 = _ADD2(_LO(_MPYSU4(_HI(sg), 0x01010101)), _HI(_MPYSU4(_HI(sg), 0x01010101)));
  repl = expTbl[PhaseAcc>>22];
  PhaseAcc+= phaseInc;
  sumRe1 = _DOTP2(sg0, repl);
  sumIm1 = _DOTPN2(sg0, _PACKLH2(repl, repl));

  resReP += _LO(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
  resImP += _HI(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));

  #ifdef _TMS320C6X
    #pragma UNROLL(2)
  #endif

  for(i=0; i<((GPSCHAN_SAMPLERATE>>3) - 1); i++)
  {
    Get_memd8((void*)pSamples, sg);
    pSamples += 4;
    sg0 = _ADD2(_LO(_MPYSU4(_LO(sg), 0x01010101)), _HI(_MPYSU4(_LO(sg), 0x01010101)));
    repl = expTbl[PhaseAcc>>22];
    PhaseAcc+= phaseInc;
    sumRe0 = _DOTP2(sg0, repl);
    sumIm0 = _DOTPN2(sg0, _PACKLH2(repl, repl));

    resReL += _LO(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
    resImL += _HI(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));

    codePR = (int32)(*pPRcode++);
    codePR = _PACK2(codePR, codePR);

    resReE += _LO(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
    resImE += _HI(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));

    sg0 = _ADD2(_LO(_MPYSU4(_HI(sg), 0x01010101)), _HI(_MPYSU4(_HI(sg), 0x01010101)));
    repl = expTbl[PhaseAcc>>22];
    PhaseAcc+= phaseInc;
    sumRe1 = _DOTP2(sg0, repl);
    sumIm1 = _DOTPN2(sg0, _PACKLH2(repl, repl));

    resReP += _LO(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
    resImP += _HI(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
  }

  Get_memd8((void*)pSamples, sg);
  sg0 = _ADD2(_LO(_MPYSU4(_LO(sg), 0x01010101)), _HI(_MPYSU4(_LO(sg), 0x01010101)));
  repl = expTbl[PhaseAcc>>22];
  PhaseAcc+= phaseInc;
  sumRe0 = _DOTP2(sg0, repl);
  sumIm0 = _DOTPN2(sg0, _PACKLH2(repl, repl));

  resReL += _LO(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));
  resImL += _HI(_MPY2(_ADD2(_PACK2(sumIm0, sumRe0), _PACK2(sumIm1, sumRe1)), codePR));

  pPrompt->re = resReP>>CORR_SHIFT;
  pPrompt->im = resImP>>CORR_SHIFT;
  pEarly->re  = resReE>>CORR_SHIFT;
  pEarly->im  = resImE>>CORR_SHIFT;
  pLate->re   = resReL>>CORR_SHIFT;
  pLate->im   = resImL>>CORR_SHIFT;

  return phase0;
}


#ifdef EXT_SRAM
#pragma CODE_SECTION (getBit, "fast_text")
#endif

//================================================================================
/// get bit and estimate C/N
///
/// @param[in]  x        - pointer to correlator outputs for 20 epochs
/// @param[in]  noiseQ16 - pointer to noise estimate
/// @param[out] CNQ7     - pointer to C/N estimation, Q7
/// @return     returns detected bit
//================================================================================
eGPSChanBit getBit(const tComplex32* restrict x,
                   int32* restrict            noiseQ16,
                   int16* restrict            CNQ7)
{
  int        i;
  int32      std = 0;  ///< noise power estimation
  tComplex32 y;        ///< average complex value
  int32      y2;       ///< signal estimation
  int16      exp;      ///< exponent
  
  y.re = y.im = 0;
  /// calculate scale factor to fit optimum bit width
  {
    int16 expRe,expIm;
    expRe = expIm = 31;
    for (i=0; i<CORR_BIT; i++)
    {
      expRe=Min(expRe,Exp0(x[i].re));
      expIm=Min(expIm,Exp0(x[i].im));
    }
    exp = Min(expRe,expIm);
  }
  exp -=5;
  /// accumulate CORR_BIT epoch
  for (i=0; i<CORR_BIT; i++)
  {
    y.re += L_shl(x[i].re,exp);
    y.im += L_shl(x[i].im,exp);
  }
  /// calculate average complex value
  y.re = L_mpy_ls(y.re, BIT_NQ14);
  y.im = L_mpy_ls(y.im, BIT_NQ14);
  exp +=3;
  /// noise power estimation
  for (i=0; i<CORR_BIT; i++)
  {
    tComplex16 z;
    z.re = extract_h(L_shl(x[i].re,exp) - (y.re>>1));
    z.im = extract_h(L_shl(x[i].im,exp) - (y.im>>1));
    std = L_add(std,L_add(L_mult(z.re,z.re),L_mult(z.im,z.im)));
  }
  std=L_shl(std,2);
  std = L_mpy_ls(std, BIT_NQ19)>>4;
  y2 = L_mult(extract_h(y.re),extract_h(y.re)) + L_mult(extract_h(y.re),extract_h(y.re)); // |y|^2
  exp = Exp(y2);
  /// noise to signal ratio estimation
  if ((y2<<exp)>std>>1)
  {
    std = Div31Fast(std>>1,y2<<exp);        // Q30/Q(31+exp)->Q(30-exp)
    std = L_shl(std,(int16)(16-(30-exp)));  // Q16
  }
  else
  {
    std = 0x7FFFFFFF;
  }
  if (noiseQ16[0]==0) *noiseQ16 = std;
  noiseQ16[0] += L_mpy_ls(std-noiseQ16[0], Q15_0_1);
  /// calculate SNR
  /// CN[0] = 27 + 10*log10(1/noise);
  {
    int32 Acc;
    Acc = Log2(noiseQ16[0]+1);         /// returns Q9 for Q0 input
    Acc = Acc - (16<<9);               /// add 16 in Q19 because noiseQ16 in Q16 format
    Acc = L_mpy_ls (Acc, LOG10_2Q13);  /// Q9*Q13->Q7
    Acc = (CN_MIN<<7) + Acc;           /// 27 dBHz is low limit for C/N ratio
    CNQ7[0]= sature(Acc);
  }

  return y.re>0 ? egpschan_ONE : egpschan_ZERO;
}

//================================================================================
/// Maximum module find
///
/// @param[in]  x - pointer to correlator outputs for ARRAY_SIZE epochs
/// @param[in]  N - noise estimate
/// @return     returns maximum index
//================================================================================
static int findMaxMod(const int32* x, int N)
{
  static const uint8 triangle[TRIANGLE] = {4, 5, 6, 7, 8, 7, 6, 5, 4};  ///< value for triangle
  int32              conv         = 0,  ///< convolution
                     max_conv     = 0;  ///< maximal value of convolution
  int                maxIdx       = 0,  ///< index of convolution maximal
                     k,
                     i;
  int16              exp;               ///< exponent

  exp = 31;
  for (i=0; i<N; i++)
  {
    exp = Min(exp,Exp0(x[i]));
  }

  if (exp != 0)
    exp -= 1;  /// -1 to fit the range after 2 summations
  /// calculate convolution correlations and triangle and search maximal value
  for(i=(TRIANGLE>>1);i<(ARRAY_SIZE - (TRIANGLE>>1));i++)
  {
    for(k=0;k<TRIANGLE;k++)
    {
      conv += extract_h(L_shl(x[k+i-(TRIANGLE>>1)],exp))*triangle[k];
    }
    if (conv > max_conv)
    {
      max_conv = conv;
      maxIdx = i;
    }
    conv = 0;
  }
  return maxIdx;
}

//================================================================================
/// Position refine
///
/// @param[in]  pResult      - pointer to result structure
/// @param[in]  epochCounter - epoch counter
/// @param[in]  position     - position in buffer
/// @param[in]  array        - array for correlations accumulator
/// @param[in]  pPrompt      - current correlation
//================================================================================
static void refinePos(tEstPosResult    *pResult,
                      int               epochCounter,
                      int               position,
                      int32            *array,
                      const tComplex32 *pPrompt)
{
  int idx = epochCounter + (ARRAY_SIZE>>1);  ///< index in input array: start with center of range
  
  position++;
  pResult->isFinished = 0;

  /// control boundary position
  if (position >= GPSCHAN_SAMPLERATE) position -= GPSCHAN_SAMPLERATE;

  /// shift position from maximal boundary to minimal boundary
  if (idx == ARRAY_SIZE)
  {
    position -= ARRAY_SIZE;
    if (position < 0) position += GPSCHAN_SAMPLERATE;
  }
  /// shift index
  if (idx >= ARRAY_SIZE)
    idx -= ARRAY_SIZE;
  /// accumulate module of correlations
  array[idx] = L_sqrt_l(L_add(mpy((int16)pPrompt[0].re,(int16)pPrompt[0].re),mpy((int16)pPrompt[0].im,(int16)pPrompt[0].im)));
  epochCounter++;
  /// process finished
  if (epochCounter == ARRAY_SIZE)
  {
    /// find maximal of convolution
    int index = findMaxMod(array, epochCounter);
    /// shift position to maximal
    position += (index - (ARRAY_SIZE>>1));
    if (position>=GPSCHAN_SAMPLERATE) position-=GPSCHAN_SAMPLERATE;
    if (position<0) position+=GPSCHAN_SAMPLERATE;
    pResult->isFinished = 1;
  }
  pResult->position = position;
}

//===========================================================================================
/// Update LO (calculates phase increment)
///
/// @param[in]  pGPSchan         - pointer to instance
/// @param[in]  doppler          - doppler frequency, [Hz]
/// @param[in]  dopplerPrimeQ10  - doppler frequency, [Hz/s]
//===========================================================================================
static void updateReplicaDoppler(tGPSchan *pGPSchan, int32 dopplerQ15, int32 dopplerPrimeQ10)
{
  pGPSchan->dopplerQ15      = dopplerQ15;
  pGPSchan->dopplerPrimeQ10 = dopplerPrimeQ10;
  pGPSchan->phaseInc        = L_mpy_ll (pGPSchan->dopplerQ15, K_DOPQ50)>>2;           /// Q15*Q50 -> Q34 -> Q32
  pGPSchan->phaseInc       += L_mpy_ll (pGPSchan->dopplerPrimeQ10, K_DOP_PRMQ60)>>7;  /// Q10*Q60 -> Q39 -> Q32
}

//================================================================================
/// Update sampling position
///
/// @param[in]  pGPSchan - pointer to instance
/// @param[in]  delay    - delay from PLLDLL [sec]
/// @return     the process flag value:
///             0  - we are still in the current epoch
///             +1 - we are moved left and need to process epoch again, because
///                   it already is present in the delay line
///             -1 - we are moved right and we need to skip the next epoch
//================================================================================
static epfGPSChanProcessFlag updateDelay(tGPSchan *pGPSchan, tLongFract48 delay)
{
  int        tauQ,                               ///< position in milliseconds
             lastPosition = pGPSchan->position,  ///< last position
             offset;                             ///< position offset
  int32      Acc;                                ///< current position
  uint16     fract_0;                            ///< fractional parts for tLongFract48 type
  uint32     fract_1;                            ///< fractional parts for tLongFract48 type
  epfGPSChanProcessFlag res;                     ///< process flag

  pGPSchan->status.delay = delay;

  Acc = ((uint32)delay.fract_0<<16) | (delay.fract_1>>16);  /// fractional part delay
  Acc = L_mpy_ls(((uint32)Acc>>1), GPSCHAN_SAMPLERATE);     /// position calculate: Q31*Q0 -> Q16
  tauQ = (Acc + Q16_0_5)>>16;                               /// ((int)floor(x+0.5)) in Q0
  Acc  = L_mpy_ls(INVGPSCHAN_SAMPLERATE_Q41, (int16)tauQ);  /// calculate new delay (1/GPSCHAN_SAMPLERATE) Q41*Q0->Q26
  /// new tauQ value saved
  fract_0 = (uint16)(Acc>>10);
  fract_1 = (uint32)(Acc<<22);
  LongFract48Set((int16)((Acc>>26)+delay.integral), fract_0, fract_1, &pGPSchan->status.tauQ);

  _NASSERT(tauQ <= GPSCHAN_SAMPLERATE*2);
  _NASSERT(tauQ >=0);
  
  /// buffer boundary test
  while (tauQ>=GPSCHAN_SAMPLERATE)    tauQ -= GPSCHAN_SAMPLERATE;
  pGPSchan->position = tauQ;
    
  /// update the LO phase if we change sampling position
  offset = (pGPSchan->position - lastPosition);

  if (offset > (GPSCHAN_SAMPLERATE/2))  
    res = epf_PROCESS_EPOCH_AGAIN;
  else if (offset < (-GPSCHAN_SAMPLERATE/2))  
    res = epf_SKIP_EPOCH;
  else
    res = epf_DONOTHING_ELSE;

  return res;
}

//================================================================================
/// call DLL only
///
/// @param[in]  pGPSchan   - pointer to instance
/// @param[in]  pPrompt    - pointer to prompt correlation
/// @param[in]  pEarly     - pointer to early correlation
/// @param[in]  pLate      - pointer to late correlation
/// @param[in]  coefPllDll - structure with coefficient for PLL/DLL
//================================================================================
static void DLLonly(tGPSchan         *pGPSchan,
                    const tComplex32 *pPrompt,
                    const tComplex32 *pEarly,
                    const tComplex32 *pLate,
                    const tCoefPLLDLL coefPllDll)
{
  const tPLLDLLStatus *pStatus;  ///< status of PLL/DLL

  /// one epoch processed and update delay
  #if PLLDLL_FIX
  /// fix-point PLL/DLL
    pStatus = PLLDLLProcess(&pGPSchan->pllDll, pPrompt, pEarly, pLate,
                            pGPSchan->status.tauQ, 0, (coefPllDll.update_dll-1), coefPllDll);
    updateDelay(pGPSchan, pStatus->tau);
  #else  /// floating-point PLL/DLL
  {
    double tauQ;
    tLongFract48 delay;
    tauQ = LongFract48GetDouble(&pGPSchan->status.tauQ);
    pStatus = PLLDLLProcess(&pGPSchan->pllDll, pPrompt, pEarly, pLate, 
                            tauQ/1000., 0, (coefPllDll.update_dll-1), coefPllDll);
    LongFract48SetDouble(pStatus->tau*1000., &delay);
    updateDelay(pGPSchan, delay);
  }
  #endif
}

//================================================================================
/// Start DLL
///
/// @param[in]  pGPSchan - pointer to instance
/// @param[in]  pos      - position
//================================================================================
static void DLLstart(tGPSchan *pGPSchan, int pos)
{
  tComplex32 prompt={1,0}, e={0,0}, l={0,0};
  /// convert position to delay and update delay in PLL/DLL
  {
    int32 Acc;
    tLongFract48 delay;

    _NASSERT(pos<GPSCHAN_SAMPLERATE && pos>=0);

    Acc = L_mpy_ls(INVGPSCHAN_SAMPLERATE_Q43, (int16)(pos<<2))<<1;  /// 1/GPSCHAN_SAMPLERATE in Q43: Q43*Q2->Q30->Q31
    LongFract48SetQ31(Acc, &delay);
    updateDelay(pGPSchan, delay);
  }
  /// initialize PLL/DLL
  #if PLLDLL_FIX
  /// fix-point
    PLLDLLInit(&pGPSchan->pllDll, &prompt, &e, &l, pGPSchan->status.delay, pGPSchan->dopplerQ15);
  #else  /// floating-point
  {
    double delay, doppler;
    delay   = LongFract48GetDouble(&pGPSchan->status.delay);
    doppler = ldexp(pGPSchan->dopplerQ15, -15) * M_2PI;
    PLLDLLInit(&pGPSchan->pllDll, &prompt, &e, &l, delay/1000., doppler);
  }
  #endif
}

//================================================================================
/// Initialize the channel
///
/// @param[in]  pGPSchan   - pointer to instance
/// @param[in]  sv_num     - SV number
//================================================================================
void GPSchanInit(tGPSchan         *pGPSchan,
                 int               sv_num)
{
  tLongFract48 delay;

  /// initialize structure
  pGPSchan->sv_num                            = (uint8)sv_num;
  pGPSchan->StartupTime                       = STARTUP_TIME;
  pGPSchan->status.epochCounter               = 0;
  pGPSchan->status.state                      = es_IDLE;
  pGPSchan->status.bit                        = egpschan_NOBIT;
  pGPSchan->trialCount                        = 0;
  pGPSchan->phase                             = 0;
  pGPSchan->BitBoundaryTime                   = BIT_BOUND_TIME;

  /// initialize Doppler frequency
  updateReplicaDoppler(pGPSchan, 0, 0);

  /// initialize delay
  pGPSchan->status.CN_dBHzQ7 = 0;
  pGPSchan->noiseQ16         = 0;
  LongFract48Set(0,0,0, &delay);
  updateDelay(pGPSchan, delay);

  /// set PLL/DLL coefficients
  pGPSchan->bitBoundControl = fg_OFF;  
  /// calculate C/A code
  getCAcode(pGPSchan->prompt, sv_num);
  
}

//================================================================================
/// Start initialization and tracking from known position and estimated doppler
///
/// @param[in]  pGPSchan - pointer to instance
/// @param[in]  doppler  - rough estimation of doppler frequency, [Hz]
/// @param[in]  position - rough estimation of sampling position inside the epoch
//================================================================================
void GPSchanStart(tGPSchan *pGPSchan, double doppler, double position)
{
  int i;
  tLongFract48 delay;  ///< delay

  /// set delay
  pGPSchan->status.state = es_INIT_DLL;
  LongFract48SetDouble((position/(double)GPSCHAN_SAMPLERATE), &delay);
  updateDelay(pGPSchan, delay);

  /// initialize parameters
  pGPSchan->status.epochCounter = 0;
  pGPSchan->trialCount          = 0;

  /// set Doppler frequency
  updateReplicaDoppler(pGPSchan, (int32)ldexp(doppler,15), 0);

  /// initialize array for initialization PLL/DLL
  for (i=0; i<SIZE(pGPSchan->initArray); i++)
  {
    pGPSchan->initArray[i].re = pGPSchan->initArray[i].im = 0;
  }
  pGPSchan->processFlag = epf_DONOTHING_ELSE;
}

//================================================================================
/// Process epoch
///
/// @param[in]  pGPSchan         - pointer to instance
/// @param[in]  pEpoch           - pointer to epoch (2 continuous milliseconds of
///                                data at UPSAMPLE_RATIOx chip rate)
/// @param[in]  week             - GPS week number
/// @param[in]  ms_count         - current time [ms]
/// @param[in]  preamble         - pointer to preamble structure
/// @param[in]  CN_THRESHOLD     - C/N threshold in dB-Hz
/// @param[in]  coefPllDll       - structure with coefficients for PLL/DLL
/// @param[in]  Nframe           - bits counter
//================================================================================
void GPSchanProcessEpoch(tGPSchan         *pGPSchan,
                         const tEpoch     *pEpoch,
                         const int16       week,
                         const uint32      ms_count,
                         tPreamble        *preamble,
                         int8              CN_THRESHOLD,
                         const tCoefPLLDLL coefPllDll,
                         int16             Nframe)
{
  tComplex32 prompt, e, l;          ///< prompt, early, late correlations
  int        shift;                 ///< one or two buffer: even or odd
  const tComplex8Aligned  *buffer;  ///< pointer to samples

  /// even or odd position
  if ((pGPSchan->position&0x1) == 0) shift = 0;
  if ((pGPSchan->position&0x1) == 1) shift = BUFFER_HALF;

  buffer = pEpoch->pSamples + (pGPSchan->position>>1) + shift;

  /// calculate correlation
  pGPSchan->phase = correlator(&buffer->d,
                               pGPSchan->phase,
                               pGPSchan->phaseInc,
                               pGPSchan->prompt,
                               &prompt,
                               &e,
                               &l);
  
  switch(pGPSchan->status.state)
  {
  /// initialization DLL
  case es_INIT_DLL:
    {
      tEstPosResult res;
      refinePos(&res,
                pGPSchan->status.epochCounter++,
                pGPSchan->position,
                pGPSchan->array,
                &prompt);
      pGPSchan->position = res.position;
      if (res.isFinished)
      {
        pGPSchan->status.epochCounter = 0;
        pGPSchan->status.state        = es_INIT_PLL;
        DLLstart(pGPSchan, pGPSchan->position);
      }
    }
    break;
  /// initialization PLL
  case es_INIT_PLL:
    {
      pGPSchan->initArray[pGPSchan->status.epochCounter++] = prompt;
      
      if (pGPSchan->status.epochCounter == CORR_BIT)
      {
        tFindBitResult result;

        pGPSchan->status.epochCounter = 0;
        DLLonly(pGPSchan, &prompt, &e, &l, coefPllDll);

        /// find bit boundary and refine doppler estimation
        FindBit(&result, pGPSchan->initArray, CORR_BIT);
        if ((result.bitBound > 0) && (result.bitBound < CORR_BIT))
        {
          /// if bit boundary is found, just wait for the next bit and start
          updateReplicaDoppler (pGPSchan, (pGPSchan->dopplerQ15+(result.freq>>1)), 0);  /// since result.freq is in the Q16
          pGPSchan->status.dopplerQ15 = pGPSchan->dopplerQ15;
          
          if (result.bitBound<=0) result.bitBound += CORR_BIT;

          pGPSchan->status.epochCounter = result.bitBound;
          
          pGPSchan->status.state = es_WAIT_BITBOUNDARY;
          /// initialize PLL/DLL
          #if PLLDLL_FIX
          /// fix-point
            PLLDLLInit(&pGPSchan->pllDll, &prompt, &e, &l, pGPSchan->status.tauQ, pGPSchan->status.dopplerQ15);
          #else  /// floating-point
          {
            double delay, doppler;
            
            delay = LongFract48GetDouble(&pGPSchan->status.delay);
            doppler = ldexp(pGPSchan->dopplerQ15, -15) * M_2PI;
            PLLDLLInit(&pGPSchan->pllDll, &prompt, &e, &l, delay/1000., doppler);
          }
          #endif
        }
        /// if we made too much trials, than fall back to the idle
        if (pGPSchan->trialCount++>= MAX_TRIALS)
        {
          pGPSchan->status.state = es_IDLE;
        }
      }
    }
    break;

  /// waiting for beginning of the first bit
  case es_WAIT_BITBOUNDARY:
    { 
      DLLonly(pGPSchan, &prompt, &e, &l, coefPllDll);

      /// at least update the doppler at the startup !
      pGPSchan->status.epochCounter--;
      if (pGPSchan->status.epochCounter <= 0)
      {
        pGPSchan->status.dopplerQ15 = pGPSchan->dopplerQ15;
        /// initialize PLL/DLL
        #if PLLDLL_FIX
        /// fix-point
          PLLDLLInit(&pGPSchan->pllDll, &prompt, &e, &l,  
                   pGPSchan->status.delay, pGPSchan->status.dopplerQ15);
        #else  /// floating-point
        {
          double delay, doppler;

          delay = LongFract48GetDouble(&pGPSchan->status.delay);
          doppler = ldexp(pGPSchan->dopplerQ15, -15) * M_2PI;
          PLLDLLInit(&pGPSchan->pllDll, &prompt, &e, &l, delay/1000., doppler);
        }
        #endif

        pGPSchan->noiseQ16            = 0;
        pGPSchan->status.CN_dBHzQ7    = 0;
        pGPSchan->status.epochCounter = 0 ;
        pGPSchan->epochInBit          = 0;
        pGPSchan->status.state        = es_TRACKING_STARTUP;
      }
    }
    break;
  /// tracking phase
  case es_TRACKING_STARTUP:
  case es_TRACKING:
    {
      const tPLLDLLStatus *pStatus;

      /// exclude skip epoch status
      if (pGPSchan->processFlag != epf_SKIP_EPOCH)
      {
        pGPSchan->phase = 0;

        /// PLL/DLL process
        #if PLLDLL_FIX
        /// fix-point
          pStatus = PLLDLLProcess(&pGPSchan->pllDll, &prompt, &e, &l, pGPSchan->status.tauQ,
                                  pGPSchan->status.epochCounter, 
                                  pGPSchan->epochInBit, coefPllDll);
        #else  /// floating-point
        {
          double tauQ;

          tauQ = LongFract48GetDouble(&pGPSchan->status.tauQ);
          pStatus = PLLDLLProcess(&pGPSchan->pllDll, &prompt, &e, &l, (tauQ/1000.),
                                  pGPSchan->status.epochCounter, pGPSchan->epochInBit,
                                  coefPllDll);
        }
        #endif

        /// update delay and process status by accumulation time
        if ((pGPSchan->epochInBit+1) % coefPllDll.update_dll == 0)
        {
          /// update delay
          #if PLLDLL_FIX
          /// fix-point
            pGPSchan->processFlag = updateDelay(pGPSchan, pStatus->tau);
          #else  /// floating-point
          {
            tLongFract48 delay;

            LongFract48SetDouble(pStatus->tau*1000., &delay);
            pGPSchan->processFlag = updateDelay(pGPSchan, delay);
          }
          #endif

          /// skip epoch or process epoch again
          if (pGPSchan->processFlag == epf_SKIP_EPOCH)
            pGPSchan->status.epoch_control = EC_SKIP;
        }
        else
        {
          /// update frequency and delay
          #if PLLDLL_FIX
          /// fix-point
            pGPSchan->status.delay = pStatus->tau;
          #else  /// floating-point
            LongFract48SetDouble(pStatus->tau*1000., &pGPSchan->status.delay);
          #endif
        }
        /// update Doppler, Doppler prime frequency and phase
        #if PLLDLL_FIX
        /// fix-point
          pGPSchan->status.dopplerQ15       = pStatus->dopplerQ15;
          pGPSchan->status.dopplerPrimeQ10  = pStatus->dopplerPrimeQ10;
          pGPSchan->status.phase_err_avrQ14 = pStatus->phase_err_avr;

          updateReplicaDoppler(pGPSchan, pGPSchan->status.dopplerQ15, pGPSchan->status.dopplerPrimeQ10);
          pGPSchan->phase = pStatus->phaseQ32;
        #else  /// floating-point
          pGPSchan->status.dopplerQ15       = (int32)(ldexp((pStatus->doppler/M_2PI), 15));
          pGPSchan->status.dopplerPrimeQ10  = (int32)(ldexp((pStatus->dopplerPrime/M_2PI), 10));
          pGPSchan->status.phase_err_avrQ14 = (int16)ldexp(pStatus->phase_err_avr, 14);

          updateReplicaDoppler(pGPSchan, pGPSchan->status.dopplerQ15, pGPSchan->status.dopplerPrimeQ10);
          pGPSchan->phase = (uint32)(ldexp((pStatus->phase/M_2PI), 32));
        #endif

        pGPSchan->status.epochCounter++;

        /// accumulate correlations
        pGPSchan->initArray[CORR_BIT + pGPSchan->epochInBit++] = pStatus->y;

        pGPSchan->status.bit = egpschan_NOBIT;

        /// bit detected and calculate SNR
        if (pGPSchan->epochInBit >= CORR_BIT)
        {
          int i;
          pGPSchan->epochInBit = 0;
          /// average pllOut, calculate bit and C/N
          if (pGPSchan->status.state == es_TRACKING_STARTUP)
          {
            /// clear noise estimate during startup
            pGPSchan->noiseQ16 = 0;
          }
          pGPSchan->status.bit = getBit(&pGPSchan->initArray[CORR_BIT], 
                                        &pGPSchan->noiseQ16,
                                        &pGPSchan->status.CN_dBHzQ7);
          /// bit boundary test for GPS: when bit value is change
          if ((pGPSchan->status.bit != pGPSchan->lastBit) && (pGPSchan->lastBit != egpschan_NOBIT)
              && (pGPSchan->bitBoundControl == fg_OFF))
          {
            int shift;
            /// calculate difference between neighboring positions taking into account bit sign for some bit position
            for(i=0;i<BIT_BOUND;i++)
            {
              shift = i - 5;
              if (pGPSchan->lastBit == egpschan_ZERO)
              {
                pGPSchan->accBitBoundCorr[i] += (-pGPSchan->initArray[CORR_BIT-1+shift].re + pGPSchan->initArray[CORR_BIT+shift].re);
              }
              else
              {
                pGPSchan->accBitBoundCorr[i] += (pGPSchan->initArray[CORR_BIT-1+shift].re - pGPSchan->initArray[CORR_BIT+shift].re);
              }
            }
          }
          /// bit boundary test finished
          if ((pGPSchan->status.epochCounter>pGPSchan->BitBoundaryTime) && (pGPSchan->bitBoundControl == fg_OFF))
          {
            int max=0, i_max=0;
            /// search maximal value
            for(i=0;i<BIT_BOUND;i++)
            {
              if (pGPSchan->accBitBoundCorr[i] > max)
              {
                max   = pGPSchan->accBitBoundCorr[i];
                i_max = i - 5;
              }
            }
            /// change bit position
            if (i_max != 0)
            {
              /// bit position and correlations right shift
              if (i_max > 0)
              {
                pGPSchan->status.bit = egpschan_NOBIT;
                pGPSchan->epochInBit = (CORR_BIT - i_max);
                for(i=0;i<(CORR_BIT - i_max);i++)
                {
                  pGPSchan->initArray[CORR_BIT + i] = pGPSchan->initArray[CORR_BIT + i + i_max];
                }
              }
              else  /// bit position and correlations left shift
              {
                pGPSchan->epochInBit = -i_max;
                for(i=(CORR_BIT + i_max);i<CORR_BIT;i++)
                {
                  pGPSchan->initArray[i - i_max] = pGPSchan->initArray[CORR_BIT + i];
                }
              }
            }
            pGPSchan->bitBoundControl = fg_ON;
          }
          else
          {
            /// save bit value and copy correlations to begin array
            pGPSchan->lastBit = pGPSchan->status.bit;
            for(i=0;i<CORR_BIT;i++)
              pGPSchan->initArray[i] = pGPSchan->initArray[i+CORR_BIT];
          }
        }
      }
      else  /// skip epoch
      {
        pGPSchan->processFlag          = epf_DONOTHING_ELSE;
        pGPSchan->status.bit           = egpschan_NOBIT;
        pGPSchan->status.epoch_control = EC_TYPICAL;
      }
      /// process epoch again
      if (pGPSchan->processFlag == epf_PROCESS_EPOCH_AGAIN)
      { 
        eGPSChanBit oldBit;
        pGPSchan->processFlag = epf_DONOTHING_ELSE;
        /// process the case if bit decoding is done on the boundary of epoch
        oldBit = pGPSchan->status.bit;
        GPSchanProcessEpoch(pGPSchan, pEpoch, week, ms_count, preamble, CN_THRESHOLD, coefPllDll, Nframe);
        if (oldBit!=egpschan_NOBIT)
        {
          pGPSchan->status.bit = oldBit;
        }
      }
    }
    /// control SNR in tracking startup mode
    if (pGPSchan->status.state == es_TRACKING_STARTUP && pGPSchan->status.epochCounter>CN_TIME)
    {
      if ((pGPSchan->status.CN_dBHzQ7 <= CN_LONG_BB) && (pGPSchan->CNControl == fg_OFF))
      {
        pGPSchan->StartupTime     = STARTUP_LONG;
        pGPSchan->BitBoundaryTime = BB_LONG_TIME;
        pGPSchan->CNControl       = fg_ON;
      }
      if(pGPSchan->status.CN_dBHzQ7<(CN_THRESHOLD<<7)) 
      {
        pGPSchan->status.state = es_IDLE;
      }
    }
    /// tracking startup mode finished and start tracking mode
    if ((pGPSchan->status.state == es_TRACKING_STARTUP) && 
        (pGPSchan->status.epochCounter > pGPSchan->StartupTime))
    {
      pGPSchan->status.state = es_TRACKING;
      /// for reacquisition and aiding: calculate bit numbers of satellite removing to current time
      if (preamble->PreambleTime != -1)
      {
        uint32 delta_ms;

        if (((preamble->week == (WEEK-1)) && (week == 0)) || (week > preamble->week))
        {
          delta_ms = ms_count + MS_WEEK - preamble->time - pGPSchan->epochInBit - pGPSchan->StartupTime;
        }
        else
        {
          delta_ms = ms_count - preamble->time - pGPSchan->epochInBit - pGPSchan->StartupTime;
        }
        pGPSchan->status.Nframe_add = (int32)(delta_ms*0.05 + 0.5);
      }
    }
    /// control SNR in tracking mode
    if (pGPSchan->status.state == es_TRACKING)
    {
      if(pGPSchan->status.CN_dBHzQ7<(CN_THRESHOLD<<7))
      {
        pGPSchan->status.state = es_IDLE;
      }
    }
    break;
  default: break;
  }
}

//================================================================================
/// Return status of channel
///
/// @param[in]  pGPSchan     - pointer to instance
//================================================================================
const tGPSchanStatus* GPSchanGetStatus(const tGPSchan *pGPSchan)
{
  return &pGPSchan->status;
}


