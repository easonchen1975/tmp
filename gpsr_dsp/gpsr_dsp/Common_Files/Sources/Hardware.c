/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Hardware initialize and start
 *    @file                   Hardware.c
 *    @author                 B. Oblakov
 *    @date                   25.06.2005
 *    @version                1.0
 */
/*****************************************************************************/
/* INT Version : Tom */

#include "SampleDriver.h"
#include "Hardware.h"
#include "MainManager.h"   

/// for target
#ifdef USE_DSPBIOS
/// asm-function: individual interrupt enable
extern int32 asmWriteIER(void);
extern int32 asmWriteICR(void);

//=========================================================================
/// @brief     Delay in mcs
///
/// @param[in] time_mcs - number mcs
//=========================================================================
void delay_mcs(int32 time_mcs)
{
  int32 delay = 60*time_mcs;
  *(volatile int32 *)0x01980000 = 0x301;       /// Timer#1 stop
  *(volatile int32 *)0x01980004 = 0xffffffff;  /// Timer#1 period
  *(volatile int32 *)0x01980000 = 0x3c1;       /// Timer#1 start

  while(1)
  {
    int32 counter = *(volatile int32 *)0x01980008;
    if(counter >= delay) break;
  }
  *(volatile int32 *)0x01980000 = 0x301;       /// Timer#1 stop
  return;
}

//=========================================================================
/// @brief       Hardware initialize
//=========================================================================
void HardwareInit()
{
  extern tSampleManager SampleManagerState;

  /// GPIO initialization
  *(volatile int32 *)0x01b00000   = 0xf0;  /// GPIO 4 enable 5,6,7
  /// Event Clearing
  *(volatile int32 *)0x01a0fff8   = 0x10;  /// ECRL
  /// Event Polarity
  *(volatile int32 *)0x01a0ffdc   = 0x00;  /// EPRL

  /// RFFE setting
  /// Register 0
  //*(volatile uint16 *)0x68000020 = 0x1a30; //0x0010;  //LSB
  *(volatile uint16 *)0x68000004 = 0x1a30;  /// v.703 -  LSB
  delay_mcs(10);

  //*(volatile uint16 *)0x68000022 = 0xa293;  /// MSB
  //*(volatile uint16 *)0x68000006 = 0xa293;  /// v.703 -  MSB
  *(volatile uint16 *)0x68000006 = 0xa295;  /// For Passive Antrenna

  delay_mcs(200);
  /// Register 1
  //*(volatile uint16 *)0x68000020 = 0x0881; //0x2881;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0881;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x8550;  /// MSB

  *(volatile uint16 *)0x68000006 = 0x8550;  /// v.703 -  63d

    delay_mcs(200);
  /// Register 2
  //*(volatile uint16 *)0x68000020 = 0x1dc2;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x1dc2;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0xeadf;  /// MSB
  *(volatile uint16 *)0x68000006 = 0xeadf;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 3
  //*(volatile uint16 *)0x68000020 = 0x0083;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0083;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x9ec0;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x9ec0;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 4
  //*(volatile uint16 *)0x68000020 = 0x0804;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0804;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x0c00; //0x0c08;  ///MSB
  *(volatile uint16 *)0x68000006 = 0x0c00;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 5
  //*(volatile uint16 *)0x68000020 = 0x0705;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0705;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x8000;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x8000;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 6
  //*(volatile uint16 *)0x68000020 = 0x0006;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0006;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x8000;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x8000;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 7
  //*(volatile uint16 *)0x68000020 = 0x1b47;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x1b47;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x1006;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x1006;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 8
  //*(volatile uint16 *)0x68000020 = 0x4018;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x4018;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x1e0f;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x1e0f;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 9
  //*(volatile uint16 *)0x68000020 = 0x4029;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x4029;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x14c0;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x14c0;  /// v.703 -  MSB 
  delay_mcs(2000);


  /// Interrupt Flags clearing
  asmWriteICR();
  delay_mcs(10);
  /// Individual interrupt enable
  asmWriteIER();
  delay_mcs(100);

  /// Interrupt enable
  *(volatile int32 *)0x01a0ffe8 = 0x100;  /// Channel interrupt enable low register

  //********************* even ch0***************************************
  /// EDMA channel 4 
  *(volatile int32 *)0x01a00060 = 0x08380003;                                /// Ch4 Options       
  *(volatile int32 *)0x01a00064 = 0x60000000;                                /// Ch4 SRC Address
  *(volatile int32 *)0x01a00068 = 0x002B005D;                                /// Ch4 Frame count - Element count
  *(volatile int32 *)0x01a0006c = (int32)SampleManagerState.pPackedBuff[0];  /// Ch4 DST Address
  *(volatile int32 *)0x01a00070 = 0x0;                                       /// Ch4 Frame index - Element index
  *(volatile int32 *)0x01a00074 = 0x00000600;                                /// Ch4 Element count reload - Link Address

  /// EDMA link channel 4
  *(volatile int32 *)0x01a00600 = 0x08380003;                                /// Ch4 Options
  *(volatile int32 *)0x01a00604 = 0x60000000;                                /// Ch4 SRC Address
  *(volatile int32 *)0x01a00608 = 0x002B005D;                                /// Ch4 Frame count - Element count
  *(volatile int32 *)0x01a0060c = (int32)SampleManagerState.pPackedBuff[1];  /// Ch4 DST Address
  *(volatile int32 *)0x01a00610 = 0x0;                                       /// Ch4 Frame index - Element index
  *(volatile int32 *)0x01a00614 = 0x00000618;                                /// Ch4 Element count reload - Link Address

  /// EDMA link channel 4
  *(volatile int32 *)0x01a00618 = 0x08380003;                                /// Ch4 Options
  *(volatile int32 *)0x01a0061c = 0x60000000;                                /// Ch4 SRC Address
  *(volatile int32 *)0x01a00620 = 0x002B005D;                                /// Ch4 Frame count - Element count
  *(volatile int32 *)0x01a00624 = (int32)SampleManagerState.pPackedBuff[0];  /// Ch4 DST Address
  *(volatile int32 *)0x01a00628 = 0x0;                                       /// Ch4 Frame index - Element index
  *(volatile int32 *)0x01a0062c = 0x00000600;                                /// Ch4 Element count reload - Link Address

  /// switch RFFE & ANT & OCXO
  //*(volatile uint8 *)0x68000004  = 0x00;
  
  /// UART reset
  //*(volatile uint16 *)0x68000008 = 0x0040;
  *(volatile uint16 *)0x68000024 = 0x0004; // v703- disUART
  *(volatile uint16 *)0x68000024 = 0x0005; // v703- enUART
  delay_mcs(5000);

  /// UART_CNTRL
  //*(volatile uint16 *)0x68000008 = 0x000a;
  //delay_mcs(5000);

  /// nPPS initialization
  /// disable FIFO	
  //*(volatile uint8 *)0x68000000 = 0x0;
  *(volatile uint8 *)0x68000002 = 0x0;  //v.703 - disableFIFO
  delay_mcs(10);
  /// Register CONTROL: Reset
  *(volatile int16 *)0x68000116 = 0x1;
  /// Register CODE1MS
  //*(volatile int16 *)0x68000114 = 0x3fef; 
  *(volatile int16 *)0x6800001a = 0x3fef; //v.703 - value of 1ms
  /// Register PPSMODE: 2 - 1 PPS
  *(volatile int16 *)0x68000118 = 2;
  /// shift
  //*(volatile int16 *)0x68000102 = 0x0;
  *(volatile int16 *)0x68000012 = 0x0; // v.703 -  shift for counter to 1 ms
  //*(volatile int16 *)0x68000108 = 0x0;
  *(volatile int16 *)0x68000014 = 0x0; // v.703 - shift for counter ms
  return;
}

//=========================================================================
/// @brief     Hardware start
//=========================================================================
void HardwareStart(void)
{
	uint8 overRunErr=0;	//v.703 - Test Code
	

   overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code	
   //*(volatile uint8 *)0x68000000 = 0x1;  /// reset FIFO	
   *(volatile uint8 *)0x68000002 = 0x2;  //v.703 - flushFIFO
   overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code
   delay_mcs(10);
   //*(volatile uint8 *)0x68000000 = 0x0;  ///
   *(volatile uint8 *)0x68000002 = 0x0;  //v.703 - disableFIFO
   overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code
   //delay_mcs(10);
   
  /// Start EDMA
  *(volatile int32 *)0x01a0fff4 = 0x00000010;  /// Event enable low register
  delay_mcs(10);
  /// Start FIFO
  //*(volatile uint8 *)0x68000000 = 0x2;         /// mux enable
  *(volatile uint8 *)0x68000002 = 0x1;  //v.703 - enableFIFO
  overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code
  
  /// Register CONTROL
  *(volatile int16 *)0x68000116 = 0x2;         /// Enable
}

#endif
