/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        Satellite manager
 *   @file         SatelliteManager.c
 *   @author       V. Yeremeyev, M. Zhokhova
 *   @date         09.09.2005
 *   @version      2.0
 */
/*****************************************************************************/

#include "SatelliteManager.h"
#include "Terminal.h"
#include "Framework.h"
#include "Control.h"
#include "caCode.h"
#include "NavTask.h"
#include "MainManager.h"
#include "FrameDecoderGPS.h"
#include "SVCordComp.h"
#include "comfuncs.h"
#include "DataConversion.h"

//#undef _DEBUG
#ifdef _DEBUG
//  #include <stdio.h>
//  #define SATMGR_LOG_START      /* search start/end */
//  #define SATMGR_LOG_FOUND      /* satellite signal detection */
//  #define SATMGR_LOG_CREATION   /* satellite object creation */
//  #define SATMGR_LOG_DESTRUCT   /* satellite object destruction */
#endif

/// for target
#ifdef USE_DSPBIOS
//========================================================================================
/// Static function for truncate a 'double' to a 'short' with clamping
/// @param[in]  d  - input 'double' data
/// @return     truncated data
//========================================================================================
static short d2s(double d);

//========================================================================================
/// Generates twiddle factors for TI's custom FFTs.
/// @param[out]  - pointer to twiddle-factor array (2*N elements)
/// @param[in]   - size of FFT
/// @param[in]   - Scale factor to apply to values
/// @return        actual twiddle-factor array size
//========================================================================================
static int gen_twiddle(short *w, int n, double scale);

#else  /// for PC model
//========================================================================================
/// generates vector of twiddle factors for optimized algorithm
/// @param[out]  - pointer to twiddle-factor array (2*N elements)
/// @param[in]   - size of FFT
//========================================================================================
static void tw_gen(short *w, int N);
#endif

//========================================================================================
/// Navigation task parameters structure initialization
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  sv_num    - GPS satellite number (1...32)
/// @return     pointer to navigation task parameters structure or NULL
//========================================================================================
static tNavTaskParams *SatMgr_CreateNavTaskParams(tSatelliteManager *pSatMgr, int8 sv_num);

//========================================================================================
/// Create satellite GPS
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
/// @param[in]  sv_num  - satellite ID
/// @return     pointer to requested satellite or NULL if no satellite found
//========================================================================================
static tSatelliteGPS *CreateSatellite(tSatelliteManager *pSatMgr, int8 sv_num);

//========================================================================================
/// Remove satellite GPS
///
/// @param[in]  pThis - pointer to satellite manager instance
/// @param[in]  sv_num - satellite ID
//========================================================================================
static void RemoveSatellite(tSatelliteManager *pSatMgr, int8 sv_num);

//========================================================================================
/// Create, initialize and remove satellites
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
//========================================================================================
static void InitSatellites(tSatelliteManager *pSatMgr);

//========================================================================================
///  Form list with navigation parameters objects
///
/// @param[in]  pSatMgr           - pointer to satellite manager instance
/// @param[in]  config            - pointer to configuration setup structure
/// @param[in]  NavCond           - pointer to structure with navigation conditions
/// @param[in]  sv_num            - satellite number
/// @return     1 if navigation parameters object available otherwise 0
//========================================================================================
static int SatMgr_NavTaskParamsList(tSatelliteManager *pSatMgr,
                                    CONFIG_SETUP      *config,
                                    NAV_CONDITIONS    *NavCond,
                                    uint8              sv_num);

/// for target
#ifdef USE_DSPBIOS
//========================================================================================
/// Static function for truncate a 'double' to a 'short' with clamping
/// @param[in]  d  - input 'double' data
/// @return     truncated data
//========================================================================================
static short d2s(double d)
{
    if (d >=  32767.0) return  32767;
    if (d <= -32768.0) return -32768;
    return (short)d;
}

//========================================================================================
/// Generates twiddle factors for TI's custom FFTs.
/// @param[out]  - pointer to twiddle-factor array (2*N elements)
/// @param[in]   - size of FFT
/// @param[in]   - Scale factor to apply to values
/// @return        actual twiddle-factor array size
//========================================================================================
static int gen_twiddle(short *w, int n, double scale)
{
  int i, j, k;

  for (j = 1, k = 0; j < n >> 2; j = j << 2)
  {
    for (i = 0; i < n >> 2; i += j << 1)
    {
      w[k + 11] = d2s(scale * cos(6.0 * M_PI * (i + j) / n));
      w[k + 10] = d2s(scale * sin(6.0 * M_PI * (i + j) / n));
      w[k +  9] = d2s(scale * cos(6.0 * M_PI * (i    ) / n));
      w[k +  8] = d2s(scale * sin(6.0 * M_PI * (i    ) / n));

      w[k +  7] = d2s(scale * cos(4.0 * M_PI * (i + j) / n));
      w[k +  6] = d2s(scale * sin(4.0 * M_PI * (i + j) / n));
      w[k +  5] = d2s(scale * cos(4.0 * M_PI * (i    ) / n));
      w[k +  4] = d2s(scale * sin(4.0 * M_PI * (i    ) / n));

      w[k +  3] = d2s(scale * cos(2.0 * M_PI * (i + j) / n));
      w[k +  2] = d2s(scale * sin(2.0 * M_PI * (i + j) / n));
      w[k +  1] = d2s(scale * cos(2.0 * M_PI * (i    ) / n));
      w[k +  0] = d2s(scale * sin(2.0 * M_PI * (i    ) / n));

      k += 12;
    }
  }

  return k;
}

#else  /// for PC model

//========================================================================================
/// generates vector of twiddle factors for optimized algorithm
/// @param[out]  - pointer to twiddle-factor array (2*N elements)
/// @param[in]   - size of FFT
//========================================================================================
static void tw_gen(short *w, int N)
{
  int j, k, i;
  double x_t, y_t, theta1, theta2, theta3;
  const double M = 32767.0;  /// M is 16383 for scale by 4

  for (j=1, k=0; j <= N>>2; j = j<<2)
  {
    for (i=0; i < N>>2; i+=j)
    {
      theta1 = 2*M_PI*i/N;
      x_t = M*cos(theta1);
      y_t = M*sin(theta1);
      w[k]   =  (short)x_t;
      w[k+1] =  (short)y_t;

      theta2 = 4*M_PI*i/N;
      x_t = M*cos(theta2);
      y_t = M*sin(theta2);
      w[k+2] =  (short)x_t;
      w[k+3] =  (short)y_t;

      theta3 = 6*M_PI*i/N;
      x_t = M*cos(theta3);
      y_t = M*sin(theta3);
      w[k+4] =  (short)x_t;
      w[k+5] =  (short)y_t;
      k+=6;
    }
  }
}
#endif

//=============================================================================
/// Satellite manager initialization
///
/// @param[in]  pSatMgr    - pointer to satellite manager instance
/// @param[in]  coefDLL    - PLL/DLL coefficients
/// @param[in]  timeDLL    - PLL and DLL predetection integration time
//=============================================================================
void SatMgr_Init(tSatelliteManager *pSatMgr,
                 int32              coefDLL,
                 int8               timeDLL)
{
  int i; 

  /// initialize search parameters
  pSatMgr->IsFSearchStarted  = fg_ON;
  pSatMgr->IsFSearchFinished = fg_ON;
  pSatMgr->sv_fsearch        = 1;
  pSatMgr->mode_fsearch      = sm_GPS;
  for(i=0;i<GPS_SATELLITE_COUNT;i++)
    pSatMgr->infoSat[i].state = fsm_NORMAL;

  /// initialize decoder structures
  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    pSatMgr->TopSystem.DecCommon.preamble[i].PreambleTime = -1;
  }

  pSatMgr->nPPSCorrect = fg_OFF;
     
  /// Generates twiddle factors for TI's custom FFTs
  #ifdef USE_DSPBIOS
#ifdef EXT_SRAM
   gen_twiddle((int16*)pSatMgr->fast_search->wFFT, 4096, 32767.0);
#else  
    gen_twiddle((int16*)pSatMgr->fast_search.wFFT, 4096, 32767.0);
#endif	
  #else
    tw_gen((int16*)pSatMgr->fast_search.wFFT, 4096);
  #endif

  /// calculate coefficients for GPS satellites
  PLLDLLSetDllCoef(&pSatMgr->coefPllDll, coefDLL, timeDLL);
}

//========================================================================================
/// Navigation task parameters structure initialization
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  sv_num    - GPS satellite number (1...32)
/// @return     pointer to navigation task parameters structure or NULL
//========================================================================================
static tNavTaskParams *SatMgr_CreateNavTaskParams(tSatelliteManager *pSatMgr, int8 sv_num)
{
  int i;

  if(IS_GPS_SAT(sv_num))
  {
    for(i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
      if(pSatMgr->TopSystem.navtask_params[i].sv_num == SATELLITE_INVALID_ID)
      {
        pSatMgr->TopSystem.navtask_params[i].sv_num = sv_num;
        return &pSatMgr->TopSystem.navtask_params[i];
      }
  }
  return 0;
}

//========================================================================================
/// Create satellite GPS
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
/// @param[in]  sv_num  - satellite ID
/// @return     pointer to requested satellite or NULL if no satellite found
//========================================================================================
static tSatelliteGPS *CreateSatellite(tSatelliteManager *pSatMgr, int8 sv_num)
{
  int i;
  
  #ifdef SATMGR_LOG_CREATION
    tTerminal *pTerm = GetPrimaryDebugTerminal();
    static char buff[256];
    sprintf(buff, "Creating #%d\r\n",sv_num);
    pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
  #endif
 
  if(IS_GPS_SAT(sv_num))
  {
    for(i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
      if(pSatMgr->gps_satellites[i].sv_num == SATELLITE_INVALID_ID)
      {
        pSatMgr->gps_satellites[i].sv_num = sv_num;
        pSatMgr->SVArray[sv_num-1] = 1;
        return &pSatMgr->gps_satellites[i];
      }
  }
  return 0;
}

//========================================================================================
/// Remove satellite navigation task parameters
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
/// @param[in]  sv_num  - satellite ID
//========================================================================================
void SatMgr_RemoveNavTaskParams(tSatelliteManager *pSatMgr, int8 sv_num)
{
  int i;

  if(IS_GPS_SAT(sv_num))    
  {
    for(i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
    {
      if(pSatMgr->TopSystem.navtask_params[i].sv_num == sv_num)
      {
        memset(&pSatMgr->TopSystem.navtask_params[i], 0, sizeof(pSatMgr->TopSystem.navtask_params[i]));
        pSatMgr->TopSystem.navtask_params[i].sv_num = SATELLITE_INVALID_ID;
        pSatMgr->infoSat[sv_num-1].state = fsm_NORMAL;
        break;
      }
    }
  }
}

//========================================================================================
/// Remove satellite GPS
///
/// @param[in]  pThis - pointer to satellite manager instance
/// @param[in]  sv_num - satellite ID
//========================================================================================
static void RemoveSatellite(tSatelliteManager *pSatMgr, int8 sv_num)
{
  int i;

  #ifdef SATMGR_LOG_DESTRUCT
    tTerminal *pTerm = GetPrimaryDebugTerminal();
    static char buff[256];
    if (IS_GPS_SAT(sv_num))
    {
      tSatelliteGPS *pSat = SatMgr_GetSatellite(pSatMgr, sv_num);
        sprintf(buff, "Dest #%d %d, %d\r\n", sv_num, 
                pSat->gps_chan.status.CN_dBHzQ7>>7, pSat->gps_chan.status.epochCounter);
        pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
    }
  #endif

  /// for GPS satellite: reset satellite object
  if(IS_GPS_SAT(sv_num))
  {
    for(i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
    {
      if(pSatMgr->gps_satellites[i].sv_num == sv_num)
      {
        pSatMgr->SVArray[sv_num-1] = 0;
        memset(&pSatMgr->gps_satellites[i], 0, sizeof(tSatelliteGPS));
        pSatMgr->gps_satellites[i].sv_num = SATELLITE_INVALID_ID;
		
#ifdef EXT_SRAM
{		
		extern int8 *pMyPrompt[];
		// restore prompt link
        pSatMgr->gps_satellites[i].gps_chan.prompt = pMyPrompt[i];
}		
#endif		
        break;
      }   
    }
  }
}

//========================================================================================
/// Get pointer to navigation task parameters structure
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  sv_num    - GPS satellite number (1...32)
/// @return     pointer to navigation task parameters structure or NULL
//========================================================================================
tNavTaskParams *SatMgr_GetNavTaskParams(tSatelliteManager *pSatMgr, int8 sv_num)
{
  int i;

  if(IS_GPS_SAT(sv_num))
  {
    for(i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
      if(pSatMgr->TopSystem.navtask_params[i].sv_num == sv_num)
        return &(pSatMgr->TopSystem.navtask_params[i]);
  }
  return 0;
}

//========================================================================================
/// Get existing satellite GPS or SBAS
///
/// @param[in]  pThis - pointer to satellite manager instance
/// @param[in]  sv_num - satellite ID
/// @return     pointer to requested satellite or NULL if no satellite found
//========================================================================================
tSatelliteGPS *SatMgr_GetSatellite(tSatelliteManager *pSatMgr,
                                   int8               sv_num)
{
  int i;

  if(IS_GPS_SAT(sv_num))
  {
    for(i = 0; i < GPS_SAT_CHANNELS_COUNT; i++)
      if(pSatMgr->gps_satellites[i].sv_num == sv_num)
        return &(pSatMgr->gps_satellites[i]);
  }
  return 0;
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (SatMgr_FSearch, "fast_text")
#endif

//========================================================================================
/// Search thread routine
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  pSamples  - pointer to samples buffer
//========================================================================================
void SatMgr_FSearch(tSatelliteManager *pSatMgr, int32 *pSamples)
{
  tFSearchRes res;                            ///< fast search result

  #ifdef SATMGR_LOG_START
    tTerminal *pTerm = GetPrimaryDebugTerminal();
    static char buff[256];
  #endif

  ASSERT(pSatMgr->IsFSearchFinished == fg_ON);
  ASSERT(pSatMgr->IsFSearchStarted  == fg_ON);

  /// clear fast search finished flag
  pSatMgr->IsFSearchFinished = fg_OFF;

  #ifdef SATMGR_LOG_START
  {
    sprintf(buff, "SV #%d, %d\r\n", pSatMgr->sv_fsearch, pSatMgr->mode_fsearch);
    pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
  }
  #endif

  /// GPS satellite search
  if (pSatMgr->mode_fsearch == sm_GPS)
  {
    memset(&pSatMgr->fs_res[0], 0, sizeof(tFSearchRes));

#ifdef EXT_SRAM
    /// signal transformation to the frequency domain
    FsearchSignalFFT(pSatMgr->fast_search, pSamples);
    /// C/A code replica FFT calculation
    FsearchInit(pSatMgr->fast_search, pSatMgr->sv_fsearch);  
    /// carrier out satellite fast search in 8 given epochs
    FsearchProcess(pSatMgr->fast_search, &res, FSEARCH_SHIFT_MIN, FSEARCH_SHIFT_MAX, pSamples);
#else
    /// signal transformation to the frequency domain
    FsearchSignalFFT(&pSatMgr->fast_search, pSamples);
    /// C/A code replica FFT calculation
    FsearchInit(&pSatMgr->fast_search, pSatMgr->sv_fsearch);  
    /// carrier out satellite fast search in 8 given epochs
    FsearchProcess(&pSatMgr->fast_search, &res, FSEARCH_SHIFT_MIN, FSEARCH_SHIFT_MAX, pSamples);
#endif	
    /// check result of fast search
    if (res.isFound)
        pSatMgr->fs_res[0] = res;
  }
  else  /// reacquisition fast search or search by almanac
  {
    double nShift;  ///< temporary shift doppler frequency
    int    i;
    int16  delta;   ///< additional shift fro doppler

#ifdef EXT_SRAM
    /// signal transformation to the frequency domain
    FsearchSignalFFT(pSatMgr->fast_search, pSamples);
#else
    /// signal transformation to the frequency domain
    FsearchSignalFFT(&pSatMgr->fast_search, pSamples);
#endif

    for(i=0;i<pSatMgr->SVFSearch.N; i++)
    {
      /// calculate average shift doppler frequency
      nShift = ldexp(pSatMgr->infoSat[(pSatMgr->SVFSearch.Cells[i]-1)].dopplerQ15, -15) / (1000.*0.4995);
      delta  = pSatMgr->infoSat[(pSatMgr->SVFSearch.Cells[i]-1)].nShift_add;

      #ifdef SATMGR_LOG_START
      {
        sprintf(buff, "Reacq: SV #%d\r\n", pSatMgr->SVFSearch.Cells[i]);
        pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
      }
      #endif
      /// clear fast search result structures
      res.isFound = 0;
      memset(&pSatMgr->fs_res[i], 0, sizeof(tFSearchRes));
#ifdef EXT_SRAM
      /// C/A code replica FFT calculation for current satellite
      FsearchInit(pSatMgr->fast_search, pSatMgr->SVFSearch.Cells[i]);
      /// carrier out current satellite fast search in 8 given epochs
      FsearchProcess(pSatMgr->fast_search, &res, ((int16)nShift - delta), ((int16)nShift + delta), pSamples);
#else
      /// C/A code replica FFT calculation for current satellite
      FsearchInit(&pSatMgr->fast_search, pSatMgr->SVFSearch.Cells[i]);
      /// carrier out current satellite fast search in 8 given epochs
      FsearchProcess(&pSatMgr->fast_search, &res, ((int16)nShift - delta), ((int16)nShift + delta), pSamples);
#endif	  
      /// check result of fast search
      if (res.isFound)
        pSatMgr->fs_res[i] = res;
    }
  }
  /// clear fast search started flag
  pSatMgr->IsFSearchStarted = fg_OFF;
}

//========================================================================================
/// Determine if the new search can be started
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @return     search finished flag 
//========================================================================================
eFlag SatMgr_CanStartFSearch(const tSatelliteManager *pSatMgr)
{
  return pSatMgr->IsFSearchFinished;
}

//========================================================================================
/// Create, initialize and remove satellites
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
//========================================================================================
static void InitSatellites(tSatelliteManager *pSatMgr)
{
  tSatelliteGPS *pSat;      ///< pointer to satellite
  double         doppler,   ///< doppler
                 position;  ///< position
  int            i;

  #ifdef SATMGR_LOG_FOUND
    tTerminal *pTerm = GetPrimaryDebugTerminal();
    static char buff[256];
  #endif

  #ifdef SATMGR_LOG_FOUND
    {
      sprintf(buff, "SV: %d\r\n", pSatMgr->sv_fsearch);
      pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
    }
  #endif

  /// GPS satellite search
  if (pSatMgr->mode_fsearch == sm_GPS)
  {
    /// check satellite is found flag
    if(pSatMgr->fs_res[0].isFound == 1)
    {       
      /// check satellite is exist
      pSat=SatMgr_GetSatellite(pSatMgr, pSatMgr->sv_fsearch);
      if (pSat == NULL)
      {   
        /// Satellite create
        pSat = CreateSatellite(pSatMgr, pSatMgr->sv_fsearch);
        /// check pointer to satellite object
        if(pSat != NULL)
        {
          /// calculate doppler = convert doppler of fast search from Q11 format kHz to Hz
          doppler  = (pSatMgr->fs_res[0].freqEst / 2048. * 1000.);
          /// calculate position = convert from 2046 samples to 8184 samples and extrapolation by fast search time
          position = pSatMgr->fs_res[0].position*4 - ((pSatMgr->count_search-8)*doppler*CONV_PLL2DLL*M_2PI*8184.);
          /// initialize satellite
          SatGPS_Init(pSat, doppler, position, pSatMgr->wn, pSatMgr->ms_count, 
                      &pSatMgr->TopSystem.DecCommon.preamble[pSat->sv_num-1]);
          /// for GPS satellite set ephemeris and almanac flags
          if(IS_GPS_SAT(pSat->sv_num))
          {
            /// ephemeris: 2 bit
            if ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<(pSat->sv_num-1))) == (0x1<<(pSat->sv_num-1)))
              pSat->status |= 0x4;
            /// almanac: 3 bit
            if ((pSatMgr->TopSystem.alm_set_flgs&(0x1<<(pSat->sv_num-1))) == (0x1<<(pSat->sv_num-1)))
              pSat->status |= 0x8;
          }
          /// set state of satellite to Tracking
          pSatMgr->infoSat[pSat->sv_num-1].state = fsm_TRACKING;

          #ifdef SATMGR_LOG_FOUND
            {
              sprintf(buff, "Acq - ms: %d, #%2d: %dHz, pos: %d\r\n", pSatMgr->ms_count, pSat->sv_num, (int)doppler, (int)position);
              pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
            }
          #endif
        }  /// if (pSat != NULL)
      }    /// if (pSat == NULL)
    }      /// if (pSatMgr->fs_res[0].isFound == 1)
  }
  else  /// reacquisition fast search or search by almanac
  {
    /// for all satellite from list
    for(i=0;i<pSatMgr->SVFSearch.N; i++)
    {
      /// check satellite is found flag
      if (pSatMgr->fs_res[i].isFound == 1)
      {
        /// check satellite is exist
        pSat=SatMgr_GetSatellite(pSatMgr, pSatMgr->SVFSearch.Cells[i]);
        if (pSat != NULL)
          continue;
        /// create satellite
        pSat=CreateSatellite(pSatMgr, pSatMgr->SVFSearch.Cells[i]);
        /// check pointer to satellite object
        if (pSat != NULL)
        {
          /// calculate doppler = convert doppler of fast search from Q11 format kHz to Hz
          doppler = (pSatMgr->fs_res[i].freqEst / 2048. * 1000.);
          /// calculate position = convert from 2046 samples to 8184 samples and extrapolation by fast search time
          position = pSatMgr->fs_res[i].position*4 - ((pSatMgr->count_search-8)*doppler*CONV_PLL2DLL*M_2PI*8184.);
          /// initialize satellite
          SatGPS_Init(pSat, doppler, position, pSatMgr->wn, pSatMgr->ms_count, 
                      &pSatMgr->TopSystem.DecCommon.preamble[pSat->sv_num-1]);
          /// for GPS satellite set ephemeris and almanac flags
          if(IS_GPS_SAT(pSat->sv_num))
          {
            /// ephemeris: 2 bit
            if ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<(pSat->sv_num-1))) == (0x1<<(pSat->sv_num-1)))
              pSat->status |= 0x4;
            /// almanac: 3 bit
            if ((pSatMgr->TopSystem.alm_set_flgs&(0x1<<(pSat->sv_num-1))) == (0x1<<(pSat->sv_num-1)))
              pSat->status |= 0x8;
          }
          /// set state of satellite to Tracking
          pSatMgr->infoSat[pSat->sv_num-1].state = fsm_TRACKING;

          #ifdef SATMGR_LOG_FOUND
          {
            sprintf(buff, "ReAcq - ms: %d, #%2d: %dHz, pos: %d\r\n", pSatMgr->ms_count, pSat->sv_num, (int)doppler, (int)position);
            pTerm->vtable.pfnWrite(&pTerm->vtable, buff, strlen(buff));
          }
          #endif              
        }
      }
      else  /// if !(pSatMgr->fs_res[i].isFound == 1)
      {
        /// check satellite state
        if (pSatMgr->infoSat[(pSatMgr->SVFSearch.Cells[i]-1)].state == fsm_REACQ)
        {
          /// control time for reacquisition mode
          if ((pSatMgr->infoSat[(pSatMgr->SVFSearch.Cells[i]-1)].time_reacq + REACQ_TIME) < pSatMgr->ms_count)
          {
            /// set normal state
            pSatMgr->infoSat[(pSatMgr->SVFSearch.Cells[i]-1)].state = fsm_NORMAL;
            /// control satellite in other antennas
            pSatMgr->TopSystem.DecCommon.preamble[(pSatMgr->SVFSearch.Cells[i]-1)].PreambleTime = -1;
          }    /// control time of reacquisition
        }      /// reacquisition state
      }        /// if !(pSatMgr->fs_res[i].isFound == 1)
    }          /// for(i=0;i<pSatMgr->SVFSearch.N; i++)
  }            /// reacquisition fast search
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (SatMgr_SearchProcess, "fast_text")
#endif

//========================================================================================
/// Main satellite manager processing routine for satellite search
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
//========================================================================================
void SatMgr_SearchProcess(tSatelliteManager *pSatMgr)
{
  uint8 i,
        j,
        busy = 0;  ///< busy channels number
  uint32 mask = 0;

  /// check error value flags
  if (pSatMgr->IsFSearchFinished == fg_ON && pSatMgr->IsFSearchStarted == fg_OFF)
  {
    /// enable new fast search
    pSatMgr->IsFSearchFinished = fg_ON;
    pSatMgr->IsFSearchStarted  = fg_ON;
  }
  /// check fast search finished state
  if (pSatMgr->IsFSearchFinished == fg_OFF && pSatMgr->IsFSearchStarted == fg_OFF)
  {
    /// calculate busy channels number
    for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
    {
      if (pSatMgr->gps_satellites[i].sv_num != SATELLITE_INVALID_ID)
        busy++;
    }
    /// initialize satellite
    if (busy < SAT_SEARCH_COUNT)
      InitSatellites(pSatMgr);

    /// enable new fast search
    pSatMgr->IsFSearchFinished = fg_ON;
    pSatMgr->IsFSearchStarted  = fg_ON;

    /// select search mode
    switch(pSatMgr->mode_fsearch)
    {
    case sm_GPS:  /// GPS satellite search mode
      /// switch search mode: current GPS -> REACQ -> ALM -> GPS
      pSatMgr->SVFSearch.N = 0;
      /// search GPS satellite with reacquisition state
      for(i=1;i<=GPS_SATELLITE_COUNT; i++)
      {
        if (pSatMgr->infoSat[i-1].state == fsm_REACQ && pSatMgr->SVFSearch.N<SEARCH_COUNT)
          pSatMgr->SVFSearch.Cells[pSatMgr->SVFSearch.N++] = i;
      }
      /// check satellites number in list
      if (pSatMgr->SVFSearch.N == 0)
      {
        /// search satellite with almanac
        pSatMgr->SVFSearch.N = 0;
        /// search GPS satellite with reacquisition state
        for(i=1;i<=GPS_SATELLITE_COUNT; i++)
        {
          if (pSatMgr->infoSat[i-1].state == fsm_ALM && pSatMgr->SVFSearch.N<SEARCH_COUNT)
            pSatMgr->SVFSearch.Cells[pSatMgr->SVFSearch.N++] = i;
        }
        /// check satellites number in list
        if (pSatMgr->SVFSearch.N == 0)
        {
          /// search next GPS satellite with acquisition state
          j = 0;
          do 
          { j++;
            pSatMgr->sv_fsearch++;
            pSatMgr->sv_fsearch %= (GPS_SATELLITE_COUNT + 1);
            /// check overflow satellite number
            if(pSatMgr->sv_fsearch == 0)
              pSatMgr->sv_fsearch = 1;
          } 
          while ((j<GPS_SATELLITE_COUNT) && (pSatMgr->infoSat[pSatMgr->sv_fsearch-1].state != fsm_NORMAL));
        }
        else  /// if !(pSatMgr->SVFSearch.N == 0) almanac mode
          pSatMgr->mode_fsearch = sm_ALM;
      }
      else  /// if !(pSatMgr->SVFSearch.N == 0) reacquisition mode
        pSatMgr->mode_fsearch = sm_REACQ;
    break;
    case sm_REACQ:  /// reacquisition search mode: REACQ -> ALM -> GPS
      /// GPS satellite number
      {
        pSatMgr->SVFSearch.N = 0;
        /// search GPS satellite with almanac state
        for(i=1;i<=GPS_SATELLITE_COUNT; i++)
        {
          if (pSatMgr->infoSat[i-1].state == fsm_ALM && pSatMgr->SVFSearch.N<SEARCH_COUNT)
            pSatMgr->SVFSearch.Cells[pSatMgr->SVFSearch.N++] = i;
        }
        /// check satellites number in list
        if (pSatMgr->SVFSearch.N == 0)
        {
          j = 0;
          /// search next GPS satellite with acquisition state
          do
          { j++;
            pSatMgr->sv_fsearch++;
            pSatMgr->sv_fsearch %= (GPS_SATELLITE_COUNT + 1);
            /// check overflow satellite number
            if(pSatMgr->sv_fsearch == 0)
              pSatMgr->sv_fsearch = 1;
          } 
          while ((j<GPS_SATELLITE_COUNT) && (pSatMgr->infoSat[pSatMgr->sv_fsearch-1].state != fsm_NORMAL));
          pSatMgr->mode_fsearch = sm_GPS;
        }
        else  /// if !(pSatMgr->SVFSearch.N == 0) almanac mode
          pSatMgr->mode_fsearch = sm_ALM;
      }
      break;
    case sm_ALM:  /// almanac search mode: ALM -> REACQ -> GPS
      /// GPS satellite number
      {
        pSatMgr->SVFSearch.N = 0;
        /// search GPS satellite with reacquisition state
        for(i=1;i<=GPS_SATELLITE_COUNT; i++)
        {
          if (pSatMgr->infoSat[i-1].state == fsm_REACQ && pSatMgr->SVFSearch.N<SEARCH_COUNT)
            pSatMgr->SVFSearch.Cells[pSatMgr->SVFSearch.N++] = i;
        }
        /// check satellites number in list
        if (pSatMgr->SVFSearch.N == 0)
        {
          j = 0;
          /// search next GPS satellite with acquisition state
          do
          { j++;
            pSatMgr->sv_fsearch++;
            pSatMgr->sv_fsearch %= (GPS_SATELLITE_COUNT + 1);
            /// check overflow satellite number
            if(pSatMgr->sv_fsearch == 0)
              pSatMgr->sv_fsearch = 1;
          } 
          while ((j<GPS_SATELLITE_COUNT) && (pSatMgr->infoSat[pSatMgr->sv_fsearch-1].state != fsm_NORMAL));
          pSatMgr->mode_fsearch = sm_GPS;
        }
        else  /// if !(pSatMgr->SVFSearch.N == 0) reacquisition search mode
          pSatMgr->mode_fsearch = sm_REACQ;
      }
      break;
    }    /// switch(pSatMgr->mode_fsearch)
  }      /// if (pSatMgr->IsFSearchFinished == fg_OFF && pSatMgr->IsFSearchStarted == fg_OFF)
  
  /// for all GPS satellite
  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    mask = (0x1<<i);

    if ((pSatMgr->TopSystem.eph_set_flgs&mask) == mask)
    {
      /// check health of satellite
      if (pSatMgr->TopSystem.EphGPS[i].health != 0)
      {
        tSatelliteGPS *pSat;  ///< pointer to satellite

        /// get satellite
        pSat = SatMgr_GetSatellite(pSatMgr, pSatMgr->TopSystem.EphGPS[i].prn);
        /// check pointer to satellite and remove it
        if (pSat != NULL)
          RemoveSatellite(pSatMgr, pSat->sv_num); 
        /// set state of satellite to no search state
        pSatMgr->infoSat[i].state = fsm_WAIT;
        pSatMgr->TopSystem.health_flgs &= (0xFFFFFFFF - (0x1<<i));
        continue;
      }
      else
      {
        pSatMgr->TopSystem.health_flgs |= (0x1<<i);
      }
    }
    if ((pSatMgr->TopSystem.alm_set_flgs&mask) == mask)
    {
      if (pSatMgr->TopSystem.AlmGPS[i].health != 0)
      {
        tSatelliteGPS *pSat;  ///< pointer to satellite

        /// get satellite
        pSat = SatMgr_GetSatellite(pSatMgr, pSatMgr->TopSystem.AlmGPS[i].prn);
        /// check pointer to satellite and remove it
        if (pSat != NULL)
          RemoveSatellite(pSatMgr, pSat->sv_num); 
        /// set state of satellite to no search state
        pSatMgr->infoSat[i].state = fsm_WAIT;
        pSatMgr->TopSystem.health_flgs &= (0xFFFFFFFF - (0x1<<i));
      }
      else
      {
        pSatMgr->TopSystem.health_flgs |= (0x1<<i);
        /// set state of satellite to acquisition state
        if (pSatMgr->infoSat[i].state == fsm_WAIT)
          pSatMgr->infoSat[i].state = fsm_NORMAL;
      }
    }
  }
}

//========================================================================================
/// Main satellite manager processing routine
///
/// @param[in]  pSatMgr       - pointer to satellite manager instance
/// @param[in]  pSamples      - pointer to samples buffer
/// @param[in]  sv_num        - satellite number
/// @param[in]  config        - configuration parameters structure
//========================================================================================
void SatMgr_Process(tSatelliteManager   *pSatMgr,
                    tComplex8Aligned    *pSamples,
                    int8                 sv_num,
                    CONFIG_SETUP         config)
{
  tSatelliteGPS  *pSat;         ///< pointer to satellite object
  tGPSchanStatus  status;       ///< channel status
  uint8           eph_set_flg;  ///< ephemeris available of current satellite

  /// get satellite object
  pSat = SatMgr_GetSatellite(pSatMgr, sv_num);
  /// check satellite object
  if(pSat == NULL)
  {
    pSatMgr->SVArray[sv_num-1] = 0;
    return;
  }

  if ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<(sv_num-1))) == (0x1<<(sv_num-1)))
    eph_set_flg = 1;
  else
    eph_set_flg = 0;

  /// GPS satellite process
  status = *SatGPS_Process(pSat, pSamples, &pSatMgr->TopSystem.DecCommon, pSatMgr->wn, pSatMgr->ms_count, 
                           config.SNRth, pSatMgr->coefPllDll, pSatMgr->WeekInit, eph_set_flg);  
  /// check IDLE state
  if (status.state == es_IDLE)
  { 
    /// reacquisition mode
    if (pSat->gps_chan.status.epochCounter >= 2000)
    {
      /// save doppler, current time, set reacquisition state
      pSatMgr->infoSat[sv_num-1].dopplerQ15 = pSat->gps_chan.dopplerQ15;
      pSatMgr->infoSat[sv_num-1].nShift_add = 4;
      pSatMgr->infoSat[sv_num-1].state      = fsm_REACQ;
      pSatMgr->infoSat[sv_num-1].time_reacq = pSatMgr->ms_count;
    }
    else  /// acquisition mode
      pSatMgr->infoSat[sv_num-1].state = fsm_NORMAL;

    /// remove satellite
    RemoveSatellite(pSatMgr, sv_num);
  }
}

//========================================================================================
/// Interface control function for nPPS signal generator
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  User      - user parameters structure
/// @param[in]  Correct   - structure for time correction by navigation task solution result
/// @param[in]  User      - User parameters
//========================================================================================
void SatMgr_nPPSControl(tSatelliteManager  *pSatMgr,
                        NAV_CONDITIONS      NavCond,
                        uint32              PeriodTime,
                        tCorrectTime        Correct,
                        USER                User)
{
  int16   ms1_shift,     ///< 1ms shift
          ms1000_shift;  ///< 1s shift
  float64 R,             ///< clock bias
          dt_ms,         ///< for 1ms shift
          dt_tf,         ///< ms number
          dt_tx,         ///< next second
          t_npps,        ///< next nPPS
          dt_corr;       ///< correction time
  uint32  ts;            ///< current time

  if (Correct.flag_bl == 0)
  {
    Correct.flag_bl = 1;

    /// calculate clock bias
    if (User.SolutionFlag && (User.CoordValid == OK_NAV_TASK))
      R = (Correct.R_nts * l_cLight * 1e+3) + (Correct.Rdot_nts * l_cLight * PeriodTime);  /// [ms]
    else
      R = (Correct.R * l_cLight * 1e+3) + (Correct.Rdot * l_cLight * PeriodTime);  /// [ms]

    /// calculate current time
    ts = (uint32)(NavCond.t*1e+3 - R + 0.5);

    /// number of ms from last second
    dt_tf = (float64)(ts%1000);  

    // time to next second
    dt_tx = 1000.0 - dt_tf + R + 1;
    
    /// check overflow
    if (dt_tx > 1000.0)
      dt_tx -= 1000.0;

    /// time to next signal nPPS
    t_npps = (float64)(1000 - (pSatMgr->ms1000_latch+1)) + (float64)(16368 - pSatMgr->ms1_latch)/16368.0; 
         
    /// time of correct 
    dt_corr = dt_tx - t_npps;   
    
    if (fabs(dt_corr) >= 1000.0)
    {
	  if (dt_corr > 0)
	    dt_corr -= 1000.0;
	  else
	    dt_corr += 1000.0;
    } 
	  
    /// ms
    {
      ms1000_shift = ((int16)dt_corr);  /// integral part of ms
      if (ms1000_shift > 0) 
        ms1000_shift = -(1000-ms1000_shift);
//      *(volatile int16*)0x68000108 = -ms1000_shift;
      *(volatile int16*)0x68000014 = -ms1000_shift; // v.703 - shift for counter ms
    }
    /// fraction of ms
    {
      dt_ms = dt_corr - (int16)dt_corr;
      ms1_shift = (int16)(dt_ms * 16368.) + 7;
//      *(volatile int16*)0x68000102 = -ms1_shift;
	  *(volatile int16*)0x68000012 = -ms1_shift; // v.703 - shift for counter to 1 ms
    }

    Correct.flag_bl = 0;
  }
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (SatMgr_CorrectTime, "fast_text")
#endif

//========================================================================================
/// Function for system time correction
///
/// @param[in]  pSatMgr           - pointer to satellite manager instance
/// @param[in]  CountPostNavTask  - pointer to navigation task solution counter
/// @param[in]  PeriodTime        - time slice for navigation task solution
/// @param[in]  pCorrectTime      - pointer to structure for time correction
/// @param[in]  pCorrectTimeOP    - pointer to structure for OP time correction
/// @param[in]  fOP_Condition     - OP condition
/// @param[in]  User              - User parameters
//========================================================================================
void SatMgr_CorrectTime(tSatelliteManager  *pSatMgr,
                        int32              *CountPostNavTask,
                        uint32              PeriodTime,
                        tCorrectTime       *pCorrectTime,
                        tCorrectTimeOP     *pCorrectTimeOP,
                        int8                fOP_Condition,
                        USER                User)
{
  double dt,                           ///< delta time
         deltaR   = 0.,                ///< next clock bias
         deltaROP = 0.;                ///< next clock bias for OP
  int8   sv_num,                       ///< satellite number
         svID,                         ///< satellite with good preamble
         i,                            ///< counter
         firstPreamble = 0,            ///< first preamble decoded of any satellite
         correct = 0;                  ///< correction flag
  int32  navtask = *CountPostNavTask;  ///< counter for navigation task solution
  int32  ms_count_old,                 ///< old time of week, ms
         sec;                          ///< second number
  LIST32 allSVNum;                     ///< all satellite with first preamble detection list
  tSatelliteGPS *pSat,                 ///< pointer to current satellite
                *pSatSel;              ///< pointer to selected satellite
  int32 t;                             ///< delta time

  if (!(User.SolutionFlag && (User.CoordValid == OK_NAV_TASK)))
  {
    allSVNum.N = 0;
    for (sv_num = 0; sv_num < GPS_SATELLITE_COUNT; sv_num++) 
    {
      if (pSatMgr->SVArray[sv_num] != 0)
      {
        pSat = SatMgr_GetSatellite(pSatMgr, (sv_num+1));
        if (pSat != NULL)
        {
          /// check correct time flag
          if (pSat->decoder.CorrectTime == fg_ON)
          {
            /// calculate difference between time by preamble and current receiver time
            if ((pSat->decoder.wn == (WEEK-1)) && (pSatMgr->wn == 0))
              dt = (pSat->decoder.wn - pSatMgr->wn - WEEK)*S_WEEK + (int32)(pSat->decoder.tow - pSatMgr->ms_count)*1.0e-3;
            else if ((pSat->decoder.wn == 0) && (pSatMgr->wn == (WEEK-1)))
              dt = (pSat->decoder.wn - pSatMgr->wn + WEEK)*S_WEEK + (int32)(pSat->decoder.tow - pSatMgr->ms_count)*1.0e-3;
            else
              dt = (pSat->decoder.wn - pSatMgr->wn)*S_WEEK + (int32)(pSat->decoder.tow - pSatMgr->ms_count)*1.0e-3;

            /// Receiver time is need correction
            if(fabs(dt)>0.2)  
            { 
              /// raise correction time by preamble flag, set week number, calculate new time of week
              pSatMgr->CorrTimeByPr = fg_ON;
              ms_count_old          = pSatMgr->ms_count;
              pSatMgr->wn           = pSat->decoder.wn; 
              pSatMgr->ms_count     = pSat->decoder.tow + 64;  /// in ms
              pSatMgr->wn_OP        = pSat->decoder.wn; 
              pSatMgr->ms_count_OP  = pSat->decoder.tow + 64;  /// in ms
              /// calculate second number
              sec                   = (int32)((int32)((double)pSat->decoder.tow/1000.)*1000.);
              navtask               = sec;
              /// search next time for posting navigation task solution
              while (navtask < (pSatMgr->ms_count + 36))
              {
                navtask += PeriodTime;
              }
              *CountPostNavTask     = navtask;
              correct               = 1;
            }
            pSat->decoder.CorrectTime = fg_OFF;
          }  /// if (pSatMgr->gps_satellites[antenna][chan].decoder->CorrectTime == fg_ON)
          if (pSat->decoder.FastPreamble == fg_ON)
          {
            firstPreamble = 1;
            allSVNum.Cells[allSVNum.N++] = pSat->sv_num;
          }
        }
      }
    }

    if (pSatMgr->preambleStart == fg_OFF)
    {
      if (firstPreamble == 1)
        pSatMgr->preambleStart = fg_ON;
    }
    else  /// pSatMgr->preambleStart = fg_ON
    {
      if (pSatMgr->ms_fastStart == BIT_TIME_MS)
      {
        int8 ok;  ///< good preamble

        if (correct == 0)
        {
          if (allSVNum.N >= 4)
          {
            svID = SATELLITE_INVALID_ID;
            for(sv_num = 0; sv_num < allSVNum.N; sv_num++)
            {
              ok = 0;
              pSatSel = SatMgr_GetSatellite(pSatMgr, allSVNum.Cells[sv_num]);
              for(i = (sv_num+1); i < allSVNum.N; i++)
              {
                pSat = SatMgr_GetSatellite(pSatMgr, allSVNum.Cells[i]);
                if (pSatSel->decoder.PreambleTime == pSat->decoder.PreambleTime)
                {
                  ok++;
                }
                if (ok >= 3)
                {
                  svID = allSVNum.Cells[sv_num];
                }
              }
              if (svID != SATELLITE_INVALID_ID)
                break;
            }
            if (svID != SATELLITE_INVALID_ID)
            {
              pSatSel = SatMgr_GetSatellite(pSatMgr, svID);
              if (ok < (allSVNum.N-1))
              {
                for(sv_num=0; sv_num < allSVNum.N; sv_num++)
                {
                  pSat = SatMgr_GetSatellite(pSatMgr, allSVNum.Cells[sv_num]);
                  if (pSatSel->decoder.PreambleTime != pSat->decoder.PreambleTime)
                  {
                    RemoveSatellite(pSatMgr, allSVNum.Cells[sv_num]);
                    pSatMgr->TopSystem.DecCommon.preamble[allSVNum.Cells[sv_num]-1].PreambleTime = -1;
                  }
                }
              }
              /// calculate difference between time by preamble and current receiver time
              if ((pSatSel->decoder.wn == (WEEK-1)) && (pSatMgr->wn == 0))
                dt = (pSatSel->decoder.wn - pSatMgr->wn - WEEK)*S_WEEK 
                   + (int32)(pSatSel->decoder.tow - pSatMgr->ms_count)*1.0e-3;
              else if ((pSatSel->decoder.wn == 0) && (pSatMgr->wn == (WEEK-1)))
                dt = (pSatSel->decoder.wn - pSatMgr->wn + WEEK)*S_WEEK
                   + (int32)(pSatSel->decoder.tow - pSatMgr->ms_count)*1.0e-3;
              else
                dt = (pSatSel->decoder.wn - pSatMgr->wn)*S_WEEK 
                   + (int32)(pSatSel->decoder.tow - pSatMgr->ms_count)*1.0e-3;

              /// Receiver time is need correction
              if(fabs(dt)>0.2)  
              { 
                /// raise correction time by preamble flag, set week number, calculate new time of week
                pSatMgr->CorrTimeByPr = fg_ON;
                ms_count_old          = pSatMgr->ms_count;
                pSatMgr->wn           = pSatSel->decoder.wn; 
                pSatMgr->ms_count     = pSatSel->decoder.tow + 64 + 20;  /// in ms
                pSatMgr->wn_OP        = pSatSel->decoder.wn; 
                pSatMgr->ms_count_OP  = pSatSel->decoder.tow + 64 + 20;  /// in ms
                /// calculate second number
                sec                   = (int32)((int32)((double)pSatSel->decoder.tow/1000.)*1000.);
                navtask               = sec;
                /// search next time for posting navigation task solution
                while (navtask < (pSatMgr->ms_count + 36 - 20))
                {
                  navtask += PeriodTime;
                }
                *CountPostNavTask     = navtask;
                correct               = 1;
              }
            }
          }
        }
        for(sv_num=0; sv_num < allSVNum.N; sv_num++)
        {
          pSat = SatMgr_GetSatellite(pSatMgr, allSVNum.Cells[sv_num]);
          if (pSat != NULL)
            pSat->decoder.FastPreamble = fg_OFF;
        }
        pSatMgr->ms_fastStart  = 0;
        pSatMgr->preambleStart = fg_OFF;
      }
      else  /// increment counter
        pSatMgr->ms_fastStart++;
    }

    /// new correction time in preamble structure
    if (correct == 1)
    {
      for(sv_num=0; sv_num<GPS_SATELLITE_COUNT; sv_num++)
        if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].PreambleTime != -1)
        {
          pSatMgr->TopSystem.DecCommon.preamble[sv_num].time += (pSatMgr->ms_count - ms_count_old);
          pSatMgr->TopSystem.DecCommon.preamble[sv_num].week  = pSatMgr->wn;
        }
    }
  }
  else
  {
    for (sv_num = 0; sv_num < GPS_SATELLITE_COUNT; sv_num++) 
    {
      if (pSatMgr->SVArray[sv_num] != 0)
      {
        pSat = SatMgr_GetSatellite(pSatMgr, (sv_num+1));
        if (pSat != NULL)
        {
          /// check correct time flag
          if (pSat->decoder.CorrectTime == fg_ON)
          {
            pSat->decoder.CorrectTime = fg_OFF;
          }
          if (pSat->decoder.FastPreamble == fg_ON)
          {
            pSat->decoder.FastPreamble = fg_OFF;
          }
        }
      }
    }
  }

  /// check correct time
  if (pCorrectTimeOP->FlagCorrectTime == fg_ON)
  {
    if (pCorrectTimeOP->flag_bl == 0)
    {
      pCorrectTimeOP->flag_bl         = 1;
      pCorrectTimeOP->R               = (pCorrectTimeOP->R + pCorrectTimeOP->R_nts - pCorrectTimeOP->Rnc);
      pCorrectTimeOP->Rdot            = pCorrectTimeOP->Rdot_nts;
      pCorrectTimeOP->FlagCorrectTime = fg_OFF;
      pCorrectTimeOP->flag_bl         = 0;
    }
  }
  else if (pCorrectTimeOP->Rdot != 0)  /// check clock drift value
  {
    pCorrectTimeOP->R += (pCorrectTimeOP->Rdot*0.001);
  }
  
  /// check correct time
  if (pCorrectTime->FlagCorrectTime == fg_ON)
  {
    if (pCorrectTime->flag_bl == 0)
    {
      pCorrectTime->flag_bl         = 1;
      pCorrectTime->R               = (pCorrectTime->R + pCorrectTime->R_nts - pCorrectTime->Rnc);
      pCorrectTime->R_mes           = (pCorrectTime->R_mes + pCorrectTime->R_nts - pCorrectTime->Rnc);
      pCorrectTime->Rdot            = pCorrectTime->Rdot_nts;
      pCorrectTime->FlagCorrectTime = fg_OFF;
      pCorrectTime->flag_bl         = 0;

      /// calculate clock bias
      deltaR = pCorrectTime->R*l_cLight*1000;

      /// correct time of week, to integral ms
      pSatMgr->ms_count_OP = pSatMgr->ms_count = (pSatMgr->ms_count - (int32)ceil(deltaR-0.5));
      navtask = *CountPostNavTask;
      if (pSatMgr->ms_count > navtask)
      {
        while (pSatMgr->ms_count > navtask)
        {
          navtask += PeriodTime;
        }
        *CountPostNavTask = navtask;
      }

      /// correct time of preamble structure
      t = (int32)ceil(deltaR-0.5);
      if (Abs(t)>0)
      {
        for(sv_num=0; sv_num<GPS_SATELLITE_COUNT; sv_num++)
        {
          if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].PreambleTime != -1)
          {
            pSatMgr->TopSystem.DecCommon.preamble[sv_num].time -= t;
            if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].time < 0)
            {
              pSatMgr->TopSystem.DecCommon.preamble[sv_num].time += MS_WEEK;
              pSatMgr->TopSystem.DecCommon.preamble[sv_num].week -= 1;
            }
            if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].time > MS_WEEK)
            {
              pSatMgr->TopSystem.DecCommon.preamble[sv_num].time -= MS_WEEK;
              pSatMgr->TopSystem.DecCommon.preamble[sv_num].time += 1;
            }
          }
        }
      }
      /// correct clock bias
      pCorrectTime->R   = (deltaR - ceil(deltaR-0.5))*cLight/1000;
      if (fOP_Condition == OP_COND_NO_SOLUTION)
      {
        pCorrectTimeOP->R       = pCorrectTime->R;
        pCorrectTimeOP->Rdot    = pCorrectTime->Rdot;
        pSatMgr->ms_count_OP    = pSatMgr->ms_count;
      }
      else
      {
        deltaROP = pCorrectTimeOP->R*l_cLight*1000;
        pCorrectTimeOP->R = (deltaROP - ceil(deltaR-0.5))*cLight/1000;
      }
    }
  }
  else if (pCorrectTime->Rdot != 0)  /// check clock drift value
  {
    /// calculate new clock bias value
    deltaR             = (pCorrectTime->R + pCorrectTime->Rdot*0.001)*l_cLight*1000;
    pCorrectTime->R_mes += (pCorrectTime->Rdot*0.001);

    /// correct time of week to integral ms
    pSatMgr->ms_count_OP = pSatMgr->ms_count = (pSatMgr->ms_count - (int32)ceil(deltaR-0.5));
    navtask = *CountPostNavTask;
    if (pSatMgr->ms_count > navtask)
    {
      while (pSatMgr->ms_count > navtask)
      {
        navtask += PeriodTime;
      }
      *CountPostNavTask = navtask;
    }
    /// correct time of preamble structure
    t = (int32)ceil(deltaR-0.5);
    if (Abs(t)>0)
    {
      for(sv_num=0; sv_num<GPS_SATELLITE_COUNT; sv_num++)
      {
        if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].PreambleTime != -1)
        {
          pSatMgr->TopSystem.DecCommon.preamble[sv_num].time -= t;
          if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].time < 0)
          {
            pSatMgr->TopSystem.DecCommon.preamble[sv_num].time += MS_WEEK;
            pSatMgr->TopSystem.DecCommon.preamble[sv_num].week -= 1;
          }
          if (pSatMgr->TopSystem.DecCommon.preamble[sv_num].time > MS_WEEK)
          {
            pSatMgr->TopSystem.DecCommon.preamble[sv_num].time -= MS_WEEK;
            pSatMgr->TopSystem.DecCommon.preamble[sv_num].time += 1;
          }
        }
      }
    }
    /// correct clock bias
    pCorrectTime->R = (deltaR - ceil(deltaR-0.5))*cLight/1000;
    deltaROP = pCorrectTimeOP->R*l_cLight*1000;
    pCorrectTimeOP->R = (deltaROP - ceil(deltaR-0.5))*cLight/1000;
  }
}

//========================================================================================
/// Interface function increments on-board time
///
/// @param[in]  pSatMgr          - pointer to satellite manager instance
/// @param[in]  CountPostNavTask - pointer to navigation task solution counter
/// @param[in]  PeriodTime       - period of time for posting NavTaskThread
//========================================================================================
void SatMgr_IncrementTime(tSatelliteManager *pSatMgr,
                          int32             *CountPostNavTask,
                          uint32             PeriodTime)
{
  pSatMgr->count_search++;
  pSatMgr->ms_count++;
  pSatMgr->ms_count_OP++;
  // control end of week
  if (pSatMgr->ms_count > MS_WEEK)
  {
    pSatMgr->ms_count = 1;
    *CountPostNavTask = PeriodTime;
    pSatMgr->wn++;
    if (pSatMgr->wn >= WEEK)
      pSatMgr->wn -= WEEK;
  }
  if (pSatMgr->ms_count_OP > MS_WEEK)
  {
    pSatMgr->ms_count_OP = 1;
    pSatMgr->wn_OP++;
    if (pSatMgr->wn_OP >= WEEK)
      pSatMgr->wn_OP -= WEEK;
  }
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SatMgr_NavTaskParamsList, "fast_text_3")
	#endif
#endif

//========================================================================================
///  Form list with navigation parameters objects
///
/// @param[in]  pSatMgr           - pointer to satellite manager instance
/// @param[in]  config            - pointer to configuration setup structure
/// @param[in]  NavCond           - pointer to structure with navigation conditions
/// @param[in]  sv_num            - satellite number
/// @return     1 if navigation parameters object available otherwise 0
//========================================================================================
static int SatMgr_NavTaskParamsList(tSatelliteManager *pSatMgr,
                                    CONFIG_SETUP      *config,
                                    NAV_CONDITIONS    *NavCond,
                                    uint8              sv_num)
{
  tSatelliteGPS  *pSat;            ///< pointer to current satellite
  tNavTaskParams *navtask_params;  ///< pointer to navigation task parameters satellite
  uint8           params = 0;      ///< navigation parameters availability flag

  /// get pointer to satellite
  pSat = SatMgr_GetSatellite(pSatMgr, sv_num);
  /// check pointer to satellite
  if (pSat != NULL)
  {
    /// raw data or coordinates time is exist
    if (   (pSat->raw_data_fix.attempt                         == 1)
        && ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<(pSat->sv_num-1))) == (0x1<<(pSat->sv_num-1)))
        && (pSatMgr->TopSystem.EphGPS[pSat->sv_num-1].health   == 0)
        && (pSatMgr->TopSystem.EphGPS[pSat->sv_num-1].accuracy < 5))
    {
      params = 1;
      /// get pointer to satellite navigation parameters
      navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sv_num);
      /// check it and create satellite navigation parameters
      if (navtask_params == NULL)
        navtask_params = SatMgr_CreateNavTaskParams(pSatMgr, sv_num);
      /// check pointer to satellite navigation parameters
      if (navtask_params != NULL)
      {
        /// initialize satellite navigation parameters
        NavCond->NavSV.Cells[NavCond->NavSV.N++] = sv_num;
        navtask_params->ELEV_MSK             = 0;
        navtask_params->USED                 = 0;
        navtask_params->BAD_SAT              = 0;
        navtask_params->paramsOP.USED_OP     = 0;
        navtask_params->paramsOP.COORDINATES = 0;
        navtask_params->paramsOP.ELEV_MSK_OP = 0;
        navtask_params->paramsOP.BAD_SAT_OP  = 0;
        /// save raw data, estimated errors and SNR
        navtask_params->raw_data_fix.attempt                     = pSat->raw_data_fix.attempt;
        navtask_params->raw_data_fix.ps_delay.integral           = pSat->raw_data_fix.ps_delay.integral;
        navtask_params->raw_data_fix.ps_delay.fract              = pSat->raw_data_fix.ps_delay.fract;
        navtask_params->raw_data_fix.ps_freq.integral            = pSat->raw_data_fix.ps_freq.integral;
        navtask_params->raw_data_fix.ps_freq.fract               = pSat->raw_data_fix.ps_freq.fract;
        navtask_params->raw_data_fix.timeCoord.integral          = pSat->raw_data_fix.timeCoord.integral;
        navtask_params->raw_data_fix.timeCoord.fract             = pSat->raw_data_fix.timeCoord.fract;
        navtask_params->paramsOP.raw_data_fix.attempt            = pSat->raw_data_OP.attempt;
        navtask_params->paramsOP.raw_data_fix.ps_delay.integral  = pSat->raw_data_OP.ps_delay.integral;
        navtask_params->paramsOP.raw_data_fix.ps_delay.fract     = pSat->raw_data_OP.ps_delay.fract;
        navtask_params->paramsOP.raw_data_fix.ps_freq.integral   = pSat->raw_data_OP.ps_freq.integral;
        navtask_params->paramsOP.raw_data_fix.ps_freq.fract      = pSat->raw_data_OP.ps_freq.fract;
        navtask_params->paramsOP.raw_data_fix.timeCoord.integral = pSat->raw_data_OP.timeCoord.integral;
        navtask_params->paramsOP.raw_data_fix.timeCoord.fract    = pSat->raw_data_OP.timeCoord.fract;
        navtask_params->SNR_log                                  = pSat->SNR_log;
      }
    }  /// if (pSat->raw_data_fix.attempt == 1)
  }    /// if (pSat != NULL)

  if (params == 0)
  {
    /// this satellite is not used in navigation task solution
    navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sv_num);
    if (navtask_params != NULL)
    {
      SatMgr_RemoveNavTaskParams(pSatMgr, sv_num);  
      pSat = SatMgr_GetSatellite(pSatMgr, sv_num);
      if (pSat != NULL)
      {
        pSat->status &= 0x0C;  /// clear bits for used in navigation, reject by elevation mask
      }
    }
  }
  return params;
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SatMgr_NavCondForming, "fast_text_3")
	#endif
#endif

//========================================================================================
///  Interface function for navigation conditions forming
///
/// @param[in]  pSatMgr          - pointer to satellite manager instance
/// @param[in]  config           - pointer to configuration setup structure
/// @param[in]  NavCond          - pointer to structure with navigation conditions
/// @param[in]  Synchronization  - synchronization OP and navigation task solution flag
/// @param[in]  CorrectTime      - pointer to structure for time correction
/// @param[in]  CorrectTimeOP    - pointer to structure for OP time correction
/// @param[in]  timeTrackingSWI  - time between function calls from SWITracking thread
//========================================================================================
void SatMgr_NavCondForming(tSatelliteManager   *pSatMgr,
                           CONFIG_SETUP        *config,
                           NAV_CONDITIONS      *NavCond,
                           uint8                Synchronization,
                           tCorrectTime        *CorrectTime,
                           tCorrectTimeOP      *CorrectTimeOP,
                           float32              timeTrackingSWI)
{
  uint8           i,
                  j,
				  sv_num;
  tSatelliteGPS  *pSat;            ///< pointer to current satellite
  tLongFract      Rlf, RlfOP;      ///< clock bias in tLongFract format
  int16           accuracy;        ///< user range accuracy (URA)
  int32           dt_IONQ17,       ///< Ionosphere correction in Q17
                  dt_TROQ17;       ///< Troposphere correction in Q17
  tNavTaskParams *navtask_params;  ///< navigation parameters

  /// Reference time for raw measurements in sec
  NavCond->wn   = pSatMgr->wn;
  NavCond->t    = pSatMgr->ms_count*1.0e-3 + CorrectTime->R*l_cLight;
  NavCond->wnOP = pSatMgr->wn_OP;
  NavCond->tOP  = pSatMgr->ms_count_OP*1.0e-3;
  /// save clock bias for navigation task solution
  CorrectTime->Rnc     = CorrectTime->R;
  CorrectTime->Rnc_mes = CorrectTime->R_mes - CorrectTime->R;
  CorrectTimeOP->Rnc   = CorrectTimeOP->R;
  /// Elevation mask
  NavCond->ElevMask = (float32)config->ElevMask*M_PI/180.0;
  /// OP mode
  NavCond->OPmode   = config->OPmode;
  /// ionosphere/troposphere corrections
  NavCond->IonosphereModel = config->IonosphereModel;
  NavCond->TroposphereModel = config->TroposphereModel;
  /// Orbital/Static OP model
  NavCond->OrbStatic = config->OrbStatic;
  /// RAIM threshold
  NavCond->UERE = config->RAIMth;
  /// work time of tracking thread
  NavCond->timeTracking = (uint32)(timeTrackingSWI);
  /// save almanac, health and visibility
  NavCond->alm_set_flgs    = pSatMgr->TopSystem.alm_set_flgs;
  NavCond->health_flgs     = pSatMgr->TopSystem.health_flgs;
  NavCond->visibility_flgs = pSatMgr->TopSystem.visibility_flgs;
  /// clear list
  NavCond->NavSV.N    = 0;

  /// convert clock bias from double, [m] to tLongFract, [ms]
  LongFractSetDouble((CorrectTimeOP->R*l_cLight*1000.0), &RlfOP);
  LongFractSetDouble((CorrectTime->R*l_cLight*1000.0), &Rlf);

  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    if (pSatMgr->SVArray[i] == 1)
    {
      pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
      if (pSat != NULL)
      {
        /// set accuracy
        if ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<i)) == (0x1<<i))
          accuracy = pSatMgr->TopSystem.EphGPS[i].accuracy;
        else
          accuracy = 0;

        if (pSat->gps_chan.status.state == es_TRACKING)
        {
          SatGPS_RawMeasurementsFormingFix(pSat->gps_chan.status.dopplerQ15, 
                                           pSat->gps_chan.status.dopplerPrimeQ10,
                                           pSat->gps_chan.status.delay, 
                                           pSat->gps_chan.status.tauQ, 
                                           pSat->gps_chan.status.phase_err_avrQ14,
                                           pSat->gps_chan.epochInBit, 
                                           pSatMgr->ms_count, 
                                           pSat->gps_chan.status.epoch_control, 
                                           Rlf, 
                                           &pSat->decoder, 
                                           &pSatMgr->TopSystem.DecCommon.preamble[pSat->sv_num-1], 
                                           pSat->gps_chan.status.Nframe_add, 
                                           pSatMgr->CorrTimeByPr,
                                           &pSat->raw_data_fix);
          SatGPS_RawMeasurementsFormingFix(pSat->gps_chan.status.dopplerQ15, 
                                           pSat->gps_chan.status.dopplerPrimeQ10,
                                           pSat->gps_chan.status.delay, 
                                           pSat->gps_chan.status.tauQ, 
                                           pSat->gps_chan.status.phase_err_avrQ14,
                                           pSat->gps_chan.epochInBit, 
                                           pSatMgr->ms_count_OP, 
                                           pSat->gps_chan.status.epoch_control, 
                                           RlfOP, 
                                           &pSat->decoder, 
                                           &pSatMgr->TopSystem.DecCommon.preamble[pSat->sv_num-1], 
                                           pSat->gps_chan.status.Nframe_add, 
                                           pSatMgr->CorrTimeByPr,
                                           &pSat->raw_data_OP);
        }
        else
          pSat->raw_data_fix.attempt = 0;
        navtask_params = SatMgr_GetNavTaskParams(pSatMgr, pSat->sv_num);
        if (navtask_params != NULL)
        {
          dt_IONQ17 = navtask_params->sat_coord.dt_IONQ17;
          dt_TROQ17 = navtask_params->sat_coord.dt_TROQ17;
        }
        else
        {
          dt_IONQ17 = 0;
          dt_TROQ17 = 0;
        }
        /// check state of channel and calculate satellite estimated errors
        if ((pSat->gps_chan.status.state == es_TRACKING) || (pSat->gps_chan.status.state == es_TRACKING_STARTUP))
        {
          SatGPS_EstimatedErrorFix(&pSat->est_err_fix, accuracy, dt_IONQ17, dt_TROQ17, pSat->gps_chan.status.epochCounter,
            pSat->SNR_log, pSatMgr->coefPllDll.k_begin_val, pSatMgr->coefPllDll.k_begin_shr, config->CoefDLL);
        }
      }
    }
  }

  /// first forming satellites list with (GPS_SAT_CHANNELS_COUNT>>1) satellites number
  if (Synchronization == SYNC_PROCESS)
  {
    /// save satellites from previous list
    for(i=0;i< NavCond->NavSVOld.N;i++)
      SatMgr_NavTaskParamsList(pSatMgr, config, NavCond,  NavCond->NavSVOld.Cells[i]);
  }
  else 
  {
    if (NavCond->NavSVOld.N < 4)
    {
      /// save satellites from previous list
      for(i=0;i< NavCond->NavSVOld.N;i++)
        SatMgr_NavTaskParamsList(pSatMgr, config, NavCond,  NavCond->NavSVOld.Cells[i]);
      for(i=0;i<GPS_SATELLITE_COUNT;i++)
      {
        if (pSatMgr->SVArray[i] != 0)
        {
          sv_num = 0;
          for(j=0;j<NavCond->NavSV.N;j++)
          {
            if ((i+1) == NavCond->NavSV.Cells[j])
            {
              sv_num = 1;
              break;
            }
          }
          if (sv_num == 0)
            SatMgr_NavTaskParamsList(pSatMgr, config, NavCond, (i+1));
        }
        if (NavCond->NavSV.N >= (GPS_SAT_CHANNELS_COUNT>>1))
          break;
      }
    }
    else  /// next forming satellites list
    {
      /// save satellites from previous list
      for(i=0;i< NavCond->NavSVOld.N;i++)
        SatMgr_NavTaskParamsList(pSatMgr, config, NavCond,  NavCond->NavSVOld.Cells[i]);
      /// add one satellite is not found in previous list
      for(i=0;i<GPS_SATELLITE_COUNT;i++)
      {
        if (pSatMgr->SVArray[i] != 0)
        {
          sv_num = 0;
          for(j=0;j< NavCond->NavSV.N;j++)
          {
            if((i+1) ==  NavCond->NavSV.Cells[j])
            {
              sv_num = 1;
              break;
            }
          }
          if (sv_num == 0)
          {
            if ((SatMgr_NavTaskParamsList(pSatMgr, config, NavCond, (i+1)) == 1) && (NavCond->NavSV.N >= (GPS_SAT_CHANNELS_COUNT>>1)))
              break;
          }
        }
      }
    }
  }

  NavCond->NavSVOld.N = 0;  
}

//========================================================================================
///  Almanac and User parameters analyzing function
///
/// @param[in]  pSatMgr - pointer to instance
/// @param[in]  User    - pointer to User parameters structure
/// @param[in]  initMes - pointer to initial data
//========================================================================================
void SatMgr_AnalyzeAlmanacUser(tSatelliteManager *pSatMgr, USER *User, tInitMes *initMes)
{
  tNavTaskParams navtask_params;  ///< satellite navigation parameters
  double         doppler;         ///< doppler
  int            i;
  USER           UserTmp;
  int64          time;
  
  if (pSatMgr->sat_alm.AlmCalc == fg_ON)
    return;

  /// control User parameters
  if (User->SolutionFlag && (User->CoordValid == OK_NAV_TASK))
  {
    pSatMgr->sat_alm.sat_msk = 0;
    /// for all satellite
    for(i=0;i<GPS_SATELLITE_COUNT;i++)
    {
      /// check availability almanac flag
      if ((pSatMgr->TopSystem.alm_set_flgs&(0x1<<i)) == (0x1<<i))
      {
        /// check satellite health 
        if (pSatMgr->TopSystem.AlmGPS[i].health == 0)
        {
          memset(&navtask_params, 0, sizeof(tNavTaskParams));
          
          /// compute GPS satellite position from time and almanac data
          SV_Alm_GPS(&navtask_params, &pSatMgr->TopSystem.AlmGPS[i], User->wn, User->t);

          /// calculation satellite elevation and radial velocity
          SV_User_parameters(&navtask_params, User, 1);

          /// check satellite elevation
          if(navtask_params.sat_params.elev > 0)
          {
            /// calculate doppler frequency and set search by almanac state of satellite
            doppler                        = -(navtask_params.Vrad + User->Rdot)*(l_cLight*f0_GPS); // [Hz] 
            pSatMgr->sat_alm.dopplerQ15[i] = (int32)ldexp(doppler, 15);
            pSatMgr->sat_alm.nShift_add[i] = 0;
            pSatMgr->sat_alm.sat_msk      |= (0x1<<i);
            if (initMes->flag_bl == 0)
            {
              initMes->flag_bl  = 1;
              initMes->InitData = 0;
              initMes->flag_bl  = 0;
            }
          }
        }
      }
    }
    pSatMgr->sat_alm.AlmCalc = fg_ON;
  } 
  else  /// no navigation task solution
  {
    /// check blocking flag
    if (initMes->flag_bl == 0)
    {
      initMes->flag_bl = 1;
      if (initMes->InitData == 1)
      {
        memset(&UserTmp, 0, sizeof(User));

        /// set User parameters
        UserTmp.wn   =  initMes->initData.ecef.wn;
        time      = (((int64)initMes->initData.ecef.mks_h<<16)&0xffffffffffff0000) | (int64)initMes->initData.ecef.mks_l;
        UserTmp.t    = (float64)time*1e-6;
        if (initMes->initData.ecef.status == 1)  /// Geo
        {
          UserTmp.geo.Lat  = ldexp((float64)initMes->initData.geo.Lat, -48);
          UserTmp.geo.Lon  = ldexp((float64)initMes->initData.geo.Lon, -48);
          UserTmp.geo.H    = (float64)initMes->initData.geo.Alt*1.e-3;
          UserTmp.geo.Ndot = (float64)initMes->initData.geo.VNorth*1.e-3;
          UserTmp.geo.Edot = (float64)initMes->initData.geo.VEest*1.e-3;
          UserTmp.geo.Udot = (float64)initMes->initData.geo.VUp*1.e-3;
          Geoid_to_ECEF(&UserTmp.ecef, UserTmp.TOPO, &UserTmp.geo);
        }
        if (initMes->initData.ecef.status == 2)  /// OE
        {
          UserTmp.oe.A      = (float64)initMes->initData.oe.A*1.e-3;
          UserTmp.oe.Omega  = ldexp((float64)initMes->initData.oe.Omega, -48);
          UserTmp.oe.M      = ldexp((float64)initMes->initData.oe.M, -48);
          UserTmp.oe.I      = ldexp((float64)initMes->initData.oe.I, -29);
          UserTmp.oe.OmegaP = ldexp((float64)initMes->initData.oe.OmegaP, -29);
          UserTmp.oe.Ecc    = ldexp((float64)initMes->initData.oe.Ecc, -31);
//          OE_to_ECEF();
        }
        if (initMes->initData.ecef.status == 0)  /// ECEF
        {
          UserTmp.ecef.X    = (float64)(initMes->initData.ecef.X)*1.e-3;
          UserTmp.ecef.Y    = (float64)(initMes->initData.ecef.Y)*1.e-3;
          UserTmp.ecef.Z    = (float64)(initMes->initData.ecef.Z)*1.e-3;
          UserTmp.ecef.Xdot = (float64)(initMes->initData.ecef.VX)*1.e-3;
          UserTmp.ecef.Ydot = (float64)(initMes->initData.ecef.VY)*1.e-3;
          UserTmp.ecef.Zdot = (float64)(initMes->initData.ecef.VZ)*1.e-3;
          /// calculation of basis vectors fro user's local  coordinate system
          ComputeTOPO(&UserTmp);
        }
    
        pSatMgr->sat_alm.sat_msk = 0;
 
        for(i=0;i<GPS_SATELLITE_COUNT;i++)
        {
          /// check availability almanac flag
          if ((pSatMgr->TopSystem.alm_set_flgs&(0x1<<i)) == (0x1<<i))
          {
            /// check satellite health 
            if (pSatMgr->TopSystem.AlmGPS[i].health == 0)
            {
              memset(&navtask_params, 0, sizeof(tNavTaskParams));
        
              /// compute GPS satellite position from time and almanac data
              SV_Alm_GPS(&navtask_params, &pSatMgr->TopSystem.AlmGPS[i], UserTmp.wn, UserTmp.t);
        
              /// calculation satellite elevation and radial velocity
              SV_User_parameters(&navtask_params, &UserTmp, 1);

              /// check satellite elevation
              if (navtask_params.sat_params.elev > 0)
              {
                /// calculate doppler
                doppler                        = -navtask_params.Vrad*(l_cLight*f0_GPS);  /// [Hz]
                pSatMgr->sat_alm.dopplerQ15[i] = (int32)ldexp(doppler, 15);
                pSatMgr->sat_alm.nShift_add[i] = 8;
                pSatMgr->sat_alm.sat_msk      |= (0x1<<i);
              }
            }
          }
        }
        pSatMgr->sat_alm.AlmCalc = fg_ON;
      }
      initMes->InitData = 0;
      initMes->flag_bl  = 0;
    }
  }
}

//========================================================================================
///  Function for fast search using GPS almanac
///
/// @param[in]  pSatMgr  - pointer to satellite manager instance
//========================================================================================
void SatMgr_AlmFastSearch(tSatelliteManager *pSatMgr)
{
  int i;

  /// if satellite parameters is calculate by almanac then copy it 
  /// to satellite information structure and set state to reacquisition
  if (pSatMgr->sat_alm.AlmCalc == fg_ON)
  {
    pSatMgr->TopSystem.visibility_flgs = 0;
    for(i=0;i<GPS_SATELLITE_COUNT;i++)
    {
      if (((pSatMgr->sat_alm.sat_msk>>i)&0x1) == 0x1)
      {
        pSatMgr->TopSystem.visibility_flgs |= (0x1<<i);
        if ((pSatMgr->infoSat[i].state == fsm_NORMAL) || (pSatMgr->infoSat[i].state == fsm_ALM))
        {
          pSatMgr->infoSat[i].dopplerQ15 = pSatMgr->sat_alm.dopplerQ15[i];
          pSatMgr->infoSat[i].nShift_add = pSatMgr->sat_alm.nShift_add[i];
          pSatMgr->infoSat[i].state      = fsm_ALM;
        }
      }
      else if (pSatMgr->infoSat[i].state == fsm_ALM)
        pSatMgr->infoSat[i].state = fsm_NORMAL;

      if (pSatMgr->infoSat[i].state == fsm_TRACKING)
      {
        tSatelliteGPS *pSat;

        pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
        if (pSat != NULL)
        {
          if (pSat->gps_chan.status.state == es_TRACKING)
            pSatMgr->TopSystem.visibility_flgs |= (0x1<<i);
        }
      }
    }
    pSatMgr->sat_alm.AlmCalc = fg_OFF;
  }
}
