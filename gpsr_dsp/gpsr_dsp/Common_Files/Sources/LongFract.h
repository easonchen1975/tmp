/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Basic operations for tLongFract and tLongFract48
 *                            fractional types
 *    @file                   LongFract.h
 *    @author                 M. Zhokhova
 *    @date                   06.09.2005
 *    @version                2.0
 */
/*****************************************************************************/

#ifndef LONGFRACT_H__
#define LONGFRACT_H__

#include "comtypes.h"
#include <math.h>
#include "baseop.h"
#include "L_mpy_ll.h"

/****************************************************************************
  Description: Setting tLongFract48 value
****************************************************************************/
inline_ void LongFract48Set(int16         integral,
                            uint16        fract_0,
                            uint32        fract_1,
                            tLongFract48 *res)
{
  res->integral = integral;
  res->fract_0  = fract_0;
  res->fract_1  = fract_1;
}

/****************************************************************************
  Description: Double value convert to tLongFract48 value
****************************************************************************/
inline_ void LongFract48SetDouble(double y, tLongFract48 *res)
{
  res->integral = (int16)floor(y);
  y             = (y - res->integral)*65536UL;
  res->fract_0  = (uint16)floor(y);
  y             = (y - res->fract_0)*4294967296UL;
  res->fract_1  = (uint32)y;
}

/****************************************************************************
  Description: tLongFract48 value convert to double value
****************************************************************************/
inline_ double LongFract48GetDouble(const tLongFract48 *x)
{
  return (double)x->integral + x->fract_0/65536. + x->fract_1/281474976710656.;
}

/****************************************************************************
  Description: Sum two tLongFract48 numbers
****************************************************************************/
inline_ void LongFract48Add(const tLongFract48 *x,
                            const tLongFract48 *y,
                            tLongFract48       *res)
{
  uint32 z;  ///< sum fractional part
  int32 q;   ///< sum integral part
  int C;     ///< carry bit
  int32 xh = ((int32)x->integral << 16) | (int32) x->fract_0;
  int32 yh = ((int32)y->integral << 16) | (int32) y->fract_0;

  z             = x->fract_1 + y->fract_1;
  C             = (z < x->fract_1) || (z < y->fract_1);
  res->fract_1  = z;
  q             = xh + yh + C;
  res->fract_0  = (uint32)q & 0xFFFF;
  res->integral = q >> 16;
}

/****************************************************************************
  Description: Difference two tLongFract48 numbers
****************************************************************************/
inline_ void LongFract48Sub(const tLongFract48 *x,
                            const tLongFract48 *y,
                            tLongFract48       *res)
{
  int C, C1;  ///< carry bits

  C             = (x->fract_1 < y->fract_1);
  res->fract_1  = x->fract_1 - y->fract_1;
  C1            = (x->fract_0 < (y->fract_0+C));
  res->fract_0  = x->fract_0 - y->fract_0 - C;
  res->integral = x->integral - y->integral - C1;
}

/****************************************************************************
  Description: 32-bit integer in Q31 convert to tLongFract48 value
****************************************************************************/
inline_ void LongFract48SetQ31(int32 y, tLongFract48 *res)
{
  res->integral = (int16)(y >> 31);
  res->fract_0  = (uint16)((uint32)(y << 1) >> 16);
  res->fract_1  = (uint32)y << 17;
}

/****************************************************************************
  Description: tLongFract48 value convert to 32-bit integer in Q31
****************************************************************************/
inline_ int32 LongFract48GetQ31(tLongFract48 *x)
{
  int32 res;
  if (x->integral>= 1) return 0x7FFFFFFF;
  if (x->integral< -1) return 0x80000000;

  res  = (int32)(x->integral >> 15) << 31;
  res |= (uint32)x->fract_0 << 15;
  res |= x->fract_1 >> 17;

  return res;
}

/****************************************************************************
  Description: tLongFract in Q48 value convert tLongFract48 value
****************************************************************************/
inline_ void LongFract48SetLongFractQ48(const tLongFract *y,
                                        tLongFract48     *res)
{
  res->integral = (int16)(y->integral>>16);
  res->fract_0  = (uint16)(y->integral&0xffff);
  res->fract_1  = y->fract;
}

/****************************************************************************
  Description: tLongFract value convert tLongFract48 value
****************************************************************************/
inline_ void LongFract48SetLongFract(const tLongFract *y,
                                     tLongFract48     *res)
{
  res->integral = (int16)(y->integral);
  res->fract_0  = ((uint16)(y->fract>>16))&0xFFFF;
  res->fract_1  = (uint32)(y->fract&0xFFFF)<<16;
}

/****************************************************************************
  Description: tLongFract48 value convert tLongFract value
****************************************************************************/
inline_ void LongFractSetLongFract48(const tLongFract48 *y,
                                     tLongFract         *res)
{
  res->integral = (int32)y->integral;
  res->fract    = ((uint32)y->fract_0<<16)&0xFFFF0000 | (y->fract_1>>16)&0xFFFF;
}

/****************************************************************************
  Description: Setting tLongFract value
****************************************************************************/
inline_ void LongFractSet(int32       integral,
                          uint32      fract,
                          tLongFract *res)
{
  res->integral = integral;
  res->fract    = fract;
}

/****************************************************************************
  Description: Double value convert tLongFract value
****************************************************************************/
inline_ void LongFractSetDouble(double y, tLongFract *res)
{
  res->integral = (int32)floor(y);
  y             = (y - res->integral)*4294967296UL;
  res->fract    = (uint32)y;
}

/****************************************************************************
  Description: tLongFract value convert double value
****************************************************************************/
inline_ double LongFractGetDouble(const tLongFract *x)
{
  return x->integral + x->fract/4294967296.;
}

inline_ void LongFractSetInvQ15(int32 x, tLongFract *y)
{
  uint32 fract;
  int32  integral;

  fract = ((uint32)(x&0x7fff)<<17);
  if (fract != 0)
    fract = 4294967295 - fract;
  integral = (-x>>15);
  LongFractSet(integral, fract, y);
}

/****************************************************************************
  Description: 64-bit integer in Q48 convert tLongFract48 value
****************************************************************************/
inline_ void LongFract48SetQ48(int64 y, tLongFract48 *res)
{
  res->integral = (int16)(y >> 48);
  res->fract_0  = (uint16)((uint64)(y << 16) >> 48);
  res->fract_1  = (uint32)y;
}

/****************************************************************************
  Description: 32-bit integer in Q31 convert tLongFract value
****************************************************************************/
inline_ void LongFractSetQ31(int32 y, tLongFract *res)
{
  if (y<0)
  {
    res->integral = -1;
    res->fract    = y<<1;
  }
  else
  {
    res->integral = 0;
    res->fract    = y<<1;
  }
}

/****************************************************************************
  Description: get saturated value to the range [-1...1]
****************************************************************************/
inline_ int32 LongFractGetQ31(const tLongFract *x)
{
  if (x->integral>= 1) return 0x7FFFFFFF;
  if (x->integral< -1) return 0x80000000;
  if (x->integral>=0)  return x->fract>>1;
  return ((int32)x->fract)>>1;
}

/****************************************************************************
  Description: Sum two tLongFract numbers
****************************************************************************/
inline_ void LongFractAdd(const tLongFract *x,
                          const tLongFract *y,
                          tLongFract       *res)
{
  uint32 z;  ///< sum fractional part
  int C;     ///< carry bit

  z             = x->fract + y->fract;
  C             = (z < x->fract) || (z < y->fract);
  res->fract    = z;
  res->integral = x->integral + y->integral + C;
}

/****************************************************************************
  Description: Difference two tLongFract numbers
****************************************************************************/
inline_ void LongFractSub(const tLongFract *x,
                          const tLongFract *y,
                          tLongFract       *res)
{
  int C;  ///< carry bit

  C             = (x->fract < y->fract);
  res->fract    = x->fract - y->fract;
  res->integral = x->integral - y->integral - C;
}

/****************************************************************************
  Description: Multiply two 32-bit signed integers, result in tLongFract
****************************************************************************/
inline_ void LongFractMul32(int32       x,
                            int32       y,
                            tLongFract *res)
{
  tLongFractDouble dbl;                                    ///< product result
  tLongFract hi, lo;                                       ///< lower and higher parts
  uint32 lo_p   = _MPYU(x, y);                             ///< u16lsb x u16lsb
  int32  hi_p   = _MPYLUHS(x, y);                          ///< u16lsb x s16msb
  uint32 lo_sum = (lo_p >> 16) + ((uint32)hi_p & 0xFFFF);  ///< sum lower part
  int32  carry = lo_sum >> 16;                             ///< carry

  /// lower part: Xlsb * Y
  lo.integral = (hi_p >> 16) + carry;
  lo.fract    = (lo_sum << 16) | (lo_p & 0xFFFF);

  /// higher part: Xmsb * Y
  dbl.dbl     = L_mpy_hi(x, y);  /// 16msb x 32-bit
  hi.integral = ((int32)dbl.lf.hi << 16) | ((uint32)dbl.lf.lo >> 16);
  hi.fract    = (uint32)dbl.lf.lo << 16;
  
  /// sum
  LongFractAdd(&lo, &hi, res);
}

/****************************************************************************
  Description: Shift to right tLongFract value
****************************************************************************/
inline_ void LongFractShr(tLongFract *arg, uint16 shift)
{
  ASSERT(shift < 32);
  arg->fract    = (arg->fract >> shift) | (arg->integral << (32 - shift));
  arg->integral = (arg->integral >> shift);
}

/****************************************************************************
  Description: Shift to left tLongFract value
****************************************************************************/
inline_ void LongFractShl(tLongFract *arg, uint16 shift)
{
  ASSERT(shift < 32);
  arg->integral = (arg->integral << shift) | (arg->fract >> (32 - shift));
  arg->fract    = (arg->fract << shift);
}

/*********************************************************************************
  Description: Multiply 32-bit signed and unsigned integers, result in tLongFract
*********************************************************************************/
inline_ void LongFractMulU32(uint32      x,
                             int32       y,
                             tLongFract *res)
{
  tLongFract hi, lo;  ///< lower and higher parts
  /// Xlsb * Y
  {
    uint32 lo_p   = _MPYU(x, y);                             ///< u16lsb x u16lsb:
    int32  hi_p   = _MPYLUHS(x, y);                          ///< u16lsb x s16msb
    uint32 lo_sum = (lo_p >> 16) + ((uint32)hi_p & 0xFFFF);  ///< sum lower part
    int32  carry  = lo_sum >> 16;                            ///< carry

    lo.integral = (hi_p >> 16) + carry;
    lo.fract = (lo_sum << 16) | (lo_p & 0xFFFF);
  }
  /// Xmsb * Y
  {
    uint32 lo_p   = _MPYHLU(x,y);                            ///< u16msb x u16lsb
    int32 hi_p    = _MPYHUS(x,y);                            ///< u16msb x s16msb
    uint32 lo_sum = (lo_p >> 16) + ((uint32)hi_p & 0xFFFF);  ///< sum lower part
    int32  carry  = lo_sum >> 16;                            ///< carry

    hi.integral = (hi_p >> 16) + carry;
    hi.fract    = (lo_sum << 16) | (lo_p & 0xFFFF);
    /// left shift result by 16 bits
    LongFractShl(&hi, 16);
  }
  /// sum
  LongFractAdd(&lo, &hi, res);
}

/*****************************************************************************
  Description: Multiply 32-bit unsigned integers, result in tLongFract (-Q32)
*****************************************************************************/
inline_ void LongFractMul2U32(uint32      x,
                              uint32      y,
                              tLongFract *res)
{
  tLongFract lo, hi;  ///< lower and higher parts
  /// Xlsb * Y
  {
    uint32 lo_p   = _MPYU(x, y);                             ///< u16lsb x u16lsb
    uint32 hi_p   = _MPYLHU(x, y);                           ///< u16lsb x u16msb
    uint32 lo_sum = (lo_p >> 16) + ((uint32)hi_p & 0xFFFF);  ///< sum lower part
    int32  carry  = lo_sum >> 16;                            ///< carry

    lo.integral = 0;
    lo.fract = (hi_p >> 16) + carry;
  }
  /// Xmsb * Y
  {
    uint32 lo_p   = _MPYHLU(x,y);                            ///< u16msb x u16lsb
    uint32 hi_p   = _MPYHU(x,y);                             ///< u16msb x u16msb
    uint32 lo_sum = (lo_p >> 16) + ((uint32)hi_p & 0xFFFF);  ///< sum lower part
    int32  carry  = lo_sum >> 16;                            ///< carry

    hi.integral = (carry >> 16);
    hi.fract    = (lo_sum & 0xFFFF) | ((lo_p & 0xFFFF) >> 16) | (hi_p&0xffff0000);
  }
  /// sum
  LongFractAdd(&lo, &hi, res);
}

/***********************************************************************************************
  Description: Multiply 32-bit signed integer and tLongFract value, result in tLongFract (-Q16)
************************************************************************************************/
inline_ void LongFractMPY(const tLongFract *x,
                          int32             y,
                          tLongFract       *res)
{
  tLongFract hi, lo;  ///< lower and higher parts

  LongFractMulU32(x->fract, y, &lo);    /// multiply 32-bit signed and unsigned integers
  LongFractShr(&lo, 16);                /// right shift
  LongFractMul32(x->integral, y, &hi);  /// multiply two 32-bit signed integers
  LongFractShl(&hi, 16);                /// left shift
  LongFractAdd(&hi, &lo, res);          /// sum
}

/*************************************************************************************************
  Description: Multiply 32-bit unsigned integer and tLongFract value, result in tLongFract (-Q32)
*************************************************************************************************/
inline_ LongFractMPYU(const tLongFract *x,
                      uint32            y,
                      tLongFract       *res)  // "-Q32"
{
  tLongFract hi, lo;  ///< lower and higher parts

  LongFractMul2U32(y, x->fract, &lo);    /// multiply 32-bit unsigned integers
  LongFractMulU32(y, x->integral, &hi);  /// multiply 32-bit signed and unsigned integers
  LongFractAdd(&hi, &lo, res);           /// sum
}

/****************************************************************************
  Description: Multiply square tLongFract value
****************************************************************************/
inline_ void LongFractSquare(const tLongFract *x,
                             tLongFract       *res) 
{
  tLongFract hi, lo,        ///< lower and higher parts
             hi_hi, hi_lo;  ///< lower and higher parts of higher part

  LongFractMulU32(x->fract, x->integral, &hi_lo);    /// multiply 32-bit signed and unsigned integers
  LongFractMul32(x->integral, x->integral, &hi_hi);  /// multiply two 32-bit signed integers
  /// shift by 32 bits
  hi_hi.integral = hi_hi.fract;
  hi_hi.fract    = 0;

  LongFractAdd(&hi_hi, &hi_lo, &hi);  /// sum
  LongFractMPYU(x, x->fract, &lo);    /// multiply 32-bit unsigned integer and 64-bit value
  LongFractAdd(&hi, &lo, res);        /// sum
}

#endif

