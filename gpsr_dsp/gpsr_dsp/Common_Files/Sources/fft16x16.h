/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  function for calculation 4096-point FFT
 *    @file                   fft16x16.h
 *    @author                 TI Signal Processing Library
 *    @date                   13.06.2006
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef _FFT16x16_H_
#define _FFT16x16_H_

#include "comtypes.h"

//==================================================================================
/// Function for calculation 4096-point FFT
///
/// @param[in]  n      - length of FFT in complex samples
/// @param[in]  ptr_x  - pointer to complex data input
/// @param[in]  ptr_w  - pointer to complex twiddle factor
/// @param[in]  ptr_y  - pointer to complex data output
/// @param[in]  brev   - pointer to bit reverse table containing 64 entries
/// @param[in]  n_min  - smallest FFT butterfly used in computation
/// @param[in]  offset - index in complex samples of sub-FFT from start of main FFT
/// @param[in]  n_max  - size of main FFT in complex samples
//==================================================================================
EXTERN_C void DSP_fft16x16r_c(int           n,
                              short         ptr_x[],
                              short         ptr_w[],
                              short         ptr_y[],
                              unsigned char brev[],
                              int           n_min,
                              int           offset,
                              int           n_max);

#endif // _FFT16x16_H_
