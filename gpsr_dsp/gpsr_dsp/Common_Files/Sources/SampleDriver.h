/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Sample driver interface implementation
 *    @file                   SampleDriver.h
 *    @author                 M. Zhokhova
 *    @date                   16.02.2006
 *    @version                3.0
 */
/*****************************************************************************/
#ifndef _SAMPLE_DRIVER_H_
#define _SAMPLE_DRIVER_H_


#include "comtypes.h"
#include <stdio.h>
#include "baseop.h"
#include "SampleManager.h"
#include "GPS.h"
#include "DataForm.h"

/// General parameters
typedef tComplex8Aligned tSamples;    // samples

// TARGET: force file input (instead of antenna)
#define SMPL_DRV_FORCE_FILE_INPUT
#ifdef USE_DSPBIOS
#undef SMPL_DRV_FORCE_FILE_INPUT
#endif

#define SMP_FE_BLOCK_SIZE       1023
#define SMP_FE_BLOCKS_AMOUNT    8  
#define OUT_SAMPLES             (SMP_FE_BLOCKS_AMOUNT*SMP_FE_BLOCK_SIZE)
#define ADD_BUFFERS             ((SMP_FE_BLOCK_SIZE*8) + 16)
#define SAMPLES_OF_ONE_IRQ_GPS  16368                         ///< 16368 in ms + add 4 samples: number of samples in one GPS block
#define SAMPLES_OF_ONE_MS_GPS   8184                          ///< number samples in millisecond
#define BUFFER_HALF             (SAMPLES_OF_ONE_MS_GPS)       ///< shift even part buffers in relation to odd part
#define SAMPLES_SHIFT           (12276)                       ///< 3/4 from buffer

#ifdef EXT_SRAM
union packbuff_union
{
    tDoubleWord      d[1];
    int16            s[2][4092];
};
union signal_union
{
    tDoubleWord      d[SAMPLES_OF_ONE_IRQ_GPS/4];
    tSamples         s[SAMPLES_OF_ONE_IRQ_GPS];   ///< Unpacked even and odd GPS signal
};  
#endif

typedef struct tagSampleManager
{
  int16  *pPackedBuff[2];
#ifdef EXT_SRAM
  union packbuff_union *PackedBuff;
#else
  union
  {
    tDoubleWord      d[1];
    int16            s[2][4092];
  }                  
  PackedBuff;
#endif  

  volatile int8      bufIndex;   ///< buffer selection

#ifdef EXT_SRAM
  union signal_union *Signal;
  int32              *FastSearchBuf;
#else
  union
  {
    tDoubleWord      d[SAMPLES_OF_ONE_IRQ_GPS/4];
    tSamples         s[SAMPLES_OF_ONE_IRQ_GPS];   ///< Unpacked even and odd GPS signal
  }Signal;
  int32              FastSearchBuf[(OUT_SAMPLES + ADD_BUFFERS)];
#endif  

  volatile int32     out_samples;                 ///< number output samples from front end
  volatile eFlag     SamplesOld;   

  #ifdef SMPL_DRV_FORCE_FILE_INPUT
    FILE* script;
  #endif

} tSampleManager;


//============================================================================================
/// Sample driver initialization
///
/// @param[in] pSampleDrv - pointer to instance
/// @param[in] script     - file name for input samples of PC model
//============================================================================================
EXTERN_C void SmpMgr_Init(tSampleManager *pThis, const char *script);

#endif //_SAMPLE_DRIVER_H_
