/*****************************************************************************
 *    Copyright (c) 2007 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Data protocol
 *    @file                   DataProtocol.c
 *    @author                 M. Zhokhova
 *    @date                   13.06.2007
 *    @version                3.1
 */
/*****************************************************************************/

#include "DataProtocol.h"
#include "Control.h"
#include "Comfuncs.h"
#include "baseop.h"
#include <string.h>

/// for PC model
#ifndef USE_DSPBIOS
#include "DebugPrintf.h"
#include "Report.h"
#endif

#define Q5_1000 (0x7D00)      ///< 1000 in Q5

//=====================================================================
/// Satellites information message
///
/// @param[in]  SatStr  - pointer to string
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//=====================================================================
static void CreateSatString(DATA_STRING       *SatStr,
                              tSatelliteManager *pSatMgr);

//=====================================================================
/// DOP information message
///
/// @param[in]  DOPStr   - pointer to string
/// @param[in]  User_fix - pointer to User object
//=====================================================================
static void CreateDOPString(DATA_STRING *DOPStr, USERfix User_fix);

//=====================================================================
/// User position and velocity information message
/// $POS, time of week[s], X[m], Y[m], Z[m], VX[m/s], VY[m/s], VZ[m/s], 
/// clock bias [mks], oscillator offset [ppm] (in ECEF)
/// ($POS,295278.800000, 2848482.900, 2198811.639, 5248681.296,
///  -0.019,  -0.015,   0.006, 6734.154346, -0.345923)
///
/// @param[in]  POSStr   - pointer to string
/// @param[in]  User_fix - pointer to User object
//=====================================================================
static void CreatePOSString(DATA_STRING *POSStr, USERfix User_fix);

//=====================================================================
/// OP User position and velocity information message
/// $OPP, time of week[s], X[m], Y[m], Z[m], VX[m/s], VY[m/s], VZ[m/s], 
/// clock bias [mks], oscillator offset [ppm], OP condition (in ECEF)
/// ($POS,295278.800000, 2848482.900, 2198811.639, 5248681.296,
///  -0.019,  -0.015,   0.006, 6734.154346, -0.345923, 3)
///
/// @param[in]  OPStr   - pointer to string
/// @param[in]  User_OP - OP User parameters
//=====================================================================
static void CreateOPString(DATA_STRING *OPStr, USERop User_OP);

//=============================================================================
/// Satellites quality estimation information message
/// $SQE, satellites in tracking number, satellite ID,
/// estErrPseudoRange(bias), estErrPseudoRange(noise), estErrPseudoRangeRate,
/// ... for all satellites
/// ($SQE,2, 9, 5.260, 0.131, 0.009,11, 9.610, 0.240, 0.017)
///
/// @param[in]  SQEStr  - pointer to string
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//=============================================================================
static void CreateSQEString(DATA_STRING *SQEStr, tSatelliteManager *pSatMgr);

//=============================================
/// Function converts an integer to a string
///
/// @param[in]  x - integer value
/// @param[in]  Str - pointer to string
/// @return     pointer to string end
//=============================================
static int8* Itoa(int32 x, int8 *Str);

//=======================================================
/// Function converts an integer to a string with point
///
/// @param[in]  x      - integer value
/// @param[in]  dgtPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string end
//=======================================================
static int8* Itoa64(int64 x, int32 dgtPnt, int8 *Str);

//=======================================================
/// Function converts an integer to a string with point
///
/// @param[in]  x      - integer value
/// @param[in]  dgtPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string end
//=======================================================
static int8* Itoa32(int32 x, int32 dgtPnt, int8 *Str);

//=================================================
/// Function converts an integer to a string
///
/// @param[in]  Str - pointer to string
/// @param[in]  N   - necessary digit in string
/// @return     pointer to string with value
//=================================================
static int8 *AddLeadingSpace(int8 *Str, int32 N);

//=============================================
/// Function converts an integer to a string
///
/// @param[in]  x - integer value
/// @param[in]  Str - pointer to string
/// @return     pointer to string end
//=============================================
static int8* Itoa(int32 x, int8 *Str)
{
  int32 devided_x, rest_x, n = 0;
  int8 *StrPtr = Str, StrInt[11];
  
  if(x < 0)
  {
    x = -x;
    *(StrPtr++) = '-';
  }
  
  do // conversion of value to reverse string
  { 
    devided_x = x / 10;                // ceil part after division by 10
    rest_x = x - devided_x * 10;       // residue of division by 10
    StrInt[n++] = (int8)(48 + rest_x); // conversion to ASCII
    x = devided_x;
  }
  while(x != 0);
  
  do // copying of reverse string to string
  {
    *(StrPtr++) = StrInt[--n];
  }
  while(n>0);
  
  *StrPtr = '\0';
  
  return StrPtr;
}

//=======================================================
/// Function converts an integer to a string with point
///
/// @param[in]  x      - integer value
/// @param[in]  dgtPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string end
//=======================================================
static int8* Itoa64(int64 x, int32 dgtPnt, int8 *Str)
{
  int64 devided_x, rest_x;
  int32 n = 0, i;
  int8 *StrPtr = Str, StrInt[20];
  
  if(x < 0)
  {
    x = -x;
    *(StrPtr++) = '-';
  }
  
  do // conversion of value to reverse string
  { 
    devided_x = x / 10;                // ceil part after division by 10
    rest_x = x - devided_x * 10;       // residue of division by 10
    StrInt[n++] = (int8)(48 + rest_x); // conversion to ASCII
    if (n == dgtPnt)
      StrInt[n++] = '.';
    x = devided_x;
  }
  while(x != 0);

  if (n < dgtPnt)
  {
    for(i=n; i<dgtPnt; i++)
      StrInt[n++] = (int8)48;
    StrInt[n++] = '.';
  }
  if (n == (dgtPnt+1))
    StrInt[n++] = (int8)48;
  
  do // copying of reverse string to string
  {
    *(StrPtr++) = StrInt[--n];
  }
  while(n>0);
  
  *StrPtr = '\0';
  
  return StrPtr;
}

//=======================================================
/// Function converts an integer to a string with point
///
/// @param[in]  x      - integer value
/// @param[in]  dgtPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string end
//=======================================================
static int8* Itoa32(int32 x, int32 dgtPnt, int8 *Str)
{
  int32 devided_x, rest_x;
  int32 n = 0, i;
  int8 *StrPtr = Str, StrInt[20];
  
  if(x < 0)
  {
    x = -x;
    *(StrPtr++) = '-';
  }
  
  do // conversion of value to reverse string
  { 
    devided_x = x / 10;                // ceil part after division by 10
    rest_x = x - devided_x * 10;       // residue of division by 10
    StrInt[n++] = (int8)(48 + rest_x); // conversion to ASCII
    if (n == dgtPnt)
      StrInt[n++] = '.';
    x = devided_x;
  }
  while(x != 0);

  if (n < dgtPnt)
  {
    for(i=n; i<dgtPnt; i++)
      StrInt[n++] = (int8)48;
    StrInt[n++] = '.';
  }
  if (n == (dgtPnt+1))
    StrInt[n++] = (int8)48;
  
  do // copying of reverse string to string
  {
    *(StrPtr++) = StrInt[--n];
  }
  while(n>0);
  
  *StrPtr = '\0';
  
  return StrPtr;
}


//=================================================
/// Function converts an integer to a string
///
/// @param[in]  Str - pointer to string
/// @param[in]  N   - necessary digit in string
/// @return     pointer to string with value
//=================================================
static int8 *AddLeadingSpace(int8 *Str, int32 N)
{
  int32 Ncurrent, i, dN;
  int8* strTemp = Str;
  
  Ncurrent = strlen(strTemp);
  
  if(Ncurrent < N)
  {
    i = N;
    dN = N - Ncurrent;
    while(i >= dN) 
    { 
      strTemp[i] = strTemp[i - dN]; 
      i--; 
    }
    i = 0;
    while(i < dN) 
      strTemp[i++] = ' ';
  }
  
  return Str;
}

//=====================================================================
/// Convert integer value to string
///
/// @param[in]  value  - input value
/// @param[in]  digits - necessary quantity of digits in string
/// @param[in]  Str    - pointer to string
/// @return     pointer to string with value
//=====================================================================
int8* Int_to_String(int32 value, int32 digits, int8 *Str)
{
  Itoa(value,Str);
  
  AddLeadingSpace(Str, digits);
  
  Str[digits] = ','; 
  Str[digits+1] = '\0';
  
  return Str;
}

//=======================================================================
/// Convert 64-bit integer value to string with point
///
/// @param[in]  value  - input value
/// @param[in]  digits - necessary quantity of digits in string
/// @param[in]  dgsPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string with value
//=======================================================================
int8* Int64_to_String(int64 value, int32 digits, int32 dgtPnt, int8 *Str)
{
  Itoa64(value, dgtPnt, Str);
  
  AddLeadingSpace(Str, (digits+1));
  
  Str[digits+1] = ','; 
  Str[digits+2] = '\0';
  
  return Str;
}

//=======================================================================
/// Convert 32-bit integer value to string with point
///
/// @param[in]  value  - input value
/// @param[in]  digits - necessary quantity of digits in string
/// @param[in]  dgsPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string with value
//=======================================================================
int8* Int32_to_String(int32 value, int32 digits, int32 dgtPnt, int8 *Str)
{
  Itoa32(value, dgtPnt, Str);
  
  AddLeadingSpace(Str, (digits+1));
  
  Str[digits+1] = ','; 
  Str[digits+2] = '\0';
  
  return Str;
}

//=====================================================================
/// Satellites information message
/// $SAT, satellites in tracking number, satellite ID, 
/// azimuth angle, elevation angle, SNR, used or no used in navigation 
/// task solution, ... for all satellites
/// ($SAT,1,2, 8, 91,65,50,U,17,  0, 0,41,-)
///
/// @param[in]  SatStr  - pointer to string
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//=====================================================================
static void CreateSatString(DATA_STRING       *SatStr,
                            tSatelliteManager *pSatMgr)
{
  int8          *str = SatStr->str;
  int32          i;
  tSatelliteGPS *pSat;
  LIST12         SVtrack;

  SVtrack.N = 0;

  // Command prefix:
  strcpy(str, "$SAT,\0");
  str += 5;
  // satellites in tracking number
  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    if (pSatMgr->SVArray[i] == 1)
    {
      pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
      if (pSat != NULL)
        if (pSat->gps_chan.status.state == es_TRACKING)
          SVtrack.Cells[SVtrack.N++] = (i+1);
    }
  }
  // Satellites in view:
  Int_to_String(SVtrack.N, 2, str);
  str += (SVtrack.N > 0) ? 3 : 2;

  // Dump all tracking satellites:
  for(i = 0; i < SVtrack.N; i++)
  {
    uint8           sat_id = SVtrack.Cells[i];
    uint8           used, OP;
    tNavTaskParams *navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sat_id);
    int32           angle;

    pSat = SatMgr_GetSatellite(pSatMgr, sat_id);

    _NASSERT(pSat != NULL);
    _NASSERT(pSat->sv_num == sat_id);
    
    used = 0;
	OP   = 0;
    // Satellite ID:
    Int_to_String(pSat->sv_num, 2, str); 
    str += 3;
    // navtask_params structure exist
    if (navtask_params != NULL)
    {
      // Satellite azimuth:
      angle = (int32)(navtask_params->sat_params.azim * (float32)rad_to_degree + 0.5);
      Int_to_String(angle, 3, str);    
      str += 4;
      // Satellite elevation:
      angle = (int32)(navtask_params->sat_params.elev * (float32)rad_to_degree + 0.5);
      Int_to_String(angle, 3, str);    
      str += 4;
      Int_to_String(navtask_params->SNR_log, 2, str);
      str += 3;
      // minimum satellite quality estimated error mode
      if ((navtask_params->USED == 1) && (navtask_params->BAD_SAT == 0) && (navtask_params->ELEV_MSK == 0))
        used = 1;
	    if ((navtask_params->paramsOP.USED_OP     == 1) && 
          (navtask_params->paramsOP.BAD_SAT_OP  == 0) &&
          (navtask_params->paramsOP.ELEV_MSK_OP == 0))
	      OP = 1;
    }
    else  // if (navtask_params == NULL)
    {
      // Satellite azimuth:
      angle = (int32)0;
      Int_to_String(angle, 3, str);    
      str += 4;
      // Satellite elevation:
      Int_to_String(angle, 3, str);    
      str += 4;
      Int_to_String(pSat->SNR_log, 2, str);
      str += 3;
    }
	if ((used == 0) && (OP == 0))
	  *str++ = '-';
	if ((used == 1) && (OP == 0))
	  *str++ = 'N';
	if ((used == 0) && (OP == 1))
	  *str++ = 'O';
	if ((used == 1) && (OP == 1))
	  *str++ = 'U';
    if(i < SVtrack.N-1)
      *str++ = ',';
  }
  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';
  SatStr->length = strlen(SatStr->str);
}

//=====================================================================
/// DOP information message: $DOP, GDOP, HDOP, PDOP, TDOP, VDOP
/// ($DOP, 2.59, 1.04, 2.32, 1.16, 2.07)
///
/// @param[in]  DOPStr   - pointer to string
/// @param[in]  User_fix - pointer to User object
//=====================================================================
static void CreateDOPString(DATA_STRING *DOPStr,
                            USERfix      User_fix)
{
  int8 *str = DOPStr->str;
  
  // Command prefix:
  strcpy(str, "$DOP,\0");
  str += 5;
  
  // GDOP
  Int32_to_String(User_fix.GDOP, 4, 2, str);
  str += 6;
  // HDOP
  Int32_to_String(User_fix.HDOP, 4, 2, str);
  str += 6;
  // PDOP
  Int32_to_String(User_fix.PDOP, 4, 2, str);
  str += 6;
  // TDOP
  Int32_to_String(User_fix.TDOP, 4, 2, str);
  str += 6;
  // VDOP
  Int32_to_String(User_fix.VDOP, 4, 2, str);
  str += 5;

  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';

  DOPStr->length = strlen(DOPStr->str);
}

//========================================================================
/// User position and velocity information message
/// $POS, time of week[s], X[m], Y[m], Z[m], VX[m/s], VY[m/s], VZ[m/s], 
/// clock bias [mks], oscillator offset [ppm] (in ECEF)
/// ($POS,295278.800000, 2848482.900, 2198811.639, 5248681.296,
///  -0.019,  -0.015,   0.006, 6734.154346, -0.345923)
///
/// @param[in]  POSStr   - pointer to string
/// @param[in]  User_fix - pointer to User object
//========================================================================
static void CreatePOSString(DATA_STRING *POSStr,
                            USERfix      User_fix)
{
  int8 *str = POSStr->str;

  // Command prefix:
  strcpy(str, "$POS,\0");
  str += 5;

  // Receiver time (current time within week) [s]
  Int64_to_String(User_fix.t, 12, 6, str);
  str += 14;

  // User ECEF X,Y,Z-coordinates, [m]
  Int64_to_String(User_fix.X, 11, 3, str);
  str += 13;
  Int64_to_String(User_fix.Y, 11, 3, str);
  str += 13;
  Int64_to_String(User_fix.Z, 11, 3, str);
  str += 13;

  // User X,Y,Z-coordinates' derivatives, [m/s]
  Int32_to_String(User_fix.Xdot, 8, 3, str);
  str += 10;
  Int32_to_String(User_fix.Ydot, 8, 3, str);
  str += 10;
  Int32_to_String(User_fix.Zdot, 8, 3, str);
  str += 10;

  /// mks
  Int64_to_String(User_fix.R, 12, 6, str);
  str += 14;  
  /// ppm
  Int32_to_String(User_fix.OscillOffset, 9, 6, str);
  str += 11;
  
  Int_to_String(User_fix.fNT_Condition, 2, str);
  str += 2;

  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';

  POSStr->length = strlen(POSStr->str);
}

//=====================================================================
/// OP User position and velocity information message
/// $OPP, time of week[s], X[m], Y[m], Z[m], VX[m/s], VY[m/s], VZ[m/s], 
/// clock bias [mks], oscillator offset [ppm], OP condition (in ECEF)
/// ($POS,295278.800000, 2848482.900, 2198811.639, 5248681.296,
///  -0.019,  -0.015,   0.006, 6734.154346, -0.345923, 3)
///
/// @param[in]  OPStr - pointer to string
/// @param[in]  User  - OP User parameters
//=====================================================================
static void CreateOPString(DATA_STRING *OPStr, USERop UserOP)
{
  int8 *str = OPStr->str;

  // Command prefix:
  strcpy(str, "$OPP,\0");
  str += 5;

  // Receiver time (current time within week) [s]
  Int64_to_String(UserOP.t, 12, 6, str);
  str += 14;

  // User ECEF X,Y,Z-coordinates, [m]
  Int64_to_String(UserOP.X, 11, 3, str);
  str += 13;
  Int64_to_String(UserOP.Y, 11, 3, str);
  str += 13;
  Int64_to_String(UserOP.Z, 11, 3, str);
  str += 13;

  // User X,Y,Z-coordinates' derivatives, [m/s]
  Int32_to_String(UserOP.Xdot, 8, 3, str);
  str += 10;
  Int32_to_String(UserOP.Ydot, 8, 3, str);
  str += 10;
  Int32_to_String(UserOP.Zdot, 8, 3, str);
  str += 10;

  // User receiver clock bias and drift
  /// mks
  Int64_to_String(UserOP.R, 12, 6, str);
  str += 14;  
  /// ppm
  Int32_to_String(UserOP.OscillOffset, 9, 6, str);
  str += 11;

  /// last OP solution condition
  Int_to_String((int32)UserOP.fOP_Condition, 2, str);
  str += 2;

  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';

  OPStr->length = strlen(OPStr->str);
}

//===========================================================================
/// Satellites quality estimation information message
/// $SQE, satellites in tracking number, satellite ID,
/// estErrPseudoRange(bias), estErrPseudoRange(noise), estErrPseudoRangeRate,
/// ... for all satellites
/// ($SQE,2, 9, 5.260, 0.131, 0.009,11, 9.610, 0.240, 0.017)
///
/// @param[in]  SQEStr  - pointer to string
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//===========================================================================
static void CreateSQEString(DATA_STRING       *SQEStr,
                            tSatelliteManager *pSatMgr)
{
  int8          *str = SQEStr->str;
  int8           i;
  tSatelliteGPS *pSat;
  LIST12         SVtrack;
  int32 psr_err_b, psr_err_n, psrr_err;

  SVtrack.N = 0;
  
  /// Command prefix:
  strcpy(str, "$SQE,\0");
  str += 5;
  /// satellites in tracking number
  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    if (pSatMgr->SVArray[i] == 1)
    {
      pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
      if (pSat != NULL)
        if ((pSat->gps_chan.status.state == es_TRACKING) && (IS_GPS_SAT(pSat->sv_num)))
          SVtrack.Cells[SVtrack.N++] = (i+1);
    }
  }
  /// Satellites in view:
  Int_to_String(SVtrack.N, 2, str);
  str += 3;

  /// Dump all tracking satellites:
  for(i = 0; i < SVtrack.N; i++)
  {
    uint8           sat_id = SVtrack.Cells[i];
    tNavTaskParams *navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sat_id);

    pSat = SatMgr_GetSatellite(pSatMgr, sat_id);

    _NASSERT(pSat != NULL);
    _NASSERT(pSat->sv_num == sat_id);
      
    /// Satellite ID:
    Int_to_String(pSat->sv_num, 2, str); 
    str += 3;

    /// navtask_params structure exist
    psr_err_b = L_mpy_ls(pSat->est_err_fix.ps_delay_biasQ17, Q5_1000)>>7; // Q17*Q5 -> Q7
    psr_err_n = (int16)(L_mpy_ls(pSat->est_err_fix.ps_delay_noiseQ17, Q5_1000)>>7);
    psrr_err  = (int16)(L_mpy_ls(pSat->est_err_fix.ps_freqQ17, Q5_1000)>>7);

    Int32_to_String(psr_err_b, 5, 3, str);
    str += 7;
    Int32_to_String(psr_err_n, 5, 3, str);
    str += 7;
    Int32_to_String(psrr_err, 5, 3, str);
    str += 7;
  }

  str--;
  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';

  SQEStr->length = strlen(SQEStr->str);
}

//=====================================================================
/// Ionosphere and troposphere information message
/// $ATM, satellites in tracking number, satellite ID, 
/// elevation angle, ion, tro, used or no used in navigation 
/// task solution, ... for all satellites
///
/// @param[in]  SatAtm  - pointer to string
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//=====================================================================
static void CreateAtmString(DATA_STRING       *SatAtm,
                            tSatelliteManager *pSatMgr)
{
  int8          *str = SatAtm->str;
  int32          i;
  tSatelliteGPS *pSat;
  LIST12         SVtrack;

  SVtrack.N = 0;

  // Command prefix:
  strcpy(str, "$ATM,\0");
  str += 5;
  // satellites in tracking number
  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    if (pSatMgr->SVArray[i] == 1)
    {
      pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
      if (pSat != NULL)
        if (pSat->gps_chan.status.state == es_TRACKING)
          SVtrack.Cells[SVtrack.N++] = (i+1);
    }
  }
  // Satellites in view:
  Int_to_String(SVtrack.N, 2, str);
  str += (SVtrack.N > 0) ? 3 : 2;

  // Dump all tracking satellites:
  for(i = 0; i < SVtrack.N; i++)
  {
    uint8           sat_id = SVtrack.Cells[i];
    uint8           used, OP;
    tNavTaskParams *navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sat_id);
    int32           atm;

    pSat = SatMgr_GetSatellite(pSatMgr, sat_id);

    _NASSERT(pSat != NULL);
    _NASSERT(pSat->sv_num == sat_id);
    
    used = 0;
    OP   = 0;
    // Satellite ID:
    Int_to_String(pSat->sv_num, 2, str); 
    str += 3;
    // navtask_params structure exist
    if (navtask_params != NULL)
    {
      // Satellite elevation:
      atm = (int32)(navtask_params->sat_params.elev * (float32)rad_to_degree + 0.5);
      Int_to_String(atm, 3, str);    
      str += 4;
      // Satellite ionosphere
      atm = L_mpy_ls(navtask_params->sat_coord.dt_IONQ17, Q5_1000)>>7;  // Q17*Q5->Q7->Q0
      Int32_to_String(atm, 6, 3, str);
      str += 8;
      // Satellite troposphere
      atm = L_mpy_ls(navtask_params->sat_coord.dt_TROQ17, Q5_1000)>>7;  // Q17*Q5->Q7->Q0
      Int32_to_String(atm, 6, 3, str);
      str += 8;
      // minimum satellite quality estimated error mode
      if ((navtask_params->USED == 1) && (navtask_params->BAD_SAT == 0) && (navtask_params->ELEV_MSK == 0))
        used = 1;
      if ((navtask_params->paramsOP.USED_OP     == 1) && 
          (navtask_params->paramsOP.BAD_SAT_OP  == 0) &&
          (navtask_params->paramsOP.ELEV_MSK_OP == 0))
        OP = 1;
    }
    else  // if (navtask_params == NULL)
    {
      // Satellite elevation:
      atm = (int32)0;
      Int_to_String(atm, 3, str);    
      str += 4;      
      // Satellite ionosphere:
      atm = (int32)0;
      Int32_to_String(atm, 6, 3, str);
      str += 8;
      // Satellite troposphere:
      Int32_to_String(atm, 6, 3, str);
      str += 8;
    }
    if ((used == 0) && (OP == 0))
      *str++ = '-';
    if ((used == 1) && (OP == 0))
      *str++ = 'N';
    if ((used == 0) && (OP == 1))
      *str++ = 'O';
    if ((used == 1) && (OP == 1))
      *str++ = 'U';
      if(i < SVtrack.N-1)
        *str++ = ',';
  }
  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';
  SatAtm->length = strlen(SatAtm->str);
}

//=================================================================================
/// @brief      Messages forming
///
/// @param[in]  pNavTask - pointer to Navigation Task Solution object
/// @param[in]  pSatMgr  - pointer to Satellite manager object
/// @param[in]  pOPMgr   - pointer to OP Manager object
/// @param[in]  Message  - message types
/// @param[in]  data     - pointer to data
/// @param[in]  pTerm    - pointer to Debug terminal
//=================================================================================
void MessagesForming(tNavTask          *pNavTask,
                     tSatelliteManager *pSatMgr,
                     tOPManager        *pOPMgr,
                     uint16             Message,
                     tICDdata          *data,
                     tTerminal         *pTerm)
{
  float64     tmp;         ///< temporary variable
  int16       NavSolTime;  ///< during of navigation task solution
  uint16      navCPU,      ///< CPU in % for navigation task solution
              trCPU;       ///< CPU in % for tracking thread
  uint32      PeriodTime;  ///< period time for navigation task posting

  if ((pNavTask->Synchronization == SYNC_PROCESS) && ((pNavTask->CountPostNavTask - pNavTask->CountSync) > PERIOD_SYNC))
    PeriodTime = PERIOD_SYNC;
  else
    PeriodTime = pNavTask->PeriodTime;

  /// calculate during of navigation task solution
  NavSolTime = (int16)(pSatMgr->ms_count - (uint32)(pNavTask->NavCond.t*1.e+3 + 0.5));
  /// calculate CPU in % for navigation task solution
  tmp = (float64)pNavTask->NavCond.timeTracking/(float64)PeriodTime;
  tmp = 1.0 - tmp;
  tmp = (tmp*100.0*(float64)NavSolTime)/(float64)PeriodTime;
  navCPU = (uint16)tmp;
  /// calculate CPU in % for tracking thread
  tmp = (100.0*(float64)pNavTask->NavCond.timeTracking)/(float64)PeriodTime;
  /// calculate all CPU in %
  trCPU = (uint16)tmp;
  pNavTask->NavCond.CPU = (trCPU + navCPU); 
 
  #ifdef SIMULATOR
  {
    int8 fNT_Condition = 0;
    int i;
    if ((pNavTask->NavSolution == 2) && (pNavTask->Synchronization == SYNC_OK))
      fNT_Condition = 1;
    if (pNavTask->User.SolutionFlag)
    {
      DEBUGprintf("navuser.dat", 
                  "%4d %12.6f %9.2f %9.2f %9.2f %7.3f %7.3f %7.3f %10.3lf %6.3lf %10.3lf %6.3lf %1d\n",
                  pNavTask->User.wn, (pNavTask->User.t - pNavTask->User.R*l_cLight),
                  pNavTask->User.ecef.X, pNavTask->User.ecef.Y, pNavTask->User.ecef.Z,
                  pNavTask->User.ecef.Xdot, pNavTask->User.ecef.Ydot, pNavTask->User.ecef.Zdot,
                  pNavTask->User.R, pNavTask->User.Rdot, pNavTask->CorrectTime.R, 
                  pNavTask->CorrectTime.Rdot,pNavTask->User.CoordValid);

      for(i=0;i<pNavTask->NavCond.NavSV.N;i++)
      {
        tNavTaskParams *navtask_params = SatMgr_GetNavTaskParams(pSatMgr, pNavTask->NavCond.NavSV.Cells[i]);
        if (navtask_params == NULL)
          continue;
        DEBUGprintf("navparams.dat","%9d, %1d %2d %6.2lf ",
           pSatMgr->ms_count,
           navtask_params->USED,
           navtask_params->sv_num,
           (navtask_params->sat_params.elev*rad_to_degree));
        DEBUGprintf("navparams.dat","%2d",navtask_params->SNR_log);
        DEBUGprintf("navparams.dat","\n");
      }
      DEBUGprintf("navparams.dat","----------------------------------------\n");
    }
      
    if (pOPMgr->fOP_Condition)
      DEBUGprintf("OPuser.dat", 
                  "%12.6f \t%10.3f %10.3f %10.3f   %10.6f %10.6f %10.6f  %10.6f %10.6f %1d\n",
                  pOPMgr->btLastUpdate.t, 
                  pOPMgr->X[I_X], pOPMgr->X[I_Y], pOPMgr->X[I_Z], 
                  pOPMgr->X[I_VX], pOPMgr->X[I_VY], pOPMgr->X[I_VZ],
                  pOPMgr->X[I_B], pOPMgr->X[I_D], pOPMgr->User_OP.fOP_Condition);
  }
  #endif
  /// String message by RS422
  if ((Message & SAT_MES) == SAT_MES)  /// SAT Message, bit 0
  {
    CreateSatString(&pNavTask->GSVStrings, pSatMgr);
    pTerm->vtable.pfnWrite(&pTerm->vtable, pNavTask->GSVStrings.str, pNavTask->GSVStrings.length);
  }
  if ((Message & SQE_MES) == SQE_MES)  /// SQE Message, bit 5
  {
    CreateSQEString(&pNavTask->GSVStrings, pSatMgr);
    pTerm->vtable.pfnWrite(&pTerm->vtable, pNavTask->GSVStrings.str, pNavTask->GSVStrings.length);
  }
  if ((Message & ATM_MES) == ATM_MES)  /// ATM Message, bit 6
  {
    CreateAtmString(&pNavTask->GSVStrings, pSatMgr);
    pTerm->vtable.pfnWrite(&pTerm->vtable, pNavTask->GSVStrings.str, pNavTask->GSVStrings.length);
  }
  /// messages forming: navigation and status; OP navigation and status; line of sight vectors
  if (pNavTask->NavSolution == 2)
  {
    pNavTask->User.User_fix.fNT_Condition = 1;
  }
  else
  {
    pNavTask->User.User_fix.fNT_Condition = 0;
    pNavTask->User.flag_bl = 1;  /// User structure blocking
    /// Convert double User data to integer format
    pNavTask->User.User_fix.wn     = pNavTask->NavCond.wn;
    pNavTask->User.User_fix.t      = (int64)((pNavTask->NavCond.t - pNavTask->CorrectTime.Rnc*l_cLight)*1.e+6 + 0.5);  /// s->mks
    pNavTask->User.User_fix.R      = (int64)(pNavTask->CorrectTime.Rnc*l_cLight*1.e+12);        /// 10^-12 sec
    pNavTask->User.User_fix.OscillOffset = (int32)(pNavTask->CorrectTime.Rdot*l_cLight*1.e+12);         /// 10^-6 ppm : (Rdot*f0_GPS*10^6)/(cLight*1575.42)
    pNavTask->User.User_fix.X      = 0;
    pNavTask->User.User_fix.Y      = 0;
    pNavTask->User.User_fix.Z      = 0;
    pNavTask->User.User_fix.Xdot   = 0;
    pNavTask->User.User_fix.Ydot   = 0;
    pNavTask->User.User_fix.Zdot   = 0;
    pNavTask->User.flag_bl = 0;  /// User structure unblocking
  }
  {
    /// wait unblocking data
    while (data->navData.flag_bl == 1);
    data->navData.flag_bl   = 1;  /// blocking data
    /// navigation and status data to ECEF, geographic system and OE
    MainMgr_NavAndStatusECEF(&data->navData.message.ecef, pNavTask->User, pNavTask->NavCond);
    MainMgr_NavAndStatusGEO(&data->navData.message.geo, pNavTask->User, pNavTask->NavCond);
    MainMgr_NavAndStatusOE(&data->navData.message.oe, pNavTask->User, pNavTask->NavCond);
    data->navData.flag_bl = 0;  /// unblocking data
    /// wait unblocking data
    while (data->navDataOP.flag_bl == 1);
    data->navDataOP.flag_bl   = 1;  /// blocking data
    /// OP navigation and status data to ECEF, geographic system and OE
    MainMgr_OPNavAndStatusECEF(&data->navDataOP.message.ecef, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
    MainMgr_OPNavAndStatusGEO(&data->navDataOP.message.geo, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
    MainMgr_OPNavAndStatusOE(&data->navDataOP.message.oe, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
    data->navDataOP.flag_bl = 0;  /// unblocking data
    /// wait unblocking data
    while (data->messLOS.flag_bl == 1);
    data->messLOS.flag_bl = 1;  /// blocking data
    MainMgr_LOS(&data->messLOS.message, pSatMgr->TopSystem.navtask_params, pNavTask->User.User_fix.wn, pNavTask->User.User_fix.t);
    data->messLOS.flag_bl = 0;  /// unblocking data
  } 
  if (pNavTask->NavSolution == 2)
  {
    /// string message by RS422 for 3D-mode of navigation task solution
    if ((Message & POS_MES) == POS_MES)  /// POS Message, bit 1
    {
      CreatePOSString(&pNavTask->GSVStrings, pNavTask->User.User_fix);
      pTerm->vtable.pfnWrite(&pTerm->vtable, pNavTask->GSVStrings.str, pNavTask->GSVStrings.length);
    }
    if ((Message & DOP_MES) == DOP_MES)  /// DOP Message, bit 2
    {
      CreateDOPString(&pNavTask->GSVStrings, pNavTask->User.User_fix);
      pTerm->vtable.pfnWrite(&pTerm->vtable, pNavTask->GSVStrings.str, pNavTask->GSVStrings.length);
    }
  }
  if (pOPMgr->fOP_Condition)
  {
    if ((Message & OPP_MES) == OPP_MES)  /// OPP Message, bit 4
    {
      CreateOPString(&pNavTask->GSVStrings, pOPMgr->User_OP);
      pTerm->vtable.pfnWrite(&pTerm->vtable, pNavTask->GSVStrings.str, pNavTask->GSVStrings.length);
    }
  }
}






