#ifndef _SAMPLE_MANAGER_H_
#define _SAMPLE_MANAGER_H_

#include "comtypes.h"

struct tagSampleManager;

//============================================================================================
///  Interface function for input samples preprocessing
///
/// @param[in] pSmpMgr - pointer to instance
/// @return    1 if samples are ready or 0 otherwise
//============================================================================================
EXTERN_C void SmpMgr_Process(struct tagSampleManager *pSmpMgr);

//============================================================================================
///  Interface function removes last epoch samples
///
/// @param[in] pSmpMgr - pointer to instance
//============================================================================================
EXTERN_C void SmpMgr_RemoveLastEpoch(struct tagSampleManager *pSmpMgr);

//============================================================================================
///  Interface function gets pointer to unpacked samples buffer
///
/// @param[in] pSmpMgr - pointer to samples buffer
//============================================================================================
EXTERN_C tComplex8Aligned *SmpMgr_GetEpoch(struct tagSampleManager *pSmpMgr);

//============================================================================================
///  Interface function starts fast search buffer forming
///
/// @param[in] pSmpMgr - pointer to samples buffer
//============================================================================================
EXTERN_C void SmpMgr_StartFastSearchBuf(struct tagSampleManager *pSmpMgr);

//============================================================================================
///  Interface function check if buffer for fast search is ready
///
/// @param[in] pSmpMgr - pointer to samples buffer
/// @return    1 if samples amount is greater than OUT_SAMPLES or 0 otherwise
//============================================================================================
EXTERN_C int  SmpMgr_IsFinishedFastSearchBuf(struct tagSampleManager *pSmpMgr);

//============================================================================================
///  Interface function gets pointer to buffer for fast search algorithm
///
/// @param[in] pSmpMgr - pointer to samples buffer
/// @return    pointer to samples buffer
//============================================================================================
EXTERN_C int32 *SmpMgr_GetPackedSignal(struct tagSampleManager *pSmpMgr);

//============================================================================================
///  Interface function checks if buffer for fast search is processed
///
/// @param[in] pSmpMgr - pointer to samples buffer
/// @return    fg_ON if buffer contains processed samples or fg_OFF otherwise
//============================================================================================
EXTERN_C eFlag SmpMgr_NeedStartFastSearchBuf(struct tagSampleManager *pSmpMgr);

#endif // _SAMPLE_MANAGER_H_
