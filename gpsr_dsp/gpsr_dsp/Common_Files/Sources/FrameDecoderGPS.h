/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Frame decoder
*    @file                   FrameDecoderGPS.h
*    @author                 A. Baloyan
*    @date                   25.06.2005
*    @version                1.0
*/
/*****************************************************************************/
#ifndef _FRAME_DECODER_GPS_H_
#define _FRAME_DECODER_GPS_H_

#include "DataForm.h"
#include "GPSChan.h"

#define MAX_TOC_TOE      (37799)       ///< toc and toe exceeds 604784 sec and scale factor 16
#define MAX_E            (257698038)   ///< eccentricity exceeds 0.03 and scale factor 2^-33
#define S_MS             (1000)        ///< s -> ms
#define ONE_BOUND_ROOTA  (2569011200)  ///< {roota} < 4900 m and scale factor 2^-19
#define TWO_BOUND_ROOTA  (2778726400)  ///< {roota} > 5300 m and scale factor 2^-19
#define MAX_TOT          (147)         ///< tot exceeds 602112 sec and scale factor 2^12
#define MAX_DAY          (7)           ///< day number
#define BIT_COUNT        (300)         ///< bit number in sub frame
#define BIT_PREAMBLE     (60)          ///< for preamble detected need 60 bit
#define FRAME_TIME_S     (6)           ///< sub frame time [s]
#define BIT_TIME_MS      (20)          ///< one bit time [ms]
#define HALF_WEEK (int32)(302400)      ///< seconds number in half week

/// Decoder structure
typedef struct
{
  int16 Nframe;         ///< bit counter
  int32 frame[10];      ///< buffer for bits
  int32 PreambleTime;   ///< preamble time [s]
  int16 wn;             ///< week number
  int32 tow;            ///< time of week [ms]
  eFlag FastPreamble;   ///< search preamble by first 60 bits
  eFlag CorrectTime;    ///< correct time flag
  eFlag Synchronized;   ///< synchronized flag
  RAW_EPH_GPS  RawEph;  ///< raw ephemeris structure
}
tDecoder;

/// Structure for temporary data from decoder
typedef struct
{
  TEMP_ION_UTC  TempIonUTC;                       ///< temporary buffer for ionosphere and UTC-GPS parameters
  TEMP_ALM_GPS  TempAlmGPS[GPS_SATELLITE_COUNT];  ///< temporary buffer for almanac
  RAW_ALM_GPS   RawAlmGPS[GPS_SATELLITE_COUNT];   ///< buffer for raw almanac
  TEMP_EPH_GPS  TempEph[GPS_SATELLITE_COUNT];     ///< temporary buffer for ephemeris
  int32         LastSubFrame;                     ///< last sub frame number
  tPreamble     preamble[GPS_SATELLITE_COUNT];    ///< preamble and preamble saving time
}
tDecoderCommon;

//============================================================================================
/// @brief      Decodes GPS sub frame
/// @param[in]  sv_num      - satellite number
/// @param[in]  bit         - bit value
/// @param[in]  common      - pointer to structure with temporary data
/// @param[in]  pDecoder    - pointer to Decoder
/// @param[in]  week        - week number
/// @param[in]  ms_count    - current time [ms]
/// @param[in]  WeekInit    - week is initialized
/// @param[in]  eph_set_flg - ephemeris availability flag
//============================================================================================
EXTERN_C void SatGPS_DecoderSubframe(int8            sv_num,
                                     eGPSChanBit     bit,
                                     tDecoderCommon *common,
                                     tDecoder       *pDecoder,
                                     const int16     week,
                                     const uint32    ms_count,
                                     eFlag           WeekInit,
                                     uint8           eph_set_flg);

//============================================================================================
/// @brief      returns decoder status
/// @return     Synchronized - eFlag 
/// @param[in]  pDecoder     - pointer to tDecoder structure
//============================================================================================
EXTERN_C eFlag SatGPS_GetDecoderStatus(tDecoder* pDecoder);

//============================================================================================
/// @brief      Converts truncated week number to untruncated one
/// @param[in]  wn  - current week number
/// @param[in]  wnt - truncated week number
/// @return     untruncated week number
//============================================================================================
EXTERN_C int16 ConvertTruncatedWeekNum(int16 wn, uint8 wnt);

//============================================================================================
/// @brief      Calculates the checksum for a words from sub frame
/// @param[in]  inWord - checked sub frame's word
/// @param[in]  D29pre - 29th bit of the previous word of sub frame
/// @param[in]  D30pre - 30th bit of the previous word of sub frame
/// @return     1 - checksum is OK, 0 - otherwise
//============================================================================================
EXTERN_C int32 CheckParityGPS(int32 inWord, int32 D29pre, int32 D30pre);

#endif //_FRAME_DECODER_GPS_H_
