/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        satellite manager
 *   @file         SatelliteManager.h
 *   @author       V. Yeremeyev, M. Zhokhova
 *   @date         09.09.2005
 *   @version      2.0
 */
/*****************************************************************************/

// @cond
#ifndef _SATELLITE_MANAGER_INCLUDED_
#define _SATELLITE_MANAGER_INCLUDED_
// @endcond

#include "satellite.h"
#include "Fsearch.h"

/// structure for time correction
typedef struct 
{
  float64 R_nts;             ///< clock bias from navigation task solution,  [m]
  float64 Rdot_nts;          ///< clock drift from navigation task solution, [m/s]
  float64 Rnc;               ///< clock bias saving during navigation conditions forming, [m]
  float64 R;                 ///< clock bias,  [m]
  float64 Rdot;              ///< clock drift, [m/s]
  float64 R_mes;             ///< clock bias for messages, [m]
  float64 Rnc_mes;           ///< clock bias for message saving during navigation conditions forming, [m]
  eFlag   FlagCorrectTime;   ///< correct time after navigation task solution finished
  uint8   flag_bl;           ///< blocking flag for R_nts, Rdot_nts, FlagCorrectTime
}
tCorrectTime;

typedef struct 
{
  float64 R_nts;             ///< clock bias from OP,  [m]
  float64 Rdot_nts;          ///< clock drift from OP, [m/s]
  float64 Rnc;               ///< clock bias saving during navigation conditions forming, [m]
  float64 R;                 ///< clock bias,  [m]
  float64 Rdot;              ///< clock drift, [m/s]
  eFlag   FlagCorrectTime;   ///< correct time after OP finished
  uint8   flag_bl;           ///< blocking flag for R_nts, Rdot_nts, FlagCorrectTime
}
tCorrectTimeOP;

/// structure for calculate satellite parameters by almanac
typedef struct
{
  eFlag  AlmCalc;                          ///< calculate satellite parameters flag
  int32  dopplerQ15[GPS_SATELLITE_COUNT];  ///< doppler, in Q15 [Hz]
  int16  nShift_add[GPS_SATELLITE_COUNT];  ///< additional nShift from doppler
  int32  sat_msk;                          ///< mask for satellite, if bit to 1 then doppler is valid
}
tSatAlm;

//=============================================================================
/// Structure top system parameters
//=============================================================================
typedef  struct 
{
  /// satellite navigation parameters
  tNavTaskParams  navtask_params[GPS_SAT_CHANNELS_COUNT];
  int8            usedSV[GPS_SATELLITE_COUNT];              ///< if field is equal 1 then satellites ID used in navigation task solution
  
  /// structure with raw or temporary data from sub frames
  tDecoderCommon DecCommon;                                 ///< common data

  /// structure with data from sub frames for navigation task solution
  ALM_GPS AlmGPS[GPS_SATELLITE_COUNT];                      ///< almanac for all satellites
  ION_UTC IonUTC;                                           ///< ionosphere and UTC-GPS parameters
  EPH_GPS EphGPS[GPS_SATELLITE_COUNT];                      ///< ephemeris for all satellites

  /// lists satellite with ephemeris and almanac
  uint32  eph_set_flgs;                                     ///< ephemeris validity flags
  uint32  alm_set_flgs;                                     ///< almanac validity flags
  uint32  health_flgs;                                      ///< health of satellite flags
  uint32  visibility_flgs;                                  ///< visibility of satellite flags

} tTopSystem;

/// search state for satellite
typedef enum
{
  fsm_WAIT,     ///< no search
  fsm_NORMAL,   ///< acquisition
  fsm_ALM,      ///< search with almanac
  fsm_REACQ,    ///< reacquisition
  fsm_TRACKING  ///< tracking, no search
}
eSearchState;

/// satellite information for search
typedef struct
{
  eSearchState state;       ///< satellite state
  int16        nShift_add;  ///< additional nShift from doppler
  int32        dopplerQ15;  ///< doppler for reacquisition
  int32        time_reacq;  ///< time for reacquisition
}
tInfoSat;

/// search mode
typedef enum
{
  sm_GPS,   ///< GPS satellite search
  sm_ALM,   ///< search GPS satellite by almanac
  sm_REACQ  ///< reacquisition
}
eSearchMode;

//======================================================================================
/// Satellite Manager structure
//======================================================================================
typedef struct
{
  int32           ms_count;                          ///< time of week, [ms]
  int16           wn;                                ///< GPS week number
  eFlag           WeekInit;                          ///< GPS week number initialized from message of Master
  
  int32           ms_count_OP;                       ///< time of week, [ms]
  int16           wn_OP;                             ///< GPS week number

  tSatelliteGPS   gps_satellites[GPS_SAT_CHANNELS_COUNT];  ///< channels for satellites
  
  eFlag           CorrTimeByPr;                      ///< 1 if correct time by preamble exist

#ifdef EXT_SRAM
  tFsearch        *fast_search;                       ///< fast search structure
#else
  tFsearch        fast_search;                       ///< fast search structure
#endif  
  tFSearchRes     fs_res[SEARCH_COUNT];              ///< results of fast search
  uint32          count_search;                      ///< counter for fast search
  eSearchMode     mode_fsearch;                      ///< fast search mode        
  LIST6           SVFSearch;                         ///< list of satellite in reacquisition mode
  tInfoSat        infoSat[GPS_SATELLITE_COUNT];      ///< satellite information
  int             sv_fsearch;                        ///< current GPS satellite number for fast search
  volatile eFlag  IsFSearchStarted;                  ///< fast search started flag
  volatile eFlag  IsFSearchFinished;                 ///< fast search finished flag
  tSatAlm         sat_alm;                           ///< satellite parameters for fast search by almanac

  tTopSystem      TopSystem;                         ///< top system structure

  int8            SVArray[GPS_SATELLITE_COUNT];      ///< GPS satellite number array

  int16           ms1_latch;                         ///< 1ms latch value
  int16           ms1000_latch;                      ///< 1s latch value

  tCoefPLLDLL     coefPllDll;                        ///< coefficients for PLL/DLL of GPS satellite
  
  eFlag           nPPSCorrect;                       ///< nPPS signal correct flag
  
  uint32          ms_fastStart;                      ///< for correct time by preamble
  eFlag           preambleStart;                     ///< set fg_ON if preamble of satellite decoded
  
} tSatelliteManager;

//========================================================================================
/// Satellite manager initialization
///
/// @param[in]  pSatMgr     - pointer to satellite manager instance
/// @param[in]  coefDLL     - PLL/DLL coefficients
/// @param[in]  timeDLL     - PLL and DLL predetection integration time
//========================================================================================
EXTERN_C void SatMgr_Init(tSatelliteManager *pSatMgr,
                          int32              coefDLL,
                          int8               timeDLL);

//========================================================================================
/// Remove satellite navigation task parameters
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
/// @param[in]  sv_num  - satellite ID
//========================================================================================
EXTERN_C void SatMgr_RemoveNavTaskParams(tSatelliteManager *pSatMgr, int8 sv_num);

//========================================================================================
/// Get pointer to navigation task parameters structure
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  sv_num    - GPS satellite number (1...32)
/// @return     pointer to navigation task parameters structure or NULL
//========================================================================================
EXTERN_C tNavTaskParams *SatMgr_GetNavTaskParams(tSatelliteManager *pSatMgr, int8 sv_num);

//========================================================================================
/// Get existing satellite GPS
///
/// @param[in]  pSatMgr - pointer to satellite manager instance
/// @param[in]  sv_num  - satellite ID
/// @return     Pointer to requested satellite or NULL if no satellite found
//========================================================================================
EXTERN_C tSatelliteGPS *SatMgr_GetSatellite(tSatelliteManager *pSatMgr, 
                                            int8               sv_num);

//========================================================================================
/// Search thread routine
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  pSamples  - pointer to samples buffer
//========================================================================================
EXTERN_C void SatMgr_FSearch(tSatelliteManager *pSatMgr, int32 *pSamples);

//========================================================================================
/// Determine if the new search can be started
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @return     search finished flag 
//========================================================================================
EXTERN_C eFlag SatMgr_CanStartFSearch(const tSatelliteManager *pSatMgr);

//========================================================================================
/// Main satellite manager processing routine for satellite search
///
/// @param[in]  pSatMgr       - pointer to satellite manager instance
//========================================================================================
EXTERN_C void SatMgr_SearchProcess(tSatelliteManager *pSatMgr);

//========================================================================================
/// Main satellite manager processing routine
///
/// @param[in]  pSatMgr       - pointer to satellite manager instance
/// @param[in]  pSamples      - pointer to samples buffer
/// @param[in]  sv_num        - satellite number
/// @param[in]  config        - configuration parameters structure
//========================================================================================
EXTERN_C void SatMgr_Process(tSatelliteManager   *pSatMgr,
                             tComplex8Aligned    *pSamples,
                             int8                 sv_num,
                             CONFIG_SETUP         config);

//========================================================================================
/// Interface control function for nPPS signal generator
///
/// @param[in]  pSatMgr   - pointer to satellite manager instance
/// @param[in]  User      - user parameters structure
/// @param[in]  Correct   - structure for time correction by navigation task solution result
/// @param[in]  User      - User parameters
//========================================================================================
EXTERN_C void SatMgr_nPPSControl(tSatelliteManager  *pSatMgr,
                                 NAV_CONDITIONS      NavCond,
                                 uint32              PeriodTime,
                                 tCorrectTime        Correct,
                                 USER                User);

//========================================================================================
/// Function for system time correction
///
/// @param[in]  pSatMgr           - pointer to satellite manager instance
/// @param[in]  CountPostNavTask  - pointer to navigation task solution counter
/// @param[in]  PeriodTime        - time slice for navigation task solution
/// @param[in]  pCorrectTime      - pointer to structure for time correction
/// @param[in]  pCorrectTimeOP    - pointer to structure for OP time correction
/// @param[in]  fOP_Condition     - OP condition
/// @param[in]  User              - User parameters
//========================================================================================
EXTERN_C void SatMgr_CorrectTime(tSatelliteManager  *pSatMgr,
                                 int32              *CountPostNavTask,
                                 uint32              PeriodTime,
                                 tCorrectTime       *pCorrectTime,
                                 tCorrectTimeOP     *pCorrectTimeOP,
                                 int8                fOP_Condition,
                                 USER                User);

//========================================================================================
/// Interface function increments system time
///
/// @param[in]  pSatMgr          - pointer to satellite manager instance
/// @param[in]  CountPostNavTask - pointer to navigation task solution counter
/// @param[in]  PeriodTime       - period of time for posting NavTaskThread
//==========================================================================================
EXTERN_C void SatMgr_IncrementTime(tSatelliteManager *pSatMgr,
                                   int32             *CountPostNavTask,
                                   uint32             PeriodTime);

//========================================================================================
///  Interface function for navigation conditions forming
///
/// @param[in]  pSatMgr          - pointer to satellite manager instance
/// @param[in]  config           - pointer to configuration setup structure
/// @param[in]  NavCond          - pointer to structure with navigation conditions
/// @param[in]  Synchronization  - synchronization OP and navigation task solution flag
/// @param[in]  CorrectTime      - pointer to structure for time correction
/// @param[in]  CorrectTimeOP    - pointer to structure for OP time correction
/// @param[in]  timeTrackingSWI  - time between function calls from SWITracking thread
//========================================================================================
EXTERN_C void SatMgr_NavCondForming(tSatelliteManager   *pSatMgr,
                                    CONFIG_SETUP        *config,
                                    NAV_CONDITIONS      *NavCond,
                                    uint8                Synchronization,
                                    tCorrectTime        *CorrectTime,
                                    tCorrectTimeOP      *CorrectTimeOP,
                                    float32              timeTrackingSWI);


//========================================================================================
///  Function for fast search using GPS almanac
///
/// @param[in]  pSatMgr  - pointer to satellite manager instance
//========================================================================================
EXTERN_C void SatMgr_AlmFastSearch(tSatelliteManager *pSatMgr);

//========================================================================================
///  Almanac and User parameters analyzing function
///
/// @param[in]  pSatMgr - pointer to instance
/// @param[in]  User    - pointer to User parameters structure
/// @param[in]  initMes - pointer to initial data
//========================================================================================
EXTERN_C void SatMgr_AnalyzeAlmanacUser(tSatelliteManager *pSatMgr, 
                                        USER              *User, 
                                        tInitMes          *initMes);

// @cond
#endif // !defined(_SATELLITE_MANAGER_INCLUDED_)
// @endcond
