/*****************************************************************************
*    Copyright (c) 2001-2002 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Multiply a 32 bit number and a 32 bit number
 *    @file                   L_mpy_ll.c
 *    @author                 A.Nazarov
 *    @version                1.0
 */
/*****************************************************************************/

// do not compile when it must be linked as a part of common SPCORP library
#ifndef _SPCORPLIB

#include "l_mpy_ll.h"

#ifndef _TMS320C54X_ENABLE

#include "FractOper.h"

//=========================================================================
/// @brief      Multiply a 32 bit number (L_var1) and a 32 bit number
///             (L_var2), and return a 32 bit result. 
///             Precision conversion: Q result = Q(x+y-31)
///
/// @implementation: Performs a 31x31 bit multiply, Complexity=24 Ops.
///                  Let x1x0, or y1y0, be the two constituent halves
///                  of a 32 bit number.  This function performs the
///                  following:
///                  low = ((x0 >> 1)*(y0 >> 1)) >> 16     (low * low)
///                  mid1 = [(x1 * (y0 >> 1)) >> 1 ]       (high * low)
///                  mid2 = [(y1 * (x0 >> 1)) >> 1]        (high * low)
///                  mid =  (mid1 + low + mid2) >> 14      (sum so far)
///                  output = (y1*x1) + mid                (high * high)
///
/// @param[in]  L_var1 - int32 multiplier
/// @param[in]  L_var2 - int32 multiplier
/// @return     int32 product
//=========================================================================
int32 L_mpy_llCversion(int32 L_var1, int32 L_var2)
{
  int16 swLow1,
         swLow2,
         swHigh1,
         swHigh2;
  int32  L_mid;

  swLow1 = (((int16)(L_var1))>>1)&0x7FFF;
  swLow2 = (((int16)(L_var2))>>1)&0x7FFF;
  swHigh1 = extract_h(L_var1);
  swHigh2 = extract_h(L_var2);



  L_mid = (L_mult(swHigh1, swLow2)>>1) +
      (L_mult(swHigh2, swLow1)>>1) +
      (L_mult(swLow1,  swLow2)>>16);
  L_mid = L_mid >> 14;
  L_mid += L_mult(swHigh1, swHigh2);

  return L_mid;
}

#endif

#endif  // _SPCORP
