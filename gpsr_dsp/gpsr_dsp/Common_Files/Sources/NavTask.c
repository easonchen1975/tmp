 /*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Navigation task
 *   @file                   NavTask.c
 *   @author                 A. Baloyan, M. Zhokhova
 *   @date                   02.12.2005
 *   @version                2.1
 */
/*****************************************************************************/

#include "NavTask.h"
#include "SVCordComp.h"
#include "MainManager.h"
#include "DataProtocol.h"
#include "DataConversion.h"
#include "Exp.h"
#include "L_sqrt_l.h"
#include "AtmosphericDelay.h"
#include <stdio.h>

#define VELOCITY_THRESH     (float64)(12000.0)    ///< User velocity threshold [m/s]
#define HEIGHT_THRESH       (float64)(2000000.0)  ///< User height threshold [m]
#define LOW_HEIGHT_THRESH   (float64)(-1000.0)   ///< User low height threshold [m]

//========================================================================
/// @brief     First navigation task solution
///
/// @param[in] User    - pointer to user parameters
/// @param[in] NavCond - pointer to navigation conditions
/// @param[in] pSatMgr - pointer to Satellite Manager object
/// @param[in] R_mes   - clock bias for message, [m]
//========================================================================
static void NavTask_FirstSolutionRAIM(USER              *User,
                                      NAV_CONDITIONS    *NavCond,
                                      tSatelliteManager *pSatMgr,
                                      float64            R_mes);

//========================================================================
/// @brief     Coordinates calculation
///
/// @param[in] User      - pointer to user parameters
/// @param[in] NavCond   - pointer to navigation conditions
/// @param[in] pSatMgr   - pointer to Satellite Manager object
/// @param[in] R_mes     - clock bias for message, [m]
/// @param[in] iteration - iterations number for navigation task
//========================================================================
static void CoordinatesCalculation(USER              *User,
                                   NAV_CONDITIONS    *NavCond,
                                   tSatelliteManager *pSatMgr,
                                   float64            R_mes,
                                   int8               iteration);

//========================================================================
/// @brief     Navigation task solution
///
/// @param[in] pNavTask - pointer to instance
/// @param[in] pSatMgr  - pointer to Satellite Manager object
//========================================================================
static void NavTaskSolution(tNavTask          *pNavTask,
                            tSatelliteManager *pSatMgr);

//========================================================================
/// @brief     Function for computation squares of
///            GDOP, PDOP, TDOP, VDOP and HDOP
///
/// @param[in] R    - pointer to instance
/// @param[in] User - pointer to instance
//========================================================================
static void DOP_Estimation(Matrix *R, USER *User);

//========================================================================
/// @brief      RAIM and FDI function
///
/// @param[in]  Q      - pointer to instance
/// @param[in]  R      - pointer to instance
/// @param[in]  Y      - pointer to instance
/// @param[in]  RAIMth - RAIM threshold
/// @return     fault satellite number
//========================================================================
static int32 RAIM(Matrix *Q, Matrix *R, Matrix *Y, int16 RAIMth);

//========================================================================================
///  @brief      Vectorial multiplication of vectors: a = b*c with following normalization
///
///  @param[in]  b - pointer to instance
///  @param[in]  c - pointer to instance
///  @param[out] a - pointer to instance
//========================================================================================
static void mvec(float64 *a, float64 *b, float64 *c);
/*
//========================================================================
/// @brief      Calculation of atmospheric delay for SV's signal
///             (see ICD-GPS-200C, par.20.3.3.5.2.6)
///
/// @param[in]  NP      - pointer to satellite navigation parameters
/// @param[in]  User    - pointer to user parameters
/// @param[in]  IONpar  - pointer to ionosphere parameters
/// @param[in]  GPStime - receiver computed system time
/// @param[out] t_ION   - ionospheric delay correction
/// @param[out] t_TRO   - tropospheric delay correction
//========================================================================
static void AtmosphereDelay(tNavTaskParams *NP,
                            USER           *User,
                            ION_UTC        *IONpar,
                            float64         GPStime,
                            float32        *t_ION,
                            float32        *t_TRO);
*/
//=================================================================================
/// @brief      Copy ephemeris from temporary buffer to buffer for navigation task
///
/// @param[in]  pTmpEphGPS - pointer to instance
/// @param[in]  pEphGPS    - pointer to GPS satellite almanac
//=================================================================================
static SaveEphemerisForNavTask(TEMP_EPH_GPS *pTmpEphGPS, EPH_GPS *pEphGPS);

//=================================================================================
/// @brief      Copy almanac from temporary buffer to buffer for navigation task
///
/// @param[in]  pTmpAlmGPS - pointer to instance
/// @param[in]  pAlmGPS    - pointer to almanac GPS satellite
//=================================================================================
static SaveAlmanacForNavTask(TEMP_ALM_GPS *pTmpAlmGPS, ALM_GPS *pAlmGPS);

//=================================================================================
/// @brief      Copy ionosphere and UTC parameters from temporary buffer to 
///             buffer for navigation task
///
/// @param[in]  pTmpIonUTC - pointer to instance
/// @param[in]  pION_UTC   - pointer to ionosphere and UTC parameters
//=================================================================================
static SaveIonUTCForNavTask(TEMP_ION_UTC *pTmpIonUTC, ION_UTC *pION_UTC);

//=================================================================================
/// @brief      Fix-point raw data convert floating-point format
///
/// @param[in]  raw_data_fix - pointer to fix-point raw data
/// @param[in]  raw_data     - pointer to floating-point raw data
//=================================================================================
static void NavTask_RawData(tRawDataFix *raw_data_fix,
                            tRawData    *raw_data);

//======================================================================================
/// @brief      Control satellite with ephemeris list
///
/// @param[in]  pSatMgr - pointer to Satellite manager object
/// @param[in]  sec     - GPS time of week, [s]
/// @param[in]  week    - GPS week number
//======================================================================================
static void ControlEphemeris(tSatelliteManager *pSatMgr, float64 sec, int16 week);

//====================================================================================
/// @brief      Control satellite with almanac list
///
/// @param[in]  pSatMgr - pointer to Satellite manager object
/// @param[in]  sec     - GPS time of week, [s]
/// @param[in]  week    - GPS week number
//====================================================================================
static void ControlAlmanac(tSatelliteManager *pSatMgr, float64 sec, int16 week);

//========================================================================
/// @brief     First navigation task solution
///
/// @param[in] User    - pointer to user object
/// @param[in] NavCond - pointer to navigation conditions
/// @param[in] pSatMgr - pointer to Satellite Manager object
/// @param[in] R_mes   - clock bias for message, [m]
//========================================================================
static void NavTask_FirstSolutionRAIM(USER              *User,
                                      NAV_CONDITIONS    *NavCond,
                                      tSatelliteManager *pSatMgr,
                                      float64            R_mes)
{
  float64           err,       ///< coordinates error
                    X,         ///< coordinate X in ECEF
                    Y,         ///< coordinate Y in ECEF
                    Z;         ///< coordinate Z in ECEF
  tNavTaskParams   *pTempSat;  ///< pointer to satellite navigation parameters
  int               i,
                    j;
  LIST12            svList;    ///< satellites list

  /// calculate coordinates
  CoordinatesCalculation(User, NavCond, pSatMgr, R_mes, ITERATIONS_START);
  /// check navigation task solution status
  if (User->CoordValid == NO_NAV_TASK)
    return;
  else  /// navigation task solution is exist
  {
    svList.N = 0;
    for(j=0;j<GPS_SATELLITE_COUNT;j++)
    {
      if (pSatMgr->TopSystem.usedSV[j] == 1)
      {
        svList.Cells[svList.N++] = (j+1);
      }
    }
    /// copy top system structure and coordinates
    X = User->ecef.X;
    Y = User->ecef.Y;
    Z = User->ecef.Z;
    /// search bad satellite
    for (i=1;i<=svList.N;i++)
    {
      for(j=0;j<svList.N;j++)
        pSatMgr->TopSystem.usedSV[svList.Cells[j]-1] = 1;
      /// delete current satellite from array
      pSatMgr->TopSystem.usedSV[svList.Cells[i-1]-1] = 0;
      memset(User, 0, sizeof(USER));
      /// calculate new coordinates
      CoordinatesCalculation(User, NavCond, pSatMgr, R_mes, ITERATIONS_START);
      /// calculate difference between old and current coordinates
      err = fabs(X - User->ecef.X) + fabs(Y - User->ecef.Y) + fabs(Z - User->ecef.Z);
      /// check difference
      if (err > (NavCond->UERE * NavCond->UERE * svList.N * THRESH_FIRST_NTS))
      {
        /// for exclude false alarm calculate coordinates once more
        CoordinatesCalculation(User, NavCond, pSatMgr, R_mes, ITERATIONS_START);
        /// calculate difference between old and current coordinates
        err = fabs(X - User->ecef.X) + fabs(Y - User->ecef.Y) + fabs(Z - User->ecef.Z);
        /// check difference
        if (err > sqrt(NavCond->UERE * NavCond->UERE * svList.N))
        {
          /// if error is occur then delete satellite from list and return from function
          pTempSat = SatMgr_GetNavTaskParams(pSatMgr, svList.Cells[i-1]);
          pTempSat->BAD_SAT = 1;
          return;
        }
      }
    }
    /// if bad satellite is not found then copy old satellites array and calculate coordinates once more
    for(j=0;j<svList.N;j++)
      pSatMgr->TopSystem.usedSV[svList.Cells[j]-1] = 1;
    CoordinatesCalculation(User, NavCond, pSatMgr, R_mes, ITERATIONS_START);
    return;
  }
}

//========================================================================
/// @brief     Coordinates calculation
///
/// @param[in] User      - pointer to user object
/// @param[in] NavCond   - pointer to navigation conditions
/// @param[in] pSatMgr   - pointer to Satellite Manager object
/// @param[in] R_mes     - clock bias for message, [m]
/// @param[in] iteration - iterations number for navigation task
//========================================================================
static void CoordinatesCalculation(USER              *User,
                                   NAV_CONDITIONS    *NavCond,
                                   tSatelliteManager *pSatMgr,
                                   float64            R_mes,
                                   int8               iteration)
{ 
  int32          NRow,                   ///< row number
                 cycle_count = 0,        ///< cycle counter
                 Nfault;                 ///< bad satellite
  float32        PR_corr = 0,            ///< pseudo range correction
                 dt_ION,                 ///< ionosphere correction
                 dt_TRO;                 ///< troposphere correction
  float64        dt,                     ///< delta time
                 CarrFreq,               ///< carrier frequency
                 delta = 1e10,           ///< auxiliary parameter for cycle
                 dx,                     ///< difference between User and satellite coordinate X
                 dy,                     ///< difference between User and satellite coordinate Y
                 dz,                     ///< difference between User and satellite coordinate Z
                 delay;                  ///< delay between User and satellite
  Matrix         H,                      ///< H matrix
                 PR,                     ///< pseudo range matrix
                 PV,                     ///< pseudo range rate matrix
                 Q,                      ///< Q matrix
                 R,                      ///< R matrix
                 dX,                     ///< coordinates and clock bias matrix
                 dV,                     ///< velocity and clock drift matrix 
                 TMP;                    ///< temporary matrix
  tNavTaskParams tmpNP,                  ///< temporary satellite navigation parameters
                *ptrNP,                  ///< pointer to satellite navigation parameters
                *ptrTmpNP;               ///< pointer to temporary satellite navigation parameters
  USER           UserTmp;                ///< temporary User parameters structure
  LIST12         SVList;                 ///< satellites used in navigation task solution
  
  /// initialize matrices
  DestroyMatrix(&H);
  DestroyMatrix(&PR);
  DestroyMatrix(&PV);
  DestroyMatrix(&Q);
  DestroyMatrix(&R);
  DestroyMatrix(&dX);
  DestroyMatrix(&dV);
  DestroyMatrix(&TMP);

  /// Copying of the user structure
  User->flag_bl = 1;  /// User parameters blocking
  UserTmp       = *User;
  User->flag_bl = 0;  /// User parameters unblocking

  SVList.N = 0;
  for(NRow=0; NRow < GPS_SATELLITE_COUNT; NRow++)
  {
    if (pSatMgr->TopSystem.usedSV[NRow] == 1)
      SVList.N++;
  }

  /// check satellites number
  if(SVList.N <= SAT_COUNT_NO_NT)
  {
    /// satellites is not enough for navigation task solution
    if ((User->SolutionFlag == 1) || (User->CoordValid != NO_NAV_TASK))
    {
      User->ecef.X     = 0;
      User->ecef.Y     = 0;
      User->ecef.Z     = 0;
      User->ecef.Xdot  = 0;
      User->ecef.Ydot  = 0;
      User->ecef.Zdot  = 0;
      NavCond->RAIMKey = 0;
    }
    User->SolutionFlag = 0;
    User->CoordValid   = NO_NAV_TASK;
    return;
  }

  /// Coordinates extrapolation
  {
    dt = NavCond->t - UserTmp.t;  /// time from last position computing

    if(dt > S_HALF_WEEK)
      dt -= S_WEEK;
    else if(dt < - S_HALF_WEEK)
      dt += S_WEEK;

    UserTmp.ecef.X  += UserTmp.ecef.Xdot  * dt;
    UserTmp.ecef.Y  += UserTmp.ecef.Ydot  * dt;
    UserTmp.ecef.Z  += UserTmp.ecef.Zdot  * dt;
    UserTmp.R       += UserTmp.Rdot  * dt;
  }

  /// Navigation task solution
  while((cycle_count++ < iteration) && (delta > THRESH_POS))  /// some iterations or while position correction exceeds 1 cm
  {
    SVList.N = 0;
    /// cycle for all SVs with computed coordinates and raw measurements 
    for(NRow = 0; NRow < GPS_SATELLITE_COUNT; NRow++)
    {
      if (pSatMgr->TopSystem.usedSV[NRow] == 1)
      {
        /// get pointer to satellite navigation parameters
        ptrNP = SatMgr_GetNavTaskParams(pSatMgr, (int8)(NRow+1));

        /// copy coordinates and velocities
        tmpNP.sat_coord.coord.x  = ptrNP->sat_coord.coord.x; 
        tmpNP.sat_coord.coord.y  = ptrNP->sat_coord.coord.y; 
        tmpNP.sat_coord.coord.z  = ptrNP->sat_coord.coord.z;
        tmpNP.sat_coord.coord.vx = ptrNP->sat_coord.coord.vx; 
        tmpNP.sat_coord.coord.vy = ptrNP->sat_coord.coord.vy; 
        tmpNP.sat_coord.coord.vz = ptrNP->sat_coord.coord.vz;

        /// Rough signal delay estimation
        dx = tmpNP.sat_coord.coord.x - UserTmp.ecef.X;
        dy = tmpNP.sat_coord.coord.y - UserTmp.ecef.Y; 
        dz = tmpNP.sat_coord.coord.z - UserTmp.ecef.Z;
        delay = l_cLight * sqrt(dx * dx + dy * dy + dz * dz);

        /// Earth rotation compensate
        Compensate_Rotation(&tmpNP.sat_coord.coord, &tmpNP.sat_coord.coord, delay);

        /// calculate satellite to User parameters
        SV_User_parameters(&tmpNP, &UserTmp, 1);    

        /// copy line of sight vector
        ptrNP->sat_coord.x_los = tmpNP.sat_coord.x_los;
        ptrNP->sat_coord.y_los = tmpNP.sat_coord.y_los;
        ptrNP->sat_coord.z_los = tmpNP.sat_coord.z_los;
      
        /// copy receiver to satellite range and radial velocity, elevation and azimuth angles
        ptrNP->range           = tmpNP.range; 
        ptrNP->Vrad            = tmpNP.Vrad;
        ptrNP->sat_params.elev = tmpNP.sat_params.elev;
        ptrNP->sat_params.azim = tmpNP.sat_params.azim;

        /// convert float data to integer data for messages
        ptrNP->sat_params.elev_i16 = (int16)(ptrNP->sat_params.elev*rad_to_degree*10.0);  /// radians->0.1 degrees
        ptrNP->sat_params.azim_i16 = (int16)(ptrNP->sat_params.azim*rad_to_degree*10.0);
     
        /// Direction cosines matrix filling
        ptrNP->Hmat[0]      = tmpNP.Hmat[0];
        ptrNP->Hmat[1]      = tmpNP.Hmat[1];
        ptrNP->Hmat[2]      = tmpNP.Hmat[2];

        /// Check elevation angle (if previous computed coordinates are valid)
        if((User->CoordValid != NO_NAV_TASK) && (tmpNP.sat_params.elev < NavCond->ElevMask))  /// satellite elevation is less then elevation mask
        { 
          ptrNP->ELEV_MSK          = 1;
          pSatMgr->TopSystem.usedSV[NRow] = 0;
          continue;
        }
        SVList.Cells[SVList.N++] = (NRow+1);
      }  /// if (pSatMgr->TopSystem.usedSV[NRow] == 1)
    }    /// end of for(NRow=0; NRow<TopSys->UsedSV.N; NRow++)

    /// Check of availability of necessary satellites quantity
    if(SVList.N <= SAT_COUNT_NO_NT)
    { 
      /// Navigation task isn't solved now
      UserTmp.CoordValid = NO_NAV_TASK;
      break;  /// of while(cycle_count++<iteration && delta>THRESH_POS);
    }

    /// Creation of matrices that is necessary for navigation task solution
    UserTmp.CoordValid = OK_NAV_TASK;
    CreateMatrix(&H, SVList.N, 4);

    /// creation of matrices for pseudo range, pseudo range rate and weights
    CreateMatrix(&PR, SVList.N, 1);
    CreateMatrix(&PV, SVList.N, 1);

    /// cycle for all satellites which can be used for coordinates computing
    for(NRow = 0; NRow < SVList.N; NRow++)
    {
      ptrNP = SatMgr_GetNavTaskParams(pSatMgr, (int8)SVList.Cells[NRow]);

      /// this satellite is used in navigation task solution
      ptrNP->USED = 1;

      /// Signal carrier frequency
      CarrFreq = f0_GPS;

      if(UserTmp.CoordValid == OK_NAV_TASK)
      {
        /// Insert element in the direction cosines matrix
        H.Matr[NRow][3] = 1;
      }

      /// Measurements matrices filling
      PR.Matr[NRow][0] = ptrNP->raw_data.ps_delay * cLight;        /// Pseudo range
      PV.Matr[NRow][0] = ptrNP->raw_data.ps_freq*cLight/CarrFreq;  /// Pseudo range rate

      /// Direction cosines matrix filling
      H.Matr[NRow][0] = ptrNP->Hmat[0];
      H.Matr[NRow][1] = ptrNP->Hmat[1];
      H.Matr[NRow][2] = ptrNP->Hmat[2];

      #ifndef SIMULATOR
      /*if (NavCond->IonTropo == 1)
      {*/
        /// Computing of signal delay in atmosphere
        AtmosphereDelay(ptrNP, &UserTmp, &pSatMgr->TopSystem.IonUTC, UserTmp.t,
                        NavCond->IonosphereModel, NavCond->TroposphereModel,
                        &dt_ION, &dt_TRO);
        /// pseudo range correction by atmosphere model
        PR_corr                     = (float32)((dt_ION + dt_TRO) * cLight);
        ptrNP->sat_coord.dt_ION    = dt_ION * cLight;
        ptrNP->sat_coord.dt_TRO    = dt_TRO * cLight;
        ptrNP->sat_coord.dt_IONQ17 = (int32)ldexp((double)(dt_ION * cLight), 17);  /// in Q17
        ptrNP->sat_coord.dt_TROQ17 = (int32)ldexp((double)(dt_TRO * cLight), 17);  /// in Q17
/*      }
      else
      {
        ptrNP->sat_coord.dt_ION    = 0.0;
        ptrNP->sat_coord.dt_TRO    = 0.0;
        ptrNP->sat_coord.dt_IONQ17 = 0;  /// in Q17
        ptrNP->sat_coord.dt_TROQ17 = 0;  /// in Q17
        PR_corr = 0.0;
      }*/
      #else  /// for PC model
        PR_corr = 0.0;
      #endif

      /// pseudo range correction
      PR.Matr[NRow][0] -= ptrNP->range + UserTmp.R + PR_corr - ptrNP->sat_coord.coord.dt_SV * cLight;

      /// pseudo range rate correction
      PV.Matr[NRow][0] -= ptrNP->Vrad + UserTmp.Rdot;
      PV.Matr[NRow][0] += pSatMgr->TopSystem.EphGPS[ptrNP->sv_num-1].af1 * cLight 
      + pSatMgr->TopSystem.EphGPS[ptrNP->sv_num-1].af2 * (ptrNP->raw_data.timeCoord - pSatMgr->TopSystem.EphGPS[ptrNP->sv_num-1].toc) * cLight;

    }    /// end of for(NRow=0; NRow<TopSys->UsedSV.N; NRow++)
    
    /// Calculation of QR-decomposition of the direction cosines matrix
    QR_Decomposition(&H, &Q, &R);
   
    /// RAIM : if RAIM option is ON and satellites quantity is enough for RAIM and FDI
    if((NavCond->RAIMKey == 1) && (H.mRaw > SAT_COUNT_RAIM))
    {
      Nfault = RAIM(&Q, &R, &PR, NavCond->UERE);

      if(Nfault == H.mRaw)  /// Two or more fault satellites are detected
      {
        UserTmp.CoordValid = RAIM_ERROR;  /// Navigation task isn't solved now
        break;
      }
      /// Fault satellite is detected then delete one from navigation task solution
      if(Nfault != -1)
      {
        /// raise bad satellite flag
        ptrTmpNP = SatMgr_GetNavTaskParams(pSatMgr, SVList.Cells[Nfault]);
        ptrTmpNP->BAD_SAT = 1;
        /// Deletion of fault satellite ID from used satellites array
        pSatMgr->TopSystem.usedSV[SVList.Cells[Nfault]-1] = 0;
        Nfault++;
        DelRowMatrix(&H,  &H,  Nfault, 1);    /// Deletion of row with satellite direction cosines
        DelRowMatrix(&PR, &PR, Nfault, 1);    /// Deletion of satellite pseudo range measurement
        DelRowMatrix(&PV, &PV, Nfault, 1);    /// Deletion of satellite pseudo range rate measurement
        
        /// Calculation of QR-decomposition of the new direction cosines matrix
        QR_Decomposition(&H, &Q, &R);

      }  /// end of if(Nfault != -1)
    }    /// end of if((NavCond->RAIMKey == 1) && (H.mRaw > SAT_COUNT_RAIM) && (NavCond->DiffModeKey != DIF_ROVER))

    /// DOP estimation
    if((User->CoordValid != NO_NAV_TASK) && (R.mRaw > SAT_COUNT_DOP))
    {
      DOP_Estimation(&R, &UserTmp);
    }
    /// DOP is too large
    if(UserTmp.HDOP > NavCond->DOP)
    { 
      UserTmp.CoordValid   = ERROR_NAV_TASK;  /// User coordinates is not calculated now
      UserTmp.SolutionFlag = 1;
      break;
    }
    /// Correction term determination
    MultMatrixT_(&dX, &Q, &PR);
    MultMatrixT_(&dV, &Q, &PV);

    UppTriSolution(&dX, &R, &dX); 
    UppTriSolution(&dV, &R, &dV);

    /// User coordinates and velocity correction
    if (UserTmp.CoordValid == OK_NAV_TASK)
    { 
      UserTmp.ecef.X    += dX.Matr[0][0];
      UserTmp.ecef.Y    += dX.Matr[1][0];
      UserTmp.ecef.Z    += dX.Matr[2][0];
      UserTmp.ecef.Xdot += dV.Matr[0][0];
      UserTmp.ecef.Ydot += dV.Matr[1][0];
      UserTmp.ecef.Zdot += dV.Matr[2][0];

      if(dX.mRaw > SAT_COUNT_NO_NT)
      {
        /// Clock shift and bias correction
        UserTmp.R    += dX.Matr[3][0];
        UserTmp.Rdot += dV.Matr[3][0];
      }
      /// auxiliary parameter for while cycle break condition
      delta = sqrt(dX.Matr[0][0] * dX.Matr[0][0] + dX.Matr[1][0] * dX.Matr[1][0] + dX.Matr[2][0] * dX.Matr[2][0]);
    }
  }  /// end of while(cycle_count++<iteration && fabs(delta)>THRESH_POS)

  /// Auxiliary matrices destroying
  DestroyMatrix(&H);
  DestroyMatrix(&PR);
  DestroyMatrix(&PV);
  DestroyMatrix(&Q);
  DestroyMatrix(&R);
  DestroyMatrix(&dX);
  DestroyMatrix(&dV);

  /// check navigation task solution status
  if (UserTmp.CoordValid == OK_NAV_TASK)
  {
    float64 Velocity;

    /// Calculation of the local coordinate system (TOPO)
    ComputeTOPO(&UserTmp);

    UserTmp.SolutionFlag = 1;

    /// ECEF to geographic position, velocity and their quality estimations
    ECEF_to_Geoid(&UserTmp.geo, &UserTmp.ecef, UserTmp.TOPO);

    Velocity = sqrt(UserTmp.ecef.Xdot * UserTmp.ecef.Xdot + 
                    UserTmp.ecef.Ydot * UserTmp.ecef.Ydot + 
                    UserTmp.ecef.Zdot * UserTmp.ecef.Zdot);

    /// check height and velocity
    if((fabs(UserTmp.geo.H) > HEIGHT_THRESH) || (Velocity > VELOCITY_THRESH))
      UserTmp.CoordValid = ERROR_NAV_TASK;
    if ( UserTmp.geo.H < LOW_HEIGHT_THRESH)
      UserTmp.CoordValid = ERROR_NAV_TASK;
  }

  /// Coordinates validness checking
  if (UserTmp.CoordValid == OK_NAV_TASK)
  {
    /// Copying of the measurements time to temporary user structure
    UserTmp.t  = NavCond->t;
    UserTmp.wn = NavCond->wn;
    
    /// Copying of NavTask solution results to User parameters structure
    User->flag_bl = 1;  /// User structure blocking
    *User = UserTmp;
    /// Convert double User data to integer format
    User->User_fix.wn     = User->wn;
    User->User_fix.t      = (int64)((User->t - User->R*l_cLight)*1.e+6 + 0.5);  /// s->mks
    User->User_fix.X      = (int64)(User->ecef.X*1.e+3);                        /// m->mm
    User->User_fix.Y      = (int64)(User->ecef.Y*1.e+3);
    User->User_fix.Z      = (int64)(User->ecef.Z*1.e+3);
    User->User_fix.Xdot   = (int32)(User->ecef.Xdot*1.e+3);                     /// m/s->mm/s
    User->User_fix.Ydot   = (int32)(User->ecef.Ydot*1.e+3);
    User->User_fix.Zdot   = (int32)(User->ecef.Zdot*1.e+3);
    User->User_fix.Lat    = (int64)(ldexp(User->geo.Lat, 48));                  /// 2^48 rad.
    User->User_fix.Lon    = (int64)(ldexp(User->geo.Lon, 48));
    User->User_fix.H      = (int64)(User->geo.H*1.e+3);                         /// m->mm
    User->User_fix.Ndot   = (int32)(User->geo.Ndot*1.e+3);                      /// m/s->mm/s
    User->User_fix.Edot   = (int32)(User->geo.Edot*1.e+3);
    User->User_fix.Udot   = (int32)(User->geo.Udot*1.e+3);
    User->User_fix.A      = (int64)(User->oe.A*1.e+3);                          /// m -> mm
    User->User_fix.Omega  = (int64)(ldexp(User->oe.Omega, 48));                 /// 2^48 rad.
    User->User_fix.M      = (int64)(ldexp(User->oe.M, 48));                     /// 2^48 rad.
    User->User_fix.I      = (int32)(ldexp(User->oe.I, 29));                     /// 2^29 rad.
    User->User_fix.OmegaP = (int32)(ldexp(User->oe.OmegaP, 29));               /// 2^29 rad.
    User->User_fix.Ecc    = (int32)(ldexp(User->oe.Ecc, 31));                  /// 2^31
    User->User_fix.R      = (int64)((R_mes + User->R)*l_cLight*1.e+12);        /// 10^-12 sec
    User->User_fix.OscillOffset = (int32)(User->Rdot*l_cLight*1.e+12);         /// 10^-6 ppm : (Rdot*f0_GPS*10^6)/(cLight*1575.42)

// Alex, 20141114, to handle overflow	
#if 1
{
	uint32 tmp;
	
	tmp = (int32)(User->GDOP*100.);
	if ( tmp >= 0x7fff )	// 32767
		tmp = 0x7fff ;
		
    User->User_fix.GDOP   = (int16)tmp;
}	
#else
    User->User_fix.GDOP   = (int16)(User->GDOP*100.);                           /// 1->0.01
#endif
	
// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC
{
	uint32 tmp;
	
	tmp = (int32)(User->PDOP*100.);
	if ( tmp >= 0x7fff )	// 32767
		tmp = 0x7fff ;
		
    User->User_fix.PDOP   = (int16)tmp;
}
#else
    User->User_fix.PDOP   = (int16)(User->PDOP*100.);
#endif	// end NEW_TMTC

    User->User_fix.HDOP   = (int16)(User->HDOP*100.);
    User->User_fix.TDOP   = (int16)(User->TDOP*100.);
    User->User_fix.VDOP   = (int16)(User->VDOP*100.);
    User->flag_bl = 0;  /// User structure unblocking
  }
  else
  {
    User->flag_bl      = 1;  /// User structure blocking
    if ((User->SolutionFlag == 1) || (User->CoordValid != NO_NAV_TASK))
    {
      User->ecef.X     = 0;
      User->ecef.Y     = 0;
      User->ecef.Z     = 0;
      User->ecef.Xdot  = 0;
      User->ecef.Ydot  = 0;
      User->ecef.Zdot  = 0;
      NavCond->RAIMKey = 0;
    }
    User->CoordValid   = UserTmp.CoordValid;
    User->SolutionFlag = UserTmp.SolutionFlag;
    User->flag_bl      = 0;  /// User structure unblocking
  }   
  return;
}

//========================================================================
/// @brief     Navigation task solution
///
/// @param[in] pNavTask - pointer to instance
/// @param[in] pSatMgr  - pointer to Satellite Manager object
//========================================================================
static void NavTaskSolution(tNavTask          *pNavTask,
                            tSatelliteManager *pSatMgr)
{ 
  int32           NRow,       ///< row number
                  i;
  float64         dt;         ///< delta time
  LIST12          SVList;     ///< auxiliary list for satellites numbers storage 
  EPH_GPS        *EphPtrGPS;  ///< pointer to ephemeris
  tNavTaskParams *SatPtr;     ///< pointer to satellite navigation parameters
  tNavTaskParams  AlmPtr;     ///< pointer to temporary satellite navigation parameters

  /// executable code start
  SVList.N = 0;

  for(NRow=0; NRow<GPS_SATELLITE_COUNT; NRow++)
    pSatMgr->TopSystem.usedSV[NRow] = 0;

  /// Cycle for all tracked satellite (satellite coordinates calculation and satellites list filling)
  for(NRow = 0; NRow < pNavTask->NavCond.NavSV.N; NRow++)
  { 
    SatPtr = SatMgr_GetNavTaskParams(pSatMgr, pNavTask->NavCond.NavSV.Cells[NRow]);
    /// raw measurements are good
    if(SatPtr->raw_data.attempt == 1)
    { 
      /// check ephemeris available, health satellite and accuracy of ephemeris
      if (((pSatMgr->TopSystem.eph_set_flgs&(0x1<<(SatPtr->sv_num-1))) == (0x1<<(SatPtr->sv_num-1))) && 
          (pSatMgr->TopSystem.EphGPS[SatPtr->sv_num-1].health   == 0) && 
          (pSatMgr->TopSystem.EphGPS[SatPtr->sv_num-1].accuracy <  5))
      {
        /// calculate age ephemeris
        EphPtrGPS = &pSatMgr->TopSystem.EphGPS[SatPtr->sv_num-1];
        if ((pNavTask->NavCond.wn == (WEEK-1)) && (EphPtrGPS->wn == 0))
          dt = fabs((pNavTask->NavCond.wn - EphPtrGPS->wn - WEEK) * S_WEEK + pNavTask->NavCond.t - EphPtrGPS->toe);
        else if ((pNavTask->NavCond.wn == 0) && (EphPtrGPS->wn == (WEEK-1)))
          dt = fabs((pNavTask->NavCond.wn - EphPtrGPS->wn + WEEK) * S_WEEK + pNavTask->NavCond.t - EphPtrGPS->toe);
        else
          dt = fabs((pNavTask->NavCond.wn - EphPtrGPS->wn) * S_WEEK + pNavTask->NavCond.t - EphPtrGPS->toe);
      
        if(dt > GOOD_EPH_TIME)
        {
          continue;
        }

        /// ephemeris parameters blocking
        EphPtrGPS->flag_bl = 1;  

        /// satellite coordinate computation by ephemeris
        SatGPS_EphCoordCalc(&SatPtr->sat_coord.coord, &pSatMgr->TopSystem.EphGPS[SatPtr->sv_num-1], SatPtr->raw_data.timeCoord);
        if (SatPtr->paramsOP.raw_data.attempt == 1)
        {
          SatGPS_EphCoordCalc(&SatPtr->paramsOP.coord, &pSatMgr->TopSystem.EphGPS[SatPtr->sv_num-1], SatPtr->paramsOP.raw_data.timeCoord);
          SatPtr->paramsOP.COORDINATES = 1;
        }

        /// ephemeris parameters unblocking
        EphPtrGPS->flag_bl = 0;
        
        if (SatPtr->sv_num < 33)
        {
        /// check validity ephemeris by almanac
        if ((pSatMgr->TopSystem.alm_set_flgs&(0x1<<(SatPtr->sv_num-1))) == (0x1<<(SatPtr->sv_num-1)))
        {
          /// calculate age almanac
          if ((pNavTask->NavCond.wn == (WEEK-1)) && (pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num-1].wna == 0))
            dt = fabs((pNavTask->NavCond.wn - pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num-1].wna - WEEK) * S_WEEK 
                      + pNavTask->NavCond.t - pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num - 1].toa);
          else if ((pNavTask->NavCond.wn == 0) && (pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num-1].wna == (WEEK-1)))
            dt = fabs((pNavTask->NavCond.wn - pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num-1].wna + WEEK) * S_WEEK 
                      + pNavTask->NavCond.t - pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num - 1].toa);
          else
            dt = fabs((pNavTask->NavCond.wn - pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num-1].wna) * S_WEEK 
                      + pNavTask->NavCond.t - pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num - 1].toa);

          if(dt < GOOD_ALM_FOR_EPH)
          {
            memset(&AlmPtr, 0, sizeof(tNavTaskParams));
            /// satellite coordinate computation by almanac
            SV_Alm_GPS(&AlmPtr, &pSatMgr->TopSystem.AlmGPS[SatPtr->sv_num-1], pNavTask->NavCond.wn, pNavTask->NavCond.t);

            /// compare coordinates by almanac and by ephemeris
            if (   (fabs(SatPtr->sat_coord.coord.x - AlmPtr.sat_coord.coord.x) > 500000)
                || (fabs(SatPtr->sat_coord.coord.y - AlmPtr.sat_coord.coord.y) > 500000)
                || (fabs(SatPtr->sat_coord.coord.z - AlmPtr.sat_coord.coord.z) > 500000))
            {
              /// clear ephemeris available flag and raise bad satellite flag
              pSatMgr->TopSystem.eph_set_flgs &= (0xFFFFFFFF - (0x1<<(SatPtr->sv_num-1)));
              SatPtr->BAD_SAT              = 1;
              SatPtr->paramsOP.COORDINATES = 0;
              continue;
            }
          }
        }
        }
      
        /// add satellite to satellites list for navigation task solution
        SVList.Cells[SVList.N++] = SatPtr->sv_num;
        pNavTask->NavCond.NavSVOld.Cells[pNavTask->NavCond.NavSVOld.N++] = SatPtr->sv_num;
        
      }  /// if ((TopSys->eph_set_flg[SatPtr->sv_num-1] == 1) && (TopSys->EphGPS[SatPtr->sv_num-1].health == 0) && (TopSys->EphGPS[SatPtr->sv_num-1].accuracy < 5))
    }    /// if(SatPtr->raw_data.attempt == 1)
  }      /// for(NRow=0; NRow<TopSys->SVTrack.N; NRow++)
  
  /// Used satellites list copying
  for(i = 0; i < SVList.N; i++) 
  {
    pSatMgr->TopSystem.usedSV[SVList.Cells[i]-1] = 1;
  }

  /// User Coordinates calculation
  if (pNavTask->FirstSolution == fg_OFF)  /// first navigation task solution
  {
    /// RAIM for first navigation task solution call only if satellites number more then 5
    if (SVList.N > SAT_COUNT_RAIM)
    {
      NavTask_FirstSolutionRAIM(&pNavTask->User, &pNavTask->NavCond, pSatMgr, pNavTask->CorrectTime.Rnc_mes);
      /// first navigation task solved
      if (pNavTask->User.CoordValid == OK_NAV_TASK)
        pNavTask->FirstSolution = fg_ON;
    }
    else  /// without RAIM
    {
      CoordinatesCalculation(&pNavTask->User, &pNavTask->NavCond, pSatMgr, 
                             pNavTask->CorrectTime.Rnc_mes, ITERATIONS_START);
      /// first navigation task solved
      if (pNavTask->User.CoordValid == OK_NAV_TASK)
      {
        CoordinatesCalculation(&pNavTask->User, &pNavTask->NavCond, pSatMgr, 
                               pNavTask->CorrectTime.Rnc_mes, ITERATIONS);
        if (pNavTask->User.CoordValid == OK_NAV_TASK)
          pNavTask->FirstSolution = fg_ON;
      }
    }
  }
  else  /// next navigation task solution
  {
    /// User Coordinates calculation
    if (pNavTask->User.CoordValid != OK_NAV_TASK)
    {
      CoordinatesCalculation(&pNavTask->User, &pNavTask->NavCond, pSatMgr, 
                             pNavTask->CorrectTime.Rnc_mes, ITERATIONS_START);
      if (pNavTask->User.CoordValid == OK_NAV_TASK)
      {
        pNavTask->NavCond.RAIMKey = 1;
        CoordinatesCalculation(&pNavTask->User, &pNavTask->NavCond, pSatMgr, 
                               pNavTask->CorrectTime.Rnc_mes, ITERATIONS);
      }
    }
    else
      CoordinatesCalculation(&pNavTask->User, &pNavTask->NavCond, pSatMgr, 
                             pNavTask->CorrectTime.Rnc_mes, ITERATIONS);
  }  /// if !(*FlagFirstSolution == fg_OFF)

  return;
}

//========================================================================
/// @brief     Function for computation squares of
///            GDOP, PDOP, TDOP, VDOP and HDOP
///
/// @param[in] R    - pointer to instance
/// @param[in] User - pointer to instance
//========================================================================
static void DOP_Estimation(Matrix *R, USER *User)
{
  float64 temp,      ///< temporary variable
          tmpa,      ///< temporary accumulator
          res[5],    ///< result vectors
          Xuser[3];  ///< User coordinates
  Matrix  RI;        ///< auxiliary matrix
  int32   i, 
          j;

  DestroyMatrix(&RI);

  User->flag_bl = 1;  /// User parameters blocking

  /// copy User coordinates
  Xuser[0] = User->ecef.X; 
  Xuser[1] = User->ecef.Y; 
  Xuser[2] = User->ecef.Z;

  /// calculate DOPS for Inv(T(H)*H) = Inv(R)*T(Inv(R))
  UnitaryMatrix(&RI,R->mRaw);
  UppTriSolModif(&RI,R,&RI);
  
  /// calculate PDOP
  for(i = 0, User->PDOP = 0.0; i < 3; i++)
  {
    for (j=i; j<R->mRaw; j++) 
    {
      User->PDOP += RI.Matr[i][j] * RI.Matr[i][j];
    }
  }
  /// calculate TDOP
  if(R->mRaw > 3)
  {
    User->TDOP = (R->mRaw == 4) ? RI.Matr[3][3] * RI.Matr[3][3]
                           :      RI.Matr[3][3] * RI.Matr[3][3] + RI.Matr[4][4] * RI.Matr[4][4];
  } 
  else 
  {
    User->TDOP = 0.0;
  }
  /// calculate GDOP
  User->GDOP = User->PDOP + User->TDOP;

  /// calculate DOP in the height direction
  for(i = 0; i < R->mRaw; i++)
  {
    for(j = 0, res[i] = 0.0; j <= i && j < 3; j++) 
    {
      res[i] += RI.Matr[j][i] * Xuser[j];
    }
  }
  for(i = 0, temp = 0.0, tmpa = 0.0; i < 3; i++)
  { 
    temp += Xuser[i] * Xuser[i];
      tmpa += res[i] * res[i];
  }
  for(i = 3; i < R->mRaw; i++) 
  {
    tmpa += res[i] * res[i];
  }
  DestroyMatrix(&RI);

  /// resulting DOP:
  User->VDOP = tmpa / temp;
  User->HDOP = User->PDOP - User->VDOP;
  User->GDOP = sqrt(User->GDOP);
  User->PDOP = sqrt(User->PDOP);
  User->TDOP = sqrt(User->TDOP);
  User->HDOP = sqrt(User->HDOP);
  User->VDOP = sqrt(User->VDOP);
  User->flag_bl = 0;  /// User parameters unblocking
  
  return;
}

//========================================================================
/// @brief      RAIM and FDI function
///
/// @param[in]  Q      - pointer to instance
/// @param[in]  R      - pointer to instance
/// @param[in]  Y      - pointer to instance
/// @param[in]  RAIMth - RAIM threshold
/// @return     fault satellite number
//========================================================================
static int32 RAIM(Matrix *Q, Matrix *R, Matrix *Y, int16 RAIMth)
{ 
  int32   i, 
          NSat = Q->mRaw,        ///< satellites number
          Nfault = -1;           ///< returns value
  float64 Res2,                  ///< result
          Threshold,             ///< threshold
          ResTmp,                ///< resolution threshold
          RAIM_AlarmLevel = 1.;  ///< alarm level
  Matrix  dY,                    ///< residual vector
          Hold,                  ///< old directional cosines matrix
          Htmp,                  ///< temporary directional cosines matrix
          Ytmp,                  ///< temporary residual vector
          Qtmp,                  ///< temporary Q-matrix
          Rtmp;                  ///< temporary R-matrix

  DestroyMatrix(&dY);
  DestroyMatrix(&Hold);
  DestroyMatrix(&Htmp);
  DestroyMatrix(&Ytmp);
  DestroyMatrix(&Qtmp);
  DestroyMatrix(&Rtmp);

  /// calculate the residual vector
  MultMatrixT_(&dY, Q, Y);
  MultMatrix(&dY, Q, &dY);

  Sum_Matrix(&dY, &dY, Y);
  Res2 = Mod(&dY);
  
  /// define the normal noise level
  Threshold = RAIMth * RAIMth * NSat;
  
  /// set False Alarm Level
  Threshold *= RAIM_AlarmLevel;

  if(Res2 > Threshold)
  { 
    /// FDI part of the Algorithm
    if(NSat < SAT_COUNT_FDI)
    {
      return NSat;  /// impossible to isolate fault
    }
    else
    {
      /// old directional cosines matrix
      MultMatrix(&Hold, Q, R);

      ResTmp = Res2;

      for(i = 0; i < NSat; i++)
      { 
        /// delete i-th satellite from navigation task solution
        DelRowMatrix(&Htmp, &Hold, i + 1, 1);
        DelRowMatrix(&Ytmp, Y, i + 1, 1);
        
        /// QR decomposition of the matrix Htmp
        QR_Decomposition(&Htmp, &Qtmp, &Rtmp);
        
        /// calculate the residual vector
        MultMatrixT_(&dY, &Qtmp, &Ytmp);
        MultMatrix(&dY, &Qtmp, &dY);
        
        Sum_Matrix(&dY, &dY, &Ytmp);
        ResTmp = Mod(&dY);
        
        /// compare the residual with one for full constellation
        if(ResTmp < Res2) 
        { 
          Res2 = ResTmp;
          Nfault = i;
        }
      }

      /// auxiliary matrix destroying
      DestroyMatrix(&Hold);
      DestroyMatrix(&Htmp); 
      DestroyMatrix(&Ytmp);
      DestroyMatrix(&Qtmp); 
      DestroyMatrix(&Rtmp);

      if(Res2 > Threshold) 
      {
        Nfault = NSat;  /// probably two or more faults exist
      }
     
    }  /// end of if(NSat<SAT_COUNT_FDI)
  
  }    /// end of if(Res2>Threshold)
  
  /// auxiliary matrix destroying
  DestroyMatrix(&dY);

  return Nfault;
}

//========================================================================
/// @brief     Earth's rotation compensation
///
/// @param[in] NP_out - pointer to output satellite coordinates
/// @param[in] NP_inp - pointer to input satellite coordinates
/// @param[in] dt     - time of signal propagation from satellite to user
//========================================================================
void Compensate_Rotation(tCoord *NP_out, tCoord *NP_inp, float64 dt)
{ 
  tCoord  NP_tmp,  ///< temporary satellite navigation parameters
         *ptr_NP;  ///< pointer to satellite navigation parameters
  float64 ang;     ///< angle
  
  /// ca = cos(ang); sa = sin(ang); sin -> angle (10^-17-> sin(angle) = angle), cos -> 1 (10^-11)
  ang = w_Earth * dt;

  ptr_NP = (NP_out == NP_inp) ? &NP_tmp : NP_out;
  ptr_NP->x  =  NP_inp->x + NP_inp->y * ang;
  ptr_NP->y  = -NP_inp->x * ang + NP_inp->y;
  ptr_NP->z  =  NP_inp->z;
  ptr_NP->vx =  NP_inp->vx + NP_inp->vy * ang;
  ptr_NP->vy = -NP_inp->vx * ang + NP_inp->vy;
  ptr_NP->vz =  NP_inp->vz;
  
  if(NP_out == NP_inp)
  { 
    NP_out->x  = ptr_NP->x;
    NP_out->y  = ptr_NP->y;
    NP_out->z  = ptr_NP->z;

    NP_out->vx = ptr_NP->vx;
    NP_out->vy = ptr_NP->vy;
    NP_out->vz = ptr_NP->vz;
  }

  return;
}

//========================================================================================
///  @brief      Vectorial multiplication of vectors: a = b*c with following normalization
///
///  @param[in]  b - pointer to instance
///  @param[in]  c - pointer to instance
///  @param[out] a - pointer to instance
//========================================================================================
static void mvec(float64 *a, float64 *b, float64 *c)
{
  float64 r;  ///< auxiliary variable

  a[0]=b[1] * c[2] - b[2] * c[1];
  a[1]=b[2] * c[0] - b[0] * c[2];
  a[2]=b[0] * c[1] - b[1] * c[0];

  r = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);

  if(r != 0)
  {
    r = 1 / r;
    a[0] *= r;  
    a[1] *= r;
    a[2] *= r;
  }
  
  return;
}


//===================================================================================
/// @brief     Calculation of basis (unit) vectors for user's local coordinate system
///
/// @param[in] User - pointer to instance
//===================================================================================
void ComputeTOPO(USER *User)
{
  float64 n[3]={0.,0.,1.},  ///< auxiliary vector
          e[3],             ///< auxiliary vector
          u[3],             ///< auxiliary vector
          r;                ///< auxiliary variable
  int32   i;

  User->flag_bl = 1;  /// user parameters blocking
  
  u[0] = User->ecef.X * l_a2_Earth;
  u[1] = User->ecef.Y * l_a2_Earth;
  u[2] = User->ecef.Z * a2_e2_Earth;

  r = sqrt(u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

  if(r)  /// r != 0
  {
    r = 1.0 / r;

    u[0] *= r;
    u[1] *= r;
    u[2] *= r;

    mvec(e, n, u);
    mvec(n, u, e);

    for(i = 0; i < 3; i++)
    { 
      User->TOPO[0][i]=n[i];
      User->TOPO[1][i]=e[i];
      User->TOPO[2][i]=u[i];
    }
  }
  else  /// local coordinate system is not defined
  {
    User->TOPO[0][0]=0.0;
    User->TOPO[0][1]=0.0;
    User->TOPO[0][2]=0.0;

    User->TOPO[1][0]=0.0;
    User->TOPO[1][1]=0.0;
    User->TOPO[1][2]=0.0;

    User->TOPO[2][0]=0.0; 
    User->TOPO[2][1]=0.0;
    User->TOPO[2][2]=0.0;
  }
  User->flag_bl = 0;  /// user parameters unblocking

  return;
}

//==========================================================================
/// @brief     Calculation of SV's elevation and azimuth, User-SV range,
///            radial velocity and directional cosines matrix
///
/// @param[in] ptrNP - pointer to satellite navigation parameters
/// @param[in] User  - pointer to user parameters
/// @param[in] flag  - computation flag
///            (0 - elevation is only compute, 1 - all parameters do)
//===========================================================================
void SV_User_parameters(tNavTaskParams *ptrNP, USER *User, int8 flag)
{
  float64 dx,  ///< delta X coordinates satellite and User
          dy,  ///< delta Y coordinates satellite and User
          dz,  ///< delta Z coordinates satellite and User
          n,   ///< north component of local coordinate system
          e,   ///< east component of local coordinate system
          u,   ///< up component of local coordinate system
          r;   ///< vector

  User->flag_bl = 1;  /// user parameters blocking

  /// Calculation of User to satellite vector [dx, dy, dz]
  dx = ptrNP->sat_coord.coord.x - User->ecef.X;
  dy = ptrNP->sat_coord.coord.y - User->ecef.Y;
  dz = ptrNP->sat_coord.coord.z - User->ecef.Z;

  ptrNP->sat_coord.x_los = (int64)(dx*1.e+3); // m->mm
  ptrNP->sat_coord.y_los = (int64)(dy*1.e+3);
  ptrNP->sat_coord.z_los = (int64)(dz*1.e+3);

  /// Convert User to satellite vector to NEU in user's local coordinate system
  n = User->TOPO[0][0] * dx + User->TOPO[0][1] * dy + User->TOPO[0][2] * dz;
  e = User->TOPO[1][0] * dx + User->TOPO[1][1] * dy;  /// User->TOPO[1][2] always equals zero
  u = User->TOPO[2][0] * dx + User->TOPO[2][1] * dy + User->TOPO[2][2] * dz;
  r = sqrt(n * n + e * e);

  /// Computation of the satellite elevation
  ptrNP->sat_params.elev = u ? (float32)atan2(u, r) : 0;
  
  if(flag > 0)
  {
    /// Computation of the satellite azimuth
    ptrNP->sat_params.azim = e ? (float32)atan2(e, n) : 0;

    if(ptrNP->sat_params.azim < 0) 
      ptrNP->sat_params.azim += (float32)M_2PI;

    /// Computation of the User satellite range 
    ptrNP->range = sqrt(dx * dx + dy * dy + dz * dz);
    
    /// Computation of the satellite directional cosines
    r = 1.0 / ptrNP->range;

    ptrNP->Hmat[0] = -dx * r;
    ptrNP->Hmat[1] = -dy * r;
    ptrNP->Hmat[2] = -dz * r;

    /// Computation of the User satellite radial velocity
    ptrNP->Vrad  = ptrNP->Hmat[0] * (User->ecef.Xdot - ptrNP->sat_coord.coord.vx)
                 + ptrNP->Hmat[1] * (User->ecef.Ydot - ptrNP->sat_coord.coord.vy)
                 + ptrNP->Hmat[2] * (User->ecef.Zdot - ptrNP->sat_coord.coord.vz);
  }
  User->flag_bl = 0;  /// user parameters unblocking

  return;
}
/*
//========================================================================
/// @brief      Calculation of atmospheric delay for SV's signal
///             (see ICD-GPS-200C, par.20.3.3.5.2.6)
///
/// @param[in]  NP      - pointer to satellite navigation parameters
/// @param[in]  User    - pointer to user parameters
/// @param[in]  IONpar  - pointer to ionosphere parameters
/// @param[in]  GPStime - receiver computed system time
/// @param[out] t_ION   - ionospheric delay correction
/// @param[out] t_TRO   - tropospheric delay correction
//========================================================================
static void AtmosphereDelay(tNavTaskParams *NP,
                            USER           *User,
                            ION_UTC        *IONpar,
                            float64         GPStime,
                            float32        *t_ION,
                            float32        *t_TRO)
{
  float32 E,    ///< elevation
          F,    ///< obliquity factor
          psi,  ///< Earth's central angle
          fi,   ///< geodetic latitude
          li,   ///< geodetic longitude
          fm,   ///< geomagnetic latitude
          tmp,  ///< temporary variable
          t,    ///< local time
          fm2,  ///< square fm
          fm3,  ///< multiply fm2 to fm
          AMP,  ///< AMP
          PER,  ///< PER
          X,    ///< phase
          x2,   ///< square X
          x4;   ///< multiply square X to square X

  /// satellite elevation
  E = NP->sat_params.elev * (float32)M_1_PI;
  
  /// Earth's central angle between the user position and the Earth projection of
  /// ionospheric intersection point [semi-cycles]
  psi = 0.0137f / (E + 0.11f) - 0.022f;
  User->flag_bl = 1;    /// user parameters blocking
  IONpar->flag_bl = 1;  /// ION and UTC parameters blocking
  
  /// geodetic latitude of the Earth projection on the ionospheric intersection
  /// point [semi-cycles]
  fi = (float32)User->geo.Lat * (float32)M_1_PI + psi * (float32)cos(NP->sat_params.azim);

  if(fi >  0.416f) 
    fi =  0.416f;
  if(fi < -0.416f) 
    fi = -0.416f;

  /// geodetic longitude of the Earth projection on the ionospheric intersection
  /// point [semi-cycles]
  li = (float32)User->geo.Lon * (float32)M_1_PI + psi * (float32)sin(NP->sat_params.azim) / (float32)cos(fi * M_PI);

  /// geomagnetic latitude of the Earth projection on the ionospheric intersection
  /// point [semi-cycles]
  fm = fi + 0.064f * (float32)cos((float32)M_PI * (li - 1.617f));

  /// obliquity factor [-]
  tmp = 0.53f - E;
  F = 1.0f + 16.0f * tmp * tmp * tmp;

  /// local time [sec]
  t = (float32)Fmodulo(43200.0f * li + GPStime, 86400.0f);

  /// compute AMP [sec] and PER [sec]
  fm2 = fm * fm;
  fm3 = fm2 * fm;
  AMP = IONpar->alpha0 + IONpar->alpha1 * fm + IONpar->alpha2 * fm2 + IONpar->alpha3 * fm3;

  if(AMP < 0.0f) 
    AMP = 0.0f;

  PER = IONpar->beta0  + IONpar->beta1 * fm  + IONpar->beta2 * fm2  + IONpar->beta3 * fm3;

  if(PER < 72000.0f) 
    PER = 72000.0f;

  /// phase [radians]
  X = (float32)M_2PI * (t - 50400.0f) / PER;

  /// ionospheric correction [sec]
  if((float32)fabs(X) < 1.57f)
  {
    x2 = X * X;
    x4 = x2 * x2;
    *t_ION = F * (5e-9f + AMP * (1.0f - 0.5f * x2 + 0.0416666f * x4)); // 1/24 = 0.0416666
  }
  else
    *t_ION = F * 5e-9f;

  /// tropospheric correction [sec]  (2.4224/299792458. = 8.0802566e-9)
  *t_TRO = 8.0802566e-9f * (float32)exp(-0.13346e-3f * (float32)User->geo.H) / (0.026f + (float32)sin(NP->sat_params.elev));

  if(*t_TRO < 0.0f) 
    *t_TRO = 0.0f;

  User->flag_bl = 0;    /// user parameters unblocking
  IONpar->flag_bl = 0;  /// ION and UTC parameters unblocking

  return;
}
*/

//===========================================================================
/// @brief     Compute GPS satellite position from time and almanac data
///
/// @param[in]  navtask_params - pointer to instance
/// @param[in]  Alm            - pointer to GPS satellite almanac
/// @param[in]  wk             - desired time, GPS week number
/// @param[in]  sec            - desired time, seconds of week
//===========================================================================
void SV_Alm_GPS(tNavTaskParams *navtask_params,
                ALM_GPS        *Alm,
                int             wk,
                double          sec)
{ 
  float64 a,        ///< semi-major axis 
          l_a,      ///< 1/a 
          sq_mu_a,  ///< temporary mean motion 
          n0,       ///< mean motion 
          dt,       ///< delta time
          ecc,      ///< eccentricity
          sq_ecc,   ///< root eccentricity
          M,        ///< mean anomaly
          E,        ///< eccentric anomaly
          E0,       ///< eccentric anomaly
          cos_E,    ///< cos by eccentric anomaly
          sin_E,    ///< sin by eccentric anomaly
          nu,       ///< true anomaly
          wa_new,   ///< argument of latitude
          cos_wa,   ///< cos by argument of latitude
          sin_wa,   ///< sin by argument of latitude
          r,        ///< radius-vector 
          cos_i,    ///< inclination
          sin_i,    ///< Inclination
          X,        ///< position in the orbital plane
          Y,        ///< position in the orbital plane
          la_new,   ///< longitude of the ascending node
          cos_la,   ///< cos by longitude of the ascending node
          sin_la,   ///< sin by longitude of the ascending node
          Vr,       ///< radial velocity
          Vu,       ///< tangential velocity
          V;        ///< velocity

  Alm->flag_bl = 1;  /// almanac parameters blocking

  ecc = Alm->e; sq_ecc = sqrt(1 - ecc*ecc);

  /// Semi-major axis [m]
  a = Alm->roota; a *= a; l_a = 1.0/a;

  /// Mean motion [rad/s]
  sq_mu_a = sqrt(mu_Earth*l_a);
  n0 = sq_mu_a*l_a;

  /// Compute the difference between the almanac epoch and wn & sec [s]
  dt = (wk-Alm->wna)*S_WEEK + sec - Alm->toa;

  /// Compute SV's time shift relatively system time
  navtask_params->sat_coord.coord.dt_SV = Alm->af0 + Alm->af1*dt;

  /// Obtain eccentric anomaly by solving Kepler's equation.
  E = M = Fmodulo(Alm->m0 + n0*dt,M_2PI);  /// mean anomaly

  do
  {
    E0 = E; E = M + ecc*sin(E0);
  }
  while(fabs(E-E0) > 1.e-13);

  cos_E = cos(E);
  sin_E = sin(E);

  /// Compute the true anomaly
  nu = atan2(sq_ecc*sin_E,cos_E - ecc);

  /// Radius-vector [m]
  r = a*(1-ecc*cos_E);

  /// Argument of latitude
  wa_new = nu + Alm->omega;
  cos_wa = cos(wa_new); sin_wa = sin(wa_new);
  
  /// Longitude of the ascending node
  la_new = Alm->omega0 + (Alm->omegadot - w_Earth)*dt - w_Earth*Alm->toa;
  cos_la = cos(la_new); sin_la = sin(la_new);

  /// Inclination
  cos_i = cos(Alm->i0); sin_i = sin(Alm->i0);
  Alm->flag_bl = 0;  /// almanac parameters unblocking

  /// Position in the orbital plane
  X = r*cos_wa; Y = r*sin_wa;

  /// Transform position in the orbital plane to position in space (WGS-84)
  navtask_params->sat_coord.coord.x = X*cos_la - Y*cos_i*sin_la;  /// cos_i  is cos(i), sin_i  is sin(i)
  navtask_params->sat_coord.coord.y = X*sin_la + Y*cos_i*cos_la;  /// cos_wa is cos(u), sin_wa is sin(u)
  navtask_params->sat_coord.coord.z = Y*sin_i;                    /// cos_la is cos(W), sin_la is sin(W)

  /// compute the velocities in the orbital plane
  V  = sq_mu_a/sq_ecc;
  Vr = V*ecc*sin(nu);      /// Radial velocity
  Vu = V*(1+ecc*cos(nu));  /// Tangential velocity

  /// Satellite velocities in WGS-84
  r = 1.0/r;
  navtask_params->sat_coord.coord.vx = Vr*navtask_params->sat_coord.coord.x*r  /// Sat->x/r is equal (cos_wa*cos_la - sin_wa*sin_la*cos_i)
          - Vu*(sin_wa*cos_la + cos_wa*sin_la*cos_i)
          + w_Earth*navtask_params->sat_coord.coord.y;
  navtask_params->sat_coord.coord.vy = Vr*navtask_params->sat_coord.coord.y*r  /// Sat->y/r is equal (cos_wa*sin_la + sin_wa*cos_la*cos_i)
          - Vu*(sin_wa*sin_la - cos_wa*cos_la*cos_i)
          - w_Earth*navtask_params->sat_coord.coord.x;
  navtask_params->sat_coord.coord.vz = (Vr*sin_wa + Vu*cos_wa)*sin_i;
  
  return;
}

/// Necessary multiplier definition
static const int32
     TO4    = 16,
     TO11   = 2048,
     TO12   = 4096,
     TO14   = 16384,
     TO16   = 65536;

static const float64 
     l_TO5  = 1.0/32.0,
     l_TO11 = 1.0/2048.0,
     l_TO19 = 1.0/524288.0,
     l_TO20 = 1.0/1048576.0,
     l_TO21 = 1.0/2097152.0,
     l_TO23 = 1.0/8388608.0,
     l_TO24 = 1.0/16777216.0,
     l_TO27 = 1.0/134217728.0,
     l_TO29 = 1.0/536870912.0,
     l_TO30 = 1.0/1073741824.0,
     l_TO31 = 1.0/2147483648.0,
     l_TO33 = 1.0/8589934592.0,
     l_TO38 = 1.0/274877906944.0,
     l_TO43 = 1.0/8796093022208.0,
     l_TO50 = 1.0/1125899906842624.0,
     l_TO55 = 1.0/36028797018963968.0;

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SaveEphemerisForNavTask, "fast_text_4")
	#endif
#endif
//=================================================================================
/// @brief      Copy ephemeris from temporary buffer to buffer for navigation task
///
/// @param[in]  pTmpEphGPS - pointer to instance
/// @param[in]  pEphGPS    - pointer to ephemeris GPS satellite
//=================================================================================
static SaveEphemerisForNavTask(TEMP_EPH_GPS *pTmpEphGPS, EPH_GPS *pEphGPS)
{
  pEphGPS->prn = pTmpEphGPS->TEphGPS.prn;
            
  /// copying SUBFRAME # 1
  pEphGPS->health   = pTmpEphGPS->TEphGPS.health ;
  pEphGPS->wn       = pTmpEphGPS->TEphGPS.wn;
  pEphGPS->accuracy = pTmpEphGPS->TEphGPS.accuracy;
  pEphGPS->tgd      = (float32)(pTmpEphGPS->TEphGPS.tgd * l_TO31);
  pEphGPS->toc      = pTmpEphGPS->TEphGPS.toc * TO4;
  pEphGPS->iodc     = pTmpEphGPS->TEphGPS.iodc;
  pEphGPS->af2      = (float32)(pTmpEphGPS->TEphGPS.af2 * l_TO55);
  pEphGPS->af1      = (float32)(pTmpEphGPS->TEphGPS.af1 * l_TO43);
  pEphGPS->af0      = (float32)(pTmpEphGPS->TEphGPS.af0 * l_TO31);
      
  /// copying SUBFRAME # 2
  pEphGPS->iode     = pTmpEphGPS->TEphGPS.iode;
  pEphGPS->crs      = (float32)(pTmpEphGPS->TEphGPS.crs * l_TO5);
  pEphGPS->deltan   = (float32)(pTmpEphGPS->TEphGPS.deltan * l_TO43 * M_PI);    /// transform in [radians/s] !!!
  pEphGPS->m0       = pTmpEphGPS->TEphGPS.m0 * l_TO31 * M_PI;                   /// transform in [radians] !!!
  pEphGPS->cuc      = (float32)(pTmpEphGPS->TEphGPS.cuc * l_TO29);
  pEphGPS->e        = pTmpEphGPS->TEphGPS.e * l_TO33;
  pEphGPS->cus      = (float32)(pTmpEphGPS->TEphGPS.cus * l_TO29);
  pEphGPS->roota    = pTmpEphGPS->TEphGPS.roota * l_TO19;
  pEphGPS->toe      = pTmpEphGPS->TEphGPS.toe * TO4;
      
  /// copying SUBFRAME # 3
  pEphGPS->cic      = (float32)(pTmpEphGPS->TEphGPS.cic * l_TO29);
  pEphGPS->omega0   = pTmpEphGPS->TEphGPS.omega0 * l_TO31 * M_PI;               /// transform in [radians] !!!
  pEphGPS->cis      = (float32)(pTmpEphGPS->TEphGPS.cis * l_TO29);
  pEphGPS->i0       = pTmpEphGPS->TEphGPS.i0 * l_TO31 * M_PI;                   /// transform in [radians] !!!
  pEphGPS->crc      = (float32)(pTmpEphGPS->TEphGPS.crc * l_TO5);
  pEphGPS->omega    = pTmpEphGPS->TEphGPS.omega * l_TO31 * M_PI;                /// transform in [radians] !!!
  pEphGPS->omegadot = (float32)(pTmpEphGPS->TEphGPS.omegadot * l_TO43 * M_PI);  /// transform in [radians/s] !!!
  pEphGPS->idot     = (float32)(pTmpEphGPS->TEphGPS.idot * l_TO43 * M_PI);      /// transform in [radians/s] !!!
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SaveAlmanacForNavTask, "fast_text_4")
	#endif
#endif
//=================================================================================
/// @brief      Copy almanac from temporary buffer to buffer for navigation task
///
/// @param[in]  pTmpAlmGPS - pointer to instance
/// @param[in]  pAlmGPS    - pointer to almanac GPS satellite
//=================================================================================
static SaveAlmanacForNavTask(TEMP_ALM_GPS *pTmpAlmGPS, ALM_GPS *pAlmGPS)
{
  pAlmGPS->prn      = pTmpAlmGPS->TAlmGPS.prn;
  pAlmGPS->wna      = pTmpAlmGPS->TAlmGPS.wna;
  pAlmGPS->e        = (float32)(pTmpAlmGPS->TAlmGPS.e * l_TO21);
  pAlmGPS->toa      = pTmpAlmGPS->TAlmGPS.toa * TO12;
  pAlmGPS->i0       = (float32)((pTmpAlmGPS->TAlmGPS.i0 * l_TO19 + 0.3) * M_PI);  /// transform in [radians] !!!
  pAlmGPS->omegadot = (float32)(pTmpAlmGPS->TAlmGPS.omegadot * l_TO38 * M_PI);    /// transform in [radians/s] !!!
  pAlmGPS->health   = pTmpAlmGPS->TAlmGPS.health;
  pAlmGPS->roota    = pTmpAlmGPS->TAlmGPS.roota * l_TO11;
  pAlmGPS->omega0   = pTmpAlmGPS->TAlmGPS.omega0 * l_TO23 * M_PI;                 /// transform in [radians] !!!
  pAlmGPS->omega    = pTmpAlmGPS->TAlmGPS.omega * l_TO23 * M_PI;                  /// transform in [radians] !!!
  pAlmGPS->m0       = pTmpAlmGPS->TAlmGPS.m0 * l_TO23 * M_PI;                     /// transform in [radians] !!!
  pAlmGPS->af0      = (float32)(pTmpAlmGPS->TAlmGPS.af0 * l_TO20);
  pAlmGPS->af1      = (float32)(pTmpAlmGPS->TAlmGPS.af1 * l_TO38);
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SaveIonUTCForNavTask, "fast_text_4")
	#endif
#endif
//=================================================================================
/// @brief      Copy ionosphere and UTC parameters from temporary buffer to 
///             buffer for navigation task
///
/// @param[in]  pTmpIonUTC - pointer to instance
/// @param[in]  pION_UTC   - pointer to ionosphere and UTC parameters
//=================================================================================
static SaveIonUTCForNavTask(TEMP_ION_UTC *pTmpIonUTC, ION_UTC *pION_UTC)
{
  /// good row parameters
  pION_UTC->alpha0 = (float32)(pTmpIonUTC->TIonUTC.ION.alpha0 * l_TO30);
  pION_UTC->alpha1 = (float32)(pTmpIonUTC->TIonUTC.ION.alpha1 * l_TO27);
  pION_UTC->alpha2 = (float32)(pTmpIonUTC->TIonUTC.ION.alpha2 * l_TO24);
  pION_UTC->alpha3 = (float32)(pTmpIonUTC->TIonUTC.ION.alpha3 * l_TO24);
  pION_UTC->beta0  = (float32)(pTmpIonUTC->TIonUTC.ION.beta0  * TO11);
  pION_UTC->beta1  = (float32)(pTmpIonUTC->TIonUTC.ION.beta1  * TO14);
  pION_UTC->beta2  = (float32)(pTmpIonUTC->TIonUTC.ION.beta2  * TO16);
  pION_UTC->beta3  = (float32)(pTmpIonUTC->TIonUTC.ION.beta3  * TO16);
  
  pION_UTC->A0     = pTmpIonUTC->TIonUTC.UTC.A0 * l_TO30;
  pION_UTC->A1     = pTmpIonUTC->TIonUTC.UTC.A1 * l_TO50;
  pION_UTC->tot    = pTmpIonUTC->TIonUTC.UTC.tot * TO12;
  pION_UTC->WNt    = ConvertTruncatedWeekNum(pTmpIonUTC->TIonUTC.UTC.WN, pTmpIonUTC->TIonUTC.UTC.WNt);
  pION_UTC->WNlsf  = ConvertTruncatedWeekNum(pTmpIonUTC->TIonUTC.UTC.WN, pTmpIonUTC->TIonUTC.UTC.WNlsf);
  pION_UTC->dtls   = pTmpIonUTC->TIonUTC.UTC.dtls;
  pION_UTC->dtlsf  = pTmpIonUTC->TIonUTC.UTC.dtlsf;
  pION_UTC->DN     = pTmpIonUTC->TIonUTC.UTC.DN;
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (NavTask_CopyDecodedData, "fast_text_4")
	#endif
#endif

//=================================================================================
/// @brief      Copies ephemeris, almanac and ionosphere data
///             from temporary buffers to auxiliary buffers
///
/// @param[in]  TopSys - pointer to Top System parameters
//=================================================================================
void NavTask_CopyDecodedData(tTopSystem *TopSys)
{
  int32 i;

  /// GPS EPHEMERIS
  for(i=0; i < GPS_SATELLITE_COUNT; i++)
  {
    /// check availability and blocking flag of temporary ephemeris
    if ((TopSys->DecCommon.TempEph[i].flag == 1) && (TopSys->DecCommon.TempEph[i].flag_bl == 0))
    {
      /// raise blocking flag
      TopSys->DecCommon.TempEph[i].flag_bl = 1;
      /// check blocking flag to ephemeris buffer for navigation task solution
      if (TopSys->EphGPS[i].flag_bl == 0)
      {
        /// raise blocking flag
        TopSys->EphGPS[i].flag_bl = 1;
        /// raise availability ephemeris flag
        TopSys->eph_set_flgs |= (0x1<<i);
        /// save ephemeris to navigation task solution buffer
        SaveEphemerisForNavTask(&TopSys->DecCommon.TempEph[i], &TopSys->EphGPS[i]);
        /// clear blocking flags and availability temporary ephemeris flag
        TopSys->EphGPS[i].flag_bl          = 0;
        TopSys->DecCommon.TempEph[i].flag  = 0;
      }
      /// clear blocking flags of temporary buffer
      TopSys->DecCommon.TempEph[i].flag_bl = 0;
    }
  }
  /// GPS ALMANAC
  for(i = 0; i < GPS_SATELLITE_COUNT; ++i)
  {
    /// check availability and blocking flag of temporary almanac
    if ((TopSys->DecCommon.TempAlmGPS[i].flag == 1) && (TopSys->DecCommon.TempAlmGPS[i].flag_bl == 0))
    {
      /// raise blocking flag
      TopSys->DecCommon.TempAlmGPS[i].flag_bl = 1;
      /// check blocking flags of navigation task solution buffer
      if(TopSys->AlmGPS[i].flag_bl == 0)
      {
        /// raise blocking flag
        TopSys->AlmGPS[i].flag_bl = 1;
        /// raise availability almanac flag
        TopSys->alm_set_flgs |= (0x1<<i);
        /// save almanac to navigation task solution buffer
        SaveAlmanacForNavTask(&TopSys->DecCommon.TempAlmGPS[i], &TopSys->AlmGPS[i]);
        /// clear blocking flag and availability temporary almanac flag
        TopSys->AlmGPS[i].flag_bl             = 0;
        TopSys->DecCommon.TempAlmGPS[i].flag  = 0;
      }
      /// clear blocking flag of temporary buffer
      TopSys->DecCommon.TempAlmGPS[i].flag_bl = 0;
    }
  }
  /// GPS IonUTC: check availability and blocking flag of ionosphere and UTC_GPS parameters
  if ((TopSys->DecCommon.TempIonUTC.flag) && (TopSys->DecCommon.TempIonUTC.flag_bl == 0))
  {
    /// raise blocking flag
    TopSys->DecCommon.TempIonUTC.flag_bl = 1;
    /// check blocking flags to navigation task solution buffers
    if(TopSys->IonUTC.flag_bl == 0)
    {
      /// raise blocking flag
      TopSys->IonUTC.flag_bl = 1;
      /// save ionosphere and UTC_GPS parameters to navigation task solution buffer
      SaveIonUTCForNavTask(&TopSys->DecCommon.TempIonUTC, &TopSys->IonUTC);
      /// clear blocking flag and availability temporary ionosphere and UTC_GPS parameters flag
      TopSys->IonUTC.flag_bl             = 0;
      TopSys->DecCommon.TempIonUTC.flag  = 0;
    }
    /// clear blocking flag of temporary buffer
    TopSys->DecCommon.TempIonUTC.flag_bl = 0;
  }
}

//=================================================================================
/// @brief      Fix-point raw data convert floating-point format
///
/// @param[in]  raw_data_fix - pointer to fix-point raw data
/// @param[in]  raw_data     - pointer to floating-point raw data
//=================================================================================
static void NavTask_RawData(tRawDataFix *raw_data_fix,
                            tRawData    *raw_data)
{
  raw_data->attempt   = raw_data_fix->attempt;
  raw_data->ps_delay  = LongFractGetDouble(&raw_data_fix->ps_delay)/1000.;   /// ms -> s
  raw_data->ps_freq   = LongFractGetDouble(&raw_data_fix->ps_freq);
  raw_data->timeCoord = LongFractGetDouble(&raw_data_fix->timeCoord)/1000.;  /// ms -> s
}

//======================================================================================
/// @brief      Control satellite with ephemeris list
///
/// @param[in]  pSatMgr - pointer to Satellite manager object
/// @param[in]  sec     - GPS time of week, [s]
/// @param[in]  week    - GPS week number
//======================================================================================
static void ControlEphemeris(tSatelliteManager *pSatMgr, float64 sec, int16 week)
{
  float64        dt;        ///< delta time
  tSatelliteGPS *pSat;      ///< pointer to satellite
  int32          i;

  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    /// check availability ephemeris
    if ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<i)) == (0x1<<i))
    {
      /// calculate age ephemeris
      if ((week == (WEEK-1)) && (pSatMgr->TopSystem.EphGPS[i].wn == 0))
      {
        dt = fabs((week - pSatMgr->TopSystem.EphGPS[i].wn - WEEK) * S_WEEK  + sec - pSatMgr->TopSystem.EphGPS[i].toe);
      }
      else if ((week == 0) && (pSatMgr->TopSystem.EphGPS[i].wn == (WEEK-1)))
      {
        dt = fabs((week - pSatMgr->TopSystem.EphGPS[i].wn + WEEK) * S_WEEK + sec - pSatMgr->TopSystem.EphGPS[i].toe);
      }
      else
      {
        dt = fabs((week - pSatMgr->TopSystem.EphGPS[i].wn) * S_WEEK + sec - pSatMgr->TopSystem.EphGPS[i].toe);
      }

      /// ephemeris is old  
      if(dt > GOOD_EPH_TIME)
      {
        /// clear availability ephemeris flag
        pSatMgr->TopSystem.eph_set_flgs &= (0xFFFFFFFF - (0x1<<i));
        /// clear availability ephemeris flag
        pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
        if (pSat != NULL)
          pSat->status &= 0xFB;  /// for ephemeris: 2 bit
      }  /// if(dt > GOOD_EPH_TIME)
    }
    else  /// if !(pSatMgr->TopSystem.eph_set_flg[i] == 1)
    {
      /// clear availability ephemeris flag
      pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
      if (pSat != NULL)
        pSat->status &= 0xFB; /// for ephemeris: 2 bit
    }  /// if !((pSatMgr->TopSystem.eph_set_flgs&(0x1<<i)) == (0x1<<i))
  }    /// for(i=0;i<GPS_SATELLITE_COUNT;i++)
}

//====================================================================================
/// @brief      Control satellite with almanac list
///
/// @param[in]  pSatMgr - pointer to Satellite manager object
/// @param[in]  sec     - GPS time of week, [s]
/// @param[in]  week    - GPS week number
//====================================================================================
static void ControlAlmanac(tSatelliteManager *pSatMgr, float64 sec, int16 week)
{
  float64        dt;        ///< delta time
  tSatelliteGPS *pSat;      ///< pointer to satellite
  int32          i;

  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    /// check availability almanac
    if ((pSatMgr->TopSystem.alm_set_flgs&(0x1<<i)) == (0x1<<i))
    {
      /// calculate age almanac
      if ((week == (WEEK-1)) && (pSatMgr->TopSystem.AlmGPS[i].wna == 0))
      {
        dt = fabs((week - pSatMgr->TopSystem.AlmGPS[i].wna - WEEK) * S_WEEK + sec - pSatMgr->TopSystem.AlmGPS[i].toa);
      }
      else if ((week == 0) && (pSatMgr->TopSystem.AlmGPS[i].wna == (WEEK-1)))
      {
        dt = fabs((week - pSatMgr->TopSystem.AlmGPS[i].wna + WEEK) * S_WEEK + sec - pSatMgr->TopSystem.AlmGPS[i].toa);
      }
      else
      {
        dt = fabs((week - pSatMgr->TopSystem.AlmGPS[i].wna) * S_WEEK + sec - pSatMgr->TopSystem.AlmGPS[i].toa);
      }

      /// almanac is old  
      if(dt > GOOD_ALM_TIME)
      {
        /// clear availability almanac flag
        pSatMgr->TopSystem.alm_set_flgs &= (0xFFFFFFFF - (0x1<<i));
        /// clear availability almanac flag
        pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
        if (pSat != NULL)
          pSat->status &= 0xF7;  /// for almanac: 3 bit
      }
    }
    else  /// if !(pSatMgr->TopSystem.alm_set_flg[i] == 1)
    {
      /// clear availability almanac flag
      pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
      if (pSat != NULL)
        pSat->status &= 0xF7;  /// for almanac: 3 bit
    }  /// if 1((pSatMgr->TopSystem.alm_set_flgs&(0x1<<i)) == (0x1<<i))
  }    /// for(i=0;i<GPS_SATELLITE_COUNT;i++)
}

//=================================================================================
/// @brief      Clear User parameters
///
/// @param[in]  pNavTask - pointer to Navigation Task Solution object
/// @param[in]  pSatMgr  - pointer to Satellite manager object
//=================================================================================
void ClearUser(tNavTask *pNavTask, tSatelliteManager *pSatMgr)
{
  int i;

  /// clear User structure
  memset(&pNavTask->User, 0, sizeof(USER));
  /// clear list with satellite is used in navigation task solution
  pNavTask->NavCond.NavSVOld.N = 0;
  for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
  {
    tSatelliteGPS* pSat;  ///< pointer to satellite
    /// check satellite ID
    if (pSatMgr->TopSystem.navtask_params[i].sv_num != SATELLITE_INVALID_ID)
    {
      /// set channel status to IDLE
      pSat = SatMgr_GetSatellite(pSatMgr, pSatMgr->TopSystem.navtask_params[i].sv_num);
      if (pSat != NULL)
        pSat->gps_chan.status.state = es_IDLE;
      /// clear preamble time and satellite navigation parameters
      pSatMgr->TopSystem.DecCommon.preamble[pSatMgr->TopSystem.navtask_params[i].sv_num-1].PreambleTime = -1;
      SatMgr_RemoveNavTaskParams(pSatMgr, pSatMgr->TopSystem.navtask_params[i].sv_num);
    }
  }
  /// clear RAIM key, first solution, nPPS start flags
  pNavTask->NavCond.RAIMKey = 0;
  pNavTask->FirstSolution   = fg_OFF;
}

//=================================================================================
/// @brief      Setting satellite status
///
/// @param[in]  pNavTask - pointer to Navigation task solution object
/// @param[in]  pSatMgr  - pointer to Satellite manager object
//=================================================================================
void SatellitesStatus(tNavTask *pNavTask, tSatelliteManager *pSatMgr)
{
  uint8          i;    
  tSatelliteGPS *pSat;  ///< pointer to satellite

  for (i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
  {
    /// check satellite ID
    if (pSatMgr->TopSystem.navtask_params[i].sv_num != SATELLITE_INVALID_ID)
    {
      pSat = SatMgr_GetSatellite(pSatMgr, pSatMgr->TopSystem.navtask_params[i].sv_num);
      if (pSat != NULL)
      {
        /// check bad satellite status
        if (pSatMgr->TopSystem.navtask_params[i].BAD_SAT == 1)
        {
          pSat->gps_chan.status.state = es_IDLE;
          pSatMgr->TopSystem.DecCommon.preamble[pSatMgr->TopSystem.navtask_params[i].sv_num-1].PreambleTime = -1;
          SatMgr_RemoveNavTaskParams(pSatMgr, pSatMgr->TopSystem.navtask_params[i].sv_num);
          continue;
        }
        else if (pSatMgr->TopSystem.navtask_params[i].paramsOP.BAD_SAT_OP == 1)
        {
          pSat->gps_chan.status.state = es_IDLE;
          pSatMgr->TopSystem.DecCommon.preamble[pSatMgr->TopSystem.navtask_params[i].sv_num-1].PreambleTime = -1;
          SatMgr_RemoveNavTaskParams(pSatMgr, pSatMgr->TopSystem.navtask_params[i].sv_num);
          continue;
        }
        /// used in navigation task: 0 bit
        if ((pSatMgr->TopSystem.navtask_params[i].USED     == 1) && 
            (pSatMgr->TopSystem.navtask_params[i].ELEV_MSK == 0) &&
            (pSatMgr->TopSystem.navtask_params[i].BAD_SAT  == 0))
          pSat->status |= 0x1;
        else
          pSat->status &= 0xFE;
        /// elevation mask: bit 1
        if ((pSatMgr->TopSystem.navtask_params[i].ELEV_MSK == 1) || (pSatMgr->TopSystem.navtask_params[i].paramsOP.ELEV_MSK_OP == 1))
          pSat->status |= 0x2;
        else
          pSat->status &= 0xFD;
        if ((pSatMgr->TopSystem.navtask_params[i].paramsOP.USED_OP     == 1) &&
            (pSatMgr->TopSystem.navtask_params[i].paramsOP.ELEV_MSK_OP == 0) &&
            (pSatMgr->TopSystem.navtask_params[i].paramsOP.BAD_SAT_OP  == 0))
          pSat->status |= 0x10;
        else
          pSat->status &= 0xEF;
      }  /// if (pSat != NULL)
      else
        SatMgr_RemoveNavTaskParams(pSatMgr, pSatMgr->TopSystem.navtask_params[i].sv_num);
    }  /// if (pSatMgr->TopSystem.navtask_params[i].sv_num != SATELLITE_INVALID_ID)  
  }    /// for (i=0;i<GPS_SAT_TO_NAVIGATION;i++)
}

//=================================================================================
/// @brief      Navigation task initialization
///
/// @param[in]  pNavTask   - pointer to instance
/// @param[in]  NavSolRate - navigation task solution rate [Hz]
/// @param[in]  OPmode     - OP mode: 0 - only navigation, 1 - navigation and OP
//=================================================================================
void NavTask_Init(tNavTask *pNavTask, int16 NavSolRate, int8 OPmode)
{
  uint32 time,  ///< period of navigation task solution in ms
         rem;   ///< remainder   

  /// disable navigation task solution
  if (NavSolRate == 0)
    pNavTask->NoNavTaskSol = fg_ON;
  else
  {
    pNavTask->NoNavTaskSol = fg_OFF;
    time = (uint32)NavSolRate;  // ms
  }
  pNavTask->NavCond.RAIMKey  = 0;
  pNavTask->NavCond.DOP      = 100;
  pNavTask->PeriodTime       = time;
  pNavTask->CountPostNavTask = time;
  pNavTask->FirstSolution    = fg_OFF;
  pNavTask->nPPSstart        = fg_OFF;
  pNavTask->NavTaskEnd       = fg_ON;
  pNavTask->NavSolution      = 0;
  pNavTask->Synchronization  = NO_SYNC;
  /// calculate fractional period for synchronization
  rem                        = time%PERIOD_SYNC;
  pNavTask->numPeriod        = (uint8)(time/PERIOD_SYNC);
  pNavTask->PeriodSyncFract  = PERIOD_SYNC + rem;
  pNavTask->cntPeriod        = 0;
  if (OPmode == 0)
    pNavTask->Synchronization = SYNC_OK;
}

//=======================================================================================
/// @brief      Navigation task initialization
///
/// @param[in]  pNavTask   - pointer to instance
/// @param[in]  pSatMgr    - pointer to Satellite Manager object
/// @param[in]  weekData   - pointer to week data structure
/// @param[in]  controlWN  - pointer to control week data
/// @param[in]  OrbStatic  - Orbital/Static OP model: 0 - orbital model, 1 - static model
//=======================================================================================
void NavTask_Process(tNavTask          *pNavTask,
                     tSatelliteManager *pSatMgr,
                     tOPManager        *pOPMgr,
                     tWeekData         *weekData,
                     tControlWN        *controlWN,
                     int8               OrbStatic,
                     eFlag              BinStrFlag,
                     DATA_STRING       *dataStr,
                     tTerminal         *DebugTerminal)
{
  int i;

  /// convert fix-point data to floating-point
  for(i=0;i<pNavTask->NavCond.NavSV.N;i++)
  {
    tNavTaskParams *navtask_params = SatMgr_GetNavTaskParams(pSatMgr, pNavTask->NavCond.NavSV.Cells[i]);
    if (navtask_params != NULL)
    {
      NavTask_RawData(&navtask_params->raw_data_fix, &navtask_params->raw_data);
      NavTask_RawData(&navtask_params->paramsOP.raw_data_fix, &navtask_params->paramsOP.raw_data);
    }
  }

  if (pNavTask->NoNavTaskSol == fg_OFF)
  {
    /// navigation task solution
    NavTaskSolution(pNavTask, pSatMgr);
  }
  /// start message forming and OP initialization after second solution
  if ((pNavTask->User.SolutionFlag == 1) && (pNavTask->User.CoordValid == OK_NAV_TASK) && (pNavTask->NavSolution == 1))
  {
    pNavTask->NavSolution = 2;
    pNavTask->CountSync   = pNavTask->CountPostNavTask;      
  }
  else if ((pNavTask->User.SolutionFlag == 1) && (pNavTask->User.CoordValid == OK_NAV_TASK) && (pNavTask->NavSolution == 0))
    pNavTask->NavSolution = 1;
  if ((pNavTask->User.SolutionFlag == 0) || (pNavTask->User.CoordValid != OK_NAV_TASK))
  {
    pNavTask->NavSolution = 0;
    pNavTask->CountSync   = 0;
    if (pNavTask->Synchronization == SYNC_PROCESS)
    {
      if (BinStrFlag == fg_OFF)
      {
        sprintf(dataStr->str, "ERR_FNT: time:%12.6lf, modelOS:%1d\r\n", pNavTask->NavCond.t, OrbStatic);
        DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
      }
      pNavTask->Synchronization = NO_SYNC;
      pOPMgr->fSynchronization  = NO_SYNC;
      OPMgr_Init(pOPMgr, OrbStatic);
    }
  }
  /// start synchronization OP and navigation task solution
  if ((pNavTask->Synchronization == NO_SYNC) && (pNavTask->NavSolution == 2))
  {
    int32 dt      = pNavTask->CountPostNavTask - pNavTask->CountSync;
//    int8  sv_nums = 0;

    if (dt < -MS_HALF_WEEK)
      dt += MS_WEEK;

/*    for(i=0;i<GPS_SATELLITE_COUNT;i++)
    {
      if (pSatMgr->TopSystem.usedSV[i] == 1)
        sv_nums++;
    }

    if (sv_nums >= (GPS_SAT_CHANNELS_COUNT>>1))
    {
      pNavTask->Synchronization = SYNC_OK;
      pOPMgr->fSynchronization  = SYNC_OK;
      pNavTask->enableMessages  = 1;
      pNavTask->CountSync       = 0;
    }
    else*/ if (dt >= SYNC_TIME)
    {
      if (BinStrFlag == fg_OFF)
      {
        sprintf(dataStr->str, "SYNC_PRS: time:%12.6lf\r\n", pNavTask->NavCond.t);
        DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
      }
      pNavTask->Synchronization = SYNC_PROCESS;
      pOPMgr->fSynchronization  = SYNC_PROCESS;
      pNavTask->enableMessages  = 1;
      pNavTask->CountSync       = pNavTask->CountPostNavTask;
    }
  }

  /// starting nPPS signal forming algorithm after second navigation task solution
  if ((pNavTask->NavCond.RAIMKey == 1) && (pNavTask->nPPSstart == fg_OFF) && (pNavTask->Synchronization == SYNC_OK))
    pNavTask->nPPSstart = fg_ON;
  if ((pNavTask->FirstSolution   == fg_ON)       && 
      (pNavTask->User.CoordValid == OK_NAV_TASK) && 
      (pNavTask->NavCond.RAIMKey == 0))
    pNavTask->NavCond.RAIMKey = 1;
  /// control age of ephemeris and almanac
  if ((pNavTask->User.SolutionFlag) && (pNavTask->User.CoordValid == OK_NAV_TASK))
  {
    ControlEphemeris(pSatMgr, pNavTask->NavCond.t, pNavTask->NavCond.wn);
    ControlAlmanac(pSatMgr, pNavTask->NavCond.t, pNavTask->NavCond.wn);
  }
  if (((pNavTask->User.SolutionFlag) && (pNavTask->User.CoordValid != OK_NAV_TASK))
    || (pNavTask->User.SolutionFlag == 0))
  {
    pNavTask->CorrectTime.R_mes   = 0;
    pNavTask->CorrectTime.Rnc_mes = 0;
  }

  /// control week data
  if (pNavTask->User.SolutionFlag && (pNavTask->User.CoordValid == OK_NAV_TASK))
  {
    /// if week data is exist and is not saving to FLASH
    if ((weekData->DataError == fg_OFF) && (controlWN->SaveData == fg_OFF))
    {
      /// check difference between current week number and week number from FLASH 
      if (weekData->wnFlash != pNavTask->User.wn)
      {
        /// if week number from FLASH more then current week number then overflow is occur
        if (weekData->wnFlash > pNavTask->User.wn)
          weekData->cntOverFlow++;
        /// save new data
        weekData->wnFlash           = pNavTask->User.wn;
        controlWN->SaveData = fg_ON;
      }
    }
  }
}


//=======================================================================================
/// @brief      Navigation task initialization
///
/// @param[in]  pNavTask   - pointer to Navigation task object
/// @param[in]  pOPMgr     - pointer to OP manager object
/// @param[in]  pSatMgr    - pointer to Satellite Manager object
/// @param[in]  OrbStatic  - Orbital/Static OP model: 0 - orbital model, 1 - static model
//=======================================================================================
void ControlOPAndNavigation(tNavTask          *pNavTask, tOPManager *pOPMgr, 
                            tSatelliteManager *pSatMgr,  int8        OrbStatic,
                            eFlag              BinStrFlag, DATA_STRING *dataStr,
                            tTerminal         *DebugTerminal)
{
  int8  res = -1;
  int32 dt;
  eFlag fProcessing = fg_ON;

  if (pNavTask->Synchronization == SYNC_PROCESS)
  {
    dt = pNavTask->CountPostNavTask - pNavTask->CountSync;
    if (dt < -MS_HALF_WEEK)
      dt += MS_WEEK;
    if (dt >= SYNC_TIME)
      fProcessing = fg_OFF;
    else
      fProcessing = fg_ON;
    res = FirstSolutionValidity(&pNavTask->User, pOPMgr, &pOPMgr->solutionStatus, fProcessing,
                                BinStrFlag, dataStr, DebugTerminal);
    if (res == 1)
    {
      pNavTask->Synchronization = SYNC_OK;
      pOPMgr->fSynchronization  = SYNC_OK;
      if (BinStrFlag == fg_OFF)
      {
        sprintf(dataStr->str, "SYNC_OK_FS: time:%12.6lf\r\n", pNavTask->NavCond.t);
        DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
      }
    }
    if (res == 0)
    {
      if (BinStrFlag == fg_OFF)
      {
        sprintf(dataStr->str, "SYNC_ERR_FS: time:%12.6lf\r\n", pNavTask->NavCond.t);
        DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
      }
      pNavTask->Synchronization = NO_SYNC;
      pOPMgr->fSynchronization  = NO_SYNC;
      ClearUser(pNavTask, pSatMgr);
      OPMgr_Init(pOPMgr, OrbStatic);
    }
    return;
  }
  
  // Routine control
  if (pNavTask->Synchronization == SYNC_OK)
  {
    if ((pOPMgr->fOP_Condition != OP_COND_PROPAGATION) &&
        (pOPMgr->fOP_Condition != OP_COND_ESTIMATION))
    {// no OP solution
      // no control 
      return;
    }

    if (!pNavTask->User.SolutionFlag)
    {// OP solution & no NavTask solution
      ///> to do: OP solution control by estimation & propagation
      return;
    }
    
    // OP solution & no NavTask solution

    // critical RAIM error
    if (pNavTask->User.CoordValid == RAIM_ERROR)
    {
      if (BinStrFlag == fg_OFF)
      {
        sprintf(dataStr->str, "RAIM_ERR: time:%12.6lf\r\n", pNavTask->NavCond.t);
        DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
      }
      pNavTask->Synchronization = NO_SYNC;
      pOPMgr->fSynchronization  = NO_SYNC;
      ClearUser(pNavTask, pSatMgr);
      OPMgr_Init(pOPMgr, OrbStatic);
      return;
    }

    // check case when OP invalidates SV`s by OP raw data filter
    {
      int8 i, ok = 0;
      
      for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
      {
        if (pSatMgr->TopSystem.navtask_params[i].sv_num != SATELLITE_INVALID_ID)
        {
          if (   (pSatMgr->TopSystem.navtask_params[i].USED                == 1)
              && (pSatMgr->TopSystem.navtask_params[i].paramsOP.BAD_SAT_OP == 1))
          {
            ok = 1;
            if (BinStrFlag == fg_OFF)
            {
              sprintf(dataStr->str, "BAD_SAT: time:%12.6lf, SV:%2d\r\n", pNavTask->NavCond.t, pSatMgr->TopSystem.navtask_params[i].sv_num);
              DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
            }
          }
        }
      }
      if (ok == 1)
      {
        /// clear User structure
        memset(&pNavTask->User, 0, sizeof(USER));
        /// clear RAIM key, first solution, nPPS start flags
        pNavTask->NavCond.RAIMKey = 0;
        pNavTask->FirstSolution   = fg_OFF;
        return;
      }
    }

    // Error NavTask checking
    if (pNavTask->User.CoordValid == ERROR_NAV_TASK)
    {
      if (BinStrFlag == fg_OFF)
      {
        sprintf(dataStr->str, "ERR_NAV_TASK: time:%12.6lf\r\n", pNavTask->NavCond.t);
        DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
      }
      pNavTask->Synchronization = NO_SYNC;
      pOPMgr->fSynchronization  = NO_SYNC;
      ClearUser(pNavTask, pSatMgr);
      OPMgr_Init(pOPMgr, OrbStatic);
      return;
    }

    // Solution validity
    if (pNavTask->User.CoordValid == OK_NAV_TASK)
    {
      if (SolutionValidity(&pNavTask->User, pOPMgr, BinStrFlag, dataStr, DebugTerminal) == 0)
      {
        if (BinStrFlag == fg_OFF)
        {
          sprintf(dataStr->str, "SYNC_ERR: time:%12.6lf\r\n", pNavTask->NavCond.t);
          DebugTerminal->vtable.pfnWrite(&DebugTerminal->vtable, dataStr->str, strlen(dataStr->str));
        }
        pNavTask->Synchronization = NO_SYNC;
        pOPMgr->fSynchronization  = NO_SYNC;
        ClearUser(pNavTask, pSatMgr);
        OPMgr_Init(pOPMgr, OrbStatic);
        return;
      }
    }
  }
}


