/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  control functions
*    @file                   Control.c
*    @author                 A. Baloyan
*    @date                   25.06.2005
*    @version                1.0
******************************************************************************/
#include <math.h>
#include "Control.h"

//===================================================================================
/// @brief      Function calculates x modulo y, the remainder zfof x/y. 
/// @param[in]  X - number type float64
/// @param[in]  Y - number type float64
/// @return     the remainder f, where x = ay + f for some integer a, and 0 < f < y 
//===================================================================================
float64 Fmodulo(float64 X, float64 Y)
{
  return (X - floor(X/Y)*Y); 
}  


