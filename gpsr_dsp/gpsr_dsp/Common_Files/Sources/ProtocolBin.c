 /*****************************************************************************
 *    Copyright (c) 2007 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Binary protocol
 *   @file                   ProtocolBin.c
 *   @author                 M. Zhokhova
 *   @date                   04.06.2007
 *   @version                2.0
 */
/*****************************************************************************/

#include "MainManager.h"
#include "Control.h"
#include "comfuncs.h"

#include <stdio.h>

extern uint32 can_or_uart, pre_can_or_uart;
extern uint16 TC_count, TC_success_count;
//============================================================================
/// @brief     Binary command parser
///
/// @param[in] pMainMgr - pointer to Main Manager object
/// @param[in] pSatMgr  - pointer to Satellite Manager object
/// @param[in] pNavTask - pointer to Navigation Task object
/// @param[in] data     - pointer to binary data
//============================================================================
static void MainMgr_ParserBinMessage(tMainManager      *pMainMgr,
                                     tSatelliteManager *pSatMgr,
                                     tNavTask          *pNavTask,
                                     tFlashDrv         *pFlashDrv,
                                     DATA_BIN          *data);

//========================================================================
/// @brief     Binary command parser
///            flag = 0 for RS422 without acknowledge, 
///            flag = 1 for RS422 with acknowledge,
///
/// @param[in] pMainMgr - pointer to Main Manager object
/// @param[in] pThis    - pointer to UART driver
/// @param[in] param    - parameter command
/// @param[in] flag     - UART flag
//========================================================================
static void ParserBinCommand(tMainManager *pMainMgr,
                             tTerminal    *pThis,
                             uint8         param,
                             uint8         flag);

//============================================================================
/// @brief     Binary request parser
/// 
/// @param[in] pMainMgr   - pointer to Main Manager object
/// @param[in] pFramework - pointer to Framework object
/// @param[in] pSatMgr    - pointer to Satellite Manager object
/// @param[in] pNavTask   - pointer to Navigation Task object
/// @param[in] pOPMgr     - pointer to OP Manager object
/// @param[in] data       - pointer to binary data
//============================================================================
static void ParserBinRequest(tMainManager      *pMainMgr,
                             tFramework        *pFramework,
                             tSatelliteManager *pSatMgr,
                             tNavTask          *pNavTask,
                             tOPManager        *pOPMgr,
                             DATA_BIN          *data);

//==============================================================================
/// @brief     Download System file
/// 
/// @param[in] pThis - pointer to Flash Driver
/// @param[in] data  - pointer to binary data
//==============================================================================
static void DownloadFile(tFlashDrv *pThis, DATA_BIN *data);

//============================================================================
/// @brief     Upload System file
/// 
/// @param[in] pThis - pointer to Flash Driver
/// @param[in] file  - pointer to instance
/// @param[in] data  - pointer to binary data
//============================================================================
static void UploadFile(tFlashDrv *pThis, DATA_BIN *data);

//========================================================================
/// @brief     Search binary message
/// 
/// @param[in] pThis - pointer to UART driver
/// @param[in] data  - pointer to binary data
//========================================================================
int MainMgr_SearchMesBin(tTerminal *pThis,
                         DATA_BIN  *data)
{
  int length;

  /// get filled size receiver buffer
  length = FIFO_GetFilledSize(&pThis->ReceiveQueue);

  if (length == 0)
    return 0;

  // if before reading header message
  if (data->length != 0)
  {
    if (length >= (data->length+1))
    {
      // reading message remainder
      pThis->vtable.pfnReadBinary(&pThis->vtable, (uint8*)data->str, data->length);
      pThis->vtable.pfnReadBinary(&pThis->vtable, &data->check_sum, 1);
      return 1;
    }  
  }
  if (data->length == 0)
  {
    while(length >= LENGTH_HEADER)
    {
      uint8 i,
            check_sum=0,
            header[LENGTH_HEADER],
            tmp;

      pThis->vtable.pfnReadBinary(&pThis->vtable, &header[0], 1);
      /// search beginning header: slave address
      tmp = header[0];
      if(tmp == SYNC)
      {
        // read header remainder
        pThis->vtable.pfnReadBinary(&pThis->vtable, &header[1], (LENGTH_HEADER-1));
        // control checksum
        for(i=0;i<(LENGTH_HEADER-1);i++)
          check_sum += header[i];
        // save header
        data->length  = (uint8)header[1];
        data->length |= ((uint16)header[2]<<8)&0xFF00;
        data->param  = (header[3]>>3)&0x1F;
        data->opcode = header[3]&0x7;
        if ((check_sum&0xFF) != header[4])
        {
          data->length = 0;
          length -= LENGTH_HEADER;
        }
        else
        {
          if (data->opcode == DATA_HR)
          {
            if ((length-LENGTH_HEADER) >= (data->length+1))
            {
              pThis->vtable.pfnReadBinary(&pThis->vtable, (uint8*)data->str, data->length);
              pThis->vtable.pfnReadBinary(&pThis->vtable, &data->check_sum, 1);
              return 1;
            }
            else return 0;
          }
          else if (data->opcode == DATA_RH)
            return 1;
          else if (data->opcode == COMMAND)
            return 1;
          else if (data->opcode == COMMAND_ACK)
            return 1;
          else 
          {
            data->length = 0;
            length -= LENGTH_HEADER; 
          }
        }
      }
      else length--;
    }
  }
  return 0;
}

//========================================================================
/// @brief     Binary parser
/// 
/// @param[in] pMainMgr   - pointer to Main Manager object
/// @param[in] pFramework - pointer to Framework object
/// @param[in] pSatMgr    - pointer to Satellite Manager object
/// @param[in] pNavTask   - pointer to Navigation Task object
/// @param[in] pOPMgr     - pointer to OP Manager object
//========================================================================
void MainMgr_ParserBin(tMainManager      *pMainMgr,
                       tFramework        *pFramework,
                       tSatelliteManager *pSatMgr,
                       tNavTask          *pNavTask,
                       tOPManager        *pOPMgr)
{
  if (pMainMgr->MESSAGE.dataBin.opcode == DATA_HR)
  {
    /// flag = 0 for RS422
    MainMgr_ParserBinMessage(pMainMgr, pSatMgr, pNavTask, &pFramework->FlashDrv, &pMainMgr->MESSAGE.dataBin);
    /// save current opcode and param for additional information message
    pMainMgr->MESSAGE.dataBin.opcode_old = pMainMgr->MESSAGE.dataBin.opcode;
    pMainMgr->MESSAGE.dataBin.param_old  = pMainMgr->MESSAGE.dataBin.param;
    pMainMgr->MESSAGE.dataBin.opcode     = 0;
  }
    if (pMainMgr->MESSAGE.dataBin.opcode == COMMAND)
  {
    /// flag = 3 for RS422 without acknowledge, flag = 7 for RS422 with acknowledge
    ParserBinCommand(pMainMgr, &pFramework->DebugTerminal, pMainMgr->MESSAGE.dataBin.param, COMMAND);
    /// save current opcode and param for additional information message
    pMainMgr->MESSAGE.dataBin.opcode_old = pMainMgr->MESSAGE.dataBin.opcode;
    pMainMgr->MESSAGE.dataBin.param_old  = pMainMgr->MESSAGE.dataBin.param;
    pMainMgr->MESSAGE.dataBin.opcode     = 0;
  }
  if (pMainMgr->MESSAGE.dataBin.opcode == COMMAND_ACK)
  {
    /// flag = 3 for RS422 without acknowledge, flag = 7 for RS422 with acknowledge
    ParserBinCommand(pMainMgr, &pFramework->DebugTerminal, pMainMgr->MESSAGE.dataBin.param, COMMAND_ACK);
    /// save current opcode and param for additional information message
    pMainMgr->MESSAGE.dataBin.opcode_old  = pMainMgr->MESSAGE.dataBin.opcode;
    pMainMgr->MESSAGE.dataBin.param_old   = pMainMgr->MESSAGE.dataBin.param;
    pMainMgr->MESSAGE.dataBin.opcode      = 0;
  }
  if (pMainMgr->MESSAGE.dataBin.opcode == DATA_RH)
  {
    ParserBinRequest(pMainMgr, pFramework, pSatMgr, pNavTask, pOPMgr, &pMainMgr->MESSAGE.dataBin);
    /// save current opcode and param for additional information message
    pMainMgr->MESSAGE.dataBin.opcode_old  = pMainMgr->MESSAGE.dataBin.opcode;
    pMainMgr->MESSAGE.dataBin.param_old   = pMainMgr->MESSAGE.dataBin.param;
    pMainMgr->MESSAGE.dataBin.opcode      = 0;
  }
}

//========================================================================
/// @brief     Binary command parser
///            flag = 3 for RS422 without acknowledge,
///            flag = 7 for RS422 with acknowledge,
/// 
/// @param[in] pMainMgr - pointer to Main Manager object
/// @param[in] pThis    - pointer to UART/Multi-drop driver
/// @param[in] param    - parameter command
/// @param[in] flag     - UART flag
//========================================================================
static void ParserBinCommand(tMainManager *pMainMgr,
                             tTerminal    *pThis,
                             uint8         param,
                             uint8         flag)
{
  uint8 header[2];
  header[0] = SYNC;
  header[1] = (param<<3)&0xF8 | (flag&0x7);
  
  switch(param) 
  {
  /// NMEA string mode
  case NMEA_STR:
    {
      pMainMgr->BinStrFlag = fg_OFF;
      pMainMgr->Message    = (SAT_MES | POS_MES | OPP_MES);  /// SAT, POS, OPP
      return;
    }
  /// Request Built-in Test
  case BUILT_IN_TEST:
    {
      pMainMgr->flags.BIT = 1;
      return;
    }
  /// Software Reset
  case SW_RESET:
    {
#ifdef NEW_TMTC
	reset_type = 1 ;
#endif	
      pMainMgr->flags.RESET = 1;
      return;
    }
  case CLEAR_EPH_ALM:
    {
      pMainMgr->flags.CLEAR    = 1;
      pMainMgr->flags.FL_CLEAR = 1;
    }
  default:
    break;
  }

  /// acknowledge for any unknown command
  if (flag == COMMAND_ACK)
    pThis->vtable.pfnWriteBinary(&pThis->vtable, header, 2);
  return;
}

//============================================================================
/// @brief     Binary request parser
/// 
/// @param[in] pMainMgr   - pointer to Main Manager object
/// @param[in] pFramework - pointer to Framework object
/// @param[in] pSatMgr    - pointer to Satellite Manager object
/// @param[in] pNavTask   - pointer to Navigation Task object
/// @param[in] pOPMgr     - pointer to OP Manager object
/// @param[in] data       - pointer to binary data
//============================================================================
static void ParserBinRequest(tMainManager      *pMainMgr,
                             tFramework        *pFramework,
                             tSatelliteManager *pSatMgr,
                             tNavTask          *pNavTask,
                             tOPManager        *pOPMgr,
                             DATA_BIN          *data)
{
  uint8           header[2];

  header[0] = SYNC;
  header[1] = (data->param<<3)&0xF8 | (data->opcode&0x7);

  switch(data->param)
  {
  /// Configuration Setup 
  case DRH_CONFIG:
    {
      _memcpy_cs(data->str, &pMainMgr->config, sizeof(CONFIG_SETUP));
    }
    break;
  /// BIT and Status
  case DRH_BIT_STATUS:
    {
      _memcpy_cs(data->str, &pFramework->BuiltInTest, sizeof(tBuiltInTest));
    }
    break;
  /// Time and Channels Status
  case DRH_TIME_CH_STATUS:
    {
      _memcpy_cs(data->str, &pMainMgr->data.timeChStatus, sizeof(tTimeChStatus));
    }
    break;
  /// Channels range measurements
  case DRH_CH_RM:
    {
     _memcpy_cs(data->str, &pMainMgr->data.chRM, sizeof(tChRM));
    }  
    break;
  case DRH_LOS:
    {
      /// this message forming in navigation task solution thread: if structure is blocking then data for it is ready and
      /// therefore message may be forming in this function
      if (pMainMgr->data.messLOS.flag_bl == 0)
      {
        pMainMgr->data.messLOS.flag_bl = 1;  /// blocking data
        _memcpy_cs(data->str, &pMainMgr->data.messLOS.message, sizeof(tDataLOS));
        pMainMgr->data.messLOS.flag_bl = 0;  /// unblocking data
      }
      else  /// message forming
      {
        MainMgr_LOS(&pMainMgr->data.BinData.dataLOS, pSatMgr->TopSystem.navtask_params, pNavTask->User.User_fix.wn, pNavTask->User.User_fix.t);
        _memcpy_cs(data->str, &pMainMgr->data.BinData.dataLOS, sizeof(tDataLOS));
      }
    }
	break;
  /// Navigation and Status (ECEF/Geo/OE)
  case DRH_NAV_STATUS:
    {
      /// check ECEF, geographic or OE format of message
      switch(pMainMgr->config.ECEF) 
      {
      case CF_ECEF:  /// ECEF
        {
          /// this message forming in navigation task solution thread: if structure is blocking then data for it is ready and
          /// therefore message may be forming in this function
          if (pMainMgr->data.navData.flag_bl == 0)
          {
            pMainMgr->data.navData.flag_bl = 1;  /// blocking data
            _memcpy_cs(data->str, &pMainMgr->data.navData.message.ecef, sizeof(tECEFNavStatus));
            pMainMgr->data.navData.flag_bl = 0;  /// unblocking data
          }
          else  /// message forming
          {
            MainMgr_NavAndStatusECEF(&pMainMgr->data.BinData.navMes.ecef, pNavTask->User, pNavTask->NavCond);
            _memcpy_cs(data->str, &pMainMgr->data.BinData.navMes.ecef, sizeof(tECEFNavStatus));
          }
        }
        break;
      case CF_GEO:  /// Geographic
        {
          /// this message forming in navigation task solution thread: if structure is blocking then data for it is ready and
          /// therefore message may be forming in this function
          if (pMainMgr->data.navData.flag_bl == 0)
          {
            pMainMgr->data.navData.flag_bl = 1;  /// blocking data
            _memcpy_cs(data->str, &pMainMgr->data.navData.message.geo, sizeof(tGeoNavStatus));
            pMainMgr->data.navData.flag_bl = 0;  /// unblocking data
          }
          else  /// message forming
          {
            MainMgr_NavAndStatusGEO(&pMainMgr->data.BinData.navMes.geo, pNavTask->User, pNavTask->NavCond);
            _memcpy_cs(data->str, &pMainMgr->data.BinData.navMes.geo, sizeof(tGeoNavStatus));
          }
        }
        break;
      case CF_OE:  /// OE
        {
          /// this message forming in navigation task solution thread: if structure is blocking then data for it is ready and
          /// therefore message may be forming in this function
          if (pMainMgr->data.navData.flag_bl == 0)
          {
            pMainMgr->data.navData.flag_bl = 1;  /// blocking data
            _memcpy_cs(data->str, &pMainMgr->data.navData.message.oe, sizeof(tOENavStatus));
            pMainMgr->data.navData.flag_bl = 0;  /// unblocking data
          }
          else  /// message forming
          {
            MainMgr_NavAndStatusOE(&pMainMgr->data.BinData.navMes.oe, pNavTask->User, pNavTask->NavCond);
            _memcpy_cs(data->str, &pMainMgr->data.BinData.navMes.oe, sizeof(tOENavStatus));
          }
        }
        break;
      }
    }
    break;
  /// OP Navigation and Status (ECEF/Geo/OE)
  case DRH_OP_NAV_STATUS:
    {
      /// check ECEF, geographic or OE format of message
      switch(pMainMgr->config.ECEF) 
      {
      case CF_ECEF:  /// ECEF
        {
          /// this message forming in OP navigation task solution thread: if structure is blocking then data for it is ready and
          /// therefore message may be forming in this function
          if (pMainMgr->data.navDataOP.flag_bl == 0)
          {
            pMainMgr->data.navDataOP.flag_bl = 1;  /// blocking data
            _memcpy_cs(data->str, &pMainMgr->data.navDataOP.message.ecef, sizeof(tECEFOPNavStatus));
            pMainMgr->data.navDataOP.flag_bl = 0;  /// unblocking data
          }
          else  /// message forming
          {
            // navigation and status for OP
            MainMgr_OPNavAndStatusECEF(&pMainMgr->data.BinData.OPnavMes.ecef, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
            _memcpy_cs(data->str, &pMainMgr->data.BinData.OPnavMes.ecef, sizeof(tECEFOPNavStatus));
          }
        }
        break;
      case CF_GEO:  /// Geographic
        {
          /// this message forming in OP navigation task solution thread: if structure is blocking then data for it is ready and
          /// therefore message may be forming in this function
          if (pMainMgr->data.navDataOP.flag_bl == 0)
          {
            pMainMgr->data.navDataOP.flag_bl = 1;  /// blocking data
            _memcpy_cs(data->str, &pMainMgr->data.navDataOP.message.geo, sizeof(tGeoOPNavStatus));
            pMainMgr->data.navDataOP.flag_bl = 0;  /// unblocking data
          }
          else  /// message forming
          {
            MainMgr_OPNavAndStatusGEO(&pMainMgr->data.BinData.OPnavMes.geo, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
            _memcpy_cs(data->str, &pMainMgr->data.BinData.OPnavMes.geo, sizeof(tGeoOPNavStatus));
          }
        }
        break;
      case CF_OE:  /// OE
        {
          /// this message forming in OP navigation task solution thread: if structure is blocking then data for it is ready and
          /// therefore message may be forming in this function
          if (pMainMgr->data.navDataOP.flag_bl == 0)
          {
            pMainMgr->data.navDataOP.flag_bl = 1;  /// blocking data
            _memcpy_cs(data->str, &pMainMgr->data.navDataOP.message.oe, sizeof(tOEOPNavStatus));
            pMainMgr->data.navDataOP.flag_bl = 0;  /// unblocking data
          }
          else  /// message forming
          {
            MainMgr_OPNavAndStatusOE(&pMainMgr->data.BinData.OPnavMes.oe, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
            _memcpy_cs(data->str, &pMainMgr->data.BinData.OPnavMes.oe, sizeof(tOEOPNavStatus));
          }
        }
        break;
      }
    }
    break;
  /// Almanac data
  case DRH_ALMANAC:
    {
      MainMgr_Almanac(&pMainMgr->data.BinData.mesAlm, &pSatMgr->TopSystem);
      _memcpy_cs(data->str, &pMainMgr->data.BinData.mesAlm, sizeof(tMesAlmanac));
    }
    break;
  /// Ephemeris data 
  case DRH_EPHEMERIS:
    {
      MainMgr_Ephemeris(&pMainMgr->data.BinData.mesEph, pSatMgr);
      _memcpy_cs(data->str, &pMainMgr->data.BinData.mesEph, sizeof(tMesEphemeris));
    }
    break;
  /// Ionosphere correction data
  case DRH_INOSPHERE:
    {
      /// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
      if ((pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0) || (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag == 0))
        _memcpy(&pMainMgr->data.BinData.mesIon, &pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.ION, sizeof(RAW_ION));
      else  /// if data is block then raw data is not available
        memset(&pMainMgr->data.BinData.mesIon, 0, sizeof(RAW_ION));
      _memcpy_cs(data->str, &pMainMgr->data.BinData.mesIon, sizeof(RAW_ION));
    }
    break;
  /// UTC-GPS clock correction
  case DRH_UTC_GPS:
    {
      /// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
      if ((pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0) || (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag == 0))
        _memcpy(&pMainMgr->data.BinData.mesUtc, &pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.UTC, sizeof(RAW_UTC));
      else  /// if data is block then raw data is not available
        memset(&pMainMgr->data.BinData.mesUtc, 0, sizeof(RAW_UTC));
      _memcpy_cs(data->str, &pMainMgr->data.BinData.mesUtc, sizeof(RAW_UTC));
    }
    break;
  /// S/W download system
  case DRH_SW_SYSTEM:
    {
      DownloadFile(&pFramework->FlashDrv, data);
    }
    break;
  /// Additional information
  case DRH_ADDITIONAL_INF:
    {
      static int8 flag_err = 0x0;
      /// previous opcode and param
      pMainMgr->data.BinData.addInf.lastOpcode = data->opcode_old;
      pMainMgr->data.BinData.addInf.lastParam  = data->param_old;
      /// process and error uploading System file
      flag_err = pFramework->FlashDrv.flashData.controlFile.Process & pFramework->FlashDrv.flashData.controlFile.SaveData;
      pMainMgr->data.BinData.addInf.systemUpload = (flag_err&0x1) | ((pFramework->FlashDrv.flashData.controlFile.Error<<1)&0x2);
      _memcpy_cs(data->str, &pMainMgr->data.BinData.addInf, sizeof(tAddInf));
    }
    break;
  default:
    break;
  }

  pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, header, 2);
  pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, data->str, (uint16)(data->length+1));
}

//============================================================================
/// @brief     Binary command parser
///
/// @param[in] pMainMgr - pointer to Main Manager object
/// @param[in] pSatMgr  - pointer to Satellite Manager object
/// @param[in] pNavTask - pointer to Navigation Task object
/// @param[in] data     - pointer to binary data
//============================================================================
void MainMgr_ParserBinMessage(tMainManager      *pMainMgr,
                              tSatelliteManager *pSatMgr,
                              tNavTask          *pNavTask,
                              tFlashDrv         *pFlashDrv,
                              DATA_BIN          *data)
{
  int   i;
  uint8 check_sum = 0;  ///< check sum

  /// calculate check sum
  for(i=0;i<data->length;i++)
    check_sum += data->str[i];
  /// control check sum
  if (check_sum != data->check_sum)
    return;
 
  switch(data->param) 
  {
  /// Configuration Setup
  case DHR_CONFIG:
    {
      /// check message length
      if (data->length == LENGTH_CONFIG)
      {
        _memcpy(&pMainMgr->config_new, data->str, data->length);
        pMainMgr->NewConfig = fg_ON;
      }
    }
  	break;
  /// Almanac Upload
  case DHR_ALMANAC:
    {
      /// check message length
      if (data->length == LENGTH_ALMANAC)
      {
        _memcpy(&pMainMgr->data.BinData.mesAlm, data->str, data->length);
        /// copy almanac to temporary buffer for every satellite
        for(i=0;i<GPS_SATELLITE_COUNT;i++)
        {
          if (pMainMgr->data.BinData.mesAlm.almanac[i].prn != SATELLITE_INVALID_ID)
          {
            /// check blocking flag
            if (pSatMgr->TopSystem.DecCommon.TempAlmGPS[pMainMgr->data.BinData.mesAlm.almanac[i].prn-1].flag_bl == 0)
            {
              pSatMgr->TopSystem.DecCommon.TempAlmGPS[pMainMgr->data.BinData.mesAlm.almanac[i].prn-1].flag_bl = 1;  /// blocking data
              _memcpy(&pSatMgr->TopSystem.DecCommon.TempAlmGPS[pMainMgr->data.BinData.mesAlm.almanac[i].prn-1].TAlmGPS, 
                    &pMainMgr->data.BinData.mesAlm.almanac[i], sizeof(MES_ALM_GPS));
              pSatMgr->TopSystem.DecCommon.TempAlmGPS[pMainMgr->data.BinData.mesAlm.almanac[i].prn-1].flag    = 1;  /// raise availability almanac flag
              pSatMgr->TopSystem.DecCommon.TempAlmGPS[pMainMgr->data.BinData.mesAlm.almanac[i].prn-1].flag_bl = 0;  /// unblocking data 
            }  
          }
        }
      }
    }
  	break;
  /// Ephemeris Upload
  case DHR_EPHEMERIS:
    {
      /// check message length
      if (data->length == LENGTH_EPHEMERIS)
      {
        _memcpy(&pMainMgr->data.BinData.mesEph, data->str, data->length);
        /// copy ephemeris to temporary buffer for every satellite
        for(i=0;i<GPS_SATELLITE_COUNT;i++)
        {
          if (pMainMgr->data.BinData.mesEph.ephemeris[i].prn != SATELLITE_INVALID_ID)
          {
            /// check blocking flag
            if (pSatMgr->TopSystem.DecCommon.TempEph[pMainMgr->data.BinData.mesEph.ephemeris[i].prn-1].flag_bl == 0)
            {
              pSatMgr->TopSystem.DecCommon.TempEph[pMainMgr->data.BinData.mesEph.ephemeris[i].prn-1].flag_bl = 1;  /// blocking data
              _memcpy(&pSatMgr->TopSystem.DecCommon.TempEph[pMainMgr->data.BinData.mesEph.ephemeris[i].prn-1].TEphGPS,
                  &pMainMgr->data.BinData.mesEph.ephemeris[i], sizeof(MES_EPH_GPS));
              pSatMgr->TopSystem.DecCommon.TempEph[pMainMgr->data.BinData.mesEph.ephemeris[i].prn-1].flag    = 1;  /// raise availability ephemeris flag
              pSatMgr->TopSystem.DecCommon.TempEph[pMainMgr->data.BinData.mesEph.ephemeris[i].prn-1].flag_bl = 0;  /// unblocking data 
            }
          }
        }
      }
    }
  	break;
  /// Ionosphere parameters Upload
  case DHR_IONOSPHERE:
    {
      /// check message length
      if (data->length == LENGTH_IONOSPHERE)
      {
        /// check blocking flag
        if (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0)
        {
          pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl = 1;  /// blocking data
          _memcpy(&pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.ION, data->str, data->length);
          pSatMgr->TopSystem.DecCommon.TempIonUTC.flag    = 1;  /// raise availability ionosphere data flag
          pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl = 0;  /// unblocking data
        }
      }
    }
  	break;
  /// UTC-GPS clock correction Upload
  case DHR_UTC_GPS:
    {
      /// check message length
      if (data->length == LENGTH_UTC_GPS)
      {
        /// check blocking flag
        if (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0)
        {
          pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl = 1;  /// blocking data
          _memcpy(&pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.UTC, data->str, data->length);
          pSatMgr->TopSystem.DecCommon.TempIonUTC.flag    = 1;  /// raise availability UTC-GPS parameters flag
          pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl = 0;  /// unblocking data
        }
      }
    }
  	break;
  case DHR_INIT_POS:
    {
      /// check message length
      if (data->length == LENGTH_INIT)
      {
        /// check blocking flag
        if (pMainMgr->initMes.flag_bl == 0)
        {
          pMainMgr->initMes.flag_bl = 1;
          _memcpy(&pMainMgr->initMes.initData.ecef, data->str, data->length);
          if (pSatMgr->WeekInit == fg_OFF)
          {
            pSatMgr->wn       = pMainMgr->initMes.initData.ecef.wn;
            pSatMgr->WeekInit = fg_ON;
          }
          pMainMgr->initMes.InitData = 1;
          pMainMgr->initMes.flag_bl = 0;
        }
      }
    }
  /// S/W upload system
  case DHR_SW_SYSTEM:
    {
      UploadFile(pFlashDrv, data);
    }
  	break;
  default:
    break;
  }
}

//=============================================================================
/// @brief     Download System file
/// 
/// @param[in] pThis - pointer to Flash Driver
/// @param[in] data  - pointer to binary data
//=============================================================================
static void DownloadFile(tFlashDrv *pThis, DATA_BIN *data)
{
  uint8  length,         ///< length of data
         check_sum = 0,  ///< check sum
        *src,            ///< pointer to source
         rem;            ///< remainder message
  uint32 size;           ///< size of data
  int    i;              ///< counter
  /// control system file structure
  tControlFile *file = &pThis->flashData.controlFile;

  /// if data block is equal 1 then calculate blocks number and define check sum location in messages
  if (file->currentBlock == 1)
  {
    /// pointer to system file length in FLASH
    src = (uint8*)(pThis->address[FL_SYSTEM] + 8);
    /// read length of file
    COPYINT(((uint8*)&size), src, 4);
    /// added date + version + length + check_sum
    size += 16;
    file->totalLength = size;
    /// calculate blocks number
    file->blocksNumber = (uint16)(size/LENGTH_MAX_SW_SM);
    /// calculate remainder
    rem = size % LENGTH_MAX_SW_SM;
    if (rem != 0)
      file->blocksNumber++;
    /// define check sum location in messages: if remainder less check sum size 
    /// then check sum transmit in two messages otherwise in last message
    if (rem < CHECK_SUM_SYSTEM)
      file->checkSum = rem;
    else
      file->checkSum = 0;
    /// length of data
    length = LENGTH_MAX_SW_SM;
  }
  else  /// if !(pMainMgr->currentBlFPGA == 1) next data block
  {
    /// check error block number
    if ((file->currentBlock == 0) || (file->currentBlock > file->blocksNumber))
      length = 0;
    else  /// is not error
    {
      /// calculate length of data for last data block
      if (file->currentBlock == file->blocksNumber)
      {
        /// calculate length of data
        length = file->totalLength % LENGTH_MAX_SW_SM;
        /// if length is equal zero then last data length is equal LENGTH_MAX_SW_SM
        if (length == 0)
          length = LENGTH_MAX_SW_SM;
      }
      else  /// the rest of the messages
        length = LENGTH_MAX_SW_SM;
    }
  }  /// if !(pMainMgr->currentBlFPGA == 1)

  /// blocks number
  src = (uint8*)&file->blocksNumber;
  COPYINT(data->str, src, 2);
  /// current block number
  src = (uint8*)&file->currentBlock;
  COPYINT(((uint8*)&data->str[2]), src, 2); 
  /// length of data
  src = (uint8*)&length;
  COPYINT(((uint8*)&data->str[4]), src, 1);

  /// save part check sum in next to last message: then data length - part check sum size
  if ((file->checkSum  > 0) && ((file->currentBlock +1) == file->blocksNumber))
    length -= (4 - file->checkSum);
  /// last data block
  if (file->currentBlock == file->blocksNumber)
  {
    /// if all check sum is saved in last message then data length - check sum size
    if (file->checkSum == 0)
      length -= CHECK_SUM_SYSTEM;
    else  /// otherwise last part check sum is saved in last message then data is finished and data length = 0
      length = 0;
  }
  src = (uint8*)(pThis->address[FL_SYSTEM] + (LENGTH_MAX_SW_SM*(file->currentBlock-1)));
  /// read data from sector FLASH
  for(i=0;i<length;i++)
  {
    data->str[5+i] = *src++;
  }
  /// save part check sum in next to last message
  if ((file->checkSum > 0) && ((file->currentBlock + 1) == file->blocksNumber))
  {
    /// pointer to check sum in FLASH
    src = (uint8*)(pThis->address[FL_SYSTEM] + file->totalLength - CHECK_SUM_SYSTEM);
    /// read part check sum
    COPYINT(((uint8*)&data->str[5+length]), src, (CHECK_SUM_SYSTEM - file->checkSum));
    /// increment length of data
    length += (CHECK_SUM_SYSTEM - file->checkSum);
  }
  /// last data block
  if (file->currentBlock == file->blocksNumber)
  {
    /// save check sum
    if (file->checkSum == 0)
    {
      /// pointer to check sum in sector FLASH
      src = (uint8*)(pThis->address[FL_SYSTEM] + file->totalLength - CHECK_SUM_SYSTEM);
      /// read check sum
      COPYINT(((uint8*)&data->str[5+length]), src, CHECK_SUM_SYSTEM);
      /// increment length of data
      length += CHECK_SUM_SYSTEM;
    }
    else
    {
      /// pointer to last part check sum
      src = (uint8*)(pThis->address[FL_SYSTEM] + file->totalLength - file->checkSum);
      /// read part check sum
      COPYINT(((uint8*)&data->str[5+length]), src, file->checkSum);
      /// increment length of data
      length += file->checkSum;
    }
  }
  /// added zero in message
  if (length < LENGTH_MAX_SW_SM)
  {
    for(i=0;i<(LENGTH_MAX_SW_SM-length);i++)
      data->str[5+length+i] = 0;
  }
  /// calculate check sum of message
  for(i=0;i<LENGTH_SW_SM;i++)
    check_sum += data->str[i];
  data->str[LENGTH_SW_SM] = (check_sum&0xff);
  /// increment current block number
  file->currentBlock += 1;
  /// reset current block number
  if (file->currentBlock > file->blocksNumber)
  {
    file->currentBlock = 1;
    file->totalLength  = 0;
  }
}

//============================================================================
/// @brief     Upload System file
/// 
/// @param[in] pThis - pointer to Flash Driver
/// @param[in] file  - pointer to instance
/// @param[in] data  - pointer to binary data
//============================================================================
static void UploadFile(tFlashDrv *pThis, DATA_BIN *data)
{
  uint16 numOfBlocks,      ///< blocks number
         currentBlock;     ///< current block number
  uint8 *src = data->str;  ///< pointer to source
  /// control system file structure
  tControlFile *file = &pThis->flashData.controlFile;
  
  /// read blocks number
  COPYINT(((uint8*)&numOfBlocks), src, 2);
  /// read current block number
  COPYINT(((uint8*)&currentBlock), src, 2); 
  /// for first block
  if (currentBlock == 1)
  {
    /// if error FLASH is exist then clear error FLASH flag
    if (file->ErrorFlash == fg_ON)
      file->ErrorFlash = fg_OFF;
    /// initialize parameters
    file->Error        = 0;
    file->Process      = 1;
    file->SaveData     = fg_ON;
    file->numOfBlocks  = 1;
    file->checkSum     = 0;
    file->totalLength  = 0;
    /// copy data to work buffer and calculate check sum
    file->length = (data->length - 4);
    _memcpy_cs32(file->m_Data, &data->str[4], (data->length - 4), &file->checkSum);
  }
  else  /// if !(CurrentBlock == 1) next data block
  {
    /// check error FLASH
    if (file->ErrorFlash == fg_OFF)
    {
      /// check error block number
      if ((currentBlock == 0) || (currentBlock > numOfBlocks))
      {
        /// process finished and raise error flag
        file->Process  = 0;
        file->Error    = 1;
        file->SaveData = fg_OFF;
      }
      else  /// if !((CurrentBlock == 0) || (CurrentBlock > NumOfBlocks)) is not error
      {
        /// check save data finished flag
        if (file->SaveData == fg_OFF)
        {
          /// check error block number
          if ((file->numOfBlocks+1) != currentBlock)
          {
            /// process finished and raise error flag
            file->Process  = 0;
            file->Error    = 1;
            file->SaveData = fg_OFF;
          }
          else  /// is not error
          {
            /// start save new data block
            file->SaveData     = fg_ON;
            file->numOfBlocks  = currentBlock;
            /// copy data to work buffer and calculate check sum
            file->length = (data->length - 4);
            _memcpy_cs32(file->m_Data, &data->str[4], (data->length - 4), &file->checkSum);
            /// raise data finished
            if (numOfBlocks == currentBlock)
              file->EndData = fg_ON;
          }
        }  /// if (file->SaveData == fg_OFF)
      }    /// if !((CurrentBlock == 0) || (CurrentBlock > NumOfBlocks))
    }      /// if (file->ErrorFlash == fg_OFF)
  }        /// if !(CurrentBlock == 1)
}


int dbg_printf(char *fmt, ...)
{
extern tFramework           FrameworkState;
extern tMainManager         MainManagerState;
extern tSatelliteManager    SatelliteManagerState;


	uint8 * p_frame, * p_msg ;
	uint8 tmp_8 ;
	int i ;
	tECEFNavStatus *p_tmp_ecef ;
	va_list args;

//#ifdef CAN_PORT_TMTC
//	return 0;
//#endif
    if(can_or_uart > OUTPUT_UART)
		return 0;

	
    va_start(args, fmt);
#ifdef NEW_TMTC
	if ( tmtc_mode != DETAIL_MODE )
	{	
		p_msg = gpsr_dbg_tm_frame.message ;	
	    sprintf(p_msg, "%u:", SatelliteManagerState.ms_count);
	    vsprintf(p_msg+strlen(p_msg), fmt, args);
// checksum
		p_frame = (uint8 *)&gpsr_dbg_tm_frame ;
		tmp_8 = 0 ;
		for ( i = 0 ; i < TM_GPSR_DBG_MSG_FRAME_LENGTH - 1 ; i ++ )
			tmp_8 += p_frame[i] ;
		gpsr_dbg_tm_frame.chksum = (- tmp_8) ;
// send it out
/*
	#ifdef CAN_PORT_TMTC	
		FrameworkState.DebugTerminal.vtable.pfnWriteBinary(&FrameworkState.DebugTerminal.vtable, (uint8 *)p_frame, TM_GPSR_DBG_MSG_CAN_FRAME_LENGTH);
	#else
		FrameworkState.DebugTerminal.vtable.pfnWriteBinary(&FrameworkState.DebugTerminal.vtable, (uint8 *)p_frame, TM_GPSR_DBG_MSG_FRAME_LENGTH);
	#endif
*/
	if(can_or_uart > OUTPUT_UART)
		FrameworkState.DebugTerminal.vtable.pfnWriteBinary(&FrameworkState.DebugTerminal.vtable, (uint8 *)p_frame, TM_GPSR_DBG_MSG_CAN_FRAME_LENGTH);
	else
		FrameworkState.DebugTerminal.vtable.pfnWriteBinary(&FrameworkState.DebugTerminal.vtable, (uint8 *)p_frame, TM_GPSR_DBG_MSG_FRAME_LENGTH);
		
	}
	else
#endif
	if ( MainManagerState.BinStrFlag == fg_OFF )
	{
		p_frame = p_msg = (uint8 *)MainManagerState.TestStr.str ;
	    vsprintf(p_msg, fmt, args);
		FrameworkState.DebugTerminal.vtable.pfnWrite(&FrameworkState.DebugTerminal.vtable, (uint8 *)p_frame, strlen(p_frame));
	}

    va_end(args);
	
	return 1;
}

// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC

static RAW_UTC myUtc ;

void update_myUtc(tSatelliteManager *pSatMgr)
{
// GPS-UTC clock correction parameters
	/// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
	if ((pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0) || (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag == 0))
		_memcpy(&myUtc, &pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.UTC, sizeof(RAW_UTC));
	else  /// if data is block then raw data is not available
		memset(&myUtc, 0, sizeof(RAW_UTC));
}


static void output_nav_tm(	tMainManager      *pMainMgr,
					tFramework        *pFramework,
                    tSatelliteManager *pSatMgr,
                    tNavTask          *pNavTask,
                    tOPManager        *pOPMgr)							 
{
	tECEFNavStatus *p_tmp_ecef ;
	tTimeChStatus *p_time_ch ;
	tECEFOPNavStatus *p_tmp_opecef ;
	uint64 tmp_64 ;
	uint32 tmp_32 ;
	uint16 tmp_16 ;
	float32 ftmp_32 ;
	uint8 tmp_8 ;
	int i ;
	uint8 * p_frame ;

	
// ECEF navigation and status data
#if 0		
	/// this message forming in navigation task solution thread: if structure is blocking then data for it is ready and
	/// therefore message may be forming in this function
	p_tmp_ecef = &pMainMgr->data.BinData.navMes.ecef ;
	if (pMainMgr->data.navData.flag_bl == 0)
	{
		pMainMgr->data.navData.flag_bl = 1;  /// blocking data
		_memcpy_cs(p_tmp_ecef, &pMainMgr->data.navData.message.ecef, sizeof(tECEFNavStatus));
		pMainMgr->data.navData.flag_bl = 0;  /// unblocking data
	}
	else  /// message forming
	{
		MainMgr_NavAndStatusECEF(p_tmp_ecef, pNavTask->User, pNavTask->NavCond);
	}
#else
	p_tmp_ecef = &pMainMgr->data.navData.message.ecef ;
#endif	
	SET_SHORT_BE(&gpsr_nav_tm_frame.gps_week_number, p_tmp_ecef->wn) ;

	tmp_64 = (uint64)p_tmp_ecef->mks_l | ( ((uint64)p_tmp_ecef->mks_h << 16 ) & 0xffffffffffff0000) ;
	tmp_32 = tmp_64 / 1000 ;		// micro-second --> mili-second
	SET_LONG_BE(&gpsr_nav_tm_frame.gps_time, tmp_32) ;

	// validity
	if (pNavTask->NoNavTaskSol == fg_ON)
		tmp_16 = 0 ;
	else
	if (pNavTask->NavSolution == 2)
		tmp_16 = 1 ;
	else
		tmp_16 = 0 ;
	SET_SHORT_BE(&gpsr_nav_tm_frame.validity, tmp_16) ;
		
	SET_SHORT_BE(&gpsr_nav_tm_frame.pdop, p_tmp_ecef->PDOP) ;
	
	ftmp_32 = (float32)p_tmp_ecef->X ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.pos_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->Y ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.pos_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->Z ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.pos_z, tmp_32) ;
	
	ftmp_32 = (float32)p_tmp_ecef->VX ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.vel_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->VY ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.vel_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->VZ ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.vel_z, tmp_32) ;

	SET_LONG_BE(&gpsr_nav_tm_frame.oscill_offset, p_tmp_ecef->OscillOffset) ;
	
	SET_LONG_LONG_BE(&gpsr_nav_tm_frame.clock_bias[0], p_tmp_ecef->R) ;

	SET_LONG_BE(&gpsr_nav_tm_frame.health_of_satellites, p_tmp_ecef->Health) ;
	
	SET_LONG_BE(&gpsr_nav_tm_frame.visibility_of_satellites, p_tmp_ecef->Visibility) ;
	
	
// configuration setup message
	
	SET_SHORT_BE(&gpsr_nav_tm_frame.elevation_mask, pMainMgr->config.ElevMask) ;
	tmp_8 = pMainMgr->config.OPmode | ( pMainMgr->config.IonosphereModel << 1 ) | ( pMainMgr->config.TroposphereModel << 3 );
	gpsr_nav_tm_frame.op_mode_iono_tropo_flags = tmp_8 ;
	gpsr_nav_tm_frame.raim_threshold = pMainMgr->config.RAIMth ;
	gpsr_nav_tm_frame.cno_threshold_for_tracking = pMainMgr->config.SNRth ;
	
// Time and channel data
	p_time_ch = &pMainMgr->data.timeChStatus ;
	for ( i = 0 ; i < sizeof(gpsr_nav_tm_frame.channel)/sizeof(gpsr_nav_tm_frame.channel[0]) ; i ++ )
	{
		tSatelliteGPS *pSat;
		tNavTaskParams *navtask_params ;
		uint8 sat_id ;
		
		gpsr_nav_tm_frame.channel[i].sv_id = p_time_ch->satCh[i].sv_id ;
		gpsr_nav_tm_frame.channel[i].SNR = p_time_ch->satCh[i].SNR ;	
		gpsr_nav_tm_frame.channel[i].status = p_time_ch->satCh[i].status ;
		
// fix SNR & status

		sat_id = p_time_ch->satCh[i].sv_id ;
		pSat = SatMgr_GetSatellite(pSatMgr, sat_id);
		if (pSat != NULL)
		{
			if (pSat->gps_chan.status.state == es_TRACKING)
			{
				navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sat_id);
				if (navtask_params != NULL)
				{
					gpsr_nav_tm_frame.channel[i].SNR =  navtask_params->SNR_log ;
					
					if ((navtask_params->USED == 1) && (navtask_params->BAD_SAT == 0) && (navtask_params->ELEV_MSK == 0))
						gpsr_nav_tm_frame.channel[i].status |= 1;		// set bit 0
					else
						gpsr_nav_tm_frame.channel[i].status &= 0x1e;	// clear bit 0
						
					if ((navtask_params->paramsOP.USED_OP == 1) && (navtask_params->paramsOP.BAD_SAT_OP == 0) && (navtask_params->paramsOP.ELEV_MSK_OP == 0) )
						gpsr_nav_tm_frame.channel[i].status |= 0x10;	// set bit 4
					else
						gpsr_nav_tm_frame.channel[i].status &= 0x0f;	// clear bit 4
				}
			}
		}		
	}
	
// Boot status
	gpsr_nav_tm_frame.boot_status = boot_status ;
	
// ECEF OP navigation and status
#if 0
	/// this message forming in OP navigation task solution thread: if structure is blocking then data for it is ready and
	/// therefore message may be forming in this function
	p_tmp_opecef = &pMainMgr->data.BinData.OPnavMes.ecef ;
	if (pMainMgr->data.navDataOP.flag_bl == 0)
	{
		pMainMgr->data.navDataOP.flag_bl = 1;  /// blocking data
		_memcpy_cs(p_tmp_opecef, &pMainMgr->data.navDataOP.message.ecef, sizeof(tECEFOPNavStatus));
		pMainMgr->data.navDataOP.flag_bl = 0;  /// unblocking data
	}
	else  /// message forming
	{
		// navigation and status for OP
		MainMgr_OPNavAndStatusECEF(p_tmp_opecef, pNavTask->NavCond, pNavTask->User.CoordValid, pOPMgr->User_OP);
	}
#else
	p_tmp_opecef = &pMainMgr->data.navDataOP.message.ecef ;
#endif	

	SET_SHORT_BE(&gpsr_nav_tm_frame.op_nav_status, p_tmp_opecef->NavStatus) ;
	
	ftmp_32 = (float32)p_tmp_opecef->X ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.op_pos_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_opecef->Y ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.op_pos_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_opecef->Z ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.op_pos_z, tmp_32) ;
	
	ftmp_32 = (float32)p_tmp_opecef->VX ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.op_vel_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_opecef->VY ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.op_vel_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_opecef->VZ ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_nav_tm_frame.op_vel_z, tmp_32) ;
	
// GPS-UTC clock correction parameters
#if 0
	/// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
	if ((pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0) || (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag == 0))
		_memcpy(&pMainMgr->data.BinData.mesUtc, &pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.UTC, sizeof(RAW_UTC));
	else  /// if data is block then raw data is not available
		memset(&pMainMgr->data.BinData.mesUtc, 0, sizeof(RAW_UTC));
//#else

// GPS-UTC clock correction parameters
//	if(pSatMgr->TopSystem.IonUTC.flag_bl == 0)
//	{
//		pMainMgr->data.BinData.mesUtc.dtls = pSatMgr->TopSystem.IonUTC.dtls ;
//		pMainMgr->data.BinData.mesUtc.WNlsf = pSatMgr->TopSystem.IonUTC.WNlsf ;
//		pMainMgr->data.BinData.mesUtc.DN = pSatMgr->TopSystem.IonUTC.DN ;
//		pMainMgr->data.BinData.mesUtc.dtlsf = pSatMgr->TopSystem.IonUTC.dtlsf ;
//	}	
//	else
//		memset(&pMainMgr->data.BinData.mesUtc, 0, sizeof(RAW_UTC));

	SET_SHORT_BE(&gpsr_nav_tm_frame.delta_t_LS, pMainMgr->data.BinData.mesUtc.dtls) ;
	SET_SHORT_BE(&gpsr_nav_tm_frame.WN_LSF, pMainMgr->data.BinData.mesUtc.WNlsf) ;
	gpsr_nav_tm_frame.DN = pMainMgr->data.BinData.mesUtc.DN ;
	gpsr_nav_tm_frame.delta_t_LSF = pMainMgr->data.BinData.mesUtc.dtlsf ;
#endif		

	SET_SHORT_BE(&gpsr_nav_tm_frame.delta_t_LS, myUtc.dtls) ;
	SET_SHORT_BE(&gpsr_nav_tm_frame.WN_LSF, myUtc.WNlsf) ;
	gpsr_nav_tm_frame.DN = myUtc.DN ;
	gpsr_nav_tm_frame.delta_t_LSF = myUtc.dtlsf ;

// Tracking performance data
	SET_SHORT_BE(&gpsr_nav_tm_frame.max_utilization, pre_max_utilization) ;
	pre_max_utilization_time = 1234;
	SET_LONG_BE(&gpsr_nav_tm_frame.max_utilization_time, pre_max_utilization_time) ;
	for ( i = 0 ; i < sizeof(gpsr_nav_tm_frame.tracking_utilization)/sizeof(gpsr_nav_tm_frame.tracking_utilization[0]) ; i ++ )
		SET_SHORT_BE(&gpsr_nav_tm_frame.tracking_utilization[i], pre_tracking_utilization[i]) ;


	// report TC command status to CVI
	gpsr_nav_tm_frame.TC_count =  TC_count;
	gpsr_nav_tm_frame.TC_success_count = TC_success_count ;
		
// checksum
	p_frame = (uint8 *)&gpsr_nav_tm_frame ;
	tmp_8 = 0 ;
	for ( i = 0 ; i < TM_GPSR_NAV_TLM_FRAME_LENGTH - 1 ; i ++ )
		tmp_8 += p_frame[i] ;
	gpsr_nav_tm_frame.chksum = (- tmp_8) ;
	
// send it out
/*
#ifdef CAN_PORT_TMTC
	pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, (uint8 *)&gpsr_nav_tm_frame, TM_GPSR_NAV_TLM_CAN_FRAME_LENGTH);
#else
	pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, (uint8 *)&gpsr_nav_tm_frame, TM_GPSR_NAV_TLM_FRAME_LENGTH);
#endif
*/

	if(can_or_uart > OUTPUT_UART)
		pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, (uint8 *)&gpsr_nav_tm_frame, TM_GPSR_NAV_TLM_CAN_FRAME_LENGTH);
	else
		pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, (uint8 *)&gpsr_nav_tm_frame, TM_GPSR_NAV_TLM_FRAME_LENGTH);



}	

static void output_short_nav_tm(	tMainManager      *pMainMgr,
					tFramework        *pFramework,
                    tSatelliteManager *pSatMgr,
                    tNavTask          *pNavTask,
                    tOPManager        *pOPMgr)							 
{
	tECEFNavStatus *p_tmp_ecef ;
	tTimeChStatus *p_time_ch ;
	tECEFOPNavStatus *p_tmp_opecef ;
	uint64 tmp_64 ;
	uint32 tmp_32 ;
	uint16 tmp_16 ;
	float32 ftmp_32 ;
	uint8 tmp_8 ;
	int i ;
	uint8 * p_frame ;

	
// ECEF navigation and status data
#if 0		
	/// this message forming in navigation task solution thread: if structure is blocking then data for it is ready and
	/// therefore message may be forming in this function
	p_tmp_ecef = &pMainMgr->data.BinData.navMes.ecef ;
	if (pMainMgr->data.navData.flag_bl == 0)
	{
		pMainMgr->data.navData.flag_bl = 1;  /// blocking data
		_memcpy_cs(p_tmp_ecef, &pMainMgr->data.navData.message.ecef, sizeof(tECEFNavStatus));
		pMainMgr->data.navData.flag_bl = 0;  /// unblocking data
	}
	else  /// message forming
	{
		MainMgr_NavAndStatusECEF(p_tmp_ecef, pNavTask->User, pNavTask->NavCond);
	}
#else
	p_tmp_ecef = &pMainMgr->data.navData.message.ecef ;
#endif	
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.gps_week_number, p_tmp_ecef->wn) ;

	tmp_64 = (uint64)p_tmp_ecef->mks_l | ( ((uint64)p_tmp_ecef->mks_h << 16 ) & 0xffffffffffff0000) ;
	tmp_32 = tmp_64 / 1000 ;		// micro-second --> mili-second
	SET_LONG_BE(&gpsr_short_nav_tm_frame.gps_time, tmp_32) ;

	// validity
	if (pNavTask->NoNavTaskSol == fg_ON)
		tmp_16 = 0 ;
	else
	if (pNavTask->NavSolution == 2)
		tmp_16 = 1 ;
	else
		tmp_16 = 0 ;
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.validity, tmp_16) ;
		
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.pdop, p_tmp_ecef->PDOP) ;
	
	ftmp_32 = (float32)p_tmp_ecef->X ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_short_nav_tm_frame.pos_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->Y ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_short_nav_tm_frame.pos_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->Z ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_short_nav_tm_frame.pos_z, tmp_32) ;
	
	ftmp_32 = (float32)p_tmp_ecef->VX ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_short_nav_tm_frame.vel_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->VY ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_short_nav_tm_frame.vel_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->VZ ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_short_nav_tm_frame.vel_z, tmp_32) ;

	SET_LONG_BE(&gpsr_short_nav_tm_frame.oscill_offset, p_tmp_ecef->OscillOffset) ;
	
	SET_LONG_LONG_BE(&gpsr_short_nav_tm_frame.clock_bias[0], p_tmp_ecef->R) ;

	SET_LONG_BE(&gpsr_short_nav_tm_frame.health_of_satellites, p_tmp_ecef->Health) ;
	
	SET_LONG_BE(&gpsr_short_nav_tm_frame.visibility_of_satellites, p_tmp_ecef->Visibility) ;
	
	
// configuration setup message
	
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.elevation_mask, pMainMgr->config.ElevMask) ;
	tmp_8 = pMainMgr->config.OPmode | ( pMainMgr->config.IonosphereModel << 1 ) | ( pMainMgr->config.TroposphereModel << 3 );
	gpsr_short_nav_tm_frame.op_mode_iono_tropo_flags = tmp_8 ;
	gpsr_short_nav_tm_frame.raim_threshold = pMainMgr->config.RAIMth ;
	gpsr_short_nav_tm_frame.cno_threshold_for_tracking = pMainMgr->config.SNRth ;
	
// Time and channel data
	p_time_ch = &pMainMgr->data.timeChStatus ;
	for ( i = 0 ; i < sizeof(gpsr_short_nav_tm_frame.channel)/sizeof(gpsr_short_nav_tm_frame.channel[0]) ; i ++ )
	{
		tSatelliteGPS *pSat;
		tNavTaskParams *navtask_params ;
		uint8 sat_id ;
		
		gpsr_short_nav_tm_frame.channel[i].sv_id = p_time_ch->satCh[i].sv_id ;
		gpsr_short_nav_tm_frame.channel[i].SNR = p_time_ch->satCh[i].SNR ;	
		
// fix SNR & status

		sat_id = p_time_ch->satCh[i].sv_id ;
		pSat = SatMgr_GetSatellite(pSatMgr, sat_id);
		if (pSat != NULL)
		{
			if (pSat->gps_chan.status.state == es_TRACKING)
			{
				navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sat_id);
				if (navtask_params != NULL)
				{
					gpsr_short_nav_tm_frame.channel[i].SNR =  navtask_params->SNR_log ;					
				}
			}
		}		
	}
	
// Boot status
	gpsr_short_nav_tm_frame.boot_status = boot_status ;
	
	
// GPS-UTC clock correction parameters
#if 0
	/// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
	if ((pSatMgr->TopSystem.DecCommon.TempIonUTC.flag_bl == 0) || (pSatMgr->TopSystem.DecCommon.TempIonUTC.flag == 0))
		_memcpy(&pMainMgr->data.BinData.mesUtc, &pSatMgr->TopSystem.DecCommon.TempIonUTC.TIonUTC.UTC, sizeof(RAW_UTC));
	else  /// if data is block then raw data is not available
		memset(&pMainMgr->data.BinData.mesUtc, 0, sizeof(RAW_UTC));
//#else

// GPS-UTC clock correction parameters
//	if(pSatMgr->TopSystem.IonUTC.flag_bl == 0)
//	{
//		pMainMgr->data.BinData.mesUtc.dtls = pSatMgr->TopSystem.IonUTC.dtls ;
//		pMainMgr->data.BinData.mesUtc.WNlsf = pSatMgr->TopSystem.IonUTC.WNlsf ;
//		pMainMgr->data.BinData.mesUtc.DN = pSatMgr->TopSystem.IonUTC.DN ;
//		pMainMgr->data.BinData.mesUtc.dtlsf = pSatMgr->TopSystem.IonUTC.dtlsf ;
//	}	
//	else
//		memset(&pMainMgr->data.BinData.mesUtc, 0, sizeof(RAW_UTC));

	SET_SHORT_BE(&gpsr_short_nav_tm_frame.delta_t_LS, pMainMgr->data.BinData.mesUtc.dtls) ;
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.WN_LSF, pMainMgr->data.BinData.mesUtc.WNlsf) ;
	gpsr_short_nav_tm_frame.DN = pMainMgr->data.BinData.mesUtc.DN ;
	gpsr_short_nav_tm_frame.delta_t_LSF = pMainMgr->data.BinData.mesUtc.dtlsf ;
#endif		

	SET_SHORT_BE(&gpsr_short_nav_tm_frame.delta_t_LS, myUtc.dtls) ;
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.WN_LSF, myUtc.WNlsf) ;
	gpsr_short_nav_tm_frame.DN = myUtc.DN ;
	gpsr_short_nav_tm_frame.delta_t_LSF = myUtc.dtlsf ;
		
// checksum
	p_frame = (uint8 *)&gpsr_short_nav_tm_frame ;
	tmp_8 = 0 ;
	for ( i = 0 ; i < TM_GPSR_S_NAV_TLM_FRAME_LENGTH - 1 ; i ++ )
		tmp_8 += p_frame[i] ;
	gpsr_short_nav_tm_frame.chksum = (- tmp_8) ;
	
// send it out
	pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, (uint8 *)&gpsr_short_nav_tm_frame, TM_GPSR_S_NAV_TLM_FRAME_LENGTH);

}	

static void output_sci_tm(	tMainManager      *pMainMgr,
					tFramework        *pFramework,
                    tSatelliteManager *pSatMgr,
                    tNavTask          *pNavTask,
                    tOPManager        *pOPMgr)							 
{
	tECEFNavStatus *p_tmp_ecef ;
	tTimeChStatus *p_time_ch ;
	tECEFOPNavStatus *p_tmp_opecef ;
	uint64 tmp_64 ;
	uint32 tmp_32 ;
	uint16 tmp_16 ;
	float32 ftmp_32 ;
	uint8 tmp_8 ;
	int i ;
	uint8 * p_frame ;
	
	
// ECEF navigation and status data
	p_tmp_ecef = &pMainMgr->data.navData.message.ecef ;

	SET_SHORT_BE(&gpsr_sci_tm_frame.gps_week_number, p_tmp_ecef->wn) ;

	tmp_64 = (uint64)p_tmp_ecef->mks_l | ( ((uint64)p_tmp_ecef->mks_h << 16 ) & 0xffffffffffff0000) ;
	tmp_32 = tmp_64 / 1000 ;		// micro-second --> mili-second
	SET_LONG_BE(&gpsr_sci_tm_frame.gps_time, tmp_32) ;

	// validity
	if (pNavTask->NoNavTaskSol == fg_ON)
		tmp_16 = 0 ;
	else
	if (pNavTask->NavSolution == 2)
		tmp_16 = 1 ;
	else
		tmp_16 = 0 ;
	SET_SHORT_BE(&gpsr_sci_tm_frame.validity, tmp_16) ;
		
	SET_SHORT_BE(&gpsr_sci_tm_frame.pdop, p_tmp_ecef->PDOP) ;
	
	ftmp_32 = (float32)p_tmp_ecef->X ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_sci_tm_frame.pos_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->Y ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_sci_tm_frame.pos_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->Z ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_sci_tm_frame.pos_z, tmp_32) ;
	
	ftmp_32 = (float32)p_tmp_ecef->VX ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_sci_tm_frame.vel_x, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->VY ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_sci_tm_frame.vel_y, tmp_32) ;

	ftmp_32 = (float32)p_tmp_ecef->VZ ;
	ftmp_32 /= 1000.0 ;	// mm --> m
	tmp_32 = *(uint32 *)&ftmp_32 ;
	SET_LONG_BE(&gpsr_sci_tm_frame.vel_z, tmp_32) ;

	SET_LONG_BE(&gpsr_sci_tm_frame.oscill_offset, p_tmp_ecef->OscillOffset) ;
	
	SET_LONG_LONG_BE(&gpsr_sci_tm_frame.clock_bias[0], p_tmp_ecef->R) ;

	SET_LONG_BE(&gpsr_sci_tm_frame.health_of_satellites, p_tmp_ecef->Health) ;
	
	SET_LONG_BE(&gpsr_sci_tm_frame.visibility_of_satellites, p_tmp_ecef->Visibility) ;
	
	
// configuration setup message	
	SET_SHORT_BE(&gpsr_sci_tm_frame.elevation_mask, pMainMgr->config.ElevMask) ;
	gpsr_sci_tm_frame.raim_threshold = pMainMgr->config.RAIMth ;
	gpsr_sci_tm_frame.cno_threshold_for_tracking = pMainMgr->config.SNRth ;
	
// Time and channel data
	p_time_ch = &pMainMgr->data.timeChStatus ;
	for ( i = 0 ; i < sizeof(gpsr_sci_tm_frame.channel)/sizeof(gpsr_sci_tm_frame.channel[0]) ; i ++ )
	{
		tSatelliteGPS *pSat;
		tNavTaskParams *navtask_params ;
		uint8 sat_id ;

		// sv_id
		gpsr_sci_tm_frame.channel[i].sv_id = p_time_ch->satCh[i].sv_id ;
		
		sat_id = p_time_ch->satCh[i].sv_id ;
		if ( sat_id == 0 )
		{
			memset(&gpsr_sci_tm_frame.channel[i].sv_id, 0, sizeof(gpsr_sci_tm_frame.channel[0])) ;
			continue ;
		}
		else
		{
			pSat = SatMgr_GetSatellite(pSatMgr, sat_id);
			navtask_params = SatMgr_GetNavTaskParams(pSatMgr, sat_id);
		}
		
		if ( navtask_params != NULL )
		{
			// SNR
			gpsr_sci_tm_frame.channel[i].SNR =  navtask_params->SNR_log ;
			// amimuth
			tmp_16 = (int16)(navtask_params->sat_params.azim * (float32)rad_to_degree + 0.5);
			SET_SHORT_BE(&gpsr_sci_tm_frame.channel[i].amimuth, tmp_16) ;
			// elevation
			tmp_16 = (int16)(navtask_params->sat_params.elev * (float32)rad_to_degree + 0.5);
			SET_SHORT_BE(&gpsr_sci_tm_frame.channel[i].elevation, tmp_16) ;
			// time shift
			ftmp_32 = navtask_params->sat_coord.coord.dt_SV ;
			tmp_32 = *(uint32 *)&ftmp_32 ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].time_shift[0], tmp_32) ;
			// position
			ftmp_32 = navtask_params->sat_coord.coord.x ;
//			ftmp_32 /= 1000.0 ;	// mm --> m
			tmp_32 = *(uint32 *)&ftmp_32 ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].pos_x[0], tmp_32) ;
			ftmp_32 = navtask_params->sat_coord.coord.y ;
//			ftmp_32 /= 1000.0 ;	// mm --> m
			tmp_32 = *(uint32 *)&ftmp_32 ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].pos_y[0], tmp_32) ;
			ftmp_32 = navtask_params->sat_coord.coord.z ;
//			ftmp_32 /= 1000.0 ;	// mm --> m
			tmp_32 = *(uint32 *)&ftmp_32 ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].pos_z[0], tmp_32) ;
			// velocity
			ftmp_32 = navtask_params->sat_coord.coord.vx ;
//			ftmp_32 /= 1000.0 ;	// mm --> m
			tmp_32 = *(uint32 *)&ftmp_32 ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].vel_x[0], tmp_32) ;
			ftmp_32 = navtask_params->sat_coord.coord.vy ;
//			ftmp_32 /= 1000.0 ;	// mm --> m
			tmp_32 = *(uint32 *)&ftmp_32 ;			
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].vel_y[0], tmp_32) ;
			ftmp_32 = navtask_params->sat_coord.coord.vz ;
//			ftmp_32 /= 1000.0 ;	// mm --> m
			tmp_32 = *(uint32 *)&ftmp_32 ;			
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].vel_z[0], tmp_32) ;
		}
		else
		{
			gpsr_sci_tm_frame.channel[i].SNR = 0 ;
			tmp_16 = 0 ;
			SET_SHORT_BE(&gpsr_sci_tm_frame.channel[i].amimuth, tmp_16) ;
			SET_SHORT_BE(&gpsr_sci_tm_frame.channel[i].elevation, tmp_16) ;
			tmp_32 = 0 ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].time_shift[0], tmp_32) ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].pos_x[0], tmp_32) ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].pos_y[0], tmp_32) ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].pos_z[0], tmp_32) ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].vel_x[0], tmp_32) ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].vel_y[0], tmp_32) ;
			SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].vel_z[0], tmp_32) ;
		}
		
	// ps_delay, sec -> meter
		{
		#define C_LIGHT_Q0     (299792458)   ///< cLight
		int64 tmp_l;
		tLongFract mul;
		
		LongFractMPY(&pSat->raw_data_fix.ps_delay, C_LIGHT_Q0, &mul);
		tmp_l = ((int64)mul.integral<<16) | (int64)((mul.fract>>16)&0xffff);
		tmp_l /= 1000 ;
		SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].ps_delay[0], tmp_l) ;
		}
		
	// doppler & phase
		{
		int32 doppler ;
		uint16 phase ;
		
		doppler = ldexp(pSat->gps_chan.dopplerQ15, -15) ;		
		SET_LONG_BE(&gpsr_sci_tm_frame.channel[i].doppler[0], doppler) ;
		
		phase = ldexp(pSat->gps_chan.phase, -32) * 360 ;	// degree
		SET_SHORT_BE(&gpsr_sci_tm_frame.channel[i].phase, phase) ;
		}
		
		
	// position
		tmp_16 = pSat->gps_chan.position ;
		SET_SHORT_BE(&gpsr_sci_tm_frame.channel[i].position, tmp_16) ;
		
	}
		
// ECEF OP navigation and status
	p_tmp_opecef = &pMainMgr->data.navDataOP.message.ecef ;
	SET_SHORT_BE(&gpsr_sci_tm_frame.op_nav_status, p_tmp_opecef->NavStatus) ;
	
// GPS-UTC clock correction parameters
	SET_SHORT_BE(&gpsr_sci_tm_frame.delta_t_LS, myUtc.dtls) ;

// 20170809: Tom
// Latch Registers
	SET_SHORT_BE(&gpsr_sci_tm_frame.ms1_latch, pSatMgr->ms1_latch) ;
	SET_SHORT_BE(&gpsr_sci_tm_frame.ms1000_latch, pSatMgr->ms1000_latch) ;
	
// checksum
	p_frame = (uint8 *)&gpsr_sci_tm_frame ;
	tmp_8 = 0 ;
	for ( i = 0 ; i < TM_GPSR_SCI_TLM_FRAME_LENGTH - 1 ; i ++ )
		tmp_8 += p_frame[i] ;
	gpsr_sci_tm_frame.chksum = (- tmp_8) ;
	
// send it out
	pFramework->DebugTerminal.vtable.pfnWriteBinary(&pFramework->DebugTerminal.vtable, (uint8 *)&gpsr_sci_tm_frame, TM_GPSR_SCI_TLM_FRAME_LENGTH);
}							 
						 

typedef void(*Tm_Out_Fun)(tMainManager*,tFramework*,tSatelliteManager*,tNavTask*,tOPManager*);
static Tm_Out_Fun tm_output[3] = { output_nav_tm, output_sci_tm, output_short_nav_tm } ;

void output_new_tm_message(tMainManager      *pMainMgr,
                             tFramework        *pFramework,
                             tSatelliteManager *pSatMgr,
                             tNavTask          *pNavTask,
                             tOPManager        *pOPMgr)
{
	(*tm_output[tlm_type])(pMainMgr, pFramework, pSatMgr, pNavTask, pOPMgr) ;	
}


void process_gpsr_config_tc(tMainManager *pMainMgr, void *p_tc, uint8 tc_id)
{
	Tc_Frame_Gpsr_Config_1 *p1 ;
	Tc_Frame_Gpsr_Config_2 *p2 ;
	Tc_Frame_Gpsr_Config_3 *p3 ;
	
// preserve original setting	
	_memcpy_cs(&pMainMgr->config_new, &pMainMgr->config, sizeof(CONFIG_SETUP));
	
	switch ( tc_id )
	{
		case TC_GPSR_CONFIG_1_TYPE_ID:
			p1 = (Tc_Frame_Gpsr_Config_1 *)p_tc ;			
			pMainMgr->config_new.ElevMask = GET_SHORT_BE(&p1->elevation_mask[0]) ;
			pMainMgr->config_new.NavSolRate = GET_SHORT_BE(&p1->nav_solution_period[0]) ;
			pMainMgr->config_new.RAIMth = p1->raim_threshold ;
			pMainMgr->config_new.SNRth = p1->cno_threshold_for_tracking ;
			break ;
		case TC_GPSR_CONFIG_2_TYPE_ID:
			p2 = (Tc_Frame_Gpsr_Config_2 *)p_tc ;
			pMainMgr->config_new.ECEF = p2->ECEF_Geo ;
			pMainMgr->config_new.OPmode = p2->op_mode ;
			pMainMgr->config_new.IonosphereModel = p2->ionosphere_flag ;
			pMainMgr->config_new.OrbStatic = p2->op_model ;		
			break ;
		case TC_GPSR_CONFIG_3_TYPE_ID:
			p3 = (Tc_Frame_Gpsr_Config_3 *)p_tc ;
			pMainMgr->config_new.CoefDLL = GET_LONG_BE(&p3->DLL_time_parameter[0]) ;
			pMainMgr->config_new.DLLtime = p3->predetection_integrated_time ;
			pMainMgr->config_new.TroposphereModel = p3->troposphere_flag ;
			break ;
	}
// turn on new config flag	
	pMainMgr->NewConfig = fg_ON;
}

void process_write_rffe_reg_tc(Tc_Frame_Write_Rffe_Reg *p_tc)
{
	uint16 low_part, high_part ;
	
	low_part = GET_SHORT_BE(&p_tc->low_part) ;
	high_part = GET_SHORT_BE(&p_tc->high_part) ;
	
	*(volatile uint16 *)0x68000004 = low_part;  /// v.703 -  LSB
	delay_mcs(10);
	*(volatile uint16 *)0x68000006 = high_part;  /// v.703 -  MSB
	delay_mcs(200);
	
}

int process_soft_reset_tc(Tc_Frame_Gpsr_Soft_Reset *p_tc)
{
	if ( ( p_tc->reset_flag != 1 ) && ( p_tc->reset_flag != 2 ) )
		return 0;
	
	return p_tc->reset_flag ;
}


// BOOT CONFIG & IMAGE 
#define BOOT_CONFIG_ADDR	(0x64000F00)
#define IMAGE_EXT_ADDR		(0x64001000)
#define IMAGE_INT_ADDR		(0x64040800)

#define BOOT_CONFIG_LEN		(PAGE_SIZE)
//#define BOOT_CONFIG_LEN		(IMAGE_EXT_ADDR - BOOT_CONFIG_ADDR)
//#define BOOT_CONFIG_LEN		(sizeof(Boot_Config))
#define IMAGE_MAX_LEN	( IMAGE_INT_ADDR - IMAGE_EXT_ADDR )

#define BOOT_CONFIG_SIGNATURE	0x4F50534E	//"NSPO"
#define IMAGE_EXT_ID	0
#define IMAGE_INT_ID	1
#define IMAGE_EXT_CHKSUM	0xC0
#define IMAGE_INT_CHKSUM	0xBF

#define BOOT_STATUS_CFG_SETTING_BIT	0

typedef struct
{
	uint32 signature ;
	uint8 image_id ;
	uint8 chksum ;
}  Boot_Config ;

static Boot_Config default_boot_config[2] = {	{ BOOT_CONFIG_SIGNATURE, IMAGE_EXT_ID, IMAGE_EXT_CHKSUM },
												{ BOOT_CONFIG_SIGNATURE, IMAGE_INT_ID, IMAGE_INT_CHKSUM } };

												
#define EEPROM_MRAM

#ifdef EEPROM_MRAM
	#define BANK_SIZE	(512*1024)
	#define PAGE_SIZE	256
	#define	BANK_NUM	1
#else
	#define EEPROM_128K
	//#define EEPROM_512K

	#ifdef EEPROM_128K
		#define BANK_SIZE	(128*1024)	// 128K bytes, address bus: A0 ~ A16 (17 lines)
		#define PAGE_SIZE	128
		#define	BANK_NUM	4
	#endif

	#ifdef EEPROM_512K
		#define BANK_SIZE	(512*1024)	// 512K bytes, address bus: A0 ~ A18 (19 lines)
		#define PAGE_SIZE	256
		#define	BANK_NUM	1
	#endif
#endif			

#define BANK1_START	0x64000000	
#define MAX_POLLING_CNT	0x10000
											
static int FlashProgrammingArray(uint32 addr, int length, uint8 *data) 
{
  int i;
  uint32 base;
  uint32 addr0 = addr;
  	int cnt=0;
	int retry=1;


	if ( length > PAGE_SIZE )
	{
		return 0;
	}

#ifndef EEPROM_MRAM
	base = BANK1_START + (addr - BANK1_START) / BANK_SIZE * BANK_SIZE ;

  // Writing commands sequence
  *(volatile uint8 *)(base + 0x5555) = 0xaa;
  *(volatile uint8 *)(base + 0x2aaa) = 0x55;
  *(volatile uint8 *)(base + 0x5555) = 0xa0;
#endif

  for(i=0;i<length;i++)
  {
    *(volatile uint8 *)addr0 = data[i];
	addr0++;
  }

  addr0--;
  cnt=0;
  // Data# Polling bit and Exceeded Timing Limits bit analysis
  
  while (length)
  {
	retry = 1;
	while(retry)
	{
		uint8 DQ_flags;
		
		DQ_flags = *(volatile uint8 *)(addr0);
		if(DQ_flags == data[length-1])  // Data# Polling bit
		{
			length--;
			addr0--;
			retry = 0;
			cnt = 0;
		}
		else
			if (cnt++ >= MAX_POLLING_CNT)
				return 0;
	}
  }
  return 1;
}
												
void change_boot_config(int new_config)
{
	if ( ( new_config != IMAGE_EXT_ID ) && ( new_config != IMAGE_INT_ID ) )
		return ;
	if ( ((Boot_Config *)BOOT_CONFIG_ADDR)->image_id == new_config )
		return ;
		
	FlashProgrammingArray(BOOT_CONFIG_ADDR, BOOT_CONFIG_LEN, (uint8 *)&default_boot_config[new_config]) ;
	
	// if flash programming OK, update boot_status's boot config image id
	if ( ! memcmp((uint8 *)BOOT_CONFIG_ADDR, (uint8 *)&default_boot_config[new_config], sizeof(default_boot_config[0])) )
	{
		boot_status &= ~( 1 << BOOT_STATUS_CFG_SETTING_BIT ) ;
		boot_status |= new_config << BOOT_STATUS_CFG_SETTING_BIT ;
	}
}

#endif	// end NEW_TMTC
