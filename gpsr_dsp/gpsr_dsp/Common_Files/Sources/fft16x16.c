/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  function for calculation 4096-point FFT
 *    @file                   fft16x16.c
 *    @author                 TI Signal Processing Library
 *    @date                   13.06.2006
 *    @version                1.0
 */
/*****************************************************************************/

#include "fft16x16.h"
#include "Exp.h"

//==================================================================================
/// Function for calculation 4096-point FFT
///
/// @param[in]  n      - length of FFT in complex samples
/// @param[in]  ptr_x  - pointer to complex data input
/// @param[in]  ptr_w  - pointer to complex twiddle factor
/// @param[in]  ptr_y  - pointer to complex data output
/// @param[in]  brev   - pointer to bit reverse table containing 64 entries
/// @param[in]  n_min  - smallest FFT butterfly used in computation
/// @param[in]  offset - index in complex samples of sub-FFT from start of main FFT
/// @param[in]  n_max  - size of main FFT in complex samples
//==================================================================================
void DSP_fft16x16r_c(int           n,
                     short         ptr_x[],
                     short         ptr_w[],
                     short         ptr_y[],
                     unsigned char brev[],
                     int           n_min,
                     int           offset,
                     int           n_max)
 {
  int  i, j, k, l1, l2, h2, predj;
  int  tw_offset, stride, fft_jmp;

  short x0, x1, x2, x3,x4,x5,x6,x7;
  short xt0, yt0, xt1, yt1, xt2, yt2, yt3;
  short yt4, yt5, yt6, yt7;
  short si1,si2,si3,co1,co2,co3;
  short xh0,xh1,xh20,xh21,xl0,xl1,xl20,xl21;
  short x_0, x_1, x_l1, x_l1p1, x_h2 , x_h2p1, x_l2, x_l2p1;
  short xl0_0, xl1_0, xl0_1, xl1_1;
  short xh0_0, xh1_0, xh0_1, xh1_1;
  short *x,*w;
  int   k0, k1, j0, j1, l0, radix;
  short * y0, * ptr_x0, * ptr_x2;

  radix = n_min;

  stride = n;  // n is the number of complex samples
  tw_offset = 0;
  while (stride > radix)
  {
    j = 0;
    fft_jmp = stride + (stride>>1);
    h2 = stride>>1;
    l1 = stride;
    l2 = stride + (stride>>1);
    x = ptr_x;
    w = ptr_w + tw_offset;

    for (i = 0; i < n; i += 4)
    {
      co1 = w[j];
      si1 = w[j+1];
      co2 = w[j+2];
      si2 = w[j+3];
      co3 = w[j+4];
      si3 = w[j+5];

      x_0    = x[0];
      x_1    = x[1];
      x_h2   = x[h2];
      x_h2p1 = x[h2+1];
      x_l1   = x[l1];
      x_l1p1 = x[l1+1];
      x_l2   = x[l2];
      x_l2p1 = x[l2+1];

      xh0  = x_0    + x_l1;
      xh1  = x_1    + x_l1p1;
      xl0  = x_0    - x_l1;
      xl1  = x_1    - x_l1p1;

      xh20 = x_h2   + x_l2;
      xh21 = x_h2p1 + x_l2p1;
      xl20 = x_h2   - x_l2;
      xl21 = x_h2p1 - x_l2p1;

      ptr_x0 = x;
      ptr_x0[0] = ((short) (xh0 + xh20))>>1; //can be changed to 2
      ptr_x0[1] = ((short) (xh1 + xh21))>>1; // can be changed to 2

      ptr_x2 = ptr_x0;
      x += 2;
      j += 6;
      predj = (j - fft_jmp);
      if (!predj) x += fft_jmp;
      if (!predj) j = 0;

      xt0 = xh0 - xh20;
      yt0 = xh1 - xh21;
      xt1 = xl0 + xl21;
      yt2 = xl1 + xl20;
      xt2 = xl0 - xl21;
      yt1 = xl1 - xl20;

      ptr_x2[l1  ] = (xt1 * co1 + yt1 * si1 + 0x8000)>>16;
      ptr_x2[l1+1] = (yt1 * co1 - xt1 * si1 + 0x8000)>>16;
      ptr_x2[h2  ] = (xt0 * co2 + yt0 * si2 + 0x8000)>>16;
      ptr_x2[h2+1] = (yt0 * co2 - xt0 * si2 + 0x8000)>>16;
      ptr_x2[l2  ] = (xt2 * co3 + yt2 * si3 + 0x8000)>>16;
      ptr_x2[l2+1] = (yt2 * co3 - xt2 * si3 + 0x8000)>>16;
    }
    tw_offset += fft_jmp;
    stride = stride>>2;
  }  // end while

  j = offset>>2; 

  ptr_x0 = ptr_x;
  y0 = ptr_y;
  l0 = Exp0(n_max) - 17;  // get size of FFT _norm

  if (radix <= 4) for (i = 0; i < n; i += 4)
  {
    // reversal computation

    j0 = (j) & 0x3F;
    j1 = (j >> 6) & 0x3F;
    k0 = brev[j0];
    k1 = brev[j1];
    k = (k0 << 6) |  k1;
    k = k >> l0;
    j++;  // multiple of 4 index

    x0   = ptr_x0[0];  x1 = ptr_x0[1];
    x2   = ptr_x0[2];  x3 = ptr_x0[3];
    x4   = ptr_x0[4];  x5 = ptr_x0[5];
    x6   = ptr_x0[6];  x7 = ptr_x0[7];
    ptr_x0 += 8;

    xh0_0  = x0 + x4;
    xh1_0  = x1 + x5;
    xh0_1  = x2 + x6;
    xh1_1  = x3 + x7;

    if (radix == 2) 
    {
      xh0_0 = x0;
      xh1_0 = x1;
      xh0_1 = x2;
      xh1_1 = x3;
    }

    yt0  = xh0_0 + xh0_1;
    yt1  = xh1_0 + xh1_1;
    yt4  = xh0_0 - xh0_1;
    yt5  = xh1_0 - xh1_1;

    xl0_0  = x0 - x4;
    xl1_0  = x1 - x5;
    xl0_1  = x2 - x6;
    xl1_1  = x3 - x7;

    if (radix == 2) 
    {
      xl0_0 = x4;
      xl1_0 = x5;
      xl1_1 = x6;
      xl0_1 = x7;
    }

    yt2  = xl0_0 + xl1_1;
    yt3  = xl1_0 - xl0_1;
    yt6  = xl0_0 - xl1_1;
    yt7  = xl1_0 + xl0_1;

    if (radix == 2) 
    {
      yt7  = xl1_0 - xl0_1;
      yt3  = xl1_0 + xl0_1;
    }

    y0[k] = yt0; y0[k+1] = yt1;
    k += n>>1;
    y0[k] = yt2; y0[k+1] = yt3;
    k += n>>1;
    y0[k] = yt4; y0[k+1] = yt5;
    k += n>>1;
    y0[k] = yt6; y0[k+1] = yt7;
  }
}



