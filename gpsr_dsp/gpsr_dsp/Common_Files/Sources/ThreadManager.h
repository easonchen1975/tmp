/*****************************************************************************
*    Copyright (c) 2003 Spirit Corp.
******************************************************************************/
/**  
*   @brief                  Simulator of DSP threads, HWI or SWI
*   @file                   ThreadManager.h
*   @author                 Alex G. Nazarov
*   @version                1.0
*/
/*  Each thread can be either periodic or non-periodic. 
*   All periodic threads are called with given relative frequency. Relative 
*   call frequency is set as a fraction of 2 integral numbers. Non-periodic 
*   threads are post themselves automatically. Another type of periodic threads 
*   is SWI-like procedures, which never called automatically, but can be posted 
*   explicitly using ThreadManagerPost() function.
*   Threads can preempt each other. Threads with bigger priority preempt 
*   actually executed threads with the same or lesser priority. Lesser priority 
*   thread can not preempt threads with higher priority. Thread can disable 
*   preemption temporarily for the short time to make some operation under the 
*   data shared with another threads.
*
* VERY IMPORTANT LIMITATION:
*   Since file I/O from standard C-library does not work correctly in multithread 
*   environment, make all calls of I/O routines inside block 
*   ThreadManagerDisablePreemption () / ThreadManagerEnablePreemption ().
*   Function printf() can be used without such limitation.
*
* Functions:
*   ThreadManagerInit        Initialize thread manager with descriptors of 
*                            threads/HWI/SWI
*   ThreadManagerStart       post start command to all threads
*   ThreadManagerStop        stop all threads
*   ThreadManagerClose       delete all threads
*   ThreadManagerDisablePreemption 
*                            Disable preemption of caller thread by another 
*                            threads
*   ThreadManagerEnablePreemption  
*                            Enable preemption of caller thread by 
*                            another threads
*   ThreadManagerPost        Post signal to the given thread
*   ThreadManagerWait        Wait until given thread will not finished
*   ThreadManagerChangePeriod Change Period of given thread
*
*****************************************************************************/
#ifndef THREADMANAGER_H_HH
#define THREADMANAGER_H_HH


#ifndef USE_DSPBIOS
  #include <stdlib.h>
  #include <stddef.h>
  #undef EXTERN_C   // prevent warning on EXTERN_C redefinition because windows.h has the same define
  #include <windows.h>
  #include "comtypes.h"
  #undef EXTERN_C   // prevent warning on EXTERN_C redefinition because windows.h has the same define
  #define EXTERN_C externalCprogram
#else
  // TO DO Make path to be relative
  //#include "Target\\GPSReceiver\\GPSReceiverDSPcfg.h"
  #include "gpsrcfg.h"
  #include "..\gpsrcfg_csl.h"
  #include <swi.h>
#endif

  #include "comtypes.h"

struct tagThread;
typedef int (*fnThreadProc)(void*, int threadID); // Thread function

#ifndef USE_DSPBIOS
  typedef int  tThreadID;
#else
  #define  CRITICAL_SECTION  uint32
  typedef SWI_Handle  tThreadID;
  #define HANDLE  uint32
  #define DWORD   uint32
#endif

//=========================================================
//  thread descriptor
//=========================================================/
typedef struct
{
  fnThreadProc  fnProc;    // thread function 
  int           priority;  // 0 - idle, 15 -- highest
  tThreadID     id;        // unique ID for this thread
  struct
  {
    int32 Modulo;
    int32 Step;
  } period;     // period of call, {0,0} if it should be 
                // called with no interruption. Otherwise, 
                // thread will be called each Modulo/Step 
                // thread ticks
  char pName[80];
} tThreadParams;


struct tagThreadManager;
//=========================================================
//  context of particular thread
//=========================================================
typedef struct
{
  HANDLE            DoEvent;          // event to call the thread
  HANDLE            WaitingEvent;     // event which is set during thread release
  HANDLE            ThreadDescr;      // thread handle
  HANDLE            StoppedEvent;     // event to indicate that thread has finished
  DWORD             ThreadId;         // Windows thread ID
  tThreadParams     ThreadParams;     // thread parameters
  struct tagThreadManager    *pThreadManager;
  int32             tickCounter;      // tick counter for thread switching
  // statistics
  int32             callCount;        // call count
  int32             preemptionCount;  // preemption count
  //
  int               index;
}
tThreadContext;

//=========================================================
//  thread manager
//=========================================================
typedef struct tagThreadManager
{
#ifndef USE_DSPBIOS
  tThreadContext   ThreadContext[16]; // up to 16 threads
  int              NThreads;          // number of threads
  CRITICAL_SECTION cs;                // critical section for switching protection 
  HANDLE           DoEvent;           // event to call the thread
  HANDLE           StopEvent;         // event to kill threads
  HANDLE           StoppedEvent;      // event to indicate that task switcher is finished
  HANDLE           EnabledEvent;
  HANDLE           TaskSwitcher;      // handle for task switcher
  // stack of called threads (on IDs)
  volatile struct
  {
    tThreadContext* aThreads[16];    // currently executed threads
    int             count;           // count in the stack
    int             maxCount;        // maximum count: for statistics only
  }                CallStack;
  volatile tThreadContext*  pLastThread; // the thread which has called last
#endif
  void             *pInstance;          // instance for all threads
}
tThreadManager;


//=====================================================================================
///  Initialize thread manager with descriptors of threads/HWI/SWI
//
/// @param[in]  pThreadManager          thread manager instance
/// @param[in]  pThreadParams[NThreads] thread descriptor
/// @param[in]  NThreads                number of threads
/// @param[in]  pInstance               parameter which will be passed to each thread
//=====================================================================================
externalCprogram void ThreadManagerInit (tThreadManager       *pThreadManager,
                                         const tThreadParams*  pThreadParams, 
                                         int                   NThreads,
                                         void                 *pInstance);

//=====================================================================================
///  Post start command to all threads
//
/// @param[in]  pThreadManager      thread manager instance
//=====================================================================================
externalCprogram void ThreadManagerStart (tThreadManager *pThreadManager);

//=====================================================================================
///  Stop all threads
//
// @param[in]  pThreadManager      thread manager instance
//=====================================================================================
externalCprogram void ThreadManagerStop (tThreadManager *pThreadManager);

//=====================================================================================
///  Delete all threads
//
/// @param[in]  pThreadManager      thread manager instance
//=====================================================================================
externalCprogram void ThreadManagerClose (tThreadManager *pThreadManager);

//=====================================================================================
//  Thread manipulations
//
//  Disable preemption of caller thread by another threads. Should be 
//  used throughly and time interval between disabling and enabling 
//  preemption should be as short as possible !!
//
//  pThreadManager      thread manager instance
//=====================================================================================
#ifndef USE_DSPBIOS
  externalCprogram void ThreadManagerDisablePreemption (tThreadManager *pThreadManager);
#else
inline_ void ThreadManagerDisablePreemption (tThreadManager *pThreadManager)
{
     SWI_disable();
}
#endif

//=====================================================================================
///  Enable preemption of caller thread by another threads. Should be 
///  used in pair with ThreadManagerDisablePreemption() with the same 
//
/// @param[in]  pThreadManager      thread manager instance
//=====================================================================================
#ifndef USE_DSPBIOS
  externalCprogram void ThreadManagerEnablePreemption  (tThreadManager *pThreadManager);
#else
inline_ void ThreadManagerEnablePreemption  (tThreadManager *pThreadManager)
{
  SWI_enable();
}

#endif

//=====================================================================================
///  Post signal to the given thread. Should be used with non-periodic 
///  threads only !
//
/// @param[in]  pThreadManager      thread manager instance
/// @param[in]  threadID            ID of the thread 
//=====================================================================================

#ifndef USE_DSPBIOS
  externalCprogram void ThreadManagerPost (tThreadManager *pThreadManager, tThreadID threadID);
#else
inline_ void ThreadManagerPost (tThreadManager *pThreadManager, tThreadID threadID)
{
  SWI_post(threadID);
}
#endif

//=====================================================================================
///  Wait until given thread will not finished.
//
/// @param[in]  pThreadManager      thread manager instance
/// @param[in]  threadID            ID of the thread 
//=====================================================================================
#ifndef USE_DSPBIOS
  externalCprogram void ThreadManagerWait (tThreadManager *pThreadManager, tThreadID threadID);
#else
inline_   void ThreadManagerWait (tThreadManager *pThreadManager, tThreadID threadID) {}
#endif  

//=====================================================================================
///  Check given thread for closing. Return 1 if thread has exited
///  Also, it returns 1 if thread is not found
//
// @param[in]  pThreadManager      thread manager instance
// @param[in]  threadID            ID of thread 
//=====================================================================================
externalCprogram int ThreadManagerIsThreadClosed (tThreadManager *pThreadManager, int threadID);

//=====================================================================================
///  Print threads status
//
/// @param[in]  pThreadManager      thread manager instance
//=====================================================================================
externalCprogram void ThreadManagerPrintStatus (tThreadManager *pThreadManager, char* fname);


//=====================================================================================
///  Change period if given thread
//
/// @param[in]  pThreadManager      thread manager instance
/// @param[in]  id                  id of thread
//=====================================================================================
externalCprogram void ThreadManagerChangePeriod(tThreadManager *pThreadManager,
                                                tThreadID       threadID,
                                                int32           Modulo,
                                                int32           Step);

//=====================================================================================

//=====================================================================================
externalCprogram void ThreadManagerPause(int bPaused);

#endif // THREADMANAGER_H_HH
