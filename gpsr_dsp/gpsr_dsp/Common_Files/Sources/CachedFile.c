/*****************************************************************************
*    Copyright (c) 2001-2002 Spirit Corp.
******************************************************************************
*
*    Project: ............   HF modem MIL-STD-188-110A
*    Title: ..............   Cached file operations
*    File name: ..........   CachedFile.c
*    Authors: ............   Alex G. Nazarov
*    Revision ............   1.0
*
*    Subroutines:
*	   CachedFileOpen     open file or returns handle of file if it has been 
*                       already opened
*	   CachedFileClose    close cached file
*	   CachedFileRemove   remove file and delete appropriate entry in the cached 
*                       list
*    CachedFileCloseAll close all cached files
*
*    Internal routines:
*    CachedFileAutoClose  close cached files on timeout
*
*****************************************************************************/

#include <time.h>
#include <string.h>
#include <stdio.h>

#include "CachedFile.h"

//  forward static declarations
//-----------------------------
static void  CachedFileAutoClose (void);

typedef struct
{
  FILE*   FileHandle;   // file handle
  clock_t TimeToClose;  // time to close file automatically
  char    FileName[30]; // name of opened file
}
tCachedFile;

#define EXPIRATION_TIME 5   // 5 seconds to live a file

static tCachedFile CachedFiles[10] = {{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}};

/****************************************************************************
  Description: open file or returns handle of file if it has been already opened 
 
  Input:
  filename       name of file to be opened

  returns file handle
****************************************************************************/
FILE* CachedFileOpen (const char* filename)
{
  FILE* f;
  int i;
    // scan through opened files
  for (i=0; i<sizeof(CachedFiles)/sizeof(CachedFiles[0]); i++)
  {
    if (CachedFiles[i].FileHandle==NULL) continue;
    if (strcmp(filename,CachedFiles[i].FileName)==0)
      return CachedFiles[i].FileHandle;
  }
  f = fopen (filename, "at+");
    // try to save in a cached list
  if (strlen(filename)<sizeof(CachedFiles[i].FileName))
  {
    for (i=0; i<sizeof(CachedFiles)/sizeof(CachedFiles[0]); i++)
    {
      if (CachedFiles[i].FileHandle==NULL) 
      {
        CachedFiles[i].FileHandle = f;
        strcpy (CachedFiles[i].FileName,filename);
        CachedFiles[i].TimeToClose = clock() + (clock_t)(CLOCKS_PER_SEC*EXPIRATION_TIME);
        break;
      }
    }
  }
  return f;
}


/****************************************************************************
  Description: close cached file
 
  Input:
  f       file handle to be closed
****************************************************************************/
void  CachedFileClose (FILE *f)
{
  int i;
  if (f) 
  {
      // scan through opened files
    for (i=0; i<sizeof(CachedFiles)/sizeof(CachedFiles[0]); i++)
    {
      if (CachedFiles[i].FileHandle==f) 
      {
        f = NULL;
        break;
      }
    }
    // whe file is not in list so close it
    if (f) fclose(f);
  }
  CachedFileAutoClose();
}

/****************************************************************************
  Description: close all cached files
****************************************************************************/
void  CachedFileCloseAll ()
{
  int i;
    // scan through opened files
  for (i=0; i<sizeof(CachedFiles)/sizeof(CachedFiles[0]); i++)
  {
    if (CachedFiles[i].FileHandle) 
    {
      fclose(CachedFiles[i].FileHandle);
      CachedFiles[i].FileHandle = NULL;
    }
  }
}


/****************************************************************************
  Description: remove file and delete appropriate entry in the cached list
 
  Input:
  filename       name of file to be removed
****************************************************************************/
void CachedFileRemove (const char* filename)
{
  FILE* f = NULL;
  int i;
    // scan through opened files
  for (i=0; i<sizeof(CachedFiles)/sizeof(CachedFiles[0]); i++)
  {
    if (CachedFiles[i].FileHandle==NULL) continue;
    if (strcmp(filename,CachedFiles[i].FileName)==0)
    {    // close and remove from the list
      fclose(CachedFiles[i].FileHandle);
      CachedFiles[i].FileHandle = NULL;
      break;
    }
  }
  f = fopen (filename, "wt");
  if (f) fclose(f);
  CachedFileAutoClose();
}

/****************************************************************************
  Description: close cached files on timeout
****************************************************************************/
static void CachedFileAutoClose (void)
{
  clock_t time=clock();
  int i;
    // scan through opened files
  for (i=0; i<sizeof(CachedFiles)/sizeof(CachedFiles[0]); i++)
  {
    if (CachedFiles[i].FileHandle==NULL) continue;
    if ( (int)(time-CachedFiles[i].TimeToClose)>=0) 
    {               // close file on timeout expiration
      fclose(CachedFiles[i].FileHandle);
      CachedFiles[i].FileHandle = NULL;
    }
  }
}

