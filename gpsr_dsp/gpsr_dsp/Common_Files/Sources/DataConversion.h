/*****************************************************************************
*    Copyright (c) 2007 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  control functions
*    @file                   DataConversion.h
*    @author                 D.Churikov
*    @date                   25.05.2007
*    @version                1.0
******************************************************************************/
#if !defined(__DATACONVERSION_H)
#define __DATACONVERSION_H

#include"comtypes.h"
#include"DataForm.h"


//========================================================================
/// @brief     User ECEF coordinates transformation into geographic system
///
/// @param[out] geo - pointer to tLLA object
/// @param[in] TOPO -  TOPO matrix
/// @param[in] ecef - pointer to tECEF object

//========================================================================
EXTERN_C void ECEF_to_Geoid(tLLA *geo, tECEF * ecef, float64  TOPO[3][3]);

//========================================================================
/// @brief     User geographic coordinates transformation into 
///            ECEF & TOPO matrix calculation
///
/// @param[out] ecef - pointer to tECEF object
/// @param[out] TOPO -  TOPO matrix
/// @param[in] geo - pointer to tLLA object
//========================================================================
EXTERN_C void Geoid_to_ECEF(tECEF * ecef, float64 TOPO[3][3], tLLA *geo);


#endif

