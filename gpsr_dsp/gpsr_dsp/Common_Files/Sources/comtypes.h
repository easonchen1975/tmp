/*************************************************************
 *   (C) SPIRIT Corp 2000-2005 
 *************************************************************/
/**
*  @file       comtypes.h
*  @brief      Set of commonly used type definition
*              More or less platform independent
*/
/*************************************************************/

/*  Defines following types:
*  int8        8-bit signed integer
*  int16       16-bit signed integer
*  int32       32-bit signed integer
*  uint8       8-bit unsigned integer
*  uint16      16-bit unsigned integer
*  uint32      32-bit unsigned integer
*  tComplex    double precision complex data type
*  tShortReal  double type substitution
*  bool,tBool  boolean type definition
*
*  Defines following macros:
*  EXTERN_C    declaration modifier for external routines and a data
*  inline_     inline definition
*  ASSERT      Assertion support
*
*  Defines following constants:
*  M_PI        pi number
*/
#ifndef __COMTYPES_AGN_H
#define __COMTYPES_AGN_H

#ifdef  _XDAS
#include "xdasname.h"
#ifdef  _SPCORPLIB
#include "lib_spcorp.h"
#endif  /* _SPCORPLIB */
#endif  /* _XDAS      */
/*****************************************
  correct definition for external
  routines and a data) insert EXTERN_C
  before any externally accessibly
  identifiers
******************************************/
#ifdef __cplusplus
#undef externalCprogram
#define externalCprogram extern "C"
#else
#undef externalCprogram
#define externalCprogram extern
#endif
#undef EXTERN_C
#define EXTERN_C externalCprogram

/******************************************
  true inline definition
*******************************************/
#ifdef _MSC_VER
#undef inline_
#define inline_ __inline
#else
#undef inline_
#define inline_ static inline
#endif

/******************************************
  assertion support
*******************************************/
#ifndef _TMS320C6X

#ifndef ASSERT
#include <assert.h>
#ifdef _DEBUG
#define ASSERT(x)  assert(x)
 #ifdef  NOASSERT
  #undef  ASSERT
  #define ASSERT(_ignore)  ((void)0)
 #endif  /* NOASSERT */
#else
#undef  ASSERT
#define ASSERT(_ignore)  ((void)0)
#endif  /* _DEBUG */
#endif  /* ASSERT */

/******************************************
*******************************************/
#ifdef _MMX_ENABLE
  #define MMX_CALL _fastcall
#else
  #define MMX_CALL
#endif

#else /* _TMS320C6X */

#define ASSERT(_ignore) ((void)0)

#endif /* _TMS320C6X */

#ifndef __COMTYPES_H
#define __COMTYPES_H

/******************************************
  True types definition
  default double and float type accuracy
*******************************************/
#define _DOUBLE_BITS  64
#define _FLOAT_BITS   32

/******************************************
  define compiler and target specific
  types
*******************************************/
#ifdef _TMS320C5XX
  #define __CODECOMPOSER_C5XX 1
  // on c54,c55 double and float types are coincided and are of 32 bits !
  #undef _DOUBLE_BITS  
  #undef _FLOAT_BITS
  #define _DOUBLE_BITS  32
  #define _FLOAT_BITS   32
#endif

#ifdef CHIP_6416 //_TMS320C6XX
  #define __CODECOMPOSER_C6XX 1
  // on c54,c55 double and float types are coincided and are of 32 bits !
  #undef _DOUBLE_BITS  
  #undef _FLOAT_BITS
  #define _DOUBLE_BITS  32
  #define _FLOAT_BITS   32
#endif

#if __BORLANDC__
     #define DEF_INT_SIZE 16
#elif __WIN32__ || __DJGPP__
     #define DEF_INT_SIZE 32
#elif _MSC_VER /* VCC */
  #define DEF_INT_SIZE 32
  #ifndef _WINDOWS
  // typedef unsigned char bool;
  #endif
  #define false 0
  #define true 1
#elif __CODECOMPOSER_C5XX
  #define DEF_INT_SIZE 16
  #ifndef false 
    #define false 0
  #endif  // false 
  #ifndef true
    #define true 1
  #endif  // true
  typedef unsigned char bool;
#elif __CODECOMPOSER_C6XX
  #define DEF_INT_SIZE 32
  #ifndef false 
    #define false 0
  #endif  // false 
  #ifndef true
    #define true 1
  #endif  // true
  typedef unsigned char bool;
#else
#error unknow compiler
#endif //compiler type

/******************************************
  Common character types
*******************************************/
  typedef char int8;
  typedef unsigned char uint8;

/******************************************
  16 and 32 bit integer types 
*******************************************/
#if DEF_INT_SIZE==16
  typedef unsigned long uint32;
  typedef unsigned int  uint16;
  typedef long int32;
  typedef int  int16;

#else 
  /* DEF_INT_SIZE!=16*/
  #if __DJGPP__ || __MINGW32__
    typedef unsigned long uint32;
    typedef unsigned short uint16;
    typedef long int32;
    typedef short int16;
  #elif _MSC_VER /* VCC*/
    typedef unsigned __int32 uint32;
    typedef unsigned __int16 uint16;
    typedef unsigned __int64 uint64;
    typedef __int32 int32;
    typedef __int16 int16;
    typedef __int64 int64;
    typedef float   float32;
    typedef double  float64;
    #ifndef __cplusplus
      typedef unsigned char bool;
    #endif //__cplusplus
  #elif __CODECOMPOSER_C6XX
      typedef unsigned short     uint16;
      typedef unsigned int       uint32;
      typedef unsigned long      uint40;
      typedef short              int16;
      typedef int                int32;
      typedef long               int40;
      typedef float              float32;
      typedef double             float64;
      typedef unsigned long long uint64;
      typedef long long          int64;
  #else
    typedef unsigned long uint32;
    typedef unsigned short uint16;
    typedef long int32;
    typedef short int16;
  #endif

#endif //DEF_INT_SIZE==16

/******************************************
  double precision complex data type
*******************************************/
typedef struct 
{
  double re,im;
} tComplex;

/******************************************
  generic 8-bit complex type
*******************************************/
typedef struct
{
  int8 re;
  int8 im;
}
tComplex8;

/******************************************
  generic aligned 8-bit complex type
*******************************************/
typedef union
{
  tComplex8  c;
  int16      d;
}
tComplex8Aligned;

/******************************************
  generic 16-bit complex type
*******************************************/
typedef struct
{
  int16 re;
  int16 im;
}
tComplex16;

/******************************************
  generic aligned 16-bit complex type
*******************************************/
typedef union
{
  tComplex16  c;
  int32       dummy;
}
tComplex16Aligned;

/*******************************************
  generic aligned 64bit 16-bit complex type
********************************************/
typedef union
{
  tComplex16  c[2];
  int32       w[2];
  double      dummy;
}
tComplex16Aligned64;

/******************************************
  generic generic 32-bit complex type
*******************************************/
typedef struct
{
  int32 re;
  int32 im;
}
tComplex32;

/******************************************
  enum for flag
*******************************************/
typedef enum Flag
{
  fg_ON  = 1,
  fg_OFF = 0
}
eFlag;

/******************************************
  generic fractional type Q32
*******************************************/
typedef struct
{
  int32   integral;  // integral part
  uint32  fract;     // fractional part 
}
tLongFract;

/******************************************
  generic fractional type Q48
*******************************************/
typedef struct
{
  int16   integral;  // integral part 
  uint16  fract_0;   // fractional part 16 bit
  uint32  fract_1;   // fractional part 32 bit
}
tLongFract48;

/******************************************
  generic 64-bit type
*******************************************/
typedef struct
{
  uint32 lo;
  int32  hi;
}
tLoHi64;

/******************************************
  generic aligned fractional type Q32
*******************************************/
typedef union
{
#ifdef _TMS320C6X
  double  dbl;
#else
  int64   dbl;
#endif
  tLoHi64 lf;
}
tLongFractDouble;

/******************************************
  generic aligned 64-bit type
*******************************************/
typedef union
{
  tLoHi64  c;
  int64    d;
}
tI64;

/******************************************
  Type definition fo double word memory
  access emulation
  (using LDDW/STDW instruction)
*******************************************/
#ifdef _TMS320C6X
  // Alias for C64's double type
  typedef double tDoubleWord;
#else 

// Data types models C64's double word memory access 
typedef struct
{
  int32 lo;  // for LITTLE ENDIAN format !!!!
  int32 hi;
} tDoubleWord;

#endif  // _TMS320C6X

typedef union
{
  double  d;
  int32   i[2];
} 
tDouble;

/******************************************
  Generates no code. Tells the optimizer
  that the expression declared with the 
  assert function is true. This gives a 
  hint to the compiler as to what 
  optimizations might be valid 
  (using word�wide optimizations).
*******************************************/
#ifdef _TMS320C6X
#else
#define restrict 
#endif

/******************************************
  common constants
*******************************************/
#ifndef M_PI
#if !(__DJGPP__ || __MINGW32__)
#define M_PI  (3.1415926535897932384626433832795e0)
#define M_2PI (6.28318530717958647693)
#define M_PI_2  1.57079632679489661923
#define M_1_PI  0.318309886183790671538
#endif
#endif
/* size of array*/
#define SIZE(array) (sizeof(array)/sizeof(array[0]))

#endif //__COMTYPES_H

#endif //__COMTYPES_AGN_H
