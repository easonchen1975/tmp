
#ifndef _CAN_DRV_H_
#define _CAN_DRV_H_
 
#include "comtypes.h"
#include "terminal.h"

#define CAN_SOFTWARE_CONTROL_VERSION 20180820	// Henry's

/* CAN error code declarations */
#define DRV_CAN_OK				1
#define DRV_CAN_PAR_ERROR		-1	// parameter error
#define DRV_CAN_RES_CONFLICT	-2	// resource conflict
#define DRV_CAN_HW_NOTREADY		-3	// HW not ready
#define DRV_CAN_BUFFER			-4	// rx buffer empty or tx buffer full
#define DRV_CAN_ERROR_FRM		-5	// error frame


#define CAN_A_SIDE
//#define CAN_B_SIDE


int can_init() ;
int can_start(void) ;
int can_open(uint32 txid, uint32 rxid) ;
int can_rx_frame(int vport, uint8 *rx_frame) ;
int can_tx_frame(int vport, uint8 *tx_frame, int32 len) ;

int can_loopback_test() ;

int can_tmtc_init(void);
int can_send_tm(tTerminal *pTerminal);
int can_rcv_tc(tTerminal *pTerminal);


#endif
