/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Almanac, ephemeris, FPGA and System file, 
 *                           configuration setup read/write to FLASH
 *   @file                   BuiltInTest.c
 *   @author                 M. Zhokhova
 *   @date                   25.08.2006
 *   @version                2.0
 */
/*****************************************************************************/
#include "DataForm.h"
#include "baseop.h"
#include "comfuncs.h"
#include "MainManager.h"

//===========================================================
/// Write System files in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
//===========================================================
void MainMgr_UploadFile(tFlashDrv *pFlashDrv)
{
  uint8         res_save;   ///< result saving
  uint32        address,    ///< current address
                check_sum;  ///< check sum
  uint8        *src;        ///< pointer to FLASH
  /// pointer to control file structure
  tControlFile *pThis = &pFlashDrv->flashData.controlFile;

  pFlashDrv->state.current_status = BUSY_FLASH;
  /// current address in FLASH  
  address = pFlashDrv->address[FL_SYSTEM] + pThis->totalLength;

  /// if data is saved then function return nonzero value
  res_save = FrameWork_FlashProgrammCntrl(pFlashDrv, address, pThis->length, pThis->m_Data);

  /// error data saved
  if (res_save == ERROR_FLASH)
  {
    /// raise error flag and set control file structure in beginning state
    pThis->ErrorFlash  = fg_ON;
    pThis->Process     = 0;
    pThis->Error       = 1;
    pThis->SaveData    = fg_OFF;
    pThis->checkSum    = 0;
    pThis->numOfBlocks = 1;
    pThis->totalLength = 0;
    pThis->EndData     = fg_OFF;
    pFlashDrv->state.current_status = FREE_FLASH;
  }
  /// good data saved
  if (res_save == FINISH)
  {
    /// increment total length of file
    pThis->totalLength += pThis->length;
    pThis->SaveData     = fg_OFF;
    /// if end data flag set then read from FLASH check sum
    if (pThis->EndData == fg_ON)
    {
      address = pFlashDrv->address[FL_SYSTEM] + pThis->totalLength - CHECK_SUM_SYSTEM;
      /// pointer to check sum from FLASH
      src = (uint8*)address;
      COPYINT(((uint8*)&check_sum), src, CHECK_SUM_SYSTEM);
      pThis->checkSum -= (check_sum&0xff);
      pThis->checkSum -= ((check_sum>>8)&0xff);
      pThis->checkSum -= ((check_sum>>16)&0xff);
      pThis->checkSum -= ((check_sum>>24)&0xff);
      /// if reading check sum equal accumulated check sum then check sum save in end final sector FLASH
      if (pThis->checkSum == check_sum)
        pThis->Error = 0;
      else  /// error write
        pThis->Error = 1;
      /// set control file structure in beginning state
      pThis->ErrorFlash  = fg_OFF;
      pThis->Process     = 0;
      pThis->SaveData    = fg_OFF;
      pThis->checkSum    = 0;
      pThis->EndData     = fg_OFF;
      pThis->numOfBlocks = 1;
      pThis->totalLength = 0;
      pFlashDrv->state.current_status = FREE_FLASH;
    }  /// if (pThis->EndData == fg_ON)
  }    /// if (res_save == FINISH)
}

//=================================================================
/// Configuration setup structure initialize
///
/// @param[in]  pFlashDrv - pointer to instance
/// @param[in]  config    - pointer to configuration setup
/// @return     0 if check sum correct otherwise 1
//=================================================================
int MainMgr_ConfigInit(tFlashDrv *pFlashDrv, CONFIG_SETUP *config)
{
  // for target
  #ifdef USE_DSPBIOS
  {
/*    uint16 check_sum = 0,  ///< calculated check sum
           chs_flash = 0;  ///< check sum from FLASH
    int    i;              ///< counter
    uint8 *src,            ///< pointer to source
          *dst;            ///< pointer to destination

    /// calculate check sum
    for(i=0;i<LENGTH_CONFIG;i++)
      check_sum += *(uint8*)(pFlashDrv->address[FL_CONFIG] + i);
    /// read check sum from FLASH
    dst = (uint8*)&chs_flash;
    src = (uint8*)(pFlashDrv->address[FL_CONFIG] + LENGTH_CONFIG);
    COPYINT(dst, src, CHECK_SUM);
    /// control check sum
    if (check_sum == chs_flash)
    {
      /// read configuration from FLASH
      _memcpy(config, (uint8*)pFlashDrv->address[FL_CONFIG], LENGTH_CONFIG);
      return 0;
    }
    else  /// initialize configuration setup by default */
    {
      config->ElevMask         = 0;             ///< satellite elevation mask; default = 0 [degree]
      config->NavSolRate       = 1000;          ///< navigation solution rate in ms: default - 1000
      config->CoefDLL          = DLL_DEF_TIME;  ///< DLL time parameter; default = 1250
      config->DLLtime          = 1;             ///< predetection integrated time (1, 2, 4, 5, 10, 20); default = 1
      config->ECEF             = 0;             ///< ECEF/Geo/OE: 0 - ECEF, 1 - LLA; 2 - Orbital Elements in ECI; default = 0
      config->OPmode           = 0;             ///< OP mode: 0 - only navigation, 1 - navigation and OP; default 1
      config->IonosphereModel  = 1;             ///< Ionosphere model, default: no ionosphere
      config->TroposphereModel = 1;             ///< Troposphere model, default: no troposphere
      config->OrbStatic        = 1;             ///< Orbital/Static OP model: 0 - orbital model, 1 - static model; default = 0
      config->BaudRate422      = 5;             ///< baud rate; bit map; default = 5 - 115200
      config->RAIMth           = 30;           ///< RAIM threshold [meter]; default = 100
      config->SNRth            = 27;            ///< C/N0 threshold for tracking [dB]; default = 27
      return 1;
    }   
  }
  #else  // for PC model
  {
    config->ElevMask         = 0;             ///< satellite elevation mask; default = 0 [degree]
    config->NavSolRate       = 100;           ///< navigation solution rate in ms: default - 1000
    config->CoefDLL          = DLL_DEF_TIME;  ///< DLL time parameter; default = 1250
    config->DLLtime          = 1;             ///< predetection integrated time (1, 2, 4, 5, 10, 20); default = 1
    config->ECEF             = 0;             ///< ECEF/Geo/OE: 0 - ECEF, 1 - LLA; 2 - Orbital Elements in ECI; default = 0
    config->OPmode           = 1;             ///< OP mode: 0 - only navigation, 1 - navigation and OP; defaul 1
    config->IonosphereModel  = 0;             ///< Ionosphere model, default: no ionosphere
    config->TroposphereModel = 0;             ///< Troposphere model, default: no troposphere
    config->OrbStatic        = 0;             ///< Orbital/Static OP model: 0 - orbital model, 1 - static model; default = 0
    config->BaudRate422      = 5;             ///< baud rate; bit map; default = 5 - 115200
    config->RAIMth           = 100;           ///< RAIM threshold [meter]; default = 100
    config->SNRth            = 27;            ///< C/N0 threshold for tracking [dB]; default = 27
    return 1;
  }
  #endif
}

//====================================================================
/// Configuration setup write-in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  config    - pointer to configuration setup structure
//====================================================================
void MainMgr_ConfigFlash(tFlashDrv *pFlashDrv, CONFIG_SETUP *config)
{
  uint8 res_save;  ///< result saving

  /// start programming
  if (pFlashDrv->flashData.controlConfig.StartProgramming == fg_OFF)
  {
    pFlashDrv->flashData.controlConfig.StartProgramming = fg_ON;
    pFlashDrv->state.current_status                     = BUSY_FLASH;
    /// copy configuration setup, calculate check sum and it save in buffer
    _memcpy_cs16(pFlashDrv->flashData.controlConfig.m_Data, config, LENGTH_CONFIG);
  }
  /// loading data in FLASH
  res_save = FrameWork_FlashProgrammCntrl(pFlashDrv, pFlashDrv->address[FL_CONFIG], 
                                         (LENGTH_CONFIG + CHECK_SUM), 
                                         pFlashDrv->flashData.controlConfig.m_Data);
  /// control load data ending
  if ((res_save == FINISH) || (res_save == ERROR_FLASH))
  {
    pFlashDrv->flashData.controlConfig.SaveData         = fg_OFF;
    pFlashDrv->flashData.controlConfig.StartProgramming = fg_OFF;
    pFlashDrv->state.current_status                     = FREE_FLASH;
  }
}

//=============================================================================
/// Reads week data from FLASH sector
///
/// @param[in]  pFlashDrv - pointer to Flash Driver
/// @param[in]  weekData  - pointer to week data structure
//=============================================================================
void MainMgr_WeekDataInit(tFlashDrv *pFlashDrv, tWeekData *weekData)
{
  uint8  check_sum = 0,  ///< calculated check sum
         chs_flash = 0,  ///< check sum from FLASH
        *src,            ///< pointer to source
        *dst;            ///< pointer to destination
  int    i;              ///< counter

  /// calculate check sum
  for(i=0;i<LENGTH_WN;i++)
    check_sum += *(uint8*)(pFlashDrv->address[FL_WN] + i);
  /// read check sum from FLASH
  dst = &chs_flash;
  src = (uint8*)(pFlashDrv->address[FL_WN] + LENGTH_WN);
  COPYINT(dst, src, CHECK_SUM_WN);
  /// control check sum
  if (check_sum == chs_flash)
  {
    /// data is not error
    weekData->DataError = fg_OFF;
    
    src = (uint8*)pFlashDrv->address[FL_WN];
    dst = (uint8*)&weekData->wnFlash;
    /// read week number
    COPYINT(dst, src, (sizeof(weekData->wnFlash)));
    /// read counter of week number overflow
    dst = (uint8*)&weekData->cntOverFlow;
    COPYINT(dst, src, (sizeof(weekData->cntOverFlow)));
  }
  else  /// data error
    weekData->DataError = fg_ON;
}

//====================================================================
/// Week data write-in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  weekData  - pointer to week data structure
//====================================================================
void MainMgr_WeekDataFlash(tFlashDrv  *pFlashDrv,
                           tWeekData  *weekData)
{
  uint8  res_save,       ///< result saving
        *src,            ///< pointer to source
        *dst,            ///< pointer to destination
         check_sum = 0;  ///< calculated check sum
  int    i;

  /// start programming
  if (pFlashDrv->flashData.controlWN.StartProgramming == fg_OFF)
  {
    pFlashDrv->flashData.controlWN.StartProgramming = fg_ON;
    pFlashDrv->state.current_status                 = BUSY_FLASH;

    dst = pFlashDrv->flashData.controlWN.m_Data;
    src = (uint8*)&weekData->wnFlash;
    /// save week number
    for(i=0;i<sizeof(weekData->wnFlash);i++)
      check_sum += dst[i] = *src++;
    /// save counter of week number overflow
    check_sum += dst[sizeof(weekData->wnFlash)] = weekData->cntOverFlow;
    dst[LENGTH_WN] = check_sum;
  }
  /// loading data in FLASH
  res_save = FrameWork_FlashProgrammCntrl(pFlashDrv, pFlashDrv->address[FL_WN], 
                                         (LENGTH_WN + CHECK_SUM_WN), pFlashDrv->flashData.controlWN.m_Data);
  /// control load data ending
  if ((res_save == FINISH) || (res_save == ERROR_FLASH))
  {
    pFlashDrv->flashData.controlWN.SaveData         = fg_OFF;
    pFlashDrv->flashData.controlWN.StartProgramming = fg_OFF;
    pFlashDrv->state.current_status = FREE_FLASH;
  }
}

//=============================================================
/// Read ephemeris from FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pSatMgr   - pointer to Satellite Manager object
//=============================================================
void MainMgr_EphemerisInit(tFlashDrv         *pFlashDrv,
                           tSatelliteManager *pSatMgr)
{
  int           i,
                j;          ///< counters
  TEMP_EPH_GPS *pEph;       ///< pointer to current ephemeris in work structure
  uint8        *src,        ///< pointer to source
               *dst,        ///< pointer to destination
                sv_num;     ///< satellite number
  uint16        check_sum,  ///< check sum calculated
                chs_flash;  ///< check sum from FLASH

  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    /// start address + (ephemeris size + check sum size)*satellite counter
    src = (uint8*)(pFlashDrv->address[FL_EPHEMERIS] + ((SIZE_EPHEMERIS + CHECK_SUM)*i));
    dst = &sv_num;
    /// read satellite number
    COPYINT(dst, src, 1);
    /// control satellite number
    if (sv_num == (i+1))
    {
      /// calculate check sum
      check_sum = sv_num;
      for(j=0;j<(SIZE_EPHEMERIS-1);j++)
        check_sum += *src++;
      /// read check sum from FLASh
      dst = (uint8*)&chs_flash;
      COPYINT(dst, src, CHECK_SUM);
      /// control check sum
      if (check_sum == chs_flash)
      {
        /// pointer to current ephemeris in work structure initialize
        pEph = &pSatMgr->TopSystem.DecCommon.TempEph[sv_num-1];
        /// pointer to current ephemeris in FLASH initialize
        src = (uint8*)(pFlashDrv->address[FL_EPHEMERIS] + ((SIZE_EPHEMERIS + CHECK_SUM)*i));
        _memcpy(&pEph->TEphGPS, src, SIZE_EPHEMERIS);
        /// ephemeris exist flag set
        pEph->flag = 1;        
      }
    }
  }
}

//==============================================================
/// Read almanac from FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pSatMgr   - pointer to Satellite Manager object
//==============================================================
void MainMgr_AlmanacInit(tFlashDrv         *pFlashDrv,
                         tSatelliteManager *pSatMgr)
{
  int           i,
                j;          ///< counters
  TEMP_ALM_GPS *pAlm;       ///< pointer to current almanac in work structure
  uint8        *src,        ///< pointer to source
               *dst,        ///< pointer to destination
                sv_num;     ///< satellite number
  uint16        check_sum,  ///< check sum calculated
                chs_flash;  ///< check sum from FLASH

  for(i=0;i<GPS_SATELLITE_COUNT;i++)
  {
    /// start address + (almanac size + check sum size)*satellite counter
    src = (uint8*)(pFlashDrv->address[FL_ALMANAC] + ((SIZE_ALMANAC + CHECK_SUM)*i));
    dst = &sv_num;
    /// read satellite number
    COPYINT(dst, src, 1);
    /// control satellite number
    if (sv_num == (i+1))
    {
      /// calculate check sum
      check_sum = sv_num;
      for(j=0;j<(SIZE_ALMANAC-1);j++)
        check_sum += *src++;
      /// read check sum from FLASh
      dst = (uint8*)&chs_flash;
      COPYINT(dst, src, CHECK_SUM);
      /// control check sum
      if (check_sum == chs_flash)
      {
        /// pointer to current ephemeris in work structure initialize
        pAlm = &pSatMgr->TopSystem.DecCommon.TempAlmGPS[sv_num-1];
        /// pointer to current ephemeris in FLASH initialize
        src = (uint8*)(pFlashDrv->address[FL_ALMANAC] + ((SIZE_ALMANAC + CHECK_SUM)*i));
        _memcpy(&pAlm->TAlmGPS, src, SIZE_ALMANAC);
        /// ephemeris exist flag set
        pAlm->flag = 1;        
      }
    }
  }
}

//====================================================================
/// Write ephemeris in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pTopSys   - pointer to Top System
//====================================================================
void MainMgr_EphemerisFlash(tFlashDrv   *pFlashDrv,
                            tTopSystem  *pTopSys)
{
  uint8 *src,       ///< pointer to flash
         iode,      ///< ephemeris age
         res_save,  ///< result saving
         sv_num,    ///< current satellite number
         ok = 0,    ///< save ephemeris flag
         i;         ///< counter
  uint16 wne;       ///< ephemeris week number
  uint32 address;   ///< start address ephemeris
  /// pointer to control ephemeris structure
  tControlEph *pThis = &pFlashDrv->flashData.controlEph;

  /// start cycle
  if ((pThis->svID == 0) && (pFlashDrv->state.current_status != BUSY_FLASH))
  {
    pFlashDrv->state.current_status = BUSY_FLASH;
  }

  /// search ephemeris for write-in flash
  if (pThis->SaveData == fg_OFF)
  {
    /// start address for current ephemeris
    address = pFlashDrv->address[FL_EPHEMERIS] + ((SIZE_EPHEMERIS + CHECK_SUM)*pThis->svID);
    src = (uint8*)address;
    /// read satellite number
    COPYINT((&sv_num), src, 1);
    /// check ephemeris exist
    if ((pTopSys->eph_set_flgs&(0x1<<pThis->svID)) == (0x1<<pThis->svID))
    {
      /// check satellite number
      if (sv_num == (pThis->svID+1))
      {
        /// address for iode of current ephemeris
        src = (uint8*)(address + DELTA_IODE);
        COPYINT((&iode), src, 1);
        /// if ephemeris age in structure not equal ephemeris age in flash then need to save ephemeris in flash
        if (pTopSys->DecCommon.TempEph[pThis->svID].TEphGPS.iode != iode)
          ok = DATA_RECORD;
        else
        {
          /// address for week number of current ephemeris
          src = (uint8*)(address + DELTA_WN);
          COPYINT(((uint8*)&wne), src, 2);
          /// if ephemeris week number in structure not equal ephemeris week number in flash 
          /// then need to save ephemeris in flash
          if (pTopSys->DecCommon.TempEph[pThis->svID].TEphGPS.wn != wne)
            ok = DATA_RECORD;
        }
      }
      else  /// if !(sv_num == (pThis->svID+1))
        ok = DATA_RECORD;
    }
    else if (sv_num != SATELLITE_INVALID_ID)  /// if ephemeris exist in flash then it nulling
      ok = DATA_NULLING;

    /// data nulling or record
    if (ok != 0)
    {
      /// zero buffer
      if (ok == DATA_NULLING)
      {
        for(i=0;i<(SIZE_EPHEMERIS+CHECK_SUM);i++)
          pThis->m_Data[i] = 0;
        pThis->SaveData = fg_ON;
      }
      /// copy ephemeris in buffer, calculate check sum and save it in buffer
      if (ok == DATA_RECORD)
      {
        /// control blocking flag
        if (pTopSys->DecCommon.TempEph[pThis->svID].flag_bl == 0)
        {
          pTopSys->DecCommon.TempEph[pThis->svID].flag_bl = 1;  ///< raise blocking flag
          _memcpy_cs16(pThis->m_Data, &pTopSys->DecCommon.TempEph[pThis->svID].TEphGPS, SIZE_EPHEMERIS);
          pTopSys->DecCommon.TempEph[pThis->svID].flag_bl = 0;  ///< clear blocking flag
          pThis->SaveData = fg_ON;
        }
      }
      /// loading data
      if (pThis->SaveData == fg_ON)
      {
        FrameWork_FlashProgrammCntrl(pFlashDrv, address, (SIZE_EPHEMERIS+CHECK_SUM), pThis->m_Data);
      }
      else
      {
        /// increment satellite number
        pThis->svID++;
        /// end cycle
        if (pThis->svID >= GPS_SATELLITE_COUNT)
        {
          pThis->svID                     = 0;
          pFlashDrv->state.current_status = FREE_FLASH;
        }
      }
    }  /// if (ok != 0)
    else
    {
      /// increment satellite number
      pThis->svID++;
      /// end cycle
      if (pThis->svID >= GPS_SATELLITE_COUNT)
      {
        pThis->svID                     = 0;
        pFlashDrv->state.current_status = FREE_FLASH;
      }
    }
    return;
  }
  /// pThis->SaveData == fg_ON
  /// start address for current ephemeris
  address = pFlashDrv->address[FL_EPHEMERIS] + ((SIZE_EPHEMERIS + CHECK_SUM)*pThis->svID);
  /// if check sum is saved then function return nonzero value
  res_save = FrameWork_FlashProgrammCntrl(pFlashDrv, address, (SIZE_EPHEMERIS+CHECK_SUM), pThis->m_Data);

  /// control load data ending
  if ((res_save == FINISH) || (res_save == ERROR_FLASH))
  {
    pThis->svID++;
    pThis->SaveData = fg_OFF;
    /// end cycle then free FLASH
    if (pThis->svID >= GPS_SATELLITE_COUNT)
    {
      pThis->svID                     = 0;
      pFlashDrv->state.current_status = FREE_FLASH;
    }
  }
}

//=======================================================================
/// Write almanac in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pTopSys   - pointer to Top System
//=======================================================================
void MainMgr_AlmanacFlash(tFlashDrv  *pFlashDrv,
                          tTopSystem *pTopSys)
{
  uint8 *src,       ///< pointer to flash
         toa,       ///< almanac age
         res_save,  ///< result saving
         sv_num,    ///< current satellite number
         ok = 0,    ///< save almanac flag
         i;         ///< counter
  uint16 wna;       ///< almanac week number
  uint32 address;   ///< start address almanac
  /// pointer to control almanac structure
  tControlAlm *pThis = &pFlashDrv->flashData.controlAlm;

  /// start cycle
  if ((pThis->svID == 0) && (pFlashDrv->state.current_status != BUSY_FLASH))
    pFlashDrv->state.current_status = BUSY_FLASH;

  /// search almanac for write-in flash
  if (pThis->SaveData == fg_OFF)
  {
    /// start address for current almanac
    address = pFlashDrv->address[FL_ALMANAC] + ((SIZE_ALMANAC + CHECK_SUM)*pThis->svID);
    src = (uint8*)address;
    /// read satellite number
    COPYINT((&sv_num), src, 1);
    /// check almanac exist
    if ((pTopSys->alm_set_flgs&(0x1<<pThis->svID)) == (0x1<<pThis->svID))
    {
      /// check satellite number
      if (sv_num == (pThis->svID+1))
      {
        /// address for toa of current almanac
        src = (uint8*)(address + DELTA_TOA);
        COPYINT((&toa), src, 1);
        /// if almanac age in structure not equal almanac age in flash then need to save almanac in flash
        if (pTopSys->DecCommon.TempAlmGPS[pThis->svID].TAlmGPS.toa != toa)
          ok = DATA_RECORD;
        else
        {
          /// address for wna of current almanac
          src = (uint8*)(address + DELTA_WN);
          COPYINT(((uint8*)&wna), src, 2);
          /// if almanac week number in structure not equal almanac week number in flash 
          /// then need to save almanac in flash
          if (pTopSys->DecCommon.TempAlmGPS[pThis->svID].TAlmGPS.wna != wna)
            ok = DATA_RECORD;
        }
      }
      else
        ok = DATA_RECORD;
    }
    else if (sv_num != SATELLITE_INVALID_ID)  /// if almanac exist in flash then it nulling
      ok = DATA_NULLING;

    /// data nulling or record
    if (ok != 0)
    {
      /// zero buffer
      if (ok == DATA_NULLING)
      {
        for(i=0;i<(SIZE_ALMANAC+CHECK_SUM);i++)
          pThis->m_Data[i] = 0;
        pThis->SaveData = fg_ON;
      }
      /// copy almanac in buffer, calculate check sum and save it in buffer
      if (ok == DATA_RECORD)
      {
        /// control blocking flag
        if (pTopSys->DecCommon.TempAlmGPS[pThis->svID].flag_bl == 0)
        {
          pTopSys->DecCommon.TempAlmGPS[pThis->svID].flag_bl = 1;  ///< raise blocking flag
          _memcpy_cs16(pThis->m_Data, &pTopSys->DecCommon.TempAlmGPS[pThis->svID].TAlmGPS, SIZE_ALMANAC);
          pTopSys->DecCommon.TempAlmGPS[pThis->svID].flag_bl = 0;  ///< clear blocking flag
          pThis->SaveData = fg_ON;
        }
      }
      /// loading data
      if (pThis->SaveData == fg_ON)
      {
        FrameWork_FlashProgrammCntrl(pFlashDrv, address, (SIZE_ALMANAC+CHECK_SUM), pThis->m_Data);
      }
      else
      {
        /// increment satellite number
        pThis->svID++;
        /// end cycle
        if (pThis->svID >= GPS_SATELLITE_COUNT)
        {
          pThis->svID                     = 0;
          pFlashDrv->state.current_status = FREE_FLASH;
        }
      }
    }  /// if (ok != 0)
	else
	{
      /// increment satellite number
      pThis->svID++;
      /// end cycle
      if (pThis->svID >= GPS_SATELLITE_COUNT)
      {
        pThis->svID                     = 0;
        pFlashDrv->state.current_status = FREE_FLASH;
      }
	}
    return;
  }
  /// pThis->SaveData == fg_ON
  /// start address for current almanac
  address = pFlashDrv->address[FL_ALMANAC] + ((SIZE_ALMANAC + CHECK_SUM)*pThis->svID);
  /// if check sum is saved then function return nonzero value
  res_save = FrameWork_FlashProgrammCntrl(pFlashDrv, address, (SIZE_ALMANAC+CHECK_SUM), pThis->m_Data);

  /// control load data ending
  if ((res_save == FINISH) || (res_save == ERROR_FLASH))
  {
    pThis->svID++;
    pThis->SaveData = fg_OFF;
    /// end cycle then free FLASH
    if (pThis->svID >= GPS_SATELLITE_COUNT)
    {
      pThis->svID                     = 0;
      pFlashDrv->state.current_status = FREE_FLASH;
    }
  }
}

//====================================================================
/// Clear ephemeris and almanac in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @return 0 if clearing processing, 1 if clearing finished
//====================================================================
int MainMgr_EphAlmClear(tFlashDrv *pFlashDrv)
{
  uint8  res_save,  ///< result saving
         i;         ///< counter
  uint32 address;   ///< start address ephemeris
  /// pointer to control clearing structure
  tControlClr *pThis = &pFlashDrv->flashData.controlClr;

  /// start clearing ephemeris and almanac
  if (pThis->SaveData == fg_OFF)
  {
    pFlashDrv->state.current_status = BUSY_FLASH;
    pThis->SaveData = fg_ON;
    for(i=0;i<PAGE_SIZE;i++)
      pThis->m_Data[i] = 0;
    /// start address for ephemeris block data
    address = pFlashDrv->address[FL_EPHEMERIS];
    /// increment 
    pThis->pagesNumber = 0;
    FrameWork_FlashProgrammCntrl(pFlashDrv, address, PAGE_SIZE, pThis->m_Data);
    return 0;
  }
  /// pThis->SaveData == fg_ON
  /// start address for current FLASH page
  address = pFlashDrv->address[FL_EPHEMERIS] + (PAGE_SIZE*pThis->pagesNumber);
  /// if check sum is saved then function return nonzero value
  res_save = FrameWork_FlashProgrammCntrl(pFlashDrv, address, PAGE_SIZE, pThis->m_Data);

  /// control load data ending
  if ((res_save == FINISH) || (res_save == ERROR_FLASH))
  {
    pThis->pagesNumber++;
    if (pThis->pagesNumber >= EPH_ALM_PAGE)
    {
      pThis->pagesNumber              = 0;
      pThis->SaveData                 = fg_OFF;
      pFlashDrv->state.current_status = FREE_FLASH;
      return 1;
    }
  }
  return 0;
}








