/*************************************************************************************
*    Copyright (c) 2005 Spirit Corp.
**************************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  for Navigation task
*    @file                   matrix.h
*    @author                 A. Baloyan
*    @date                   25.06.2005
*    @version                1.0
*/ 
/*************************************************************************************/

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "DataForm.h"
#include "comtypes.h"

#define MATRIX_MAX_N  (12)  ///< number row
#define MATRIX_MAX_M  (5)   ///< number column

// Matrix structure
typedef struct
{ 
  float64 Matr[MATRIX_MAX_N][MATRIX_MAX_M];  ///< Matrix elements
  int32  mRaw;                               ///< Number of rows
  int32  mCol;                               ///< Number of columns
} Matrix;

//=====================================================================================
/// Matrix initialization
///
/// @param[in] A - pointer to matrix
/// @param[in] N - number of rows
/// @param[in] M - number of columns
//=====================================================================================
EXTERN_C void    CreateMatrix(Matrix *A, int32 N, int32 M);

//=====================================================================================
/// Free matrix dynamic allocation memory
///
/// @param[in] A - pointer to matrix
//=====================================================================================
EXTERN_C void    DestroyMatrix(Matrix* A);

//=====================================================================================
/// Copy matrix B to A (A = B)
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
//=====================================================================================
EXTERN_C void    CopyMatrix(Matrix *A, Matrix *B);

//=====================================================================================
/// Make unitary Matrix A = I( N x N)
///
/// @param[out] A - pointer to matrix
/// @param[in]  N - matrix dimention
//=====================================================================================
EXTERN_C void    UnitaryMatrix(Matrix *A, int32 N);

//=====================================================================================
/// Square of the module of matrix A computation
///
/// @param[in] A - pointer to matrix
/// @return    square of the module of matrix A 
//=====================================================================================
EXTERN_C float64 Mod(Matrix *A);

//=====================================================================================
/// Computation of the inverse Matrix A = Inv(B)
///
/// @param[out] A - pointer to matrix A = Inv(B)
/// @param[in]  B - pointer to matrix B
//=====================================================================================
EXTERN_C void    InverseMatrix(Matrix *A, Matrix *B);

//=====================================================================================
/// Computation of the difference of matrices A = B-C
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
/// @param[in]  C - pointer to matrix C
//=====================================================================================
EXTERN_C void    Sum_Matrix(Matrix* A, Matrix* B, Matrix* C);

//=====================================================================================
/// Multiplication of matrices A = B*C
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
/// @param[in]  C - pointer to matrix C
//=====================================================================================
EXTERN_C void    MultMatrix(Matrix *A, Matrix *B, Matrix *C);

//=====================================================================================
/// Multiplication of matrices A = T(B)*C
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
/// @param[in]  C - pointer to matrix C
//=====================================================================================
EXTERN_C void    MultMatrixT_(Matrix *A, Matrix *B, Matrix *C);

//=====================================================================================
/// QR-decomposition of the matrix A=QR
/// Resultant matrices are contained in R and Q correspondly
///
/// @param[in]  A - pointer to matrix A=QR
/// @param[out] Q - pointer to matrix Q
/// @param[out] R - pointer to matrix R
//=====================================================================================
EXTERN_C void    QR_Decomposition(Matrix *A, Matrix *Q, Matrix *R);

//=====================================================================================
/// Solution of linear system X=R*Y (i.e. Y=Inv(R)*X)
/// where R is an upper triangular matrix
///
/// @param[out] Y - pointer to matrix Y=Inv(R)*X
/// @param[in]  R - pointer to matrix R
/// @param[in]  X - pointer to matrix X
//=====================================================================================
EXTERN_C void    UppTriSolution(Matrix *Y, Matrix *R, Matrix *X);

//=====================================================================================
/// Computation of a matrix G=Inv(R)*Q where R is an upper triangular matrix
///
/// @param[out] G - pointer to matrix G=Inv(R)*Q
/// @param[in]  R - pointer to matrix R
/// @param[in]  Q - pointer to matrix Q
//=====================================================================================
EXTERN_C void    UppTriSolModif(Matrix *G, Matrix *R, Matrix *Q);

//=====================================================================================
/// Remove N_Row rows from matrix starting with J_Row row: A = DelRow(B)
///
/// @param[out] A     - pointer to matrix A
/// @param[in]  B     - pointer to matrix B
/// @param[in]  I_Row - starting row
/// @param[in]  N_Row - number row
//=====================================================================================
EXTERN_C void    DelRowMatrix(Matrix *A, Matrix *B, int32 I_Row, int32 N_Row);

#endif //_MATRIX_H_
