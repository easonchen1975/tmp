/*******************************************************************************
 *    Copyright (c) 2005-2008 Spirit Corp.
 ******************************************************************************/
/**
 *    @project                GPSR
 *    @brief                  Ionospheric & tropospheric delays calculation
 *    @file                   AtmosphericDelay.h
 *    @author                 D. Churikov
 *    @date                   29.04.2008
 *    @version                1.0
 */
/******************************************************************************/
#ifndef __ATMOSPHERICDELAY_H
#define __ATMOSPHERICDELAY_H

#include "comtypes.h"
#include "control.h"
#include "satellite.h"

//========================================================================
/// @brief      Calculation of atmospheric delay for SV's signal
///             (see ICD-GPS-200C, par.20.3.3.5.2.6)
///
/// @param[in]  NP      - pointer to satellite navigation parameters
/// @param[in]  User    - pointer to user parameters
/// @param[in]  IONpar  - pointer to ionosphere parameters
/// @param[in]  GPStime - receiver computed system time
/// @param[out] t_ION   - ionospheric delay correction
/// @param[out] t_TRO   - tropospheric delay correction
//========================================================================
EXTERN_C void AtmosphereDelay( tNavTaskParams *NP,
                               USER           *User,
                               ION_UTC        *IONpar,
                               float64         GPStime,
                               int8           IonModel,
                               int8           TropoModel,
                               float32        *t_ION,
                               float32        *t_TRO);


#endif

