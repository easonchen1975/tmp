/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        General satellite structures and function headers
 *   @file         satellite.h
 *   @author       V. Yeremeyev, M.Zhokhova
 *   @date         21.09.2005
 *   @version      2.0
 */
/*****************************************************************************/

#ifndef _SATELLITE_INFO_INCLUDED_
#define _SATELLITE_INFO_INCLUDED_

#include "ComTypes.h"
#include "GPSChan.h"
#include "RawMeasurForm.h"

typedef struct
{
  float64  dt_SV;                   ///< SV's time shift relatively system time
  float64  x, y, z;                 ///< Geocentric satellite coordinates [m]
  float64  vx, vy, vz;              ///< Geocentric satellite velocities  [m/s]
}
tCoord;

/// satellite coordinates
typedef struct 
{
  tCoord   coord;                   ///< satellite coordinates
  float32  dt_ION;                  ///< Ionosphere correction,  [m]
  float32  dt_TRO;                  ///< Troposphere correction, [m]
  int32    dt_IONQ17;               ///< Ionosphere correction in Q17
  int32    dt_TROQ17;               ///< Troposphere correction in Q17
  int64    x_los, y_los, z_los;     ///< LOS in ECEF [mm]
}
tSatCoord;

/// satellite parameters
typedef struct 
{
  float32  elev;                    ///< SV's elevation angle [radians]
  float32  azim;                    ///< SV's azimuth [radians]
  int16    elev_i16;                ///< SV's elevation angle [0.1 degrees]
  int16    azim_i16;                ///< SV's azimuth [0.1 degrees]
}
tSatParams;

/// fix point estimated errors
typedef struct 
{
  int32 ps_delay_biasQ17;           ///< pseudo-range estimated error (bias) in Q17           [mm]
  int32 ps_delay_noiseQ17;          ///< pseudo-range estimated error (noise) in Q17          [mm]
  int32 ps_freqQ17;                 ///< pseudo-range rate estimated error in Q17             [mm/s]
  int32 err_dllQ17;                 ///< beginning pseudo-range estimated error (bias) in Q17 [mm]
}
tEstErrFix;

typedef struct
{
  tCoord      coord;                ///< satellite coordinates
  tRawData    raw_data;             ///< floating point raw data for navigation task solution
  tRawDataFix raw_data_fix;         ///< fix point raw data for navigation task solution
  uint8       BAD_SAT_OP;           ///< satellite exclude from OP
  uint8       ELEV_MSK_OP;          ///< satellite is not used in OP by elevation mask
  uint8       USED_OP;              ///< satellite is used in OP
  uint8       COORDINATES;          ///< satellite coordinates
}
tParamsOP;

/// satellite navigation parameters
typedef struct 
{
  tSatParams      sat_params;                  ///< satellite parameters

  tSatCoord       sat_coord;                   ///< satellite coordinates
  
  float64         range;                       ///< receiver-SV range       [m]
  float64         Vrad;                        ///< Radial user-SV velocity [m/s]
  float64         Hmat[3];                     ///< Directional cosines matrix [-]

  tRawData        raw_data;                    ///< floating point raw data for navigation task solution
  tRawDataFix     raw_data_fix;                ///< fix point raw data for navigation task solution

  tParamsOP       paramsOP;                    ///< satellite parameters for OP
  
  uint8           BAD_SAT;                     ///< satellite exclude from navigation task solution by RAIM
  uint8           ELEV_MSK;                    ///< satellite is not used in navigation task solution by elevation mask
  uint8           USED;                        ///< satellite is used in navigation task solution

  int8            SNR_log;                     ///< Signal to noise ratio [dBHz]
  int8            sv_num;                      ///< satellite ID
}
tNavTaskParams;

//*****************************************************************************
/// Satellite information
//*****************************************************************************
typedef struct tagSatellite
{
  int8            sv_num;         ///< satellite ID

  tGPSchan        gps_chan;       ///< channel structure
  
  tDecoder        decoder;        ///< pointer to decoder

  tRawDataFix     raw_data_fix;   ///< fix point raw data for navigation task solution
  tRawDataFix     raw_data_OP;    ///< fix point raw data for OP
  
  tEstErrFix      est_err_fix;    ///< fix point satellite estimated error
 
  int8            SNR_log;        ///< Signal to noise ratio [dBHz]

  uint8           status;         ///< bit 0 - using in navigation;    bit 1 - elevation mask, 
                                  ///  bit 2 - ephemeris availability, bit 3 - almanac availability
} tSatelliteGPS;

//=======================================================================================
/// GPS satellite object initialization
//
/// @param[in] pObject  - pointer to instance
/// @param[in] doppler  - Doppler frequency
/// @param[in] position - position in millisecond
/// @param[in] wn       - GPS week number
/// @param[in] ms_count - time of week, [ms]
/// @param[in] preamble - pointer to preamble structure
//=======================================================================================
EXTERN_C void SatGPS_Init(tSatelliteGPS *pObject,
                          double         doppler,
                          double         position,
                          int16          wn,
                          uint32         ms_count,
                          tPreamble     *preamble);

//============================================================================================
/// Calculate pseudo range estimated error bias and noise, pseudo range rate estimated error
///
/// @param[in]  pThis         - pointer to instance
/// @param[in]  accuracy      - user range accuracy (URA), coded 0..15
/// @param[in]  dt_IONQ17     - Ionosphere correction in Q17
/// @param[in]  dt_TROQ17     - Troposphere correction in Q17
/// @param[in]  epochCounter  - epoch counter
/// @param[in]  SNR_log       - satellite SNR
/// @param[in]  k_begin_val   - fractional beginning coefficient for DLL
/// @param[in]  k_begin_shr   - Q value beginning coefficient
/// @param[in]  k_end_dll     - ending coefficient for DLL
//============================================================================================
EXTERN_C void SatGPS_EstimatedErrorFix(tEstErrFix  *pThis,
                                       int16        accuracy,
                                       int32        dt_IONQ17,
                                       int32        dt_TROQ17,
                                       int32        epochCounter,
                                       int8         SNR_log,
                                       int16        k_begin_val,
                                       int16        k_begin_shr,
                                       int32        k_end_dll);

//=======================================================================================
/// GPS satellite process
//
/// @param[in] pThis             - pointer to satellite object
/// @param[in] pSamples           - pointer to samples
/// @param[in] DecCommon          - pointer to common decoder
/// @param[in] week               - GPS week number
/// @param[in] ms_count           - time of week
/// @param[in] CN_THRESHOLD       - minimal SNR boundary
/// @param[in] coefPllDll         - coefficients for PLL/DLL
/// @param[in] WeekInit           - week number of initialized
/// @param[in] eph_set_flg        - ephemeris set flag
//=======================================================================================
EXTERN_C const tGPSchanStatus *SatGPS_Process(tSatelliteGPS          *pThis, 
                                              const tComplex8Aligned *pSamples,
                                              tDecoderCommon         *DecCommon,
                                              const int16             week,
                                              uint32                  ms_count,
                                              int8                    CN_THRESHOLD,
                                              const tCoefPLLDLL       coefPllDll,
                                              eFlag                   WeekInit,
                                              uint8                   eph_set_flg);

#endif // !defined(_SATELLITE_INFO_INCLUDED_)
