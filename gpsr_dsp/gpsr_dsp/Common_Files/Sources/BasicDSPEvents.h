/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  BasicDSP events
 *    @file                   BasicDSPEvents.h
 *    @author                 M. Zhokhova
 *    @date                   25.06.2005
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef _BASICDSP_EVENTS_H
#define _BASICDSP_EVENTS_H
#include <string.h>

//=============================================================
/// Callback for post tracking manager
///
/// @param[in] pHost - pointer to instance
//=============================================================
EXTERN_C void BasicDSP_FnPostTrackingManager(void *pHost);

//=============================================================
/// Callback for post navigation task
///
/// @param[in] pHost - pointer to instance
//=============================================================
EXTERN_C void BasicDSP_FnPostNavTask (void* pHost);

#endif // _BASICDSP_EVENTS_H
