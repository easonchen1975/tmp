/*****************************************************************************
*    Copyright (c) 2007 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  data formation
*    @file                   DataForm.h
*    @author                 M. Zhokhova
*    @date                   13.06.2007
*    @version                4.0
*****************************************************************************/
#if !defined(__DATAFORM_H)
#define __DATAFORM_H

#include "comtypes.h"

/// Satellites info:
#define GPS_SATELLITE_COUNT      (42)           ///< GPS satellites number + QZSS

#define SATELLITE_INVALID_ID     (0)            ///< invalid ID for satellite

/// GPS ID satellite
#define IS_GPS_SAT(id)       (((id) > 0)) && ((id) <= GPS_SATELLITE_COUNT)

/// Tracking satellites:
// #define GPS_SAT_CHANNELS_COUNT   (12)           ///< channels number
#define GPS_SAT_CHANNELS_COUNT   (12)           ///< channels number

/// For search algorithm
#define SEARCH_COUNT             (4)            ///< satellites number for search in reacquisition mode
#ifdef USE_DSPBIOS
#define SAT_SEARCH_COUNT         (12)           ///< satellites number for fast search
#else
#define SAT_SEARCH_COUNT         (12)           ///< satellites number for fast search
#endif

#if 0
	#define FSEARCH_SHIFT_MIN        (-130)         ///< minimum shifts for search
	#define FSEARCH_SHIFT_MAX        (130)          ///< maximum shifts for search
#else	// for MicroSat
	#define FSEARCH_SHIFT_MIN        (-100)         ///< minimum shifts for search
	#define FSEARCH_SHIFT_MAX        (100)          ///< maximum shifts for search
#endif

/// for PC model: for navigation task solution
#ifndef USE_DSPBIOS
#define SIMULATOR
#endif

/// time for reacquisition mode
//#define REACQ_TIME               (30000)        ///< time in milliseconds
#define REACQ_TIME               (20000)        ///< time in milliseconds

/// Common information
#define S_WEEK            (float64)(604800.0)   ///< seconds number in week
#define S_HALF_WEEK       (float64)(302400.0)   ///< seconds number in half week
#define MS_WEEK             (int32)(604800000)  ///< millisecond number in week
#define MS_HALF_WEEK        (int32)(302400000)  ///< millisecond number in half week
#define WEEK                       (1024)       ///< maximal week number (overflow 10-bits week number)

/// RS422 status
#define TERMINAL_FREE              (0)          ///< for UART: read data from temporary buffer end
#define TERMINAL_READ              (1)          ///< for UART: data in temporary buffer

/// defines for epoch control flag
#define EC_TYPICAL                 (0)          ///< typical mode
#define EC_SKIP                    (1)          ///< skip epoch - current ms

/// synchronization OP and navigation task solution defines
#define NO_SYNC                    (0)          ///< no synchronization
#define SYNC_PROCESS               (1)          ///< synchronization in process
#define SYNC_OK                    (2)          ///< synchronization finished
#define PERIOD_SYNC                (200)        ///< period for navigation task posting during synchronization


#define UTILIZATION_ZONE	10

/// Description of list structures
typedef struct
{  
  int8 N;                     ///< Current quantity of elements
  int8 Cells[6];              ///< Elements array
} LIST6;

typedef struct
{ 
  int8 N;                     ///< Current quantity of elements
  int8 Cells[12];             ///< Elements array
} LIST12;

typedef struct 
{
  int8 N;                     ///< Current quantity of elements
  int8 Cells[32];             ///< Elements array
} LIST32;

typedef struct
{
  int16 wn;                   ///< Receiver time (current week number) [week]
  int64 t;                    ///< Receiver time (current time within week) [mks]
  int64 X, Y, Z;              ///< User ECEF X,Y,Z-coordinates, [mm]
  int32 Xdot, Ydot,   Zdot;   ///< User X,Y,Z-coordinates' derivatives, [m/s]
  int64 Lon,  Lat,    H;      ///< User longitude, latitude [radians] and height [m]
  int32 Ndot, Edot,   Udot;   ///< User vehicle velocity in LLA frame
  int64 A,    Omega,  M;      ///< Semi-major axis [m], right ascension of ascending node [rad], mean anomaly [rad]
  int32 I,    OmegaP, Ecc;    ///< Inclination [rad], argument of perigee [rad], eccentricity [-]  
  int64 R;                    ///< Clock bias  [10^-12 sec]
  int32 OscillOffset;         ///< Clock drift [10^-6 ppm]
  int16 GDOP;                 ///< Geometrical delusion of precision [-]
  int16 PDOP;                 ///< Position delusion of precision    [-]
  int16 TDOP;                 ///< Time delusion of precision        [-]
  int16 HDOP;                 ///< Horizontal delusion of precision  [-]
  int16 VDOP;                 ///< Vertical delusion of precision    [-]
  int8  fNT_Condition;        ///< last navigation task condition // no sol., sol.
}
USERfix;

typedef struct
{
  int16 wn;                   ///< Receiver time (current week number) [week]
  int64 t;                    ///< Receiver time (current time within week) [mks]
  int64 X, Y, Z;              ///< User ECEF X,Y,Z-coordinates, [mm]
  int32 Xdot, Ydot,   Zdot;   ///< User X,Y,Z-coordinates' derivatives, [m/s]
  int64 Lon,  Lat,    H;      ///< User longitude, latitude [radians] and height [m]
  int32 Ndot, Edot,   Udot;   ///< User vehicle velocity in LLA frame
  int64 A,    Omega,  M;      ///< Semi-major axis [m], right ascension of ascending node [rad], mean anomaly [rad]
  int32 I,    OmegaP, Ecc;    ///< Inclination [rad], argument of perigee [rad], eccentricity [-]  
  int64 R;                    ///< Clock bias  [10^-12 sec]
  int32 OscillOffset;         ///< Clock drift [10^-6 ppm]
  int8  fOP_Condition;        ///< last OP solution condition // no sol; prop; est
}
USERop;

typedef struct
{
  float64  Xdot, Ydot, Zdot;  ///< User X,Y,Z-coordinates' derivatives, [m/s]
  float64  X,    Y,    Z;     ///< User ECEF X,Y,Z-coordinates, [m]
}
tECEF;

typedef struct
{
  float64  Ndot, Edot, Udot;  ///< User vehicle velocity in LLA frame
  float64  Lon,  Lat,  H;     ///< User longitude, latitude [radians] and height [m]
}
tLLA;

typedef struct
{
  float64  A;                 ///< Semi-major axis [m]
  float64  Ecc;               ///< Eccentricity [-]
  float64  I;                 ///< Inclination [rad]
  float64  Omega;             ///< Right ascension of ascending node [rad]
  float64  OmegaP;            ///< Argument of perigee [rad]
  float64  M;                 ///< Mean anomaly [rad]
}
tOE;

/// User status information
typedef struct
{
  int8     CoordValid;        ///< Flag of valid user's coordinates
  int8     SolutionFlag;      ///< Flag of Navigation Task solution
  tECEF    ecef;              ///< Position and velocity are calculated in ECEF frame
  tLLA     geo;               ///< Position and velocity are calculated in LLA frame
  tOE      oe;                ///< Keplerian Orbital Elements in current Equatorial ECI Frame (based on instant equinox)
  float64  R,  Rdot;          ///< Clock bias and drift, [m] and [m/s]
  int16    wn;                ///< Receiver time (current week number) [week]
  float64  t;                 ///< Receiver time (current time within week) [s]
  float64  TOPO[3][3];        ///< User's local coordinate system basis
  float64  GDOP;              ///< Geometrical delusion of precision [-]
  float64  PDOP;              ///< Position delusion of precision [-]
  float64  TDOP;              ///< Time delusion of precision [-]
  float64  HDOP;              ///< Horizontal delusion of precision [-]
  float64  VDOP;              ///< Vertical delusion of precision [-]
  USERfix  User_fix;          ///< User information to integer format
  int8     flag_bl;           ///< blocking flag
} USER;

/// Navigation task conditions
typedef struct
{
  int16    wn;                ///< week number for measurements
  float64  t;                 ///< time in week for measurements
  int16    wnOP;              ///< week number for measurements
  float64  tOP;               ///< time in week for measurements
  LIST12   NavSV;             ///< List of SVs with computed raw measurements
  LIST12   NavSVOld;          ///< List of SVs with calculated coordinates
  uint32   alm_set_flgs;      ///< almanac validity flags
  uint32   health_flgs;       ///< health of satellite flags
  uint32   visibility_flgs;   ///< visibility of satellite flags
  float32  ElevMask;          ///< Elevation mask [radians]
  int8     IonosphereModel;   ///< Ionosphere model type: 0 - no Ion corrections, 1 - Klobuchar model; 2 - Space Constant Ion model;  default = 0
  int8     TroposphereModel;  ///< Troposphere model type: 0 - no Tropo corrections, 1 - standatd Tropo corrections; default = 0
  int8     OrbStatic;         ///< Orbital/Static OP model: 0 - orbital model, 1 - static model; default = 0
  int8     OPmode;            ///< OP mode: 0 -only navigation, 1 - navigation and OP; default = 1
  float32  DOP;               ///< DOP mask [-]
  uint32   timeTracking;      ///< SWI_TrackingManager() work time [ms]
  int32    RAIMKey;           ///< Key of RAIM option (0 - OFF, 1 - ON)
  int16    UERE;              ///< RAIM threshold
  int32    CPU;               ///< CPU loading level [%]
} NAV_CONDITIONS;

/// GPS ephemeris structure
typedef struct
{ 
  int8     prn;       ///< satellite PRN number (0..31) [-]
  int8     health;    ///< satellite health
  int16    wn;        ///< GPS week number (1..1023) [-]
  int32    tow;       ///< second of GPS week (0..604799) [s]
  float32  tgd;       ///< group delay [s]
  int16    iodc;      ///< clock data issue
  int32    toc;       ///< clock data reference time (0..604784) [s]
  float32  af0;       ///< clock correction [s]
  float32  af1;       ///< clock correction [s/s]
  float32  af2;       ///< clock correction [s/s^2]
  int16    iode;      ///< orbit data issue
  float32  deltan;    ///< mean anomaly correction [radians/s] not [semicycles/s]
  float64  m0;        ///< mean anomaly at reference time [radians] not [semicycles]
  float64  e;         ///< eccentricity [-]
  float64  roota;     ///< square root of semi-major axis [m^{1/2}]
  int32    toe;       ///< reference time for orbit [s]
  float32  cic;       ///< harmonic correction term [radians]
  float32  cis;       ///< harmonic correction term [radians]
  float32  cuc;       ///< harmonic correction term [radians]
  float32  cus;       ///< harmonic correction term [radians]
  float32  crc;       ///< harmonic correction term [m]
  float32  crs;       ///< harmonic correction term [m]
  float64  omega0;    ///< longitude of the ascension node [radians] not [semicycles]
  float64  omega;     ///< argument of perigee [radians] not [semicycles]
  float64  i0;        ///< inclination angle [radians] not [semicycles]
  float32  omegadot;  ///< rate of right ascension [radians/s] not [semicycles/s]
  float32  idot;      ///< rate of inclination [radians/s] not [semicycles/s]
  int16    accuracy;  ///< user range accuracy (URA), coded 0..15
  int32    flag_bl;   ///< blocking flag
} EPH_GPS;

/// GPS almanac structure
typedef struct
{
  int8     prn;       ///< satellite PRN number (0..31) [-]
  int8     health;    ///< satellite health (1-bad, 0-good) [-]
  int16    wna;       ///< almanac week number [-]
  int32    toa;       ///< reference time for orbit [s]
  float64  roota;     ///< square root of semi-major axis [m^{1/2}]
  float64  omega0;    ///< longitude of the ascension node [radians] not [semicycles]
  float64  omega;     ///< argument of perigee [radians] not [semicycles]
  float64  m0;        ///< mean anomaly at reference time [radians] not [semicycles]
  float32  e;         ///< eccentricity [-]
  float32  i0;        ///< inclination angle [radians] not [semicycles]
  float32  omegadot;  ///< rate of right ascension [radians/s] not [semicycles/s]
  float32  af0;       ///< satellite clock shift [s]
  float32  af1;       ///< satellite clock rate  [s/s]
  int32    flag_bl;   ///< blocking flag
} ALM_GPS;

/// Raw almanac structure
typedef struct
{ 
  signed   prn       : 8;   ///<  8 bits Satellite PRN number (0..31) [-]
  signed   wna       : 16;  ///< 10 bits Almanac week number (1..1023)
  unsigned toa       : 8;   ///<  8 bits Time of applicability 
  signed   e         : 32;  ///< 16 bits Eccentricity
  signed   i0        : 16;  ///< 16 bits Orbital inclination  deviation ( relative to I0 = 0.30 semicircles)
  signed   omegadot  : 16;  ///< 16 bits Rate of right ascension
  signed   roota     : 32;  ///< 24 bits Square root of the semi major axis
  signed   omega0    : 32;  ///< 24 bits Longitude of ascending node of orbit plane at reference time
  signed   omega     : 32;  ///< 24 bits Argument of perigee
  signed   m0        : 32;  ///< 24 bits Mean anomaly at reference time
  signed   af0       : 16;  ///< 11 bits Truncated clock correction parameters
  signed   af1       : 16;  ///< 11 bits // summa = 32 bytes
  signed   health    : 16;  ///< 8 bits Satellite health
  
  signed   valid     : 16;  ///< used for raw almanac readiness control // summa = 36 bytes
} RAW_ALM_GPS;

/// Definition of the structure for temporary storage of almanac parameters
typedef struct
{
  uint32       flag;     ///< set flag when ready raw almanac
  int32        flag_bl;  ///< blocking flag
  RAW_ALM_GPS  TAlmGPS;  ///< raw almanac structure
} TEMP_ALM_GPS;  

/// Raw ephemeris structure
typedef struct
{ 
  signed   prn       : 8;   ///<  8 bits Satellite PRN number (0..31) [-]
  signed   wn        : 16;  ///< 10 bits Current GPS week number (10  LSB bits are in use)  (1..1023)
  signed   tgd       : 8;   ///<  8 bits Time group delay (second)
  unsigned toc       : 16;  ///< 16 bits Clock data reference time
  signed   af1       : 16;  ///< 16 bits Clock correction
  signed   af0       : 32;  ///< 22 bits Clock correction
  signed   af2       : 8;   ///<  8 bits Clock correction
  unsigned iode      : 8;   ///<  8 bits Issue of data (ephemeris)
  signed   deltan    : 16;  ///< 16 bits Mean motion difference from computed value  
  signed   m0        : 32;  ///< 32 bits Mean anomaly at reference time
  unsigned e         : 32;  ///< 32 bits Eccentricity of the orbit
  unsigned roota     : 32;  ///< 32 bits Square root of the semi major axis
  signed   omega0    : 32;  ///< 32 bits Longitude of ascending node of orbit plane at reference time
  signed   i0        : 32;  ///< 32 bits Inclination angle at reference time
  signed   omega     : 32;  ///< 32 bits Argument of perigee
  signed   omegadot  : 32;  ///< 24 bits Rate of right ascension
  unsigned toe       : 16;  ///< 16 bits Reference time for ephemeris
  signed   idot      : 16;  ///< 14 bits Rate of inclination angle
  signed   crc       : 16;  ///< 16 bits Amplitude of the cosine harmonic correction term to the orbit radius  
  signed   crs       : 16;  ///< 16 bits Amplitude of the sine harmonic correction term to the orbit radius
  signed   cuc       : 16;  ///< 16 bits Amplitude of the cosine harmonic correction term to the argument of latitude
  signed   cus       : 16;  ///< 16 bits Amplitude of the sine harmonic correction term to the argument of latitude
  signed   cic       : 16;  ///< 16 bits Amplitude of the cosine harmonic correction term to the to the angle of inclination
  signed   cis       : 16;  ///< 16 bits Amplitude of the sine harmonic correction term to the angle of inclination // summa = 60 bytes
  signed   accuracy  : 16;  ///<  4 bits User range accuracy (URA), coded 0..15
  signed   health    : 16;  ///<  6 bits Satellite health

  signed   iodc      : 16;  ///< 10 bits Clock data issue
  signed   valid     : 16;  ///< used for raw ephemeris readiness control // summa = 68 bytes
} RAW_EPH_GPS;

/// Definition of the structure for temporary storage of ephemeris parameters
typedef struct
{ 
  RAW_EPH_GPS  TEphGPS;  ///< raw ephemeris structure
  uint32       flag;     ///< set flag when ready raw ephemeris
  int32        flag_bl;  ///< blocking flag
} 
TEMP_EPH_GPS; 

/// ION and UTC parameters structure
typedef struct
{ 
  ///< Parameters of Klobuchar ionosphere model
  float32  alpha0, alpha1, alpha2, alpha3;  ///< ION amplitude coefficients
  float32  beta0,  beta1,  beta2,  beta3;   ///< ION period coefficients
  
  ///< UTC parameters
  float64  A0, A1;   ///< UTC coefficients, [s] and [s/s] correspondingly
  int32    tot;      ///< reference time for UTC data, [s]
  int16    WNt;      ///< UTC reference week number
  int16    WNlsf;    //
  int8     dtls;     ///< delta time due to leap second
  int8     dtlsf;    //
  int8     DN;       ///< UTC reference day number
  int32    flag_bl;  ///< blocking flag
} ION_UTC;

/// Raw ionospheric correction parameters message
typedef struct
{
  signed alpha0 : 8;  ///< The coefficients of a cubic equation representing the amplitude of the vertical delay [sec]
  signed alpha1 : 8;  ///< [sec/semicircle]
  signed alpha2 : 8;  ///< [sec/semicircle^2]
  signed alpha3 : 8;  ///< [sec/semicircle^3]
  signed beta0  : 8;  ///< The coefficients of a cubic equation representing the period of the model [sec]
  signed beta1  : 8;  ///< [sec/semicircle]
  signed beta2  : 8;  ///< [sec/semicircle^2]
  signed beta3  : 8;  ///< [sec/semicircle^3]
}
RAW_ION;

/// Raw UTC-GPS clock correction parameters message
typedef struct
{
  signed   A0     : 32;  ///< UTC coefficients [s]
  signed   A1     : 32;  ///< UTC coefficients, [s/s]
  signed   tot    : 16;  ///< Reference time for UTC data
  signed   WNt    : 16;  ///< UTC reference week number
  signed   dtls   : 16;  ///< Delta time due to leap seconds
  signed   WNlsf  : 16;  ///< Week number
  signed   DN     : 8;   ///< UTC reference day number
  signed   dtlsf  : 8;   ///< Delta time due to leap second
  signed   WN     : 16;  ///< Current week number
}
RAW_UTC;

/// Raw ionospheric and UTC-GPS clock corrections
typedef struct
{
  RAW_ION  ION;  ///< raw ionospheric correction parameters
  RAW_UTC  UTC;  ///< raw UTC-GPS clock correction parameters
}
RAW_ION_UTC;

/// Structure for temporary storage of ION and UTC-GPS clock parameters
typedef struct
{ 
  RAW_ION_UTC  TIonUTC;  ///< raw ionospheric and UTC-GPS clock corrections
  uint32       flag;     ///< set flag when ready raw ionospheric and UTC-GPS clock corrections
  uint32       flag_bl;  ///< blocking flag
} 
TEMP_ION_UTC;

/// maximal length string message
#define MAX_MES_STR_LENGTH  (512)

/// structure forming string
typedef struct
{
  int8  str[MAX_MES_STR_LENGTH];  ///< data buffer
  int32 length;                   ///< data length
}
DATA_STRING;

#define SAT_MES  (0x1)   ///< satellites information message
#define POS_MES  (0x2)   ///< user position and velocities message
#define DOP_MES  (0x4)   ///< DOP information message
#define RAW_MES  (0x8)   ///< RAW data of satellites message
#define OPP_MES  (0x10)  ///< OPP information message
#define SQE_MES  (0x20)  ///< satellite quality message
#define ATM_MES  (0x40)  ///< satellite atmosphere message

/// maximal length binary message
#define MAX_MES_BIN_LENGTH  (2048)

/// structure forming string
typedef struct
{
  uint16 length;                   ///< data length
  uint8  param;                    ///< message parameter
  uint8  opcode;                   ///< message opcode
  uint8  str[MAX_MES_BIN_LENGTH];  ///< data buffer
  uint8  check_sum;                ///< data check sum
  uint8  param_old;                ///< previous message parameter
  uint8  opcode_old;               ///< previous message opcode
}
DATA_BIN;

#define SYNC             (0xAA)  ///< codeword for RS422

/// opcode of binary messages
#define DATA_HR          (1)  ///< message with data from Host to GPS receiver
#define DATA_RH          (2)  ///< message with data from GPS receiver to Host
#define COMMAND          (3)  ///< message without data from Host to GPS receiver without acknowledge request
#define COMMAND_ACK      (7)  ///< message without data from Host to GPS receiver with acknowledge request

/// param of binary messages: message without data from Host to GPS receiver
#define ACK_REQ          (0)  ///< Acknowledge request
#define NMEA_STR         (1)  ///< NMEA string mode
#define BUILT_IN_TEST    (2)  ///< Request Built-in Test
#define SW_RESET         (3)  ///< Software reset
#define CLEAR_EPH_ALM    (4)  ///< Clear ephemeris and almanac 

/// param of binary messages: message with data from Host to GPS receiver 
#define DHR_CONFIG       (0)  ///< Configuration setup
#define DHR_ALMANAC      (1)  ///< Almanac upload 
#define DHR_EPHEMERIS    (2)  ///< Ephemeris upload
#define DHR_IONOSPHERE   (3)  ///< Ionosphere parameters upload
#define DHR_UTC_GPS      (4)  ///< UTC-GPS clock correction upload
#define DHR_INIT_POS     (5)  ///< Initial position (ECEF/Geo/OE)
#define DHR_SW_SYSTEM    (6)  ///< S/W upload system

/// param of binary messages: message with data from GPS receiver to Host
#define DRH_CONFIG          (0)   ///< Configuration setup
#define DRH_BIT_STATUS      (1)   ///< BIT and status
#define DRH_TIME_CH_STATUS  (2)   ///< Time and channels status
#define DRH_CH_RM           (3)   ///< Channels range measurements
#define DRH_LOS             (4)   ///< Line of Sight Vectors
#define DRH_NAV_STATUS      (5)   ///< Navigation and status (ECEF/Geo/OE)
#define DRH_OP_NAV_STATUS   (6)   ///< OP Navigation and status (ECEF/Geo/OE)
#define DRH_ALMANAC         (7)   ///< Almanac data
#define DRH_EPHEMERIS       (8)   ///< Ephemeris data
#define DRH_INOSPHERE       (9)   ///< Ionosphere data
#define DRH_UTC_GPS         (10)  ///< UTC-GPS clock correction
#define DRH_SW_SYSTEM       (11)  ///< SW system upload 
#define DRH_ADDITIONAL_INF  (12)  ///< Additional information

/// size of message
#define LENGTH_CONFIG          (20)    ///< Configuration Setup 
#define LENGTH_BIT_STATUS      (20)    ///< BIT and Status
#define LENGTH_TIME_CH_STATUS  (68)    ///< Time and Channels Status
#define LENGTH_RM              (248)   ///< Channels Range Measurements
#define LENGTH_LOS             (392)   ///< Line of Sight Vectors
#define LENGTH_NAV_STATUS      (80)    ///< Navigation and Status 
#define LENGTH_OP_NAV_STATUS   (72)    ///< OP Navigation and Status
#define LENGTH_ALMANAC         (1152)  ///< Almanac data
#define LENGTH_EPHEMERIS       (2048)  ///< Ephemeris data 
#define LENGTH_IONOSPHERE      (8)     ///< Ionosphere correction data
#define LENGTH_UTC_GPS         (20)    ///< UTC-GPS clock correction
#define LENGTH_INIT            (48)    ///< ECEF/Geo/OE initial data
#define LENGTH_ADDITIONAL_INF  (4)     ///< Additional information
#define LENGTH_MAX_SW_SM       (15)    ///< Length maximum part of file System from GPS receiver to Host
#define LENGTH_SW_SM           (20)    ///< Length message for software download System from GPS receiver to Host
#define LENGTH_FILE            (128)   ///< Length file from Host to GPS receiver message

// define for ICD message
#define LENGTH_HEADER          (5)     ///< bytes number for header message from Host to GPS receiver
#define SIZE_EPHEMERIS         (64)    ///< ephemeris structure size
#define SIZE_ALMANAC           (36)    ///< almanac structure size

#define CF_ECEF (0x0)  ///< 0 - output in ECEF
#define CF_GEO  (0x1)  ///< 1 - output in LLA
#define CF_OE   (0x2)  ///< 2 - output Orbital Elements in ECI

/// Configuration setup
typedef struct 
{
  signed ElevMask         : 16;  ///< satellite elevation mask; default = 0 [degree]
  signed NavSolRate       : 16;  ///< navigation solution rate in ms: default - 1000
  signed CoefDLL          : 32;  ///< DLL time parameter; default = 1250
  signed DLLtime          : 8;   ///< pre-detection integrated time (1, 2, 4, 5, 10, 20); default = 1
  signed ECEF             : 8;   ///< ECEF/Geo/OE: 0 - output in ECEF, 1 - output in LLA; 2 - output Orbital Elements in ECI; default = 0
  signed OPmode           : 8;   ///< OP mode: 0 -only navigation, 1 - navigation and OP; default = 1
  signed IonosphereModel  : 8;   ///< Ionosphere model type: 0 - no Ion corrections, 1 - Klobuchar model; 2 - Space Constant Ion model;  default = 0
  signed TroposphereModel : 8;   ///< Troposphere model type: 0 - no Tropo corrections, 1 - standatd Tropo corrections; default = 0
  signed OrbStatic        : 8;   ///< Orbital/Static OP model: 0 - orbital model, 1 - static model; default = 0
  signed BaudRate422      : 8;   ///< baud rate; bit map: 0 - 4800, 1 - 9600, 2 - 19200, 3 - 38400, 4 - 57600, 5 - 115200
  signed RAIMth           : 8;   ///< RAIM threshold [meter]; default = 100
  signed SNRth            : 8;   ///< C/N0 threshold for tracking [dB]; default = 27
  signed UpdateConfig     : 24;  ///< Update configuration in FPGA flag: 0 - not update, 1 - update
}
CONFIG_SETUP;

/// BIT data
typedef struct
{
  signed  daySystem         : 8;  ///< Date of compilation of system software file
  signed  monthSystem       : 8;
  signed  yearSystem        : 16;
  signed  sizeSystem        : 32; ///< System software size
  signed  versionSystem     : 16; ///< System software version
  signed  subVersionSystem  : 16; ///< System software subversion
  signed  checkSumSystem    : 32; ///< System software check sum
  signed  statusFlags       : 16; ///< Status flags
  signed  antNoise          : 16; ///< Antenna measured noise
}
tBuiltInTest;

/// Almanac data message
typedef struct
{ 
  signed   prn       : 8;   ///<  8 bits Satellite PRN number (0..31) [-]
  signed   wna       : 16;  ///< 10 bits Almanac week number (1..1023)
  unsigned toa       : 8;   ///<  8 bits Time of applicability 
  signed   e         : 32;  ///< 16 bits Eccentricity
  signed   i0        : 16;  ///< 16 bits Orbital inclination  deviation ( relative to I0 = 0.30 semicircles)
  signed   omegadot  : 16;  ///< 16 bits Rate of right ascension
  signed   roota     : 32;  ///< 24 bits Square root of the semi major axis
  signed   omega0    : 32;  ///< 24 bits Longitude of ascending node of orbit plane at reference time
  signed   omega     : 32;  ///< 24 bits Argument of perigee
  signed   m0        : 32;  ///< 24 bits Mean anomaly at reference time
  signed   af0       : 16;  ///< 11 bits Truncated clock correction parameters
  signed   af1       : 16;  ///< 11 bits // summa = 32 bytes
  signed   health    : 16;  ///< 8 bits Satellite health
  signed   spare     : 16;
} 
MES_ALM_GPS;   

typedef struct
{
  MES_ALM_GPS almanac[GPS_SATELLITE_COUNT];
}
tMesAlmanac;

/// Ephemeris data message
typedef struct
{ 
  signed   prn       : 8;   ///<  8 bits Satellite PRN number (0..31) [-]
  signed   wn        : 16;  ///< 10 bits Current GPS week number (10  LSB bits are in use)  (1..1023)
  signed   tgd       : 8;   ///<  8 bits Time group delay (second)
  unsigned toc       : 16;  ///< 16 bits Clock data reference time
  signed   af1       : 16;  ///< 16 bits Clock correction
  signed   af0       : 32;  ///< 22 bits Clock correction
  signed   af2       : 8;   ///<  8 bits Clock correction
  unsigned iode      : 8;   ///<  8 bits Issue of data (ephemeris)
  signed   deltan    : 16;  ///< 16 bits Mean motion difference from computed value  
  signed   m0        : 32;  ///< 32 bits Mean anomaly at reference time
  unsigned e         : 32;  ///< 32 bits Eccentricity of the orbit
  unsigned roota     : 32;  ///< 32 bits Square root of the semi major axis
  signed   omega0    : 32;  ///< 32 bits Longitude of ascending node of orbit plane at reference time
  signed   i0        : 32;  ///< 32 bits Inclination angle at reference time
  signed   omega     : 32;  ///< 32 bits Argument of perigee
  signed   omegadot  : 32;  ///< 24 bits Rate of right ascension
  unsigned toe       : 16;  ///< 16 bits Reference time for ephemeris 
  signed   idot      : 16;  ///< 14 bits Rate of inclination angle
  signed   crc       : 16;  ///< 16 bits Amplitude of the cosine harmonic correction term to the orbit radius  
  signed   crs       : 16;  ///< 16 bits Amplitude of the sine harmonic correction term to the orbit radius
  signed   cuc       : 16;  ///< 16 bits Amplitude of the cosine harmonic correction term to the argument of latitude
  signed   cus       : 16;  ///< 16 bits Amplitude of the sine harmonic correction term to the argument of latitude
  signed   cic       : 16;  ///< 16 bits Amplitude of the cosine harmonic correction term to the to the angle of inclination
  signed   cis       : 16;  ///< 16 bits Amplitude of the sine harmonic correction term to the angle of inclination // summa = 60 bytes
  signed   accuracy  : 16;  ///<  4 bits User range accuracy (URA), coded 0..15
  signed   health    : 16;  ///<  6 bits Satellite health
} 
MES_EPH_GPS;      

typedef struct
{
  MES_EPH_GPS ephemeris[GPS_SATELLITE_COUNT];
}
tMesEphemeris;       

/// Time and channels status data message
typedef struct
{
  signed   sv_id    : 8;  ///< Satellite PRN number (0..31) [-]
  signed   SNR      : 8;  ///< Signal-to-noise ratio [dBHz]
  signed   status   : 8;  ///< Status
  unsigned phase_l  : 8;  ///< phase error [0.1 degrees]
  signed   phase_h  : 8;
}
tSatChStatus;

typedef struct
{
  signed   wn     : 16;  ///< GPS week number since Epoch
  unsigned mks_l  : 16;  ///< GPS time tag from start of the week [mks]
  signed   mks_h  : 32;  ///< GPS time tag from start of the week [mks]
  tSatChStatus satCh[GPS_SAT_CHANNELS_COUNT];
}
tTimeChStatus;

/// Channels Range Measurements message
typedef struct
{
  signed   num_sat    : 8;   ///< Number of satellites or spare
  signed   sv_id      : 8;   ///< Satellite PRN number (0..31) [-]
  unsigned psr_l      : 16;  ///< Pseudo range to satellite, [mm]
  signed   psr_h      : 32;
  signed   psrr       : 32;  ///< Pseudo range rate to satellite, [mm/s]
  signed   psr_err_b  : 32;  ///< Pseudo range estimated error (bias) to satellite,  [mm]
  signed   psr_err_n  : 16;  ///< Pseudo range estimated error (noise) to satellite, [mm]
  signed   psrr_err   : 16;  ///< Pseudo range rate estimated error (noise) to satellite, [mm/s]
}
tSatRM;

typedef struct
{
  signed   wn     : 16;  ///< GPS week number since Epoch
  unsigned mks_l  : 16;  ///< GPS time tag from start of the week [mks]
  signed   mks_h  : 32;  ///< GPS time tag from start of the week [mks]
  tSatRM   satRM[GPS_SAT_CHANNELS_COUNT];
}
tChRM;

/// Line of Sight Vectors data message
typedef struct
{
  signed   num_sat  : 8;   ///< Number of satellites or spare
  signed   sv_id    : 8;   ///< Satellite PRN number (0..31) [-]
  signed   SNR      : 16;  ///< Signal-to-noise ratio [dBHz]
  signed   elev     : 16;  ///< Elevation [0.1 degrees]
  signed   azim     : 16;  ///< Azimuth [0.1 degrees]
  int64    losX;           ///< X component of LOS vector in ECEF, [mm]
  int64    losY;           ///< Y component of LOS vector in ECEF, [mm]
  int64    losZ;           ///< Z component of LOS vector in ECEF, [mm]
}
tSatLOS;

typedef struct
{
  signed   wn     : 16;  ///< GPS week number since Epoch
  unsigned mks_l  : 16;  ///< GPS time tag from start of the week [mks]
  signed   mks_h  : 32;  ///< GPS time tag from start of the week [mks]
  tSatLOS  satLos[GPS_SAT_CHANNELS_COUNT];
}
tDataLOS;

/// Line of Sight Vectors data message
typedef struct
{
  tDataLOS  message;  ///< message
  uint32    flag_bl;  ///< block flag
}
tDataLOSMess;

/// ECEF/Geo/OE navigation and status data message

/// Navigation status map
#define NS_NO_3D_MODE (0x0)  ///< no 3D solution
#define NS_3D_MODE    (0x1)  ///< 3D solution
#define NS_ECEF       (0x0)  ///< ECEF format
#define NS_GEO        (0x2)  ///< Geographic format
#define NS_OE         (0x4)  ///< Keplerian Orbital Elements format

/// ECEF format
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     X;                  ///< Host vehicle position in ECEF frame [mm]
  int64     Y;
  int64     Z;
  signed    VX           : 32;  ///< Host vehicle velocity in ECEF frame [mm/sec]  
  signed    VY           : 32;
  signed    VZ           : 32;
  signed    OscillOffset : 32;  ///< Device clock drift [10^-6 ppm]
  int64     R;                  ///< Device clock bias  [10^-12 sec]
  unsigned  Almanac      : 32;  ///< satellites almanac bit map
  unsigned  Health       : 32;  ///< health of satellites bit map
  unsigned  Visibility   : 32;  ///< visibility of satellites bit map
  unsigned  NavStatus    : 16;  ///< Bit 0: (0 - No Mode 3D, 1 - Mode 3D) Bit 1-2: (0 - ECEF, 1 - GEO, 2 - OE)
  signed    CPU          : 16;  ///< CPU loading level
  signed    GDOP         : 16;  ///< Geometrical Dilution Of Precision 
  signed    PDOP         : 16;  ///< Position Dilution Of Precision
  signed    HDOP         : 16;  ///< Height Dilution Of Precision
  signed    TDOP         : 16;  ///< Time Dilution Of Precision
}
tECEFNavStatus;

/// Geographic format
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     Lat;                ///< Host vehicle position in LLA frame [rad]
  int64     Lon;
  int64     Alt;                ///< [mm]
  signed    VNorth       : 32;  ///< Host vehicle velocity in LLA frame [mm/sec]  
  signed    VEest        : 32;
  signed    VUp          : 32;
  signed    OscillOffset : 32;  ///< Device clock drift [10^-6 ppm]
  int64     R;                  ///< Device clock bias  [10^-12 sec]
  unsigned  Almanac      : 32;  ///< satellites almanac bit map
  unsigned  Health       : 32;  ///< health of satellites bit map
  unsigned  Visibility   : 32;  ///< visibility of satellites bit map
  unsigned  NavStatus    : 16;  ///< Bit 0: (0 - No Mode 3D, 1 - Mode 3D) Bit 1-2: (0 - ECEF, 1 - GEO, 2 - OE)
  signed    CPU          : 16;  ///< CPU loading level
  signed    GDOP         : 16;  ///< Geometrical Dilution Of Precision 
  signed    PDOP         : 16;  ///< Position Dilution Of Precision
  signed    HDOP         : 16;  ///< Height Dilution Of Precision
  signed    TDOP         : 16;  ///< Time Dilution Of Precision
}
tGeoNavStatus;

/// Keplerian Orbital Elements in current Equatorial ECI Frame (based on instant equinox) 
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     A;                  ///< Semi-major axis [mm]
  int64     Omega;              ///< Right ascension of ascending node [2^-48 rad.]
  int64     M;                  ///< Mean anomaly [2^-48 rad.]
  signed    I            : 32;  ///< Inclination [2^-29 rad.]
  signed    OmegaP       : 32;  ///< Argument of perigee [2^-29 rad.]
  signed    Ecc          : 32;  ///< Eccentricity [2^-31]
  signed    OscillOffset : 32;  ///< Device clock drift [10^-6 ppm]
  int64     R;                  ///< Device clock bias  [10^-12 sec]
  unsigned  Almanac      : 32;  ///< satellites almanac bit map
  unsigned  Health       : 32;  ///< health of satellites bit map
  unsigned  Visibility   : 32;  ///< visibility of satellites bit map
  unsigned  NavStatus    : 16;  ///< Bit 0: (0 - No Mode 3D, 1 - Mode 3D) Bit 1-2: (0 - ECEF, 1 - GEO, 2 - OE)
  signed    CPU          : 16;  ///< CPU loading level
  signed    GDOP         : 16;  ///< Geometrical Dilution Of Precision 
  signed    PDOP         : 16;  ///< Position Dilution Of Precision
  signed    HDOP         : 16;  ///< Height Dilution Of Precision
  signed    TDOP         : 16;  ///< Time Dilution Of Precision
}
tOENavStatus;

/// ECEF/Geo/OE
typedef struct
{
  tECEFNavStatus ecef;
  tGeoNavStatus  geo;
  tOENavStatus   oe;
}
NavMessage;

/// Navigation message
typedef struct
{
  NavMessage  message;  ///< message
  uint32      flag_bl;  ///< block flag
}
tNavStatusData;

/// OP ECEF/Geo/OE navigation and status data message

/// ECEF format
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     X;                  ///< Host vehicle position in ECEF frame [mm]
  int64     Y;
  int64     Z;
  signed    VX           : 32;  ///< Host vehicle velocity in ECEF frame [mm/sec]  
  signed    VY           : 32;
  signed    VZ           : 32;
  signed    OscillOffset : 32;  ///< Device clock drift [10^-6 ppm]
  int64     R;                  ///< Device clock bias  [10^-12 sec]
  unsigned  Almanac      : 32;  ///< satellites almanac bit map
  unsigned  Health       : 32;  ///< health of satellites bit map
  unsigned  Visibility   : 32;  ///< visibility of satellites bit map
  unsigned  NavStatus    : 16;  ///< Bit 0: (0 - No Mode 3D, 1 - Mode 3D) Bit 1-2: (0 - ECEF, 1 - GEO, 2 - OE)
  signed    CPU          : 16;  ///< CPU loading level
}
tECEFOPNavStatus;

/// Geographic format
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     Lat;                ///< Host vehicle position in LLA frame [rad]
  int64     Lon;
  int64     Alt;                ///< [mm]
  signed    VNorth       : 32;  ///< Host vehicle velocity in LLA frame [mm/sec]  
  signed    VEest        : 32;
  signed    VUp          : 32;
  signed    OscillOffset : 32;  ///< Device clock drift [10^-6 ppm]
  int64     R;                  ///< Device clock bias  [10^-12 sec]
  unsigned  Almanac      : 32;  ///< satellites almanac bit map
  unsigned  Health       : 32;  ///< health of satellites bit map
  unsigned  Visibility   : 32;  ///< visibility of satellites bit map
  unsigned  NavStatus    : 16;  ///< Bit 0: (0 - No Mode 3D, 1 - Mode 3D) Bit 1-2: (0 - ECEF, 1 - GEO, 2 - OE)
  signed    CPU          : 16;  ///< CPU loading level
}
tGeoOPNavStatus;

/// Keplerian Orbital Elements in current Equatorial ECI Frame (based on instant equinox) 
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     A;                  ///< Semi-major axis [mm]
  int64     Omega;              ///< Right ascension of ascending node [2^-48 rad.]
  int64     M;                  ///< Mean anomaly [2^-48 rad.]
  signed    I            : 32;  ///< Inclination [2^-29 rad.]
  signed    OmegaP       : 32;  ///< Argument of perigee [2^-29 rad.]
  signed    Ecc          : 32;  ///< Eccentricity [2^-31]
  signed    OscillOffset : 32;  ///< Device clock drift [10^-6 ppm]
  int64     R;                  ///< Device clock bias  [10^-12 sec]
  unsigned  Almanac      : 32;  ///< satellites almanac bit map
  unsigned  Health       : 32;  ///< health of satellites bit map
  unsigned  Visibility   : 32;  ///< visibility of satellites bit map
  unsigned  NavStatus    : 16;  ///< Bit 0: (0 - No Mode 3D, 1 - Mode 3D) Bit 1-2: (0 - ECEF, 1 - GEO, 2 - OE)
  signed    CPU          : 16;  ///< CPU loading level
}
tOEOPNavStatus;

/// ECEF/Geo/OE
typedef struct
{
  tECEFOPNavStatus ecef;
  tGeoOPNavStatus  geo;
  tOEOPNavStatus   oe;
}
OPNavMessage;

/// OP Navigation message
typedef struct
{
  OPNavMessage  message;  ///< message
  uint32        flag_bl;  ///< block flag
}
tOPNavStatusData;

/// ECEF/Geo/OE initial data message

/// ECEF format
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     X;                  ///< Host vehicle position in ECEF frame [mm]
  int64     Y;
  int64     Z;
  signed    VX           : 32;  ///< Host vehicle velocity in ECEF frame [mm/sec]  
  signed    VY           : 32;
  signed    VZ           : 32;
  signed    status       : 32;  ///< status: 0 - ECEF, 1 - Geo, 2 - OE
}
tInitECEF;

/// Geographic format
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     Lat;                ///< Host vehicle position in LLA frame [rad]
  int64     Lon;
  int64     Alt;                ///< [mm]
  signed    VNorth       : 32;  ///< Host vehicle velocity in LLA frame [mm/sec]  
  signed    VEest        : 32;
  signed    VUp          : 32;
  signed    status       : 32;  ///< status: 0 - ECEF, 1 - Geo, 2 - OE
}
tInitGeo;

/// Keplerian Orbital Elements in current Equatorial ECI Frame (based on instant equinox) 
typedef struct
{
  signed    wn           : 16;  ///< GPS week number since Epoch
  unsigned  mks_l        : 16;  ///< GPS time tag from start of the week [mks]
  signed    mks_h        : 32;  ///< GPS time tag from start of the week [mks]
  int64     A;                  ///< Semi-major axis [mm]
  int64     Omega;              ///< Right ascension of ascending node [2^-48 rad.]
  int64     M;                  ///< Mean anomaly [2^-48 rad.]
  signed    I            : 32;  ///< Inclination [2^-29 rad.]
  signed    OmegaP       : 32;  ///< Argument of perigee [2^-29 rad.]
  signed    Ecc          : 32;  ///< Eccentricity [2^-31]
  signed    status       : 32;  ///< status: 0 - ECEF, 1 - Geo, 2 - OE
}
tInitOE;

/// ECEF/Geo/OE
typedef union
{
  tInitECEF ecef;      ///< ECEF format
  tInitGeo  geo;       ///< Geographic format
  tInitOE   oe;        ///< Keplerian Orbital Elements in current Equatorial ECI Frame (based on instant equinox) 
}
tInitData;

typedef struct 
{
  tInitData initData;  ///< ECEF/Geo/OE
  uint16    InitData;  ///< initial data availability flag
  uint16    flag_bl;   ///< blocking flag
}
tInitMes;

/// Additional information
typedef struct
{
  unsigned lastOpcode   : 8;   ///< opcode last message
  unsigned lastParam    : 8;   ///< param last message
  unsigned systemUpload : 16;  ///< system upload status: bit 0 - process, bit 1 - error
}
tAddInf;

typedef union
{
  NavMessage     navMes;    ///< Navigation message
  OPNavMessage   OPnavMes;  ///< OP navigation message
  tDataLOS       dataLOS;   ///< Line of Sight Vectors message
  tMesAlmanac    mesAlm;    ///< Almanac data message
  tMesEphemeris  mesEph;    ///< Ephemeris data message
  RAW_ION        mesIon;    ///< Ionosphere data message
  RAW_UTC        mesUtc;    ///< UTC-GPS parameters message
  tAddInf        addInf;    ///< Additional information
}
tBinStrs;

typedef struct
{
  unsigned BIT      : 1;  ///< Request Built-in Test
  unsigned RESET    : 1;  ///< Software Reset
  unsigned CLEAR    : 1;  ///< Clear ephemeris and almanac in RAM
  unsigned FL_CLEAR : 1;  ///< Clear ephemeris and almanac in FLASH
}
tFlagsControl;

/// temporary data buffers
typedef struct
{
  tNavStatusData     navData;       ///< Navigation and status
  tOPNavStatusData   navDataOP;     ///< OP Navigation and status
  tTimeChStatus      timeChStatus;  ///< Time and channels status data
  tChRM              chRM;          ///< Channels Range Measurements message
  tDataLOSMess       messLOS;       ///< Line of Sight Vectors message
  tBinStrs           BinData;       ///< binary message
}
tICDdata;

/// PLL/DLL coefficients
typedef struct
{
  int8  update_pll;   ///< update PLL time
  int8  update_dll;   ///< update DLL time
  int16 alpha_val;    ///< fractional alpha coefficient
  int16 alpha_shr;    ///< Q value alpha
  int16 beta_val;     ///< fractional beta coefficient
  int16 beta_shr;     ///< Q value beta
  int16 gamma2_val;   ///< fractional gamma2 coefficient
  int16 gamma2_shr;   ///< Q value gamma2
  int16 k_end_val;    ///< fractional ending coefficient for DLL
  int16 k_end_shr;    ///< Q value ending coefficient
  int16 k_begin_val;  ///< fractional beginning coefficient for DLL
  int16 k_begin_shr;  ///< Q value beginning coefficient
  int32 k_dop_val;    ///< fractional coefficient for converts Doppler to DLL aiding
  int16 k_dop_shr;    ///< Q value coefficient
}
tCoefPLLDLL;

/// FLASH status
#define FREE_FLASH    (0)  ///< not write-in flash
#define BUSY_FLASH    (1)  ///< write-in flash in process
/// object FLASH
#define NO_OBJECT     (0)
#define CONFIG_FLASH  (1)  ///< write configuration setup to FLASH
#define SYSTEM_FLASH  (2)  ///< write system file to FLASH
#define WN_FLASH      (3)  ///< write week data to FLASH
#define EPH_FLASH     (4)  ///< write ephemeris to FLASH
#define ALM_FLASH     (5)  ///< write almanac to FLASH
#define CLEAR_FLASH   (6)  ///< clear ephemeris and almanac to FLASH
/// FLASH define
#define PAGE_SIZE     (0x80)  ///< FLASH page size

typedef struct 
{
  int8 current_status;
  int8 current_object;
}
tFlashState;

#define CHECK_SUM_SYSTEM (4)  ///< bytes number for check sum for System files 
#define CHECK_SUM        (2)  ///< check sum for configuration data and ephemeris and almanac in FLASH
#define CHECK_SUM_WN     (1)  ///< check sum for week data in FLASH

///< structure for configuration setup write-in Flash 
typedef struct
{
  eFlag  SaveData;                             ///< fg_ON if data is keep
  eFlag  StartProgramming;                     ///< start programming
  uint8  m_Data[(LENGTH_CONFIG + CHECK_SUM)];  ///< buffer for data
}
tControlConfig;

#define LENGTH_WN (3)  ///< week number and overflow week number counter

///< structure for week data write-in Flash
typedef struct
{
  eFlag  SaveData;                            ///< fg_ON if data is keep
  eFlag  StartProgramming;                    ///< start programming
  uint8  m_Data[(LENGTH_WN + CHECK_SUM_WN)];  ///< buffer for data
}
tControlWN;

///< structure for data write-in Flash: System file
typedef struct
{
  eFlag   SaveData;             ///< fg_ON if data is keep
  eFlag   EndData;              ///< fg_ON if end data
  uint8   Error;                ///< 1 if error writing process
  uint8   Process;              ///< 1 if writing process 
  eFlag   ErrorFlash;           ///< fg_ON if error flash
  uint16  numOfBlocks;          ///< number blocks written in flash
  uint8   length;               ///< length of block
  uint8   m_Data[LENGTH_FILE];  ///< buffer for data
  uint32  checkSum;             ///< check sum for all file
  uint32  totalLength;          ///< total length of file
  uint16  blocksNumber;         ///< file blocks number for download file
  uint16  currentBlock;         ///< current file block for download file
}
tControlFile;

#define DELTA_IODE  (13)  ///< bytes number from beginning ephemeris to iode
#define DELTA_WN     (1)  ///< bytes number from beginning ephemeris/almanac to wn
#define DELTA_TOA    (3)  ///< bytes number from beginning almanac to toa

///< structure for ephemeris write-in FLASH
typedef struct
{
  eFlag  SaveData;                              ///< fg_ON if data is keep
  uint8  svID;                                  ///< current satellite number 0..31
  uint8  m_Data[(SIZE_EPHEMERIS + CHECK_SUM)];  ///< buffer for data
}
tControlEph;

///< structure for almanac write-in FLASH
typedef struct
{
  eFlag  SaveData;                            ///< fg_ON if data is keep
  uint8  svID;                                ///< current satellite number 0..31
  uint8  m_Data[(SIZE_ALMANAC + CHECK_SUM)];  ///< buffer for data
}
tControlAlm;

//#define EPH_BLOCK_SIZE (2112)  ///< ephemeris block size = (SIZE_EPHEMERIS + CHECK_SUM)*GPS_SATELLITE_COUNT
//#define ALM_BLOCK_SIZE (1216)  ///< almanac   block size = (SIZE_ALMANAC   + CHECK_SUM)*GPS_SATELLITE_COUNT
//#define EPH_ALM_SIZE   (3328)  ///< ephemeris and almanac blocks size = (EPH_BLOCK_SIZE + ALM_BLOCK_SIZE)
#define EPH_ALM_PAGE   (26)    ///< EPH_ALM_SIZE by page of FLASH = (EPH_ALM_SIZE/PAGE_SIZE)

///< structure for clearing ephemeris and almanac in FLASH
typedef struct
{
  eFlag  SaveData;           ///< fg_ON if data is keep
  uint8  pagesNumber;        ///< ephemeris and almanac data FLASH pages number
  uint8  m_Data[PAGE_SIZE];  ///< buffer for data
}
tControlClr;

typedef struct
{
  tControlFile    controlFile;    ///< for file System
  tControlEph     controlEph;     ///< for ephemeris
  tControlAlm     controlAlm;     ///< for almanac
  tControlClr     controlClr;     ///< for clearing
  tControlConfig  controlConfig;  ///< for configuration
  tControlWN      controlWN;      ///< for week number
}
tFlashData;

// Alex, 20140619, NEW TMTC ICD
#ifdef NEW_TMTC

	#define START_OF_FRAME_0			'N'
	#define START_OF_FRAME_1			'S'
	#define START_OF_FRAME_2			'P'
	#define START_OF_FRAME_3			'O'
	
	#define TMTC_FRAME_OVERHEAD_LENGTH	7
	
	#define TC_GPSR_ICD_MODE_TYPE_ID		0x00
	#define TC_GPSR_ICD_MODE_DATA_LENGTH		1
	#define TC_GPSR_ICD_MODE_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_ICD_MODE_DATA_LENGTH)

	#define TC_GPSR_CONFIG_1_TYPE_ID		0x01
	#define TC_GPSR_CONFIG_1_DATA_LENGTH	6
	#define TC_GPSR_CONFIG_1_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_CONFIG_1_DATA_LENGTH)

	#define TC_GPSR_CONFIG_2_TYPE_ID		0x06
	#define TC_GPSR_CONFIG_2_DATA_LENGTH	4
	#define TC_GPSR_CONFIG_2_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_CONFIG_2_DATA_LENGTH)

	#define TC_GPSR_CONFIG_3_TYPE_ID		0x07
	#define TC_GPSR_CONFIG_3_DATA_LENGTH	6
	#define TC_GPSR_CONFIG_3_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_CONFIG_3_DATA_LENGTH)
	
	#define TC_WRITE_RFFE_REG_TYPE_ID		0x02
	#define TC_WRITE_RFFE_REG_DATA_LENGTH	4
	#define TC_WRITE_RFFE_REG_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_WRITE_RFFE_REG_DATA_LENGTH)

	#define TC_GPSR_SOFT_RESET_TYPE_ID		0x03
	#define TC_GPSR_SOFT_RESET_DATA_LENGTH		1
	#define TC_GPSR_SOFT_RESET_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_SOFT_RESET_DATA_LENGTH)

	#define TC_GPSR_BOOT_CONFIG_TYPE_ID		0x04
	#define TC_GPSR_BOOT_CONFIG_DATA_LENGTH		1
	#define TC_GPSR_BOOT_CONFIG_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_BOOT_CONFIG_DATA_LENGTH)	
	
	#define TC_GPSR_TLM_CONFIG_TYPE_ID		0x05
	#define TC_GPSR_TLM_CONFIG_DATA_LENGTH		1
	#define TC_GPSR_TLM_CONFIG_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_TLM_CONFIG_DATA_LENGTH)	

	#define TC_GPSR_OUTPUT_MODE_SEL_ID		0x08
	#define TC_GPSR_OUTPUT_MODE_SEL_DATA_LENGTH		1
	#define TC_GPSR_OUTPUT_MODE_SEL_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TC_GPSR_OUTPUT_MODE_SEL_DATA_LENGTH)



	#define TM_GPSR_TC_ACK_TYPE_ID			0x20
	#define TM_GPSR_TC_ACK_DATA_LENGTH		2
	#define TM_GPSR_TC_ACK_FRAME_LENGTH		(TMTC_FRAME_OVERHEAD_LENGTH + TM_GPSR_TC_ACK_DATA_LENGTH)	
	
	#define TM_GPSR_NAV_TLM_TYPE_ID			0x21
	#define TM_GPSR_NAV_TLM_DATA_LENGTH		156
	#define TM_GPSR_NAV_TLM_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TM_GPSR_NAV_TLM_DATA_LENGTH)

	#define TM_GPSR_SCI_TLM_TYPE_ID			0x22
	#define TM_GPSR_SCI_TLM_DATA_LENGTH		622
	#define TM_GPSR_SCI_TLM_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TM_GPSR_SCI_TLM_DATA_LENGTH)

	#define TM_GPSR_S_NAV_TLM_TYPE_ID		0x23
	#define TM_GPSR_S_NAV_TLM_DATA_LENGTH	90
	#define TM_GPSR_S_NAV_TLM_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TM_GPSR_S_NAV_TLM_DATA_LENGTH)
	
	#define TM_GPSR_DBG_MSG_TYPE_ID			0x2F
	#define TM_GPSR_DBG_MSG_DATA_LENGTH		128		// maximum
	#define TM_GPSR_DBG_MSG_FRAME_LENGTH	(TMTC_FRAME_OVERHEAD_LENGTH + TM_GPSR_DBG_MSG_DATA_LENGTH)

//	#ifdef CAN_PORT_TMTC
		#define CAN_FRAME_TM_DATA_LENGTH	60
		#define TM_GPSR_NAV_TLM_CAN_FRAME_LENGTH	((TM_GPSR_NAV_TLM_FRAME_LENGTH+CAN_FRAME_TM_DATA_LENGTH-1)/CAN_FRAME_TM_DATA_LENGTH*CAN_FRAME_TM_DATA_LENGTH)
		#define TM_GPSR_DBG_MSG_CAN_FRAME_LENGTH	((TM_GPSR_DBG_MSG_FRAME_LENGTH+CAN_FRAME_TM_DATA_LENGTH-1)/CAN_FRAME_TM_DATA_LENGTH*CAN_FRAME_TM_DATA_LENGTH)
		#define TM_GPSR_TC_ACK_CAN_FRAME_LENGTH		((TM_GPSR_TC_ACK_FRAME_LENGTH+CAN_FRAME_TM_DATA_LENGTH-1)/CAN_FRAME_TM_DATA_LENGTH*CAN_FRAME_TM_DATA_LENGTH)
//	#endif

	// write 8-byte big-endian
	// x - address
	// y - value
	#define SET_LONG_LONG_BE(x, y)	{(*((unsigned char *)x+0)) = ( (unsigned long long)y >> 56 ) & 0xff; \
									(*((unsigned char *)x+1)) = ( (unsigned long long)y >> 48 ) & 0xff; \
									(*((unsigned char *)x+2)) = ( (unsigned long long)y >> 40 ) & 0xff; \
									(*((unsigned char *)x+3)) = ( (unsigned long long)y >> 32 ) & 0xff; \
									(*((unsigned char *)x+4)) = ( (unsigned long long)y >> 24 ) & 0xff; \
									(*((unsigned char *)x+5)) = ( (unsigned long long)y >> 16 ) & 0xff; \
									(*((unsigned char *)x+6)) = ( (unsigned long long)y >> 8 ) & 0xff; \
									(*((unsigned char *)x+7)) = ( (unsigned long long)y >> 0 ) & 0xff; }

								
	// read 4-byte big-endian
	// x - address
	#define GET_LONG_BE(x)	(((*((unsigned char *)x)) << 24) + ((*((unsigned char *)x+1)) << 16) + ((*((unsigned char *)x+2)) << 8) + (*((unsigned char *)x+3)))

	// write 4-byte big-endian
	// x - address
	// y - value
	#define SET_LONG_BE(x, y)	{(*((unsigned char *)x+0)) = ( (unsigned long)y >> 24 ) & 0xff; \
								(*((unsigned char *)x+1)) = ( (unsigned long)y >> 16 ) & 0xff; \
								(*((unsigned char *)x+2)) = ( (unsigned long)y >> 8 ) & 0xff; \
								(*((unsigned char *)x+3)) = ( (unsigned long)y >> 0 ) & 0xff; }

	// read 2-byte big-endian
	// x - address
	#define GET_SHORT_BE(x)	(((*((unsigned char *)x)) << 8) + ((*((unsigned char *)x+1)) << 0 ))

	// write 2-byte big-endian
	// x - address
	// y - value
	#define SET_SHORT_BE(x, y)	{(*((unsigned char *)x+0)) = ( (unsigned short)y >> 8 ) & 0xff; \
								(*((unsigned char *)x+1)) = ( (unsigned short)y >> 0 ) & 0xff; }
		

typedef struct
{
	char sof[4] ;
	uint8 attribute[2] ;
//	uint8 frame_type_id ;
//	uint8 data_length ;
} New_Tmtc_Frame_Hdr ;

// GPSR_ICD_MODE TC, data length 1
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 icd_mode ;
	uint8 chksum ;
} Tc_Frame_Gpsr_Icd_Mode ;

// GPSR_CONFIG_1 TC, data length 6
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	int8 elevation_mask[2] ;
	uint8 nav_solution_period[2] ;
	uint8 raim_threshold ;
	uint8 cno_threshold_for_tracking ;
	
	uint8 chksum ;
} Tc_Frame_Gpsr_Config_1 ;

// GPSR_CONFIG_2 TC, data length 4
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 ECEF_Geo ;
	uint8 op_mode ;
	uint8 ionosphere_flag ;
	uint8 op_model ;
	
	uint8 chksum ;
} Tc_Frame_Gpsr_Config_2 ;

// GPSR_CONFIG_3 TC, data length 6
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 DLL_time_parameter[4] ;
	uint8 predetection_integrated_time ;
	uint8 troposphere_flag ;
	
	uint8 chksum ;
} Tc_Frame_Gpsr_Config_3 ;

// WRITE_RFFE_REG TC, data length 4
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint16 low_part ;
	uint16 high_part ;
	uint8 chksum ;
} Tc_Frame_Write_Rffe_Reg ;

// GPSR_SOFT_RESET TC, data length 1
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 reset_flag ;
	uint8 chksum ;
} Tc_Frame_Gpsr_Soft_Reset ;

// GPSR_BOOT_CONFIG TC, data length 1
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 boot_config ;
	uint8 chksum ;
} Tc_Frame_Gpsr_Boot_Config ;

// GPSR_TLM_CONFIG TC, data length 1
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 tlm_config ;
	uint8 chksum ;
} Tc_Frame_Gpsr_Tlm_Config ;


// GPSR_TC_ACK TM, data length 2
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
	
	uint8 command_id ;
	uint8 status ;
	uint8 chksum ;
} Tm_Frame_Gpsr_Tc_Ack ;

typedef struct
{
	uint8	sv_id ;
	uint8	SNR ;
	uint8	status ;
} Channel_Status_Nav_Data ;

// GPSR_NAV_TLM TM, data length 156
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
// data begin
	// ECEF navigation and status data
	uint16 	gps_week_number ;
	uint32 	gps_time ;
	uint16	validity ;
	uint16 	pdop ;	
	float32	pos_x ;
	float32	pos_y ;
	float32	pos_z ;
	float32	vel_x ;
	float32	vel_y ;
	float32	vel_z ;
	int32 	oscill_offset ;
	int8 	clock_bias[8] ;
	uint32	health_of_satellites ;
	uint32	visibility_of_satellites ;
	// configuration setup message
	int16	elevation_mask ;
	uint8	op_mode_iono_tropo_flags ;
	uint8	raim_threshold ;
	uint8	cno_threshold_for_tracking ;
	// time & channel status data
	Channel_Status_Nav_Data channel[12] ;
	// Boot image config and status
	uint8	boot_status ;
	// ECEF OP navigation and status
	uint16	op_nav_status ;
	float32	op_pos_x ;
	float32	op_pos_y ;
	float32	op_pos_z ;
	float32	op_vel_x ;
	float32	op_vel_y ;
	float32	op_vel_z ;
	// GPS-UTC clock correction parameters
	uint16	delta_t_LS ;
	uint16	WN_LSF ;
	uint8	DN ;
	uint8	delta_t_LSF ;
	// tracking performance data
	uint16	max_utilization ;
	uint32	max_utilization_time ;
	uint16	tracking_utilization[UTILIZATION_ZONE /*10+1*/] ;
	uint8	TC_count ;
	uint8	TC_success_count ;
// data end	
	uint8 chksum ;
} Tm_Frame_Gpsr_Nav_Tlm ;

typedef struct
{
	uint8	sv_id ;
	uint8	SNR ;
} Channel_Status_Short_Nav_Data ;

// GPSR_S_NAV_TLM TM, data length 90
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
// data begin
	// ECEF navigation and status data
	uint16 	gps_week_number ;
	uint32 	gps_time ;
	uint16	validity ;
	uint16 	pdop ;	
	float32	pos_x ;
	float32	pos_y ;
	float32	pos_z ;
	float32	vel_x ;
	float32	vel_y ;
	float32	vel_z ;
	int32 	oscill_offset ;
	int8 	clock_bias[8] ;
	uint32	health_of_satellites ;
	uint32	visibility_of_satellites ;
	// configuration setup message
	int16	elevation_mask ;
	uint8	op_mode_iono_tropo_flags ;
	uint8	raim_threshold ;
	uint8	cno_threshold_for_tracking ;
	// time & channel status data
	Channel_Status_Short_Nav_Data channel[12] ;
	// Boot image config and status
	uint8	boot_status ;
	// GPS-UTC clock correction parameters
	uint16	delta_t_LS ;
	uint16	WN_LSF ;
	uint8	DN ;
	uint8	delta_t_LSF ;
// data end	
	uint8 chksum ;
} Tm_Frame_Gpsr_Short_Nav_Tlm ;

// GPSR_DBG_TLM TM, data length 128
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
// data begin
	char message[TM_GPSR_DBG_MSG_DATA_LENGTH] ;
// data end	
	uint8 chksum ;
} Tm_Frame_Gpsr_Debug_Tlm ;

typedef struct
{
	uint8	sv_id ;
	uint8	SNR ;
	int16	amimuth ;
	int16	elevation ;
	uint8	ps_delay[4] ;
	uint8	doppler[4] ;
	uint16	phase ;
	uint16	position ;
	uint8	time_shift[4] ;
	uint8	pos_x[4] ;
	uint8	pos_y[4] ;
	uint8	pos_z[4] ;
	uint8	vel_x[4] ;
	uint8	vel_y[4] ;
	uint8	vel_z[4] ;
} Channel_Status_Sci_Data ;


// GPSR_SCI_TLM TM, data length 622
typedef struct
{
	New_Tmtc_Frame_Hdr header ;
// data begin
	// ECEF navigation and status data
	uint16 	gps_week_number ;
	uint32 	gps_time ;
	uint16	validity ;
	uint16 	pdop ;	
	float32	pos_x ;
	float32	pos_y ;
	float32	pos_z ;
	float32	vel_x ;
	float32	vel_y ;
	float32	vel_z ;
	int32 	oscill_offset ;
	int8 	clock_bias[8] ;
	uint32	health_of_satellites ;
	uint32	visibility_of_satellites ;
	// configuration setup message
	int16	elevation_mask ;
	uint8	raim_threshold ;
	uint8	cno_threshold_for_tracking ;
	// time & channel status data
	Channel_Status_Sci_Data channel[12] ;
	//ECEF OP navigation and status
	uint16	op_nav_status ;
	// GPS-UTC clock correction parameters
	uint16	delta_t_LS ;
	
	// reserved
	// 20170809: Tom Modification
	// uint8	reserved_0[4] ;
	 int16	ms1_latch ;
	 int16	ms1000_latch ;
	
	uint8	reserved_1[4] ;

// data end	
	uint8 chksum ;
} Tm_Frame_Gpsr_Sci_Tlm ;


#endif	// end NEW_TMTC


#endif

