/*****************************************************************************
*    Copyright (c) 2002 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Enhanced square root computation:
 *                            Newton-Rafson algorithm
 *    @file                   L_sqrt_s.c
 *    @author                 A.Nazarov
 *    @version                1.0
 */
/*****************************************************************************/

#include "comtypes.h"
#include "FractOper.h"

#ifndef  __L_SQRT_S_C
#ifndef  _L_SQRT_S_H___

/// Coefficient table
const int16 L_sqrt_coef[32]=
{ 0,0,0,0,0,0,0,0,
  -32768,-30893,-29308,-27944,-26754,-25705,-24770,-23930,-23170,
  -22478,-21845,-21262,-20724,-20224,-19759,-19325,-18918,-18536,
  -18176,-17836,-17515,-17210,-16921,-16646
};
#endif
#endif

#include "Exp.h"
#include "L_sqrt_s.h"

#ifndef __L_SQRT_S_C
#define __L_SQRT_S_C

#if  PORTING_L_SQRT_S==1
  extern const int16 L_sqrt_coef[32];
  inline
#endif

//=========================================================================
/// @brief      Computes square root of Q15 number
///             iterate 4 times: max error 4.e-5.
///             Precision conversion: Q result = Q((x-1)/2 + 8), when Qx is odd
///
/// @param[in]  x - int16 input
/// @return     int16 square root of input
//=========================================================================
int16 L_sqrt_s(int16 x)
{
  int16 coef, exp;
  int32 y;

  /// check parameters
  if (x <= 0)  return 0;

  /// move x to [0.5 .. 1.) range
  exp = (Exp0(x)-16)>>1;

  x = (int16) L_shl(x, exp);
  x = (int16) L_shl(x, exp);

  /// interpolate coefficient by using lookup table
  coef = L_sqrt_coef[x >> 10];

  /// first approximation y=0.5(x+1)
  y = L_deposit_h(((x + 0x8000)>>1) & 0x7FFF);

  /// iterate 4 times : y = y+coef*(y*y-x)
  y = L_add(y, L_mult(extract_h(L_sub(L_mult(extract_h(y),extract_h(y)),L_deposit_h(x))), coef));
  y = L_add(y, L_mult(extract_h(L_sub(L_mult(extract_h(y),extract_h(y)),L_deposit_h(x))), coef));
  y = L_add(y, L_mult(extract_h(L_sub(L_mult(extract_h(y),extract_h(y)),L_deposit_h(x))), coef));
  y = L_add(y, L_mult(extract_h(L_sub(L_mult(extract_h(y),extract_h(y)),L_deposit_h(x))), coef));

  /// returns shifted result
  return extract_h(y >> exp);
}

#endif //__L_SQRT_S_C






