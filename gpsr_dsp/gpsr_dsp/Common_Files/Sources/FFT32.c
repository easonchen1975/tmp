/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Short FFT with scaling (typical MSE~3.5e-4)
 *    @file                   FFT32.c
 *    @author                 A. Nazarov
 *    @date                   25.07.2005
 *    @version                1.0
 */
/*****************************************************************************/

#include "baseop.h"
#include "FFT32.h"

typedef union
{
  int16 sData[64];
  int32 lData[32];
}
tTwd32;

static const tTwd32 Twd32 = 
{ 32767,0,32767,0,32767,0,32137,6392,30272,12539,27244,18204,30272,12539,23169,23169,
  12539,30272,27244,18204,12539,30272,-6392,32137,23169,23169,0,32767,-23169,23169,18204,27244,
  -12539,30272,-32137,6392,12539,30272,-23169,23169,-30272,-12539,6392,32137,-30272,12539,-18204,-27244,
  32767,0,32767,0,32767,0,23169,23169,0,32767,-23169,23169, // FOR SECOND STAGE !
  0,0,0,0}; // NOT USED !!!

//==================================================================================
/// Special form of FFT of length 32. 
/// Bit-exactly converted from reference code from fft16x16c.c
///
/// @param[in]  ptr_x       - array of 32 complex samples.
///                           It is changed during execution of this routine
/// @param[in]  ptr_y       - output array of spectrum components
/// @param[in]  pTempData31 - temporary data for FFT
//==================================================================================
void fft32(tComplex16Array_32 *ptr_x,
           tComplex16Array_32 *ptr_y,
           tComplex16Array_32 *pfft32_tmp)
{
  int  i, j, k;

  {
    const int32 * restrict w; 
    const int32* restrict px = &ptr_x->iData->dummy;
    tComplex16Aligned * restrict ptr_x0; 
    w = Twd32.lData;
    ptr_x0 = pfft32_tmp->iData;

    #ifdef  _TMS320C6X
      #pragma UNROLL(2)
    #endif
    for (j = i = 0; i < 32; i += 4)
    {
      int32 zh01,zl01;
      int32  zh201,zl201;
      int32  zt0, zt1, zt2;
      int32 exp1,exp2,exp3;
      int32 z_0,z_h2,z_l1,z_l2; 
      tComplex16Aligned  ztmp1,ztmp2;

      exp1 = w[j]; exp2 = w[j+1]; exp3 = w[j+2];

      z_0    = px[ 0];
      z_h2   = px[ 8];
      z_l1   = px[16];
      z_l2   = px[24];
      px++;

      zh01 = _ADD2(z_0, z_l1);
      zl01 = _SUB2(z_0, z_l1);

      zh201 = _ADD2(z_h2,z_l2);
      zl201 = _SUB2(z_h2,z_l2);

      ptr_x0[0].dummy = _AVG2(zh01,zh201);

      zt0 = _SUB2(zh01, zh201);

      ztmp1.dummy = _ADD2(zl01, _PACKLH2(zl201,zl201));
      ztmp2.dummy = _SUB2(zl01, _PACKLH2(zl201,zl201));
      zt1 = _PACK2(ztmp2.c.im,ztmp1.c.re);
      zt2 = _PACK2(ztmp1.c.im,ztmp2.c.re);

      ptr_x0[16  ].dummy = _PACKH2(( _DOTPN2(zt1,_PACKLH2(exp1,exp1)) + 0x8000),
                                   ( _DOTP2 (zt1, exp1) + 0x8000));

      ptr_x0[ 8  ].dummy = _PACKH2(( _DOTPN2(zt0,_PACKLH2(exp2,exp2)) + 0x8000),
                                   ( _DOTP2 (zt0, exp2) + 0x8000));

      ptr_x0[24  ].dummy = _PACKH2(( _DOTPN2(zt2,_PACKLH2(exp3,exp3)) + 0x8000),
                                   ( _DOTP2 (zt2, exp3) + 0x8000));
      ptr_x0++;
      j += 3;
    }
  }

  {
    tComplex16Aligned * restrict ptr_x0 = ptr_x->iData;
    const int32* restrict px = &pfft32_tmp->iData[0].dummy;
    int32 exp13,exp4,exp6;
    exp13 = 0x7FFF;
    exp4 = _PACK2(0x5a81, 0x5a81); 
    exp6 = _PACK2(0x5a81,0-0x5a81U); 

    for (j = i = 0; i < 32; i += 8) 
    {
      int32 zh01,zl01;
      int32  zh201,zl201;
      int32  zt0, zt1, zt2;
      int32 z_0,z_h2,z_l1,z_l2;
      tComplex16Aligned  ztmp1,ztmp2;
    
      z_0    = px[ 0];
      z_h2   = px[ 2];
      z_l1   = px[ 4];
      z_l2   = px[ 6];

      zh01 = _ADD2(z_0, z_l1);
      zl01 = _SUB2(z_0, z_l1);

      zh201 = _ADD2(z_h2,z_l2);
      zl201 = _SUB2(z_h2,z_l2);

      ptr_x0[0].dummy = _AVG2(zh01,zh201);

      zt0 = _SUB2(zh01, zh201);

      ztmp1.dummy = _ADD2(zl01, _PACKLH2(zl201,zl201));
      ztmp2.dummy = _SUB2(zl01, _PACKLH2(zl201,zl201));
      zt1 = _PACK2(ztmp2.c.im,ztmp1.c.re);
      zt2 = _PACK2(ztmp1.c.im,ztmp2.c.re);
      
      ptr_x0[4].dummy = _PACK2 (_DOTPNRSU2(zt1, _PACKLH2(exp13,exp13)),_DOTPRSU2 (zt1, exp13));
      ptr_x0[2].dummy = _PACK2 (_DOTPNRSU2(zt0, _PACKLH2(exp13,exp13)),_DOTPRSU2 (zt0, exp13));
      ptr_x0[6].dummy = _PACK2 (_DOTPNRSU2(zt2, _PACKLH2(exp13,exp13)),_DOTPRSU2 (zt2, exp13));

      px++;
      ptr_x0++;

      z_0    = px[ 0];
      z_h2   = px[ 2];
      z_l1   = px[ 4];
      z_l2   = px[ 6];

      zh01 = _ADD2(z_0, z_l1);
      zl01 = _SUB2(z_0, z_l1);

      zh201 = _ADD2(z_h2,z_l2);
      zl201 = _SUB2(z_h2,z_l2);

      ptr_x0[0].dummy = _AVG2(zh01,zh201);

      zt0 = _SUB2(zh01, zh201);

      ztmp1.dummy = _ADD2(zl01, _PACKLH2(zl201,zl201));
      ztmp2.dummy = _SUB2(zl01, _PACKLH2(zl201,zl201));
      zt1 = _PACK2(ztmp2.c.im,ztmp1.c.re);
      zt2 = _PACK2(ztmp1.c.im,ztmp2.c.re);

      ptr_x0[6].dummy = _PACKH2((_DOTPN2(zt2, _PACKLH2( exp6, exp6)) + 0x8000),
                                (_DOTP2 (zt2, exp6)  + 0x8000));
      ptr_x0[4].dummy = _PACK2 (_DOTPNRSU2(zt1, exp4),_DOTPRSU2 (zt1, exp4));
      ptr_x0[2].dummy = _PACK2 (_DOTPNRSU2(zt0, exp13),_DOTPRSU2 (zt0, _PACKLH2(exp13,exp13)));

      px += 7;
      ptr_x0+=7;
    }
  } 
  
  
  {
    tDoubleWord  * restrict ptr_dx0;
    tDoubleWord  * restrict dy0;
    ptr_dx0 = ptr_x->dData;
    dy0 = ptr_y->dData;

    for (j = 0, i = 0; i < 16; i += 4)
    {
      int32  z01,z23,z45,z67;
      int32  z01_8,z23_8,z45_8,z67_8;
      k = ((uint32)_BITR(j))>>30;
      j++;
    
      z01  = _LO(ptr_dx0[0]);
      z23  = _HI(ptr_dx0[0]);
      z45  = _LO(ptr_dx0[1]);
      z67  = _HI(ptr_dx0[1]);
      z01_8  = _LO(ptr_dx0[0+8]);
      z23_8  = _HI(ptr_dx0[0+8]);
      z45_8  = _LO(ptr_dx0[1+8]);
      z67_8  = _HI(ptr_dx0[1+8]);

      dy0[k]    = _ITOD(_ADD2(z01_8,z23_8),_ADD2(z01,z23));
      dy0[k+ 4] = _ITOD(_ADD2(z45_8,z67_8),_ADD2(z45,z67));
      dy0[k+ 8] = _ITOD(_SUB2(z01_8,z23_8),_SUB2(z01,z23));
      dy0[k+12] = _ITOD(_SUB2(z45_8,z67_8),_SUB2(z45,z67));
      ptr_dx0+=2;
    }
  }
}
