/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Inline definitions
 *   @file                   baseop.h
 *   @author                 M. Zhokhova
 *   @date                   04.12.2006
 *   @version                3.0
 */
/*****************************************************************************/
#ifndef __BASEOP_H
#define __BASEOP_H

#include "comtypes.h"
#include "FractOper.h"
#include <stdlib.h>

#ifndef _TMS320C5XX

/***********************************************************************************

  Inline definitions of fixed point operations bit-exact with 'C54 intrinsics

***********************************************************************************/

/****************************************************************************
  Description: _memcpy() substitute
****************************************************************************/
inline_ void _memcpy(void* restrict voidpDst, const void* restrict voidpSrc, size_t size)
{
  size_t i;
  const char* restrict pSrc;
  char* restrict pDst;
  pSrc = (const char*)voidpSrc;
  pDst = (char*)voidpDst;
  for (i=0; i<size;i++)
  {
    pDst[i] = pSrc[i];
  }
}

/****************************************************************************
  Description: _memcpy_cs() substitute and check sum calculation
****************************************************************************/
inline_ void _memcpy_cs(void* restrict voidpDst, const void* restrict voidpSrc, int size)
{
  int i;
  unsigned char tmp = 0;
  const char* restrict pSrc;
  char* restrict pDst;
  pSrc = (const char*)voidpSrc;
  pDst = (char*)voidpDst;
  for (i=0; i<size;i++)
  {
    tmp += pDst[i] = pSrc[i];
  }
  pDst[size] = (0xff&tmp);
}

/****************************************************************************
  Description: _memcpy_cs() substitute and 16-bit check sum calculation
****************************************************************************/
inline_ void _memcpy_cs16(void* restrict voidpDst, const void* restrict voidpSrc, int size)
{
  int i;
  uint16 tmp = 0;
  const unsigned char* restrict pSrc;
  unsigned char* restrict pDst;
  pSrc = (const unsigned char*)voidpSrc;
  pDst = (unsigned char*)voidpDst;
  for (i=0; i<size;i++)
  {
    tmp += pDst[i] = pSrc[i];
  }
  pDst[size] = (char)(tmp&0xff);
  pDst[size+1] = (char)(tmp>>8);
}

/****************************************************************************
  Description: _memcpy_cs32() substitute and check sum calculation
****************************************************************************/
inline_ void _memcpy_cs32(void* restrict voidpDst, const void* restrict voidpSrc, int size, uint32 *check_sum)
{
  int    i;
  uint32 tmp = *check_sum;
  const char* restrict pSrc;
  char* restrict pDst;
  pSrc = (const char*)voidpSrc;
  pDst = (char*)voidpDst;

  for (i=0; i<size;i++)
  {
    pDst[i] = pSrc[i];
    tmp    += (unsigned char)pSrc[i];
  }
  *check_sum = tmp;
}

/***********************************************************************************
  Multiplies two fractional Q15 numbers and saturates result in Q31 format
  (0x8000 * 0x8000 = 0x7fffffff)
  The src1 operand is multiplied by the src2 operand. The result is left shifted
  by 1 and placed in dst. If the left-shifted result is 0x8000 0000, then the result
  is saturated to 0x7FFF FFFF. If a saturate occurs, the SAT bit in the CSR is set
  one cycle after dst is written.
***********************************************************************************/
#ifdef _TMS320C6X
  #define L_mult(x,y) (_smpy(x,y))
#else
inline_ int32 L_mult(int16 var1, int16 var2)
{
  int32 L_var_out;
  
  L_var_out = (int32)var1 * (int32)var2;
  
  if ((var1 != (int16)0x8000) || (var2 != (int16)0x8000))
    return (L_var_out << 1);
  return (0x7fffffffL);
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16lsb x 16lsb: signed x signed = signed
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used
***********************************************************************************/
#ifdef _TMS320C6X
  #define mpy(x,y) (_mpy(x,y))
#else
inline_ int32 mpy(int16 var1, int16 var2)
{
  return (int32)var1 * (int32)var2;
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16lsb x 16lsb: unsigned x unsigned = unsigned
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYU(src1, src2)   _mpyu(src1,src2)
#else
inline_ uint32 _MPYU(uint32 src1, int32 src2)
{
  return (src1&0xffff) * ((uint32)src2&0xffff);
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16lsb x 16msb: unsigned x signed = signed
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYLUHS(src1, src2)   _mpyluhs(src1,src2)
#else
inline_ int32 _MPYLUHS(uint32 src1, int32 src2)
{
  return (src1&0xffff) * (src2>>16);
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16msb x 16lsb: unsigned x unsigned = unsigned
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYHLU(src1, src2)   _mpyhlu(src1,src2)
#else
inline_ uint32 _MPYHLU(uint32 src1, int32 src2)
{
  return ((src1>>16)&0xffff) * (uint32)(src2&0xffff);
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16msb x 16msb: unsigned x signed = signed
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYHUS(src1, src2)   _mpyhus(src1,src2)
#else
inline_ int32 _MPYHUS(uint32 src1, int32 src2)
{
  return ((src1>>16)&0xffff) * (src2>>16);
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16msb x 16msb: unsigned x unsigned = unsigned
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYHU(src1, src2)   _mpyhu(src1,src2)
#else
inline_ uint32 _MPYHU(uint32 src1, uint32 src2)
{
  return ((src1>>16)&0xffff) * ((src2>>16)&0xffff);
}
#endif

/***********************************************************************************
  Signed or unsigned integer multiply 16lsb x 16msb: unsigned x unsigned = unsigned
  The src1 operand is multiplied by the src2 operand. The result is placed in dst.
  The source operands are signed by default. The S is needed in the mnemonic
  to specify a signed operand when both signed and unsigned operands are used.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYLHU(src1, src2)   _mpylhu(src1,src2)
#else
inline_ int32 _MPYLHU(uint32 src1, uint32 src2)
{
  return (src1&0xffff) * ((src2>>16)&0xffff);
}
#endif

/***********************************************************************************
  Integer substraction with saturation to result size
***********************************************************************************/
#ifdef _TMS320C6X
 #define L_sub(x,y)  (_ssub(x,y))
#else
inline_ int32 L_sub(int32 L_var1, int32 L_var2)
{
  int32 L_var_out;

  L_var_out = L_var1 - L_var2;

  if (((L_var1 ^ L_var2) & 0x80000000L) 
    && ((L_var_out ^ L_var1) & 0x80000000L))
      L_var_out = (L_var1 < 0L) ? 0x80000000L : 0x7fffffffL;
  return (L_var_out);
}      
#endif

/***********************************************************************************
  Integer addition with saturation to result size
***********************************************************************************/
#ifdef _TMS320C6X
 #define L_add(x,y)  (_sadd(x,y))
#else
inline_ int32 L_add(int32 L_var1, int32 L_var2)
{
  int32 L_var_out;

  L_var_out = L_var1 + L_var2;

  if ((((L_var1 ^ L_var2) & 0x80000000L) == 0L) 
    && ((L_var_out ^ L_var1) & 0x80000000L))
      L_var_out = (L_var1 < 0L) ? 0x80000000L : 0x7fffffffL;
  return (L_var_out);
}
#endif

/***********************************************************************************
  Rounds 32-bit value by adding 0x00008000 and saturating result
***********************************************************************************/
#ifdef _TMS320C6X
  #define round(x)    ((int16)(_sadd(x,0x00008000L)>>16)) 
#else
inline_ int16 round(int32 L_var1)
{
  L_var1 = L_add(L_var1, (uint32)0x8000);
  return ((int16) (L_var1 >> 16));
}
#endif

/***********************************************************************************
  Shifts and saturates 32-bit number by specified shift value
***********************************************************************************/
#ifdef _TMS320C6X
  #define L_shl(x,y)   (_sshvl(x,y) )
#else
inline_ int32 L_shl(int32 Value, int16 Shift)
{
  int16 HVal;
 
  if (Shift > 0)
  {
    HVal = (int16)(Value >> (31 - Shift));
    if ((HVal == 0) || (HVal == -1))
      Value <<= Shift;
    else
      Value = (HVal > 0) ? 0x7FFFFFFFL : 0x80000000L;
  }
  else
    Value >>= -Shift;

  return Value;
}             
#endif

/***********************************************************************************
  The L_mpy_ls() performs a 32-bit by 16-bit multiply.
  The var1 is a 16-bit signed input. The L_var2 is 32-bit signed input.
  The product is then rounded into a 32-bit result by adding the value 2^14 and
  then this sum is right shifted by 15. Function is written to be bit exact 
  with C64 MPYHIR/MPYLIR instructions.
***********************************************************************************/
#ifdef _TMS320C6400
  #define L_mpy_ls(L_var2, var1)   (_mpylir(var1, L_var2))
#else
  inline_ int32 L_mpy_ls(int32 L_var2, int16 var1)
  {
    int16  Hi;
    uint16 Lo;
    int32  Y1, Y2;

    Hi =  (L_var2 >>16) & 0xFFFF;
    Lo =  L_var2 & 0xFFFF;

    Y1 =  ((int32)Hi * var1) << 1;
    Y2 =  ((int32)Lo * var1 + 0x4000) >> 15;
    return Y1 + Y2;
  }
#endif

/***********************************************************************************
  Multiply 16lsb x 32-bit to 64-bit result.
  The MPYLI instruction performs a 16-bit by 32-bit multiply. The lower half of
  src1 is used as a 16 bit signed input. The value in src2 is treated as a 32-bit
  signed value. The result is written into the lower 48 bits of a 64-bit register pair,
  dst_o:dst_e, and sign extended to 64 bits.
***********************************************************************************/
#ifdef _TMS320C6400
  #define L_mpy_li(var1, L_var2)   (_mpyli(var1, L_var2))
#else
  inline_ int64 L_mpy_li(int32 arg1, int32 arg2)
  {
    tLongFractDouble res;
    int64 result = ((int64)(arg1 & 0xFFFF)) * ((int64)arg2);
    res.lf.lo = (uint32)(result & 0xFFFFFFFF);
    res.lf.hi = (int32)(result >> 32);
    return res.dbl;
  }
#endif

/***********************************************************************************
  Multiply 16msb x 32-bit to 64-bit result.
  The MPYHI instruction performs a 16-bit by 32-bit multiply. The upper half of
  src1 is used as a 16-bit signed input. The value in src2 is treated as a 32-bit
  signed value. The result is written into the lower 48 bits of a 64-bit register pair,
  dst_o:dst_e, and sign extended to 64 bits.
***********************************************************************************/
#ifdef _TMS320C6400
  #define L_mpy_hi(var1, L_var2)   (_mpyhi(var1, L_var2))
#else
  inline_ int64 L_mpy_hi(int32 arg1, int32 arg2)
  {
    tLongFractDouble res;
    int64 result = (((int64)arg1) >> 16) * ((int64)arg2);
    res.lf.lo = (uint32)(result & 0xFFFFFFFF);
    res.lf.hi = (int32)(result >> 32);
    return res.dbl;
  }
#endif


/***********************************************************************************
  Limit the 32 bit input to the range of a 16 bit.
  L_var1 - 32 bit long signed integer (Word32) whose value falls in the range: 
  0x8000 0000 <= L_var1 <= 0x7fff ffff. Return 16 bit short signed integer (Word16) 
  whose value falls in the range : 0xffff 8000 <= Return Value <= 0x0000 7fff
***********************************************************************************/
#ifdef _TMS320C6X
  #define sature(L_var1)   (_sshl(L_var1, 16)>>16)
#else
  inline_ int16 sature(int32 L_var1)
  {
    int16 var_out;

    if (L_var1 > 0x00007fffL)  
    {
      var_out = 0x7fff;
    }
    else
    {
      if (L_var1 < (int32)0xffff8000L)
      {
        var_out = (int16)0x8000;
      }
      else
      {
        var_out=(int16)L_var1;
      }
    }
    return(var_out);
  }
#endif

/***********************************************************************************
  Generates no code. Tells the optimizer that the expression declared 
  with the assert function is true. This gives a hint to the compiler 
  as to what optimizations might be valid (using wordide 
  optimizations).
***********************************************************************************/
#ifdef _TMS320C6X
#define _NASSERT(x) _nassert(x)
#else
#define _NASSERT(x) ASSERT(x)
#endif

/***********************************************************************************
  The lower/upper half-words of src1 and src2 are placed in 
  the return value
***********************************************************************************/
#ifdef _TMS320C6X
#define _PACKH2(x,y) _packh2(x,y)
#define _PACK2(x,y)  _pack2(x,y)
#define _PACKLH2(x,y) _packlh2(x,y)
#define _PACKHL2(x,y) _packhl2(x,y)
#else
inline_ uint32 _PACKH2(uint32 x,uint32 y)
{
    return (x & 0xFFFF0000UL) | (y>>16);
}

inline_ uint32 _PACK2(uint32 x,uint32 y)
{
    return (x<<16) | (y&0x0000FFFFUL);
}

inline_ uint32 _PACKLH2(uint32 x,uint32 y)
{
    return (x<<16) | (y>>16);
}

inline_ uint32 _PACKHL2(uint32 x,uint32 y)
{
    return (x>>16) | (y<<16);
}
#endif

/***********************************************************************************
  The SPACK2 instruction takes two signed 32-bit quantities in src1 and src2
  and saturates them to signed 16-bit quantities. The signed 16-bit results 
  are then packed into a signed, packed 16-bit format and written to dst. 
  Specifically, the saturated 16-bit signed value of src1 is written to the 
  upper half word of dst, and the saturated 16-bit signed value of src2 is 
  written to the lower half word of dst. 
***********************************************************************************/
#ifdef _TMS320C6X
  #define _SPACK2(hi,lo)   _spack2(hi,lo)
#else

inline_ int32 _SPACK2(int32 val_hi, int32  val_lo)
{
  return _PACK2(sature(val_hi), sature(val_lo));
}
#endif

/***********************************************************************************
  Generates no code. Tells the optimizer that the expression declared 
  with the assert function is true. This gives a hint to the compiler 
  as to what optimizations might be valid (using wordide 
  optimizations).
***********************************************************************************/
#ifdef _TMS320C6X
#else
#define restrict 
#endif

/***********************************************************************************
  Conditional Integer Subtract and Shift �Used for Division C64's _SUBC() 
  instruction "C" equivalent. NOTE:  This SUBC instruction differs from C54's SUBC!!!
  Subtract src2 from src1. If result is greater than or equal to 0, left shift result
  by 1, add 1 to it, and place it in dst. If result is less than 0, left shift scr1 by 1,
  and place it in dst. This step is commonly used in division.
***********************************************************************************/
#ifdef _TMS320C6X
  #define L_subc(x,y)  _subc(x,y)
#else
inline_  uint32 L_subc(uint32 UNum, uint32 UDen)
  {
    int32  Temp;

    Temp = UNum - UDen;
    if (Temp >= 0)
       UNum = (Temp << 1) + 1;
    else
       UNum = UNum << 1;
    return UNum;
  }
#endif

/***********************************************************************************
  The DOTP2 instruction returns the dot-product between two pairs of signed,
  packed 16-bit values. The values in src1 and src2 are treated as signed,
  packed 16-bit quantities. The signed result is written either to a single 32-bit
  register, or sign-extended into a 64-bit register pair.
  The product of the lower half-words of src1 and src2 is added to the product
  of the upper half-words of src1 and src2. The result is then written to the dst.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _DOTP2(x,y)   _dotp2(x,y)
#else

inline_  int32  _DOTP2(int32 val1, int32  val2)
{

  return (val1>>16)*(val2>>16) + ((val1<<16)>>16)*((val2<<16)>>16);
}
#endif

/***********************************************************************************
  The DOTPRSU2 instruction returns the dot-product between two pairs of packed 
  16-bit values. This instruction takes the result of the dot-product and performs 
  an additional round and shift step. The values in src1 are treated as signed 
  packed 16-bit quantities, whereas the values in src2 are treated as unsigned 
  packed 16-bit quantities. The results are written to dst. 
  The product of the lower half-words of src1 and src2 is added to the product 
  of the upper half-words of src1 and src2. The value 2^15 is then added to this 
  sum, producing an intermediate 32-bit result. The intermediate result is signed 
  shifted right by 16, producing a rounded, shifted result that is sign extended 
  and placed in dst.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _DOTPRSU2(x,y)   _dotprsu2(x,y)
#else

inline_  int32  _DOTPRSU2(int32 src1, uint32  src2)
{

  return L_shl(((src1>>16)*(src2>>16) + ((src1<<16)>>16)*((src2<<16)>>16) + 0x8000U),-16);
}
#endif

/***********************************************************************************
  The DOTPNRUS2 pseudo-operation performs the dot-product between two pairs of 
  packed 16-bit values, where the second product is negated. This instruction 
  takes the result of the dot-product and performs an additional round and shift 
  step. The values in src1 are treated as signed, packed 16-bit quantities, 
  whereas the values in src2 are treated as unsigned, packed 16-bit quantities. 
  The results are written to dst. The assembler uses the DOTPNRSU2 src1, src2, 
  dst instruction to perform this task.
  The product of the lower half-words of src1 and src2 is subtracted from the 
  product of the upper half-words of src1 and src2. The value 2^15 is then 
  added to this sum, producing an intermediate 32-bit result. The intermediate 
  result is signed shifted right by 16, producing a rounded, shifted result 
  that is sign extended and placed in dst.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _DOTPNRSU2(x,y)   _dotpnrsu2(x,y)
#else

inline_  int32  _DOTPNRSU2(int32 src1, uint32  src2)
{

  return L_shl(((src1>>16)*(src2>>16) - ((src1<<16)>>16)*((src2<<16)>>16) + 0x8000U),-16);
}
#endif

/***********************************************************************************
  The DOTPSU4 instruction returns the dot-product between four sets of packed 
  8-bit values. The values in src1 are treated as signed packed 8-bit quantities, 
  whereas the values in src2 are treated as unsigned 8-bit packed data. 
  The signed result is written into dst. For each pair of 8-bit quantities in 
  src1 and src2, the signed 8-bit value from src1 is multiplied with the 
  unsigned 8-bit value from src2. The four products are summed together, and 
  the resulting dot product is written as a signed 32-bit result to dst. 
***********************************************************************************/
#ifdef _TMS320C6X
  #define   _DOTPSU4(src1, src2)     _dotpsu4(src1, src2)
#else

inline_ int32 _DOTPSU4(uint32 src1, uint32 src2)
{

  int32 result = 0;
  int i;

  for (i = 0; i<4; i++)
  {
    result += ((int8)(src1 & 0xFF)) *  (src2 & 0xFF);
    src1 >>= 8;
    src2 >>= 8;
  }
  return result;
}
#endif

/***********************************************************************************
  The DOTPN2 instruction returns the dot-product between two pairs of signed, 
  packed 16-bit values where the second product is negated. The values in 
  src1 and src2 are treated as signed, packed 16-bit quantities. The signed 
  result is written to a single 32-bit register.
  The product of the lower half-words of src1 and src2 is subtracted from 
  the product of the upper half-words of src1 and src2. The result is then 
  written to dst. 
***********************************************************************************/
#ifdef _TMS320C6X
  #define _DOTPN2(x,y)   _dotpn2(x,y)
#else

inline_  int32  _DOTPN2(int32 val1, int32  val2)
{

  return (val1>>16)*(val2>>16) - ((val1<<16)>>16)*((val2<<16)>>16);
}
#endif

/***********************************************************************************
  The AVG2 instruction performs an averaging operation on packed 16-bit data. 
  For each pair of signed 16-bit values found in src1 and src2, AVG2 
  calculates the average of the two values and returns a signed 16-bit 
  quantity in the corresponding position in the dst.
***********************************************************************************/

#ifdef _TMS320C6X
  #define _AVG2(x,y)   _avg2(x,y)
#else

inline_  int32  _AVG2(int32 val1, int32  val2)
{
  int16 hi = (int16)((((int32)extract_h(val1)) + extract_h(val2) + 1)>>1);
  int16 lo = (int16)((((int32)((int16)(val1))) + ((int16)(val2)) + 1)>>1);
  return _PACK2(hi,lo);
}
#endif

/***********************************************************************************
  BITR - The BITR implements a bit-reversal. A bit-reversal function
  reverses the order of bits in a 32-bit word.
***********************************************************************************/
#ifdef _TMS320C6X
  #define   _BITR(src1)     _bitr(src1)
#else
inline_  uint32 _BITR(uint32 x)
{
  int16 i;
  uint32 y = 0;
  for (i= 0; i<32; i++)
  {
    uint16 bit = (uint16)(x & 0x1);
    x >>= 1;
  
    y = (y<<1) | bit;
  }

  return y;
}
#endif

/***********************************************************************************
  Extract and sign-extend a bit field. The field in src2, specified by csta and cstb,
  is extracted and sign-extended to 32 bits. The extract is performed by a shift 
  left followed by a signed shift right. csta and cstb are the shift left amount 
  and shift right amount, respectively.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _EXT(src, shift_left, shift_right)   _ext(src, shift_left, shift_right)
#else

inline_ int32 _EXT(int32 src, uint16 shift_left, uint16 shift_right)
{
  return (src << shift_left) >> shift_right;
}
#endif

/***********************************************************************************
  Two 16-bit integer adds on upper and lower register halves.
  The upper and lower halves of the src1 operand are added to the upper and
  lower halves of the src2 operand. Any carry from the lower half add does not
  affect the upper half add.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _ADD2(src1,src2)   _add2(src1,src2)
#else

inline_  int32  _ADD2(int32 src1, int32 src2)
{
  int32 lo1 = _EXT(src1,16,16);
  int32 lo2 = _EXT(src2,16,16);

  int32 hi1 = src1>>16;
  int32 hi2 = src2>>16;

  int16 result_lo = (int16)(lo1+lo2) & 0xFFFF;
  int16 result_hi = (int16)(hi1+hi2) & 0xFFFF;

  return _PACK2(result_hi, result_lo);
}
#endif

/***********************************************************************************
  Add four 8-bit pairs for four 8-bit results. 
  The ADD4 instruction performs 2s-complement addition between packed 8-bit
  quantities. The values in src1 and src2 are treated as packed 8-bit data and
  the results are written in packed 8-bit format.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _ADD4(src1,src2)   _add4(src1,src2)
#else
inline_  int32  _ADD4(int32 src1, int32 src2)
{
  int32 lo_lo1 = _EXT(src1,24,24);
  int32 lo_lo2 = _EXT(src2,24,24);

  int32 lo_hi1 = _EXT(src1,16,24);
  int32 lo_hi2 = _EXT(src2,16,24);

  int32 hi_lo1 = _EXT(src1,8,24);
  int32 hi_lo2 = _EXT(src2,8,24);

  int32 hi_hi1 = src1>>24;
  int32 hi_hi2 = src2>>24;

  int8 result_lo_lo = (int8)(lo_lo1+lo_lo2) & 0xFF;
  int8 result_lo_hi = (int8)(lo_hi1+lo_hi2) & 0xFF;
  int8 result_hi_lo = (int8)(hi_lo1+hi_lo2) & 0xFF;
  int8 result_hi_hi = (int8)(hi_hi1+hi_hi2) & 0xFF;
  int16 result_lo = (((int16)result_lo_hi<<8)&0xFF00 | (int16)result_lo_lo&0xFF);
  int16 result_hi = (((int16)result_hi_hi<<8)&0xFF00 | (int16)result_hi_lo&0xFF);

  return _PACK2(result_hi, result_lo);
}
#endif


/***********************************************************************************
  Two 16-bit integer subtractions on upper and lower register halves.
  In the SUB2 instruction, the upper and lower halves of src2 are subtracted from
  the upper and lower halves of src1 and the result is placed in dst. Any borrow
  from the lower-half subtraction does not affect the upper-half subtraction.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _SUB2(src1,src2)   _sub2(src1,src2)
#else

inline_  int32  _SUB2(int32 src1, int32 src2)
{
  int32 lo1 = _EXT(src1,16,16);
  int32 lo2 = _EXT(src2,16,16);

  int32 hi1 = src1>>16;
  int32 hi2 = src2>>16;

  int16 result_lo = (int16)(lo1-lo2) & 0xFFFF;
  int16 result_hi = (int16)(hi1-hi2) & 0xFFFF;

  return _PACK2(result_hi, result_lo);
}
#endif

/***********************************************************************************
  Multiply signed by signed, packed 16-bit.
  The MPY2 instruction performs two 16-bit by 16-bit multiplications between
  two pairs of signed, packed 16-bit values. The values in src1 and src2 are
  treated as signed, packed 16-bit quantities. The 32-bit results are written into
  a 64-bit register pair. The product of the lower half-words of src1 and src2 is 
  written to the even destination register, dst_e. The product of the upper 
  half-words of src1 and src2 is written to the odd destination register, dst_o.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPY2(src1, src2)   _mpy2(src1,src2)
#else
inline_ tDoubleWord _MPY2(int32 src1, int32 src2)
{
  int16  low1, low2;
  int16  hi1, hi2;
  tDoubleWord result;

  low1 = (int16)(src1 & 0xffff);
  low2 = (int16)(src2 & 0xffff);

  hi1 = (int16)(src1>>16);
  hi2 = (int16)(src2>>16);

  result.lo = low1*low2;
  result.hi = hi1*hi2;

  return result;
}
#endif

/***********************************************************************************
  Nonaligned 64bit-word reading. Analog of LDNDW instruction
***********************************************************************************/
#ifdef _TMS320C6X
  #define Get_memd8_const(pdword)   _memd8_const(pdword)
#else
inline_ tDoubleWord  Get_memd8_const(const tDoubleWord *pdword)
{
  tDoubleWord   word;
  const uint16 *phalfword = (uint16*)pdword;

  word.lo = (((uint32)phalfword[1])<<16) | phalfword[0];
  word.hi = (((uint32)phalfword[3])<<16) | phalfword[2];

  return word;
}
#endif 

/***********************************************************************************
  Unaligned 64bit-word storing. Analog of LDNDW instruction
***********************************************************************************/
#ifdef _TMS320C6X
  #define Get_memd8(Addr, Data)   Data = _memd8((void*)Addr)
#else
  #define Get_memd8(Addr, Data)   Data = Get_memd8_const(Addr)
#endif

/***********************************************************************************
  Builds a new double register pair by
  reinterpreting two unsigneds, where
  src2 is the high (odd) register and src1
  is the low (even) register
***********************************************************************************/
#ifdef _TMS320C6X
  #define _ITOD(x,y)  _itod(x,y)
#else

inline_ tDoubleWord  _ITOD(const int32 Hi, const int32 Lo)
{
    tDoubleWord word;
    word.hi = Hi;
    word.lo = Lo;
    return word;
}
#endif    

/***********************************************************************************
  Returns the high (odd) register of a
  double register pair
***********************************************************************************/
#ifdef _TMS320C6X
  #define _HI(x)  _hi(x)
#else

inline_  uint32 _HI(const tDoubleWord  dword)
{
  return (uint32)dword.hi;
}
#endif    

/***********************************************************************************
  Returns the low (even) register of a
  double register pair
***********************************************************************************/
#ifdef _TMS320C6X
  #define _LO(x)  _lo(x)
#else
  #ifndef __cplusplus
  inline_  uint32 _LO(const tDoubleWord  dword)
  {
    return (uint32)dword.lo;
  }
  #endif
#endif    

/***********************************************************************************
  Multiply signed by unsigned packed, 8 bit.
  The MPYSU4 instruction returns the product between four sets of packed 8-bit
  values producing four signed 16-bit results. The four signed 16-bit results are
  packed into a 64-bit register pair, dst_o:dst_e. The values in src1 are treated
  as signed packed 8-bit quantities, whereas the values in src2 are treated as
  unsigned 8-bit packed data.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYSU4(src1, src2)   _mpysu4(src1,src2)
#else
inline_ tDoubleWord _MPYSU4(int32 src1, uint32 src2)
{
  int8  sbyte0, sbyte1, sbyte2, sbyte3;
  uint8  ubyte0, ubyte1, ubyte2, ubyte3;
  tDoubleWord result;

  sbyte0 = (int8)(src1 & 0xff);
  sbyte1 = (int8)((src1 & 0xff00)>>8);
  sbyte2 = (int8)((src1 & 0xff0000)>>16);
  sbyte3 = (int8)((src1 & 0xff000000)>>24);

  ubyte0 = (uint8)(src2 & 0xff);
  ubyte1 = (uint8)((src2 & 0xff00)>>8);
  ubyte2 = (uint8)((src2 & 0xff0000)>>16);
  ubyte3 = (uint8)((src2 & 0xff000000)>>24);

  result.lo = ((sbyte0*ubyte0) & 0xffff) + ((sbyte1*ubyte1)<<16);
  result.hi = ((sbyte2*ubyte2) & 0xffff) + ((sbyte3*ubyte3)<<16);;

  return result;
}

#endif

/***********************************************************************************
  Multiply unsigned by unsigned packed, 8 bit.
  The MPYU4 instruction returns the product between four sets of packed 8-bit
  values producing four unsigned 16-bit results that are packed into a 64-bit register
  pair, dst_o:dst_e. The values in both src1 and src2 are treated as unsigned
  8-bit packed data.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _MPYU4(src1, src2)   _mpyu4(src1,src2)
#else
inline_ tDoubleWord _MPYU4(int32 src1, uint32 src2)
{
  uint8  sbyte0, sbyte1, sbyte2, sbyte3;
  uint8  ubyte0, ubyte1, ubyte2, ubyte3;
  tDoubleWord result;

  sbyte0 = (uint8)(src1 & 0xff);
  sbyte1 = (uint8)((src1 & 0xff00)>>8);
  sbyte2 = (uint8)((src1 & 0xff0000)>>16);
  sbyte3 = (uint8)((src1 & 0xff000000)>>24);

  ubyte0 = (uint8)(src2 & 0xff);
  ubyte1 = (uint8)((src2 & 0xff00)>>8);
  ubyte2 = (uint8)((src2 & 0xff0000)>>16);
  ubyte3 = (uint8)((src2 & 0xff000000)>>24);

  result.lo = ((sbyte0*ubyte0) & 0xffff) + ((sbyte1*ubyte1)<<16);
  result.hi = ((sbyte2*ubyte2) & 0xffff) + ((sbyte3*ubyte3)<<16);;

  return result;
}
#endif

/***********************************************************************************
  The PACKL4 instruction moves the low bytes of the two half-words in 
  src1 and src2, and packs them into dst. The bytes from src1 will 
  be packed into the most significant bytes of dst, and the bytes from 
  src2 will be packed into the least significant bytes of dst. 
  Specifically, the low byte of the upper half-word of src1 is moved to 
  the upper byte of the upper half-word of dst. The low byte of the 
  lower half-word of src1 is moved to the lower byte of the upper 
  half-word of dst. The low byte of the upper half-word of src2 is 
  moved to the upper byte of the lower half-word of dst. The low byte 
  of the lower half-word of src2 is moved to the lower byte of the 
  lower half-word of dst. 
***********************************************************************************/
#ifdef _TMS320C6X
  #define _PACKL4(src1,src2)   _packl4(src1,src2)
#else
inline_ uint32 _PACKL4(uint32 src1,uint32 src2)
{
    return ((src1 & 0x000000FF) <<16) |
           ((src1 & 0x00FF0000) << 8) |
           ((src2 & 0x000000FF)     ) |
           ((src2 & 0x00FF0000) >> 8);
}
#endif

#else // _TMS320C5XX

#if !defined(L_deposit_h) && !defined(extract_h)
#include "intrindefs.h"
#endif // defined(L_deposit_h) || defined(extract_h)

#endif // _TMS320C5XX

/***********************************************************************************
  Extract and zero-extend a bit field.
  The field in src2, specified by csta and cstb, is extracted and zero extended
  to 32 bits. The extract is performed by a shift left followed by an unsigned shift
  right. csta and cstb are the amounts to shift left and shift right, respectively.
  This can be thought of in terms of the LSB and MSB of the field to be extracted.
***********************************************************************************/
#ifdef _TMS320C6X
  #define _EXTU(src, shift_left, shift_right)   _extu(src, shift_left, shift_right)
#else

inline_ int32 _EXTU(int32 src, uint16 shift_left, uint16 shift_right)
{
  return ((uint32)src << shift_left) >> shift_right;
}
#endif

#endif // __BASEOP_H
