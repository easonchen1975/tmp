/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  PLL & DLL loops
 *   @file                   PLLDLL.h
 *   @author                 M. Zhokhova
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*****************************************************************************/

#ifndef PLLDLL_H_H__
#define PLLDLL_H_H__

#include "comtypes.h"
#include "LongFract.h"
#include "DataForm.h"

#define K_BEGIN       (6.25)   ///< DLL coefficient at the begining  
#define DLL_TIME      (2000)   ///< time for startup when DLL coefficient is relatively big (K_BEGIN)
#define DLL_DEF_TIME  (1250)   ///< default DLL time parameter


#define PLLDLL_FIX     1                     ///< if 1, than PLLDLL is in the fixed point, otherwise it is in the floating point

#define CONV_PLL2DLL   1.01023817833908e-10  ///< converts pllState[1] to DLL aiding (lambda/c/two_pi)

#if PLLDLL_FIX 

/// PLL & DLL status structure (fix point version)
typedef struct
{
  /// for PLL
  uint32       phaseQ32;              ///< actual phase of LO, Q32 (0..2pi)[cycle]
  int32        dopplerQ15;            ///< doppler [Hz] , Q15 (-65...+65 kHz)
  int32        dopplerPrimeQ10;       ///< Hz/sec (+- 1 kHz/sec)
  /// for DLL
  tLongFract48 tau;                   ///< delay [milliseconds]

  tComplex32   y;                     ///< actual output of PLL prompt
  int16        phase_err;             ///< phase error in Q14 [rad.]
  int16        phase_err_avr;         ///< filtered phase error in Q14 [rad.]
}
tPLLDLLStatus;
#else
/// PLL & DLL status structure (floating point vertion)
typedef struct
{
  // for PLL
  double       phase;                 ///< phase [rad].,  phaseQ32 = Xextr[0] * 0xFFFFFFFF
  double       doppler;               ///< doppler [rad], dopplerQ15  = Xextr[1] * 32768
  double       dopplerPrime;          ///< Doppler prime [rad/s], dopplerPrimeQ10 = Xextr[2] * 1024
  // for DLL
  double       tau;                   ///< delay [seconds]
  tComplex32   y;                     ///< actual output of PLL prompt
  double       phase_err;             ///< phase error
  double       phase_err_avr;         ///< filtered phase error
}
tPLLDLLStatus;
#endif

/// PLL & DLL structure
typedef struct
{
  tPLLDLLStatus   status;         ///< PLL & DLL status structure

  tComplex32      accPrompt;      ///< accumulation correlations prompt
  tComplex32      accEarly;       ///< accumulation correlations early
  tComplex32      accLate;        ///< accumulation correlations late  
}
tPLLDLL;

//=====================================================================================
///  Initialize PLL-DLL (fix point version)
//
///  @param[in]  pPLLDLL     - pointer to instance
///  @param[in]  pPrompt     - pointer to prompt correlations buffer 
///  @param[in]  pEarly      - pointer to early correlations buffer
///  @param[in]  pLate       - pointer to late correlations buffer
///  @param[in]  tau         - initial delay
///  @param[in]  dopplerQ15  - initial Doppler frequency in Q15
//=====================================================================================
#if PLLDLL_FIX
EXTERN_C void PLLDLLInit (tPLLDLL           *pPLLDLL,
                          const tComplex32  *pPrompt,
                          const tComplex32  *pEarly,
                          const tComplex32  *pLate,
                          tLongFract48       tau,
                          int32              dopplerQ15);
#else
//=====================================================================================
///  Initialize PLL-DLL (floating point version)
//
///  @param[in]  pPLLDLL     - pointer to instance
///  @param[in]  pPrompt     - pointer to prompt correlations buffer 
///  @param[in]  pEarly      - pointer to early correlations buffer
///  @param[in]  pLate       - pointer to late correlations buffer
///  @param[in]  tau         - initial delay
///  @param[in]  doppler     - initial Doppler frequency
//=====================================================================================
EXTERN_C void PLLDLLInit (tPLLDLL           *pPLLDLL,
                          const tComplex32  *pPrompt,
                          const tComplex32  *pEarly,
                          const tComplex32  *pLate,
                          double             tau,
                          double             doppler);
#endif

//=====================================================================================
/// Interface PLL-DLL operation function
//
///  @param[in]  pPLLDLL       - pointer to instance
///  @param[in]  pPrompt       - pointer to prompt correlations buffer 
///  @param[in]  pEarly        - pointer to early correlations buffer
///  @param[in]  pLate         - pointer to late correlations buffer
///  @param[in]  tauQ          - quantized delay
///  @param[in]  epochCounter  - epoch counter
///  @param[in]  epochCntr     - epoch in bit counter (0...19)
///  @param[in]  coefPllDll    - PLL/DLL coefficients structure
///  @return     pointer to PLL & DLL status structure
//=====================================================================================
#if PLLDLL_FIX
EXTERN_C const tPLLDLLStatus *PLLDLLProcess (tPLLDLL           *pPLLDLL,
                                             const tComplex32  *pPrompt,
                                             const tComplex32  *pEarly,
                                             const tComplex32  *pLate,
                                             tLongFract48       tauQ,
                                             int32              epochCounter,
                                             int                epochCntr,
                                             const tCoefPLLDLL  coefPllDll);
#else
EXTERN_C const tPLLDLLStatus *PLLDLLProcess (tPLLDLL           *pPLLDLL,
                                             const tComplex32  *pPrompt,
                                             const tComplex32  *pEarly,
                                             const tComplex32  *pLate,
                                             double             tauQ,
                                             int32              epochCounter,
                                             int                epochCntr,
                                             const tCoefPLLDLL  coefPllDll);
#endif

//=====================================================================================
///  PLL/DLL coefficients initialization function
//
///  @param[out] coefPllDll - PLL/DLL coefficients structure
///  @param[in]  coefDLL    - DLL filter coefficient
///  @param[in]  timeDLL    - DLL integration time (1,2,4,5,10,20)
//=====================================================================================
EXTERN_C void PLLDLLSetDllCoef(tCoefPLLDLL *coefPllDll, int32 coefDLL, int8 timeDLL);

#endif // PLLDLL_H_H__
