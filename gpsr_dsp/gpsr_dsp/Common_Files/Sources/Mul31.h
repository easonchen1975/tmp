/**************************************************************************************
     Copyright (c) 2002 Spirit Corp.
***************************************************************************************
*
*    @project      GPS Receiver
*    @brief        Multiplies two Q31 fractional numbers
*    @file         Mul31.c
*    @author       Andrey Bykov
*    @date         21.11.2005
*    @version      1.0
*
**************************************************************************************/

#ifndef __MUL31_H
#define __MUL31_H

#include "comtypes.h"

#if defined(_TMS320C6400)
  #define PORTING_MUL31         1
  #define Mul31                 Mul31C64
#else
  #define PORTING_MUL31         0
  #define Mul31                 Mul31Cversion
 
//=====================================================================================
/// Multiplies two Q31 fractional numbers and returns product in Q31 format
///
/// @param[in] x - multiplier 1
/// @param[in] y - multiplier 2
/// @return    product in Q31
//=====================================================================================
  EXTERN_C int32 Mul31(int32 x, int32 y);
  
#endif  // defined(_TMS320C6400)

#if PORTING_MUL31==1
  // Use inlined version of Mul31()
  #include "Mul31.c"
#endif  // PORTING_MUL31

#endif  // __MUL31_H
