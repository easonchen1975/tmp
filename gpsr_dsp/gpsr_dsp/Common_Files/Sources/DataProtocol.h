/*****************************************************************************
*    Copyright (c) 2007 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Data protocol
*    @file                   DataProtocol.h
*    @author                 M. Zhokhova
*    @date                   13.06.2007
*    @version                3.1
*/
#ifndef _DATA_PROTOCOL_H_
#define _DATA_PROTOCOL_H_

#include "SatelliteManager.h"
#include "NavTask.h"
#include "MainManager.h"

//=================================================================================
/// @brief      Messages forming
///
/// @param[in]  pNavTask - pointer to Navigation Task Solution object
/// @param[in]  pSatMgr  - pointer to Satellite manager object
/// @param[in]  pOPMgr   - pointer to OP Manager object
/// @param[in]  Message  - message types
/// @param[in]  data     - pointer to data
/// @param[in]  pTerm    - pointer to Debug terminal
//=================================================================================
EXTERN_C void MessagesForming(tNavTask          *pNavTask,
                              tSatelliteManager *pSatMgr,
                              tOPManager        *pOPMgr,
                              uint16             Message,
                              tICDdata          *data,
                              tTerminal         *pTerm);

//=====================================================================
/// Convert integer value to string
///
/// @param[in]  value  - input value
/// @param[in]  digits - necessary quantity of digits in string
/// @param[in]  Str    - pointer to string
/// @return     pointer to string with value
//=====================================================================
EXTERN_C int8* Int_to_String(int32 value, int32 digits, int8 *Str);

//=================================================================================
/// Convert 64-bit integer value to string with point
///
/// @param[in]  value  - input value
/// @param[in]  digits - necessary quantity of digits in string
/// @param[in]  dgsPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string with value
//=================================================================================
EXTERN_C int8* Int64_to_String(int64 value, int32 digits, int32 dgtPnt, int8 *Str);

//=================================================================================
/// Convert 32-bit integer value to string with point
///
/// @param[in]  value  - input value
/// @param[in]  digits - necessary quantity of digits in string
/// @param[in]  dgsPnt - digits number with end to point
/// @param[in]  Str    - pointer to string
/// @return     pointer to string with value
//=================================================================================
EXTERN_C int8* Int32_to_String(int32 value, int32 digits, int32 dgtPnt, int8 *Str);

#endif //_DATA_PROTOCOL_H_

