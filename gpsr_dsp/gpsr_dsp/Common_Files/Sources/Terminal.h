/*****************************************************************************
 *   Copyright (c) 2003-2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Abstract terminal driver
 *   @file                   Terminal.h
 *   @author                 D. Paroshin, V. Yeremeyev
 *   @date                   02.09.2005
 *   @version                2.0
 */
/*****************************************************************************/

/// @cond
#ifndef  _TERMINAL_DRIVER_
#define  _TERMINAL_DRIVER_
/// @endcond

#include <stdio.h>
#include "FIFO.h"
#include "Global.h"
#include "TerminalDrv.h"
#include "DataForm.h"

// Receiver/transmitter buffers:
#define TERMINAL_READ_BUFFER_SIZE     (4096)      ///< Receiver buffer
#define TERMINAL_WRITE_BUFFER_SIZE    (4096)      ///< Transmitter buffer

//=============================================================================
/// Main data structure for GUI Terminal
//=============================================================================
typedef struct tagTerminal
{
  tTerminalDrv  vtable;          ///< Base object

  uint8         ParcelOfData;    ///< parcel of data for baud rate

  tFIFO         TransmitQueue;   ///< Queue for printed chars
  tFIFO         ReceiveQueue;    ///< Queue for coming from console chars

  char          m_WriteBuffer[TERMINAL_WRITE_BUFFER_SIZE];
  char          m_ReadBuffer[TERMINAL_READ_BUFFER_SIZE];

#ifdef EXT_SRAM
  uint8         *FlagRead;
  DATA_STRING   *stringWrite;
  DATA_STRING   *stringRead;
#else
  uint8         FlagRead;
  DATA_STRING   stringWrite;
  DATA_STRING   stringRead;
#endif  
  eFlag         FlagUART;

} tTerminal;

//=============================================================================
/// Read char string from host
///
/// @param[in]  pTerminal - pointer to instance
/// @param[out] str       - pointer to string to read from host
/// @param[in]  len       - maximum string length
/// @return     operation status
//=============================================================================
EXTERN_C tIOstat TRead(tTerminalDrv *pTerminal, char *str, int MaxLen);

//=============================================================================
/// Read binary data from host
///
/// @param[in]  pTerminal - pointer to instance
/// @param[out] pBuffer   - pointer to string to read from host
/// @param[in]  Count     - number of bytes to read from host
/// @return     operation status
//=============================================================================
EXTERN_C uint16 TReadBinary(tTerminalDrv  *pTerminal,
                            uint8         *pBuffer,
                            uint16         Count);

//=============================================================================
/// Write char string to host
///
/// @param[in]  pTerminal - pointer to instance
/// @param[in]  str       - pointer to string to write to host
/// @param[in]  len       - string length
/// @return     operation status
//=============================================================================
EXTERN_C tIOstat TWrite(tTerminalDrv *pTerminal, const char *str, int len);

//=============================================================================
/// Write binary data to host
///
/// @param[in]  pTerminal - pointer to instance
/// @param[in]  pBuffer   - pointer to binary data to write to host
/// @param[in]  Count     - number of bytes to write to host
/// @return     operation status
//=============================================================================
EXTERN_C uint16 TWriteBinary(tTerminalDrv  *pTerminal,
                             const uint8   *pBuffer,
                             uint16         Count);

/// @cond
#endif // _TERMINAL_DRIVER_
/// @endcond
