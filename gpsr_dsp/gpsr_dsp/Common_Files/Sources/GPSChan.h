/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS 3x12 Receiver
 *    @brief                  GPS search-tracking channel uses correlator
 *                            with decimation working either on 8184 MHz
 *    @file                   GPSChan.h
 *    @author                 A. Nazarov
 *    @date                   18.08.2005
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef GPSCHAN_H__
#define GPSCHAN_H__

#include "comtypes.h"
#include "GPS.h"
#include "PLLDLL.h"
#include "LongFract.h"
#include "caCode.h"

#define CORR_BIT        (20)           ///< correlations number in bit
#define ARRAY_SIZE      (31)           ///< correlations number for initialization DLL
#define CORR_SHIFT      (8)            ///< shift for correlations
#define BIT_NQ14        (26214)        ///< 26214 == 32/20. in Q14
#define BIT_NQ19        (26214)        ///< 26214 == 1/20 in Q19
#define CN_MIN          (27)           ///< minimal SNR
#define LOG10_2Q13      (-24660)       ///< -24660==log10(2) in Q13
#define Q15_0_1         (3277)         ///< 3277==0.1 in Q15
#define K_DOPQ50        (0x41999DB3L)  ///< (1*0.001s/1023 in Q50)
#define K_DOP_PRMQ60    (0x219654D6L)  ///< (0.001s^2)/(1023)/2 in Q60)
#define Q16_0_5         (32768UL)      ///< 0.5 in Q16
#define BIT_BOUND       (11)           ///< variant bit bounadry number

/// structure for initialization DLL
typedef struct
{
  int position;    ///< new position
  int isFinished;  ///< 1 if search is finished
}
tEstPosResult;

/// structure for preamble and detected preamble time saving
typedef struct
{
  int32 PreambleTime;         ///< preamble time [s]
  int32 time;                 ///< detected time: time of week [ms]
  int16 week;                 ///< detected time: week
}
tPreamble;

/// channel states
typedef enum
{
  es_IDLE,               ///< wait for start
                         ///< software can automatically fall back to the es_IDLE if tracking is failed
  es_INIT_PLL,           ///< initialization PLL
  es_INIT_DLL,           ///< initialization DLL
  es_WAIT_BITBOUNDARY,   ///< waiting for bit boundary
  es_TRACKING_STARTUP,   ///< tracking startup. Here, channel is locked, but no C/N defined
  es_TRACKING            ///< tracking
}
eGPSchanState;

/// process status: ms boundary transition flag
typedef enum
{
  epf_DONOTHING_ELSE,       ///< one epoch process
  epf_PROCESS_EPOCH_AGAIN,  ///< two epoch process
  epf_SKIP_EPOCH            ///< one epoch skip
}
epfGPSChanProcessFlag;

/// bit value
typedef enum
{
  egpschan_ZERO  = 0,  ///< bit is zero
  egpschan_ONE   = 1,  ///< bit is one
  egpschan_NOBIT = -1  ///< bit is not defined yet
}
eGPSChanBit;

/// channel status
typedef struct
{
  eGPSchanState state;              ///< channel state
  tLongFract48  delay;              ///< actual delay for tracking (in msec)
  tLongFract48  tauQ;               ///< position in milliseconds
  int32         dopplerQ15;         ///< actual doppler (sum of replica doppler and PLL doppler), in Q15
  int32         dopplerPrimeQ10;    ///< actual doppler prime in Hz/sec (+- 1 kHz/sec), in Q10
  int16         CN_dBHzQ7;          ///< C/N estimate in dB-Hz, Q7
  int32         epochCounter;       ///< epoch counter
  eGPSChanBit   bit;                ///< output bit
  int32         Nframe_add;         ///< for reacquisition, bit numbers
  int8          epoch_control;      ///< epoch control: two epoch process or skip epoch
  int16         phase_err_avrQ14;   ///< filtered phase error in Q14 [rad.]
}
tGPSchanStatus;

/// channel structure
typedef struct
{
#ifdef EXT_SRAM
  int8                  *prompt;         ///< array for C/A codes
#else
  int8                  prompt[CODE_LENGTH];         ///< array for C/A codes
#endif  
  tGPSchanStatus        status;                      ///< channel status
  int32                 dopplerQ15;                  ///< doppler for replica, Q15
  int32                 dopplerPrimeQ10;             ///< actual doppler prime in Hz/sec (+- 1 kHz/sec), in Q10
  int32                 noiseQ16;                    ///< noise estimate, Q16
  uint32                phase;                       ///< actual phase of LO, Q32 (0..2pi)[rad.]
  int32                 phaseInc;                    ///< phase increment for LO (at chip rate)
  int                   position;                    ///< position in sample intervals
  epfGPSChanProcessFlag processFlag;                 ///< ms boundary transition flag
  int                   epochInBit;                  ///< epoch counter inside bit [0...19].
                                                     ///  Bit is calculated as soon as this counter is reached 19. Normally,
                                                     ///  if bit is decoded (bit!=egpschan_NOBIT), this counter is zero
  tComplex32            initArray[(CORR_BIT<<1)];    ///< this array is used for saving correlation result: for two bits
  int32                 accBitBoundCorr[BIT_BOUND];  ///< array for bit boundary test
  eGPSChanBit           lastBit;                     ///< last bit value
  eFlag                 bitBoundControl;             ///< bit boundary control process end flag
  int32                 array[ARRAY_SIZE];           ///< array for initialization DLL
  int                   trialCount;                  ///< trial counts for searching the boundary
  tPLLDLL               pllDll;                      ///< PLL/DLL structure
  int                   StartupTime;                 ///< startup delay (ms)
  int                   BitBoundaryTime;             ///< time for bit boundary test
  eFlag                 CNControl;                   ///< control SNR for bit boundary test
  uint8                 sv_num;                      ///< satellite ID
} 
tGPSchan;

//================================================================================
/// get bit and estimate C/N
///
/// @param[in]  x        - pointer to correlator outputs for 20 epochs
/// @param[in]  noiseQ16 - pointer to noise estimate
/// @param[out] CNQ7     - pointer to C/N estimation, Q7
/// @return     returns detected bit
//================================================================================
eGPSChanBit getBit(const tComplex32* restrict x,
                   int32* restrict            noiseQ16,
                   int16* restrict            CNQ7);

//================================================================================
/// Initialize the channel
///
/// @param[in]  pGPSchan   - pointer to instance
/// @param[in]  sv_num     - SV number
//================================================================================
EXTERN_C void GPSchanInit(tGPSchan         *pGPSchan,
                          int               sv_num);

//================================================================================
/// Start initialization and tracking from known position and estimated doppler
///
/// @param[in]  pGPSchan - pointer to instance
/// @param[in]  doppler  - rough estimation of doppler frequency, [Hz]
/// @param[in]  position - rough estimation of sampling position inside the epoch
//================================================================================
EXTERN_C void GPSchanStart(tGPSchan *pGPSchan, double doppler, double position);

//================================================================================
/// Process epoch
///
/// @param[in]  pGPSchan         - pointer to instance
/// @param[in]  pEpoch           - pointer to epoch (2 continuous milliseconds of
///                                 data at UPSAMPLE_RATIOx chip rate)
/// @param[in]  week             - GPS week number
/// @param[in]  ms_count         - current time [ms]
/// @param[in]  preamble         - pointer to preamble structure
/// @param[in]  CN_THRESHOLD     - C/N threshold in dB-Hz
/// @param[in]  coefPllDll       - structure with coefficients for PLL/DLL
/// @param[in]  Nframe           - bits counter
//================================================================================
EXTERN_C void GPSchanProcessEpoch(tGPSchan         *pGPSchan,
                                  const tEpoch     *pEpoch,
                                  const int16       week,
                                  const uint32      ms_count,
                                  tPreamble        *preamble,
                                  int8              CN_THRESHOLD,
                                  const tCoefPLLDLL coefPllDll,
                                  int16             Nframe);

//================================================================================
/// Return status of channel
///
/// @param[in]  pGPSchan     - pointer to instance
//================================================================================
EXTERN_C const tGPSchanStatus* GPSchanGetStatus(const tGPSchan *pGPSchan);

#endif  // GPSCHAN_H__
