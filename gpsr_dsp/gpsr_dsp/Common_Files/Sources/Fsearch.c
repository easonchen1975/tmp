/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Fast search algorithm
 *   @file                   Fsearch.c
 *   @author                 B. Oblakov
 *   @date                   16.06.2006
 *   @version                1.0
 */
/*****************************************************************************/

#include "baseop.h"
#include "FSearch.h"

/// bit reverse table containing 64 entries
static uint8 brev[64] = {
0x0, 0x20, 0x10, 0x30, 0x8, 0x28, 0x18, 0x38,
0x4, 0x24, 0x14, 0x34, 0xc, 0x2c, 0x1c, 0x3c,
0x2, 0x22, 0x12, 0x32, 0xa, 0x2a, 0x1a, 0x3a,
0x6, 0x26, 0x16, 0x36, 0xe, 0x2e, 0x1e, 0x3e,
0x1, 0x21, 0x11, 0x31, 0x9, 0x29, 0x19, 0x39,
0x5, 0x25, 0x15, 0x35, 0xd, 0x2d, 0x1d, 0x3d,
0x3, 0x23, 0x13, 0x33, 0xb, 0x2b, 0x1b, 0x3b,
0x7, 0x27, 0x17, 0x37, 0xf, 0x2f, 0x1f, 0x3f
};

#ifdef USE_DSPBIOS
//==================================================================================
/// Asm - function for calculation 4096 points FFT
///
/// @param[in]  n      - length of FFT in complex samples
/// @param[in]  ptr_x  - pointer to complex data input
/// @param[in]  ptr_w  - pointer to complex twiddle factor
/// @param[in]  ptr_y  - pointer to complex data output
/// @param[in]  brev   - pointer to bit reverse table containing 64 entries
/// @param[in]  radix  - smallest FFT butterfly used in computation
/// @param[in]  offset - index in complex samples of sub-FFT from start of main FFT
/// @param[in]  n_max  - size of main FFT in complex samples
//==================================================================================
extern void DSP_fft16x16r(int                      n,
                          short *restrict          ptr_x,
                          const short *restrict    ptr_w,
                          unsigned char *restrict  brev,
                          short *restrict          ptr_y,
                          int                      radix,
                          int                      offset,
                          int                      nmax);
#else
// for PC model
#include "baseop.h"
#include "fft16x16.h"
#include <string.h>
#endif

#ifdef EXT_SRAM
#pragma CODE_SECTION (FsearchFFT8, "fast_text")
#endif

//==================================================================================
/// Static function for coherent integration using 8 point FFT
///
/// @param[in]   Corr      - pointer to correlator output buffer
/// @param[out]  threshRes - detection threshold
//==================================================================================
static void FsearchFFT8(tComplex16Aligned* Corr, int32* threshRes)
{
  int32             i, 
                    del;           ///< shift
  int32             ResPW;         ///< total power
  tComplex16Aligned G[(FFT8>>1)],  ///< 4 points FFT output (block1)
                    H[(FFT8>>1)],  ///< 4 points FFT output (block2)
                    tmp,           ///< temporary variables
                    jtmpG,         ///< temporary variables
                    jtmpH,         ///< temporary variables
                    w,             ///< temporary variables
                    H1w1,          ///< temporary variables
                    H3w3;          ///< temporary variables

  w.dummy = 0x5a825a82;  /// 1/sqrt(2) packed into two 16 bits constants
  ResPW = 0;

  for(i=0; i<(CODE_LENGTH<<1); i++)
  {
    /// First stage (2 4-points DFT)
    del = FFT8*i;
    G[0].dummy = _ADD2(_ADD2(Corr[0+del].dummy, Corr[2+del].dummy), _ADD2(Corr[4+del].dummy, Corr[6+del].dummy));
    H[0].dummy = _ADD2(_ADD2(Corr[1+del].dummy, Corr[3+del].dummy), _ADD2(Corr[5+del].dummy, Corr[7+del].dummy));

    tmp.dummy  = _SUB2(Corr[2+del].dummy, Corr[6+del].dummy);
    jtmpG.c.re  = -tmp.c.im;
    jtmpG.c.im  = tmp.c.re;
    G[1].dummy = _SUB2(_SUB2(Corr[0+del].dummy, Corr[4+del].dummy), jtmpG.dummy);
    tmp.dummy  = _SUB2(Corr[3+del].dummy, Corr[7+del].dummy);
    jtmpH.c.re  = -tmp.c.im;
    jtmpH.c.im  = tmp.c.re;
    H[1].dummy = _SUB2(_SUB2(Corr[1+del].dummy, Corr[5+del].dummy), jtmpH.dummy);

    G[2].dummy = _ADD2(_SUB2(Corr[0+del].dummy, Corr[2+del].dummy), _SUB2(Corr[4+del].dummy, Corr[6+del].dummy));
    H[2].dummy = _ADD2(_SUB2(Corr[1+del].dummy, Corr[3+del].dummy), _SUB2(Corr[5+del].dummy, Corr[7+del].dummy));

    G[3].dummy = _ADD2(_SUB2(Corr[0+del].dummy, Corr[4+del].dummy), jtmpG.dummy);
    H[3].dummy = _ADD2(_SUB2(Corr[1+del].dummy, Corr[5+del].dummy), jtmpH.dummy);

    H1w1.dummy = _PACK2(_DOTPN2(H[1].dummy, w.dummy)>>15, _DOTP2(H[1].dummy, w.dummy)>>15);

    tmp.dummy = H[2].dummy;
    H[2].c.re = tmp.c.im;
    H[2].c.im = -tmp.c.re;

    H3w3.dummy = _PACK2(-(_DOTP2(H[3].dummy, w.dummy)>>15), _DOTPN2(H[3].dummy, w.dummy)>>15);

    /// Second stage (4 2-points DFT and power calculation for adaptive threshold)  
    Corr[0+del].dummy = _ADD2(G[0].dummy, H[0].dummy); 
      ResPW += _DOTP2(Corr[0+del].dummy, Corr[0+del].dummy)>>5;

    Corr[1+del].dummy = _ADD2(G[1].dummy, H1w1.dummy);
      ResPW += _DOTP2(Corr[1+del].dummy, Corr[1+del].dummy)>>5;

    Corr[2+del].dummy = _ADD2(G[2].dummy, H[2].dummy);
      ResPW += _DOTP2(Corr[2+del].dummy, Corr[2+del].dummy)>>5;

    Corr[3+del].dummy = _ADD2(G[3].dummy, H3w3.dummy);
      ResPW += _DOTP2(Corr[3+del].dummy, Corr[3+del].dummy)>>5;

    Corr[4+del].dummy = _SUB2(G[0].dummy, H[0].dummy);
      ResPW += _DOTP2(Corr[4+del].dummy, Corr[4+del].dummy)>>5;

    Corr[5+del].dummy = _SUB2(G[1].dummy, H1w1.dummy);
      ResPW += _DOTP2(Corr[5+del].dummy, Corr[5+del].dummy)>>5;

    Corr[6+del].dummy = _SUB2(G[2].dummy, H[2].dummy);
      ResPW += _DOTP2(Corr[6+del].dummy, Corr[6+del].dummy)>>5;

    Corr[7+del].dummy = _SUB2(G[3].dummy, H3w3.dummy);
      ResPW += _DOTP2(Corr[7+del].dummy, Corr[7+del].dummy)>>5;

  }
  *threshRes = ResPW>>6;
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (FsearchSignalFFT, "fast_text")
#endif

//==================================================================================
/// Interface function for signal transformation to the frequency domain
///
/// @param[in]   pFsearch     - pointer to fast search algorithm structure
/// @param[out]  packedSignal - pointer to packed signal for 8 sequential epochs 
///                             at the double chip rate
//==================================================================================
void FsearchSignalFFT(tFsearch *pFsearch, int32* packedSignal)
{
  int32  i,
         j,
         k;
  int16* pFFT_F = (int16*)pFsearch->Sign_F;               ///< pointer to FFT of signal
  int32* pSign_T = &packedSignal[CODE_LENGTH*FFT8 + 16];  ///< pointer to signal buffer

  /// for 8 ms
  for(i=0; i<FFT8; i++)
  {
    /// Unpacking of the Signal Buffer for FFT
    memset(pFsearch->Tmp_T, 0, sizeof(pFsearch->Tmp_T));
    #ifdef  _TMS320C6X
      #pragma UNROLL(2)
    #endif
    #ifdef USE_DSPBIOS
      /// for target
      for(k=0; k<CODE_LENGTH; k++) 
      {
        pFsearch->Tmp_T[k].dummy = _MPYSU4(*(pSign_T++), 0x01010101);
        pFsearch->Tmp_T[k].c[0].re *= SIGNAL_SCALE;
        pFsearch->Tmp_T[k].c[0].im *= SIGNAL_SCALE;
        pFsearch->Tmp_T[k].c[1].re *= SIGNAL_SCALE;
        pFsearch->Tmp_T[k].c[1].im *= SIGNAL_SCALE;
      }
    #else
      /// for PC model
      for(k=0; k<CODE_LENGTH; k++)
      {
        tDoubleWord tmp = _MPYSU4(*(pSign_T++), 0x01010101);
        pFsearch->Tmp_T[k].w[0] = tmp.lo; 
        pFsearch->Tmp_T[k].w[1] = tmp.hi;
        pFsearch->Tmp_T[k].c[0].re *= SIGNAL_SCALE;
        pFsearch->Tmp_T[k].c[0].im *= SIGNAL_SCALE;
        pFsearch->Tmp_T[k].c[1].re *= SIGNAL_SCALE;
        pFsearch->Tmp_T[k].c[1].im *= SIGNAL_SCALE;
      }
    #endif

    /// Signal FFT calculation:
    /// DSP_fft16x16r(4096, (int16*)Tmp_T, (int16*)TwiddleCoeff, brev, pSign_F, 4, 0, 4096);
    #ifdef USE_DSPBIOS
      /// for target
      DSP_fft16x16r(FFT4096, (int16*)(&pFsearch->Tmp_T[0]),    (int16*)(&pFsearch->wFFT[0])   , brev, pFFT_F, 1024, 0, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[0]),    (int16*)(&pFsearch->wFFT[1536]), brev, pFFT_F, 4,    0, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[512]),  (int16*)(&pFsearch->wFFT[1536]), brev, pFFT_F, 4, 1024, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[1024]), (int16*)(&pFsearch->wFFT[1536]), brev, pFFT_F, 4, 2048, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[1536]), (int16*)(&pFsearch->wFFT[1536]), brev, pFFT_F, 4, 3072, FFT4096);
    #else
      /// for PC model
      DSP_fft16x16r_c(FFT4096, (int16*)(&pFsearch->Tmp_T[0]), (int16*)(&pFsearch->wFFT[0]), pFFT_F, brev, 4, 0, FFT4096);
    #endif

    /// Packing of the Signal frequency domain Buffer
    for(j=0; j<FFT4096_HALF; j++)
    {
      int32 tmp0 = pFsearch->Sign_F[j].w[0];
      int32 tmp1 = pFsearch->Sign_F[j].w[1];
      packedSignal[i*FFT4096_HALF+j] = _PACKL4(tmp1, tmp0);
    }
  }
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (FsearchInit, "fast_text")
#endif

//==================================================================================
///  Function for C/A code replica FFT calculation
///
/// @param[in]   pFsearch - pointer to fast search algorithm structure
/// @param[in]   SVnum    - satellite PRN number
//==================================================================================
void FsearchInit(tFsearch *pFsearch, int SVnum)
{
  int32  i;
  int8   code[CODE_LENGTH];  ///< C/A code for satellite
  int16* pRepl_F = ((int16*)pFsearch->Repl_F) + (int16)(FSEARCH_SHIFT_MAX<<1);  ///< pointer to FFT of replica

  /// replica calculation
  getCAcode (code, SVnum);

  /// scale by replica
  memset(pFsearch->Tmp_T, 0, sizeof(pFsearch->Tmp_T));
  for(i=0; i<CODE_LENGTH; i++)
  {
    pFsearch->Tmp_T[i].c[0].re = pFsearch->Tmp_T[i+CODE_LENGTH].c[0].re = code[i]*REPLICA_SCALE;
    pFsearch->Tmp_T[i].c[1].re = pFsearch->Tmp_T[i+CODE_LENGTH].c[1].re = code[i]*REPLICA_SCALE;
  }

  /// Replica FFT calculation:
  /// DSP_fft16x16r(4096, (int16*)Tmp_T, (int16*)TwiddleCoeff, brev, pRepl_F, 4, 0, 4096);
  #ifdef USE_DSPBIOS
    /// for target
    DSP_fft16x16r(FFT4096, (int16*)(&pFsearch->Tmp_T[0]),    (int16*)(&pFsearch->wFFT[0])   , brev, pRepl_F, 1024, 0, FFT4096);
    DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[0]),    (int16*)(&pFsearch->wFFT[1536]), brev, pRepl_F, 4,    0, FFT4096);
    DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[512]),  (int16*)(&pFsearch->wFFT[1536]), brev, pRepl_F, 4, 1024, FFT4096);
    DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[1024]), (int16*)(&pFsearch->wFFT[1536]), brev, pRepl_F, 4, 2048, FFT4096);
    DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[1536]), (int16*)(&pFsearch->wFFT[1536]), brev, pRepl_F, 4, 3072, FFT4096);
  #else
    /// for PC model
    DSP_fft16x16r_c(FFT4096, (int16*)(&pFsearch->Tmp_T[0]), (int16*)(&pFsearch->wFFT[0]), pRepl_F, brev, 4, 0, FFT4096);
  #endif

  /// Widened frequency domain Replica
  for(i=0; i<(FSEARCH_SHIFT_MAX>>1); i++)
  {
    pFsearch->Repl_F[i] = pFsearch->Repl_F[FFT4096_HALF+i];
    pFsearch->Repl_F[(FSEARCH_SHIFT_MAX>>1) + FFT4096_HALF + i] = pFsearch->Repl_F[i+(FSEARCH_SHIFT_MAX>>1)];
  }
}


#ifdef EXT_SRAM
#pragma CODE_SECTION (FsearchProcessInOneShift, "fast_text")
#endif

//==================================================================================
/// Static function carries out satellite fast search in 8 given epochs in one shift
///
/// @param[in]   pFsearch     - pointer to fast search algorithm structure
/// @param[out]  nShift       - shift Doppler
/// @param[out]  packedSignal - pointer to buffer which consists Fourier transformed
///                             signal epochs
/// @return      threshold
//==================================================================================
static int32 FsearchProcessInOneShift(tFsearch *pFsearch,
                                      int16     nShift,
                                      int32    *packedSignal)
{
  int16 i,
        j;
  int32 threshRes = 0;  ///< threshold

  int16* pRepl_F = ((int16*)pFsearch->Repl_F) + (int16)(FSEARCH_SHIFT_MAX<<1);  ///< pointer to FFT of replica
  int16* pSign_F = (int16*)pFsearch->Sign_F;                                    ///< pointer to FFT of signal

  /// Forming of the Corr Buffer for DFT 
  for(j=0; j<FFT8; j++)
  {
    /// Frequency domain Discrete Correlation
    for(i=0; i<FFT4096_HALF; i++)
    {
      int32 m0 = *((int32 *)pRepl_F +2*i - nShift);
      int32 m1 = _LO(_MPYSU4(packedSignal[j*FFT4096_HALF+i], 0x01010101));
      int32 m2 = *((int32 *)pRepl_F +2*i +1 - nShift);
      int32 m3 = _HI(_MPYSU4(packedSignal[j*FFT4096_HALF+i], 0x01010101));

      pFsearch->Tmp_T[i].c[0].re = (int16)(_DOTP2(m1, m0)>>FFT_SCALE);
      pFsearch->Tmp_T[i].c[0].im = (int16)(_DOTPN2(m1, _PACKLH2(m0, m0))>>FFT_SCALE);
      pFsearch->Tmp_T[i].c[1].re = (int16)(_DOTP2(m3, m2)>>FFT_SCALE);
      pFsearch->Tmp_T[i].c[1].im = (int16)(_DOTPN2(m3, _PACKLH2(m2, m2))>>FFT_SCALE);
    }
    /// Correlation calculation 
    #ifdef USE_DSPBIOS
      /// for target
      DSP_fft16x16r(FFT4096, (int16*)(&pFsearch->Tmp_T[0]),    (int16*)(&pFsearch->wFFT[0])   , brev, pSign_F, 1024, 0, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[0]),    (int16*)(&pFsearch->wFFT[1536]), brev, pSign_F, 4,    0, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[512]),  (int16*)(&pFsearch->wFFT[1536]), brev, pSign_F, 4, 1024, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[1024]), (int16*)(&pFsearch->wFFT[1536]), brev, pSign_F, 4, 2048, FFT4096);
      DSP_fft16x16r(1024,    (int16*)(&pFsearch->Tmp_T[1536]), (int16*)(&pFsearch->wFFT[1536]), brev, pSign_F, 4, 3072, FFT4096);
    #else
      /// for PC model
      DSP_fft16x16r_c(FFT4096, (int16*)(&pFsearch->Tmp_T[0]), (int16*)(&pFsearch->wFFT[0]), pSign_F, brev, 4, 0, FFT4096);
    #endif

    /// Forming of the Correlation Buffer for DFT 
    for(i=0; i<CODE_LENGTH; i++)
    {
      pFsearch->cumCorr[i*2][j].c.re =    pFsearch->Sign_F[i].c[0].re;
      pFsearch->cumCorr[i*2][j].c.im =    pFsearch->Sign_F[i].c[0].im;
      pFsearch->cumCorr[i*2+1][j].c.re =  pFsearch->Sign_F[i].c[1].re;
      pFsearch->cumCorr[i*2+1][j].c.im =  pFsearch->Sign_F[i].c[1].im;
    }
  }  /// End of the for(j=0; j<8; j++) forming of the first Corr Buffer for DFT

  /// FFT 8-point calculation
  FsearchFFT8(&pFsearch->cumCorr[0][0], &threshRes);

  return (threshRes>>THRESH_SCALE);
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (FsearchProcess, "fast_text")
#endif

//==================================================================================
/// Interface function carries out satellite fast search in 8 given epochs
///
/// @param[in]   pFsearch     - pointer to fast search algorithm structure
/// @param[out]  pResult      - search results
/// @param[out]  nShift_min   - left boundary of Doppler range
/// @param[out]  nShift_max   - right boundary of Doppler range
/// @param[out]  packedSignal - pointer to buffer which consists Fourier transformed
///                             signal epochs
//==================================================================================
void FsearchProcess(tFsearch     *pFsearch,
                    tFSearchRes  *pResult, 
                    int16         nShift_min, 
                    int16         nShift_max,
                    int32        *packedSignal)
{
  int16  i,
         j,
         n,
         nShift,                   ///< shift by Doppler frequency
         nShiftMax = 0,            ///< shift for maximal correlation: 0.5   kHz
         kfMax     = 0;            ///< shift for maximal correlation: 0.125 kHz
  int32  thresh,                   ///< threshold
         corrRes;                  ///< current correlation
  double kThresh = SEARCH_THRESH;  ///< coefficient for threshold 
  int32  corrResMax = 0,           ///< maximal correlation
         iSampMax   = 0,           ///< position for maximal correlation
         iSampBoundary;            ///< boundary for position
  double l_Thresh;                 ///< coefficient for threshold for accumulation

  
  if (nShift_max == nShift_min)
    l_Thresh = 1.0;
  else
    l_Thresh = 1.0/(nShift_max - nShift_min);  ///< coefficient for threshold for accumulation

  pResult->isFound = 0;

  /// searching process
  thresh = 0;

  /// for all shifts: from minimal to maximal
  for(n=0; n<=(nShift_max-nShift_min); n++)
  {
    /// calculate current shift
    if((n & 0x1)==1) nShift = (nShift_max+nShift_min)/2 + (n>>1) +1;
    else nShift = (nShift_max+nShift_min)/2 - (n>>1);  /// circular shift in frequency domain

    /// carries out satellite fast search in 8 given epochs in one shift
    thresh += FsearchProcessInOneShift(pFsearch, nShift, packedSignal);

    /// Maximum determination
    for(i=0; i<(CODE_LENGTH<<1); i++)
    {
      for(j=0; j<FFT8; j++)
      {
        corrRes = (int32)pFsearch->cumCorr[i][j].c.re * (int32)pFsearch->cumCorr[i][j].c.re +
                  (int32)pFsearch->cumCorr[i][j].c.im * (int32)pFsearch->cumCorr[i][j].c.im;
        if(corrRes > corrResMax)
        {
          corrResMax = corrRes;
          iSampMax = i;
          nShiftMax = nShift;
          kfMax = j;
        }
      }
    }
  }  /// end of for(nShift=nShift_min; nShift<=nShift_max; nShift++)

  thresh = (int32)((double)kThresh *(double)thresh *l_Thresh);  /// adaptive threshold

  if(corrResMax >= thresh)  /// signal found
  {
    /// carries out satellite fast search in 8 given epochs in maximal shift
    FsearchProcessInOneShift(pFsearch, nShiftMax, packedSignal);

    /// search minimal position with correlation more then threshold
    iSampBoundary = iSampMax + ADDITIONAL_POSITION;
    if (iSampBoundary > (CODE_LENGTH<<1))
      iSampBoundary = (CODE_LENGTH<<1)-1;

    for(i=(iSampMax+1); i<=iSampBoundary;i++)
    {
      corrRes = (int32)pFsearch->cumCorr[i][kfMax].c.re * (int32)pFsearch->cumCorr[i][kfMax].c.re +
                (int32)pFsearch->cumCorr[i][kfMax].c.im * (int32)pFsearch->cumCorr[i][kfMax].c.im;
      if(corrRes >= thresh)
        iSampMax = i;
    }

    /// position estimation
    pResult->position = (CODE_LENGTH<<1) - iSampMax;
    /// frequency estimation 
    if (nShiftMax&0x1 == 0x1)
    {
      pResult->freqEst = (int32)(((double)(nShiftMax>>1) + (double)kfMax*FFT8_RES)*(double)FFT4096_HALF);
    }
    else
    {
      if (kfMax >= (FFT8>>1))
        pResult->freqEst = (int32)(((double)((nShiftMax>>1) - 1) + (double)kfMax*FFT8_RES)*(double)FFT4096_HALF);
      else
        pResult->freqEst = (int32)(((double)(nShiftMax>>1) + (double)kfMax*FFT8_RES)*(double)FFT4096_HALF);
    }

    pResult->isFound = 1;
  }
}
