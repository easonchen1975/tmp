/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                AJGGR
 *    @brief                  Navigation task
 *    @file                   NavTask.h
 *    @author                 A. Baloyan, M. Zhokhova
 *    @date                   25.06.2005
 *    @version                2.1
 */
/*****************************************************************************/

#ifndef _NAVTASK_H_
#define _NAVTASK_H_

#include "matrix.h"
#include "SatelliteManager.h"
#include "Framework.h"
#include "..\OP\OPManager.h"

#define ITERATIONS                 (3)           ///< iterations number for navigation task solution
#define ITERATIONS_START           (6)
#define THRESH_FIRST_NTS  (float64)(50.0)        ///< threshold for first navigation task solution
#define SAT_COUNT_NO_NT            (3)           ///< satellites number which no navigation task solution
#define SAT_COUNT_RAIM             (5)           ///< satellites number for RAIM algorithm
#define SAT_COUNT_FDI              (6)           ///< satellites number for FDI algorithm
#define SAT_COUNT_DOP              (2)           ///< satellites number for DOP calculation
#define THRESH_POS        (float64)(0.01)        ///< position correction exceeds 1 cm
#define NO_NAV_TASK                (0)           ///< navigation task isn't solved
#define OK_NAV_TASK                (1)           ///< navigation task is successfully solved
#define ERROR_NAV_TASK             (2)           ///< navigation task is error solved
#define RAIM_ERROR                 (3)           ///< navigation task is error solved by RAIM
#define GOOD_EPH_TIME     (float64)(7200.0)      ///< (2 hour in s) time while good ephemeris for navigation task solution
#define GOOD_ALM_TIME     (float64)(15552000.0)  ///< (180 days in s) time while good almanac for fast search
#define GOOD_ALM_FOR_EPH  (float64)(1209600.0)   ///< (14 day in s) time while good almanac for control ephemeris validity
#define SYNC_TIME                  (30000)       ///< time for synchronization OP and navigation task solution

/// structure Navigation Task
typedef struct
{
  DATA_STRING      GSVStrings;         ///< for message by NMEA protocol

  USER             User;               ///< User parameters

  NAV_CONDITIONS   NavCond;            ///< navigation conditions

  uint32           PeriodTime;         ///< period of time for posting NavTaskThread

  uint32           PeriodSyncFract;    ///< fractional period for synchronization
  uint8            cntPeriod;          ///< fractional periods counter for synchronization
  uint8            numPeriod;          ///< fractional periods number for synchronization
  uint8            enableMessages;     ///< messages forming enabled
  
  uint32           CountSync;          ///< counter for synchronization

  int32            CountPostNavTask;   ///< counter navigation task solution

  tCorrectTime     CorrectTime;        ///< structure for correct time

  eFlag            FirstSolution;      ///< first solution flag - fg_ON
  eFlag            nPPSstart;          ///< nPPS signal correct start flag - fg_ON
  eFlag            NavTaskEnd;         ///< navigation task solution end flag - fg_ON
  eFlag            NoNavTaskSol;       ///< no posting navigation task solution flag - fg_ON
  uint8            Synchronization;    ///< synchronization OP and navigation task solution: 0 - no sync, 1 - sync in process, 2 - sync
  uint8            NavSolution;        ///< second navigation solution

} tNavTask;

//==========================================================================
/// @brief     Calculation of SV's elevation and azimuth, User-SV range,
///            radial velocity and directional cosines matrix
///
/// @param[in] ptrNP - pointer to satellite navigation parameters
/// @param[in] User  - pointer to user parameters
/// @param[in] flag - computation flag
///            (0 - elevation is only compute, 1 - all parameters do)
//===========================================================================
EXTERN_C void SV_User_parameters(tNavTaskParams  *ptrNP, 
                                 USER            *User, 
                                 int8             flag);

//===================================================================================
/// @brief     Calculation of basis (unit) vectors for user's local coordinate system
///
/// @param[in] User - pointer to instance
//===================================================================================
EXTERN_C void    ComputeTOPO(USER *User);

//========================================================================
/// @brief     Earth's rotation compensation
///
/// @param[in] NP_out - pointer to satellite coordinates
/// @param[in] NP_inp - pointer to satellite coordinates
/// @param[in] dt      - time of signal propagation from satellite to user
//========================================================================
EXTERN_C void Compensate_Rotation(tCoord *NP_out, tCoord *NP_inp, float64 dt);

//===========================================================================
/// @brief     Compute GPS satellite position from time and almanac data
///
/// @param[in]  navtask_params - pointer to instance
/// @param[in]  Alm            - pointer to GPS satellite almanac
/// @param[in]  wk             - desired time, GPS week number
/// @param[in]  sec            - desired time, seconds of week
//===========================================================================
EXTERN_C void SV_Alm_GPS(tNavTaskParams *navtask_params,
                         ALM_GPS        *Alm,
                         int             wk,
                         double          sec);

//=================================================================================
/// @brief      Copies ephemeris, almanac and ionosphere data
///             from temporary buffers to auxiliary buffers
///
/// @param[in]  TopSys - pointer to Top System parameters
//=================================================================================
EXTERN_C void NavTask_CopyDecodedData(tTopSystem *TopSys);

//=================================================================================
/// @brief      Setting satellite status
///
/// @param[in]  pNavTask - pointer to Navigation task solution object
/// @param[in]  pSatMgr  - pointer to Satellite manager object
//=================================================================================
EXTERN_C void SatellitesStatus(tNavTask *pNavTask, tSatelliteManager *pSatMgr);

//=================================================================================
/// @brief      Navigation task initialization
///
/// @param[in]  pNavTask   - pointer to instance
/// @param[in]  NavSolRate - navigation task solution rate [Hz]
/// @param[in]  OPmode     - OP mode: 0 - only navigation, 1 - navigation and OP
//=================================================================================
EXTERN_C void NavTask_Init(tNavTask *pNavTask, int16 NavSolRate, int8 OPmode);

//=====================================================================================
/// @brief      Navigation task initialization
///
/// @param[in]  pNavTask   - pointer to instance
/// @param[in]  pSatMgr    - pointer to Satellite Manager object
/// @param[in]  weekData   - pointer to week data structure
/// @param[in]  controlWN  - pointer to control week data
/// @param[in]  OrbStatic  - Orbital/Static OP model: 0 - orbital model, 1 - static model
//=====================================================================================
EXTERN_C void NavTask_Process(tNavTask          *pNavTask,
                              tSatelliteManager *pSatMgr,
                              tOPManager        *pOPMgr,
                              tWeekData         *weekData,
                              tControlWN        *controlWN,
                              int8               OrbStatic,
                              eFlag              BinStrFlag,
                              DATA_STRING       *dataStr,
                              tTerminal         *DebugTerminal);

//=================================================================================
/// @brief      Clear User parameters
///
/// @param[in]  pNavTask - pointer to Navigation Task Solution object
/// @param[in]  pSatMgr  - pointer to Satellite manager object
//=================================================================================
EXTERN_C void ClearUser(tNavTask *pNavTask, tSatelliteManager *pSatMgr);

//=======================================================================================
/// @brief      Navigation task initialization
///
/// @param[in]  pNavTask   - pointer to Navigation task object
/// @param[in]  pOPMgr     - pointer to OP manager object
/// @param[in]  pSatMgr    - pointer to Satellite Manager object
/// @param[in]  OrbStatic  - Orbital/Static OP model: 0 - orbital model, 1 - static model
//=======================================================================================
EXTERN_C void ControlOPAndNavigation(tNavTask          *pNavTask, tOPManager *pOPMgr, 
                                     tSatelliteManager *pSatMgr,  int8        OrbStatic,
                                     eFlag              BinStrFlag, DATA_STRING *dataStr,
                                     tTerminal         *DebugTerminal);

#endif  // _NAVTASK_H_
