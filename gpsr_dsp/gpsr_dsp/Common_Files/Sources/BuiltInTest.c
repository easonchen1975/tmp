/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Built-in test
 *   @file                   BuiltInTest.c
 *   @author                 M. Zhokhova, B. Oblakov
 *   @date                   25.08.2006
 *   @version                1.1
 */
/*****************************************************************************/

#include "Framework.h"
#include "BuiltInTest.h"
#include "Hardware.h"
#include "comfuncs.h"
#include "L_sqrt_l.h"

// asm-function: individual interrupt enable
extern int32 asmWriteIER(void);
extern int32 asmWriteIERTestDrv(void);
extern int32 asmIDLE(void);

//=========================================================
/// Writing/reading FLASH testing for
///
/// @param[in]  pThis - pointer to FLASH object
/// @return     1 if error found otherwise 0
//=========================================================
static int TestFLASH(tFlashDrv *pThis);

//==============================================================
/// Writing/reading to UART terminal test
///
/// @param[in]  pThis    - pointer to UART terminal object
/// @param[in]  BaudRate - baud rate UART
/// @param[in]  length   - length block data
/// @param[in]  Timeout  - pointer to timeout flag
/// @return     1 if error found otherwise 0
//==============================================================
static int TestUART(tTerminal *pThis,
                    int8       BaudRate,
                    uint16     length,
                    eFlag     *Timeout);

//============================================================
/// FIFO test
///
/// @param[in]  pSmpMgr - pointer to Sample Manager object
/// @param[in]  Timeout - pointer to timeout flag
/// @return     1 if error found otherwise 0
//============================================================
static int TestFIFO(tSampleManager *pSmpMgr, eFlag *Timeout);

//============================================
/// Generates random data
///
/// @param[in]  rnd - start data
/// @return     new data
//============================================
inline_ int32 next_random_data(int32 rnd)
{
  return ((rnd<<1)|((rnd>>30)^(rnd>>2))&0x1L);
}

//=========================================================
/// Writing/reading FLASH testing
///
/// @param[in]  pThis - pointer to FLASH object
/// @return     1 if error found otherwise 0
//=========================================================
static int TestFLASH(tFlashDrv *pThis)
{
  uint8           res_save = 0,        ///< result of saving
                  flash,               ///< reading data from FLASH
                  array[FL_CNT_TEST];  ///< array for testing
  volatile uint8 *ptrFLASH;            ///< pointer to FLASH
  int             i; 
  int32           rnd;                 ///< beginning data
  uint32          error = 0,           ///< errors counter
                  time,                ///< time from timer
                  addr = pThis->address[FL_TEST];   ///< starting address

  // start value for generator
  rnd = 0xFFFFFFFF;
  for(i=0;i<FL_CNT_TEST;i++)
  {
	array[i] = (rnd&0xff);
    rnd = next_random_data(rnd);
  }

  // start timer
  PROFILE_START();

  FrameWork_FlashProgramming(pThis, addr, FL_CNT_TEST, array);

  addr += (FL_CNT_TEST-1);

  while (res_save == PROCESS)
  {
    res_save = FrameWork_ControlFlashProgramming(addr, array[FL_CNT_TEST-1]);
    time = PROFILE_READ();
	if (time > 9600000)  // 20 ms
	{
	  PROFILE_STOP();
	  return 1;  /// error
	}
  }
  PROFILE_STOP();

  ptrFLASH = (uint8*)pThis->address[FL_TEST];

  // read random data from FLASH
  for(i=0;i<FL_CNT_TEST;i++)
  {
    flash = *(uint8*)ptrFLASH++;
    if(flash != array[i])
    {
      error++;
    } 
  }
  if (error != 0)
    return 1;
  else
    return 0;
}

//==============================================================
/// Writing/reading to UART terminal test
///
/// @param[in]  pThis    - pointer to UART terminal object
/// @param[in]  BaudRate - baud rate UART
/// @param[in]  length   - length block data
/// @param[in]  Timeout  - pointer to timeout flag
/// @return     1 if error found otherwise 0
//==============================================================
static int TestUART(tTerminal *pThis,
                    int8       BaudRate,
                    uint16     length,
                    eFlag     *Timeout)
{
  int    i;
  uint8  number;
  uint16 error         = 0,
         len           = 0;
  uint32 time          = 0;
  uint16 UART_BoudRate = 0;
  uint32 baudRate      = 0;

  
  // UART initialization: Reset
//  *(volatile uint16 *)0x68000008 = 0x0040;
  *(volatile uint16 *)0x68000024 = 0x0004; // v703- disUART
  *(volatile uint16 *)0x68000024 = 0x0007; // v703- enUART, echoMode, data=8bits, stop=1bit, None parity, LSB first
  delay_mcs(100);

  /// UART_CNTRL: Enable test mode
//  *(volatile uint16 *)0x68000008 = 0x0020;
//  delay_mcs(100);

  /// Baud_rate = 20e+6/baudRate
  if(BaudRate==0x0)
  {
    baudRate      = 4800;
	//UART_BoudRate = 0x1046;
	UART_BoudRate = 0x207;	// v.703- setBoudRate
  }
  if(BaudRate==0x1)
  {
    baudRate      = 9600;
	//UART_BoudRate = 0x823;
	UART_BoudRate = 0x103;	// v.703- setBoudRate
  }
  if(BaudRate==0x2)
  {
    baudRate      = 19200;
	//UART_BoudRate = 0x411;
	UART_BoudRate = 0x81;	// v.703- setBoudRate
  }
  if(BaudRate==0x3)
  {
    baudRate      = 38400;
	//UART_BoudRate = 0x208;
	UART_BoudRate = 0x40;	// v.703- setBoudRate
  }
  if(BaudRate==0x4)
  {
    baudRate      = 57600;
	//UART_BoudRate = 0x160;
	UART_BoudRate = 0x2A;	// v.703- setBoudRate
  }
  if(BaudRate==0x5)
  {
    baudRate      = 115200;
	//UART_BoudRate = 0x00AD;
	UART_BoudRate = 0x15;	// v.703- setBoudRate
  }
  //*(volatile uint16 *)0x6800000A = UART_BoudRate;
  *(volatile uint16 *)0x68000020 = UART_BoudRate; // v.703- setBoudRate
  delay_mcs(100);

  // interrupt Flags clearing
  asmWriteICR();
  // global interrupt enable
  asmWriteCSR();
  // individual interrupt enable
  asmWriteIERTestDrv();
  delay_mcs(100);

  // Global HWI enable
  HWI_enable();

  // timer start
  PROFILE_START();
  // save data in FIFO UART
//  for(i=0; i<length; i++) *(volatile uint8 *)0x68000040 = i;
  for(i=0; i<length; i++) *(volatile uint16 *)0x68000022 = i; // v.703 - data from TBR/RBR

  // control timer start: if interrupt does not more than 50 ms then error UART terminal work
  Timer0Start(18000000);  // tacts: 300ms*480000/8  3000000

  // loop while does not interrupt
  while((*Timeout == fg_OFF) && (len != length))
  {
//	len = *(volatile uint16 *)0x68000010;  /// check number received bytes
	len = *(volatile uint16 *)0x68000028;  /// v.703 - data from RCR
  }

  // timer stop
  time = PROFILE_STOP();

  // control timer stop
  Timer0Stop();

  // if interrupt from control timer then error
  if (*Timeout == fg_ON)
  {
    *Timeout = fg_OFF;
    return 1;
  }

  // Global HWI disable
  HWI_disable();

  // reading and control data from FIFO UART terminal
  for(i=0; i<length; i++) 
  {
//    number = *(volatile uint8 *)0x68000040;
	number = *(volatile uint16 *)0x68000022; // v.703 - data from TBR/RBR
    if (number != i)
      error++;
  }
  // if data is correct
  if (error==0)
  {
    double tmp;
    // number bits during ms
    tmp = (double)length*10*1.0e+3;
    // baudRate calculation
    tmp = tmp/((double)time/480000.0);
    tmp = (tmp - (double)baudRate)*100.0/(double)baudRate;  // % from baudRate_calc and baudRate_given
    if (fabs(tmp) <= 5.0)
      return 0;
  }
  return 1;
}

//===========================================================
/// FIFO test
///
/// @param[in]  pSmpMgr - pointer to Sample Manager object
/// @param[in]  Timeout - pointer to timeout flag
/// @return     1 if error found otherwise 0
//===========================================================
static int TestFIFO(tSampleManager *pSmpMgr, eFlag *Timeout)
{
  uint8  count = 0;
  int    error = 0,
         i,
         k;
  uint16 last,
         current,
         next;
  uint8 overRunErr=0;	//v.703 - Test Code         

  /// GPIO initialization
  *(volatile int32 *)0x01b00000   = 0x10;  /// GPIO 4 enable
  /// Event Clearing
  *(volatile int32 *)0x01a0fff8   = 0x10;  /// ECRL
  /// Event Polarity
  *(volatile int32 *)0x01a0ffdc   = 0x00;  /// EPRL
  
  /// RFFE setting
  /// Register 0
  //*(volatile uint16 *)0x68000020 = 0x1a30; //0x0010;  //LSB
  *(volatile uint16 *)0x68000004 = 0x1a30;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0xa293;  /// MSB
  *(volatile uint16 *)0x68000006 = 0xa293;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 1
  //*(volatile uint16 *)0x68000020 = 0x0881; //0x2881;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0881;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x8550;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x8550;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 2
  //*(volatile uint16 *)0x68000020 = 0x1dc2;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x1dc2;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0xeadf;  /// MSB
  *(volatile uint16 *)0x68000006 = 0xeadf;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 3
  //*(volatile uint16 *)0x68000020 = 0x0083;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0083;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x9ec0;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x9ec0;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 4
  //*(volatile uint16 *)0x68000020 = 0x0804;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0804;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x0c00; //0x0c08;  ///MSB
  *(volatile uint16 *)0x68000006 = 0x0c00;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 5
  //*(volatile uint16 *)0x68000020 = 0x0705;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0705;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x8000;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x8000;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 6
  //*(volatile uint16 *)0x68000020 = 0x0006;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x0006;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x8000;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x8000;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 7
  //*(volatile uint16 *)0x68000020 = 0x1b47;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x1b47;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x1006;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x1006;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 8
  //*(volatile uint16 *)0x68000020 = 0x4018;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x4018;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x1e0f;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x1e0f;  /// v.703 -  MSB
  delay_mcs(200);
  /// Register 9
  //*(volatile uint16 *)0x68000020 = 0x4029;  /// LSB
  *(volatile uint16 *)0x68000004 = 0x4029;  /// v.703 -  LSB
  delay_mcs(10);
  //*(volatile uint16 *)0x68000022 = 0x14c0;  /// MSB
  *(volatile uint16 *)0x68000006 = 0x14c0;  /// v.703 -  MSB 
  delay_mcs(2000);

  delay_mcs(100);
  /// Interrupt Flags clearing
  asmWriteICR();
  delay_mcs(100);
  /// Individual interrupt enable
  asmWriteIER();
  delay_mcs(100);

  /// Interrupt enable
  *(volatile int32 *)0x01a0ffe8 = 0x100;  /// Channel interrupt enable low register

  /// even ch0
  /// EDMA channel 4 
  *(volatile int32 *)0x01a00060 = 0x08380003;                      /// Ch4 Options       
  *(volatile int32 *)0x01a00064 = 0x60000000;                      /// Ch4 SRC Address
  *(volatile int32 *)0x01a00068 = 0x002B005D;                      /// Ch4 Frame count - Element count
  *(volatile int32 *)0x01a0006c = (int32)pSmpMgr->pPackedBuff[0];  /// Ch4 DST Address
  *(volatile int32 *)0x01a00070 = 0x0;                             /// Ch4 Frame index - Element index
  *(volatile int32 *)0x01a00074 = 0x00000600;                      /// Ch4 Element count reload - Link Address

  /// EDMA link channel 4
  *(volatile int32 *)0x01a00600 = 0x08380003;                      /// Ch4 Options
  *(volatile int32 *)0x01a00604 = 0x60000000;                      /// Ch4 SRC Address
  *(volatile int32 *)0x01a00608 = 0x002B005D;                      /// Ch4 Frame count - Element count
  *(volatile int32 *)0x01a0060c = (int32)pSmpMgr->pPackedBuff[1];  /// Ch4 DST Address
  *(volatile int32 *)0x01a00610 = 0x0;                             /// Ch4 Frame index - Element index
  *(volatile int32 *)0x01a00614 = 0x00000618;                      /// Ch4 Element count reload - Link Address

  /// EDMA link channel 4
  *(volatile int32 *)0x01a00618 = 0x08380003;                      /// Ch4 Options
  *(volatile int32 *)0x01a0061c = 0x60000000;                      /// Ch4 SRC Address
  *(volatile int32 *)0x01a00620 = 0x002B005D;                      /// Ch4 Frame count - Element count
  *(volatile int32 *)0x01a00624 = (int32)pSmpMgr->pPackedBuff[0];  /// Ch4 DST Address
  *(volatile int32 *)0x01a00628 = 0x0;                             /// Ch4 Frame index - Element index
  *(volatile int32 *)0x01a0062c = 0x00000600;                      /// Ch4 Element count reload - Link Address

  overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code
  //*(volatile uint8 *)0x68000000 = 0x1;  /// reset FIFO	
  *(volatile uint8 *)0x68000002 = 0x2;  //v.703 - flushFIFO
  overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code
  delay_mcs(10);
  //*(volatile uint8 *)0x68000000 = 0x0;  ///
  *(volatile uint8 *)0x68000002 = 0x0;  //v.703 - disableFIFO
  overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code
  //delay_mcs(10);

  /// Start EDMA
  *(volatile int32 *)0x01a0fff4 = 0x00000010;  /// Event enable low register
  delay_mcs(10);
  /// Start FIFO
  //*(volatile uint8 *)0x68000000 = 0x2 | 0x4;   ///  mux enable | FIFO test mode
  *(volatile uint8 *)0x68000002 = 0x5;   //v.703 - enableFIFO & TestPattern
  //*(volatile uint8 *)0x68000002 = 0x1;   //v.703 - enableFIFO
  overRunErr = *(volatile uint8 *)0x68000002;	//v.703 - Test Code

  // Global HWI enable
  HWI_enable();

  while(1)
  {
    // control timer start: if interrupt does not more than 2 ms then error FIFO work
    Timer0Start(120000);  // tact: 2ms*480000/8

    // IDLE loop while does not interrupt
    asmIDLE();

    // control timer stop
    Timer0Stop();

    // if interrupt from control timer then error
    if (*Timeout == fg_ON)
    {
      *Timeout = fg_OFF;
      return 1;
    }

    /// first or second buffer with data
    i = (count & 0x1);
    if (count != 0)
    {
      /// last value in previous buffer 65535 and first value 0
      if (last == 65535)
      {
#ifdef EXT_SRAM
        if (pSmpMgr->PackedBuff->s[i][0] != 0)
#else	  
        if (pSmpMgr->PackedBuff.s[i][0] != 0)
#endif		
          error = 1;
      }
      else 
      {
        /// next value equal last value plus 1
#ifdef EXT_SRAM
       if ((uint16)pSmpMgr->PackedBuff->s[i][0] != (last+1))
#else	  
        if ((uint16)pSmpMgr->PackedBuff.s[i][0] != (last+1))
#endif		
          error = 1;
      }
      /// data in buffer test: next value equal last value plus 1
      for(k=0;k<4091;k++)
      {
#ifdef EXT_SRAM
        current = pSmpMgr->PackedBuff->s[i][k];
        next = pSmpMgr->PackedBuff->s[i][k+1];
#else	  
        current = pSmpMgr->PackedBuff.s[i][k];
        next = pSmpMgr->PackedBuff.s[i][k+1];
#endif		
        /// if previous value equal 65535 then next value is zero
        if (current == 65535)
        {
          if (next != 0)
            error = 1;
        }
        else
        {
          if (current != (next-1))
            error = 1;
        }
      }
    }
    /// last value save
#ifdef EXT_SRAM
    last = pSmpMgr->PackedBuff->s[i][4091];
#else	  
    last = pSmpMgr->PackedBuff.s[i][4091];
#endif		
    // if error found then end process
    if (error == 1)
      break;  
    else
    {
      // increment ms counter
      count++;
      // test time is 10 ms
      if (count == 10)
        break;
    }
  }
 
  // stop EDMA: Event disable low register
  *(volatile int32 *)0x01a0fff4 = 0x00000000;
  // Global HWI disable
  HWI_disable();
  // if found error then return 1
  if (error == 1)
    return 1;
  else
    return 0;
}

//==================================================================
/// Built-in test: short/long
///
/// @param[in]  pSmpMgr     - pointer to Sample Manager object
/// @param[in]  pFramework  - pointer to Framework object
/// @param[in]  resetBIT    - BIT after software reset flag
/// @param[in]  oldBIT      - data old BIT
//==================================================================
void FrameWork_BuiltInTest(tSampleManager *pSmpMgr,
                           tFramework     *pFramework,
                           uint8           resetBIT,
                           tBuiltInTest    oldBIT)
{
  uint8        *dst,
               *src;
  uint8         error_UART = 0;
  tBuiltInTest *pThis      = &pFramework->BuiltInTest;

  pFramework->BITFlag  = fg_ON;
  pFramework->TCXOtest = fg_OFF;
  pFramework->TCXOerr  = fg_OFF;
 
  // if BIT after hardware reset
  if (resetBIT == 0)
  {
    uint32 chs_sys_boot, chs_sys_flash, address;
    uint16 tmp2;
    uint32 tmp4;

    // for System file
    address = pFramework->FlashDrv.address[FL_SYSTEM];
    pThis->daySystem   = *(uint8*)address; address++;
    pThis->monthSystem = *(uint8*)address; address++;
    dst = (uint8*)&tmp2;
    src = (uint8*)address;
    COPYINT(dst, src, 2);
	address += 2;
    pThis->yearSystem = tmp2;
    COPYINT(dst, src, 2);
	address += 2;
    pThis->versionSystem = tmp2;
    COPYINT(dst, src, 2);
	address += 2;
    pThis->subVersionSystem = tmp2;
    dst = (uint8*)&tmp4;
    COPYINT(dst, src, 4);
	address += 4;
    pThis->sizeSystem = tmp4;

    address += pThis->sizeSystem;
    // read check sum System file that bootloader calculation
    chs_sys_boot = *(uint32*)LAST_WORD;
    dst = (uint8*)&chs_sys_flash;
    src = (uint8*)address;
    // read check sum System file from FLASH
    COPYINT(dst, src, CHECK_SUM_SYSTEM);
    // if check sums is not equal then error: bit 0: System SW check sum flag
    if (chs_sys_boot != chs_sys_flash)
      pThis->statusFlags |= SYS_CHSUM;
    // check sum System file save
    pThis->checkSumSystem = chs_sys_flash;
  }
  else  // if BIT after software reset
  {
    // copying data about System files
    pThis->daySystem         = oldBIT.daySystem;
    pThis->monthSystem       = oldBIT.monthSystem;
    pThis->yearSystem        = oldBIT.yearSystem;
    pThis->versionSystem     = oldBIT.versionSystem;
    pThis->subVersionSystem  = oldBIT.subVersionSystem;
    pThis->checkSumSystem    = oldBIT.checkSumSystem;
    pThis->sizeSystem        = oldBIT.sizeSystem;
    pThis->statusFlags      |= (oldBIT.statusFlags&SYS_CHSUM);
    pThis->statusFlags      |= (oldBIT.statusFlags&CONFIG_CHSUM);
  }

  // bit 2: FLASH write/read flag
  if (TestFLASH(&pFramework->FlashDrv) == 1)
    pThis->statusFlags |= FLASH_WR;    

  // bit 6: FIFO module flag
  // set TCXO test flag
  pFramework->TCXOtest = fg_ON;
  if (TestFIFO(pSmpMgr, &pFramework->Timeout) == 1)
    pThis->statusFlags |= FIFO_MOD;
  // clear TCXO test and start TCXO test flags
  pFramework->TCXOtest      = fg_OFF;
  pFramework->StartTestTCXO = 0;
  // if TCXO error flag is set then error: bit 7: TCXO flag
  if (pFramework->TCXOerr == fg_ON)
    pThis->statusFlags |= TCXO;

  // if function returned 1 then counter error incremented
  // baud rate - 4800, length data 127
  if (TestUART(&pFramework->DebugTerminal, 0, 127, &pFramework->Timeout) == 1)
    error_UART++;
  // baud rate - 9600, length data 127
  if (TestUART(&pFramework->DebugTerminal, 1, 127, &pFramework->Timeout) == 1)
    error_UART++;
  // baud rate - 19200, length data 127
  if (TestUART(&pFramework->DebugTerminal, 2, 127, &pFramework->Timeout) == 1)
    error_UART++;
  // baud rate - 38400, length data 127
  if (TestUART(&pFramework->DebugTerminal, 3, 127, &pFramework->Timeout) == 1)
    error_UART++;
  // baud rate - 57600, length data 127
  if (TestUART(&pFramework->DebugTerminal, 4, 127, &pFramework->Timeout) == 1)
    error_UART++;
  // baud rate - 115200, length data 127
  if (TestUART(&pFramework->DebugTerminal, 5, 127, &pFramework->Timeout) == 1)
    error_UART++;
  // if error counter nonzero then error: bit 3: UART module flag
  if (error_UART != 0)
    pThis->statusFlags |= UART_MOD;

  // clear BIT flag
  pFramework->BITFlag = fg_OFF;
}



