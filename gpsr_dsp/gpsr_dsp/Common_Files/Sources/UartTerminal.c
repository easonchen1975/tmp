/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  UART terminal driver implementation
 *   @file                   UartTerminal.c
 *   @author                 V. Yeremeyev
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*   External Subroutines:
 *      TerminalInit()          - Initialize Terminal driver
 *      Terminal_Read()         - Read char string from console
 *      Terminal_Write()        - Write char string to console
 *      Terminal_ReadBinaty()   - Read char binary buffer from console
 *      Terminal_WriteBinary()  - Write char binary buffer to console
 *
 *   Internal Subroutines:
 *      InitializeUartDriver()  - Actual driver initialization
 *
 *****************************************************************************/

#include "Framework.h"
#include "UartTerminal.h"
#include "baseop.h"
#include "Hardware.h"

//=====================================================================================
/// @brief      Terminal driver software initialization
///
/// @param[in]  pTerminal - pointer to terminal instance
//=====================================================================================
static void InitializeUartDriver(tTerminal *pTerminal);

//=====================================================================================
/// @brief   Initialize UART Controller (baud, stop bits, etc.)
//
/// @param[in]  pTerminal      - pointer to instance
/// @param[in]  UART_BoudRate  - baud rate code
//=====================================================================================
static void InitializeUartController(tTerminal *pTerminal, int16 UART_BoudRate);

//=====================================================================================
/// @brief      Terminal driver initialization
///
/// @param[in]  pTerminal - pointer to terminal instance
/// @param[in]  BaudRate  - baud rate and parity mode
//=====================================================================================
void UartTerminalInit(tTerminal *pTerminal, int8 BaudRate)
{
  int16 UART_BoudRate;
  InitializeUartDriver(pTerminal);

  /// Baud_rate, 20e+6/115200
  if ((BaudRate&0x7) == 0)
  {
    pTerminal->ParcelOfData = 1;   // 4800/10/1000 = 0.48 number of byte in one ms
    //UART_BoudRate = 0x1046;
    UART_BoudRate = 0x207;	// v.703- setBoudRate
  }
  if ((BaudRate&0x7) == 1)
  {
    pTerminal->ParcelOfData = 2;   // 9600/10/1000 = 0.96 number of byte in one ms
    //UART_BoudRate = 0x823;
    UART_BoudRate = 0x103;	// v.703- setBoudRate
  }
  if ((BaudRate&0x7) == 2)
  {
    pTerminal->ParcelOfData = 4;   // 19200/10/1000 = 1.92
    //UART_BoudRate = 0x411;
    UART_BoudRate = 0x81;	// v.703- setBoudRate
  }
  if ((BaudRate&0x7) == 3) 
  {
    pTerminal->ParcelOfData = 8;   // 38400/10/1000 = 3.84
    //UART_BoudRate = 0x208;
    UART_BoudRate = 0x40;	// v.703- setBoudRate
  }
  if ((BaudRate&0x7) == 4)
  {
    pTerminal->ParcelOfData = 12;  // 56700/10/1000 = 5.67
    //UART_BoudRate = 0x160;
    UART_BoudRate = 0x2A;	// v.703- setBoudRate
  }
  if ((BaudRate&0x7) == 5)
  {
    pTerminal->ParcelOfData = 24;  // 115200/10/1000 = 11.52

#ifdef CAN_PORT_TMTC
// Henry's new FPGA for CAN
    UART_BoudRate = 15;	// v.703- setBoudRate - 30MHz
#else
    UART_BoudRate = 0x15;	// v.703- setBoudRate - 40MHz	
#endif	
//    UART_BoudRate = 0x1A;	// v.703- setBoudRate - 50MHz
  }

  //UART_BoudRate = 15;	// v.703- setBoudRate - 30MHz

  InitializeUartController(pTerminal, UART_BoudRate);  
}

//=====================================================================================
/// @brief      Terminal driver software initialization
///
/// @param[in]  pTerminal - pointer to terminal instance
//=====================================================================================
static void InitializeUartDriver(tTerminal *pTerminal)
{
  static const tTerminalDrv vtable =
  {
    UartRead,           // pfnRead 
    UartWrite,          // pfnWrite
    UartReadBinary,     // pfnReadBinary
    UartWriteBinary     // pfnWriteBinary
  };

  pTerminal->vtable = vtable;
  pTerminal->FlagUART = fg_OFF;

  FIFO_Init(
    &pTerminal->ReceiveQueue,
    (tFifoItem *)pTerminal->m_ReadBuffer,
    sizeof(pTerminal->m_ReadBuffer));

  FIFO_Init(
    &pTerminal->TransmitQueue,
    (tFifoItem *)pTerminal->m_WriteBuffer,
    sizeof(pTerminal->m_WriteBuffer));
}

//=====================================================================================
/// @brief     Read char string from console
//
/// @param[in]   pTerminal  - pointer to instance
/// @param[out]  str        - pointer to string read from console
/// @param[in]   MaxLen     - maximum string length
/// @return      operation status
//=====================================================================================
tIOstat UartRead(tTerminalDrv *pTerminal, char *str, int MaxLen)
{
  tIOstat     status;

  status = TRead(pTerminal, str, MaxLen);
  return status;
}

//=====================================================================================
/// @brief     Read binary buffer from console
//
/// @param[in]   pTerminal  - pointer to instance
/// @param[out]  pBuffer    - pointer to buffer read from console
/// @param[in]   Count      - number of bytes to be read
/// @return      operation status
//=====================================================================================
uint16 UartReadBinary(tTerminalDrv *pTerminal, uint8 *pBuffer, uint16 Count)
{
  uint16      res;

  res = TReadBinary(pTerminal, pBuffer, Count);
  return res;
}

//=====================================================================================
/// @brief     Write char string in console
//
/// @param[in]  pTerminal  - pointer to instance
/// @param[in]  str        - pointer to string being written in console
/// @param[in]  MaxLen     - maximum string length
/// @return     operation status
//=====================================================================================
tIOstat UartWrite(tTerminalDrv *pTerminal, const char *str, int len)
{
  tIOstat  status;
  
  FIFO_GetFilledSize(&((tTerminal *)pTerminal)->TransmitQueue);
  status = TWrite(pTerminal, str, len);
  
  return status;
}

//=====================================================================================
/// @brief     Write char string in console
//
/// @param[in]  pTerminal  - pointer to instance
/// @param[in]  pBuffer    - pointer to buffer being written in console
/// @param[in]  Count      - number of bytes to be write
/// @return     operation status
//=====================================================================================
uint16 UartWriteBinary(tTerminalDrv *pTerminal, const uint8 *pBuffer, uint16 Count)
{
  uint16 res = UartWrite(pTerminal, (const char *)pBuffer, (int)Count);
  return res;
}

//=====================================================================================
/// @brief   Initialize UART Controller (baud, stop bits, etc.)
//
/// @param[in]  pTerminal      - pointer to instance
/// @param[in]  UART_BoudRate  - baud rate code
//=====================================================================================
static void InitializeUartController(tTerminal *pTerminal, int16 UART_BoudRate)
{
#ifdef USE_DSPBIOS
  /// UART reset
//  *(volatile uint16 *)0x68000008 = 0x0040;
  *(volatile uint16 *)0x68000024 = 0x0004; // v703- disUART
  *(volatile uint16 *)0x68000024 = 0x0005; // v703- enUART
  delay_mcs(5000);

  /// UART_CNTRL
//  *(volatile uint16 *)0x68000008 = 0x000a;
//  delay_mcs(5000);
  /// Baud_rate, 20e+6/115200
//  *(volatile uint16 *)0x6800000A = UART_BoudRate;
  
  *(volatile uint16 *)0x68000020 = UART_BoudRate; // v.703- setBoudRate
  
#endif
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (OnDataTransmitted, "fast_text")
#endif

//=====================================================================================
/// @brief   Data transmit notification function
//
/// @param[in]  pTerminal - pointer to instance
//=====================================================================================
void OnDataTransmitted(tTerminal *pTerminal)
{
  //static uint16 size;
	static int16 size;
  uint8 fifo_fill = 0;
//  fifo_fill = *(volatile uint8 *)0x6800000e;
  fifo_fill = *(volatile uint8 *)0x6800002A; // v.703 - data from TCR
  size = FIFO_GetFilledSize(&pTerminal->TransmitQueue);

  if(fifo_fill > (250-24*2)){
	  fifo_fill = 0;
	  return;
  }
  //=== Error checking
  if( (size > 0)){ // &&  fifo_fill
	  if (size > (127-fifo_fill))
		size = (127-fifo_fill);
  }
  if(size > 0)
  {
    if (size > pTerminal->ParcelOfData)
      size = pTerminal->ParcelOfData;
#ifdef EXT_SRAM
    FIFO_GetBuffer(&pTerminal->TransmitQueue, (tFifoItem *)pTerminal->stringWrite->str, size);
#else
    FIFO_GetBuffer(&pTerminal->TransmitQueue, (tFifoItem *)pTerminal->stringWrite.str, size);
#endif	

    /// Event Clearing
    *(int32 *)0x01a0fff8         |= 0x100;                                 /// ECRL

    /// Chaining enable
    *(volatile int32 *)0x01a0ffec |= 0x100;                                 /// Chaining enable low register

    /// EDMA channel 8
    *(volatile int32 *)0x01a000c0 = 0x71000003;                             /// Ch10 Options  0x13390003
#ifdef EXT_SRAM
    *(volatile int32 *)0x01a000c4 = (int32)&pTerminal->stringWrite->str[0];  /// Ch10 SRC Address
#else
    *(volatile int32 *)0x01a000c4 = (int32)&pTerminal->stringWrite.str[0];  /// Ch10 SRC Address
#endif	
    *(volatile int32 *)0x01a000c8 = size;                                   /// Ch10 Frame count - Element count
//    *(volatile int32 *)0x01a000cc = 0x68000040;                             /// Ch10 DST Address
	*(volatile int32 *)0x01a000cc = 0x68000022; // v.703 - data from TBR/RBR
    *(volatile int32 *)0x01a000d0 = 0x0;                                    /// Ch10 Frame index - Element index
    *(volatile int32 *)0x01a000d4 = 0x000007e0;                             /// Ch10 Element count reload - Link Address

    /// EDMA link channel
    *(volatile int32 *)0x01a007e0 = 0x0;                                    /// Ch8 Options
    *(volatile int32 *)0x01a007e4 = 0x0;                                    /// Ch8 SRC Address
    *(volatile int32 *)0x01a007e8 = 0x0;                                    /// Ch8 Frame count - Element count
    *(volatile int32 *)0x01a007ec = 0x0;                                    /// Ch8 DST Address
    *(volatile int32 *)0x01a007f0 = 0x0;                                    /// Ch8 Frame index - Element index
    *(volatile int32 *)0x01a007f4 = 0x0;                                    /// Ch8 Element count reload - Link Address

    /// Start EDMA
    *(volatile int32 *)0x01a0fff4 |= 0x00000100;                            /// Event enable low register
  }
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (OnDataReceived, "fast_text")
#endif

//=====================================================================================
/// @brief   Data receive notification function
//
/// @param[in]   pTerminal - pointer to instance
/// @param[out]  flag      - pointer result flag
//=====================================================================================
void OnDataReceived(tTerminal *pTerminal, uint8 *flag)
{
  static uint8 fifo_fill;
  static uint8 flag_set = TERMINAL_READ;
  if(FIFO_GetFreeSize(&pTerminal->ReceiveQueue) > 0)
  {
//    fifo_fill = *(volatile uint8 *)0x68000010;
	fifo_fill = *(volatile uint8 *)0x68000028; // v.703 - data from RCR
    if (fifo_fill != 0)
    {
#ifdef EXT_SRAM
      pTerminal->stringRead->length = fifo_fill;
#else
      pTerminal->stringRead.length = fifo_fill;
#endif	  

      /// Event Clearing
      *(int32 *)0x01a0fff8          |= 0x300;                                /// ECRL

      /// Chaining enable
      *(volatile int32 *)0x01a0ffec |= 0x300;                                /// Chaining enable low register

      /// EDMA channel 8
      *(volatile int32 *)0x01a000c0 = 0x50390003;                            /// Ch0 Options  
//      *(volatile int32 *)0x01a000c4 = 0x68000040;                            /// Ch0 SRC Address
	  *(volatile int32 *)0x01a000c4 = 0x68000022;  // v.703 - data from TBR/RBR                          
      *(volatile int32 *)0x01a000c8 = fifo_fill;                             /// Ch0 Frame count - Element count
#ifdef EXT_SRAM
      *(volatile int32 *)0x01a000cc = (int32)&pTerminal->stringRead->str[0];  /// Ch0 DST Address
#else
      *(volatile int32 *)0x01a000cc = (int32)&pTerminal->stringRead.str[0];  /// Ch0 DST Address
#endif	  
      *(volatile int32 *)0x01a000d0 = 0x0;                                   /// Ch0 Frame index - Element index
      *(volatile int32 *)0x01a000d4 = 0x000007e0;                            /// Ch0 Element count reload - Link Address

      /// EDMA channel 9
      *(volatile int32 *)0x01a000d8 = 0x50000003;                            /// Ch11 Options  
      *(volatile int32 *)0x01a000dc = (int32)&flag_set;                      /// Ch11 SRC Address
      *(volatile int32 *)0x01a000e0 = 1;                                     /// Ch11 Frame count - Element count
#ifdef EXT_SRAM
      *(volatile int32 *)0x01a000e4 = *((int32*)flag);                           /// Ch11 DST Address
#else
      *(volatile int32 *)0x01a000e4 = (int32)flag;                           /// Ch11 DST Address
#endif	  
      *(volatile int32 *)0x01a000e8 = 0x0;                                   /// Ch11 Frame index - Element index
      *(volatile int32 *)0x01a000ec = 0x000007e0;                            /// Ch11 Element count reload - Link Address

      /// EDMA link channel
      *(volatile int32 *)0x01a007e0 = 0x0;                                   /// Ch10 Options
      *(volatile int32 *)0x01a007e4 = 0x0;                                   /// Ch10 SRC Address
      *(volatile int32 *)0x01a007e8 = 0x0;                                   /// Ch10 Frame count - Element count
      *(volatile int32 *)0x01a007ec = 0x0;                                   /// Ch10 DST Address
      *(volatile int32 *)0x01a007f0 = 0x0;                                   /// Ch10 Frame index - Element index
      *(volatile int32 *)0x01a007f4 = 0x0;                                   /// Ch10 Element count reload - Link Address

      /// Start EDMA
      *(volatile int32 *)0x01a0fff4 |= 0x00000300;                           /// Event enable low register
    }
  }
}

