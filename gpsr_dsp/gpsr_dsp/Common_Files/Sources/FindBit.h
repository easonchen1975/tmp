/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  PLL initialize
 *   @file                   FindBit.c
 *   @author                 A. Nazarov
 *   @date                   26.07.2005
 *   @version                1.0
 */
/*****************************************************************************/

#ifndef FindBound_H__
#define FindBound_H__

#include "comtypes.h"

// result structure
typedef struct
{
  int    bitBound;  ///< bit boundary
  int32  freq;      ///< frequency estimation in Q16
}
tFindBitResult;

//=============================================================================
/// Search for bit bound and Doppler frequency estimation
/// 
/// @param[out] result - pointer to result structure
/// @param[in]  pCorr  - pointer to 32-points signal spectrum
/// @param[in]  M      - number of nonzero samples in pCorr
//=============================================================================
EXTERN_C void FindBit (tFindBitResult* result, const tComplex32* pCorr, int M);

#endif  // FindBound_H__
