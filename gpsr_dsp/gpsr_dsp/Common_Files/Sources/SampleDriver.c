/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Sample driver interface implementation
 *   @file                   SampleDriver.c
 *   @author                 M. Zhokhova
 *   @date                   16.02.2006
 *   @version                3.0
 */
/*****************************************************************************/

#include <string.h>
#include "SampleDriver.h"

#ifdef SMPL_DRV_FORCE_FILE_INPUT
  #define SAMPLES_FILE_NAME "c:\\Share\\TestVectors\\Sim_orb_42\\smp.pcm" // "c:\\Zhokhova\\Nspo\\Src\\SaveSamples\\corr_test\\Debug\\sam_15_29.dat" // sam_11_27.dat" // sam_11_06.dat" // sam_18_34.dat" // 
#endif

//============================================================================================
///  Unpack input samples into channel
///
/// @param[in]  packed_signal   - pointer to input packed buffer
/// @param[out] unpack_signal1  - pointer to even buffer for channel
/// @param[out] unpack_signal2  - pointer to odd buffer for channel
//============================================================================================
static void SampleUnpack(uint32    * restrict packed_signal,
                         tSamples  * restrict unpack_signal1,
                         tSamples  * restrict unpack_signal2);

//============================================================================================
///  Put samples into input buffer of fast search algorithm
///
/// @param[in] pThis - pointer to instance
//============================================================================================
static void UpdateFastSearchBuf(tSampleManager *pThis);

//============================================================================================
/// Unpack samples and put then into unpacked buffer
///
/// @param[in]  pThis - pointer to instance
//============================================================================================
static void SmpMgr_PutSamples(tSampleManager *pThis);

//============================================================================================
///  Function shift input samples and overwrite old buffer
///
/// @param[in] pDest - pointer to samples buffer
//============================================================================================
static void ShiftEpoch(tDoubleWord* restrict pDest);

//============================================================================================
/// Sample driver initialization
///
/// @param[in] pSampleDrv - pointer to instance
/// @param[in] script     - file name for input samples of PC model
//============================================================================================
void SmpMgr_Init(tSampleManager *pThis, const char *script)
{
#ifdef SMPL_DRV_FORCE_FILE_INPUT
  pThis->script = fopen((script != 0) ? script : SAMPLES_FILE_NAME, "rb"); // open input file
  ASSERT("Unable to open script file" && pThis->script);  
#endif
  /// common initialization
  pThis->bufIndex       = 0;
#ifdef EXT_SRAM
  pThis->pPackedBuff[0] = &pThis->PackedBuff->s[0][0];
  pThis->pPackedBuff[1] = &pThis->PackedBuff->s[1][0];
#else  
  pThis->pPackedBuff[0] = &pThis->PackedBuff.s[0][0];
  pThis->pPackedBuff[1] = &pThis->PackedBuff.s[1][0];
#endif  
  pThis->SamplesOld     = fg_ON;
  pThis->out_samples    = OUT_SAMPLES;
}

#ifndef USE_DSPBIOS
//============================================================================================
///  Unpack input samples into channel
///
/// @param[in]  packed_signal   - pointer to input packed buffer
/// @param[out] unpack_signal1  - pointer to even buffer for channel
/// @param[out] unpack_signal2  - pointer to odd buffer for channel
//============================================================================================
static void SampleUnpack(uint32    * restrict packed_signal,
                         tSamples  * restrict unpack_signal1,
                         tSamples  * restrict unpack_signal2)
{
  uint32 pack_signal;
  int32  i;
  int32  c0, s1, c2, s3, c4, s5, c6, s7;

  pack_signal = *packed_signal++;
  c0 = _EXTU(pack_signal, 28,28);
  s1 = _EXTU(pack_signal, 24,28);
  c2 = _EXTU(pack_signal, 20,28);
  s3 = _EXTU(pack_signal, 16,28);
  c4 = _EXTU(pack_signal, 12,28);
  s5 = _EXTU(pack_signal, 8, 28);
  c6 = _EXTU(pack_signal, 4, 28);
  s7 = _EXTU(pack_signal, 0, 28);

  *(int16*)(unpack_signal1++) = (int16)_PACKL4(_PACK2(s3 - s1, c0 - c2),_PACK2(s3 - s1, c0 - c2));
  *(int16*)(unpack_signal2++) = (int16)_PACKL4(_PACK2(s3 - s5, c4 - c2),_PACK2(s3 - s5, c4 - c2));
  *(int16*)(unpack_signal1++) = (int16)_PACKL4(_PACK2(s7 - s5, c4 - c6),_PACK2(s7 - s5, c4 - c6));

  #ifdef _TMS320C6X
    #pragma UNROLL(2)
  #endif

  for(i=0; i<2045; i++)
  {
    pack_signal = *packed_signal++;
    c0 = _EXTU(pack_signal, 28,28);
    s1 = _EXTU(pack_signal, 24,28);
    c2 = _EXTU(pack_signal, 20,28);
    s3 = _EXTU(pack_signal, 16,28);
    c4 = _EXTU(pack_signal, 12,28);
    s5 = _EXTU(pack_signal, 8, 28);

    *(int16*)(unpack_signal2++) = (int16)_PACKL4(_PACK2(s7 - s1, c0 - c6),_PACK2(s7 - s1, c0 - c6));
    c6 = _EXTU(pack_signal, 4, 28);
    s7 = _EXTU(pack_signal, 0, 28);
    *(int16*)(unpack_signal1++) = (int16)_PACKL4(_PACK2(s3 - s1, c0 - c2),_PACK2(s3 - s1, c0 - c2));
    *(int16*)(unpack_signal2++) = (int16)_PACKL4(_PACK2(s3 - s5, c4 - c2),_PACK2(s3 - s5, c4 - c2));
    *(int16*)(unpack_signal1++) = (int16)_PACKL4(_PACK2(s7 - s5, c4 - c6),_PACK2(s7 - s5, c4 - c6));
  }
  
  pack_signal = *packed_signal++;
  c0 = _EXTU(pack_signal, 28,28);
  s1 = _EXTU(pack_signal, 24,28);
  *(int16*)(unpack_signal2) = (int16)_PACKL4(_PACK2(s7 - s1, c0 - c6),_PACK2(s7 - s1, c0 - c6));
}
#else

#ifdef EXT_SRAM
#pragma CODE_SECTION (SampleUnpack, "fast_text")
#endif

//============================================================================================
///  Unpack input samples into channel
///
/// @param[in]  packed_signal   - pointer to input packed buffer
/// @param[out] unpack_signal1  - pointer to even buffer for channel
/// @param[out] unpack_signal2  - pointer to odd buffer for channel
//============================================================================================
static void SampleUnpack(uint32    * restrict packed_signal,
                         tSamples  * restrict unpack_signal1,
                         tSamples  * restrict unpack_signal2)
{
   uint32 tSample;
   uint32 re0, im0, re1, im1, re2, im2, re3, im3,
          re4, im4, re5, im5, re6, im6, re7, im7,
          even, tmp, odd, i;

   tSample = *(packed_signal++);

   re0 = _EXTU(tSample, 28, 30);
   im0 = _EXTU(tSample, 30, 30);
   re1 = _EXTU(tSample, 24, 30);
   im1 = _EXTU(tSample, 26, 30);
   re2 = _EXTU(tSample, 20, 30);
   im2 = _EXTU(tSample, 22, 30);
   re3 = _EXTU(tSample, 16, 30);
   im3 = _EXTU(tSample, 18, 30);
   re4 = _EXTU(tSample, 12, 30);
   im4 = _EXTU(tSample, 14, 30);
   re5 = _EXTU(tSample, 8,  30);
   im5 = _EXTU(tSample, 10, 30);
   re6 = _EXTU(tSample, 4,  30);
   im6 = _EXTU(tSample, 6,  30);
   re7 = _EXTU(tSample, 0,  30);
   im7 = _EXTU(tSample, 2,  30);

   even = _SUB2  (_PACK2(re0,im0), _PACK2(re2,im2));
   tmp  = _PACKL4(_PACK2(im1,re1), _PACK2(im3,re3));
   odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

   *(uint16*)unpack_signal1++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));

   even = _SUB2  (_PACK2(re4,im4), _PACK2(re2,im2));
   tmp  = _PACKL4(_PACK2(im5,re5), _PACK2(im3,re3));
   odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

   *(uint16*)unpack_signal2++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));

   even = _SUB2  (_PACK2(re4,im4), _PACK2(re6,im6));
   tmp  = _PACKL4(_PACK2(im5,re5), _PACK2(im7,re7));
   odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

   *(uint16*)unpack_signal1++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));

   for(i=0; i<2045; i++)
   {
     tSample = *(packed_signal++);
     re0 = _EXTU(tSample, 28, 30);
     im0 = _EXTU(tSample, 30, 30);
     re1 = _EXTU(tSample, 24, 30);
     im1 = _EXTU(tSample, 26, 30);

     even = _SUB2  (_PACK2(re0,im0), _PACK2(re6,im6));
     tmp  = _PACKL4(_PACK2(im1,re1), _PACK2(im7,re7));
     odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

     *(uint16*)unpack_signal2++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));

     re2 = _EXTU(tSample, 20, 30);
     im2 = _EXTU(tSample, 22, 30);
     re3 = _EXTU(tSample, 16, 30);
     im3 = _EXTU(tSample, 18, 30);
     re4 = _EXTU(tSample, 12, 30);
     im4 = _EXTU(tSample, 14, 30);
     re5 = _EXTU(tSample, 8,  30);
     im5 = _EXTU(tSample, 10, 30);
     re6 = _EXTU(tSample, 4,  30);
     im6 = _EXTU(tSample, 6,  30);
     re7 = _EXTU(tSample, 0,  30);
     im7 = _EXTU(tSample, 2,  30);

     even = _SUB2  (_PACK2(re0,im0), _PACK2(re2,im2));
     tmp  = _PACKL4(_PACK2(im1,re1), _PACK2(im3,re3));
     odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

     *(uint16*)unpack_signal1++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));

     even = _SUB2  (_PACK2(re4,im4), _PACK2(re2,im2));
     tmp  = _PACKL4(_PACK2(im5,re5), _PACK2(im3,re3));
     odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

     *(uint16*)unpack_signal2++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));

     even = _SUB2  (_PACK2(re4,im4), _PACK2(re6,im6));
     tmp  = _PACKL4(_PACK2(im5,re5), _PACK2(im7,re7));
     odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

     *(uint16*)unpack_signal1++ = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));
   }

   tSample = *(packed_signal++);
   re0 = _EXTU(tSample, 28, 30);
   im0 = _EXTU(tSample, 30, 30);
   re1 = _EXTU(tSample, 24, 30);
   im1 = _EXTU(tSample, 26, 30);

   even = _SUB2  (_PACK2(re0,im0), _PACK2(re6,im6));
   tmp  = _PACKL4(_PACK2(im1,re1), _PACK2(im7,re7));
   odd  = _ADD2  (_LO(_MPYSU4(0xff0101ff, tmp)), _HI(_MPYSU4(0xff0101ff, tmp)));

   *(uint16*)unpack_signal2 = (uint16)_PACKL4(_ADD2(even, odd), _ADD2(even, odd));
}
#endif

#ifdef EXT_SRAM
#pragma CODE_SECTION (SmpMgr_PutSamples, "fast_text")
#endif

//============================================================================================
///  Unpack samples and put then into 2 buffers
///
/// @param[in]  pThis - pointer to instance
//============================================================================================
static void SmpMgr_PutSamples(tSampleManager *pThis)
{
#ifndef USE_DSPBIOS
  SampleUnpack((uint32 *)(&pThis->pPackedBuff[0][0]),
            &pThis->Signal.s[SAMPLES_OF_ONE_IRQ_GPS>>2],
            &pThis->Signal.s[SAMPLES_SHIFT]);
#else
#ifdef EXT_SRAM
  SampleUnpack((uint32 *)(&pThis->pPackedBuff[pThis->bufIndex][0]),
            &pThis->Signal->s[SAMPLES_OF_ONE_IRQ_GPS>>2],
            &pThis->Signal->s[SAMPLES_SHIFT]);
#else
  SampleUnpack((uint32 *)(&pThis->pPackedBuff[pThis->bufIndex][0]),
            &pThis->Signal.s[SAMPLES_OF_ONE_IRQ_GPS>>2],
            &pThis->Signal.s[SAMPLES_SHIFT]);
#endif			
#endif
  pThis->bufIndex ^= 0x1;
}

//============================================================================================
///  Sample driver get samples from file (PC version only)
///
/// @param[in] pSampleDrvhal - pointer to instance
//============================================================================================
#ifndef USE_DSPBIOS
static void GetSamplesOfScript(tSampleManager *pThis)
{
  int i;
  int16 data;
//  uint8 data;

  if (!feof(pThis->script)) // control end-of-file
  {
    for(i=0;i<4092;i++)
    {
      fread(&data, 2, 1, pThis->script);
      pThis->pPackedBuff[0][i] = data;
    }
/*    for(i=0;i<4092;i++)
    {
      fread(&data, 1, 1, pThis->script);
      pThis->pPackedBuff[0][i]  = (data&0x3)<<2;
      pThis->pPackedBuff[0][i] |= (data&0xC)<<4;
      pThis->pPackedBuff[0][i] |= (data&0x30)<<6;
      pThis->pPackedBuff[0][i] |= (data&0xC0)<<8;
    }*/
  }
  else 
  {
    exit(0);
  }
}
#endif


#ifdef EXT_SRAM
#pragma CODE_SECTION (UpdateFastSearchBuf, "fast_text")
#endif

//============================================================================================
///  Put samples into input buffer of fast search algorithm
///
/// @param[in] pThis - pointer to instance
//============================================================================================
static void UpdateFastSearchBuf(tSampleManager *pThis)
{
  int i;

  int32* restrict pDest = &pThis->FastSearchBuf[pThis->out_samples + ADD_BUFFERS];
#ifdef EXT_SRAM
 const tSamples* restrict pSrc = pThis->Signal->s;
#else
  const tSamples* restrict pSrc = pThis->Signal.s;
#endif
  // Check if buffer is full:
  if(pThis->out_samples >= OUT_SAMPLES)
    return;

  #ifdef _TMS320C6X
  #pragma UNROLL(2)
  #endif

  for(i=0;i<SMP_FE_BLOCK_SIZE;i++)
    pDest[i] = ((pSrc[4*i].d<<16)&0xffff0000) + ((pSrc[4*i+2].d)&0xffff); 
    
  pThis->out_samples += SMP_FE_BLOCK_SIZE;
}

//============================================================================================
///  Interface function for input samples preprocessing
///
/// @param[in] pSmpMgr - pointer to instance
/// @return    1 if samples are ready or 0 otherwise
//============================================================================================
void SmpMgr_Process(struct tagSampleManager *pSmpMgr)
{
#ifdef SMPL_DRV_FORCE_FILE_INPUT
  GetSamplesOfScript(pSmpMgr);
#endif
  SmpMgr_PutSamples(pSmpMgr);
  UpdateFastSearchBuf(pSmpMgr);
}

#ifdef EXT_SRAM
#pragma CODE_SECTION (ShiftEpoch, "fast_text")
#endif

//============================================================================================
///  Function shift input samples and overwrite old buffer
///
/// @param[in] pDest - pointer to samples buffer
//============================================================================================
static void ShiftEpoch(tDoubleWord* restrict pDest)
{
  // shift input samples 
  {
    int32 i;
    const tDoubleWord* restrict pSrc = pDest + (SAMPLES_OF_ONE_IRQ_GPS>>4);
    int32 count = (SAMPLES_OF_ONE_IRQ_GPS>>4);
    _NASSERT(count >= 0);

    #ifdef _TMS320C6X
    #pragma UNROLL(4)
    #endif
    for(i=0;i<count; i++)
    {
      pDest[i] = pSrc[i];
    }

    pSrc = pDest + 3069;

    #ifdef _TMS320C6X
    #pragma UNROLL(4)
    #endif
    for(i=0;i<count; i++)
    {
      pDest[i+(SAMPLES_OF_ONE_IRQ_GPS>>3)] = pSrc[i];
    }
  }
}

//============================================================================================
///  Interface function removes last epoch samples
///
/// @param[in] pSmpMgr - pointer to samples buffer
//============================================================================================
void SmpMgr_RemoveLastEpoch(struct tagSampleManager *pSmpMgr)
{
#ifdef EXT_SRAM
  ShiftEpoch(pSmpMgr->Signal->d);
#else
  ShiftEpoch(pSmpMgr->Signal.d);
#endif  
}

//============================================================================================
///  Interface function gets pointer to unpacked samples buffer
///
/// @param[in] pSmpMgr - pointer to instance
//============================================================================================
tSamples *SmpMgr_GetEpoch(struct tagSampleManager *pSmpMgr)
{
#ifdef EXT_SRAM
  return pSmpMgr->Signal->s;
#else
  return pSmpMgr->Signal.s;
#endif  
}

//============================================================================================
///  Interface function starts fast search buffer forming
///
/// @param[in] pSmpMgr - pointer to samples buffer
//============================================================================================
void SmpMgr_StartFastSearchBuf(struct tagSampleManager *pSmpMgr)
{
  pSmpMgr->out_samples = 0;
  pSmpMgr->SamplesOld = fg_OFF;
}

//============================================================================================
///  Interface function check if buffer for fast search is ready
///
/// @param[in] pSmpMgr - pointer to samples buffer
/// @return    1 if samples amount is greater than OUT_SAMPLES or 0 otherwise
//============================================================================================
int  SmpMgr_IsFinishedFastSearchBuf(struct tagSampleManager *pSmpMgr)
{
  return pSmpMgr->out_samples >= OUT_SAMPLES;
}

//============================================================================================
///  Interface function gets pointer to buffer for fast search algorithm
///
/// @param[in] pSmpMgr - pointer to samples buffer
/// @return    pointer to samples buffer
//============================================================================================
int32 *SmpMgr_GetPackedSignal(struct tagSampleManager *pSmpMgr)
{
  return pSmpMgr->FastSearchBuf;
}

//============================================================================================
///  Interface function checks if buffer for fast search is processed
///
/// @param[in] pSmpMgr - pointer to samples buffer
/// @return    fg_ON if buffer contains processed samples or fg_OFF otherwise
//============================================================================================
eFlag SmpMgr_NeedStartFastSearchBuf(struct tagSampleManager *pSmpMgr)
{
  return pSmpMgr->SamplesOld;
}

