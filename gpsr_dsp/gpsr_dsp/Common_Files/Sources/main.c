/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Main
 *   @file                   main.c
 *   @author                 M. Zhokhova
 *   @date                   22.09.2005
 *   @version                2.0
 *
 *               EXT_SRAM  -  Tom
 */
/*****************************************************************************/
#include "Framework.h"
#include "SatelliteManager.h"
#include "SampleDriver.h"
#include "NavTask.h"
#include "MainManager.h"
#include "BuiltInTest.h"
#include "../OP/OPManager.h"
#include "DataProtocol.h"
#include <stdio.h>
#include <csl_cache.h>
#include <csl_irq.h>

#define HARDWARE_RESET                   (0)        ///< hardware reset
#define SOFTWARE_RESET                   (1)        ///< software reset
#define BUILT_IN_TEST                    (2)        ///< built-in test
//#define ADDR_FLAG      (*(volatile uint8*)0xFFFFB)  ///< address of flag for hardware/software reset or built-in test
#define ADDR_FLAG      (*(volatile uint8*)0xBFFF0)  ///< address of flag for hardware/software reset or built-in test
#define BOOT_STATUS		(*(volatile uint8*)0xBFFF4)
#define BOOT_CHKSUM		(*(volatile uint32*)0xBFFFC)

/// for target
#ifdef USE_DSPBIOS
#pragma DATA_SECTION (MainManagerState,       "MainManagerState")
#pragma DATA_SECTION (FrameworkState,         "FrameworkState")
#pragma DATA_SECTION (SatelliteManagerState,  "SatelliteManagerState")
#pragma DATA_SECTION (SampleManagerState,     "SampleManagerState")
#pragma DATA_SECTION (NavTaskState,           "NavTaskState")
#pragma DATA_SECTION (OPManagerState,         "OPManagerState")
#else  /// for PC model
#include <string.h>
#include "Report.h"
static const char *DataFile = 0;
static const char *LogFile  = "SAMPLES.LOG";
volatile uint32 counter = 0;
/// Tracking thread routine
void SWI_TrackingManager(void);
/// Search thread routine
void IDLE_SearchThread(void);
/// Navigation Task thread routine
void SWI_NavTask(void);
#endif

tMainManager         MainManagerState;
tFramework           FrameworkState;
tSatelliteManager    SatelliteManagerState;
tSampleManager       SampleManagerState;
tNavTask             NavTaskState;
tOPManager  		 OPManagerState;

//---------------------------------------------------
// Tom, for through-put output
float		time_temp;
int			DebugFlag = 0;
uint32		ms_count_new;
uint32		ms_count_old; 
//---------------------------------------------------
// override default TMTC mode
#ifdef NEW_TMTC

// define this macro for using DETAIL mode as default
//#define OVERRIDE_TMTCMODE	DETAIL_MODE

//	#ifdef CAN_PORT_TMTC
		#include "can_drv.h"
		#define OVERRIDE_TMTCMODE	BRIEF_MODE
//	#endif  
#endif	// end NEW_TMTC


#ifdef EXT_SRAM
// alex, for external SRAM
#pragma DATA_SECTION (uart_dma_WriteBuffer,         "fast_data")
#pragma DATA_SECTION (uart_dma_ReadBuffer,         "fast_data")
#pragma DATA_SECTION (myPackedBuff,         "fast_data")
#pragma DATA_SECTION (mySignal,         "fast_data")
#pragma DATA_SECTION (myFastSearchBuf,         "fast_data")
#pragma DATA_SECTION (my_fast_search,         "fast_data")
#pragma DATA_SECTION (myPrompt,         "fast_data")
#pragma DATA_SECTION (myFlagRead,         "fast_data")

DATA_STRING uart_dma_WriteBuffer;
DATA_STRING uart_dma_ReadBuffer;
union packbuff_union myPackedBuff;
union signal_union mySignal;
int32 myFastSearchBuf[(OUT_SAMPLES + ADD_BUFFERS)];
tFsearch        my_fast_search;                       ///< fast search structure
int8 myPrompt[GPS_SAT_CHANNELS_COUNT][(CODE_LENGTH+4)/4*4];
int8 *pMyPrompt[GPS_SAT_CHANNELS_COUNT];
uint8 myFlagRead;
#endif

// Alex, 20140619, NEW TMTC ICD
#ifdef NEW_TMTC

//#ifdef EXT_SRAM
//#pragma DATA_SECTION (tmtc_mode,         "fast_data")
//#endif

int tmtc_mode ;
int tlm_type ;
int reset_type ;

unsigned int nspo_sof ;
Tm_Frame_Gpsr_Nav_Tlm gpsr_nav_tm_frame ;
Tm_Frame_Gpsr_Sci_Tlm gpsr_sci_tm_frame ;
Tm_Frame_Gpsr_Tc_Ack gpsr_tc_ack_tm_frame ;
Tm_Frame_Gpsr_Short_Nav_Tlm gpsr_short_nav_tm_frame ;
Tm_Frame_Gpsr_Debug_Tlm gpsr_dbg_tm_frame ;

uint16 tracking_utilization[UTILIZATION_ZONE + 1];
uint32 max_utilization_time ;
uint16 max_utilization ;

uint16 pre_tracking_utilization[UTILIZATION_ZONE + 1];
uint32 pre_max_utilization_time ;
uint16 pre_max_utilization ;

#endif	// end NEW_TMTC

uint8 boot_status ;
int new_boot_config = -1 ;

int post_nav_aligned, extra_ms_post_nav ;

uint32 can_or_uart = OUTPUT_CAN1; //OUTPUT_CAN;//OUTPUT_UART;
uint32 pre_can_or_uart ;

#define GPEN          ((unsigned int*)0x01B00000)
#define GPDIR         ((unsigned int*)0x01B00004)
#define GPVAL         ((unsigned int*)0x01B00008)

void set_gpio(unsigned int*reg, int pin, int val)
{
        unsigned int tmp;

        tmp = * reg;

        if ( val)
                * reg = tmp | (1 <<pin);
        else
                * reg = tmp & (~(1 <<pin));
}


//*****************************************************************************
///                         M A I N   R O U T I N E
//*****************************************************************************
#define EMIFB_CE2_WRSETUP      	3
#define EMIFB_CE2_WRSTRB        3
#define EMIFB_CE2_WRHLD         3

#define EMIFB_CE2_RDSETUP       4//7
#define EMIFB_CE2_RDSTRB        8//0xe
#define EMIFB_CE2_RDHLD         3

#define EMIFB_CE2_TA            0x3
#define EMIFB_CE2_MTYPE         0x1//16 bits async
#define EMIFB_CE2_WRHLDMSB  	0x0

#define EMIFB_CE2_CONFIG ((EMIFB_CE2_WRSETUP << 28) | (EMIFB_CE2_WRSTRB << 22) | (EMIFB_CE2_WRHLD << 20) | (EMIFB_CE2_RDSETUP << 16) | (EMIFB_CE2_TA << 14) | (EMIFB_CE2_RDSTRB << 8) | (EMIFB_CE2_MTYPE << 4) | (EMIFB_CE2_WRHLDMSB << 3) | (EMIFB_CE2_RDHLD))
//#define EMIFB_CE3_CONFIG 	0x30F7CE03
#define EMIFB_CE2_CTRL      0x01a80010

void main()
{
  uint8        resetBIT;    /// hardware or software reset or BIT flag
  tBuiltInTest oldBIT;      /// BIT structure
  CONFIG_SETUP oldConfig;   /// configuration setup structure
  eFlag        BinStrMode;  /// binary or string mode flag
	int i ;
#ifdef EXT_SRAM  
	volatile uint32 tmp;
#endif


 
  /// for PC model
  #ifndef USE_DSPBIOS
    FILE *logFile[GPS_SATELLITE_COUNT];
    int8 i;
    for(i=0;i<GPS_SATELLITE_COUNT;i++)
    {
      char name [20];
      sprintf(name,"%d.log", i+1);
      logFile[i] = fopen(name, "wt");
    }
    printf("Data source: %s\n", DataFile);
    printf("Log:         %s\n", LogFile);
    resetBIT = 0;
  #else  /// for target
  
// alex added, for DSP/BIOS 5.4
	CSL_init();
	cslCfgInit();
	*((uint32 *)EMIFB_CE2_CTRL) = EMIFB_CE2_CONFIG ;
  
    resetBIT = ADDR_FLAG;  /// read flag from memory: hardware or software reset or BIT
	boot_status = BOOT_STATUS ;
	pre_can_or_uart = can_or_uart;


#ifdef EXT_SRAM  
	//
	// Caution: NOT check "641x - Configure L2 Memory Settings" in  <System/Global Settings> of DSP/BIOS Config
	//
	if (resetBIT == HARDWARE_RESET)	// if SOFTWARE_RESET or BUILT_IN_TEST, bypass cache init
	{
		tmp = *(volatile unsigned int *)0x01840000;	// CCFG
		tmp &= 0xfffffff8;	// clear bits
		if ( tmp == 0 )	// not init yet, true cold start
		{
			// 0 : for EXT_SRAM with Stack in ISRAM
			// 1 : for fully EXT_SRAM
			#if 0
			//FlushCache   
			{ 
				// Invalidate L1I and L1D
				*(int *)0x01840000 = (*(int *)0x01840000 | 0x00000300);
				// Clean L2
				*(int *)0x01845004 = 0x1; 
			}
			#endif   	
		
	//		tmp |= 0x01;	// change L2mode to 0x1; 32K L2 cache, CCFG
			tmp |= 0x02;	// change L2mode to 0x2; 64K L2 cache, CCFG
	//		tmp |= 0x03;	// change L2mode to 0x3; 128K L2 cache, 896K SRAM; CCFG
	//		tmp |= 0x07;	// change L2mode to 0x7; 256K L2 cache, 768K SRAM; CCFG

			*(volatile unsigned int *)0x01840000 = tmp;
			*(volatile unsigned int *)0x01848200 = 0x01;	// enable cache of EMIFA CE0 0x80000000~0x80ffffff; MAR128
		}
	}
#endif

	
    /// control flag error
    if ((resetBIT != HARDWARE_RESET) && (resetBIT != SOFTWARE_RESET) && (resetBIT != BUILT_IN_TEST))
      resetBIT = HARDWARE_RESET;

    if (resetBIT != HARDWARE_RESET)
    {
      /// save binary/string mode for UART, watchdog mode, old Built-in test results and configuration setup
      BinStrMode = MainManagerState.BinStrFlag;
      oldBIT     = FrameworkState.BuiltInTest;
      oldConfig  = MainManagerState.config;
    }
  #endif

//  *(volatile uint8 *)0x68000002 = 0x0;

  /// memset main object
  memset(&MainManagerState,      0, sizeof(tMainManager));
  memset(&FrameworkState,        0, sizeof(tFramework));
  memset(&SampleManagerState,    0, sizeof(tSampleManager));
  memset(&SatelliteManagerState, 0, sizeof(tSatelliteManager));
  memset(&NavTaskState,          0, sizeof(tNavTask));
  memset(&OPManagerState,        0, sizeof(tOPManager));
  
#ifdef EXT_SRAM
  memset(&uart_dma_WriteBuffer, 0, sizeof(uart_dma_WriteBuffer));
  memset(&uart_dma_ReadBuffer,  0, sizeof(uart_dma_ReadBuffer));
  memset(&myPackedBuff,         0, sizeof(myPackedBuff));
  memset(&mySignal,         	0, sizeof(mySignal));
  memset(&myFastSearchBuf[0],  	0, sizeof(myFastSearchBuf));
  memset(&my_fast_search,  		0, sizeof(my_fast_search));
  memset(&myPrompt[0][0],  		0, sizeof(myPrompt));
  myFlagRead = TERMINAL_FREE ;
#endif
  
  
  /// initialize Sample Manager object
#ifdef EXT_SRAM  
  // alex, for external SRAM
  SampleManagerState.PackedBuff = &myPackedBuff;
  SampleManagerState.Signal = &mySignal;
  SampleManagerState.FastSearchBuf = &myFastSearchBuf[0];
#endif
  SmpMgr_Init(&SampleManagerState, 0);

  /// initialize Flash Driver object
  FrameWork_FlashInit(&FrameworkState.FlashDrv);

  if (resetBIT == HARDWARE_RESET)
  {
    /// read configuration setup from FLASH or setting default values
    if (MainMgr_ConfigInit(&FrameworkState.FlashDrv, &MainManagerState.config))
      FrameworkState.BuiltInTest.statusFlags |= 0; // CONFIG_CHSUM;
  }
  else  /// for SOFTWARE_RESET and BUILT_IN_TEST save old configuration setup, UART binary/string mode, watchdog mode
  {
    MainManagerState.config     = oldConfig;
    MainManagerState.BinStrFlag = BinStrMode;
  }

  /// initialize Main Manager object
  MainMgr_Init(&MainManagerState);
  
#ifdef NEW_TMTC
	for ( i = 0 ; i < sizeof(tracking_utilization)/sizeof(tracking_utilization[0]) ; i ++ )
		tracking_utilization[i] = 0 ;
	for ( i = 0 ; i < sizeof(pre_tracking_utilization)/sizeof(pre_tracking_utilization[0]) ; i ++ )
		pre_tracking_utilization[i] = 0 ;
		
	max_utilization_time = 0 ;
	max_utilization = 0 ;
	pre_max_utilization_time = 0 ;
	pre_max_utilization = 0 ;
	
#ifdef OVERRIDE_TMTCMODE
  tmtc_mode = OVERRIDE_TMTCMODE ;
#endif  
#endif	// end NEW_TMTC
  
// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC
	if ( tmtc_mode == BRIEF_MODE )
		MainManagerState.Message = 0 ;
	else
		MainManagerState.Message = (SAT_MES | POS_MES | OPP_MES);  /// SAT, POS, OPP  
#else
		MainManagerState.Message = (SAT_MES | POS_MES | OPP_MES);  /// SAT, POS, OPP  
#endif	// end NEW_TMTC

  /// initialize Nav Task object
  NavTask_Init(&NavTaskState, MainManagerState.config.NavSolRate, MainManagerState.config.OPmode);

  /// initialize OP object
  OPMgr_Init(&OPManagerState, MainManagerState.config.OrbStatic);

  FrameworkState.BITFlag = fg_OFF;

  /// for target
  #ifdef USE_DSPBIOS
    if (resetBIT != SOFTWARE_RESET)  /// for HARDWARE_RESET and BUILT_IN_TEST
    {
      /// Built-in test and load FPGA
//      FrameWork_BuiltInTest(&SampleManagerState, &FrameworkState, resetBIT, oldBIT);
    }
    else  /// for SOFTWARE_RESET
      FrameworkState.BuiltInTest = oldBIT;

    /// save HARDWARE_RESET to memory
    resetBIT  = HARDWARE_RESET;
    ADDR_FLAG = resetBIT;

    /// initialize hardware
    HardwareInit();

    // 關閉 6支時脈信號輸出
    // EMIFA GBLCTL, clear bits 3, 4, 5, 16 (CLK6EN, CLK4EN, EK1EN, EK2EN)
    *(volatile unsigned int *)0x01800000 &= ~0x10038;

    // EMIFB GBLCTL, clear bits 3, 4, 5, 16 (CLK6EN, CLK4EN, EK1EN, EK2EN)
    *(volatile unsigned int *)0x01A80000 &= ~0x10038;

    // 關閉 MRAM Power
    set_gpio(GPDIR, 0, 1);     // GPIO-0 is an output
    //delay_mcs(200);
    set_gpio(GPVAL, 0, 0);    // drive LOW to GPIO-0
    //delay_mcs(200);
    set_gpio(GPEN, 0, 1);      // enable GPIO-0
    //delay_mcs(200);
    
    /*
    //override CLKOUT4 (120MHz) to GPIO1
    set_gpio(GPDIR, 1, 1);     // GPIO-1 is an output
    set_gpio(GPVAL, 1, 0);    // drive LOW to GPIO-1
    set_gpio(GPEN, 1, 1);      // enable GPIO-1
         
    //override CLKOUT6 (80MHz) to GPIO2
    set_gpio(GPDIR, 2, 1);     // GPIO-2 is an output
    set_gpio(GPVAL, 2, 0);    // drive LOW to GPIO-2
    set_gpio(GPEN, 2, 1);      // enable GPIO-2
    */

  #endif

  /// initialize Satellite Manager object
#ifdef EXT_SRAM
  // alex, for external SRAM
  SatelliteManagerState.fast_search = &my_fast_search;
  
  for ( i = 0 ; i < GPS_SAT_CHANNELS_COUNT ; i ++ )
  {
	SatelliteManagerState.gps_satellites[i].gps_chan.prompt = &myPrompt[i][0];
	pMyPrompt[i] = &myPrompt[i][0];
  }
#endif  

  SatMgr_Init(&SatelliteManagerState, MainManagerState.config.CoefDLL, MainManagerState.config.DLLtime);

  /// initialize Framework object
#ifdef EXT_SRAM
  // alex, for external SRAM
  FrameworkState.DebugTerminal.stringWrite = &uart_dma_WriteBuffer ;
  FrameworkState.DebugTerminal.stringRead = &uart_dma_ReadBuffer ;
  FrameworkState.DebugTerminal.FlagRead = &myFlagRead ;
#endif  
  FrameWork_Init(&FrameworkState, MainManagerState.config.BaudRate422);

  /// for target
  #ifdef USE_DSPBIOS
/*    /// read ephemeris from FLASH
    MainMgr_EphemerisInit(&FrameworkState.FlashDrv, &SatelliteManagerState);
    /// read almanac from FLASH
    MainMgr_AlmanacInit(&FrameworkState.FlashDrv, &SatelliteManagerState);
    /// copy ephemeris and almanac for navigation task solution
    NavTask_CopyDecodedData(&SatelliteManagerState.TopSystem);
    /// read week data from FLASH
    MainMgr_WeekDataInit(&FrameworkState.FlashDrv, &FrameworkState.weekData);*/

    /// hardware start

    HardwareStart();




    /// Version software
    {
      FrameworkState.version    = 7;
      FrameworkState.subversion = 1;
    }
    /// Configuration setup
//#ifdef CAN_PORT_TMTC
#if 1
	can_tmtc_init() ;
#endif
	
#ifdef NEW_TMTC
	if ( ! (( tmtc_mode == DETAIL_MODE ) && (MainManagerState.BinStrFlag == fg_ON) ) )
#else
	if ( MainManagerState.BinStrFlag == fg_OFF )
#endif	// 	NEW_TMTC
	{
#ifdef NEW_TMTC
// protocol checking
		void *p;
		int len, protocol_checking_error ;
			
		protocol_checking_error = 0 ;
		
		if ( ( len = (char *)&((Tm_Frame_Gpsr_Nav_Tlm *)p)->chksum - &((Tm_Frame_Gpsr_Nav_Tlm *)p)->header.sof[0] + 1 ) != TM_GPSR_NAV_TLM_FRAME_LENGTH )
		{
			dbg_printf("Tm_Frame_Gpsr_Nav_Tlm: expected %d, real %d", TM_GPSR_NAV_TLM_FRAME_LENGTH, len ) ;
			protocol_checking_error = 1 ;
		}
		if ( ( len = (char *)&((Tm_Frame_Gpsr_Sci_Tlm *)p)->chksum - &((Tm_Frame_Gpsr_Sci_Tlm *)p)->header.sof[0] + 1 ) != TM_GPSR_SCI_TLM_FRAME_LENGTH )
		{
			dbg_printf("Tm_Frame_Gpsr_Sci_Tlm: expected %d, real %d", TM_GPSR_SCI_TLM_FRAME_LENGTH, len ) ;
			protocol_checking_error = 1 ;
		}
		if ( ( len = (char *)&((Tm_Frame_Gpsr_Short_Nav_Tlm *)p)->chksum - &((Tm_Frame_Gpsr_Short_Nav_Tlm *)p)->header.sof[0] + 1 ) != TM_GPSR_S_NAV_TLM_FRAME_LENGTH )
		{
			dbg_printf("Tm_Frame_Gpsr_Short_Nav_Tlm: expected %d, real %d", TM_GPSR_S_NAV_TLM_FRAME_LENGTH, len ) ;
			protocol_checking_error = 1 ;
		}
		if ( ( len = (char *)&((Tm_Frame_Gpsr_Debug_Tlm *)p)->chksum - &((Tm_Frame_Gpsr_Debug_Tlm *)p)->header.sof[0] + 1 ) != TM_GPSR_DBG_MSG_FRAME_LENGTH )
		{
			dbg_printf("Tm_Frame_Gpsr_Debug_Tlm: expected %d, real %d", TM_GPSR_DBG_MSG_FRAME_LENGTH, len ) ;
			protocol_checking_error = 1 ;
		}
		
		if ( ! protocol_checking_error )
			dbg_printf("GPSR New TMTC Protocol checking OK!!" ) ;	
			
#endif	// NEW_TMTC

#if 1
		dbg_printf("Image ID: %d, BOOT_STATUS: %x, CONFIG: Nav_&OP:%d, modelOS:%d, I:%d, T:%d\r\n", (BOOT_STATUS&0x04)>>2, BOOT_STATUS, MainManagerState.config.OPmode, MainManagerState.config.OrbStatic, 
							MainManagerState.config.IonosphereModel, MainManagerState.config.TroposphereModel);
#else
		sprintf(MainManagerState.TestStr.str, "Image ID: %d, BOOT_STATUS: %x, CONFIG: Nav_&OP:%d, modelOS:%d, I:%d, T:%d\r\n", BOOT_STATUS&0x04, BOOT_STATUS, MainManagerState.config.OPmode, MainManagerState.config.OrbStatic, 
							MainManagerState.config.IonosphereModel, MainManagerState.config.TroposphereModel);
		FrameworkState.DebugTerminal.vtable.pfnWrite(&FrameworkState.DebugTerminal.vtable, 
							   MainManagerState.TestStr.str, strlen(MainManagerState.TestStr.str));
#endif							   
	}
						   	   				   
	
    /// enable global hardware interrupt
    HWI_enable();
  #else  /// for PC model
    {
      int sv_cnt;

      while(1)
      {
        sv_cnt = 0;
        for(i=0;i<GPS_SATELLITE_COUNT;i++)
        {
          if (SatelliteManagerState.SVArray[i] == 1)
            sv_cnt++;
        }
        if (sv_cnt < 5)
          IDLE_SearchThread();
        
        SWI_TrackingManager();
        for(i=0;i<GPS_SAT_CHANNELS_COUNT; i++)
        {
          int8 num = SatelliteManagerState.gps_satellites[i].sv_num;
          if (IS_GPS_SAT(num))
            printStatistics(logFile[num-1],
                           &SatelliteManagerState.gps_satellites[i].gps_chan,
                           &SatelliteManagerState.gps_satellites[i].decoder,
                           SatelliteManagerState.ms_count);
        }
      }

      for(i=0;i<GPS_SATELLITE_COUNT;i++)
      {
        fclose(logFile[i]);
      }    
  }
  #endif
}


#ifdef EXT_SRAM
#pragma CODE_SECTION (SWI_TrackingManager, "fast_text")
#endif

///============================================================================
/// Tracking thread routine
///============================================================================



//PERFORMANCE_MEASUREMENT
//#define HWI_SCHEDULING_TEST		
#undef HWI_SCHEDULING_TEST		
	
#ifdef HWI_SCHEDULING_TEST		
	unsigned long tracking_called_cnt=0;
	unsigned long tracking_run_cnt=0;
#endif

static int tctctc = 0;
void check_switch_CAN_PortB_pulse();
void SWI_TrackingManager(void)
{
  int32 i;
  int navTask_posted = 0 ;

  check_switch_CAN_PortB_pulse();
  /// for target
  #ifdef USE_DSPBIOS

    PROFILE_START();  /// start timer

    /// control for terminals
    MainMgr_Control(&MainManagerState, &FrameworkState, &SatelliteManagerState, &NavTaskState, &OPManagerState);

    /// one ms data received, two ms data transmitted
    if (FrameworkState.DebugTerminal.FlagUART == fg_OFF)
    {

			can_rcv_tc(GetPrimaryDebugTerminal());

#ifdef EXT_SRAM
			OnDataReceived(GetPrimaryDebugTerminal(), (uint8 *)&FrameworkState.DebugTerminal.FlagRead);
#else
			OnDataReceived(GetPrimaryDebugTerminal(), &FrameworkState.DebugTerminal.FlagRead);
#endif


      FrameworkState.DebugTerminal.FlagUART = fg_ON;
    }
    else
    {

	  if(can_or_uart == OUTPUT_UART){
		  	OnDataTransmitted(GetPrimaryDebugTerminal());

      }
	  else{
		  can_send_tm(GetPrimaryDebugTerminal());
      }



      FrameworkState.DebugTerminal.FlagUART = fg_OFF;
    }
    /// read latches for nPPS signal forming algorithm: its must be correspond to navigation task solution
    if (((SatelliteManagerState.ms_count+1) >= NavTaskState.CountPostNavTask) && (NavTaskState.NavTaskEnd == fg_ON))
    {
      SatelliteManagerState.ms1_latch    = LATCH_1MS;
      SatelliteManagerState.ms1000_latch = LATCH_1S;
    }
    /// control nPPS signal forming: when navigation task solution finished
    if ((NavTaskState.nPPSstart == fg_ON) && (NavTaskState.NavTaskEnd == fg_ON)
        && (SatelliteManagerState.nPPSCorrect == fg_OFF))
    {
      SatMgr_nPPSControl(&SatelliteManagerState, NavTaskState.NavCond, NavTaskState.PeriodTime, 
                         NavTaskState.CorrectTime, NavTaskState.User);
      SatelliteManagerState.nPPSCorrect = fg_ON;
    }
//    else  /// in other time: data write-in FLASH
//      FrameWork_FlashControl(&FrameworkState.FlashDrv,  &SatelliteManagerState.TopSystem,
//                             &MainManagerState.config, &MainManagerState.flags, &FrameworkState.weekData);
  #endif

  SatMgr_AlmFastSearch(&SatelliteManagerState);

  /// samples process and forming search buffer for current antenna
  SmpMgr_Process(&SampleManagerState);

  /// increment current time
  SatMgr_IncrementTime(&SatelliteManagerState, &NavTaskState.CountPostNavTask, NavTaskState.PeriodTime);
  /// control search process
  SatMgr_SearchProcess(&SatelliteManagerState);

  /// satellite process
  for(i=0; i<GPS_SATELLITE_COUNT; i++)
  {
    if (SatelliteManagerState.SVArray[i] == 1)
      SatMgr_Process(&SatelliteManagerState, SmpMgr_GetEpoch(&SampleManagerState), (i+1), MainManagerState.config);
  }
   
  /// new period of navigation task solution
  MainMgr_PeriodNavTask(&MainManagerState, &NavTaskState, SatelliteManagerState.ms_count);

  /// control new time for navigation task solution
  if ((SatelliteManagerState.ms_count > NavTaskState.CountPostNavTask) && (NavTaskState.NavTaskEnd == fg_ON))
  {
    while (SatelliteManagerState.ms_count > NavTaskState.CountPostNavTask)
    {
      if (NavTaskState.Synchronization == SYNC_PROCESS)
      {
        if (NavTaskState.cntPeriod == 0)
          NavTaskState.enableMessages = 1;
        else
          NavTaskState.enableMessages = 0;          
        NavTaskState.cntPeriod++;
        if (NavTaskState.cntPeriod >= NavTaskState.numPeriod)
        {
          NavTaskState.CountPostNavTask += NavTaskState.PeriodSyncFract;
          NavTaskState.cntPeriod         = 0;
        }
        else
          NavTaskState.CountPostNavTask += PERIOD_SYNC;
      }
      else
        NavTaskState.CountPostNavTask += NavTaskState.PeriodTime;
    }
  }
  
  
///////////////////////////////////////////////////////
// adjust NavTaskState.CountPostNavTask to align post time to multiply of NavSolRate for next post
	if ( ! post_nav_aligned )	// last post is not aligned ?
	{
		if (NavTaskState.Synchronization == SYNC_PROCESS)			// in OP Init phase, add period count to shorten next post time
			NavTaskState.cntPeriod += ( extra_ms_post_nav / NavTaskState.PeriodTime ) ;
		else
			NavTaskState.CountPostNavTask -= extra_ms_post_nav ;	// otherwise, subtract the extra time
			
		extra_ms_post_nav = 0 ;		// clear
		post_nav_aligned = 1 ;		// reset flag
	}
///////////////////////////////////////////////////////  
  
  
  /// navigation conditions forming and posting navigation task solution
  if ((SatelliteManagerState.ms_count >= NavTaskState.CountPostNavTask) && (NavTaskState.NavTaskEnd == fg_ON))
  {
    #ifdef USE_DSPBIOS
      HWI_disable();
    #endif
      /// messages forming
      MainMgr_MessagesForm(&MainManagerState.data.timeChStatus, &MainManagerState.data.chRM, &SatelliteManagerState);
    #ifdef USE_DSPBIOS
      HWI_enable();
    #endif
	
    /// NMEA message forming by request
	
#if 0	// Alex, 20150325, move MainMgr_NMEARawMeasur() to behind SatMgr_NavCondForming() 	
// Alex, 20140619, NEW TMTC ICD
#ifdef NEW_TMTC
	if ( tmtc_mode == DETAIL_MODE )
#endif	// end NEW_TMTC
	{
    if ( ((MainManagerState.Message&0x8) == 0x8))
      MainMgr_NMEARawMeasur(&SatelliteManagerState);
	}
#endif

	
    if (NavTaskState.Synchronization == SYNC_PROCESS)
    {
      if (NavTaskState.cntPeriod == 0)
        NavTaskState.enableMessages = 1;
      else
        NavTaskState.enableMessages = 0;
      NavTaskState.cntPeriod++;
      if (NavTaskState.cntPeriod >= NavTaskState.numPeriod)
      {
        NavTaskState.CountPostNavTask = SatelliteManagerState.ms_count + NavTaskState.PeriodSyncFract;
        NavTaskState.cntPeriod        = 0;
      }
      else
        NavTaskState.CountPostNavTask = SatelliteManagerState.ms_count + PERIOD_SYNC;
    }
    else
      NavTaskState.CountPostNavTask = SatelliteManagerState.ms_count + NavTaskState.PeriodTime;

	  
    /// set navigation conditions and satellite with raw data list forming 
    SatMgr_NavCondForming(&SatelliteManagerState, &MainManagerState.config, &NavTaskState.NavCond, NavTaskState.Synchronization,
                          &NavTaskState.CorrectTime, &OPManagerState.CorrectTimeOP, MainManagerState.timeTrackingSWI);

#if 1	// Alex, 20150325, move MainMgr_NMEARawMeasur() to behind SatMgr_NavCondForming() 	
// Alex, 20140619, NEW TMTC ICD
#ifdef NEW_TMTC
	if ( tmtc_mode == DETAIL_MODE )
#endif	// end NEW_TMTC
	{
    if ( ((MainManagerState.Message&0x8) == 0x8))
      MainMgr_NMEARawMeasur(&SatelliteManagerState);
	}
#endif	  
						  
						  
    /// copy ephemeris, almanac, ionosphere and UTC_GPS parameters to buffers for navigation task solution
    NavTask_CopyDecodedData(&SatelliteManagerState.TopSystem);
    /// clearing flags and counter
    NavTaskState.NavTaskEnd           = fg_OFF;
    SatelliteManagerState.nPPSCorrect = fg_OFF;
    MainManagerState.timeTrackingSWI  = 0;
    /// posting navigation task solution thread
	navTask_posted = 1 ;
    #ifdef USE_DSPBIOS
    /// for target
      FrameworkState.EventHandler.pfnPostNavTask(FrameworkState.EventHandler.pHost); 
    #else  /// for PC model
      SWI_NavTask();
    #endif
  } 
  /// correct time
  SatMgr_CorrectTime(&SatelliteManagerState, &NavTaskState.CountPostNavTask, 
                     NavTaskState.PeriodTime, &NavTaskState.CorrectTime, 
                     &OPManagerState.CorrectTimeOP, OPManagerState.fOP_Condition, NavTaskState.User);
  /// remove last epoch
  SmpMgr_RemoveLastEpoch(&SampleManagerState);

#ifdef USE_DSPBIOS
  /// for target: calculate work time for tracking thread
  {
    uint32 time = PROFILE_STOP();
    MainManagerState.timeTrackingSWI += (float32)time/480000.0;
  
      
/* Performance measurement CODE */ 
//----------------------------------------------------------------------------------- 
	{
		uint32	utilization ;

#ifdef HWI_SCHEDULING_TEST		
		tracking_run_cnt++ ;
#endif	

// 100% --> 480000 cycles
// 10%  --> 48000 cycles
// 1%   --> 4800 cycles
	
		utilization = time * 100 / CPU_CYCLES_PER_MS ;
		i = utilization / UTILIZATION_ZONE ;
//		i = time/(CPU_CYCLES_PER_MS/UTILIZATION_ZONE) ;
		if ( i >= UTILIZATION_ZONE )
			i = UTILIZATION_ZONE ;
		
		tracking_utilization[i] ++ ;
		if ( utilization > max_utilization )
		{
			max_utilization = utilization ;
			max_utilization_time = SatelliteManagerState.ms_count ;
		}
		
		if ( navTask_posted )
		{
			for ( i = 0 ; i < sizeof(tracking_utilization)/sizeof(tracking_utilization[0]) ; i ++ )
			{
				pre_tracking_utilization[i] = tracking_utilization[i] ;
				tracking_utilization[i] = 0 ;
			}
			pre_max_utilization_time = max_utilization_time ;
			max_utilization_time = 0 ;
			pre_max_utilization = max_utilization ;
			max_utilization = 0 ;
		}
		
	}
//-------------------------------------------------------------------------------------      

  }
#endif
}

void performance_report(void)
{
	int i ;
	
#ifdef NEW_TMTC
	if ( tmtc_mode == DETAIL_MODE )
#endif	// end NEW_TMTC
	{
		if ( (MainManagerState.BinStrFlag == fg_OFF) && (DebugFlag == 1) )
		{
			dbg_printf("\nMax: %d%% @ %u ms\n", pre_max_utilization, pre_max_utilization_time);
												   
      		dbg_printf("Tracking Utilization: %d %d %d %d %d %d %d %d %d %d %d\n", \
				pre_tracking_utilization[0], pre_tracking_utilization[1], pre_tracking_utilization[2], pre_tracking_utilization[3], pre_tracking_utilization[4], \
				pre_tracking_utilization[5], pre_tracking_utilization[6], pre_tracking_utilization[7], pre_tracking_utilization[8], pre_tracking_utilization[9], pre_tracking_utilization[10]);
											   
#ifdef HWI_SCHEDULING_TEST		
      		dbg_printf("Tracking thread: called %lu, run %lu\n", tracking_called_cnt, tracking_run_cnt);
				
#endif				
      		dbg_printf("End of NavTask Time: %d \r\n\n", SatelliteManagerState.ms_count);
		}
	}
	
	for ( i = 0 ; i < sizeof(pre_tracking_utilization)/sizeof(pre_tracking_utilization[0]) ; i ++ )
	{
		pre_tracking_utilization[i] = 0 ;
	}
	pre_max_utilization_time = 0 ;
	pre_max_utilization = 0 ;	
}


///============================================================================
/// Navigation Task thread routine
///============================================================================
void SWI_NavTask(void)
{
	int tm_output ;
	int64 current_t ;
	int local_post_nav_aligned ;
	
#ifdef DELAYED_OPMGRINIT
extern int delayed_OPMgr_Init ;
extern int new_OrbStatic ;
extern uint8 new_Synchronization ;

	if ( delayed_OPMgr_Init )
	{
		OPMgr_Init(&OPManagerState, new_OrbStatic) ;
		
		NavTaskState.Synchronization = new_Synchronization ;

		delayed_OPMgr_Init = 0 ;
	}
	
#endif
	  
  /// navigation task process
  NavTask_Process(&NavTaskState, &SatelliteManagerState, &OPManagerState, &FrameworkState.weekData, 
                  &FrameworkState.FlashDrv.flashData.controlWN, MainManagerState.config.OrbStatic,
				  MainManagerState.BinStrFlag, &MainManagerState.TestStr, &FrameworkState.DebugTerminal);
  /// OP solution obtaining
  if ((NavTaskState.NavCond.OPmode == 1) && (NavTaskState.Synchronization != NO_SYNC))
  {
    OPMgr_Process(&OPManagerState, &SatelliteManagerState.TopSystem, &NavTaskState.NavCond, 
                 NavTaskState.User, NavTaskState.NavSolution, FrameworkState.weekData,
				 MainManagerState.BinStrFlag, &MainManagerState.TestStr, &FrameworkState.DebugTerminal);
    ControlOPAndNavigation(&NavTaskState, &OPManagerState, &SatelliteManagerState, 
                            MainManagerState.config.OrbStatic, MainManagerState.BinStrFlag, 
                            &MainManagerState.TestStr, &FrameworkState.DebugTerminal);
  }
  else if (NavTaskState.User.SolutionFlag && ((NavTaskState.User.CoordValid == ERROR_NAV_TASK) || (NavTaskState.User.CoordValid == RAIM_ERROR)))
  {
    /// if error of navigation task solution is occur then clear User parameters
    ClearUser(&NavTaskState, &SatelliteManagerState);
  }
  /// if navigation task solution is exist then time correct
  if (NavTaskState.User.SolutionFlag && (NavTaskState.User.CoordValid == OK_NAV_TASK))
  {
    while (NavTaskState.CorrectTime.flag_bl == 1);
    NavTaskState.CorrectTime.flag_bl = 1;
    if (NavTaskState.CorrectTime.FlagCorrectTime == fg_OFF)
    {
      NavTaskState.CorrectTime.R_nts           = NavTaskState.User.R;
      NavTaskState.CorrectTime.Rdot_nts        = NavTaskState.User.Rdot;
      NavTaskState.CorrectTime.FlagCorrectTime = fg_ON;
    }
    NavTaskState.CorrectTime.flag_bl = 0;
  }
  /// setting satellite status
  SatellitesStatus(&NavTaskState, &SatelliteManagerState);

// to fix message output not aligned to "multiply of NavSolRate" during/after OP Init(synchronization)
	tm_output = 1 ;
	local_post_nav_aligned = post_nav_aligned = 1 ;
	
	if (NavTaskState.Synchronization == SYNC_PROCESS) 
		if (NavTaskState.enableMessages != 1)
			tm_output = 0 ;
	
	if ( tm_output )
	{
		// get current navigation time
		if (NavTaskState.NavSolution == 2)	
			current_t = NavTaskState.User.User_fix.t ;	// NAV solved
		else	
			current_t = (int64)((NavTaskState.NavCond.t - NavTaskState.CorrectTime.Rnc*l_cLight)*1.e+6 + 0.5);  // NAV not solved
		
		// enter critical section
		// extra_ms_post_nav & post_nav_aligned may be modified in Tracking thread
		HWI_disable();
		// post_nav_aligned: flag of "aligned to multiply of NavSolRate" for this post
		// extra_ms_post_nav: extra ms longer than expected post time for this post
		extra_ms_post_nav = (current_t/1000) % MainManagerState.config.NavSolRate ;	
		if ( extra_ms_post_nav != 0 )
		{
			local_post_nav_aligned = post_nav_aligned = 0 ;	// will trigger Tracking thread to adjust next post time
			tm_output = 0 ;	// do not output message for this post
		}		
		// leave critical section
		HWI_enable();
	}
		
	if ( tm_output )
	{	
#ifdef FASTER_TRACKING
#ifdef NEW_TMTC
		if ( tmtc_mode == DETAIL_MODE )
#endif	// end NEW_TMTC
		{
		if ( ((MainManagerState.Message&0x8) == 0x8))
			MainMgr_NMEARawMeasur_2(&SatelliteManagerState);
		}
#endif

		MessagesForming(&NavTaskState, &SatelliteManagerState, &OPManagerState, 
							MainManagerState.Message, &MainManagerState.data, &FrameworkState.DebugTerminal);
							
		// Alex, 20140620, NEW TMTC ICD
		#ifdef NEW_TMTC
		if ( tmtc_mode == BRIEF_MODE )
			output_new_tm_message(&MainManagerState, &FrameworkState, &SatelliteManagerState, &NavTaskState, &OPManagerState) ;
		#endif	// end NEW_TMTC
	}

	can_or_uart = pre_can_or_uart;
	
//	dbg_printf("t=%u, tm=%d, sync=%d, al=%d, ex=%d", (uint32)(current_t/1000), tm_output, NavTaskState.Synchronization, local_post_nav_aligned, extra_ms_post_nav);
	
	SatMgr_AnalyzeAlmanacUser(&SatelliteManagerState, &NavTaskState.User, &MainManagerState.initMes);
  
	if ( tm_output )
		performance_report();
		
  /// work thread finished
  
	NavTaskState.NavTaskEnd = fg_ON;
}

///============================================================================
/// Search thread routine
///============================================================================
void IDLE_SearchThread(void)
{
  static int32 *pSamples;  ///< pointer to work buffer

  /// check search status
  if (SatMgr_CanStartFSearch(&SatelliteManagerState) == fg_ON)
  {
    /// check samples number and its age
    if (SmpMgr_NeedStartFastSearchBuf(&SampleManagerState) == fg_ON)
    {
      /// start samples forming for search
      SmpMgr_StartFastSearchBuf(&SampleManagerState);
      SatelliteManagerState.count_search = 0;
    }
    /// check samples forming process
    if (SmpMgr_IsFinishedFastSearchBuf(&SampleManagerState))
    { 
      /// get pointer to work samples
      pSamples = SmpMgr_GetPackedSignal(&SampleManagerState);
      /// samples is old
      SampleManagerState.SamplesOld = fg_ON;
      /// samples processed
      SatMgr_FSearch(&SatelliteManagerState, pSamples);
#ifdef EXT_SRAM	  
	  memset(myFastSearchBuf, 0, sizeof(myFastSearchBuf));
#else
      memset(SampleManagerState.FastSearchBuf, 0, sizeof(SampleManagerState.FastSearchBuf));
#endif	  
    }
  }
  
  if ( new_boot_config != -1 )
  {
	change_boot_config(new_boot_config) ;
	new_boot_config = -1 ;
  }
}

/// for target
#ifdef USE_DSPBIOS


#ifdef EXT_SRAM
#pragma CODE_SECTION (HWI_sampleDRV, "fast_text")
#endif

///============================================================================
/// Hardware interrupt handler routine
///============================================================================
void HWI_sampleDRV(void)
{
  int32 flags = *(volatile int32 *)0x01a0ffe4;  ///< interrupts flag

  /// samples interrupt
  if((flags & 0x100) != 0)
  {
    /// clearing samples interrupt flag
    *(volatile int32 *)0x01a0ffe4 |= 0x100;
    /// test TCXO
    if (FrameworkState.TCXOtest == fg_ON)
    {
      uint32 timer = PROFILE_STOP();
      /// measure tacts number to one ms
      if (FrameworkState.StartTestTCXO == 0)
        FrameworkState.StartTestTCXO = 1;
      else
      {
        if ((fabs((double)timer-480000.0)*100.0/480000.0) > 1.0)  /// error if more then 5 %
          FrameworkState.TCXOerr = fg_ON;
      }

      PROFILE_START();
    }

    /// post tracking manager thread in work mode
    if (FrameworkState.BITFlag == fg_OFF)
	{
#ifdef HWI_SCHEDULING_TEST		
		tracking_called_cnt++;
#endif
      FrameworkState.EventHandler.pfnPostTrackingManager(FrameworkState.EventHandler.pHost);
	}
	
if (MainManagerState.flags.RESET )
{	
    /// software reset command
    if (reset_type == 1)
    {
      /// disable interrupt
      HWI_disable();
      /// save software reset flag
      ADDR_FLAG = SOFTWARE_RESET;
	  
	  BOOT_STATUS = boot_status; 

      /// stop EDMA and channel interrupt disable
      *(volatile int32 *)0x01a0fff4 = 0x00000000;
      *(volatile int32 *)0x01a0ffe8 = 0x00000000;
      /// start system
      c_int00();
    }
    /// reboot command
    if (reset_type == 2)
    {	
      /// disable interrupt
      HWI_disable();
      /// save reboot flag
      ADDR_FLAG = HARDWARE_RESET;	
	  
	  *(volatile uint8 *)0x68000002 = 0x0;  //v.703 - disableFIFO
	  *(volatile uint16 *)0x68000024 = 0x0004; // v703- disUART
      /// stop EDMA and channel interrupt disable
      *(volatile int32 *)0x01a0fff4 = 0x00000000;
      *(volatile int32 *)0x01a0ffe8 = 0x00000000;
#if 1
	{
		unsigned char tmp ;
		
		tmp = *(volatile uint8 *)0x6800000e ;
		tmp |= 0x10 ;	// set bit 4
		*(volatile uint8 *)0x6800000e = tmp ;
	}
#else
	  
	    /// Interrupt disable
//	  *(volatile int32 *)0x01a0ffe8 = 0x000; 
		/// Interrupt Flags clearing
//	  asmWriteICR();
		IRQ_resetAll() ;

	  
	  {
		void (*rst_vector)(void) = 0 ;

	CACHE_wbAllL2(CACHE_WAIT) ;	// write back all L2 cache
	CACHE_setL2Mode(CACHE_1024KSRAM) ;	// restore L2 to all SRAM
	CACHE_reset() ;	// reset cache to power-on
	
     /// copy first 1KB from flash to ISRAM
		*(volatile int *)0x01a80004 = 0x66761907;  // CE1 FLASH 8-bit
		_memcpy((void *)0, (void *)0x64000000, 0x400) ;
		
		(*rst_vector)() ;
      }
#endif	  
    }
}
	
    if (MainManagerState.flags.BIT == 1)
    {
      /// disable interrupt
      HWI_disable();
      /// save Built-in test flag
      ADDR_FLAG = BUILT_IN_TEST;
	  
	  BOOT_STATUS = boot_status; 
	  
      /// stop EDMA and channel interrupt disable
      *(volatile int32 *)0x01a0fff4 = 0x00000000;
      *(volatile int32 *)0x01a0ffe8 = 0x00000000;
      /// start system
      c_int00();
    }
  }
}

///============================================================================
/// Hardware interrupt handler routine
///============================================================================
void HWI_Timer0(void)
{
  Timer0Stop();                    /// stop timer
  FrameworkState.Timeout = fg_ON;  /// raise timeout flag
  return;
}

#endif
