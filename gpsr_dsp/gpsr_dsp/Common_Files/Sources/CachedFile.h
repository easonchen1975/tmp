/*****************************************************************************
*    Copyright (c) 2001-2002 Spirit Corp.
******************************************************************************
*
*    Project: ............   HF modem MIL-STD-188-110A
*    Title: ..............   Cached file operations
*    File name: ..........   CachedFile.c
*    Authors: ............   Alex G. Nazarov
*    Revision ............   1.0
*
*    Subroutines:
*	   CachedFileOpen     open file or returns handle of file if it has been 
*                       already opened
*	   CachedFileClose    close cached file
*	   CachedFileRemove   remove file and delete appropriate entry in the cached 
*                       list
*    CachedFileCloseAll close all cached files
*
*    Internal routines:
*    CachedFileAutoClose  close cached files on timeout
*
*****************************************************************************/
#ifndef CachedFile_H___
#define CachedFile_H___	1

#include "comtypes.h"

/****************************************************************************
  Description: open file or returns handle of file if it has been already opened 
 
  Input:
  filename       name of file to be opened

  returns file handle
****************************************************************************/
EXTERN_C FILE* CachedFileOpen (const char* filename);

/****************************************************************************
  Description: close cached file
 
  Input:
  f       file handle to be closed
****************************************************************************/
EXTERN_C void  CachedFileClose (FILE *f);

/****************************************************************************
  Description: remove file and delete appropriate entry in the cached list
 
  Input:
  filename       name of file to be removed
****************************************************************************/
EXTERN_C void CachedFileRemove (const char* filename);

/****************************************************************************
  Description: close all cached files
****************************************************************************/
EXTERN_C void  CachedFileCloseAll ();

#endif //#ifndef CachedFile_H___
