/*****************************************************************************
*    Copyright (c) 2000 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Enhanced square root computation: 
 *                            Newton-Rafson algorithm
 *    @file                   L_sqrt_l.c
 *    @author                 A.Nazarov
 *    @version                1.0
 */
/*****************************************************************************/

/// do not compile when it must be linked as a part of common SPCORP library
#include "FractOper.h"
#include "l_sqrt_l.h"

#ifndef __L_SQRT_S_L
#define __L_SQRT_S_L

// approximate: compute y(Q31) = sqrt(x(Q31))
#if PORTING_L_SQRT_L==1
  inline
#endif

//=========================================================================
/// @brief      Computes square root of Q15 number
///             iterate 4 times: max error 4.e-5.
///             Precision conversion: Q result = Q((x-1)/2 + 16), when Qx is odd
///
/// @param[in]  x - int32 input value
/// @return     int32 square root of input
//=========================================================================
int32 L_sqrt_l(int32 x)
{
  int16 exp=Exp(x) & 0xFFFE;
  x <<= exp;
  x = L_deposit_h(L_sqrt_s (extract_h(x)));
  x >>= (exp>>1);
  return x;
}

#endif  // __L_SQRT_S_L
