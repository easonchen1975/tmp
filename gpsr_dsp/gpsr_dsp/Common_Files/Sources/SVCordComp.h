/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  compute SV coordinations
 *    @file                   SVCordComp.h
 *    @author                 A. Baloyan
 *    @date                   25.06.2005
 *    @version                1.0
 */
/*****************************************************************************/


#ifndef _SVCORDCOMP_H_
#define _SVCORDCOMP_H_

#include "Control.h"
#include "Satellite.h"

//=======================================================================================
///  @brief     Compute GPS SV position from time and ephemeris data
///
///  @param[in]  pThis     - pointer to GPS satellite coordinates
///  @param[in]  pEphGps   - pointer to GPS ephemeris structure
///  @param[in]  sec       - desired time, seconds of day
//=======================================================================================
EXTERN_C void SatGPS_EphCoordCalc(tCoord *pThis, EPH_GPS *pEphGps, float64 sec);

#endif //_SVCORDCOMP_H_
