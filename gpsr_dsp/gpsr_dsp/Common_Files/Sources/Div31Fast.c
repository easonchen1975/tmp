/*****************************************************************************
 *    Copyright (c) 2002 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Fast fractional Q31 by Q31 division
 *    @file                   Div31Fast.c
 *    @author                 Andrey Bykov, Dima Paroshin
 *    @date                   20.11.2002
 *    @version                1.0
 */
/*****************************************************************************/
#include "Div31Fast.h"
#include <math.h>

//======================================================================
/// Fast division of two Q31 fractional numbers using fast calculation 
/// of 1/x based on Newton-Rafson method. Absolute value of numenator 
/// must be less than denumenator. Result in Q31 format saturated on 
/// overflow. Maximum absolute error comparing to conventional 32-bit
/// division is 6 (in case of 0x7ffffffe / 0x7fffffff). Maximum division 
/// error is 3.26e-09 comparing to conventional 32-bit division error 
/// equal to 4.66e-10.
///
/// @param[in]  Num - Q31 numenator
/// @param[in]  Den - Q31 denumenator
/// @return     Q31 value
//======================================================================
#ifndef  __DIV31FAST_C
#define  __DIV31FAST_C


#if PORTING_DIV31FAST == 0

int32 Div31FastCversion(int32  Num, int32 Den)
{
  uint16 Sign, Shift, i;
  uint32 UNum, UDen, Quot, UNum16, UDenQ16, Tmp;
  int32 Res, Temp;

  Sign = (uint16)(((Num ^ Den)&0x80000000)>>31);
  UNum = labs(Num);
  UDen = labs(Den);
  // Check for overflow condition
  if (UDen <= UNum)
  {
    Res = (Sign != 0) ? 0x80000000L : 0x7FFFFFFFL;
    return (Res);
  }
  // Convert denumenator to Q16, remember shift value
  Shift = Exp(UDen);
  UDenQ16 = ((UDen << Shift) << 1) >> 16;
  // Calculate first approximation of 0.5/x using Q32/Q16 division 
  UNum16 = 0x7FFF0000L;
  for (i = 0; i < 16; i++)    // SUBC algorithm
  {
    Temp = UNum16 - (UDenQ16 << 15);
    if (Temp >= 0)
      UNum16 = (Temp << 1) + 1;
    else
      UNum16 = UNum16 << 1;
  }
  // Convert result to Q30
  Quot = (UNum16 & 0x0000ffffL) << 15;

  // Equation  Quot = Quot + (1 - Den * Quot) * Quot to prevent overflow converted to 
  // Quot/2 = Quot/2 + (0.5 - Den * Quot/2) * 2 * Quot/2

  // Normalize denumenator 
  UDen = UDen << Shift;
  Tmp = Mul31(UDen, Quot);
  Tmp = 0x3FFFFFFFL - Tmp;
  Tmp = Mul31(Tmp << 1, Quot);
  Tmp = Tmp + Quot;
  // Convert numenator to denumenator format
  UNum = UNum << Shift;
  // Calculate (0.5/x) * y * 2
  UNum = Mul31(UNum, Tmp) << 1;
  // Speaks for itself
  Res = (Sign != 0) ? -(int32)UNum : (int32)UNum;
  return (Res);
}

#endif // PORTING_DIV31FAST

#if ((PORTING_DIV31FAST==1) && defined(_TMS320C6400))

inline  int32 Div31FastC64(int32  Num, int32 Den)
{
  uint16 Sign, Shift;
  int16  i;
  uint32 UNum, UDen, Quot, UNum16, UDenQ16, Tmp;
  int32 Res;

  Sign = (uint16)(((Num ^ Den)& 0x80000000)>>31);
  UNum = _abs(Num);
  UDen = _abs(Den);
  // Check for overflow condition
  if (UDen <= UNum)
  {
    Res = (Sign != 0) ? 0x80000000L : 0x7FFFFFFFL;
    return (Res);
  }
  // Convert denumenator to Q16, remember shift value
  Shift = Exp(UDen);
  UDenQ16 = ((UDen << Shift) << 1) >> 16;
  // Calculate first approximation of 0.5/x using Q32/Q16 division
  UNum16 = 0x7FFF0000L;
  for (i = 0; i < 16; i++)   // SUBC algorithm
  {
    UNum16 =  _subc(UNum16, UDenQ16 << 15);
  }
  //  Convert result to Q30 
  Quot = (UNum16 & 0x0000ffffL) << 15;

  // Equation  Quot = Quot + (1 - Den * Quot) * Quot to prevent 
  // overflow converted to Quot/2 = Quot/2 + (0.5 - Den * Quot/2) * 2 * Quot/2

  // Normalize denumenator
  UDen = UDen << Shift;
  Tmp = Mul31(UDen, Quot);
  Tmp = 0x3FFFFFFFL - Tmp;
  Tmp = Mul31(Tmp << 1, Quot);
  Tmp = Tmp + Quot;
  // Convert numenator to denumenator format
  UNum = UNum << Shift;
  //   Calculate (0.5/x) * y * 2
  UNum = Mul31(UNum, Tmp) << 1;
  // Speaks for itself
  Res = (Sign != 0) ? -(int32)UNum : (int32)UNum;   
  return (Res);
}
#endif  // ((PORTING_DIV31FAST==1) && defined(_TMS320C6400))

#endif // __DIV31FAST_C
