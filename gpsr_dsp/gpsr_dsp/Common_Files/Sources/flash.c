/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        Write data in flash
 *   @file         Flash.c
 *   @author       B. Oblakov, M. Zhokhova
 *   @date         09.03.2006
 *   @version      1.0
 */
/*****************************************************************************/

#include "flash.h"
#include "Framework.h"

//=============================================================================
/// Flash driver initialize
/// 
/// @param[in]  pThis - pointer to FLASH object
//=============================================================================
void FrameWork_FlashInit(tFlashDrv *pThis)
{
  pThis->flashData.controlFile.numOfBlocks  = 1;  ///< for upload system file
  pThis->flashData.controlFile.currentBlock = 1;  ///< for download system file

  pThis->address[FL_SYSTEM]    = 0x64000B00;  ///< system file address in FLASH
  pThis->address[FL_EPHEMERIS] = 0x6403F000;  ///< ephemeris address in FLASH
  pThis->address[FL_ALMANAC]   = 0x6403F840;  ///< almanac address in FLASH
  pThis->address[FL_CONFIG]    = 0x6403FD00;  ///< configuration setup start address
  pThis->address[FL_WN]        = 0x6403FD16;  ///< week data start address
  pThis->address[FL_TEST]      = 0x6403FF80;  ///< address FLASH for test
}

//=============================================================================
/// Data write-in flash
/// 
/// @param[in]  pThis  - pointer to Flash driver
/// @param[in]  addr   - start address
/// @param[in]  length - data length
/// @param[in]  data   - pointer to data
//=============================================================================
void FrameWork_FlashProgramming(tFlashDrv *pThis, 
                                uint32     addr, 
                                uint8      length, 
                                uint8     *data)
{
  int    i;
  uint32 base;
  uint32 addr0;
  
  if (pThis->StepProgramming == fg_OFF)
  {
    i     = 0;
    addr0 = addr;
  }
  else
  {
    i     = pThis->len_programming;
    addr0 = addr + pThis->len_programming;
    pThis->StepProgramming = fg_OFF;
  }

  if (addr < FLASH2_ADDRESS)
    base = FLASH1_ADDRESS;
  else
    base = FLASH2_ADDRESS;

  /// Writing commands sequence
  *(uint8 *)(base + 0x5555) = 0xaa;
  *(uint8 *)(base + 0x2aaa) = 0x55;
  *(uint8 *)(base + 0x5555) = 0xa0;

  for(;i<length;i++)
  {
    *(uint8 *)addr0 = data[i];
	  addr0++;
    if (((addr0%PAGE_SIZE) == 0) && (length != (i+1)))
    {
      pThis->StepProgramming = fg_ON;
      pThis->len_programming = (i+1);
      break;
    }
  }
}

//============================================================================
/// Data write-in flash test
/// 
/// @param[in]  addr - end address
/// @param[in]  data - end data
/// @return     0 if data not write, 1 - data write
//============================================================================
uint8 FrameWork_ControlFlashProgramming(uint32 addr, uint8 data)
{
  uint8 DQ_flags;
  
  /// Data# Polling bit
  DQ_flags = *(volatile uint8 *)(addr);
	if (DQ_flags == data)
	  return FINISH;
  else
    return PROCESS;
}

//=============================================================================
/// Data write and control to flash
/// 
/// @param[in]  pThis  - pointer to FLASH object
/// @param[in]  addr   - start address
/// @param[in]  length - data length
/// @param[in]  data   - pointer to data
/// @return     0 if data not write, 1 - data write, 2 - error to write
//=============================================================================
uint8 FrameWork_FlashProgrammCntrl(tFlashDrv *pThis,  uint32  addr, 
                                   uint8      length, uint8  *data)
{
  int8 res_save = 0;  ///< result of save

  if (pThis->FlagProgramming == fg_OFF)
  {
    FrameWork_FlashProgramming(pThis, addr, length, data);
    pThis->FlagProgramming = fg_ON;
    pThis->cnt_programming = 0;
  }
  else
  {
    if (pThis->StepProgramming == fg_OFF)
      res_save = FrameWork_ControlFlashProgramming((addr + length - 1), data[length-1]);
    else
      res_save = FrameWork_ControlFlashProgramming((addr + pThis->len_programming - 1), data[pThis->len_programming-1]);
  }
  if (res_save == FINISH)
  {
    pThis->FlagProgramming = fg_OFF;
    if (pThis->StepProgramming == fg_OFF)
      return FINISH;
    else
      return PROCESS;
  }
  else if (pThis->cnt_programming > TIME_ERROR)
  {
    pThis->FlagProgramming = fg_OFF;
    pThis->StepProgramming = fg_OFF;
    return ERROR_FLASH;
  }
  else
  {
    pThis->cnt_programming++;
    return PROCESS;
  }
}








