/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  FIFO datatype implementation
 *   @file                   FIFO.c
 *   @author                 M. Silchev
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*****************************************************************************/

#include "FIFO.h"


// ========================================================================
/// @brief     FIFO driver initialization
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the buffer 
/// @param[in] Size    - FIFO buffer size
// ========================================================================
void FIFO_Init (register tFIFO *pFIFO, tFifoItem *pBuffer, unsigned int Size)
{
  pFIFO->iHead =
  pFIFO->iTail = 0;
  pFIFO->nSize = Size;
  pFIFO->pData = pBuffer;
}

// ========================================================================
/// @brief     FIFO driver starting
/// @param[in] pFIFO   - pointer to FIFO structure
// ========================================================================
void FIFO_Start (register tFIFO *pFIFO)
{
  pFIFO->iHead =
  pFIFO->iTail = 0;
}


#ifdef EXT_SRAM
#pragma CODE_SECTION (FIFO_GetFilledSize, "fast_text")
#endif

// ========================================================================
/// @brief     Gets amount of data available in FIFO
/// @param[in] pFIFO - pointer to FIFO structure
// ========================================================================
int FIFO_GetFilledSize (register tFIFO *pFIFO)
{
  int FilledSize;

  FilledSize = pFIFO->iHead - pFIFO->iTail;

  if (FilledSize < 0)
    FilledSize += pFIFO->nSize;

  return FilledSize;
}


#ifdef EXT_SRAM
#pragma CODE_SECTION (FIFO_GetFreeSize, "fast_text")
#endif

// ========================================================================
/// @brief     Gets amount of free space in FIFO
/// @param[in] pFIFO - pointer to FIFO structure
// ========================================================================
int FIFO_GetFreeSize (register tFIFO *pFIFO)
{
  int FreeSize;

  FreeSize = pFIFO->nSize - FIFO_GetFilledSize(pFIFO) - 1;

  return FreeSize;
}

// =======================================================================================
/// @brief     Gets several data elements from FIFO
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the buffer which receives data
/// @param[in] Count   - amount of data to get
// =======================================================================================
void FIFO_GetBuffer (register tFIFO *pFIFO,register tFifoItem *pBuffer,unsigned int Count)
{
  register int iTail = pFIFO->iTail;

  while (Count-- > 0)
  {
    *pBuffer++ = pFIFO->pData[iTail++];

    if (iTail >= (int)pFIFO->nSize)
      iTail = 0;
  }
  pFIFO->iTail = iTail;
}

// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC

void FIFO_CopyBuffer (register tFIFO *pFIFO,register tFifoItem *pBuffer,unsigned int Count)
{
  register int iTail = pFIFO->iTail;

  while (Count-- > 0)
  {
    *pBuffer++ = pFIFO->pData[iTail++];

    if (iTail >= (int)pFIFO->nSize)
      iTail = 0;
  }
//  pFIFO->iTail = iTail;
}

#endif	// end NEW_TMTC

// =============================================================================================
/// @brief     Puts several data elements from FIFO
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the data buffer
/// @param[in] Count   - amount of data to put
// =============================================================================================
void FIFO_PutBuffer (register tFIFO *pFIFO,register const tFifoItem *pBuffer,unsigned int Count)
{
  int iHead = pFIFO->iHead;

  while (Count-- > 0)
  {
    pFIFO->pData[iHead++] = *pBuffer++;

    if (iHead >= (int)pFIFO->nSize)
      iHead = 0;
  }
  pFIFO->iHead = iHead;
}

// =====================================================================================
/// @brief     Checks free space amount & puts several data elements from FIFO
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the data buffer
/// @param[in] Count   - amount of data to put
// =====================================================================================
bool FIFO_PutBufferVerified(tFIFO *pFIFO, const tFifoItem *pBuffer, unsigned int Count)
{
  if((unsigned int)FIFO_GetFreeSize(pFIFO) < Count)
    return false;

  FIFO_PutBuffer(pFIFO, pBuffer, Count);
  return true;
}
