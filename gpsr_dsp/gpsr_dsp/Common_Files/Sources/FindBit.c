/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  PLL initialize
 *   @file                   FindBit.c
 *   @author                 A. Nazarov
 *   @date                   26.07.2005
 *   @version                1.0
 */
/*****************************************************************************/

#include "FindBit.h"
#include <math.h>
#include "baseop.h"
#include "comfuncs.h"
#include "Exp.h"
#include "FFT32.h"
#include "L_sqrt_l.h"
#include "Div31Fast.h"

//========================================================================
/// Doppler frequency estimation
/// 
/// @param[in]  pInput  - pointer to 32-points signal spectrum
/// @param[in]  posMax  - position of max spectral component
/// @return     freqDop - estimated frequency in Q16 
// ========================================================================
static int32 Fest_fix(tComplex16Array_32  *pInput, int posMax)
{ 
  int32 A_1, A1, A0, Asumm;
  int32 freqDop;
  int16 k;
  int16 re, im;
  re  = pInput->iData[posMax].c.re;  im = pInput->iData[posMax].c.im;
  A0  = L_sqrt_l(mpy(re,re) + mpy(im,im))>>3;
  k   = (posMax+1)&0x1F;  // k = (kmax==31) ? 0: kmax+1;
  re  = pInput->iData[k].c.re;    	im = pInput->iData[k].c.im;
  A1  = L_sqrt_l(mpy(re,re) + mpy(im,im))>>3;
  k   = (posMax-1)&0x1F;  // k = (kmax==0) ? 31: kmax-1;
  re  = pInput->iData[k].c.re;   	im = pInput->iData[k].c.im;
  A_1 = L_sqrt_l(mpy(re,re) + mpy(im,im))>>3;
  Asumm = 2*(A1 + A_1 - 2*A0);
  // 31.25 Hz - FFT filter band with
  {
    int32 rem;
    int16 expAsumm=Exp0(Asumm);
    rem = Div31Fast((A_1 - A1),Asumm<<expAsumm);
    rem = L_shl(rem,(int16)(expAsumm-(31-9)));
    rem = rem + (posMax<<9);	// Q9
    rem = mpy((int16)rem, (int16)(31.25*128));	// Q16
    if(rem>(500L<<16)) rem-= 1000L<<16;
    freqDop  = rem;
  }
  return freqDop;
}


#ifdef EXT_SRAM
#pragma CODE_SECTION (FindBit, "fast_text")
#endif

//========================================================================
/// Search for bit bound and Doppler frequency estimation
/// 
/// @param[out] result - pointer to result structure
/// @param[in]  pCorr  - pointer to 32-points signal spectrum
/// @param[in]  M      - number of nonzero samples in pCorr
//========================================================================
void FindBit (tFindBitResult* result, const tComplex32* pCorr, int M)
{
  int j,m;
  int32 Y;
  int16 kmax = -1;
  int32 Amax = -1, AmaxMax = -1;
  tComplex16Array_32  maxout;
  tComplex16Array_32  x;
  tComplex16Array_32  y;
  tComplex16Array_32  fft32_tmp;
  int16 minExpRe,minExpIm;
  int16 i,scale;

  // calculate input scaling
  minExpRe = minExpIm = 32;
  for (i=0; i<32; i++)
  {
    minExpRe = Min(minExpRe, Exp0( pCorr[i].re));
    minExpRe = Min(minExpRe, Exp0(-pCorr[i].re));
    minExpIm = Min(minExpIm, Exp0( pCorr[i].im));
    minExpIm = Min(minExpIm, Exp0(-pCorr[i].im));
  }
  scale = Min(minExpRe,minExpIm) - 16 - 3-1;

  for(m=0; m<M; m++)  
  {  
    for(j=0; j<32; j++)
    {
      if(j<m)
      {
      x.iData[j].c.re = (int16)L_shl(-pCorr[j].re,scale);
      x.iData[j].c.im = (int16)L_shl(-pCorr[j].im,scale);
      }
      else
      {
      x.iData[j].c.re = (int16)L_shl(pCorr[j].re,scale);
      x.iData[j].c.im = (int16)L_shl(pCorr[j].im,scale);
      }
    }
    fft32(&x,&y,&fft32_tmp);
    // search on Doppler
    for(j=0; j<32; j++)
    { 
      Y = mpy(y.iData[j].c.re,y.iData[j].c.re) + mpy(y.iData[j].c.im,y.iData[j].c.im);
      if(Y > Amax) 
      { 
      Amax = Y; 
      kmax = j; 
      }
    }
    if(Amax > AmaxMax)
    {
      AmaxMax = Amax;
      result->bitBound = m;
      maxout = y; 
    }
  }
  result->freq = Fest_fix(&maxout,kmax);
}
