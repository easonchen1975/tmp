/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Short FFT with scaling (typical MSE~3.5e-4)
 *    @file                   FFT32.h
 *    @author                 A. Nazarov
 *    @date                   25.07.2005
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef FFT32_H__
#define FFT32_H__

/// complex array of 32 numbers aligned on 8-byte boundary
typedef union 
{
  int16             sData[64];
  tComplex16Aligned iData[32];
  double            tmp  [16];
  tDoubleWord       dData[16];
} 
tComplex16Array_32;

//==================================================================================
/// Special form of FFT of length 32. 
/// Bit-exactly converted from reference code from fft16x16c.c
///
/// @param[in]  ptr_x       - array of 32 complex samples.
///                           It is changed during execution of this routine
/// @param[in]  ptr_y       - output array of spectrum components
/// @param[in]  pTempData31 - temporary data for FFT
//==================================================================================
EXTERN_C void fft32(tComplex16Array_32 *ptr_x,
                    tComplex16Array_32 *ptr_y,
                    tComplex16Array_32 *pfft32_tmp);

#endif  // FFT32_H__
