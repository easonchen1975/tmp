/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS 3x12 Receiver
 *    @brief                  General constant and type for channels
 *    @file                   GPS.h
 *    @author                 A. Nazarov
 *    @date                   18.08.2005
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef GPS_H__
#define GPS_H__
#include "comtypes.h"

/// general type
typedef struct 
{
  const tComplex8Aligned *pSamples;  /// pointer to contiguous 2 ms of data
}
tEpoch;

/// general constant
#define GPSCHAN_SAMPLERATE  (8184)

#endif //GPS_H__
