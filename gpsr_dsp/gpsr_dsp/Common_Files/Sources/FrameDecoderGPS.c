/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Frame decoder
*    @file                   FrameDecoderGPS.c
*    @author                 A. Baloyan
*    @date                   25.06.2005
*    @version                1.0
*/
/*****************************************************************************/
#include "satellite.h"
#include "FrameDecoderGPS.h"

//============================================================================================
/// @brief     Extract a value from GPS sub frame that is occupying some part of one word
/// @param[in] frame     - pointer to GPS bit sub frame
/// @param[in] word      - word's number in bit sub frame
/// @param[in] first_bit - first bit position in the word
/// @param[in] N_bits    - number of bits in extracted value
/// @param[in] sign      - {1} - signed value, {0} - unsigned value
//============================================================================================
static int32 OneSegInt(int32 *frame,
                       int32  word,
                       int32  first_bit,
                       int32  N_bits,
                       int32  sign);

//============================================================================================
/// @brief     Extract a value from GPS sub frame that is occupying some part of two words
/// @param[in] frame      - pointer to GPS bit sub frame
/// @param[in] word1      - word's number in bit sub frame
/// @param[in] word2      - word's number in bit sub frame
/// @param[in] first_bit1 - first bit position in the word
/// @param[in] first_bit2 - first bit position in the word
/// @param[in] N_bits1    - number of bits in extracted value
/// @param[in] N_bits2    - number of bits in extracted value
/// @param[in] sign       - {1} - signed value, {0} - unsigned value
//============================================================================================
static int32 TwoSegInt(int32 *frame,
                       int32 word1,
                       int32 first_bit1,
                       int32 N_bits1,
                       int32 word2,
                       int32 first_bit2,
                       int32 N_bits2,
                       int32 sign);

//============================================================================================
/// @brief     Fills up the sub frame by bits
/// @param[in] frame - pointer to bit sub frame
/// @param[in] bit   - input bit
//============================================================================================
static void AddBitToSubframe(int32* frame, int8 bit);

//============================================================================================
/// @brief      Checks GPS sub frame parity
/// @param[in]  frame  - pointer to GPS bit sub frame
/// @return     check result
//============================================================================================
static int32 CheckFrameParityGPS(const  int32 *frame);

//============================================================================================
/// @brief      Detects the Preamble in GPS bit frame
/// @param[in]  frame - pointer to GPS bit sub frame
/// @param[in]  first  - first word number
/// @param[in]  second - second word number
/// @return     1 - preamble was detected and parity of first tow word is OK, 0 - otherwise
//============================================================================================
static int32 DetectPreamble(const  int32 *frame, uint8 first, uint8 second);

//============================================================================================
/// @brief     Decode raw GPS SV's almanac from frame
/// @param[in] Alm   - pointer to GPS SV's almanac structure
/// @param[in] frame - pointer to bit frame
//============================================================================================
static void DecodeAlmGPS(RAW_ALM_GPS* Alm, int32 *frame);

//============================================================================================
/// @brief     Decode raw ION and UTC parameters from frame
/// @param[in] PtrIon - pointer to raw ION and UTC parameters structure
/// @param[in] frame  - pointer to bit frame
//============================================================================================
static void DecodeION(RAW_ION_UTC *PtrIonUTC, int32 *frame);

//============================================================================================
/// @brief      Fills up the sub frame by bits
/// @param[in]  frame - pointer to bit sub frame
/// @param[in]  bit   - input bit
//============================================================================================
static void  AddBitToSubframe(int32* frame, int8 bit);

//============================================================================================
/// @brief      Decodes GPS bit frame
/// @param[in]  sv_num  - ID satellite
/// @param[in]  common  - pointer to structure with temporary data
/// @param[in]  decoder - pointer to Decoder
//============================================================================================
static void  DecodeSubframeGPS(const int8      sv_num,
                               tDecoderCommon *common,
                               tDecoder       *decoder);
 
//============================================================================================
/// @brief     Extract a value from GPS sub frame that is occupying some part of one word
/// @param[in] frame     - pointer to GPS bit sub frame
/// @param[in] word      - word's number in bit sub frame
/// @param[in] first_bit - first bit position in the word
/// @param[in] N_bits    - number of bits in extracted value
/// @param[in] sign      - {1} - signed value, {0} - unsigned value
//============================================================================================
static int32 OneSegInt(int32 *frame,
                       int32  word,
                       int32  first_bit,
                       int32  N_bits,
                       int32  sign)
{
  int32 bWord = frame[10 - word];
  
  // Check last bit in the previous word and word correcting
  if(frame[11 - word] & 0x1) 
    bWord = ~bWord;
  
  // Value separation and return
  bWord <<= (first_bit + 1);
  bWord >>= 32 - N_bits;

  return sign ? bWord : bWord & (((int32)0x1 << N_bits) - 1);
}

//============================================================================================
/// @brief     Extract a value from GPS sub frame that is occupying some part of two words
/// @param[in] frame      - pointer to GPS bit sub frame
/// @param[in] word1      - word's number in bit sub frame
/// @param[in] word2      - word's number in bit sub frame
/// @param[in] first_bit1 - first bit position in the word
/// @param[in] first_bit2 - first bit position in the word
/// @param[in] N_bits1    - number of bits in extracted value
/// @param[in] N_bits2    - number of bits in extracted value
/// @param[in] sign       - {1} - signed value, {0} - unsigned value
//============================================================================================
static int32 TwoSegInt(int32 *frame,
                       int32 word1,
                       int32 first_bit1,
                       int32 N_bits1,
                       int32 word2,
                       int32 first_bit2,
                       int32 N_bits2,
                       int32 sign)
{ 
  int32 bWord1 = frame[10 - word1], bWord2 = frame[10 - word2];
  
  // Check last bits in the previous words and words correcting
  if(frame[11 - word1] & 0x1)
  {
    bWord1 = ~bWord1;
  }
  if(frame[11 - word2] & 0x1)
  {
    bWord2 = ~bWord2;
  }
  
  // Value's MSB separation and sign correction
  // bits extraction from first word
  bWord1 <<= (first_bit1 + 1); 
  bWord1 >>= 32 - N_bits1; 
  
  // tacking into account of value sign
  bWord1 = sign ? bWord1 : bWord1 & (((int32)0x1 << N_bits1) - 1);  
  
  // stift MSB of the extracted value to the left
  bWord1 <<= N_bits2; 
  
  // Value's LSB separation and return
  bWord2 <<= (first_bit2 + 1);
  
  return ((bWord2 >> (32 - N_bits2)) & (((int32)0x1 << N_bits2) - 1)) | bWord1;
}

//============================================================================================
/// @brief     Fills up the sub frame by bits
/// @param[in] frame - pointer to bit sub frame
/// @param[in] bit   - input bit
//============================================================================================
static void AddBitToSubframe(int32* frame, int8 bit)
{ 
  // shifts all bits of each word to left by one and transfer
  // the MSB to higher word
  int32 i;

  for(i = 9; i >= 1; i--)
  {
    frame[i] = (frame[i] << 1);
    frame[i] |= (frame[i - 1] >> 29) & 0x1;
  }
  
  // writes the input bit into LSB of the first word
  frame[0] = (frame[0] << 1);
  frame[0]|= (bit & (int32)0x1);
  
  return;
}

//============================================================================================
/// @brief      Calculates the checksum for a words from sub frame
/// @param[in]  inWord - checked sub frame's word
/// @param[in]  D29pre - 29th bit of the previous word of sub frame
/// @param[in]  D30pre - 30th bit of the previous word of sub frame
/// @return     1 - checksum is OK, 0 - otherwise
//============================================================================================
int32 CheckParityGPS(int32 inWord, int32 D29pre, int32 D30pre)
{ 
  int32 Word = D30pre ? inWord ^ 0x3fffffc0 : inWord;
  int32 d[31], D25, D26, D27, D28, D29, D30;
  int32 i;

  // bits extracting from word
  for(i = 30; i > 0; i--)
  {
    d[i] = Word & 0x1;
    Word >>= 1;
  }
  
  D25 = (D29pre ^ d[1] + d[2] ^ d[3] + d[5] ^ d[6] + d[10] ^ d[11] + 
                         d[12] ^ d[13] + d[14] ^ d[17] + d[18] ^ d[20] + d[23]) & 0x1;
  
  D26 = (D30pre ^ d[2] + d[3] ^ d[4] + d[6] ^ d[7] + d[11] ^ d[12] + 
                         d[13] ^ d[14] + d[15] ^ d[18] + d[19] ^ d[21] + d[24]) & 0x1;
  
  D27 = (D29pre ^ d[1] + d[3] ^ d[4] + d[5] ^ d[7] + d[8] ^ d[12] + 
                         d[13] ^ d[14] + d[15] ^ d[16] + d[19] ^ d[20] + d[22]) & 0x1;
  
  D28 = (D30pre ^ d[2] + d[4] ^ d[5] + d[6] ^ d[8] + d[9] ^ d[13] + 
                         d[14] ^ d[15] + d[16] ^ d[17] + d[20] ^ d[21] + d[23]) & 0x1;
  
  D29 = (D30pre ^ d[1] + d[3] ^ d[5] + d[6] ^ d[7] + d[9] ^ d[10] + 
                         d[14] ^ d[15] + d[16] ^ d[17] + d[18] ^ d[21] + d[22] ^ d[24]) & 0x1;
  
  D30 = (D29pre ^ d[3] + d[5] ^ d[6] + d[8] ^ d[9] + d[10] ^ d[11] + 
                         d[13] ^ d[15] + d[19] ^ d[22] + d[23] ^ d[24]) & 0x1;

  if( d[25] == D25 && d[26] == D26 && d[27] == D27 &&
      d[28] == D28 && d[29] == D29 && d[30] == D30 ) 
  {
    return 1;
  }

  return 0;
}

//============================================================================================
/// @brief      Checks GPS subframe parity
/// @param[in]  frame  - pointer to GPS bit subframe
/// @return     check result
//============================================================================================
static int32 CheckFrameParityGPS(const int32 *frame)
{ 
  int32 result = 0x3;
  int32 Word;
  int32 i;

  for(i = 2; i < 10; i++)    // cycle along all words
  {                          // previous word in the frame
    Word = frame[10 - i];

    // check current word parity        |<- two last bit of previous word  ->|
    result |= CheckParityGPS(frame[ 9 - i],(Word >> 1) & 0x1,Word & 0x1) << i;
  }
  
  return result;
}

//============================================================================================
/// @brief      Detects the Preamble in GPS bit frame
/// @param[in]  frame - pointer to GPS bit sub frame
/// @param[in]  first  - first word number
/// @param[in]  second - second word number
/// @return     1 - preamble was detected and parity of first tow word is OK, 0 - otherwise
//============================================================================================
static int32 DetectPreamble(const int32 *frame, uint8 first, uint8 second)
{ 
  // For Preamble detection we test 8 MSB of 1th 30-bits word in bit frame, so
  // Preamble  = 10.0010.1100.0000.0000.0000.0000.0000 = 22C00000
  // inverse P = 01.1101.0000.0000.0000.0000.0000.0000 = 1D000000
  
  int32 Word = frame[first];
  int32 magic = Word & 0x3FC00000;
  int32 InvPreamble = (magic==0x1D000000) ? 0x1 : 0x0;
  int32 c1, c2;
  
  // Preamble or Inverted Preamble?
  if(magic == 0x22C00000 || InvPreamble)
  {
    // Two first word parity check
    c1 = CheckParityGPS(Word,InvPreamble,InvPreamble);
  
    //|<-- two last bit of previous word ->|
    c2 = CheckParityGPS(frame[second],(Word>>1)&0x1,Word&0x1);
    
    if(c1 && c2)  // Parity check is OK
      return 1;
  }
  return 0;
}

//============================================================================================
/// @brief     Decode raw GPS SV's almanac from frame
/// @param[in] Alm   - pointer to GPS SV's almanac structure
/// @param[in] frame - pointer to bit frame
//============================================================================================
static void DecodeAlmGPS(RAW_ALM_GPS* Alm, int32 *frame)
{ 
  Alm->e        = (uint16) OneSegInt(frame, 3,  9,  16, 0);
  Alm->toa      = (uint8)  OneSegInt(frame, 4,  1,  8,  0);
  Alm->i0       = (int16)  OneSegInt(frame, 4,  9,  16, 1);
  Alm->omegadot = (int16)  OneSegInt(frame, 5,  1,  16, 1);
  Alm->health   = (uint8)  OneSegInt(frame, 5,  17, 8,  0);
  Alm->roota    = (uint32) OneSegInt(frame, 6,  1,  24, 0);
  Alm->omega0   =          OneSegInt(frame, 7,  1,  24, 1);
  Alm->omega    =          OneSegInt(frame, 8,  1,  24, 1);
  Alm->m0       =          OneSegInt(frame, 9,  1,  24, 1);
  Alm->af0      = (int16)  TwoSegInt(frame, 10, 1,  8,  10, 20, 3, 1);
  Alm->af1      = (int16)  OneSegInt(frame, 10, 9,  11, 1);
  
  if(
     Alm->toa > 147        ||                      // {toa} exceeds 602112 sec
     Alm->e > 62915        ||                      // eccentricity exceeds 0.03
     Alm->roota < 10035200 ||                      // {roota} < 4900 m
     Alm->roota > 10854400                         // {roota} > 5300 m
    )
  {
    Alm->valid = 0; // bad raw almanac
  }
  else
  {
    Alm->valid = 1; // good raw almanac
  }

  return;
}

//============================================================================================
/// @brief      Converts truncated week number to untruncated one
/// @param[in]  wn  - current week number
/// @param[in]  wnt - truncated week number
/// @return     untruncated week number
//============================================================================================
int16 ConvertTruncatedWeekNum(int16 wn, uint8 wnt)
{ 
  int16 wnu;
  
  wnu = (int16)(wn & 0x300) | (int16)wnt;
  if(wnu - wn >  127) 
  {
    wnu = (int16)((wnu - 256) & 0x3ff);
  }
  if(wnu - wn < -127)
  {
    wnu = (int16)((wnu + 256) & 0x3ff);
  }
  
  return wnu;
}

//============================================================================================
/// @brief     Decode raw ION and UTC parameters from frame
/// @param[in] PtrIon - pointer to raw ION and UTC parameters structure
/// @param[in] frame  - pointer to bit frame
//============================================================================================
static void DecodeION(RAW_ION_UTC *PtrIonUTC, int32 *frame)
{ 
  // decode raw ION parameters
  PtrIonUTC->ION.alpha0 = (int8) OneSegInt(frame, 3, 9,  8, 1);
  PtrIonUTC->ION.alpha1 = (int8) OneSegInt(frame, 3, 17, 8, 1);
  PtrIonUTC->ION.alpha2 = (int8) OneSegInt(frame, 4, 1,  8, 1);
  PtrIonUTC->ION.alpha3 = (int8) OneSegInt(frame, 4, 9,  8, 1);
  PtrIonUTC->ION.beta0  = (int8) OneSegInt(frame, 4, 17, 8, 1);
  PtrIonUTC->ION.beta1  = (int8) OneSegInt(frame, 5, 1,  8, 1);
  PtrIonUTC->ION.beta2  = (int8) OneSegInt(frame, 5, 9,  8, 1);
  PtrIonUTC->ION.beta3  = (int8) OneSegInt(frame, 5, 17, 8, 1);
  
  // decode raw UTC parameters
  PtrIonUTC->UTC.A1     =         OneSegInt(frame, 6,  1,  24, 1);
  PtrIonUTC->UTC.A0     =         TwoSegInt(frame, 7,  1,  24, 8, 1, 8, 1);
  PtrIonUTC->UTC.tot    = (int16) OneSegInt(frame, 8,  9,  8,  0);
  PtrIonUTC->UTC.WNt    = (int16) OneSegInt(frame, 8,  17, 8,  0);
  PtrIonUTC->UTC.dtls   = (int16) OneSegInt(frame, 9,  1,  8,  1);
  PtrIonUTC->UTC.WNlsf  = (int16) OneSegInt(frame, 9,  9,  8,  0);
  PtrIonUTC->UTC.DN     = (int8)  OneSegInt(frame, 9,  17, 8,  0);
  PtrIonUTC->UTC.dtlsf  = (int8)  OneSegInt(frame, 10, 1,  8,  1);
   
  return;
}

//============================================================================================
/// @brief      Decodes GPS bit frame
/// @param[in]  sv_num  - ID satellite
/// @param[in]  common  - pointer to structure with temporary data
/// @param[in]  decoder - pointer to Decoder
//============================================================================================
static void  DecodeSubframeGPS(const int8      sv_num,
                               tDecoderCommon *common,
                               tDecoder       *decoder)
{
  int16       FrameID, SV_ID;
  int32       tow;
  int16       aode;
  uint16      wna;
  int32       i;
  uint8       toa;
  int32       dt;
  
  FrameID = (int16) OneSegInt(decoder->frame, 2, 20, 3, 0);  // sub frame ID number (1..5)

  tow = OneSegInt(decoder->frame, 2, 1, 17, 0) * 6;

  decoder->PreambleTime = tow;
  
  if(FrameID > 3 && FrameID == common->LastSubFrame)
    return;

  common->LastSubFrame = FrameID;
  
  switch(FrameID)
  {
  case 1:  // SUBFRAME # 1
    {
      decoder->RawEph.valid    = 0x1;
      decoder->RawEph.prn      = sv_num;
      decoder->RawEph.wn       = (uint16) OneSegInt(decoder->frame, 3, 1,  10, 0);
      decoder->RawEph.accuracy = (uint8)  OneSegInt(decoder->frame, 3, 13, 4,  0);
      decoder->RawEph.health   = (uint8)  OneSegInt(decoder->frame, 3, 17, 6,  0) ;
      decoder->RawEph.iodc     = (uint16) TwoSegInt(decoder->frame, 3, 23, 2,  8, 1, 8, 0);
      decoder->RawEph.tgd      = (int8)   OneSegInt(decoder->frame, 7, 17, 8,  1);
      decoder->RawEph.toc      = (uint16) OneSegInt(decoder->frame, 8, 9,  16, 0);
      decoder->RawEph.af2      = (int8)   OneSegInt(decoder->frame, 9, 1,  8,  1);
      decoder->RawEph.af1      = (int16)  OneSegInt(decoder->frame, 9, 9,  16, 1);
      decoder->RawEph.af0      =          OneSegInt(decoder->frame, 10,1,  22, 1);
      
      if(decoder->RawEph.toc   > MAX_TOC_TOE)
        decoder->RawEph.valid = 0;  // Ephemeris are bad
      decoder->wn           = decoder->RawEph.wn;
      decoder->tow          = tow * S_MS;
      decoder->CorrectTime  = fg_ON;
      decoder->FastPreamble = fg_OFF;
    } 
    break;
  case 2:  // SUBFRAME # 2
    { 
      if(decoder->RawEph.valid != 0x1) 
      {
        decoder->RawEph.valid = 0; 
        break;
      }  // bad reading of previous string
      
      // read reference time for orbit from frame
      decoder->RawEph.toe    = (uint16) OneSegInt(decoder->frame, 10, 1, 16, 0);
      decoder->RawEph.valid |= 0x2;
      decoder->RawEph.iode   = (uint8)  OneSegInt(decoder->frame, 3, 1,  8,  0);
      decoder->RawEph.crs    = (int16)  OneSegInt(decoder->frame, 3, 9,  16, 1);
      decoder->RawEph.deltan = (int16)  OneSegInt(decoder->frame, 4, 1,  16, 1);
      decoder->RawEph.m0     =          TwoSegInt(decoder->frame, 4, 17, 8,  5, 1, 24, 1);
      decoder->RawEph.cuc    = (int16)  OneSegInt(decoder->frame, 6, 1,  16, 1);
      decoder->RawEph.e      = (uint32) TwoSegInt(decoder->frame, 6, 17, 8,  7, 1, 24, 0);
      decoder->RawEph.cus    = (int16)  OneSegInt(decoder->frame, 8, 1,  16, 1);
      decoder->RawEph.roota  = (uint32) TwoSegInt(decoder->frame, 8, 17, 8,  9, 1, 24, 0);

      dt = tow - (decoder->RawEph.toe * 16);
      if (dt > HALF_WEEK)
      {
        decoder->RawEph.wn += 1;
        if (decoder->RawEph.wn >= WEEK)
          decoder->RawEph.wn -= WEEK;
      }
      else if (dt < -HALF_WEEK)
      {
        decoder->RawEph.wn -= 1;
        if (decoder->RawEph.wn < 0)
          decoder->RawEph.wn += WEEK;
      }
      
      if (sv_num < 33)
      {
      if( 
          decoder->RawEph.toe   > MAX_TOC_TOE      ||
          decoder->RawEph.e     > MAX_E            ||
          decoder->RawEph.roota < ONE_BOUND_ROOTA  ||
          decoder->RawEph.roota > TWO_BOUND_ROOTA
        )
      {
        decoder->RawEph.valid = 0;  // Ephemeris are bad
      }
      }
    }
    break;
  case 3:  // SUBFRAME # 3
    { 
      if(decoder->RawEph.valid != 0x3) 
      { 
        decoder->RawEph.valid = 0; 
        break;
      }  // bad reading of previous strings
      
      decoder->RawEph.valid   |= 0x4;
      decoder->RawEph.cic      = (int16) OneSegInt(decoder->frame, 3,  1,  16, 1);
      decoder->RawEph.omega0   =         TwoSegInt(decoder->frame, 3,  17, 8,  4, 1, 24, 1);
      decoder->RawEph.cis      = (int16) OneSegInt(decoder->frame, 5,  1,  16, 1);
      decoder->RawEph.i0       =         TwoSegInt(decoder->frame, 5,  17, 8,  6, 1, 24, 1);
      decoder->RawEph.crc      = (int16) OneSegInt(decoder->frame, 7,  1,  16, 1);
      decoder->RawEph.omega    =         TwoSegInt(decoder->frame, 7,  17, 8,  8, 1, 24, 1);
      decoder->RawEph.omegadot =         OneSegInt(decoder->frame, 9,  1,  24, 1);
      decoder->RawEph.idot     = (int16) OneSegInt(decoder->frame, 10, 9,  14, 1);
      aode                     = (int16) OneSegInt(decoder->frame, 10, 1,  8,  0);
      
      if(aode != (decoder->RawEph.iodc & 0xff) || aode != decoder->RawEph.iode)
          decoder->RawEph.valid = 0;  // bad ephemeris

      // Copying raw ephemeris
      if(decoder->RawEph.valid == 0x7 && common->TempEph[sv_num-1].flag_bl == 0)
      {
        common->TempEph[sv_num-1].flag_bl = 1;
        common->TempEph[sv_num-1].TEphGPS = decoder->RawEph;
        common->TempEph[sv_num-1].flag_bl = 0;
        // Ephemeris readiness flag setting
        common->TempEph[sv_num-1].flag = 1; 
      }
    } 
    break;
  case 4:  // SUBFRAME # 4
    { 
      SV_ID = (int16) OneSegInt(decoder->frame, 3, 3, 6, 0);

      if(SV_ID > 24 && SV_ID < 33)  // GPS almanac frame
      {
        DecodeAlmGPS(&common->RawAlmGPS[SV_ID-1], decoder->frame);
        common->RawAlmGPS[SV_ID-1].prn = SV_ID;
      }

      if(SV_ID == 56)  // ION and UTC frame
      {
        if (common->TempIonUTC.flag_bl == 0)
        {
          common->TempIonUTC.flag_bl = 1;
          DecodeION(&common->TempIonUTC.TIonUTC, decoder->frame);
          common->TempIonUTC.flag_bl = 0;

          // Parameters checking
          if (common->TempIonUTC.TIonUTC.UTC.tot <= MAX_TOT &&
              common->TempIonUTC.TIonUTC.UTC.DN <= MAX_DAY)
          {
            common->TempIonUTC.TIonUTC.UTC.WN = (int16)decoder->RawEph.wn;
            // ION and UTC readiness flag setting
            common->TempIonUTC.flag = 1;
          }
        }
      }
    }
    break;
  case 5:  // SUBFRAME # 4
    {
      SV_ID = (int16) OneSegInt(decoder->frame, 3, 3, 6, 0);

      if(SV_ID > 0 && SV_ID < 25)
      {
        DecodeAlmGPS(&common->RawAlmGPS[SV_ID-1], decoder->frame);
        common->RawAlmGPS[SV_ID-1].prn = SV_ID;
      }
      else if(SV_ID == 51)
      {
        // reading 8 LSB of reference almanac week from frame
        wna = (uint16) OneSegInt(decoder->frame, 3, 17, 8, 0);

        // reference almanac week determination
        if(wna == (uint16)(decoder->RawEph.wn & 0xff)) 
          wna = decoder->RawEph.wn;

        else if(wna == (uint16)((decoder->RawEph.wn + 1) & 0xff))
          wna = (uint16)((decoder->RawEph.wn + 1) & 0x3ff);

        else if(wna == (uint16)((decoder->RawEph.wn - 1) & 0xff))
          wna = (uint16)((decoder->RawEph.wn - 1) & 0x3ff);

        else 
          break; // bad almanac - break switch(FrameID)

        // reading reference almanac time from frame
        toa = (uint8) OneSegInt(decoder->frame, 3, 9, 8, 0);

        for(i = 0; i<32; i++)
        { 
          if(common->RawAlmGPS[i].valid != 0 && toa == common->RawAlmGPS[i].toa && common->TempAlmGPS[i].flag_bl == 0)
          {
            common->RawAlmGPS[i].wna = wna;
            common->TempAlmGPS[i].flag_bl = 1;
            common->TempAlmGPS[i].TAlmGPS = common->RawAlmGPS[i];
            common->TempAlmGPS[i].flag_bl = 0;

            // Almanac readiness flag setting
            common->TempAlmGPS[i].flag = 1;
          }
          common->RawAlmGPS[i].valid = 0;
        }
      }
    } 
    break;
  }
}

//============================================================================================
/// @brief      Decodes GPS sub frame
/// @param[in]  sv_num      - satellite number
/// @param[in]  bit         - bit value
/// @param[in]  common      - pointer to structure with temporary data
/// @param[in]  pDecoder    - pointer to Decoder
/// @param[in]  week        - week number
/// @param[in]  ms_count    - current time [ms]
/// @param[in]  WeekInit    - week is initialized
/// @param[in]  eph_set_flg - ephemeris availability flag
//============================================================================================
void SatGPS_DecoderSubframe(int8            sv_num,
                            eGPSChanBit     bit,
                            tDecoderCommon *common,
                            tDecoder       *pDecoder,
                            const int16     week,
                            const uint32    ms_count,
                            eFlag           WeekInit,
                            uint8           eph_set_flg)
{
  if (bit != egpschan_NOBIT)
  {
    pDecoder->Nframe++;
    AddBitToSubframe(pDecoder->frame, (int8)bit);

    if(pDecoder->Synchronized == fg_ON)
    {
      // we are synchronized !!!
      if(pDecoder->Nframe == BIT_COUNT)
      {
        int isPreamble = DetectPreamble(pDecoder->frame, 9, 8);
        int isCheckSum = ((CheckFrameParityGPS(pDecoder->frame) & 0x3fc) == 0x3fc);
        if (!isPreamble || !isCheckSum )
        {
          // synchronization is lost !
          pDecoder->Synchronized = fg_OFF;
        }
        else
        {  
          DecodeSubframeGPS(sv_num, common, pDecoder);
          common->preamble[sv_num-1].PreambleTime = pDecoder->PreambleTime;
          common->preamble[sv_num-1].time         = ms_count;
          common->preamble[sv_num-1].week         = week;
          pDecoder->Nframe                        = 0;
        }
      }
    }
    else // we are not in sync !!!
    {
      if (pDecoder->Nframe >= BIT_PREAMBLE)
      {
        int isPreamble = DetectPreamble(pDecoder->frame, 1, 0);
        if (isPreamble)
        {
          int32 tow;
          // synchronization is found !
          pDecoder->Synchronized = fg_ON;
          tow = OneSegInt(pDecoder->frame, 10, 1, 17, 0) * 6;
          
          pDecoder->PreambleTime                  = tow - FRAME_TIME_S;  /// it is old preamble time
          // preamble and detected time save
          common->preamble[sv_num-1].PreambleTime = pDecoder->PreambleTime;
          common->preamble[sv_num-1].time         = ms_count - (BIT_PREAMBLE*BIT_TIME_MS);  /// detected time equal current time - (60*20)ms
          common->preamble[sv_num-1].week         = week;
          pDecoder->Nframe                        = BIT_PREAMBLE;  /// if preamble detected then first 60 bit exist
          // if ephemeris for flash exist then week is know and it is possible time correct
          if ((WeekInit == fg_ON) && (eph_set_flg == 1))
          {
            /// correct time equal current time - as correct by old preamble time + first 60 bits
            pDecoder->tow          = tow*S_MS - (FRAME_TIME_S*S_MS) + (BIT_PREAMBLE*BIT_TIME_MS);
            pDecoder->wn           = week;
            pDecoder->FastPreamble = fg_ON;
          }
        }
      }
    }
  }
}

//============================================================================================
/// @brief      returns decoder status
/// @return     Synchronized - eFlag 
/// @param[in]  pDecoder     - pointer to tDecoder structure
//============================================================================================
eFlag SatGPS_GetDecoderStatus(tDecoder* pDecoder)
{
  return pDecoder->Synchronized;
}
