/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Hardware initialize and start
 *    @file                   Hardware.h
 *    @author                 B. Oblakov, V. Yeremeyev, M. Zhokhova
 *    @date                   22.09.2005
 *    @version                2.1
 */
/*****************************************************************************/

#ifndef _HARDWARE_H_INCLUDED_
#define _HARDWARE_H_INCLUDED_

#include "comtypes.h"

//=========================================================================
/// @brief     Delay in mcs
///
/// @param[in] time_mcs - number mcs
//=========================================================================
EXTERN_C void delay_mcs(int32 time_mcs);

//=========================================================================
/// @brief       Hardware initialize
//=========================================================================
EXTERN_C void HardwareInit();

//=========================================================================
/// @brief     Hardware start
//=========================================================================
EXTERN_C void HardwareStart(void);

#endif  // !defined(_HARDWARE_H_INCLUDED_)
