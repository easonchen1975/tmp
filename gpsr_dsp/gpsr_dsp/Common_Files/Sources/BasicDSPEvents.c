/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  BasicDSP events
 *   @file                   BasicDSPEvents.c
 *   @author                 M. Zhokhova
 *   @date                   05.09.2005
 *   @version                1.0
 */
/*****************************************************************************/

#include <string.h>
#include "Framework.h"

//=============================================================
/// Callback for post tracking manager
///
/// @param[in] pHost - pointer to instance
//=============================================================
void BasicDSP_FnPostTrackingManager(void *pHost)
{
  tFramework *pFramework = (tFramework*)pHost;

  ThreadManagerPost(&pFramework->BasicDSPThreadManager,
                    (tThreadID)et_THREAD_TRACKING);
}

//=============================================================
/// Callback for post navigation task
///
/// @param[in] pHost - pointer to instance
//=============================================================
void BasicDSP_FnPostNavTask (void* pHost)
{
  tFramework *pFramework = (tFramework*)pHost;

  ThreadManagerPost(&pFramework->BasicDSPThreadManager,
                    (tThreadID)et_THREAD_NAVTASK);
}




