**************************************************************
*                      asmWriteCSR                           *
*                      Hand assembly                         *
**************************************************************

        .global _asmWriteCSR

_asmWriteCSR:

         MVC CSR,B0                  ; get CSR
         OR 1,B0,B0                  ; get ready to set GIE
         MVC B0,CSR                  ; set GIE

         bnop b3, 5                  ;Rreturn to main program 

**************************************************************
*                      asmWriteIER                            *
*                      Hand assembly                         *
**************************************************************

        .global _asmWriteIER

_asmWriteIER:

         MVK 103h,B1                 ; set bit 8
         MVC IER,B0                  ; get IER
         OR B1,B0,B0                 ; get ready to set IE8
         MVC B0,IER                  ; set bit 8 in IER

         bnop b3, 5                  ;Return to main program 

**************************************************************
*                      asmWriteIERTestDrv                    *
*                      Hand assembly                         *
**************************************************************

        .global _asmWriteIERTestDrv

_asmWriteIERTestDrv:

         MVK 4003h,B1                ; set bit
         MVC IER,B0                  ; get IER
         OR B1,B0,B0                 ; get ready to set IE8
         MVC B0,IER                  ; set bit 8 in IER

         bnop b3, 5                  ;Return to main program 

**************************************************************
*                      asmWriteICR                           *
*                      Hand assembly                         *
**************************************************************

        .global _asmWriteICR

_asmWriteICR:

         MVK 100h,B1                 ; set bits 8
         MVC IFR,B0                  ; get IFR
         OR B1,B0,B0                 ; get ready to set ICR
         MVC B0,ICR                  ; set bits 4&5 in ICR

         bnop b3, 5                  ;Rreturn to main program 

**************************************************************
*                      asmIDLE                           *
*                      Hand assembly                         *
**************************************************************

        .global _asmIDLE

_asmIDLE:

         IDLE
         bnop b3, 5                  ;Rreturn to main program 

*=========================================================================== *
*             Copyright (c) 2003 Texas Instruments, Incorporated.           *
*                            All Rights Reserved.                           *
*=========================================================================== *

        .sect ".text:_fft16x16r"
        .global _DSP_fft16x16r
_DSP_fft16x16r:
*======================= SYMBOLIC REGISTER ASSIGNMENTS =====================*
        .asg    B15,        B_SP        ;stack pointer
        .asg    A7,         A_SP        ;copy of stack pointer
        .asg    B7,         B_csr       ;copy of CSR
        .asg    B9,         B_csr_no_gie;copy of CSR with interupts disabled
        .asg    B0,         B_wh        ;while predicate for outer loop
        .asg    B0,         B_early     ;while predicate for outer loop
        .asg            A4,         A_n
        .asg            B4,         B_ptr_x
        .asg            A6,         A_ptr_w
        .asg            B6,         B_brev                      ;NC
        .asg            A8,         A_ptr_y
        .asg            B8,         B_radix
        .asg            A10,        A_offset
        .asg            B10,        B_nmax
        .asg            B30,        B_stride
        .asg            A30,        A_tw_offset
        .asg            A16,        A_j
        .asg            B9,         B_j
        .asg            B1,         B_stride_1
        .asg            B17,        B_fft_jmp
        .asg            A18,        A_fft_jmp
        .asg            B13,        B_h2
        .asg            B0,         B_l1
        .asg            B16,        B_l2
        .asg            A20,        A_l1
        .asg            A19,        A_h2
        .asg            A21,        A_l2
        .asg            B7,         B_x
        .asg            A0,         A_w0
        .asg            B11,        B_w0
        .asg            A17,        A_w1
        .asg            B12,        B_w2
        .asg            B1,         B_fft_jmp_1
        .asg            A28,        A_i
        .asg            A5,         A_rnd
        .asg            B31,        B_rnd
        .asg            A0,         A_p0
* ===================== SYMBOLIC REGISTER ASSIGNMENTS ===================== *
        .asg            B11,        B_w0
        .asg            A17,        A_w1
        .asg            B12,        B_w2
        .asg            B7,         B_x
        .asg            B13,        B_h2
        .asg            B0,         B_l1
        .asg            B16,        B_l2
        .asg            A0,         A_p0
        .asg            A28,        A_i
        .asg            A16,        A_j
        .asg            B9,         B_j
        .asg            A18,        A_fft_jmp
        .asg            B17,        B_fft_jmp
        .asg            A19,        A_h2
        .asg            A20,        A_l1
        .asg            A21,        A_l2
        .asg            A5,         A_rnd
        .asg            B31,        B_rnd
        .asg            A13,        A_co11_si11
        .asg            A12,        A_co10_si10
        .asg            B27,        B_co21_si21
        .asg            B26,        B_co20_si20
        .asg            B29,        B_co31_si31
        .asg            B28,        B_co30_si30
        .asg            A25,        A_x_3_x_2
        .asg            A24,        A_x_1_x_0
        .asg            A27,        A_xh2_3_xh2_2
        .asg            A26,        A_xh2_1_xh2_0
        .asg            B19,        B_xl1_3_xl1_2
        .asg            B18,        B_xl1_1_xl1_0
        .asg            B21,        B_xl2_3_xl2_2
        .asg            B20,        B_xl2_1_xl2_0
        .asg            B18,        B_xh1_0_xh0_0
        .asg            B24,        B_xh1_1_xh0_1
        .asg            A7,         A_xl1_0_xl0_0
        .asg            A23,        A_xl1_1_xl0_1
        .asg            B19,        B_xh21_0_xh20_0
        .asg            B2,         B_xh21_1_xh20_1
        .asg            A7,         A_xl21_0_xl20_0
        .asg            A9,         A_xl21_1_xl20_1
        .asg            A3,         A_x_
        .asg            A11,        A_x__
        .asg            A1,         A_ifj
        .asg            B22,        B_x_1_x_0
        .asg            B23,        B_x_3_x_2
        .asg            B5,         B_yt0_0_xt0_0
        .asg            B22,        B_yt0_1_xt0_1
        .asg            A23,        A_xl20_0_xl21_0
        .asg            A9,         A_xl20_1_xl21_1
        .asg            A9,         A_yt2_0_xt1_0
        .asg            A23,        A_yt2_1_xt1_1
        .asg            A7,         A_yt1_0_xt2_0
        .asg            A24,        A_yt1_1_xt2_1
        .asg            A27,        A_xt1_0_yt1_0
        .asg            A2,         A_xt1_1_yt1_1
        .asg            A7,         A_xt2_0_yt2_0
        .asg            A9,         A_xt2_1_yt2_1
        .asg            B18,        B_xt0_0_yt0_0
        .asg            B22,        B_xt0_1_yt0_1
        .asg            A26,        A_yt1_0_xt1_0
        .asg            A9,         A_yt1_1_xt1_1
        .asg            A7,         A_yt2_0_xt2_0
        .asg            A29,        A_yt2_1_xt2_1
        .asg            A31,        A_x_h2_0
        .asg            A22,        A_x_h2_1
        .asg            A25,        A_x_h2_2
        .asg            A9,         A_x_h2_3
        .asg            B21,        B_x_l1_0
        .asg            B22,        B_x_l1_1
        .asg            B1,         B_x_l1_2
        .asg            B27,        B_x_l1_3
        .asg            B6,         B_x_l2_0
        .asg            B1,         B_x_l2_1
        .asg            B5,         B_x_l2_2
        .asg            B20,        B_x_l2_3
        .asg            A22,        A_xh2_1_0
        .asg            A23,        A_xh2_3_2
        .asg            B24,        B_xl1_1_0
        .asg            B25,        B_xl1_3_2
        .asg            B24,        B_xl2_1_0
        .asg            B25,        B_xl2_3_2
* =========================== PIPE LOOP PROLOG ============================ *

        STW    .D2T2    B13,           *--B_SP[8]                     ; [9,0]
||      MVC    .S2      CSR,           B_csr                          ; GIE
||      B      .S1      LOOP_WHILE                                    ;

        STW    .D2T1    A13,           *+B_SP[1]                      ; [7,0]
||      AND    .L2      B_csr,         -2,              B_csr_no_gie  ; NGIE

        STW    .D2T2    B12,           *+B_SP[2]                      ; [6,0]
||      MV     .L1X     B_SP,          A_SP                           ;
||      MVC    .S2      B_csr_no_gie,  CSR                            ; Dis.int

        STW    .D2T2    B11,           *+B_SP[3]                      ; [7,0]
||      STW    .D1T1    A11,           *+A_SP[4]                      ; [5,0]

        STW    .D2T2    B_csr,         *+B_SP[5]                      ; CSR
||      STW    .D1T1    A12,           *+A_SP[6]                      ; [6,0]
||      ZERO   .L1      A_tw_offset                                   ; [3,0]
||      MV     .L2X     A_n,           B_stride                       ; [3,0]

        SHRU    .S2     B_stride,      3,               B_h2            ;[5,0]
||      MVKL    .S1     08000h,        A_rnd                            ;[7,0]

; ----- Branch to LOOP_WHILE occurs


* =========================== PIPE LOOP KERNEL ============================ *
LOOP_Y:
        PACKH2 .S2     B_x_l1_1,        B_x_l1_0,        B_xl1_1_0      ;[24,1]
||      ADD    .L2     B_rnd,           B_x_l2_3,        B_x_l2_3       ;[24,1]
||      ADD    .L1     A_rnd,           A_x_h2_3,        A_x_h2_3       ;[24,1]
||      DOTP2  .M2X    B_co30_si30,     A_xt2_0_yt2_0,   B_x_l2_0       ;[14,2]
||      DOTP2  .M1     A_xt1_0_yt1_0,   A_co10_si10,     A_x_h2_0       ;[14,2]
||      PACKLH2.S1     A_yt2_1_xt1_1,   A_yt1_1_xt2_1,   A_xt1_1_yt1_1  ;[14,2]
||      SUB    .D1     A_fft_jmp,       A_j,             A_ifj          ;[ 4,3]
||      LDDW   .D2T2   *B_x[B_l1],      B_xl1_3_xl1_2:B_xl1_1_xl1_0     ;[ 4,3]

        PACKH2 .L2     B_x_l2_3,        B_x_l2_2,        B_xl2_3_2      ;[25,1]
||      PACKH2 .S2     B_x_l2_1,        B_x_l2_0,        B_xl2_1_0      ;[25,1]
||      PACKH2 .S1     A_x_h2_3,        A_x_h2_2,        A_xh2_3_2      ;[25,1]
||      DOTPN2 .M1     A_yt1_0_xt1_0,   A_co10_si10,     A_x_h2_1       ;[15,2]
||      PACKLH2.L1     A_yt1_1_xt2_1,   A_yt2_1_xt1_1,   A_xt2_1_yt2_1  ;[15,2]
||      SUB2   .D2     B_xh1_0_xh0_0,   B_xh21_0_xh20_0, B_yt0_0_xt0_0  ;[15,2]
||      AVG2   .M2     B_xh21_0_xh20_0, B_xh1_0_xh0_0,   B_x_1_x_0      ;[15,2]
||[!A_p0]STDW  .D1T2   B_xl1_3_2:B_xl1_1_0,              *A_x__[A_h2]   ;[25,1]

        ADD    .S1     A_rnd,           A_x_h2_1,        A_x_h2_1       ;[26,1]
||      ADD    .L2     B_rnd,           B_x_l1_2,        B_x_l1_2       ;[16,2]
||      DOTPN2 .M2X    B_co30_si30,     A_yt2_0_xt2_0,   B_x_l2_1       ;[16,2]
||      ROTL   .M1     A_xt1_1_yt1_1,   16,              A_yt1_1_xt1_1  ;[16,2]
||      PACKLH2.S2     B_yt0_0_xt0_0,   B_yt0_0_xt0_0,   B_xt0_0_yt0_0  ;[16,2]
||      ADD    .L1     A_j,             3,               A_j            ;[ 6,3]
||      LDDW   .D1T2   *A_w1[A_j],      B_co21_si21:B_co20_si20         ;[ 6,3]
||      LDDW   .D2T1   *B_w0[B_j],      A_co11_si11:A_co10_si10         ;[ 6,3]

        PACKH2 .L1     A_x_h2_1,        A_x_h2_0,        A_xh2_1_0      ;[27,1]
||      ADD    .L2     B_rnd,           B_x_l1_3,        B_x_l1_3       ;[17,2]
||      DOTP2  .M2X    B_co31_si31,     A_xt2_1_yt2_1,   B_x_l2_2       ;[17,2]
||      ROTL   .M1     A_xt2_1_yt2_1,   16,              A_yt2_1_xt2_1  ;[17,2]
||[!A_ifj]ZERO .S1     A_j                                              ;[ 7,3]
||      ADD    .S2     B_j,             3,               B_j            ;[ 7,3]
||      SUB2   .D1X    A_xh2_1_xh2_0,   B_xl2_1_xl2_0,   A_xl21_0_xl20_0;[ 7,3]
||      LDDW   .D2T2   *B_w2[B_j],      B_co31_si31:B_co30_si30         ;[ 7,3]

        BDEC   .S1     LOOP_Y,          A_i                             ;[28,1]
||      ADD    .L1     A_rnd,           A_x_h2_0,        A_x_h2_0       ;[18,2]
||      DOTP2  .M2     B_xt0_0_yt0_0,   B_co20_si20,     B_x_l1_0       ;[18,2]
||[!A_ifj]ZERO .L2     B_j                                              ;[ 8,3]
||[!A_ifj]ADD  .S2     B_x,             B_fft_jmp,       B_x            ;[ 8,3]
||      MVD    .M1X    B_x,             A_x_                            ;[ 8,3]
||      ADD2   .D2X    B_xl2_3_xl2_2,   A_xh2_3_xh2_2,   B_xh21_1_xh20_1;[ 8,3]
||[!A_p0]STDW  .D1T1   A_xh2_3_2:A_xh2_1_0,              *A_x__[A_l1]   ;[28,1]

LOOP_Y5:
  [!A_p0]STDW   .D1T2   B_xl2_3_2:B_xl2_1_0,             *A_x__[A_l2]   ;[29,1]
||      PACKH2  .L2     B_x_l1_3,       B_x_l1_2,        B_xl1_3_2      ;[19,2]
||      ADD     .S2     B_rnd,          B_x_l2_0,        B_x_l2_0       ;[19,2]
||      DOTPN2  .M2     B_yt0_0_xt0_0,  B_co20_si20,     B_x_l1_1       ;[19,2]
||      DOTP2   .M1     A_xt1_1_yt1_1,  A_co11_si11,     A_x_h2_2       ;[19,2]
||      PACKLH2 .S1     A_xl21_0_xl20_0,A_xl21_0_xl20_0, A_xl20_0_xl21_0;[ 9,3]
||      SUB2    .L1X    A_x_1_x_0,      B_xl1_1_xl1_0,   A_xl1_0_xl0_0  ;[ 9,3]
||      ADD2    .D2X    B_xl1_3_xl1_2,  A_x_3_x_2,       B_xh1_1_xh0_1  ;[ 9,3]

LOOP_Y6:
        ADD     .D2     B_rnd,          B_x_l2_1,        B_x_l2_1       ;[20,2]
||      DOTPN2  .M2X    B_co31_si31,    A_yt2_1_xt2_1,   B_x_l2_3       ;[20,2]
||      DOTPN2  .M1     A_yt1_1_xt1_1,  A_co11_si11,     A_x_h2_3       ;[20,2]
||      STDW    .D1T2   B_x_3_x_2:B_x_1_x_0,             *A_x_[0]       ;[20,2]
||      SUB2    .S1     A_xl1_0_xl0_0,  A_xl20_0_xl21_0, A_yt1_0_xt2_0  ;[10,3]
||      ADD2    .L1     A_xl1_0_xl0_0,  A_xl20_0_xl21_0, A_yt2_0_xt1_0  ;[10,3]
||      SUB2    .S2     B_xh1_1_xh0_1,  B_xh21_1_xh20_1, B_yt0_1_xt0_1  ;[10,3]
||      ADD     .L2     B_x,            8,               B_x            ;[10,3]

;LOOP_Y7:
        ADD     .L2     B_rnd,          B_x_l2_2,        B_x_l2_2       ;[21,2]
||      MVD     .M1     A_x_,           A_x__                           ;[21,2]
||      DOTPN2  .M2     B_yt0_1_xt0_1,  B_co21_si21,     B_x_l1_3       ;[11,3]
||      PACKLH2 .S2     B_yt0_1_xt0_1,  B_yt0_1_xt0_1,   B_xt0_1_yt0_1  ;[11,3]
||      PACKLH2 .L1     A_yt1_0_xt2_0,  A_yt2_0_xt1_0,   A_xt2_0_yt2_0  ;[11,3]
||      PACKLH2 .S1     A_yt2_0_xt1_0,  A_yt1_0_xt2_0,   A_xt1_0_yt1_0  ;[11,3]
||      SUB2    .D1X    A_xh2_3_xh2_2,  B_xl2_3_xl2_2,   A_xl21_1_xl20_1;[11,3]
||      LDDW    .D2T1   *B_x[B_h2],     A_xh2_3_xh2_2:A_xh2_1_xh2_0     ;[ 1,4]

;LOOP_Y8:
  [A_p0]SUB     .L1     A_p0,           1,               A_p0           ;
||      ADD     .L2     B_rnd,          B_x_l1_0,        B_x_l1_0       ;[22,2]
||      DOTP2   .M2     B_xt0_1_yt0_1,  B_co21_si21,     B_x_l1_2       ;[12,3]
||      ROTL    .M1     A_xt1_0_yt1_0,  16,              A_yt1_0_xt1_0  ;[12,3]
||      PACKLH2 .S1     A_xl21_1_xl20_1,A_xl21_1_xl20_1, A_xl20_1_xl21_1;[12,3]
||      SUB2    .D1X    A_x_3_x_2,      B_xl1_3_xl1_2,   A_xl1_1_xl0_1  ;[12,3]
||      ADD2    .S2X    B_xl1_1_xl1_0,  A_x_1_x_0,       B_xh1_0_xh0_0  ;[12,3]
||      LDDW    .D2T2   *B_x[B_l2],     B_xl2_3_xl2_2:B_xl2_1_xl2_0     ;[ 2,4]

;LOOP_Y9:
        ADD     .S2     B_rnd,          B_x_l1_1,        B_x_l1_1       ;[23,2]
||      ADD     .S1     A_rnd,          A_x_h2_2,        A_x_h2_2       ;[23,2]
||      ROTL    .M1     A_xt2_0_yt2_0,  16,              A_yt2_0_xt2_0  ;[13,3]
||      SUB2    .L1     A_xl1_1_xl0_1,  A_xl20_1_xl21_1, A_yt1_1_xt2_1  ;[13,3]
||      ADD2    .D1     A_xl1_1_xl0_1,  A_xl20_1_xl21_1, A_yt2_1_xt1_1  ;[13,3]
||      AVG2    .M2     B_xh21_1_xh20_1,B_xh1_1_xh0_1,   B_x_3_x_2      ;[13,3]
||      ADD2    .L2X    B_xl2_1_xl2_0,  A_xh2_1_xh2_0,   B_xh21_0_xh20_0;[13,3]
||      LDDW    .D2T1   *B_x[0],        A_x_3_x_2:A_x_1_x_0             ;[ 3,4]

* =========================== PIPE LOOP EPILOG ============================ *

        PACKH2  .S2     B_x_l1_1,       B_x_l1_0,        B_xl1_1_0      ;[24,4]
||      ADD     .D2     B_rnd,          B_x_l2_3,        B_x_l2_3       ;[24,4]
||      ADD     .L1     A_rnd,          A_x_h2_3,        A_x_h2_3       ;[24,4]
||      CMPGTU  .L2     B_stride,       B_radix,         B_wh           ;

        PACKH2  .L2     B_x_l2_3,       B_x_l2_2,        B_xl2_3_2      ;[25,4]
||      PACKH2  .S2     B_x_l2_1,       B_x_l2_0,        B_xl2_1_0      ;[25,4]
||[!A_p0]STDW   .D1T2   B_xl1_3_2:B_xl1_1_0,             *A_x__[A_h2]   ;[25,4]
||      PACKH2  .L1     A_x_h2_3,       A_x_h2_2,        A_xh2_3_2      ;[25,4]
||[!B_wh]B      .S1     DONE_WHILE                                      ;

        ADD     .S1     A_rnd,          A_x_h2_1,        A_x_h2_1       ;[26,4]
||[B_wh]SHRU    .S2     B_stride,       3,               B_h2           ;[5,0]

        PACKH2  .L1     A_x_h2_1,       A_x_h2_0,        A_xh2_1_0      ;[27,4]

  [!A_p0]STDW   .D1T1   A_xh2_3_2:A_xh2_1_0,             *A_x__[A_l1]   ;[28,4]

  [!A_p0]STDW   .D1T2   B_xl2_3_2:B_xl2_1_0,             *A_x__[A_l2]   ;[29,4]

        CMPGTU  .L2     B_radix,        4,               B_early        ;
        ;BRANCH OCCURS
LOOP_WHILE:
        SHRU    .S2     B_stride,      2,               B_l1            ;[6,0]

        MVKLH   .S1     00h,           A_rnd                            ;[8,0]
||      ADDAH   .D1     A_ptr_w,       A_tw_offset,     A_w0            ;[8,0]
||      ADD     .L2     B_l1,          B_h2,            B_l2            ;[7,0]

        ADD     .D1     A_w0,          8,               A_w1            ;[9,0]
||      MV      .L1X    B_l2,          A_l2                             ;[9,0]
||      SHL     .S2     B_stride,      1,               B_stride_1      ;[9,0]

        MV      .L2X    A_rnd,         B_rnd                            ;[10,0]
||      MV      .S1X    B_h2,          A_h2                             ;[10,0]
||      ADD     .D2     B_stride,      B_stride_1,      B_fft_jmp       ;[10,0]
||      MV      .S2     B_ptr_x,       B_x                              ;[13,0]

        SHRU    .S1     A_n,           3,               A_i             ;[11,0]
||      ADD     .L2X    A_w1,          8,               B_w2            ;[11,0]
||      MV      .D1X    B_l1,          A_l1                             ;[11,0]
||      SHRU    .S2     B_fft_jmp,     1,               B_fft_jmp_1     ;[11,0]
||      LDDW    .D2T1   *B_x[B_h2],    A_xh2_3_xh2_2:A_xh2_1_xh2_0      ;[ 1,1]

        MVK     .L1     2,             A_p0                             ;[12,0]
||      SUB     .D1     A_i,           2,               A_i             ;[12,0]
||      MV      .L2X    A_w0,          B_w0                             ;[12,0]
||      SHRU    .S1X    B_fft_jmp,     3,               A_fft_jmp       ;[12,0]
||      MPY     .M2     B_j,           0,               B_j             ;
||      LDDW    .D2T2   *B_x[B_l2],    B_xl2_3_xl2_2:B_xl2_1_xl2_0      ;[ 2,1]

        SUB     .L1     A_fft_jmp,     3,               A_fft_jmp       ;[13,0]
||      MPY     .M1     A_j,           0,               A_j             ;[13,0]
||      ADD     .D1X    A_tw_offset,   B_fft_jmp_1,     A_tw_offset     ;[13,0]
||      SHRU    .S2     B_stride,      2,               B_stride        ;[13,0]
||      LDDW    .D2T1   *B_x[0],       A_x_3_x_2:A_x_1_x_0              ;[ 3,1]
||      B       .S1     LOOP_Y5 + 20                                    ;

        LDDW    .D2T2   *B_x[B_l1],    B_xl1_3_xl1_2:B_xl1_1_xl1_0      ;[ 4,1]
||      B       .S1     LOOP_Y6 + 16                                    ;

        SUB     .D1     A_fft_jmp,     A_j,             A_ifj           ;[ 4,1]

        ADD     .L1     A_j,           3,               A_j             ;[ 6,1]
||      LDDW    .D1T2   *A_w1[A_j],    B_co21_si21:B_co20_si20          ;[ 6,1]
||      LDDW    .D2T1   *B_w0[B_j],    A_co11_si11:A_co10_si10          ;[6,1]

 [!A_ifj]ZERO  .L1      A_j                                             ;[ 7,1]
||      ADD     .S2     B_j,           3,               B_j             ;[ 7,1]
||      SUB2    .D1X    A_xh2_1_xh2_0, B_xl2_1_xl2_0,   A_xl21_0_xl20_0 ;[ 7,1]
||      LDDW    .D2T2   *B_w2[B_j],    B_co31_si31:B_co30_si30          ;[ 7,1]

  [!A_ifj]ZERO  .L2     B_j                                             ;[ 8,1]
||[!A_ifj]ADD   .S2     B_x,            B_fft_jmp,       B_x            ;[ 8,1]
||      MVD     .M1X    B_x,            A_x_                            ;[ 8,1]
||      ADD2    .D2X    B_xl2_3_xl2_2,  A_xh2_3_xh2_2,   B_xh21_1_xh20_1;[ 8,1]
||      B       .S1     LOOP_Y                                          ;

; ----- Branch to LOOP_Y5 occurs


* ===================== SYMBOLIC REGISTER ASSIGNMENTS ===================== *
        .asg            A4,         A_n
        .asg            B8,         B_radix
        .asg            B4,         B_ptr_x
        .asg            B10,        B_nmax
        .asg            A8,         A_ptr_y
        .asg            A10,        A_offset
        .asg            A0,         A_r2
        .asg            A3,         A_zero
        .asg            B18,        B_zero
        .asg            A9,         A_x0
        .asg            B20,        B_x0
        .asg            B19,        B_l1
        .asg            A31,        A_i
        .asg            B21,        B_j
        .asg            B0,         B_p0
        .asg            B17,        B_y0
        .asg            B16,        B_y1
        .asg            B9,         B_y2
        .asg            B5,         B_y3
* ========================================================================= *
* ===================== SYMBOLIC REGISTER ASSIGNMENTS ===================== *
        .asg            B21,        B_j
        .asg            B19,        B_l1
        .asg            A9,         A_x0
        .asg            B20,        B_x0
        .asg            A0,         A_r2
        .asg            A3,         A_zero
        .asg            B18,        B_zero
        .asg            A31,        A_i
        .asg            B17,        B_y0
        .asg            B16,        B_y1
        .asg            B9,         B_y2
        .asg            B5,         B_y3
        .asg            B0,         B_p0
        .asg            B23,        B_h3
        .asg            B25,        B_h4
        .asg            A5,         A_x3x2
        .asg            A4,         A_x1x0
        .asg            B7,         B_x7x6
        .asg            B6,         B_x5x4
        .asg            A16,        A_x7x6
        .asg            A6,         A_xl1_0_xl0_0
        .asg            A18,        A_xl1_1_xl0_1
        .asg            A17,        A_xl0_1_xl1_1
        .asg            A18,        A_mx7mx6
        .asg            A18,        A_yt7_yt2
        .asg            A17,        A_yt3_yt6
        .asg            A4,         A_y3y2
        .asg            A17,        A_y7y6
        .asg            B6,         B_xh1_0_xh0_0
        .asg            B7,         B_xh1_1_xh0_1
        .asg            B24,        B_y1y0
        .asg            B22,        B_y5y4
* =========================== PIPE LOOP PROLOG ============================ *
DONE_WHILE:
 [B_early]B     .S1     EARLY_EXIT                                      ;
||      MV     .L1X     B_SP,           A_SP                            ;

        MV      .D2X    A_ptr_y,        B_y0                            ;[ 3,0]

        ADD     .S2     B_y0,           B_nmax,         B_y1            ;[ 4,0]
||      CMPEQ   .L2     B_p0,           B_p0,           B_p0            ;[ 4,0]

        ADD     .D2     B_y1,           B_nmax,         B_y2            ;[ 5,0]

        ADD     .D2     B_y2,           B_nmax,         B_y3            ;[ 6,0]
||      SHRU    .S2X    A_offset,       2,              B_j             ;[ 6,0]
||      SHRU    .S1     A_n,            2,              A_i             ;[ 6,0]
||      MV      .D1X    B_ptr_x,        A_x0                            ;[ 6,0]
||      NORM    .L2     B_nmax,         B_l1                            ;[ 6,0]

        SUB     .D1     A_i,            1,              A_i             ;[ 7,0]
||      ADD     .D2     B_l1,           4,              B_l1            ;[ 7,0]
||      ADD     .S2     B_ptr_x,        8,              B_x0            ;[ 7,0]
||      ZERO    .L2     B_zero                                          ;[ 7,0]
||      ZERO    .L1     A_zero                                          ;[ 7,0]

        LDDW    .D1T1   *A_x0++[2],     A_x3x2:A_x1x0                   ;[ 1,1]
||      LDDW    .D2T2   *B_x0++[2],     B_x7x6:B_x5x4                   ;[ 1,1]

        SUB             B_j,            1,              B_j

        SUB     .S1X    B_radix,        2,              A_r2            ;[ 7,0]

  [!A_r2]MPY2   .M2     B_zero,         B_zero,         B_x7x6:B_x5x4   ;[ 4,1]

        BDEC    .S1     LOOP_Z,         A_i                             ;[10,1]

* =========================== PIPE LOOP KERNEL ============================ *
LOOP_Z:
        ADD     .L2     B_j,            1,              B_j             ;[11,1]
||      SHRU    .S2     B_h3,           B_l1,           B_h4            ;[11,1]
||[!A_r2]PACKHL2.L1     A_mx7mx6,       A_x7x6,         A_xl0_1_xl1_1   ;[11,1]
||      ROTL    .M1X    B_x7x6,         0,              A_x7x6          ;[ 6,2]
||      LDDW    .D1T1   *A_x0++[2],     A_x3x2:A_x1x0                   ;[ 1,3]
||      LDDW    .D2T2   *B_x0++[2],     B_x7x6:B_x5x4                   ;[ 1,3]

  [!B_p0]STW    .D2T2   B_y5y4,         *B_y2[B_h4]                     ;[12,1]
||      ADD2    .L1     A_xl1_0_xl0_0,  A_xl0_1_xl1_1,  A_yt7_yt2       ;[12,1]
||      SUB2    .D1     A_xl1_0_xl0_0,  A_xl0_1_xl1_1,  A_yt3_yt6       ;[12,1]
||      BITR    .M2     B_j,            B_h3                            ;[ 7,2]
||      SUB2    .S1X    A_x1x0,         B_x5x4,         A_xl1_0_xl0_0   ;[ 7,2]
||[!A_r2]ROTL   .M1X    B_x5x4,         0,              A_xl1_0_xl0_0   ;[ 7,2]

  [!B_p0]STW    .D2T2   B_y1y0,         *B_y0[B_h4]                     ;[13,1]
||      PACKHL2 .L1     A_yt3_yt6,      A_yt7_yt2,      A_y3y2          ;[13,1]
||      PACKHL2 .S1     A_yt7_yt2,      A_yt3_yt6,      A_y7y6          ;[13,1]
||      ADD2    .S2X    B_x5x4,         A_x1x0,         B_xh1_0_xh0_0   ;[ 8,2]
||      SUB2    .D1     A_x3x2,         A_x7x6,         A_xl1_1_xl0_1   ;[ 8,2]

  [!B_p0]STW    .D2T1   A_y7y6,         *B_y3[B_h4]                     ;[14,1]
||      ADD2    .L2X    B_x7x6,         A_x3x2,         B_xh1_1_xh0_1   ;[ 9,2]
||      ROTL    .M1     A_xl1_1_xl0_1,  16,             A_xl0_1_xl1_1   ;[ 9,2]
||[!A_r2]MPY2   .M2     B_zero,         B_zero,         B_x7x6:B_x5x4   ;[ 4,3]

        MPYSU   .M2     0,              B_p0,           B_p0            ;[15,1]
||[!B_p0]STW    .D2T1   A_y3y2,         *B_y1[B_h4]                     ;[15,1]
||      SUB2    .D1     A_zero,         A_x7x6,         A_mx7mx6        ;[10,2]
||      ADD2    .L2     B_xh1_0_xh0_0,  B_xh1_1_xh0_1,  B_y1y0          ;[10,2]
||      SUB2    .S2     B_xh1_0_xh0_0,  B_xh1_1_xh0_1,  B_y5y4          ;[10,2]
||      BDEC    .S1     LOOP_Z,         A_i                             ;[10,2]

* =========================== PIPE LOOP EPILOG ============================ *

EARLY_EXIT:

        LDW    .D2T2    *+B_SP[2],      B12                             ;[ 6,0]

        LDW    .D2T2    *+B_SP[0],      B13                             ;[ 9,0]
||      LDW    .D1T1    *+A_SP[1],      A13                             ;[ 7,0]

        LDW    .D2T2    *+B_SP[5],      B_csr                           ; CSR
||      LDW    .D1T1    *+A_SP[6],      A12                             ;[ 6,0]
||      RET    .S2      B3                                              ;

        LDW    .D2T2    *+B_SP[3],      B11                             ;[ 7,0]
||      LDW    .D1T1    *+A_SP[4],      A11                             ;[ 5,0]

        NOP             2                                               ;
        ADDAW  .D2      B_SP,           8,          B_SP                ;

        MVC    .S2      B_csr,          CSR                             ;GIE

        ;BRANCH OCCURS HERE



*===========================================================================*
* End of DSP_fft16x16r assembly code                                        *
*===========================================================================*
