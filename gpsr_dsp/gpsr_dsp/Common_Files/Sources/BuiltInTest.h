/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Built-in test
 *   @file                   BuiltInTest.h
 *   @author                 M. Zhokhova, B. Oblakov
 *   @date                   25.08.2006
 *   @version                1.1
 */
/*****************************************************************************/
#ifndef _BUILD_IN_TEST_H__
#define _BUILD_IN_TEST_H__

#include "SampleDriver.h"

#define LAST_WORD   (0xFFFFC)     // address for check sum system file saved: 1MB - 4 bytes

// latchs address
//#define LATCH_1MS   (*(volatile int16*)0x6800010a)  // latch of counter to 1 ms
#define LATCH_1MS   (*(volatile int16*)0x68000016)  //  v.703 - latch of counter to 1 ms
//#define LATCH_1S    (*(volatile int16*)0x68000110)  // latch of ms counter
#define LATCH_1S    (*(volatile int16*)0x68000018)  // v.703 - latch of ms counter
// shifts address
//#define SHIFT_1MS   (*(volatile int16*)0x68000102)  // shift for counter to 1 ms
#define SHIFT_1MS   (*(volatile int16*)0x68000012)  // v.703 - shift for counter to 1 ms
//#define SHIFT_1S    (*(volatile int16*)0x68000108)  // shift for counter ms
#define SHIFT_1S    (*(volatile int16*)0x68000014)  // v.703 - shift for counter ms

//==================================================================
/// Built-in test: short/long
///
/// @param[in]  pSmpMgr     - pointer to Sample Manager object
/// @param[in]  pFramework  - pointer to Framework object
/// @param[in]  resetBIT    - BIT after software reset flag
/// @param[in]  oldBIT      - data old BIT
/// @param[in]  Mode485     - RS485 communication mode
//==================================================================
EXTERN_C void FrameWork_BuiltInTest(tSampleManager *pSmpMgr,
                                    tFramework     *pFramework,
                                    uint8           resetBIT,
                                    tBuiltInTest    oldBIT);

#endif // _BUILD_IN_TEST_H__


