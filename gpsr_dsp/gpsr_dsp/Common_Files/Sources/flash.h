/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        Write data in flash
 *   @file         Flash.h
 *   @author       M. Zhokhova
 *   @date         09.03.2006
 *   @version      2.0
 */
/*****************************************************************************/

#ifndef _FLASH_H_
#define _FLASH_H_

#include "comtypes.h"
#include "DataForm.h"

#define FLASH1_ADDRESS  (0x64000000)  ///< start first flash address
#define FLASH2_ADDRESS  (0x64020000)  ///< start second flash address
#define FL_CNT_TEST     (128)         ///< bytes number for testing FLASH
#define PROCESS         (0)           ///< write process
#define FINISH          (1)           ///< data writing
#define ERROR_FLASH     (2)           ///< error data writing
#define TIME_ERROR      (20)          ///< 20 ms - time error for writing to FLASH
#define FLASH_CNT       (6)           ///< flash object counter
#define FL_SYSTEM       (0)           ///< system file address in FLASH
#define FL_EPHEMERIS    (1)           ///< ephemeris address in FLASH
#define FL_ALMANAC      (2)           ///< almanac address in FLASH
#define FL_CONFIG       (3)           ///< configuration setup address in FLASH
#define FL_WN           (4)           ///< week data address in FLASH
#define FL_TEST         (5)           ///< test address in FLASH

#define DATA_NULLING    (1)           ///< data nulling in FLASH
#define DATA_RECORD     (2)           ///< data record in FLASH

/// Flash Driver structure
typedef struct tagFlashDrv
{
  eFlag       FlagProgramming;     ///< data programming process flag
  uint8       cnt_programming;     ///< counter for programming FLASH
  eFlag       StepProgramming;     ///< two steps method programming (when data traverse FLASH page boundary)
  uint8       len_programming;     ///< loading data length 
  uint32      address[FLASH_CNT];  ///< start address: 0 - system, 1 - ephemeris, 2 - almanac, 
                                   ///< 3 - configuration setup, 4 - week data, 5 - test
  tFlashData  flashData;           ///< structures for work with Flash
  tFlashState state;
}
tFlashDrv;

//=============================================================================
/// Flash driver initialize
/// 
/// @param[in]  pThis - pointer to FLASH object
//=============================================================================
EXTERN_C void FrameWork_FlashInit(tFlashDrv *pThis);

//=============================================================================
/// Data write-in flash
/// 
/// @param[in]  pThis  - pointer to Flash driver
/// @param[in]  addr   - start address
/// @param[in]  length - data length
/// @param[in]  data   - pointer to data
//=============================================================================
EXTERN_C void FrameWork_FlashProgramming(tFlashDrv *pThis, 
                                         uint32     addr, 
                                         uint8      length, 
                                         uint8     *data);

//===================================================================================
/// Data write-in flash test
/// 
/// @param[in]  addr - end address
/// @param[in]  data - end data
/// @return     0 if data not write, 1 - data write
//====================================================================================
EXTERN_C uint8 FrameWork_ControlFlashProgramming(uint32 addr, uint8 data);

//=============================================================================
/// Data write and control to flash
/// 
/// @param[in]  pThis  - pointer to FLASH object
/// @param[in]  addr   - start address
/// @param[in]  length - data length
/// @param[in]  data   - pointer to data
/// @return     0 if data not write, 1 - data write, 2 - error to write
//=============================================================================
EXTERN_C uint8 FrameWork_FlashProgrammCntrl(tFlashDrv *pThis,  
                                            uint32     addr, 
                                            uint8      length, 
                                            uint8     *data);

#endif // _FLASH_H_
