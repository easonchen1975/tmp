/*****************************************************************************
*    Copyright (c) 2002 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Enhanced square root computation:
 *                            Newton-Rafson algorithm
 *    @file                   L_sqrt_s.h
 *    @author                 A.Nazarov
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef _L_SQRT_S_H___
#define _L_SQRT_S_H___	1

#include "comtypes.h"


#if defined(_TMS320C6X)
  #define PORTING_L_SQRT_S      1
  #define L_sqrt_s              L_sqrt_sC64
#else
  #define PORTING_L_SQRT_S      0
  #define L_sqrt_s              L_sqrt_sCversion

//=========================================================================
/// @brief      Computes square root of Q15 number
///             iterate 4 times: max error 4.e-5.
///             Precision conversion: Q result = Q((x-1)/2 + 8), when Qx is odd
///
/// @param[in]  x - int16 input
/// @return     int16 square root of input
//=========================================================================
EXTERN_C   int16  L_sqrt_s (int16 x);

#endif   //defined(_TMS320C6X)


#if PORTING_L_SQRT_S==1

  // Use inlined version of L_sqrt_s()
  #include "L_sqrt_s.c"

#endif  // PORTING_L_SQRT_S

#endif  // _L_SQRT_S_H___


