/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Data types for whole framework initialization
 *   @file                   Framework.c
 *   @author                 M. Zhokhova
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*****************************************************************************/

#include "Framework.h"
#include "SampleDriver.h"
#include "comfuncs.h"
#include "L_sqrt_l.h"
#include "MainManager.h"

//============================================================================
/// @brief     Select current FLASH object
/// 
/// @param[in] flashData      - data for flash
/// @param[in] flags          - pointer to control flags
/// @param[in] current_object - pointer to current object
//============================================================================
static void CurrentObject(tFlashData     flashData,
                          tFlagsControl *flags,
                          int8          *current_object)
{
  /// start upload file for System by RS422 and control FLASH error
  if((flashData.controlFile.Process == 1) && (flashData.controlFile.ErrorFlash == fg_OFF))
  {
    *current_object = SYSTEM_FLASH;
    return;
  }
  /// start upload configuration setup by RS422
  if (flashData.controlConfig.SaveData == fg_ON)
  {
    *current_object = CONFIG_FLASH;
    return;
  }
  /// start upload week number and overflow week number
  if (flashData.controlWN.SaveData == fg_ON)
  {
    *current_object = WN_FLASH;
    return;
  }
  /// start clearing ephemeris and almanac
  if (flags->FL_CLEAR == 1)
  {
    flags->FL_CLEAR = 0;
    *current_object = CLEAR_FLASH;
    return;
  }
  /// if current object ephemeris then next almanac
  if (*current_object == EPH_FLASH)
  {
    *current_object = ALM_FLASH;
    return;  
  }
  /// if current object almanac or any other object then next ephemeris
  *current_object = EPH_FLASH;
  return;  
}

//============================================================================
/// @brief     Control FLASH
/// 
/// @param[in] pThis      - pointer to instance
/// @param[in] pTopSystem - pointer to top system structure
/// @param[in] config     - pointer to configuration setup
/// @param[in] flags      - pointer to control flags
/// @param[in] weekData   - pointer to week data structure
//============================================================================
void FrameWork_FlashControl(tFlashDrv     *pThis, 
                            tTopSystem    *pTopSystem,
                            CONFIG_SETUP  *config,
                            tFlagsControl *flags,
                            tWeekData     *weekData)
{
  /// if current status FLASH is free then search next task
  if (pThis->state.current_status == FREE_FLASH)
    CurrentObject(pThis->flashData, flags, &pThis->state.current_object);

  switch(pThis->state.current_object) 
  {
  case CONFIG_FLASH:
    {
      MainMgr_ConfigFlash(pThis, config);
    }
    break;
  case SYSTEM_FLASH:
    {
      if (pThis->flashData.controlFile.SaveData == fg_ON)
        MainMgr_UploadFile(pThis);
    }
    break;
  case WN_FLASH:
    {
      MainMgr_WeekDataFlash(pThis, weekData);
    }
    break;
  case EPH_FLASH:
    {
      MainMgr_EphemerisFlash(pThis, pTopSystem);
    }
    break;
  case ALM_FLASH:
    {
      MainMgr_AlmanacFlash(pThis, pTopSystem);
    }
    break;
  case CLEAR_FLASH:
    {
      if (MainMgr_EphAlmClear(pThis) == 1)
        flags->FL_CLEAR = 0;
    }
  default:
    break;
  }
}

//==========================================================================================
/// @brief      Framework object initialize
/// @param[in]  pFramework  - pointer to instance
/// @param[in]  BaudRate422 - UART baud rate
//===========================================================================================
void FrameWork_Init(tFramework *pFramework,
                    int8        BaudRate422)
{
  pFramework->weekData.DataError = fg_ON;
  // for target
  #ifdef USE_DSPBIOS
    UartTerminalInit(&pFramework->DebugTerminal, BaudRate422);

    // initialize framework event handlers
    pFramework->EventHandler.pHost                  = pFramework;
    pFramework->EventHandler.pfnPostNavTask         = &BasicDSP_FnPostNavTask;
    pFramework->EventHandler.pfnPostTrackingManager = &BasicDSP_FnPostTrackingManager;
 
    pFramework->Timeout       = fg_OFF;
  #endif
}

//======================================================================
/// Get debug terminal
///
/// @return pointer to Debug terminal
//======================================================================
tTerminal *GetPrimaryDebugTerminal()
{
  extern tFramework FrameworkState;
  return &FrameworkState.DebugTerminal;
}

#ifdef USE_DSPBIOS

//======================================================================
/// timer 0 start
///
/// @param[in]  time - time for timer
//======================================================================
void Timer0Start(uint32 time)
{
  *(uint32 *)0x01940000 = 0x210;  /// Timer 0 stop
  *(uint32 *)0x01940004 = time;   /// Timer 0 period
  *(uint32 *)0x01940000 = 0x2D1;  /// Timer 0 start
}

//======================================================================
/// timer 0 stop
//======================================================================
void Timer0Stop()
{
  *(volatile uint32 *)0x01940000 = 0x210;  /// Timer 0 stop
}

//======================================================================
/// timer 3 start
//======================================================================
void PROFILE_START()
{  
  *(uint32 *)0x01AC0000 = 0x210;       /// Timer 2 stop
  *(uint32 *)0x01AC0004 = 0xFFFFFFFF;  /// Timer 2 period
  *(uint32 *)0x01AC0000 = 0x2D1;       /// Timer 2 start
}

//==========================================================================
/// Read timer 2 value
///
/// @return Processor time steps between PROFILE_START() and PROFILE_STOP()
//==========================================================================
uint32 PROFILE_READ()
{ 
  return (*(volatile uint32 *)0x01AC0008) << 3;
}

//==============================================================================
/// Stop profiling timer 2
///
/// @return Processor time steps between PROFILE_START() and this function call
//==============================================================================
uint32 PROFILE_STOP()
{
  *(volatile uint32 *)0x01AC0000 = 0x210;  /// Timer 3 stop

  return PROFILE_READ();
}
#endif





