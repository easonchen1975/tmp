/**************************************************************************************
*    Copyright (c) 2005 Spirit Corp.
**************************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  raw measurements formation
*    @file                   RawMeasurForm.h
*    @author                 M. Zhokhova
*    @date                   25.06.2005
*    @version                1.0
*/
/*************************************************************************************/

#ifndef _RAW_MEASUR_FORM_H_
#define _RAW_MEASUR_FORM_H_

#include "comtypes.h"
#include "FrameDecoderGPS.h"

/// floating-point raw data structure
typedef struct 
{
  float64  ps_delay;   ///< SV's pseudo-delay [s]
  float64  ps_freq;    ///< SV's pseudo-frequency [Hz]
  double   timeCoord;  ///< sec
  int8     attempt;    ///< validity flag
}
tRawData;

/// fix-point raw data structure
typedef struct 
{
  tLongFract ps_delay;    ///< SV's pseudo-delay [ms]
  tLongFract ps_freq;     ///< SV's pseudo-frequency [Hz]
  tLongFract timeCoord;   ///< ms
  int8       attempt;     ///< validity flag
}
tRawDataFix;

//=====================================================================================
///  Forming raw measurements
///
///  @param[in]  dopplerQ15         - Doppler frequency in Q15
///  @param[in]  dopplerPrimeQ10    - Doppler rate frequency in Q10
///  @param[in]  delay              - delay
///  @param[in]  tauQ               - quantized delay
///  @param[in]  phase_err_avrQ14   - filtered phase error in Q14 [rad.]
///  @param[in]  epochInBit         - number of ms between bits (0�19)
///  @param[in]  time               - time system in millisecond
///  @param[in]  epoch_control      - epoch control: reiteration or skip
///  @param[in]  R                  - clock shift
///  @param[in]  decoder            - pointer to satellite decoder structure
///  @param[in]  pPreamble          - pointer to structure with saved preamble time
///  @param[in]  Nframe_add         - number of additional frames in reacquisition mode
///  @param[in]  CorrTimeByPreamble - time corrected by preamble
///  @param[out] raw_data           - pointer to raw data structure
//=====================================================================================
EXTERN_C void SatGPS_RawMeasurementsFormingFix(int32        dopplerQ15,
                                               int32        dopplerPrimeQ10,
                                               tLongFract48 delay,
                                               tLongFract48 tauQ,
                                               int16        phase_err_avrQ14,
                                               int          epochInBit,
                                               const uint32 time,
                                               int8         epoch_control,
                                               tLongFract   R,
                                               tDecoder    *decoder,
                                               tPreamble   *pPreamble,
                                               int32        Nframe_add,
                                               eFlag        CorrTimeByPreamble,
                                               tRawDataFix *raw_data);

#endif  //_RAW_MEASUR_FORM_H_

