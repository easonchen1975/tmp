/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  10-bit Cos/sin table: contains packed cos/sin
 *    @file                   expTbl.h
 *    @author                 Alex Nazarov
 *    @date                   01.08.2005
 *    @version                2.0
 */
/*****************************************************************************/

#ifndef EXP_TBL_H__
#define EXP_TBL_H__

#include "comtypes.h"
#include "expTbl.h"

/// Cos/sin table: contains packed cos/sin
EXTERN_C const uint32 expTbl [1024];

#endif  // EXP_TBL_H__
