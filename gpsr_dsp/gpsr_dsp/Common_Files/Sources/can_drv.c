

#if 1
//#ifdef CAN_PORT_TMTC

#include <string.h>
#include "Framework.h"
#include "MainManager.h"
#include "comtypes.h"
#include "can_drv.h"


//------------- EMIFB_CE3 -------------------------------------------------

//------------- EMIFB_CE3 -------------------------------------------------

/* 25MHz */
#define EMIFB_CE3_WRSETUP      	3
#define EMIFB_CE3_WRSTRB        3
#define EMIFB_CE3_WRHLD         3

#define EMIFB_CE3_RDSETUP       7
#define EMIFB_CE3_RDSTRB        0xe
#define EMIFB_CE3_RDHLD         3

#define EMIFB_CE3_TA            0x3
#define EMIFB_CE3_MTYPE         0x0
#define EMIFB_CE3_WRHLDMSB  	0x0

#define EMIFB_CE3_CONFIG ((EMIFB_CE3_WRSETUP << 28) | (EMIFB_CE3_WRSTRB << 22) | (EMIFB_CE3_WRHLD << 20) | (EMIFB_CE3_RDSETUP << 16) | (EMIFB_CE3_TA << 14) | (EMIFB_CE3_RDSTRB << 8) | (EMIFB_CE3_MTYPE << 4) | (EMIFB_CE3_WRHLDMSB << 3) | (EMIFB_CE3_RDHLD))
//#define EMIFB_CE3_CONFIG 	0x30F7CE03
#define EMIFB_CE3_CTRL      0x01a80014
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------


#define CAN_MODULE_BASE		0x6C000000//0x68000040	// CE3
//#define CAN_PORT_SEL  *(volatile uint8 *)0x6c0000cc
#define CAN_PORT_SEL  (((Can_Core_Reg*)CAN_MODULE_BASE)->xcr_sel)

#define NUM_CAN_VPORTS_PER_PPORT	2

#define CLK_RATE	20000000	// 20MHz

//SLOW
#define S_BAUD_RATE	500000		// 500Kbps
#define N_CLK_PER_BAUD		(CLK_RATE/S_BAUD_RATE)	// 60
#define CLK_DIV		2//5
#define N_TQ		(N_CLK_PER_BAUD/CLK_DIV)	// 20
#define SEG_1_TQ	(N_TQ*75/100)               //15
#define SEG_2_TQ	(N_TQ-SEG_1_TQ)

#define S_PRESC		(CLK_DIV-1)                  //2
#define S_SEG_1		(SEG_1_TQ-2)                 //13
#define S_SEG_2		(SEG_2_TQ-1)
#define S_SJW		2

// FAST 
//#define CAN_TWO_RATE
#ifdef CAN_TWO_RATE
	#define F_BAUD_RATE	1250000 //2000000 //1000000 //500000 // 500Kbps
	#define F_N_CLK_PER_BAUD		(CLK_RATE/F_BAUD_RATE)	// 50
	#define F_CLK_DIV		2//5
	#define F_N_TQ		(F_N_CLK_PER_BAUD/F_CLK_DIV)	// 10
	#define F_SEG_1_TQ	(F_N_TQ*75/100)
	#define F_SEG_2_TQ	(F_N_TQ-F_SEG_1_TQ)

	#define F_PRESC 	(F_CLK_DIV-1)//S_PRESC
	#define F_SEG_1		(F_SEG_1_TQ-2)//S_SEG_1
	#define F_SEG_2		(F_SEG_2_TQ-1)//S_SEG_2
	#define F_SJW		2//S_SJW
#else
	#define F_BAUD_RATE	S_BAUD_RATE
	#define F_N_CLK_PER_BAUD	N_CLK_PER_BAUD
	#define F_CLK_DIV	CLK_DIV
	#define F_N_TQ		N_TQ
	#define F_SEG_1_TQ	SEG_1_TQ
	#define F_SEG_2_TQ	SEG_2_TQ

	#define F_PRESC 	S_PRESC
	#define F_SEG_1		S_SEG_1
	#define F_SEG_2		S_SEG_2
	#define F_SJW		S_SJW
#endif


#define CAN_TXCVER_PORT_0	0
#define CAN_TXCVER_PORT_1	1

//#define CAN_FD_DISABLED
//#define INTERNAL_LOOP_BACK_MODE 

//#define PTB_ONLY


#define CAN_TX_TEST
#define CAN_RX_TEST

#ifdef INTERNAL_LOOP_BACK_MODE
	#define CAN_TX_TEST
	#define CAN_RX_TEST
#endif

// CAN test CAN ids
#define CAN_TEST_TX_ID_1	11100001
#ifdef INTERNAL_LOOP_BACK_MODE
	#define CAN_TEST_RX_ID_1	CAN_TEST_TX_ID_1
#else
	#define CAN_TEST_RX_ID_1	11100002
#endif

#define CAN_TEST_TX_ID_2	22200001
#ifdef INTERNAL_LOOP_BACK_MODE
	#define CAN_TEST_RX_ID_2	CAN_TEST_TX_ID_2
#else
	#define CAN_TEST_RX_ID_2	11100002
#endif
	
#define BATCH_FRAME_TO_SEND		1


typedef struct {

	uint8 rbuf[80] ;	// 0x00 ~ 0x4f
	uint8 tbuf[72] ;	// 0x50 ~ 0x97
	uint8 tts[8];       // 0x98 ~ 0x9f
	uint8 cfg_stat ;	// 0xa0
	uint8 tcmd ;	// 0xa1
	uint8 tctrl ;	// 0xa2
	uint8 rctrl ;	// 0xa3
	uint8 rtie ;	// 0xa4
	uint8 rtif ;	// 0xa5
	uint8 errint ;	// 0xa6
	uint8 limit ;	// 0xa7
	uint8 s_seg_1 ;// 0xa8
	uint8 s_seg_2 ;// 0xa9
	uint8 s_sjw ;	// 0xaa
	uint8 s_presc ;// 0xab
	uint8 f_seg_1 ;// 0xac
	uint8 f_seg_2 ;// 0xad
	uint8 f_sjw ;	// 0xae	
	uint8 f_presc ;// 0xaf	
	uint8 ealcap ;	// 0xb0
	uint8 tdc ;	// 0xb1
	uint8 recnt ;	// 0xb2
	uint8 tecnt ;	// 0xb3
	uint8 acfctrl ;	// 0xb4
	uint8 timecfg ;	// 0xb5
	uint8 acf_en_0 ;	// 0xb6
	uint8 acf_en_1 ;	// 0xb7		
	uint8 acf_0 ;	// 0xb8
	uint8 acf_1 ;	// 0xb9
	uint8 acf_2 ;	// 0xba
	uint8 acf_3 ;	// 0xbb
	uint8 ver_0 ;	// 0xbc
	uint8 ver_1 ;	// 0xbd	
    uint8 tbslot ;	// 0xbe
	uint8 ttcfg ;	// 0xbf
	uint8 ref_msg_0;//0xc0
	uint8 ref_msg_1;//0xc1
	uint8 ref_msg_2;//0xc2
    uint8 ref_msg_3;//0xc3
    uint8 trig_cfg_0;//0xc4
    uint8 trig_cfg_1;//0xc5
    uint8 tt_trig_0;//0xc6
    uint8 tt_trig_1;//0xc7
    uint8 tt_wtrig_0;//0xc8
    uint8 tt_wtrig_1;//0xc9
    uint8 reserved_0;//0xca
    uint8 reserved_1;//0xcb
	uint8 xcr_sel;      // 0xcc	
} Can_Core_Reg ;

typedef struct {
	int used ;
	uint32 txid ;
	uint32 rxid ;
} Can_Vport ;

static Can_Vport can_vports[NUM_CAN_VPORTS_PER_PPORT] ;

static volatile Can_Core_Reg *p_can_reg = (Can_Core_Reg *)CAN_MODULE_BASE;

static int err_counter[20] ;
static uint8 alc_error=0 ;
static int restart_can_core_flag = 0 ;

// OBC I/F

#ifdef CAN_A_SIDE
	#define CAN_OBC_TARGET_ID	0x08
#else
	#define CAN_OBC_TARGET_ID	0x09
#endif

#define CAN1_OBC_TARGET_ID	0x08
#define CAN2_OBC_TARGET_ID	0x09


#define	CAN_OBC_TX_ID	((0x01 << 27) | (CAN_OBC_TARGET_ID<<22))
#define	CAN_OBC_RX_ID	((0x00 << 27) | (CAN_OBC_TARGET_ID<<22))

#define	CAN1_OBC_TX_ID	((0x01 << 27) | (CAN1_OBC_TARGET_ID<<22))
#define	CAN1_OBC_RX_ID	((0x00 << 27) | (CAN1_OBC_TARGET_ID<<22))
#define	CAN2_OBC_TX_ID	((0x01 << 27) | (CAN2_OBC_TARGET_ID<<22))
#define	CAN2_OBC_RX_ID	((0x00 << 27) | (CAN2_OBC_TARGET_ID<<22))

static uint8 signature[4] = {START_OF_FRAME_0, START_OF_FRAME_1, START_OF_FRAME_2, START_OF_FRAME_3};
static int32 obc_can_ch, obc_can_ch1, obc_can_ch2, can_ch_sel = 0 ;


int set_obc_can_ch(int can_ch_sel ){

	if(can_ch_sel == 1 ){
		obc_can_ch = obc_can_ch1;
		//p_can_reg->xcr_sel = 0;  //default Select CAN1
		CAN_PORT_SEL = 0 ;
	}
	else if(can_ch_sel == 2 ){
		obc_can_ch = obc_can_ch2;
		//p_can_reg->xcr_sel = 1;  //default Select CAN2
		CAN_PORT_SEL = 1 ;
	}
	else{

	}
}

int set_obc_can_ch2(int can_ch_sel ){
	uint32 port_value;

	port_value = CAN_PORT_SEL;

	if(can_ch_sel == 1 ){
		obc_can_ch = obc_can_ch1;
		//p_can_reg->xcr_sel = 0;  //default Select CAN1
		port_value = port_value & 0x02;
		CAN_PORT_SEL = port_value ;
	}
	else if(can_ch_sel == 2 ){
		obc_can_ch = obc_can_ch2;
		//p_can_reg->xcr_sel = 1;  //default Select CAN2
		port_value = port_value & 0x02;
		CAN_PORT_SEL = (port_value | 1 );
	}
	else{

	}
}


int check_switch_CAN_PortB_pulse(){
	uint32 port_value;

	port_value = CAN_PORT_SEL;

	if( CAN_PORT_SEL & 0x02){

		//should switch to port B, also clear status
		set_obc_can_ch(2);
		//port_value = 0x1 & port_value;
	}

	return CAN_PORT_SEL;
}


int can_init()
{
	*((uint32 *)EMIFB_CE3_CTRL) = EMIFB_CE3_CONFIG ;

	memset( &can_vports[0], 0, sizeof(can_vports) ) ;
	memset( &err_counter[0], 0, sizeof(err_counter) ) ;
	
//	p_can_reg = (Can_Core_Reg *)CAN_MODULE_BASE ;


	return DRV_CAN_OK ;
}


void error_log(int error_no)
{
	err_counter[error_no] ++ ;
	return ;
}

/*
int can_rx_config_sel(int ch){

	volatile uint8 * p_acf_enable, val;
	int i;

	i = ch;

	if ( can_vports[i].used == 0 )
		return;

	// set ACF & ACM
	p_can_reg->acfctrl = i ;	// select ACF & filter #
	p_can_reg->acf_0 = ( can_vports[i].rxid ) & 0xff ;		// ACF0
	p_can_reg->acf_1 = ( can_vports[i].rxid >> 8 ) & 0xff ;	// ACF1
	p_can_reg->acf_2 = ( can_vports[i].rxid >> 16 ) & 0xff ;	// ACF2
	p_can_reg->acf_3 = ( can_vports[i].rxid >> 24 ) & 0x1f ;	// ACF3

	p_can_reg->acfctrl = ( 0x20 | i ) ;	// select ACM & filter #
	p_can_reg->acf_0 = 0 ;	// ACM0
	p_can_reg->acf_1 = 0 ;	// ACM1
	p_can_reg->acf_2 = 0 ;	// ACM2
	p_can_reg->acf_3 = 0 ;	// ACM3
	// enable filter, should do outside "reset" ?

	p_acf_enable = ( i < 8 ) ? (&(p_can_reg->acf_en_0)) : (&(p_can_reg->acf_en_1)) ;
	val = * p_acf_enable ;
	val |= ( 1 << ( i % 8 ) ) ;
	* p_acf_enable = val ;

}
*/

int can_start(void)
{
	volatile uint8 * p_acf_enable, val ;
	int i ;
	uint16 temp0, temp1;

// enable reset bit
//	val = *(p_base + 0x8a) ;	
	val = 0x80 ;
	p_can_reg->cfg_stat = val ;
	delay_mcs(5000);	
// set bit timing	
//	p_can_reg->bittime_0 = 	(can_bit_timing[0][0] & 0x3f) | (can_bit_timing[1][2] << 6) ;	// BITTIME_0
//	p_can_reg->bittime_1 = 	(can_bit_timing[0][1] & 0x1f) | (can_bit_timing[1][1] << 5) ;	// BITTIME_1
//	p_can_reg->bittime_2 = 	(can_bit_timing[0][2] & 0x0f) | (can_bit_timing[1][0] << 4) ;	// BITTIME_2
    p_can_reg->s_seg_1 = S_SEG_1;
    p_can_reg->s_seg_1 = S_SEG_1;
    p_can_reg->s_seg_2 = S_SEG_2;
    p_can_reg->s_seg_2 = S_SEG_2;
    p_can_reg->s_sjw = S_SJW;
    p_can_reg->s_sjw = S_SJW;
	p_can_reg->s_presc = S_PRESC;//can_bit_timing[0][3] ;					// S_PRESC
	p_can_reg->s_presc = S_PRESC;
    p_can_reg->f_seg_1 = F_SEG_1;
    p_can_reg->f_seg_1 = F_SEG_1;
    p_can_reg->f_seg_2 = F_SEG_2;
    p_can_reg->f_seg_2 = F_SEG_2;
    p_can_reg->f_sjw = F_SJW;
    p_can_reg->f_sjw = F_SJW;	
	p_can_reg->f_presc = F_PRESC;//can_bit_timing[1][3] ;					// F_PRESC
	p_can_reg->f_presc = F_PRESC;
    p_can_reg->xcr_sel = 0;  //default Select CAN1

#if 1


// disable all filters
	p_can_reg->acf_en_0 = 0 ;
	p_can_reg->acf_en_1 = 0 ;

	for ( i = 0 ; i < 1 /*NUM_CAN_VPORTS_PER_PPORT */; i ++ )
	{
		if ( can_vports[i].used == 0 )
			continue ;

		/*
	// set ACF & ACM	
		p_can_reg->acfctrl = i ;	// select ACF & filter #
		p_can_reg->acf_0 = ( can_vports[i].rxid ) & 0xff ;		// ACF0
		p_can_reg->acf_1 = ( can_vports[i].rxid >> 8 ) & 0xff ;	// ACF1
		p_can_reg->acf_2 = ( can_vports[i].rxid >> 16 ) & 0xff ;	// ACF2
		p_can_reg->acf_3 = ( can_vports[i].rxid >> 24 ) & 0x1f ;	// ACF3
		
		p_can_reg->acfctrl = ( 0x20 | i ) ;	// select ACM & filter #
		p_can_reg->acf_0 = 0 ;	// ACM0
		p_can_reg->acf_1 = 0 ;	// ACM1
		p_can_reg->acf_2 = 0 ;	// ACM2
		p_can_reg->acf_3 = 0 ;	// ACM3
	// enable filter, should do outside "reset" ?
*/


		p_can_reg->acfctrl = i ;	// select ACF & filter #
		p_can_reg->acf_0 = ( can_vports[i].rxid ) & 0xff ;		// ACF0
		p_can_reg->acf_1 = ( can_vports[i].rxid >> 8 ) & 0xff ;	// ACF1
		p_can_reg->acf_2 = ( can_vports[i].rxid >> 16 ) & 0xff ;	// ACF2
		p_can_reg->acf_3 = ( can_vports[i].rxid >> 24 ) & 0x1f ;	// ACF3


		//support mask RX ID
		p_can_reg->acfctrl = ( 0x20 | 0) ;	// select ACM & filter #
		p_can_reg->acf_0 = 0 ;	// ACM0
		p_can_reg->acf_1 = 0 ;	// ACM1
		p_can_reg->acf_2 = 0xC0 ;	// ACM2
		p_can_reg->acf_3 = 0x00 ;	// ACM3
		// end of support mask RX ID


		p_acf_enable = ( i < 8 ) ? (&(p_can_reg->acf_en_0)) : (&(p_can_reg->acf_en_1)) ;
		val = * p_acf_enable ;
		val |= ( 1 << ( i % 8 ) ) ;
		* p_acf_enable = val ;
		
	}


#else
//All Pass for RX 	
     p_can_reg->acfctrl = 0x0;
     p_can_reg->acf_0 = 0x0;
     p_can_reg->acf_1 = 0x0;
     p_can_reg->acf_2 = 0x0;
     p_can_reg->acf_3 = 0x0;
     p_can_reg->acfctrl = 0x0|0x20;
     p_can_reg->acf_0 = 0xff;
     p_can_reg->acf_1 = 0xff;
     p_can_reg->acf_2 = 0xff;
     p_can_reg->acf_3 = 0xff;
     p_can_reg->acf_en_0 = 0x1;
     p_can_reg->acf_en_1 = 0x0;
#endif
	
	 p_can_reg->tctrl = 0xa0;//FD_ISO=1; TTTBM=1
// disable reset bit	
	val = p_can_reg->cfg_stat ;	
	val = p_can_reg->cfg_stat ;
	val &= ~0x80 ;
//	val = 0 ;
	p_can_reg->cfg_stat = val ;	
	delay_mcs(5000);	

#ifdef INTERNAL_LOOP_BACK_MODE
// set loopback mode
	val = p_can_reg->cfg_stat ;	
	val |= 0x20 ;
//	val = 0x20 ;
	p_can_reg->cfg_stat = val ;	
#endif
	
// set TCMD register
	val = p_can_reg->tcmd ;
	val = p_can_reg->tcmd ;

	
#ifdef PTB_ONLY
	val &= ~0x80 ;	// TBSEL=0(PTB)
//	val |= 0x10;	// TPE=1
#else
	val |= 0x88 ;	// TBSEL=1(STB), TPA=1
#endif	
	
	p_can_reg->tcmd = val;	
// set RTIE register
	p_can_reg->rtie = 0xff ;		// enable all interrupts
// set ERRINT register
	p_can_reg->errint = 0xff ;		// enable error
	return DRV_CAN_OK ;
}


int can_open(uint32 txid, uint32 rxid)
{
	int i ;
	
// allocate vport & filter	
	for ( i = 0 ; i < NUM_CAN_VPORTS_PER_PPORT ; i ++ )
		if ( can_vports[i].used == 0 )
			break ;
	if ( i >= NUM_CAN_VPORTS_PER_PPORT )
		return DRV_CAN_PAR_ERROR ;
	
	can_vports[i].used = 1 ;
	can_vports[i].txid = txid ;
	can_vports[i].rxid = rxid ;

	return ( i + 0x100 ) ;
}



int can_rx_frame(int vport, uint8 *rx_frame)
{
	int i;
	uint32 len ;
	volatile uint8 val ;
	uint32 rxid ;
	int rx_can_2_frame ;
	uint8 temp0,temp1, temp2, temp3;
	
	if ( rx_frame == NULL )
		return -1 ;
		
	vport = vport % 0x100 ;
	
	if ( can_vports[vport].used == 0 )
		return DRV_CAN_RES_CONFLICT ;

//	val = p_can_reg->rtif ;
//	if ( val )
//		error_log( __LINE__ );

// error flags
	val = p_can_reg->errint ;
//	val = p_can_reg->errint ;

	if ( val & 0x01 )
		error_log( 2 );	// bus error counter
	if ( val & 0x04 )
		error_log( 3 );	// arbitration lost error counter
	if ( val & 0x10 )
		error_log( 4 );	// error passive counter
/*		
	if ( val & 0x40 )
		error_log( 5 );	// error passive mode active counter
	if ( val & 0x80 )		
		error_log( 6 );	//  Error Warning Limit Reached Error Counter
*/
// KOER counter
	val = p_can_reg->ealcap ;
//	val = p_can_reg->ealcap ;
	alc_error = val & 0x1f ;	// ALC error
	if ( alc_error )
		error_log( 13 );
	
	val >>= 5 ;

	if ( val )
		restart_can_core_flag = 1 ;

	if ( val & 0x01 )
		error_log( 5 );
	if ( val & 0x02 )
		error_log( 6 );
	if ( val & 0x03 )
		error_log( 7 );
	if ( val & 0x04 )
		error_log( 8 );
	if ( val & 0x05 )
		error_log( 9 );
	if ( val & 0x06 )
		error_log( 10 );

	
// check current status
	val = p_can_reg->cfg_stat ;	
//	val = p_can_reg->cfg_stat ;
	if ( val & 0x80 )	// in reset?
	  p_can_reg->cfg_stat &= ~0x80;

	val = p_can_reg->cfg_stat ;
	if ( val & 0x81 )	// bus off? in reset?
		return DRV_CAN_HW_NOTREADY ;	


// check RX buffer, RCTRL	
	val = p_can_reg->rctrl ;
//	val = p_can_reg->rctrl ;
	if ( val & 0x20 )
	{
		error_log( 11 );	// Receive buffer Overflow error counter
//		event_report_can_error(pport, val) ;
	}
	
	if ( (val & 0x03) == 0 )	// buffer empty?
		return DRV_CAN_BUFFER ;

// get frame
	val = p_can_reg->rbuf[3] ;
	val &= 0x80;	// ESI bit
	if ( val )
		error_log(12) ;

temp0 = p_can_reg->rbuf[0];
//temp0 = p_can_reg->rbuf[0];
temp1 = p_can_reg->rbuf[1];
//temp1 = p_can_reg->rbuf[1];
temp2 = p_can_reg->rbuf[2];
//temp2 = p_can_reg->rbuf[2];
temp3 = p_can_reg->rbuf[3] & 0x1f;
//temp3 = p_can_reg->rbuf[3];
	rxid =  (temp3 << 24) | (temp2 << 16) | (temp1 << 8) | (temp0);
	if ( rxid != can_vports[vport].rxid )	// not my frame ?
		return DRV_CAN_ERROR_FRM ;
		
	val = p_can_reg->rbuf[4] ;
//	val = p_can_reg->rbuf[4] ;

//	if ( (val & 0xe0) != 0xa0 )	// IDE!=1 or RTR==1 or EDL!=1
//		return DRV_CAN_ERROR_FRM ;

	if ( val & 0x20 )	// EDL bit
		rx_can_2_frame = 0 ;
	else
		rx_can_2_frame = 1 ;

	len = val & 0x0f ;
	if ( len >= 0x0d )	// b'1101
		len = 16 + (len - 12) * 16 ;
	else if ( len >= 0x09 )		// b'1001
		len = 8 + (len - 8) * 4 ;

	if ( rx_can_2_frame && len > 8 )
		len = 8 ;

	for ( i = 0 ; i < len ; i ++ )
	{
		rx_frame[i] = p_can_reg->rbuf[8+i]  ;
	}
// release frame, RCTRL	
	val = p_can_reg->rctrl ;
//	val = p_can_reg->rctrl ;
	val |= 	0x10 ;	// RREL bit
	p_can_reg->rctrl = val ;
	return len ;
}


int can_tx_frame(int vport, uint8 *tx_frame, int32 len)
{
	int i ;
	volatile uint8 val ;
	uint8 dlc ;
	uint32 txid ;
#ifdef CAN_TIME_MEASUREMENT
	static uint32 time=0;
	static double t_average;
	static int tx_pkts=0;

	PROFILE_START();
#endif

	if ( tx_frame == NULL )
		return DRV_CAN_PAR_ERROR ;
		
	if ( len < 0 || len > 64 )
		return DRV_CAN_PAR_ERROR ;
		
	vport = vport % 0x100 ;
	
	if ( can_vports[vport].used == 0 )
		return DRV_CAN_RES_CONFLICT ;

//	val = p_can_reg->rtif ;
//	if ( val )
//		error_log( __LINE__ );

// error flags
//	val = p_can_reg->errint ;
	val = p_can_reg->errint ;
	
	if ( val & 0x01 )
		error_log( 2 );	// bus error counter
	if ( val & 0x04 )
		error_log( 3 );	// arbitration lost error counter
	if ( val & 0x10 )
		error_log( 4 );	// error passive counter
/*		
	if ( val & 0x40 )
		error_log( 5 );	// error passive mode active counter
	if ( val & 0x80 )		
		error_log( 6 );	//  Error Warning Limit Reached Error Counter
*/
		
// KOER counter
//	val = p_can_reg->ealcap ;
	val = p_can_reg->ealcap ;	
	alc_error = val & 0x1f ;	// ALC error
	if ( alc_error )
		error_log( 13 );
	val >>= 5 ;
	if ( val )
		restart_can_core_flag = 1 ;

	if ( val & 0x01 )
		error_log( 5 );
	if ( val & 0x02 )
		error_log( 6 );
	if ( val & 0x03 )
		error_log( 7 );
	if ( val & 0x04 )
		error_log( 8 );
	if ( val & 0x05 )
		error_log( 9 );
	if ( val & 0x06 )
		error_log( 10 );

// check current ststus	
//	val = p_can_reg->cfg_stat ;	
	val = p_can_reg->cfg_stat ;

	if ( val & 0x80 )	// in reset?
	  p_can_reg->cfg_stat &= ~0x80;
	  
	val = p_can_reg->cfg_stat ;	  
	if ( val & 0x81 )	// bus off? in reset?
		return DRV_CAN_HW_NOTREADY ;
		
#ifdef PTB_ONLY

	#if 0
		val = p_can_reg->tcmd ;

		while ( val & 0x10 )
		{
			delay_mcs(1000);
			if (++wait_cnt >= 5)
			{
				val |= 0x08 ;	// TPA=1
				p_can_reg->tcmd = val;

				wait_cnt=0;
				return DRV_CAN_HW_NOTREADY ;
			}
			val = p_can_reg->tcmd ;
		}

	#else

// check buffer, TCTRL
	val = p_can_reg->rtie ;	
	if ( val & 0x01 )	// TSFF bit, no buffer?
		return DRV_CAN_BUFFER ;

	#endif
#else
// check buffer, TCTRL
	val = p_can_reg->rtie ;	
	if ( val & 0x01 )	// TSFF bit, no buffer?
		return DRV_CAN_BUFFER ;

#endif	

	
// fill TBUF		
	txid = can_vports[vport].txid ;

	if ( len >= 32 )
		dlc = 12 + ( (len-16) / 16 ) ;
	else if ( len >= 8 )
		dlc = 8 + (( len - 8) / 4 );
	else
		dlc = len ;

	p_can_reg->tbuf[0] = (txid) & 0xff ;
	p_can_reg->tbuf[1] = (txid >> 8) & 0xff ;
	p_can_reg->tbuf[2] = (txid >> 16) & 0xff ;
	p_can_reg->tbuf[3] = (txid >> 24) & 0x1f ;
	
#ifdef CAN_FD_DISABLED
	p_can_reg->tbuf[4] = 0x80 | dlc ;	// CAN 2.0 only, IDE=1
#else
	//p_can_reg->tbuf[4] = 0xa0 | dlc |0x10;	// CAN FD IDE=1, EDL=1 BRS=1
	p_can_reg->tbuf[4] = 0xa0 | dlc |0x00;	// CAN FD IDE=1, EDL=1 BRS=1 ,
											// 20210928 : for FS-8, BRS change from 1 to 0
#endif


	for ( i = 0 ; i < len ; i ++ )
	{
		p_can_reg->tbuf[8+i] = (uint16) tx_frame[i] ;
    }

	
#ifdef PTB_ONLY
#else
// load TBUF into FIFO, TCTRL
	val = p_can_reg->tctrl;
	val |= 0x40;	
	p_can_reg->tctrl = val ;	// set TSNEXT
#endif
	
// start tx, TCMD register
//	val = p_can_reg->tcmd ;	
	val = p_can_reg->tcmd ;	
	
#ifdef PTB_ONLY
	val |= 0x10 ;	// TPE=1(STB) to tx
#else
	val |= 0x02 ;	// TSALL=1(STB) to tx
//	val |= 0x04 ;	// TSONE=1(STB) to tx
#endif

	p_can_reg->tcmd = val;
	
#ifdef CAN_TIME_MEASUREMENT
	time += PROFILE_STOP();

	tx_pkts++;
	if (tx_pkts == 10000)
	{
		t_average = (double)time/tx_pkts;	// cycle
		t_average /= 480;	// us
	}
#endif

	return DRV_CAN_OK ;
}

uint32 rx_ok_cnt=0, rx_nobuf_cnt=0, rx_frmerr_cnt=0, rx_hwerr_cnt=0, rx_others_cnt = 0, rx_data_err_cnt = 0;
uint32 tx_ok_cnt=0, tx_nobuf_cnt=0, tx_frmerr_cnt=0, tx_hwerr_cnt=0, tx_others_cnt = 0;
uint32 can_core_restart_cnt = 0 ;


int can_tmtc_init(void)
{
	can_init() ;

//	obc_can_ch = can_open(CAN_OBC_TX_ID, CAN_OBC_RX_ID) ;
	obc_can_ch1 = can_open(CAN1_OBC_TX_ID, CAN1_OBC_RX_ID) ;
	obc_can_ch2 = can_open(CAN2_OBC_TX_ID, CAN2_OBC_RX_ID) ;


	if ( obc_can_ch1 < 0 )
	{
		return 0 ;
	}

	if ( obc_can_ch2 < 0 )
	{
		return 0 ;
	}

	//obc_can_ch = obc_can_ch2;
	obc_can_ch = obc_can_ch1;

	delay_mcs(10000);

	can_start() ;

	return 1;
}

int can_send_tm(tTerminal *pTerminal)
{
	int size, i;
	unsigned char tm_buf[CAN_FRAME_TM_DATA_LENGTH+4];
	unsigned char chksum;
	unsigned char *p;
	int ret;
	static int num_frames=0;
	static int16 len=0 ;
	static int streaming_seq_num=0;
	
	size = FIFO_GetFilledSize(&pTerminal->TransmitQueue);
	if ( size < CAN_FRAME_TM_DATA_LENGTH)
		return 0;
	else
		size = CAN_FRAME_TM_DATA_LENGTH;

	p = &tm_buf[4];
	FIFO_GetBuffer(&pTerminal->TransmitQueue, (tFifoItem *)p, size);
	
	if ( !streaming_seq_num )
	{
		if (*(int *)p == *(int *)signature)
		{
			len = GET_SHORT_BE(&((New_Tmtc_Frame_Hdr *)p)->attribute[0]);
			len &= 0x3ff;	// bits 0~9
			len += TMTC_FRAME_OVERHEAD_LENGTH;	// overhead included
			num_frames = (len+CAN_FRAME_TM_DATA_LENGTH-1)/CAN_FRAME_TM_DATA_LENGTH;
			streaming_seq_num = 1;
		}
		else
			return 0;
	}

#if 0
	tm_buf[0] = 0x01;
	tm_buf[1] = num_frames;
	tm_buf[2] = streaming_seq_num++;
	tm_buf[3] = (len<CAN_FRAME_TM_DATA_LENGTH)?len:CAN_FRAME_TM_DATA_LENGTH;					
#else
	tm_buf[0] = 0x01;
	tm_buf[1] = 0;
	tm_buf[2] = (num_frames << 4) | (streaming_seq_num & 0x0f);
	tm_buf[3] = (len<CAN_FRAME_TM_DATA_LENGTH)?len:CAN_FRAME_TM_DATA_LENGTH;
	streaming_seq_num++;

	chksum=0;
	for (i=0; i<4+tm_buf[3]; i++)
	{
		chksum += tm_buf[i];
	}
	tm_buf[1] = chksum;

#endif


	ret=can_tx_frame(obc_can_ch, tm_buf, CAN_FRAME_TM_DATA_LENGTH+4);
	if ( ret != DRV_CAN_OK )
	{
		if ( ret == DRV_CAN_BUFFER )
			tx_nobuf_cnt ++ ;
		else
		if ( ret == DRV_CAN_ERROR_FRM )
			tx_frmerr_cnt ++ ;
		else
		if ( ret == DRV_CAN_HW_NOTREADY )
			tx_hwerr_cnt ++ ;
		else
			tx_others_cnt ++ ;

//		printf("$CAN TX fail, %d\n", ret);
	}
	

	len -= CAN_FRAME_TM_DATA_LENGTH;
	if ( len <= 0 )
	{
		streaming_seq_num = 0;
		len = 0 ;
		num_frames = 0;
	}
	
	return 1;
}

int can_rcv_tc(tTerminal *pTerminal)
{
	int len, i;
	unsigned char tc_buf[128];
	unsigned char chksum;


	if ( (len=can_rx_frame(obc_can_ch, tc_buf)) <= 0 )
		return 0;

	if ( len > CAN_FRAME_TM_DATA_LENGTH+4 )
		return 0;

	if (tc_buf[0] != 0x01)
		return 0;

	if (tc_buf[2] != 0x11)
		return 0;

	len = tc_buf[3];
	if ( len > CAN_FRAME_TM_DATA_LENGTH )
		return 0;

	chksum = tc_buf[0];
	for (i=2; i <len+4 ; i++)
		chksum += tc_buf[i];

	if (tc_buf[1] != chksum)
		return 0;

	memcpy((unsigned char *)&pTerminal->stringRead.str[0], &tc_buf[4], len);
	pTerminal->stringRead.length = len;
	pTerminal->FlagRead=TERMINAL_READ;

	return 1;
}

#endif
