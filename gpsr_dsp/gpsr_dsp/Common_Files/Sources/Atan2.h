/*****************************************************************************
     Copyright (c) 2002 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Arctangent computation
*    @file                   Atan2.c
*    @authors                L.Purto, (wrapped by A. Nazarov)
*    @version                1.0
*/
/*****************************************************************************/

#ifndef __ATAN2_H
#define __ATAN2_H

#include "comtypes.h"

#if defined(_TMS320C6400)
  #define PORTING_ATAN2_FXP      1
  #define ATan2_fxp              ATan2_fxpCversionC64
#else
  #define PORTING_ATAN2_FXP      0
  #define ATan2_fxp              ATan2_fxpCversion
#endif   //defined(_TMS320C6400)

//========================================================
/// Angular accuracy 2.1e-4 (at unity circle)
///
/// @param[in]  Vector - the complex number (Q15)
/// @return     Arctangent (phase) (Q14)
//========================================================
EXTERN_C   int16 ATan2_fxp(tComplex16 Vector);


#endif // __ATAN2_H


