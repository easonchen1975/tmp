/*************************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *************************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Report generator
 *   @file                   Report.c
 *   @author                 M. Zhokhova
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*************************************************************************************/

#include <stdio.h>
#include "Report.h"
#include "Framework.h"
#include "Control.h"


// for target
#ifdef USE_DSPBIOS

//=====================================================================================
///  Send status string to UART
///
///  @param[in]  sat_id    - satelite number
///  @param[in]  pGPSchan  - pointer to GPS channel structure
///  @param[in]  ps_delay  - pseudodelay
//=====================================================================================
void Stat2Uart(int8 sat_id, const tGPSchan *pGPSchan, const double ps_delay)
{
  tGPSchanStatus status = *GPSchanGetStatus(pGPSchan);
  double doppler, delay, CN_dBHz;
  tTerminal *pTerm = GetPrimaryDebugTerminal();
  static char buf[256];

  if(status.state != es_TRACKING)
  {
  sprintf(buf, "| -- |     --     |    --      |    --      |    --    |\r\n");
  pTerm->vtable.pfnWrite((struct tagTerminalDrv *)pTerm, buf, strlen(buf));
    return;
  }   
  doppler = ldexp(status.dopplerQ15,-15);
  delay   = LongFractGetDouble(status.delay) / 1000.;
  CN_dBHz = ldexp(status.CN_dBHzQ7,-7);
  sprintf(buf, "|  %2d| %10.1lf | %10.1lf | %10.2f |  %5.2f   |\r\n",
      (int)sat_id, doppler, delay * 2.99742e8, ps_delay, CN_dBHz);
  pTerm->vtable.pfnWrite((struct tagTerminalDrv *)pTerm, buf, strlen(buf));
}

#else
// for PC model

void SaveInFile(FILE *f, TEMP_EPH_GPS *pTmpGps, uint8 sv_num)
{
  fwrite(&sv_num, 1, 1, f);
  fwrite(pTmpGps, 1, sizeof(TEMP_EPH_GPS), f);
}

void ReadEphFromFile(tDecoderCommon *pDecCommon)
{
  FILE *file=fopen("eph_static.dat", "rb");
  uint8 sv_num;

  while(!feof(file))
  {
    fread(&sv_num, 1, 1, file);
    fread(&pDecCommon->TempEph[sv_num-1], sizeof(TEMP_EPH_GPS), 1, file);  
    pDecCommon->TempEph[sv_num-1].flag = 1;
    pDecCommon->TempEph[sv_num-1].flag_bl = 0;
  }
  fclose(file);
}

#if PLLDLL_FIX  // fix point PLL-DLL version
/*
void printStatistics(FILE* logFile,const tGPSchan *pGPSchan, tRawDataFix *raw_data_fix, uint32 ms_count)
{
   tGPSchanStatus status = *GPSchanGetStatus(pGPSchan);
   double doppler,CN_dBHz,ps_delay, dt, ps_phase, dopplerPrime, dll_state;
   tLongFract48 tauQ, delay, tmp;
   
   tauQ = status.tauQ;
   delay = status.delay;

   if (status.state!=es_TRACKING)
   {
	   memset(&status,0,sizeof(status));
   }
   doppler = ldexp(status.dopplerQ15,-15);
   dopplerPrime = ldexp(status.dopplerPrimeQ10, -10);
//   delay   = LongFract48GetDouble(status.delay) / 1000.;
//   tauQ    = LongFract48GetDouble(status.tauQ) / 1000.;
   CN_dBHz = ldexp(status.CN_dBHzQ7,-7);

   LongFract48Sub(&delay, &tauQ, &tmp);
	 tauQ.integral = 0;
	 LongFract48Add(&tmp, &tauQ, &delay);
	 dll_state = LongFract48GetDouble(&delay);

   dt = 2.0 - dll_state; 

   ps_delay = dll_state;

   ps_phase = CONV_PLL2DLL*(doppler + dopplerPrime*dt*1.0e-3*0.5)*(2*M_PI)*dt*1.0e-3; // sec
    
   // resulting pseudodelay calculation
   ps_delay = (ps_delay*1.0e-3 - ps_phase)*cLight; // sec

    fprintf(logFile,
	          "%5d %11.3lf %11.3lf %10.4lf %11.3lf %10.4lf %11.3lf\n",
		        ms_count,
		        ps_delay, (dll_state*cLight*1.0e-3), doppler*cLight/f0_GPS, 
            (LongFractGetDouble(&raw_data_fix->ps_delay)*cLight*1.0e-3), 
            (LongFractGetDouble(&raw_data_fix->ps_freq)*cLight/f0_GPS),
            LongFractGetDouble(&raw_data_fix->timeCoord)); 
   printf(
	       "% 6d % 8.3lf % 8.3lf % 5.2f % 2d\n",
		   ms_count,
		   doppler,
		   ps_delay,
		   CN_dBHz, 
       status.bit);
}
*/
//=====================================================================================
///  Print statistics string to specified file (for fix point PLL-DLL)
///
///  @param[in]  logFile   - pointer to the file structure
///  @param[in]  pGPSchan  - pointer to GPS channel structure
///  @param[in]  decoder   - pointer to decoder structure
///  @param[in]  ms_count  - system time in ms
//=====================================================================================
void printStatistics(FILE            *logFile,
                     const tGPSchan  *pGPSchan, 
                     const tDecoder  *decoder, 
                     uint32           ms_count)
{
   tGPSchanStatus status = *GPSchanGetStatus(pGPSchan);
   double doppler, doppler_aid ,delay,tauQ,CN_dBHz, phase, dopplerPrime, dopplerPrime_aid;
   double desc, delta_tau, phase_error;

   if (status.state!=es_TRACKING_STARTUP && status.state!=es_TRACKING)
   {
     memset(&status,0,sizeof(status));
   }
   doppler = ldexp(status.dopplerQ15,-15);
   dopplerPrime = ldexp(status.dopplerPrimeQ10, -10);
   delay   = LongFract48GetDouble(&status.delay) / 1000.;
   tauQ    = LongFract48GetDouble(&status.tauQ) / 1000.;
   CN_dBHz = ldexp(status.CN_dBHzQ7,-7);
//   desc = ldexp(pGPSchan->pllDll.status.tau_measuredQ31, -31) / 1000.;
   phase_error = ldexp(pGPSchan->pllDll.status.phase_err,-14)*(180/M_PI);
/*   
   fprintf(logFile, "% 6d  %2d  %2d  %2d  %2d  %d  %d  %d\n",
           ms_count,
           status.epochCounter,
           pGPSchan->epochInBit,
           status.Nframe_add,
           decoder->Nframe,
           decoder->PreambleTime,
           decoder->Synchronized,
           status.bit); 
*/
   fprintf(logFile,
         "% 6d % 15.3lf % 15.3lf % 15.3lf % 15.3lf %8d % 5.2f %10.5lf %5.2lf\n",
       ms_count,//status.epochCounter,
       doppler,
       dopplerPrime,
       delay * 2.99742e8,
       tauQ  * 2.99742e8,
       pGPSchan->position,
       CN_dBHz, desc * 2.99742e8, phase_error); 

   printf(
         "% 6d % 8.3lf % 8.3lf % 5.2f % 2d\n",
       status.epochCounter,
       doppler,
       delay * 2.99742e8,
       CN_dBHz, 
       status.bit);
}

//=====================================================================================
///  Print raw data to specified file (for fix point PLL-DLL)
///
///  @param[in]  logFile          - pointer to the file structure
///  @param[in]  raw_data_fix     - pointer to raw data structure
///  @param[in]  dopplerPrimeQ10  - doppler prime in Q10
///  @param[in]  ms_count         - system time in ms
//=====================================================================================
void printRaw(FILE          *logFile, 
              tRawDataFix   *raw_data_fix, 
              int32          dopplerPrimeQ10,
              int32          dopplerQ15,
              int32          CN_dBHzQ7,
              tLongFract48   delay,
              tLongFract48   tauQ,
              uint32         ms_count)
{
  double ps_delay = LongFractGetDouble(&raw_data_fix->ps_delay);
  int32 del = (int32)ps_delay;
  double CN_dBHz;
  ps_delay -= del;
  CN_dBHz = ldexp(CN_dBHzQ7,-7);

//  fprintf(logFile, "%d %15.5lf %11.3lf %10.4lf %10.4lf %10.4lf %5.2f %15.6lf %15.5lf %15.5lf %15.5lf\n", ms_count, ((LongFractGetDouble(&raw_data_fix->ps_delay)/1000.)*cLight), 
  fprintf(logFile, "%d %15.5lf %11.3lf %10.4lf %10.4lf %10.4lf %5.2f %15.5lf\n", ms_count, ((LongFractGetDouble(&raw_data_fix->ps_delay)/1000.)*cLight), 
    (ps_delay*1.0e-3*cLight), (-LongFractGetDouble(&raw_data_fix->ps_freq)*cLight/f0_GPS), (ldexp(dopplerPrimeQ10, -10)*cLight/f0_GPS),
    (ldexp(dopplerQ15, -15)*cLight/f0_GPS),
     CN_dBHz, (LongFract48GetDouble(&delay)*cLight/1000.)); 
//     (LongFractGetDouble(&raw_data_fix->dll_state)*cLight/1000.),
//     (LongFractGetDouble(&tauQ)*cLight/1000.));
  printf("%d %15.5lf %11.3lf %10.4lf\n", ms_count, ((LongFractGetDouble(&raw_data_fix->ps_delay)/1000.)*cLight), 
    (ps_delay*1.0e-3*cLight), (-LongFractGetDouble(&raw_data_fix->ps_freq)*cLight/f0_GPS));
}

#else  // floating point PLL-DLL version

//=====================================================================================
///  Print raw data to specified file (for fix point PLL-DLL)
///
///  @param[in]  logFile          - pointer to the file structure
///  @param[in]  raw_data_fix     - pointer to raw data structure
///  @param[in]  dopplerPrimeQ10  - doppler prime in Q10
///  @param[in]  ms_count         - system time in ms
//=====================================================================================
void printRaw(FILE          *logFile, 
              tRawDataFix   *raw_data_fix, 
              int32          dopplerPrimeQ10,
              int32          dopplerQ15,
              int32          CN_dBHzQ7,
              uint32         ms_count)
{
  double ps_delay = LongFractGetDouble(&raw_data_fix->ps_delay);
  int32 del = (int32)ps_delay;
  double CN_dBHz;
  ps_delay -= del;
  CN_dBHz = ldexp(CN_dBHzQ7,-7);

  fprintf(logFile, "%d %15.5lf %11.3lf %10.4lf %10.4lf %10.4lf %5.2f %15.6lf\n", ms_count, ((LongFractGetDouble(&raw_data_fix->ps_delay)/1000.)*cLight), 
    (ps_delay*1.0e-3*cLight), (-LongFractGetDouble(&raw_data_fix->ps_freq)*cLight/f0_GPS), (ldexp(dopplerPrimeQ10, -10)*cLight/f0_GPS),
    (ldexp(dopplerQ15, -15)*cLight/f0_GPS),
     CN_dBHz, LongFractGetDouble(&raw_data_fix->dt));
  printf("%d %15.5lf %11.3lf %10.4lf %5.2f  \n", ms_count, ((LongFractGetDouble(&raw_data_fix->ps_delay)/1000.)*cLight), 
    (ps_delay*1.0e-3*cLight), (-LongFractGetDouble(&raw_data_fix->ps_freq)*cLight/f0_GPS),
    CN_dBHz);
}

#endif  // PLLDLL_FIX

//=====================================================================================
///  Print DLL status string to specified file
///
///  @param[in]  logFile   - pointer to the file structure
///  @param[in]  pGPSchan  - pointer to GPS channel structure
///  @param[in]  decoder   - pointer to decoder structure
///  @param[in]  ms_count  - system time in ms
//=====================================================================================
int flag=0;
void printDll(FILE         *logFile, 
              tGPSchan     *pGPSchan, 
              tDecoder     *decoder, 
              const uint32  ms_count)
{
  tGPSchanStatus *status = &pGPSchan->status;
  double del = LongFract48GetDouble(&status->delay);
  double doppler = ldexp(status->dopplerQ15,-15);
  double dopplerPrime = ldexp(status->dopplerPrimeQ10,-10);
  float64 ps_phase;
  double dll_state, dt, tQ, ps_delay, ps_freq, dl;
  tLongFract48 tauQ = status->tauQ, delay = status->delay;

  // calculation fractional part delay
   {
      tLongFract48 diff1, diff2; 
      tLongFract del;
      LongFract48Sub(&delay, &tauQ, &diff1);
      tauQ.integral = 0;
      LongFract48Add(&diff1, &tauQ, &diff2);
      LongFractSetLongFract48(&diff2, &del);
      dll_state = LongFractGetDouble(&del);
   }

    if ( status->epoch_control != 0 )
    {
        if (tQ == 0)
        {
          flag = 1; 
        }
        else
        {
          flag = -1; 
        }
    }

    ps_delay = dll_state + flag;

    dt = dll_state + flag;

    // Pseudofrequency raw measurement forming
    ps_freq = doppler + dopplerPrime*dt*1.0e-3;

    // Pseudophase raw measurement forming
    ps_phase = CONV_PLL2DLL*(doppler + dopplerPrime*dt*1.0e-3*0.5)*(2*M_PI)*dt*1.0e-3; // sec
    
    // resulting pseudodelay calculation
    ps_delay = ps_delay*1.0e-3 - ps_phase; // sec

   fprintf(logFile, "% 8d % 15.5lf %8.3lf %8.3lf %15.5lf %8.3lf % 15.5lf %5.2lf\n",
           ms_count,
           dl * 2.99742e5,
           doppler,
           dopplerPrime,
           ps_delay * 2.99742e8,
           ps_freq,
           ps_phase * 2.99742e8,
           dt);
}

void printAllRaw(FILE *logFile, tSatelliteGPS *pSat, tDecoderCommon *common, uint32 ms_count)
{

  double ps_delay = LongFractGetDouble(&pSat->raw_data_fix.ps_delay);
  int32  del = (int32)ps_delay;
  double tauQ = LongFract48GetDouble(&pSat->gps_chan.status.tauQ);
  int32 tau = (int32)tauQ;

  tauQ -= tau;
  ps_delay -= del;
  fprintf(logFile, "%d, %15.5lf, %11.3lf, %11.3lf, %10.4lf, %d, %d, %d, %d, %d, %d %d\n", ms_count, 
    ((LongFractGetDouble(&pSat->raw_data_fix.ps_delay)/1000.)*cLight), (ps_delay*1.0e-3*cLight),
    (tauQ*1.e-3*cLight),
    (-LongFractGetDouble(&pSat->raw_data_fix.ps_freq)*cLight/f0_GPS), pSat->decoder.Synchronized,
    pSat->decoder.Nframe, pSat->gps_chan.status.Nframe_add, pSat->decoder.PreambleTime, 
    common->preamble[pSat->sv_num-1].PreambleTime, pSat->gps_chan.epochInBit, pSat->gps_chan.status.epoch_control);
}

void printGPMatrix(tOPManager *pOPMgr)
{
  int n,m;
  FILE *fGP;

  fGP = fopen( "GP.dat", "wt");

  for (n = 2; n<=18; n++)
    for (m = 0; m<=n; m++)
      fprintf(fGP, "\n %3d %3d:  %20.12e, %20.12e", n, m, pOPMgr->DM.GP.C[n][m], pOPMgr->DM.GP.S[n][m]);

   fclose(fGP);
}

#endif // !defined(USE_DSPBIOS)
