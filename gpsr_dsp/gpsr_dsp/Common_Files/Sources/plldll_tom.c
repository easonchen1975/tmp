/**************************************************************************************
 *    Copyright (c) 2008 Spirit Corp.
 *************************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  PLL & DLL loops
 *   @file                   PLLDLL.c
 *   @author                 D.Churikov, M. Zhokhova
 *   @date                   31.01.2008
 *   @version                1.0
 */
/*************************************************************************************/

#include "comtypes.h"
#include "PLLDLL.h"
#include "Exp.h"
#include "Div31Fast.h"
#include "baseop.h"
#include "Atan2.h"
#include "gps.h"
#include "expTbl.h"
#include "Comfuncs.h"
#include "L_sqrt_l.h"
#include "L_mpy_ll.h"

#define ENABLE_PLL 1
#define ENABLE_DLL 1

/// Filter coefficients according to Kaplan, 5.1.4. Band width = 18 Hz for 1 ms
#if !PLLDLL_FIX
#include "Control.h"
#define ALPHA       (0.05085936)
#define BETA        (1.08988416)
#define GAMMA2      (10.0776960)
#define FLT_0       (0.98)
#define FLT_1       (0.02)
#endif
#define ALPHAQ21    (0x424F)              ///< ALPHA/2pi in Q21 for time 1 ms
#define BETAQ17     (0x58CF)              ///< BETA/2pi in Q17 for time 1 ms
#define GAMMA2Q14   (0x66A6)              ///< GAMMA2/2pi in Q14 for time 1 ms

#define ALPHAQ19_5  (0x52E3)              ///< ALPHA*5/2pi in Q19 for time 5 ms 
#define BETAQ15_5   (0x6F03)              ///< BETA*5/2pi in Q15 for time 5 ms 
#define GAMMA2Q11_5 (0x4028)              ///< GAMMA2*5/2pi in Q11 for time 5 ms 

#define tauQ24      (0x4189)              ///< 0.001 
#define coefQ28     (0x68DB)              ///< 0.0001
#define ms5Q12      (0x5000)              ///< 5

#define K_BEGINQ7       (0x6666)          ///< K_BEGIN*0.001*2^15 in Q7 for time 1 ms
#define K_BEGINQ4_5     (0x4000)          ///< K_BEGIN*0.001*5*2^15 in Q4 for time 5 ms

#define K_DISCQ10       (0x472E)          ///< 32768.*1024./(1.8*1023)

#define MAX_INTEGRATION_PLL   (5)            ///< maximal integration time for PLL

#define tauQ40                (0x4189374BL)  ///< tau in Q40   == ldexp(0.001,40)
#define tauQ40_fract          (0xC6A7EF9DL)  ///< fractional part of tau in Q40 == 2^32*(0.001*2^40 - ldexp(0.001,40))
#define tau_2PIQ38            (0x66F196DAL)  ///< tau*2PI in Q38   == ldexp((0.001*2*pi),38)

#define CONV_PLL2DLL_Q62      (0x573D540CL)  ///< CONV_PLL2DLL*0.5ms*2*pi in Q62  == ldexp(CONV_PLL2DLL*2pi,62)
#define CONV_PLL2DLL_Q61      (0x573D540CL)  ///< CONV_PLL2DLL*1ms*2*pi in Q61  == ldexp(CONV_PLL2DLL*2pi,61)
#define CONV_PLL2DLL_Q59_5    (0x6D0CA90FL)  ///< CONV_PLL2DLL*5ms*2*pi in Q59  == ldexp(CONV_PLL2DLL*2pi,59)
#define CONV_PHASE2DELAY_Q71  (0x59555404L)  ///< (1/f0_GPS)/1000 in Q71
#define CONV_PHASE2DELAY_Q51  (0x5531E413L)  ///< 1000/(f0_GPS) in Q51
#define COEF_PHASE_FLT_0_Q15  (0x7D70L)      ///< 0.98 in Q15
#define COEF_PHASE_FLT_1_Q20  (0x51EBL)      ///< 0.02 in Q20

//=====================================================================================
///  Function calculate modulo (2*pi) number
//
///  @param[in]  x  - input number
///  @return     x mod (2*pi)
//=====================================================================================
#if !PLLDLL_FIX
#include <math.h>
static double mod2PI(double x)
{
    int N = (int)floor(x/(2*M_PI));
    return x-N*2*M_PI;
}
#endif

//=====================================================================================
///  PLL/DLL coefficients initialization function
//
///  @param[out] coefPllDll - PLL/DLL coefficients structure
///  @param[in]  coefDLL    - DLL filter coefficient
///  @param[in]  timeDLL    - DLL integration time (1,2,4,5,10,20)
//=====================================================================================
void PLLDLLSetDllCoef(tCoefPLLDLL *coefPllDll, int32 coefDLL, int8 timeDLL)
{
  /// PLL and DLL predetection integration time
  if ((timeDLL != 1) && (timeDLL != 2)  && (timeDLL != 4) && 
      (timeDLL != 5) && (timeDLL != 10) && (timeDLL != 20))
  {
    return;
  }
  /// for PLL maximum integration time is equal 5
  if (timeDLL > MAX_INTEGRATION_PLL)
    coefPllDll->update_pll = MAX_INTEGRATION_PLL;
  else
    coefPllDll->update_pll = timeDLL;
  /// save integration time for DLL
  coefPllDll->update_dll = timeDLL;
   
  /// PLL Kalman filter coefficients: alpha, beta, gamma2: multiply to integration time
  if ((coefPllDll->update_pll == 1) || (coefPllDll->update_pll == 2) || (coefPllDll->update_pll == 4))
  {
    coefPllDll->alpha_val  = ALPHAQ21;
    coefPllDll->alpha_shr  = 21 - (coefPllDll->update_pll>>1);
    coefPllDll->beta_val   = BETAQ17;
    coefPllDll->beta_shr   = 17 - (coefPllDll->update_pll>>1);
    coefPllDll->gamma2_val = GAMMA2Q14;
    coefPllDll->gamma2_shr = 14 - (coefPllDll->update_pll>>1);
  }
  if (coefPllDll->update_pll == 5)
  {
    coefPllDll->alpha_val  = ALPHAQ19_5;
    coefPllDll->alpha_shr  = 19;
    coefPllDll->beta_val   = BETAQ15_5;
    coefPllDll->beta_shr   = 15;
    coefPllDll->gamma2_val = GAMMA2Q11_5;
    coefPllDll->gamma2_shr = 11;
  }
  /// DLL filter coefficient which depends on filter bandwidth
  {
    int32 k_end;  ///< ending coefficient
    int16 val,    ///< value of fix-point ending coefficient
          shr;    ///< shift of fix-point ending coefficient

    k_end = L_mpy_ls(coefDLL, coefQ28);  /// Q0*Q28 -> Q13 (0.0001)
    if ((coefPllDll->update_dll == 1) || (coefPllDll->update_dll == 2) || (coefPllDll->update_dll == 4))
    {
      k_end = L_mpy_ls(k_end, tauQ24)<<(9 + (coefPllDll->update_dll>>1));  /// Q13*Q24 -> Q22 -> (Q15*Q16 -> Q31) (K_END * 0.001 * 32768 * 65536)
      coefPllDll->k_begin_val = K_BEGINQ7;
      coefPllDll->k_begin_shr = 7 - (coefPllDll->update_dll>>1);
    }
    if ((coefPllDll->update_dll == 5) || (coefPllDll->update_dll == 10) || (coefPllDll->update_dll == 20))
    {
      k_end = L_mpy_ls(k_end, tauQ24)<<(9 + (coefPllDll->update_dll>>3));  /// Q13*Q24 -> Q22 -> (Q15*Q16 -> Q31) (K_END * 0.001 * 32768 * 65536)
      k_end = L_mpy_ls(k_end, ms5Q12)<<3;  /// Q31*Q12 -> Q28 -> Q31
      coefPllDll->k_begin_val = K_BEGINQ4_5;
      coefPllDll->k_begin_shr = 4 - (coefPllDll->update_dll>>3);
    }
    shr = Exp0(k_end);
    val = (int16)Shr(k_end, 16-shr);
    coefPllDll->k_end_val = val;
    coefPllDll->k_end_shr = shr;
  }
  /// coefficient for Doppler
  {
    if ((coefPllDll->update_dll == 1) || (coefPllDll->update_dll == 2) || (coefPllDll->update_dll == 4))
    {
      /// dt = coefPllDll->update_dll/2
      coefPllDll->k_dop_val = CONV_PLL2DLL_Q62;
      coefPllDll->k_dop_shr = 62 - (coefPllDll->update_dll>>1);
    }
    if ((coefPllDll->update_dll == 5) || (coefPllDll->update_dll == 10) || (coefPllDll->update_dll == 20))
    {
      coefPllDll->k_dop_val = CONV_PLL2DLL_Q59_5;
      coefPllDll->k_dop_shr = 60 - (coefPllDll->update_dll>>3);
    }
  }
}

//=====================================================================================
///  Initialize PLL-DLL (fix point version)
//
///  @param[in]  pPLLDLL     - pointer to instance
///  @param[in]  pPrompt     - pointer to prompt correlations buffer 
///  @param[in]  pEarly      - pointer to early correlations buffer
///  @param[in]  pLate       - pointer to late correlations buffer
///  @param[in]  tau         - initial delay
///  @param[in]  dopplerQ15  - initial Doppler frequency in Q15
//=====================================================================================
#if PLLDLL_FIX
void PLLDLLInit (tPLLDLL           *pPLLDLL,
                 const tComplex32  *pPrompt,
                 const tComplex32  *pEarly,
                 const tComplex32  *pLate,
                 tLongFract48       tau,
                 int32              dopplerQ15)
#else
//=====================================================================================
///  Initialize PLL-DLL (floating point version)
//
///  @param[in]  pPLLDLL     - pointer to instance
///  @param[in]  pPrompt     - pointer to prompt correlations buffer 
///  @param[in]  pEarly      - pointer to early correlations buffer
///  @param[in]  pLate       - pointer to late correlations buffer
///  @param[in]  tau         - initial delay
///  @param[in]  doppler     - initial Doppler frequency
//=====================================================================================
void PLLDLLInit (tPLLDLL           *pPLLDLL,
                 const tComplex32  *pPrompt,
                 const tComplex32  *pEarly,
                 const tComplex32  *pLate,
                 double             tau, 
                 double             doppler)
#endif
{
  #if PLLDLL_FIX
    pPLLDLL->status.phaseQ32 = 0;             /// absolute phase (0...2pi) -> (0...0xFFFFFFFF)
    pPLLDLL->status.dopplerQ15 = dopplerQ15;  /// doppler , Q15 (-65...+65 kHz)
    pPLLDLL->status.dopplerPrimeQ10 =0 ;      /// Hz/sec (+- 1 kHz/sec)
  #else
    pPLLDLL->status.phase = 0;
    pPLLDLL->status.doppler = doppler;
    pPLLDLL->status.dopplerPrime = 0;
  #endif
  pPLLDLL->status.tau       = tau;
  pPLLDLL->status.phase_err = 0;
  pPLLDLL->accPrompt.re = 0; pPLLDLL->accPrompt.im = 0;
  pPLLDLL->accEarly.re = 0;  pPLLDLL->accEarly.im = 0; 
  pPLLDLL->accLate.re = 0;   pPLLDLL->accLate.im = 0;
  /// PLL output
  pPLLDLL->status.y.re = 0;  pPLLDLL->status.y.im = 0;
}


//=====================================================================================
/// Interface PLL-DLL operation function
//
///  @param[in]  pPLLDLL       - pointer to instance
///  @param[in]  pPrompt       - pointer to prompt correlations buffer 
///  @param[in]  pEarly        - pointer to early correlations buffer
///  @param[in]  pLate         - pointer to late correlations buffer
///  @param[in]  tauQ          - quantized delay
///  @param[in]  epochCounter  - epoch counter
///  @param[in]  epochCntr     - epoch in bit counter (0...19)
///  @param[in]  coefPllDll    - PLL/DLL coefficients structure
///  @return     pointer to PLL & DLL status structure
//=====================================================================================
#if PLLDLL_FIX
const tPLLDLLStatus *PLLDLLProcess(tPLLDLL           *pPLLDLL,
                                   const tComplex32  *pPrompt,
                                   const tComplex32  *pEarly,
                                   const tComplex32  *pLate,
                                   tLongFract48       tauQ,
                                   int32              epochCounter,
                                   int                epochCntr,
                                   const tCoefPLLDLL  coefPllDll)
#else
const tPLLDLLStatus *PLLDLLProcess (tPLLDLL           *pPLLDLL,
                                    const tComplex32  *pPrompt,
                                    const tComplex32  *pEarly,
                                    const tComplex32  *pLate,
                                    double             tauQ,
                                    int32              epochCounter,
                                    int                epochCntr,
                                    const tCoefPLLDLL  coefPllDll)
#endif
{
  #if !PLLDLL_FIX
    const double tau = 0.001;
  #endif
  {
    /// accumulation correlations
    {
      pPLLDLL->accPrompt.re += pPrompt->re;
      pPLLDLL->accPrompt.im += pPrompt->im;
      pPLLDLL->accEarly.re  += pEarly->re;
      pPLLDLL->accEarly.im  += pEarly->im;
      pPLLDLL->accLate.re   += pLate->re;
      pPLLDLL->accLate.im   += pLate->im;
      pPLLDLL->status.y.re   = pPrompt->re; 
      pPLLDLL->status.y.im   = pPrompt->im;
    }
    /// extrapolation
    {
      #if PLLDLL_FIX
      {
        uint32       extr_pll_state0 = pPLLDLL->status.phaseQ32;         ///< temporary phase in Q32, [rad]
        int32        extr_pll_state1 = pPLLDLL->status.dopplerQ15,       ///< temporary doppler in Q15, [Hz/s]
                     extr_pll_state2 = pPLLDLL->status.dopplerPrimeQ10;  ///< temporary doppler prime in Q10, [Hz/s/s]
        tLongFract48 extr_tmp48;                                         ///< temporary extrapolation delay
        tLongFract   tau_extr,                                           ///< extrapolation time
                     extr_delay,                                         ///< extrapolation delay
                     extr_tmp,                                           ///< temporary extrapolation value
                     dt,                                                 ///< correction of interpolation interval
                     extr_phase;                                         ///< extrapolation phase
     
        /// correction of extrapolation interval: tau_extr = 0.001s -  extr_pll_state1*0.001s/f0_GPS;
        /// 1 ms
        tau_extr.integral = tauQ40;        // integer part of 0.001s in Q40
        tau_extr.fract    = tauQ40_fract;  // fractional part of 0.001s in Q40
        /// ((d + fa)/f0_GPS)*0.001s -> convert to s
        LongFractMul32(extr_pll_state1, CONV_PHASE2DELAY_Q71, &dt);  // Q15+Q71 - Q32 -> Q54
        LongFractShr(&dt, 14);                                       // Q54 -> Q40
        LongFractSub(&tau_extr, &dt, &tau_extr);                     // in Q40, [sec]
        LongFractShr(&tau_extr, 8);                                  // Q40 -> Q32

        /// doppler prime
        pPLLDLL->status.dopplerPrimeQ10 = extr_pll_state2;

        /// doppler = doppler_current + doppler_prime*tau_extr
        LongFractMPY(&tau_extr, extr_pll_state2, &extr_tmp);      // Q32+Q10 -> Q26
        pPLLDLL->status.dopplerQ15 += (extr_tmp.integral >> 11);  // Q26 -> Q15
      
        // phase, delay, integrated doppler increment
        LongFractShr(&tau_extr, 16);                              // Q32 -> Q16
        LongFractSquare(&tau_extr, &extr_tmp);                    // Q16+Q16 -> Q32 => (tau1*tau1/2) in Q33
        LongFractMPY(&extr_tmp, extr_pll_state2, &extr_tmp);      // Q33+Q10 -> Q27, [cycle]
        LongFractShr(&extr_tmp, 12);                              // Q27 -> Q15 [cycle]

        LongFractMPY(&tau_extr, extr_pll_state1, &extr_delay);    // Q16+Q15 -> Q15;
        LongFractAdd(&extr_delay, &extr_tmp, &extr_delay);        // extr_delay in Q15, [cycle]
        LongFractShr(&extr_delay, 5);                             // Q15 -> Q10

        extr_phase = extr_delay;
        LongFractShr(&extr_phase, 10);                            // Q10 -> Q0

        // phase = extr_pll_state0 + extr_pll_state1 * tau_extr + (tau_extr*tau_extr/2) * extr_pll_state2
        pPLLDLL->status.phaseQ32 = extr_pll_state0 + extr_phase.fract;

         // delay -= 1000/f0_GPS(extr_pll_state1*tau_extr + extr_pll_state2*tau_extr*tau_extr*0.5) 
        LongFractMPY(&extr_delay, CONV_PHASE2DELAY_Q51, &extr_delay);  // Q10+Q51 -> Q45
        LongFractShr(&extr_delay, 29);                                 // Q45     -> Q16
        LongFract48SetLongFractQ48(&extr_delay, &extr_tmp48);
        LongFract48Sub(&pPLLDLL->status.tau, &extr_tmp48, &pPLLDLL->status.tau);
      }
      #else
      {
        double dt, tau_extr;

        double  extr_pll_state0 = pPLLDLL->status.phase;          // [rad]
        double  extr_pll_state1 = pPLLDLL->status.doppler;        // [rad/s]
        double  extr_pll_state2 = pPLLDLL->status.dopplerPrime;   // [rad/s/s]

        // correction of extrapolation interval
        dt = extr_pll_state1*CONV_PLL2DLL*tau;
        tau_extr = tau - dt;

        // delay
        pPLLDLL->status.tau -= CONV_PLL2DLL*(extr_pll_state1*tau_extr + extr_pll_state2*tau_extr*tau_extr*0.5);

        // phase & doppler & derivative of doppler
        pPLLDLL->status.phase = mod2PI(extr_pll_state0 + extr_pll_state1 * tau_extr + 
                                      (tau_extr*tau_extr/2) * extr_pll_state2);
        pPLLDLL->status.doppler      = extr_pll_state1 + extr_pll_state2* tau_extr;
        pPLLDLL->status.dopplerPrime = extr_pll_state2;
      }
      #endif
    }
    /// calculation error
    if ((epochCntr+1) % coefPllDll.update_pll == 0)
    {
      #if PLLDLL_FIX
      {
        int16 err_pll;  /// PLL error
        /// atan2
        {
          tComplex32 acc;
          acc.re = pPLLDLL->accPrompt.re;
          acc.im = pPLLDLL->accPrompt.im;
          if (acc.re == 0) err_pll = 0;
          else
          {
            tComplex16 Vector;
            int16 exp;  /// normalize and calculate atan()
            exp = Min(Exp0(acc.re),Exp0(acc.im));
            acc.re <<= exp; 
            acc.im <<= exp;
            Vector.re = extract_h(acc.re<<exp);
            Vector.im = extract_h(acc.im<<exp);
            /// move to the right semi plane
            if (acc.re<0) 
            {
              /// it shooed be done with saturation to avoid cases when acc.re/im=0x80000000 !!!
              acc.re = L_sub(0,acc.re); 
              acc.im = L_sub(0,acc.im);
            }
            Vector.re = extract_h(acc.re);
            Vector.im = extract_h(acc.im);
            err_pll   = ATan2_fxp(Vector);  // in Q14
            }
          #if !ENABLE_PLL
            err_pll = 0;
          #endif          
        }
        /// update state PLL
        {
          int32        d_phase;
          tLongFract   d_tau, delta_tau32;
          tLongFract48 d_tau48, delta_tau48;

          LongFract48Sub(&pPLLDLL->status.tau, &tauQ, &delta_tau48);  /// ms
          LongFractSetLongFract48(&delta_tau48, &delta_tau32);
          LongFractMPY(&delta_tau32, tau_2PIQ38, &delta_tau32);  /// ms->s Q0*Q38 -> Q22
          LongFractMPY(&delta_tau32, pPLLDLL->status.dopplerQ15, &delta_tau32);  /// Q22*Q15 -> Q21
          LongFractShr(&delta_tau32, 7);  /// Q21 -> Q14

          err_pll += (int16)delta_tau32.integral;

          /// Q14 + Qx + Q1 - Qshift -> Q32 => Qshift = Qx + Q14 + Q1 - Q32 => Qshift = Qx - Q17
          d_phase = L_mult(err_pll, coefPllDll.alpha_val)  >> (coefPllDll.alpha_shr - 17);
          pPLLDLL->status.phaseQ32        += d_phase;
          /// Q14 + Qx + Q1 - Qshift -> Q15 => Qshift = Qx + Q14 + Q1 - Q15 => Qshift = Qx
          pPLLDLL->status.dopplerQ15      += L_mult(err_pll, coefPllDll.beta_val)   >> (coefPllDll.beta_shr);  
          /// Q14 + Qx + Q1 - Qshift -> Q10 => Qshift = Qx + Q14 + Q1 - Q10 => Qshift = Qx + Q5
          pPLLDLL->status.dopplerPrimeQ10 += L_mult(err_pll, coefPllDll.gamma2_val) >> (coefPllDll.gamma2_shr + 5);  
          pPLLDLL->status.phase_err        = err_pll;
          /// Q14*Q15 -> Q30 -> Q14
          pPLLDLL->status.phase_err_avr    = (L_mult(pPLLDLL->status.phase_err_avr, COEF_PHASE_FLT_0_Q15) >> 16);
          /// Q14*Q20 -> Q35 -> filtered phase error in Q14
          pPLLDLL->status.phase_err_avr   += (L_mult(err_pll, COEF_PHASE_FLT_1_Q20) >> 21);

          /// tau correction: pPLLDLL->status.tau -= (phase_err*ALPHA)/(M_2PI*f0_GPS)
          d_tau.integral = d_phase;                            // in Q32
          d_tau.fract    = 0;
          LongFractShr(&d_tau, 16);                            // Q32 -> Q16
          LongFractMPY(&d_tau, CONV_PHASE2DELAY_Q51, &d_tau);  // Q16*Q51 -> Q51
          LongFractShr(&d_tau, 31);                            // Q61 -> Q20
          LongFractShr(&d_tau, 4);                             // Q20 -> Q16
          LongFract48SetLongFractQ48(&d_tau, &d_tau48);         
          LongFract48Sub(&pPLLDLL->status.tau, &d_tau48, &pPLLDLL->status.tau);
        }
      }
      #else
      {
        tComplex32 acc;
        double err_pll;
        acc.re = pPLLDLL->accPrompt.re;
        acc.im = pPLLDLL->accPrompt.im;
        if (acc.re==0) err_pll = 0;
        else err_pll = atan(acc.im/(double)acc.re);

        err_pll += (pPLLDLL->status.tau - tauQ) * pPLLDLL->status.doppler;

        #if !ENABLE_PLL
          err_pll = 0;
        #endif
        pPLLDLL->status.phase         = mod2PI(pPLLDLL->status.phase + err_pll * ALPHA);
        pPLLDLL->status.doppler      += err_pll * BETA;
        pPLLDLL->status.dopplerPrime += err_pll * GAMMA2;
        pPLLDLL->status.phase_err     = err_pll;
        pPLLDLL->status.phase_err_avr = FLT_0*pPLLDLL->status.phase_err_avr + FLT_1*err_pll;

        // tau correction
        pPLLDLL->status.tau          -= err_pll * ALPHA/(M_2PI*f0_GPS);
      }
      #endif
      pPLLDLL->accPrompt.re = 0; pPLLDLL->accPrompt.im = 0;
    }  /// if ((epochCntr+1) % coefPllDll.update_pll == 0)
    /// error DLL
    if ((epochCntr+1) % coefPllDll.update_dll == 0)
    {
      #if PLLDLL_FIX
      {
        int32 tau_measuredQ31,  ///< discriminator value
              tau_extrQ31,      ///< extrapolation value
              err_dllQ31,       ///< error DLL
              Acc;              ///< accumulator, divisor
        /// discriminator
        {
          tComplex16 Acce,     ///< early
                     Accl;     ///< late
          int16      expquot,  ///< exponent of dividend
                     exp;      ///< exponent of divisor
          int32      quot,     ///< dividend
                     Acc1,     ///< accumulator of early
                     Acc2;     ///< accumulator of late
          /// for shift correlation
          exp     = Min(Min(Min(Exp0(pPLLDLL->accEarly.re), Exp0(pPLLDLL->accEarly.im)),
                                Exp0(pPLLDLL->accLate.re)), Exp0(pPLLDLL->accLate.im));
          exp    -= 1;
          /// normalize
          Acce.re = extract_h(L_shl(pPLLDLL->accEarly.re, exp));
          Acce.im = extract_h(L_shl(pPLLDLL->accEarly.im, exp));
          Accl.re = extract_h(L_shl(pPLLDLL->accLate.re, exp));
          Accl.im = extract_h(L_shl(pPLLDLL->accLate.im, exp));
          /// sqrt(early_re*early_re + early_im*early_im)
          Acc1    = L_sqrt_l(L_add(mpy(Acce.re,Acce.re),mpy(Acce.im,Acce.im)));
          /// sqrt(late_re*late_re + late_im*late_im)
          Acc2    = L_sqrt_l(L_add(mpy(Accl.re,Accl.re),mpy(Accl.im,Accl.im)));
          /// divisor = sqrt(early_re*early_re + early_im*early_im) + sqrt(late_re*late_re + late_im*late_im)
          Acc     = L_add(Acc1,Acc2);
          if (Acc==0) tau_measuredQ31 = 0;
          else
          {
            /// dividend = sqrt(early_re*early_re + early_im*early_im) - sqrt(late_re*late_re + late_im*late_im)
            quot             = L_sub(Acc1,Acc2);
            /// normalize
            expquot          = Exp0(quot);
            exp              = Exp0(Acc);
            /// calculate discriminator value
            Acc              = Div31Fast(quot<<(expquot-1), Acc<<exp);   /// Q(31+expquot-1-exp)
            /// multiply by coefficient (-1/(1023*k))
            Acc              = L_mpy_ls(Acc, (int16)(-K_DISCQ10));       /// Q(31+expquot-1-exp + 10)
            tau_measuredQ31  = L_shl(Acc,(int16)-(expquot-1-exp + 10));  /// Q31 
          }
        }
        #if ENABLE_DLL
        {
          tLongFract48 tau,      ///< temporary delay
                       err_tau;  ///< error of delay
          int32        Dopp;     ///< error of discriminator by doppler to integration time

          /// copy delay
          tau.integral = pPLLDLL->status.tau.integral;
          tau.fract_0  = pPLLDLL->status.tau.fract_0;
          tau.fract_1  = pPLLDLL->status.tau.fract_1;
          /// error of discriminator = (d + fa)*(dt/2) and convert to ms, where dt = integration time
          /// Q15 + Qx - Q31 - Qshift -> Q31 => Qshift = Q15 + Qx - Q31 - Q31 => Qshift = Qx - Q47
          Dopp             = L_mpy_ll(pPLLDLL->status.dopplerQ15, coefPllDll.k_dop_val) >> (coefPllDll.k_dop_shr - 47); 
          tau_measuredQ31 -= Dopp;

          /// extrapolation
          {
            tLongFract48   delta_tau;  ///< delta delay

            LongFract48Sub(&pPLLDLL->status.tau, &tauQ, &delta_tau);
            tau_extrQ31 = LongFract48GetQ31(&delta_tau);            
          }
          /// error
          Acc = tau_measuredQ31 - tau_extrQ31;
          /// change K
          if(epochCounter < DLL_TIME)
          {
            err_dllQ31 = L_mpy_ls(Acc, coefPllDll.k_begin_val) >> coefPllDll.k_begin_shr;
          }
          else
          {
            err_dllQ31 = L_mpy_ls(Acc, coefPllDll.k_end_val) >> coefPllDll.k_end_shr;
          }
          /// update DLL state
          Acc = err_dllQ31;
          LongFract48SetQ31(Acc, &err_tau);
          LongFract48Add(&tau, &err_tau, &pPLLDLL->status.tau);
        }
        #endif  /// ENABLE_DLL
      }
      #else  /// PLLDLL_FIX
      {
        double   err_dll, tau_measured, tau_extr;
        double   Acc, doppler;
        tComplex Ze, Zl;

        Ze.re = pPLLDLL->accEarly.re;
        Ze.im = pPLLDLL->accEarly.im;
        Zl.re = pPLLDLL->accLate.re;
        Zl.im = pPLLDLL->accLate.im;
        if (fabs(sqrt(Ze.re*Ze.re+Ze.im*Ze.im)+sqrt(Zl.re*Zl.re+Zl.im*Zl.im))<1e-10)
          tau_measured = 0;
        else
          tau_measured = -1.0/(1.8*1023) * (sqrt(Ze.re*Ze.re+Ze.im*Ze.im)-sqrt(Zl.re*Zl.re+Zl.im*Zl.im))
          /(sqrt(Ze.re*Ze.re+Ze.im*Ze.im)+sqrt(Zl.re*Zl.re+Zl.im*Zl.im));
        tau_measured /= 1000.;

        doppler       = pPLLDLL->status.doppler*CONV_PLL2DLL*(coefPllDll.update_dll * tau/2.0); 
        tau_measured -= doppler;

        tau_extr = pPLLDLL->status.tau-tauQ;
        Acc = tau_measured - tau_extr;
        #if ENABLE_DLL
        {
          if(epochCounter < DLL_TIME)
          {
            err_dll  = Acc * ldexp(coefPllDll.k_begin_val, -(15+coefPllDll.k_begin_shr)); // 
          }
          else
          {
            err_dll  = Acc * ldexp(coefPllDll.k_end_val, -(15+coefPllDll.k_end_shr));
          }
          /// update time by doppler correction
          pPLLDLL->status.tau += err_dll;
        }
        #endif
      }
      #endif       
      pPLLDLL->accEarly.re = 0;      pPLLDLL->accEarly.im = 0;
      pPLLDLL->accLate.re = 0;      pPLLDLL->accLate.im = 0;
    }
  }
  return &pPLLDLL->status;
}


