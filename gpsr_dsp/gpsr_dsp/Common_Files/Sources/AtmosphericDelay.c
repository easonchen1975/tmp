/*******************************************************************************
 *    Copyright (c) 2005-2008 Spirit Corp.
 ******************************************************************************/
/**
 *    @project                GPSR
 *    @brief                  Ionospheric & tropospheric delays calculation
 *    @file                   AtmosphericDelay.c
 *    @author                 D. Churikov
 *    @date                   29.04.2008
 *    @version                1.0
 */
/******************************************************************************/

#include "AtmosphericDelay.h"
#include "comtypes.h"
#include "control.h"
#include "satellite.h"

#include <math.h>


// -----------------------------------------------------------------------------
///  @brief		The function calculates ionospheric delay by Spacecraft ionosphere
///           model which used of SPIRENT GPS simulator family.
///  @param[in]	 TEC  - Tolal Electron Concentration (vertical) above user, [electrons/m^2]
///  @param[in]	 sinE - sinus of elevation of SV relating to user
///  @return     Calculated ionocpheric delay, [sec]
// -----------------------------------------------------------------------------
static float32 IonDelay_SimSpace(float32 TEC, float32 sinE)
{
  float32 tmp = sqrt(sinE*sinE + 0.076f) + sinE;
  return 82.1f * TEC * (float32)l_cLight / ((float32)f0_GPS*(float32)f0_GPS*tmp);
}

// -----------------------------------------------------------------------------
///  @brief		The function calculates ionospheric delay by Constant ionosphere
///           model of SPIRENT 7700.
///  @param[in]	  elev        - elevation of SV relating to user, [rad]
///  @param[in]	  TEC_ref_val - reference TEC value, [electrons/m^2]
///  @return	    Calculated ionospheric delay, [sec]
// -----------------------------------------------------------------------------
static float32 IonosphereDelay_ConstTEC( float32 elev, float32 TEC_ref_val)
{
  float32 d0, d1;    ///< temporary delay

  if (elev >= 0)
  {
    return IonDelay_SimSpace(TEC_ref_val, sin(elev));
  }
  else
  {
    d1 = IonDelay_SimSpace(TEC_ref_val, sin(-elev));
    d0 = IonDelay_SimSpace(TEC_ref_val, 0);
    return (d0+d0 - d1); // 2*d(0) - d(-elev)
  }
}


// -----------------------------------------------------------------------------
///  @brief		The function calculates ionospheric delay by Klobuchar ionosphere
///           model (see ICD-GPS-200C, par.20.3.3.5.2.6)
///  @param[in]	elev    - elevation of SV relating to user, [rad]
///  @param[in] azim    - azimuths of SV relating to user, [rad]
///  @param[in]	geo_Lat - geodetic latitude of user, [rad]
///  @param[in]	geo_Lon - geodetic longitude of user, [rad]
///  @param[in]	GPStime - receiver computed system time, [second from day]
///  @param[in]	IONpar  - pointer to ionosphere parameters
///  @return     Calculated ionospheric delay, [sec]
// -----------------------------------------------------------------------------
static float32 IonosphereDelay_Klobuchar( float32        elev,
                                          float32        azim,
                                          float32        geo_Lat,
                                          float32        geo_Lon,
                                          float64        GPStime,
                                          ION_UTC        *IONpar)
{
  float32 t_ION,///< calculated ionosphere correction
          E,    ///< elevation
          F,    ///< obliquity factor
          psi,  ///< Earth's central angle
          fi,   ///< geodetic latitude
          li,   ///< geodetic longitude
          fm,   ///< geomagnetic latitude
          tmp,  ///< temporary variable
          t,    ///< local time
          fm2,  ///< square fm
          fm3,  ///< multiply fm2 to fm
          AMP,  ///< AMP
          PER,  ///< PER
          X,    ///< phase
          x2,   ///< square X
          x4;   ///< multiply square X to square X

  t_ION = 0.f;

  /// satellite elevation
  E = elev * (float32)M_1_PI;
  
  /// Earth's central angle between the user position and the Earth projection of
  /// ionospheric intersection point [semi-cycles]
  psi = 0.0137f / (E + 0.11f) - 0.022f;

  /// geodetic latitude of the Earth projection on the ionospheric intersection
  /// point [semi-cycles]
  fi = geo_Lat * (float32)M_1_PI + psi * (float32)cos(azim);

  if(fi >  0.416f) 
    fi =  0.416f;
  if(fi < -0.416f) 
    fi = -0.416f;

  /// geodetic longitude of the Earth projection on the ionospheric intersection
  /// point [semi-cycles]
  li = geo_Lon * (float32)M_1_PI + psi * (float32)sin(azim) / (float32)cos(fi * M_PI);

  /// geomagnetic latitude of the Earth projection on the ionospheric intersection
  /// point [semi-cycles]
  fm = fi + 0.064f * (float32)cos((float32)M_PI * (li - 1.617f));

  /// obliquity factor [-]
  tmp = 0.53f - E;
  F = 1.0f + 16.0f * tmp * tmp * tmp;

  /// local time [sec]
  t = (float32)Fmodulo(43200.0f * li + GPStime, 86400.0f);

  /// compute AMP [sec] and PER [sec]
  fm2 = fm * fm;
  fm3 = fm2 * fm;
  AMP = IONpar->alpha0 + IONpar->alpha1 * fm + IONpar->alpha2 * fm2 + IONpar->alpha3 * fm3;

  if(AMP < 0.0f) 
    AMP = 0.0f;

  PER = IONpar->beta0  + IONpar->beta1 * fm  + IONpar->beta2 * fm2  + IONpar->beta3 * fm3;

  if(PER < 72000.0f) 
    PER = 72000.0f;

  /// phase [radians]
  X = (float32)M_2PI * (t - 50400.0f) / PER;

  /// ionospheric correction [sec]
  if((float32)fabs(X) < 1.57f)
  {
    x2 = X * X;
    x4 = x2 * x2;
    t_ION = F * (5e-9f + AMP * (1.0f - 0.5f * x2 + 0.0416666f * x4)); // 1/24 = 0.0416666
  }
  else
    t_ION = F * 5e-9f;

  return t_ION;
}


// -----------------------------------------------------------------------------
///  @brief		The function calculates troposphere delay by Standard troposphere
///           model.  The function describe troposphere delay accurately down to
///           an elevation of 2 deg. 
///  @param[in]	elev  - elevation of SV relating to user, [rad]
///  @param[in]	geo_H - geodetic height of user, [rad]
///  @return:    0 if geo_H > 80km
///              Calculated tropospheric delay, [sec]
// -----------------------------------------------------------------------------
static float32 TroposphereDelay_Standard( float32 elev, float32 geo_H)
{
  float32 t_TRO;

  // user height limit
  if (geo_H > 80000.f)  return 0.f;

  /// elevation limit
  elev = (elev > 1.7453293e-2f) ? elev : 1.7453293e-2f;   /// down to 1 deg.
  
  /// tropospheric correction [sec]  (2.4224/299792458. = 8.0802566e-9)
  t_TRO = 8.0802566e-9f * (float32)exp(-0.13346e-3f * (float32)geo_H) / (0.026f + (float32)sin(elev));

  return t_TRO;
}


//========================================================================
/// @brief      Calculation of atmospheric delay for SV's signal
///             (see ICD-GPS-200C, par.20.3.3.5.2.6)
///
/// @param[in]  NP      - pointer to satellite navigation parameters
/// @param[in]  User    - pointer to user parameters
/// @param[in]  IONpar  - pointer to ionosphere parameters
/// @param[in]  GPStime - receiver computed system time
/// @param[out] t_ION   - ionospheric delay correction
/// @param[out] t_TRO   - tropospheric delay correction
//========================================================================
void AtmosphereDelay( tNavTaskParams *NP,
                      USER           *User,
                      ION_UTC        *IONpar,
                      float64         GPStime,
                      int8           IonModel,
                      int8           TropoModel,
                      float32        *t_ION,
                      float32        *t_TRO)
{
  User->flag_bl = 1;    /// user parameters blocking
  IONpar->flag_bl = 1;  /// ION and UTC parameters blocking
  

  // Ionosphere delay calculation
  *t_ION = 0; // default - no ionosphere delay
  if (IonModel == 1)
  {// use Klobuchar model
    *t_ION = IonosphereDelay_Klobuchar(NP->sat_params.elev, NP->sat_params.azim,
                                       User->geo.Lat, User->geo.Lon, GPStime, IONpar);
  }
  else if (IonModel == 2)
  {// use Constant ionosphere model of SPIRENT 7700
    //*t_ION = IonosphereDelay_ConstTEC(NP->sat_params.elev, IONpar->TEC_ref_value);
    *t_ION = IonosphereDelay_ConstTEC(NP->sat_params.elev, 1e17); // temporary is set to default of SPIRENT
  }

  // Troposphere delay calculation
  *t_TRO = 0; // default - no troposphere delay
  if (TropoModel == 1)
  {// use standard model
    *t_TRO = TroposphereDelay_Standard(NP->sat_params.elev, User->geo.H);
  }
  

  return;
}

