/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  FIFO datatype definition
 *   @file                   FIFO.h
 *   @author                 M. Silchev
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*****************************************************************************/

#ifndef __FIFO_H
#define __FIFO_H

#include "comtypes.h"

typedef char tFifoItem;

/// FIFO structure
typedef struct tFIFO
{
  uint32      iHead;  ///< Current head (last put element)
  uint32      iTail;  ///< Current tail (oldest element to be get)
  uint32      nSize;  ///< FIFO size
  tFifoItem  *pData;  ///< Pointer to FIFO buffer
} tFIFO;

//====================================================================================
/// @brief     FIFO driver initialization
/// @param[in] pFIFO   - pointer to FIFO instance
/// @param[in] pBuffer - pointer to the buffer 
/// @param[in] Size    - FIFO buffer size
//====================================================================================
EXTERN_C void FIFO_Init (register tFIFO *pFIFO,tFifoItem *pBuffer,unsigned int Size);

// ========================================================================
/// @brief     FIFO driver starting
/// @param[in] pFIFO   - pointer to FIFO structure
// ========================================================================
EXTERN_C void FIFO_Start (register tFIFO *pFIFO);

//========================================================================
/// Get filled size
///
/// @param[in] pFIFO   - pointer to FIFO instance
//========================================================================
EXTERN_C int  FIFO_GetFilledSize (tFIFO *pFIFO);

//========================================================================
/// Get free size
///
/// @param[in] pFIFO   - pointer to FIFO instance
//========================================================================
EXTERN_C int  FIFO_GetFreeSize (tFIFO *pFIFO);

//=======================================================================================
/// @brief     Puts several data elements from FIFO
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the data buffer
/// @param[in] Count   - amount of data to put
//=======================================================================================
EXTERN_C void FIFO_PutBuffer (tFIFO *pFIFO,const tFifoItem *pBuffer,unsigned int Count);

//===============================================================================================
/// @brief     Checks free space amount & puts several data elements from FIFO
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the data buffer
/// @param[in] Count   - amount of data to put
//===============================================================================================
EXTERN_C bool FIFO_PutBufferVerified(tFIFO *pFIFO, const tFifoItem *pBuffer, unsigned int Count);

//===================================================================================
/// @brief     Gets several data elements from FIFO
/// @param[in] pFIFO   - pointer to FIFO structure
/// @param[in] pBuffer - pointer to the buffer which receives data
/// @param[in] Count   - amount of data to get
//===================================================================================
EXTERN_C void FIFO_GetBuffer (tFIFO *pFIFO,tFifoItem *pBuffer,unsigned int Count);

// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC

EXTERN_C void FIFO_CopyBuffer (register tFIFO *pFIFO,register tFifoItem *pBuffer,unsigned int Count) ;

#endif	// end NEW_ICD

#endif //__FIFO_H
