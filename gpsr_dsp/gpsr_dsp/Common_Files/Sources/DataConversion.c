/*****************************************************************************
*    Copyright (c) 2007 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  control functions
*    @file                   DataConversion.c
*    @author                 D.Churikov
*    @date                   25.05.2007
*    @version                1.0
******************************************************************************/

#include"Control.h"
#include"DataConversion.h"
#include"..\OP\SunMoonGravitation.h"
#include <math.h>


//========================================================================
/// @brief User ECEF coordinates transformation into geographic system
///
/// @param[out] geo  - pointer to tLLA object
/// @param[in]  TOPO - TOPO matrix
/// @param[in]  ecef - pointer to tECEF object
//========================================================================
void ECEF_to_Geoid(tLLA *geo, tECEF * ecef, float64 TOPO[3][3])
{ 
  float64 R,    ///< auxiliary variable
          tb,   ///< auxiliary variable
          tb0,  ///< auxiliary variable
          zr,   ///< auxiliary variable
          p1;   ///< auxiliary variable

  // Auxiliary variable
  R = sqrt(ecef->X * ecef->X + ecef->Y * ecef->Y);

  if(R < 1e-8)
  {
    geo->Lon = 0;

    if(ecef->Z == 0)
    {
      geo->Lat = 0;
      geo->H   = -a_Earth;
    }
    else
    {
      geo->H   = fabs(ecef->Z) - a_Earth * e2_Sqrt_Earth;
      geo->Lat = (ecef->Z < 0) ? -M_PI_2 : M_PI_2;
    }
    return;
  }

  geo->Lon = atan2(ecef->Y, ecef->X);
  if(geo->Lon < 0) 
    geo->Lon += M_2PI;
  
  if(ecef->Z == 0)
  { 
    geo->Lat = 0;
    geo->H   = R - a_Earth;
  }
  else
  { 
    p1 = a_e2_Sqrt_Earth * e2_Earth / R;
    tb = zr = ecef->Z / R;

    do
    {
      tb0 = tb;
      tb = zr + p1 * tb / sqrt(l_e2_Earth + tb * tb);
    }
    while(fabs(tb - tb0) > 1.0e-10);
    
    geo->H   = (R - a_e2_Sqrt_Earth / sqrt(l_e2_Earth + tb * tb)) * sqrt(1.0 + tb * tb);
    geo->Lat = atan(tb);
  }

  /// velocity component in north direction
  geo->Ndot    =  TOPO[0][0] * ecef->Xdot
                + TOPO[0][1] * ecef->Ydot
                + TOPO[0][2] * ecef->Zdot;
  
  /// velocity component in east direction
  geo->Edot    =  TOPO[1][0] * ecef->Xdot
                + TOPO[1][1] * ecef->Ydot;  /// User->TOPO[1][2] always equals zero
  
  /// velocity component in up direction
  geo->Udot  =  TOPO[2][0] * ecef->Xdot
              + TOPO[2][1] * ecef->Ydot
              + TOPO[2][2] * ecef->Zdot;


  return;
}


//========================================================================
/// @brief User geographic coordinates transformation into 
///        ECEF & TOPO matrix calculation
///
/// @param[out] ecef - pointer to tECEF object
/// @param[out] TOPO - TOPO matrix
/// @param[in]  geo  - pointer to tLLA object
//========================================================================
void Geoid_to_ECEF(tECEF * ecef, float64 TOPO[3][3], tLLA *geo)
{
  
  double sin_Lon,  ///< temporary sin
         cos_Lon,  ///< temporary cos
         sin_Lat,  ///< temporary sin
         cos_Lat,  ///< temporary cos
         R,        ///< temporary variable
         Rc;       ///< temporary variable
  
  sin_Lat = sin(geo->Lat);       cos_Lat = cos(geo->Lat);
  sin_Lon = sin(geo->Lon);       cos_Lon = cos(geo->Lon);
  
  R  = a_Earth/sqrt(1.0 - e2_Earth*sin_Lat*sin_Lat);
  Rc = (R + geo->H)*cos_Lat;

  /// ECEF coordinates calculation
  ecef->X = Rc*cos_Lon; 
  ecef->Y = Rc*sin_Lon;
  ecef->Z = (R*(1.0 - e2_Earth) + geo->H)*sin_Lat;
  
  /// TOPO calculation
  TOPO[0][0] = -sin_Lat*cos_Lon;  TOPO[0][1] = -sin_Lat*sin_Lon;  TOPO[0][2] = cos_Lat;  
  TOPO[1][0] = -sin_Lon;          TOPO[1][1] =  sin_Lon;          TOPO[1][2] = 0;  
  TOPO[2][0] =  cos_Lat*cos_Lon;  TOPO[2][1] =  cos_Lat*sin_Lon;  TOPO[2][2] = sin_Lat;  

  /// ECEF velocities calculation
  ecef->Xdot = TOPO[0][0]*geo->Ndot + TOPO[1][0]*geo->Edot + TOPO[2][0]*geo->Udot;
  ecef->Ydot = TOPO[0][1]*geo->Ndot + TOPO[1][1]*geo->Edot + TOPO[2][1]*geo->Udot;
  ecef->Zdot = TOPO[0][2]*geo->Ndot                        + TOPO[2][2]*geo->Udot;
  return;
}

/*

//========================================================================
/// @brief     User ECEF coordinates transformation into Keplerian Orbital Elements
///            
///
/// @param[out] ecef - pointer to tECEF object
/// @param[out] TOPO -  TOPO matrix
/// @param[in] geo - pointer to tLLA object
//========================================================================
void ECEF_to_OE(tOE orb, tECEF ecef, int16 wn, float64 t)
{  
	// conversion ECEF to ECI
  
  
  // conversion ECI to OE
  
  double r,V,p,u,test, d ,s;

	r = sqrt(X[0]*X[0] + X[1]*X[1] + X[2]*X[2]);
	V = sqrt(X[3]*X[3] + X[4]*X[4] + X[5]*X[5]);
	p = (Square(X[0]*X[4] - X[1]*X[3]) + Square(X[1]*X[5] - X[2]*X[4]) + 
		Square(X[0]*X[5] - X[2]*X[3])) / MU_EARTH;
	orb->a = MU_EARTH*r / (2*MU_EARTH - r*V*V);
	orb->e = sqrt(1 - p/orb->a);
	if ( fabs(d = (X[0]*X[4] - X[1]*X[3]) / sqrt(MU_EARTH*p)) <= 1)
	{
		orb->i = acos(d);
	}
	else 
		orb->i = 0;
	
	if ((orb->i) && ( fabs(d = ( -X[4]*X[2] + X[5]*X[1]) / (sin(orb->i) * sqrt(MU_EARTH*p))) <= 1))
	{
		orb->OMEGA = asin(d);
	}
	else 
		orb->OMEGA = 0;
	test = X[0]*X[5] - X[2]*X[3];
	if(test < 0)  orb->OMEGA = M_PI - orb->OMEGA;
	if(orb->OMEGA < 0)  orb->OMEGA += (2*M_PI);

	if ((cos (orb->i)) && ( fabs(d = (X[1]*cos(orb->OMEGA) - X[0]*sin(orb->OMEGA))/(r*cos(orb->i))) <= 1))
	{
		u = asin(d);
	}
	else
		u = 0;
	test = X[0]*cos(orb->OMEGA) + X[1]*sin(orb->OMEGA);
	if(test < 0)  u = M_PI - u;
	if(u < 0)  u += (2*M_PI);
	if (orb->e > 1e-6)		// ���������� 
	{
		if ( fabs(d = sqrt(p/MU_EARTH) * (X[0]*X[3] + X[1]*X[4] + X[2]*X[5]) / (orb->e*r)) <= 1)
		{
			orb->teta = asin(d);
		}
		else 
			orb->teta = 0;
		test = p - r;
		if(test < 0)  orb->teta = M_PI - orb->teta;
		if(orb->teta < 0)  orb->teta += (2*M_PI);
		orb->omega = u - orb->teta;
		if(orb->omega < 0)  orb->omega += (2*M_PI);
	}
	else
	{
		orb->e = 0;
		orb->teta = u;
		orb->omega = 0;
	}
	
	return orb;
}// ConvertToOE

// ����������� ������
float GetOrbitalPeriod(float a,float mu)
{
	return (float)2. * (float)M_PI * Square(a) / (float)sqrt(mu * a);
}


// ����������� ������
double GetOrbitalPeriod(double a,double mu)
{
	return 2. * M_PI * Square(a) / sqrt(mu * a);
}


// �������������� � ��������� �������� (x,y,z,Vx,Vy,Vz)
double *ConvertToXYZ(ORBITAL_1 *orb,double *X)
{ 
	double r,p,Vr,Vn,u;
	double cosu,sinu,cosOM,sinOM,cosi,sini;
	
	p = orb->a * (1. - orb->e*orb->e);
	r = p / (1. + orb->e*cos(orb->teta));
	Vr = sqrt(MU_EARTH/p) * orb->e * sin(orb->teta);
	Vn = sqrt(MU_EARTH/p) * (1. + orb->e*cos(orb->teta));
	u = orb->omega + orb->teta;
	cosu = cos(u);    cosOM = cos(orb->OMEGA);     cosi = cos(orb->i);
	sinu = sin(u);    sinOM = sin(orb->OMEGA);     sini = sin(orb->i);
	X[0] = r * (cosu*cosOM - sinu*sinOM*cosi);
	X[1] = r * (cosu*sinOM + sinu*cosOM*cosi);
	X[2] = r * sinu * sini;
	X[3] = Vr * (cosu*cosOM - sinu*sinOM*cosi) - Vn * (sinu*cosOM + cosu*sinOM*cosi);
	X[4] = Vr * (cosu*sinOM + sinu*cosOM*cosi) - Vn * (sinu*sinOM - cosu*cosOM*cosi);
	X[5] = Vr*sinu*sini + Vn*cosu*sini;
return X;
}//ConvertToXYZ
*/
