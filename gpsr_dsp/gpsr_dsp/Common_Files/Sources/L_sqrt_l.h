/*****************************************************************************
*    Copyright (c) 2000 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Enhanced square root computation: 
 *                            Newton-Rafson algorithm
 *    @file                   L_sqrt_l.h
 *    @author                 A.Nazarov
 *    @version                1.0
 */
/*****************************************************************************/
#ifndef _L_SQRT_L_H___
#define _L_SQRT_L_H___	1

#include "comtypes.h"
#include "l_sqrt_s.h"
#include "exp.h"

#if defined(_TMS320C6X)
  #define PORTING_L_SQRT_L      1
  #define L_sqrt_l              L_sqrt_lC64
#else
  #define PORTING_L_SQRT_L      0
  #define L_sqrt_l              L_sqrt_lCversion

//=========================================================================
/// @brief      Computes square root of Q15 number
///             iterate 4 times: max error 4.e-5
///             Precision conversion: Q result = Q((x-1)/2 + 16), when Qx is odd
///
/// @param[in]  x - int32 input value
/// @return     int32 square root of input
//=========================================================================
EXTERN_C int32 L_sqrt_l (int32 x);

#endif  // defined(_TMS320C6X)

#if PORTING_L_SQRT_L==1
  // Use inlined version of L_sqrt_l()
  #include "L_sqrt_l.c"
#endif // PORTING_L_SQRT_L

#endif // _L_SQRT_L_H___



