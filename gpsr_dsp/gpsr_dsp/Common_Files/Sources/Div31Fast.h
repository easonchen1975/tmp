/*****************************************************************************
 *    Copyright (c) 2002 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Fast fractional Q31 by Q31 division
 *    @file                   Div31Fast.h
 *    @author                 Andrey Bykov, Dima Paroshin
 *    @date                   20.11.2002
 *    @version                1.0
 */
/*****************************************************************************/
#ifndef __DIV31FAST_H
#define __DIV31FAST_H

#include "comtypes.h"
#include "Exp.h"
#include "Mul31.h"

//======================================================================
/// Fast division of two Q31 fractional numbers using fast calculation 
/// of 1/x based on Newton-Rafson method. Absolute value of numenator 
/// must be less than denumenator. Result in Q31 format saturated on 
/// overflow. Maximum absolute error comparing to conventional 32-bit
/// division is 6 (in case of 0x7ffffffe / 0x7fffffff). Maximum division 
/// error is 3.26e-09 comparing to conventional 32-bit division error 
/// equal to 4.66e-10.
///
/// @param[in]  Num - Q31 numenator
/// @param[in]  Den - Q31 denumenator
/// @return     Q31 value
//======================================================================

#if defined(_TMS320C6400)
  #define PORTING_DIV31FAST      1
  #define Div31Fast              Div31FastC64
#else
  #define PORTING_DIV31FAST      0
  #define Div31Fast              Div31FastCversion
  
  EXTERN_C   int32 Div31Fast(int32  Num, int32 Den);
#endif   //defined(_TMS320C6400)


#if PORTING_DIV31FAST==1
  // Use inlined version of Div31Fast()
  #include "Div31Fast.c"
#endif // PORTING_DIV31FAST


#endif  // __DIV31FAST_H

