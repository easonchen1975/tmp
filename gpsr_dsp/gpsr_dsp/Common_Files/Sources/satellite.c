/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        General satellite functions
 *   @file         satellite.c
 *   @author       V. Yeremeyev, M.Zhokhova
 *   @date         09.09.2005
 *   @version      2.0
 */
/*****************************************************************************/
#include "satellite.h"
#include "FrameDecoderGPS.h"
#include "RawMeasurForm.h"
#include "Control.h"
#include "Exp.h"
#include "l_sqrt_l.h"
#include "Framework.h"

#ifndef USE_DSPBIOS
#include "DebugPrintf.h"
#endif

const int32 UereTblQ17[16] = 
{
  0x40000,  0x59999,   0x80000,   0xB6666,   0x100000,  0x169999,   0x200000,   0x400000, 
  0x800000, 0x1000000, 0x2000000, 0x4000000, 0x8000000, 0x10000000, 0x20000000, 0x30000000
}; 

/*
const float32 UereTbl[16] = 
{
  2.0,   2.8,   4.0,   5.7,    8.0,    11.3,   16.0,   32.0, 
  64.0,  128.0, 256.0, 512.0,  1024.0, 2048.0, 4096.0, 6144.0
};
*/

const int32 l_CN0TblQ39[30] = 
{                         
  0x524F3B09L, 0x41617931L, 0x33EF0C36L, 0x2940A1BCL, 0x20C49BA5L, // 26, 27, 28, 29, 30 
  0x1A074EE5L, 0x14ACDA94L, 0x106C4363L, 0x0D0B90A3L, 0x0A5CB5F4L, // 31, 32, 33, 34, 35 
  0x083B1F80L, 0x0689BF51L, 0x05318138L, 0x0420102CL, 0x0346DC5DL, // 36, 37, 38, 39, 40 
  0x029A54B0L, 0x0211490EL, 0x01A46D23L, 0x014DF4DDL, 0x01094565L, // 41, 42, 43, 44, 45 
  0x00D2B659L, 0x00A75FEEL, 0x0084F352L, 0x00699B37L, 0x0053E2D6L, // 46, 47, 48, 49, 50 
  0x0042A211L, 0x0034EDB4L, 0x002A0AE9L, 0x00216549L, 0x001A86F0L  // 51, 52, 53, 54, 55 
};

/// exp(-X0)
const int32 Y0Q17[8] = 
{
  0x20000, 0x1368B, 0x9A36, 0x3EB2, 0x12E2, 0x436, 0x77, 0x3
};

/// Q(scale) for base X-points
const int8 shiftX0[8] =
{
  15, 15, 14, 13, 13, 12, 12, 11
};

/// base X-points: 0, 0.5, 1.2, 2.1, 3.3, 4.8, 7.0, 10.5
const int16 X0[8] =
{
  0x0, 0x4000, 0x4CCC, 0x4333, 0x6999, 0x4CCC, 0x7000, 0x5400
};

#define K_1_3_Q16       (0x5555)        // 1/3 in Q16
#define SC_X0           (11)
#define X_END_Q11       (0x6000)        // 12 in Q11
#define Y_END_Q17       (0x0)           // exp(-12) in Q17
#define ERR_DLL_MAXQ10  (0x4943)        // ERR_DLL_MAX in Q10

#define Bn_END_Q40      (0x6B5F)        //  = 1e-4/4*1e-3 in Q40
#define K1_BEGIN_Q8     (0x7D00)        // 4.0*F1*D*D*Bn, where Bn=1000/4
#define K1_END_Q31      (0x68DB)        // 4.0*F1*D*D*Bn, where Bn=1e-4/4
#define K2_Q28          (0x10000000)    // 2*(1-D)
#define K3_Q4           (0x7D00)        // (4.0*F2*D)/(0.001*UPDATE_DLL)
#define K4_Q15          (0x5093)        // Lambda*K_Rad_Hz*Bn_PLL
#define K5_Q6           (0x7D00)        // 1/(2*0.001*UPDATE_PLL)
#define CHIP_METERQ6    (0x4943)        // CHIP_METER in Q6
#define ONE_Q28         (0x10000000)    // 1 in Q28
#define Bn_PLLQ10       (0x4800)        // Bn_PLL in Q10

//=======================================================================================
/// Calculate exponent (exp(-t), where t = time/(1/Bn), Bn = Kdll/4)
///
/// @param[in]  epochCounter  - epoch counter
/// @param[in]  DLLswitchTime - time for DLL coefficient switch
/// @param[in]  k_begin_val   - fractional beginning coefficient for DLL
/// @param[in]  k_begin_shr   - Q value beginning coefficient
/// @param[in]  k_end_dll     - ending coefficient for DLL
/// @return exponent value
//=======================================================================================
static int32 ExpFix(int32  epochCounter,
                    int16  k_begin_val,
                    int16  k_begin_shr,
                    int32  k_end_dll)
{
  int   i,
        exp,    ///< exponent
        exp_k;  ///< exponent of k
  int16 x,      ///< current X-point
        x0,     ///< average X-points
        x1,     ///< left X-point
        x2,     ///< right X-point
        dx;     ///< delta X-points
  int8  shift;  ///< shift for current X-point
  int32 dy,     ///< delta y (exp(-X))
        y,      ///< current value of exp(-X)
        tmp_x,  ///< temporary X value
        k;      ///< coefficient

  /// calculate t = time/(1/Bn), Bn = Kdll/4
  /// beginning coefficient DLL
  if (epochCounter < DLL_TIME)
  {
    exp = Exp0(epochCounter);
    /// Qexp + Q(k_begin_shr + 15) - Q15 + Q2 -> Q11: Qx = Qexp + Qk_begin_shr - 9
    if (exp+k_begin_shr >= 9)
      tmp_x = (L_mpy_ls((epochCounter<<exp), k_begin_val) >> (exp+k_begin_shr-9)); 
    else
      tmp_x = (L_mpy_ls((epochCounter<<exp), k_begin_val) << (9-exp-k_begin_shr)); 
  }
  else  /// ending coefficient DLL
  {
    k = L_mpy_ls(k_end_dll, Bn_END_Q40);  /// Q0*Q40 -> Q25
    exp = Exp0(epochCounter);
    exp_k = Exp0(k);
    /// Qexp + Q(25 + exp_k - 16) - Q15 -> Q11: Qexp + Qexp_k - Q17 = Qx
    if ((exp + exp_k) >= 17)
      tmp_x = (L_mpy_ls((epochCounter<<exp), extract_h(k<<exp_k)) >> (exp+exp_k-17));  
    else
      tmp_x = (L_mpy_ls((epochCounter<<exp), extract_h(k<<exp_k)) << (17-exp-exp_k));  
  }
  /// calculate exp(-X)
  if (tmp_x < X_END_Q11)
  {
    x = (int16)tmp_x;
    /// search nearest base X-point to source X-point
    for(i=1;i<8;i++)
    {
      x1 = X0[i-1] >> (shiftX0[i-1] - SC_X0);
      x2 = X0[i] >> (shiftX0[i] - SC_X0);
      x0 = (x1 + x2)>>1;
      if (x0 >= x)  break;
    }
    shift = shiftX0[i-1];
    /// calculate difference between nearest base X-point and source X-point
    dx    = X0[i-1] - (x << (shift - SC_X0));
    /// calculate Taylor('s) series 4 - degree for exp(-X)
    dy    = y = Y0Q17[i-1];
    dy    = (L_mpy_ls(dy, dx) << (15-shift));         /// Q17
    y     = L_add(y, dy);
    dy    = ((L_mpy_ls(dy, dx) << (15-shift)) >> 1);  /// Q17
    y     = L_add(y, dy);
    dy    = (L_mpy_ls(dy, dx) << (15-shift));         /// Q17
    dy    = (L_mpy_ls(dy, K_1_3_Q16)>>1);             /// Q17
    y     = L_add(y, dy);
    dy    = ((L_mpy_ls(dy, dx) << (15-shift)) >> 2);  /// Q17
    y     = L_add(y, dy);
  }
  else
    y = Y_END_Q17;  /// exp(-12) is 0
  return y;
}

//=======================================================================================
/// GPS satellite object initialization
//
/// @param[in] pObject  - pointer to instance
/// @param[in] doppler  - Doppler frequency
/// @param[in] position - position in millisecond
/// @param[in] wn       - GPS week number
/// @param[in] ms_count - time of week, [ms]
/// @param[in] preamble - pointer to preamble structure
//=======================================================================================
void SatGPS_Init(tSatelliteGPS *pObject,
                 double         doppler,
                 double         position,
                 int16          wn,
                 uint32         ms_count,
                 tPreamble     *preamble)
{
  float64 dt;

  /// initialize channel object
  GPSchanInit(&pObject->gps_chan, pObject->sv_num);
  /// start channel object
  GPSchanStart(&pObject->gps_chan, doppler, position);
  pObject->decoder.PreambleTime = -1;
  if (preamble->PreambleTime != -1)
  {
    if ((wn == 0) && (preamble->PreambleTime == (WEEK-1)))
      dt = fabs(((wn - preamble->week + WEEK)*MS_WEEK) + (ms_count - preamble->time));
    else if ((wn == (WEEK-1)) && (preamble->PreambleTime == 0))
      dt = fabs(((wn - preamble->week - WEEK)*MS_WEEK) + (ms_count - preamble->time));
    else
      dt = fabs(((wn - preamble->week)*MS_WEEK) + (ms_count - preamble->time));

    if (dt > REACQ_TIME)
      preamble->PreambleTime = -1;
  }
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (SatGPS_EstimatedErrorFix, "fast_text_3")
	#endif
#endif
//============================================================================================
/// Calculate pseudo range estimated error bias and noise, pseudo range rate estimated error
///
/// @param[in]  pThis         - pointer to instance
/// @param[in]  accuracy      - user range accuracy (URA), coded 0..15
/// @param[in]  dt_IONQ17     - Ionosphere correction in Q17
/// @param[in]  dt_TROQ17     - Troposphere correction in Q17
/// @param[in]  epochCounter  - epoch counter
/// @param[in]  SNR_log       - satellite SNR
/// @param[in]  k_begin_val   - fractional beginning coefficient for DLL
/// @param[in]  k_begin_shr   - Q value beginning coefficient
/// @param[in]  k_end_dll     - ending coefficient for DLL
//============================================================================================
void SatGPS_EstimatedErrorFix(tEstErrFix  *pThis,
                              int16        accuracy,
                              int32        dt_IONQ17,
                              int32        dt_TROQ17,
                              int32        epochCounter,
                              int8         SNR_log,
                              int16        k_begin_val,
                              int16        k_begin_shr,
                              int32        k_end_dll)
{
  int32 expQ17,         ///< exponent
        noise,          ///< temporary pseudo range estimated error noise
        freq,           ///< temporary pseudo range rate estimated error
        bias = 0,       ///< temporary first part pseudo range estimated error bias
        tmp,            ///< temporary variable
        uere,           ///< temporary second part pseudo range estimated error bias
        ion,            ///< square of ionosphere error
        tro,            ///< square of troposphere error
        k;              ///< temporary coefficient
  int   CN0,            ///< position of array in compliance with SNR
        exp,            ///< exponent
        exp_k,          ///< exponent of k
        exp_bias,       ///< exponent of bias
        sh_uere,        ///< shift for accuracy
        sh_ion,         ///< shift of square ionosphere error
        sh_tro,         ///< shift of square troposphere error
        sh_all;         ///< shift for all component

  /// check boundary SNR: from 26 to 55
  if (SNR_log < 26)
    CN0 = 0;
  else
  {
    if (SNR_log > 55)
      CN0 = 29;
    else
      CN0 = SNR_log - 26;
  }

  /// beginning coefficient DLL
  if (epochCounter < DLL_TIME)
  {
    /// part of pseudo range estimated error bias: error = error_DLL*exp(-t), where t = time/(1/Bn), Bn=Kbegin_dll/4
    /// calculate exp(-t), result in Q17
    expQ17 = ExpFix(epochCounter, k_begin_val, k_begin_shr, k_end_dll);
    /// calculate error_DLL*exp(-t):
    /// Q17 + Q10 - Q15 -> Q17 => Q5
    pThis->err_dllQ17 = L_mpy_ls(expQ17, ERR_DLL_MAXQ10) << 5;
    /// calculate square error
    exp_bias = Exp0(pThis->err_dllQ17);
    tmp      = pThis->err_dllQ17<<(exp_bias-1);
    /// Q17 + Q17 + Q(exp_bias-1) + Q(exp_bias-1) - Q31 => Q(1+exp_bias+exp_bias) =>Q21
    if ((exp_bias+exp_bias) >= 20)
      bias  = L_mpy_ll(tmp, tmp) >> (exp_bias+exp_bias-20);
    else
      bias  = L_mpy_ll(tmp, tmp) << (20-exp_bias-exp_bias);
    /// calculate pseudo range estimated error noise: where Bn=Kbegin_dll/4
    /// CHIP_METER*sqrt(4.0*F1*D*D*Bn*l_CN0Tbl[CN0]*(2*(1-D)+(4.0*F2*D*l_CN0Tbl[CN0])/(0.001*update_dll)))
    /// l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL)
    noise = L_mpy_ls(l_CN0TblQ39[CN0], K3_Q4);  /// Q39 + Q4 => Q28  
    /// (2*(1-D) + l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL))
    noise = L_add(noise, K2_Q28); 
    /// l_CN0TblQ39[CN0]*(2*(1-D) + l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL))
    noise = L_mpy_ll(noise, l_CN0TblQ39[CN0]);  /// Q28 + Q39 => Q36 
    /// (Kbegin_dll/4)*4.0*F1*D*D
    k = mpy(k_begin_val, K1_BEGIN_Q8);  /// Qk_begin_shr + Q15 + Q8 => Qk_begin_shr + Q23
    /// ((Kbegin_dll/4)*4.0*F1*D*D)*(l_CN0TblQ39[CN0]*(2*(1-D) + l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL)))
    exp_k = Exp0(k);
    /// Q36 + Q(k_begin_shr + exp_k + 23 - 16) - Q15 => Q28 + Qk_begin_shr + exp_k -> Q25 => Qx = Qk_begin_shr + exp_k + Q3
    noise = L_mpy_ls(noise, extract_h(k<<exp_k)) >> (k_begin_shr + exp_k + 3); 
    /// sqrt(value)
    noise = L_sqrt_l(noise);  /// Q(25-1)/2 + 16 => Q28
    /// CHIP_METER*sqrt(value)
    noise = L_mpy_ls(noise, CHIP_METERQ6)>>2;  /// Q28 + Q6 -> Q19 => Q17
    pThis->ps_delay_noiseQ17 = noise;
  }
  else  /// if !(epochCounter < DLLswitchTime) ending coefficient DLL
  {
    /// part of pseudo range estimated error bias: error = previous_error*exp(-t), where t = time/(1/Bn), Bn=Kend_dll/4
    /// calculate exp(-t), result in Q17
    expQ17 = ExpFix(epochCounter, k_begin_val, k_begin_shr, k_end_dll);
    /// calculate error_DLL*exp(-t)
    exp = Exp0(pThis->err_dllQ17);
    tmp = L_mpy_ls(expQ17, extract_h(pThis->err_dllQ17<<exp));  /// Q17 + Q17 => Q(3+exp)
    /// calculate square error
    exp_bias = Exp0(tmp);
    tmp = tmp<<(exp_bias-1);
    /// Q(3 + exp + exp_bias - 1) + Q(3 + exp + exp_bias - 1) - Q31 => Q(2*exp + 2*exp_bias - 27) -> Q21
    if ((exp+exp+exp_bias+exp_bias) >= 48)
      bias = L_mpy_ll(tmp, tmp) >> (exp+exp+exp_bias+exp_bias-48);
    else
      bias = L_mpy_ll(tmp, tmp) << (48-exp-exp-exp_bias-exp_bias);
    /// calculate pseudo range estimated error noise: where Bn=Kend_dll/4
    /// CHIP_METER*sqrt(4.0*F1*D*D*Bn*l_CN0Tbl[CN0]*(2*(1-D)+(4.0*F2*D*l_CN0Tbl[CN0])/(0.001*update_dll)))
    /// l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL)    
    noise = L_mpy_ls(l_CN0TblQ39[CN0], K3_Q4);  /// Q39 + Q4 => Q28 
    /// (2*(1-D) + l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL))
    noise = L_add(noise, K2_Q28); 
    /// l_CN0TblQ39[CN0]*(2*(1-D) + l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL))
    noise = L_mpy_ll(noise, l_CN0TblQ39[CN0]);  /// Q28 + Q39 => Q36
    /// (Kend_dll/4)*4.0*F1*D*D
    k = L_mpy_ls(k_end_dll, K1_END_Q31);  /// Q0 + Q31 => Q16
    /// ((Kbegin_dll/4)*4.0*F1*D*D)*(l_CN0TblQ39[CN0]*(2*(1-D) + l_CN0TblQ39[CN0]*(4.0*F2*D)/(0.001*UPDATE_DLL)))
    exp_k = Exp0(k);
    if ((exp_k-14) > 0)
    {
      noise = L_mpy_ls(noise, extract_h(k<<exp_k)) >> (exp_k-14);  /// Q36 + Q(exp_k + 16 - 16) => Q(36 + exp_k - 15) => Q35
    }
    else
    {
      noise = L_mpy_ls(noise, extract_h(k<<exp_k)) << (14-exp_k);  /// Q36 + Q(exp_k + 16 - 16) => Q(36 + exp_k - 15) => Q35
    }
    /// sqrt(value)
    noise = L_sqrt_l(noise);  /// Q(35-1)/2 + 16 = 33
    /// CHIP_METER*sqrt(value)
    noise = L_mpy_ls(noise, CHIP_METERQ6)>>7;  /// Q33 + Q6 => Q24 => Q17
    pThis->ps_delay_noiseQ17 = noise;
  }  /// if !(epochCounter < DLLswitchTime)
  /// calculate pseudo range estimated error bias: URA^2 + dll_error^2
  /// calculate user range accuracy
  exp_bias = Exp0(UereTblQ17[accuracy]);
  /// calculate square
  tmp = UereTblQ17[accuracy]<<(exp_bias-1);
  /// Q(17 + exp_bias - 1) + Q(17 + exp_bias - 1) => Q(1 + exp_bias + exp_bias)
  uere = L_mpy_ll(tmp, tmp);
  sh_uere = exp_bias + exp_bias + 1;
  /// calculate square of ionosphere error
  exp_bias = Exp0(dt_IONQ17);
  tmp = dt_IONQ17<<(exp_bias-1);
  /// Q(17 + exp_bias - 1) + Q(17 + exp_bias - 1) - Q31 => Q(1 + exp_bias + exp_bias)
  ion    = L_mpy_ll(tmp, tmp);
  if (dt_IONQ17 == 0)
    sh_ion = 21;
  else
    sh_ion = exp_bias + exp_bias + 1;
  /// calculate square of troposphere error
  exp_bias = Exp0(dt_TROQ17);
  tmp = dt_TROQ17<<(exp_bias-1);
  /// Q(17 + exp_bias - 1) + Q(17 + exp_bias - 1) - Q31 => Q(1 + exp_bias + exp_bias)
  tro    = L_mpy_ll(tmp, tmp);
  if (dt_TROQ17 == 0)
    sh_tro = 21;
  else
    sh_tro = exp_bias + exp_bias + 1;
  /// calculate total shift for uere, ion, tro and bias
  if ((uere > 21) && (ion > 21) && (tro >> 21))
  {
    uere >>= (sh_uere -21);
    ion  >>= (sh_ion - 21);
    tro  >>= (sh_tro - 21);
    sh_all = 21;
  }
  else if ((uere > ion) && (tro > ion))
  {
    bias >>= (21 - sh_ion);
    uere >>= (sh_uere - sh_ion);
    tro  >>= (sh_tro - sh_ion);
    sh_all = sh_ion;
  }
  else if (uere > tro)
  {
    bias >>= (21 - sh_tro);
    uere >>= (sh_uere - sh_tro);
    ion  >>= (sh_ion - sh_tro);
    sh_all = sh_tro;
  }
  else
  {
    bias >>= (21 - sh_uere);
    ion  >>= (sh_ion - sh_uere);
    tro  >>= (sh_tro - sh_uere);
    sh_all = sh_uere;
  }
  /// accumulation errors
  bias   = L_add(bias, uere);
  bias   = L_add(bias, ion);
  bias   = L_add(bias, tro);
  sh_all = (sh_all-1)>>1;
  pThis->ps_delay_biasQ17 = L_sqrt_l(bias) >> (sh_all-1);  /// Q(sh_all) => Q((sh_all-1)/2 + 16) => Q17
  /// calculate pseudo range rate estimated error: 
  /// Lambda*K_Rad_Hz*Bn_PLL*sqrt(Bn_PLL*l_CN0Tbl[CN0]*(1.0 + ((1.0*l_CN0Tbl[CN0])/(2*0.001*update_pll))))
  {
    /// ((1.0*l_CN0Tbl[CN0])/(2*0.001*UPDATE_PLL))
    freq = L_mpy_ls(l_CN0TblQ39[CN0], K5_Q6) >> 2;  /// Q39 + Q6 => Q30 => Q28
    /// 1.0 + ((1.0*l_CN0Tbl[CN0])/(2*0.001*UPDATE_PLL))
    freq = L_add(freq, ONE_Q28);
    /// l_CN0Tbl[CN0]*(1.0 + ((1.0*l_CN0Tbl[CN0])/(2*0.001*UPDATE_PLL)))
    freq = L_mpy_ll(freq, l_CN0TblQ39[CN0])>>5;  /// Q28 + Q39 => Q36 => Q31
    /// Bn_PLL*l_CN0Tbl[CN0]*(1.0 + ((1.0*l_CN0Tbl[CN0])/(2*0.001*UPDATE_PLL)))
    freq = L_mpy_ls(freq, Bn_PLLQ10)>>1;  /// Q31 + Q10 => Q26 => 25
    /// sqrt(value)
    freq = L_sqrt_l(freq);  /// Q(25-1)/2 + 16 => Q28
    /// Lambda*K_Rad_Hz*Bn_PLL*sqrt(value)
    pThis->ps_freqQ17 = L_mpy_ls(freq, K4_Q15)>>11;  /// Q28 + Q15 => Q28 => Q17
  }
}

//=======================================================================================
/// GPS satellite process
//
/// @param[in] pThis             - pointer to satellite object
/// @param[in] pSamples           - pointer to samples
/// @param[in] DecCommon          - pointer to common decoder
/// @param[in] week               - GPS week number
/// @param[in] ms_count           - time of week
/// @param[in] CN_THRESHOLD       - minimal SNR boundary
/// @param[in] coefPllDll         - coefficients for PLL/DLL
/// @param[in] WeekInit           - week number of initialized
/// @param[in] eph_set_flg        - ephemeris set flag
//=======================================================================================
const tGPSchanStatus *SatGPS_Process(tSatelliteGPS          *pSat, 
                                     const tComplex8Aligned *pSamples,
                                     tDecoderCommon         *DecCommon,
                                     const int16             week,
                                     uint32                  ms_count,
                                     int8                    CN_THRESHOLD,
                                     const tCoefPLLDLL       coefPllDll,
                                     eFlag                   WeekInit,
                                     uint8                   eph_set_flg)
{
  tEpoch                epoch;   ///< epoch
  const tGPSchanStatus *status;  ///< channel status

  /// initialize epoch
  epoch.pSamples = pSamples;
  /// process epoch
  GPSchanProcessEpoch(&pSat->gps_chan, &epoch, week, ms_count,&DecCommon->preamble[pSat->sv_num-1], CN_THRESHOLD, 
                      coefPllDll, pSat->decoder.Nframe);
  /// get status of channel
  status = GPSchanGetStatus(&pSat->gps_chan);
  pSat->SNR_log = status->CN_dBHzQ7>>7;

  SatGPS_DecoderSubframe(pSat->sv_num, status->bit, DecCommon, &pSat->decoder, week, ms_count, WeekInit, eph_set_flg);
  // ephemeris: 2 bit
  if ((DecCommon->TempEph[pSat->sv_num-1].flag == 1) && ((pSat->status&0x4) == 0))
    pSat->status |= 0x4;
  // almanac: 3 bit
  if ((DecCommon->TempAlmGPS[pSat->sv_num-1].flag == 1) && ((pSat->status&0x8) == 0))
    pSat->status |= 0x8;
  return status;
}

