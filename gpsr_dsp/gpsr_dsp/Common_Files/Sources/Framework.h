/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Data types for whole framework initialization
 *    @file                   Framework.h
 *    @author                 M. Zhokhova
 *    @date                   22.09.2005
 *    @version                2.0
 */
/*****************************************************************************/

#ifndef _FRAMEWORK_H
#define _FRAMEWORK_H

// alex added, for DSP/BIOS 5.4
//#include <std.h>
//#include "gpsrcfg.h"
//

#include "UartTerminal.h"
#include "ThreadManager.h"
#include "flash.h"
#include "BasicDSPEvents.h"
#include "SampleDriver.h"
#include "SatelliteManager.h"

/// Built-in test status flags
#define SYS_CHSUM    (0x1)    ///< system SW check sum flag
#define CONFIG_CHSUM (0x2)    ///< configuration setup check sum flag
#define FLASH_WR     (0x4)    ///< FLASH write/read flag
#define UART_MOD     (0x8)    ///< UART module flag
#define NPPS_MOD     (0x20)   ///< nPPS module flag
#define FIFO_MOD     (0x40)   ///< FIFO module flag
#define TCXO         (0x80)   ///< TCXO flag
#define ANTENNA      (0x2)    ///< power of antenna bit 1

typedef struct
{
  int16              wnFlash;                   ///< week number from FLASH
  uint8              cntOverFlow;               ///< counter of week number overflow
  eFlag              DataError;                 ///< if fg_ON then error week data
}
tWeekData;

/// Framework  structure
typedef struct  tagFramework
{
  tTerminal          DebugTerminal;             ///< Debug terminal driver

  tFlashDrv          FlashDrv;                  ///< Flash driver

  tEventHandler      EventHandler;              ///< EVENTs
  
  tThreadManager     BasicDSPThreadManager;     ///< THREAD MANAGER

  tBuiltInTest       BuiltInTest;               ///< BIT structure
  eFlag              BITFlag;                   ///< BIT process flag
  eFlag              TCXOtest;                  ///< TCXO test in BIT flag
  eFlag              TCXOerr;                   ///< error TCXO flag
  uint8              StartTestTCXO;             ///< TCXO test start flag
  uint16             version;                   ///< version project number
  uint16             subversion;                ///< subversion project number
  eFlag              Timeout;                   ///< timeout flag

  tWeekData          weekData;                  ///< data for control overflow week number
} 
tFramework;

//==========================================================================================
/// @brief      Framework object initialize
/// @param[in]  pFramework  - pointer to instance
/// @param[in]  BaudRate422 - UART baud rate
//===========================================================================================
EXTERN_C void FrameWork_Init(tFramework *pFramework,
                             int8        BaudRate422);

//============================================================================
/// @brief     Control FLASH
/// 
/// @param[in] pThis      - pointer to instance
/// @param[in] pTopSystem - pointer to top system structure
/// @param[in] config     - pointer to configuration setup
/// @param[in] flags      - pointer to control flags
/// @param[in] weekData   - pointer to week data structure
//============================================================================
EXTERN_C void FrameWork_FlashControl(tFlashDrv     *pThis, 
                                     tTopSystem    *pTopSystem,
                                     CONFIG_SETUP  *config,
                                     tFlagsControl *flags,
                                     tWeekData     *weekData);

//==============================================================================
/// Stop profiling timer 3
///
/// @return Processor time steps between PROFILE_START() and this function call
//==============================================================================
EXTERN_C uint32 PROFILE_STOP();

//==========================================================================
/// Read timer 3 value
///
/// @return Processor time steps between PROFILE_START() and PROFILE_STOP()
//==========================================================================
EXTERN_C uint32 PROFILE_READ();

//======================================================================
/// timer 3 start
//======================================================================
EXTERN_C void PROFILE_START();

//======================================================================
/// timer 0 stop
//======================================================================
EXTERN_C void Timer0Stop();

//======================================================================
/// timer 0 start
///
/// @param[in]  time - time for timer
//======================================================================
EXTERN_C void Timer0Start(uint32 time);

//======================================================================
/// Get debug terminal
///
/// @return pointer to Debug terminal
//======================================================================
EXTERN_C tTerminal *GetPrimaryDebugTerminal();

#endif // _FRAMEWORK_H

