/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS 3x12 Receiver
 *    @brief                  General variables, constants and types
 *    @file                   Global.h
 *    @author                 M. Zhokhova, V. Yeremeyev
 *    @date                   28.07.2005
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef _GLOBAL_H__
#define _GLOBAL_H__

#include "comtypes.h"

/// User-defined event handler for message processing from subsystems
typedef void (*fnPostTrackingManager) (void*);
typedef void (*fnPostNavTask) (void*);

/// events handler
typedef struct
{
  /// pointer to the host instance
  void                 *pHost;

  /// pointer to the function which posts TrackingManager()
  fnPostTrackingManager pfnPostTrackingManager;

  /// Pointer to the function which posts NavTaskThread()
  fnPostNavTask         pfnPostNavTask;

}
tEventHandler;

///  Thread IDs
#ifndef  USE_DSPBIOS
typedef enum
{
  et_THREAD_TRACKING,         ///< Tracking manager thread
  et_THREAD_NAVTASK,          ///< Navigation task thread
}
eThreadID;

#else  // USE_DSPBIOS
  #define  et_THREAD_TRACKING     (&SWI_Tracking_Thread) ///< Tracking manager thread
  #define  et_THREAD_NAVTASK      (&SWI_NavTask_Thread)  ///< Navigation task thread
#endif  // USE_DSPBIOS 

#endif // _GLOBAL_H__


