/*****************************************************************************
 *   Copyright (c) 2004 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Common functions
 *   @file                   comfunc.h
 *   @author                 Erast V. Kunenkov
 *   @date                   01.11.2004
 *   @version                1.0
 */
/*****************************************************************************/
#ifndef __COMFUNCS_H
#define __COMFUNCS_H

/*************************
  Minimal of two numbers
**************************/
#ifndef Min
 #define Min(x,y) (((x)<=(y))?(x):(y))
#endif

/******************
  Absolute value
*******************/
#ifndef Abs
 #define Abs(x) (((x)>=0)?(x):(-x))
#endif

/******************
  Sign of double
*******************/
#ifndef fSign		// (-1 and 1 only)
 #define fSign(x) (((x)>=0)?(1.):(-1.))
#endif

/******************
  Right shift
*******************/
#ifndef Shr
  #define Shr(Value, shift)  ( ((shift) >= 0) ? (Value) >> (shift) : (Value) << (-shift) )
#endif

/**************************************
  Data with different size saved
***************************************/
#define COPYINT(dest, src, size) \
  { int i; \
    for(i=0;i<size;i++) \
    { dest[i] = *(uint8*)src; src++; } \
  }

#endif /* __COMFUNCS_H */

