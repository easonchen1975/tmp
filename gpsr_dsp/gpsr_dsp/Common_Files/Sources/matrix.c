/*************************************************************************************
*    Copyright (c) 2005 Spirit Corp.
**************************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Matrix functions implementation
*    @file                   matrix.c
*    @author                 A. Baloyan
*    @date                   25.06.2005
*    @version                1.0
*/
/*************************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"
#include "comtypes.h"

//=====================================================================================
/// Matrix initialization
///
/// @param[in] A - pointer to matrix
/// @param[in] N - number of rows
/// @param[in] M - number of columns
//=====================================================================================
void CreateMatrix(Matrix *A, int32 N, int32 M)
{
  A->mRaw = N;
  A->mCol = M;
  memset(A->Matr, 0, sizeof(A->Matr));
  return;
}

//=====================================================================================
/// Free matrix dynamic allocation memory
///
/// @param[in] A - pointer to matrix
//=====================================================================================
void DestroyMatrix(Matrix* A)
{
  A->mRaw = 0;
  A->mCol = 0;
  return;
}

//=====================================================================================
/// Copy matrix B to A (A = B)
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
//=====================================================================================
void CopyMatrix(Matrix *A, Matrix *B)
{ 
  int32 i, j, N = B->mRaw, M = B->mCol;
  
  if(A->mRaw != N || A->mCol != M) 
    CreateMatrix(A,N,M);
  
  if(A != B)
    for(i = 0; i < N; i++)
      for(j = 0; j < M; j++)
        A->Matr[i][j] = B->Matr[i][j];
  return;
}

//=====================================================================================
/// Make unitary Matrix A = I( N x N)
///
/// @param[out] A - pointer to matrix
/// @param[in]  N - matrix dimention
//=====================================================================================
void UnitaryMatrix(Matrix *A, int32 N)
{ 
  int32 i;

  CreateMatrix(A,N,N);
  memset(A->Matr, 0, sizeof(A->Matr));
  for(i = 0; i < N; i++) 
    A->Matr[i][i] = 1.;
  return;
}

//=====================================================================================
/// Square of the module of matrix A computation
///
/// @param[in] A - pointer to matrix
/// @return    square of the module of matrix A 
//=====================================================================================
float64 Mod(Matrix *A)
{ 
  float64 D = 0.;
  int32 i, j;

  if(A->mRaw == 1 || A->mCol == 1)
  { 
    for(i = 0; i < A->mRaw; i++)
      for(j = 0; j < A->mCol; j++)
        D += A->Matr[i][j] * A->Matr[i][j];
    return D;
  }
  if(A->mRaw == A->mCol)
  { 
    for(i = 0; i < A->mRaw; i++)
      D += A->Matr[i][i] * A->Matr[i][i];
    return D;
  }
  return 0;
}

//=====================================================================================
/// Computation of the inverse Matrix A = Inv(B)
///
/// @param[out] A - pointer to matrix A = Inv(B)
/// @param[in]  B - pointer to matrix B
//=====================================================================================
void InverseMatrix(Matrix *A, Matrix *B)
{ 
  int32 i, j, l, q, N = B->mRaw;
  float64 C[MATRIX_MAX_M][MATRIX_MAX_M], D[MATRIX_MAX_M][MATRIX_MAX_M];
  float64 K, S;

  if(B->mRaw != B->mCol)
  {
    DestroyMatrix(A);
    return;
  }
  
  for(i = 0; i < N; i++)
  { 
    
    for(j = 0; j < N; j++)
    { 
      C[i][j] = B->Matr[j][i];
      D[i][j] = 0;
    } 
    
    D[i][i] = 1;
  }
  for(i = 0; i < N - 1; i++)
  { 
    for(j = i, K = 0; j < N; j++)
    { 
      S = fabs(C[i][j]);
      if(S > K) 
      { 
        l = j; 
        K = S;
      }
    }
    if(!K)
    {
      DestroyMatrix(A);
      return;
    }
    if(l != i)
      for(q = 0; q < N; q++)
      { 
        S = C[q][i];
        C[q][i] = C[q][l];
        C[q][l] = S;
        S = D[q][i];
        D[q][i] = D[q][l];
        D[q][l] = S;
      }
    for(j = i + 1 ; j < N; j++)
    { 
      K = C[i][j] / C[i][i];
      if(K)
      { 
        for(l = i; l < N; l++) 
          C[l][j] -= K * C[l][i];
        
        for(l = 0; l < N; l++) 
          D[l][j] -= K * D[l][i];
      }
    }
  }
  for(i = N - 1; i > 0; i--)
  { 
    for(j = i - 1; j >= 0; j--)
    { 
      K = C[i][j] / C[i][i];
      if(K)
      { 
        C[i][j] -= K * C[i][i];
        
        for(l = 0; l < N; l++) 
          D[l][j] -= K * D[l][i];
      }
    }
  }
  for(i = 0; i < N; i++)
  { 
    K = C[i][i];
    for(j = 0; j < N; j++)
      D[j][i] /= K;
  }
  
  CreateMatrix(A,N,N);
  
  for(i = 0; i < N; i++)
  { 
    for(j = 0; j < N; j++)
      A->Matr[j][i] = D[i][j];
  }
  return;
}

//=====================================================================================
/// Computation of the difference of matrices A = B-C
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
/// @param[in]  C - pointer to matrix C
//=====================================================================================
void Sum_Matrix(Matrix* A, Matrix* B, Matrix* C)
{ 
  int32 i, j;
  Matrix D, *Ptr;
  
  D.mCol = 0;
  D.mRaw = 0;

  if(B->mRaw != C->mRaw || B->mCol != C->mCol)
  {
    DestroyMatrix(A);
    return;
  }

  Ptr = ((A == B) || (A == C)) ? &D : A;
  CreateMatrix(Ptr,B->mRaw,B->mCol);
  for(i = 0; i < B->mRaw; i++)
    for(j = 0; j < B->mCol; j++) 
      Ptr->Matr[i][j] = B->Matr[i][j] - C->Matr[i][j];
  
  if((A == B) || (A == C)) 
  { 
    CopyMatrix(A,&D); 
    DestroyMatrix(&D); 
  }
  return;
}

//=====================================================================================
/// Multiplication of matrices A = B*C
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
/// @param[in]  C - pointer to matrix C
//=====================================================================================
void MultMatrix(Matrix *A, Matrix *B, Matrix *C)
{ 
  float64 K;
  int32 i, j, l;
  Matrix D, *Ptr;
  
  D.mCol = 0;
  D.mRaw = 0;
  
  if(B->mCol != C->mRaw)
  {
    DestroyMatrix(A);
    return;
  }
  
  Ptr = ((A == B) || (A == C)) ? &D : A;
  CreateMatrix(Ptr,B->mRaw,C->mCol);
  for(i = 0; i < B->mRaw; i++)
    for(j = 0; j < C->mCol; j++)
    { 
      for(l = 0, K = 0; l < B->mCol; l++)
        K += B->Matr[i][l] * C->Matr[l][j];
      Ptr->Matr[i][j] = K;
    }
  if((A == B) || (A == C)) 
  { 
    CopyMatrix(A,&D);
    DestroyMatrix(&D); 
  }
  return;
}

//=====================================================================================
/// Multiplication of matrices A = T(B)*C
///
/// @param[out] A - pointer to matrix A
/// @param[in]  B - pointer to matrix B
/// @param[in]  C - pointer to matrix C
//=====================================================================================
void MultMatrixT_(Matrix *A, Matrix *B, Matrix *C)
{ 
  float64 K;
  int32 i, j, l;
  Matrix D, *Ptr;
  
  D.mCol = 0;
  D.mRaw = 0;
  
  if(B->mRaw != C->mRaw)
  {
    DestroyMatrix(A);
    return;
  }

  Ptr = ((A == B) || (A == C)) ? &D : A;
  CreateMatrix(Ptr,B->mCol,C->mCol);

  for(i = 0; i < B->mCol; i++)
    for(j = 0; j < C->mCol; j++)
    { 
      for(l = 0, K = 0; l < B->mRaw; l++)
        K += B->Matr[l][i] * C->Matr[l][j];
      
      Ptr->Matr[i][j] = K;
    }
  
  if((A == B) || (A == C)) 
  { 
    CopyMatrix(A,&D);
    DestroyMatrix(&D); 
  }
  
  return;
}

//=====================================================================================
/// QR-decomposition of the matrix A=QR
/// Resultant matrices are contained in R and Q correspondly
///
/// @param[in]  A - pointer to matrix A=QR
/// @param[out] Q - pointer to matrix Q
/// @param[out] R - pointer to matrix R
//=====================================================================================
void QR_Decomposition(Matrix *A, Matrix *Q, Matrix *R)
{ 
  int32 i, j, k;
  int32 N = A->mRaw, M = A->mCol;
  float64 temp;
  
  CopyMatrix(Q,A); 
  CreateMatrix(R,M,M);
  
  for (k = 0; k < M; k++)
  { 
    for (i = 0, temp = 0.; i < N; i++) 
    {
      temp += Q->Matr[i][k] * Q->Matr[i][k];
    }
    
    R->Matr[k][k] = sqrt(temp);
    temp = 1. / R->Matr[k][k];
    
    for (i = 0; i < N; i++) 
    {
      Q->Matr[i][k] *= temp;
    }
    
    for (j = k + 1; j < M; j++)
    { 
      for (i = 0, temp = 0.; i < N; i++) 
      {
        temp += Q->Matr[i][k] * Q->Matr[i][j];
      }
      
      for (i = 0; i < N; i++) 
      {
        Q->Matr[i][j] -= temp * Q->Matr[i][k];
      }
      
      R->Matr[k][j] = temp;
    }
  }
}

//=====================================================================================
/// Solution of linear system X=R*Y (i.e. Y=Inv(R)*X)
/// where R is an upper triangular matrix
///
/// @param[out] Y - pointer to matrix Y=Inv(R)*X
/// @param[in]  R - pointer to matrix R
/// @param[in]  X - pointer to matrix X
//=====================================================================================
void UppTriSolution(Matrix *Y, Matrix *R, Matrix *X)
{ 
  int32 i, j;
  float64 temp;

  if(R->mRaw != R->mCol)
  {
    DestroyMatrix(Y);
    return;
  }
  
  if(R->mRaw != X->mRaw)
  {
    DestroyMatrix(Y);
    return;
  }
  
  CopyMatrix(Y,X);
  
  for(i = Y->mRaw - 1; i >= 0; i--)
  { 
    for(j = Y->mRaw - 1,temp = 0.; j > i; j--)
    {
      temp += R->Matr[i][j] * Y->Matr[j][0];
    }
    Y->Matr[i][0] -= temp; Y->Matr[i][0] /= R->Matr[i][i];
  }
  return;
}

//=====================================================================================
/// Computation of a matrix G=Inv(R)*Q where R is an upper triangular matrix
///
/// @param[out] G - pointer to matrix G=Inv(R)*Q
/// @param[in]  R - pointer to matrix R
/// @param[in]  Q - pointer to matrix Q
//=====================================================================================
void UppTriSolModif(Matrix *G, Matrix *R, Matrix *Q)
{ 
  int32 i, j, k;
  float64 temp;

  if(R->mRaw != R->mCol)
  {
    DestroyMatrix(G);
    return;
  }
  if(R->mRaw != Q->mRaw)
  {
    DestroyMatrix(G);
    return;
  }
  CopyMatrix(G,Q);
  for(k = 0; k < G->mCol; k++)
  { 
    for (i = G->mRaw - 1; i >= 0; i--)
    { 
      for (j = G->mRaw - 1, temp = 0.; j > i; j--)
      {
        temp += R->Matr[i][j] * G->Matr[j][k];
      }
      G->Matr[i][k] -= temp;
      G->Matr[i][k] /= R->Matr[i][i];
    }
  } 
  return;
}

//=====================================================================================
/// Remove N_Row rows from matrix starting with J_Row row: A = DelRow(B)
///
/// @param[out] A     - pointer to matrix A
/// @param[in]  B     - pointer to matrix B
/// @param[in]  I_Row - starting row
/// @param[in]  N_Row - number row
//=====================================================================================
void DelRowMatrix(Matrix *A, Matrix *B, int32 I_Row, int32 N_Row)
{ 
  int32 i, j;
  Matrix C, *Ptr;
  
  C.mRaw = 0;
  C.mCol = 0;

  if(I_Row < 1 || I_Row + N_Row - 1 > B->mRaw)
  {
    DestroyMatrix(A);
    return;
  }
  
  Ptr = (A == B) ? &C : A;
  
  CreateMatrix(Ptr,B->mRaw-N_Row,B->mCol);
  
  for(i = 0; i < I_Row - 1; i++)
  {
    for(j = 0; j < B->mCol; j++)
    {
      Ptr->Matr[i][j] = B->Matr[i][j];
    }
  }
  
  for(i = I_Row + N_Row - 1; i < B->mRaw; i++)
  {
    for(j = 0; j < B->mCol; j++)
    {
      Ptr->Matr[i - N_Row][j] = B->Matr[i][j];
    }
  }
  
  if(A == B) 
  { 
    CopyMatrix(A,&C);
    DestroyMatrix(&C); 
  }
  return;
}

