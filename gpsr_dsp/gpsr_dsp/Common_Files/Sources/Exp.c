/*****************************************************************************
 *    Copyright (c) 2000 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Getting exponent of number
 *    @file                   Exp.c
 *    @author                 Alex Nazarov
 *    @date                   10.03.2000
 *    @version                1.0
 */
/*****************************************************************************/

/*-----------------------------------------------
  do not compile when it must be linked as
  a part of common SPCORP library
-----------------------------------------------*/
#include "exp.h"

#ifndef   __EXP_C
#define   __EXP_C

#if PORTING_EXP==0
//=============================================
/// Getting exponent of number
///
/// @param[in]  Value - integer value
/// @return     exponent
//=============================================
int ExpCversion (int32 Value)
{
  int Result=0;
  if ( Value==0 )
    return 0;
  while ( (int32)(Value^(Value<<1))>0 ) //MSB != MSB-1
  {
    Value<<=1;
    Result++;
  }
  return Result;
}

//===============================================================
/// Getting exponent of number. 
/// If argument is zero, returns 0x1F (like in C6xx)
///
/// @param[in]  Value - integer value
/// @return     exponent
//================================================================
int Exp0Cversion (int32 Value)
{
  int Result=0;
  if ( Value==0 )  return 0x1F;
  
  while ( (int32)(Value^(Value<<1))>0 ) //MSB != MSB-1
  {
    Value<<=1;
    Result++;
  }
  return Result;
}
#endif  // PORTING_EXP

// Inline function for C64xx C-compiler
#if ((PORTING_EXP==1) && defined(_TMS320C6X))
inline int16 ExpC64(int32 Value)
{
  if (Value == 0) return 0;
  return _norm(Value);
}

// returns exponent. If argument is zero, returns 0x1F (like in C6xx)
inline int16 Exp0C64(int32 Value)
{
  return _norm(Value);
}
#endif // ((PORTING_EXP==1) && defined(_TMS320C6X))

#endif // __EXP_C
