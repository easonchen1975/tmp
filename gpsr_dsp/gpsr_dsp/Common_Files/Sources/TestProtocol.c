/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project      GPS Receiver
 *   @brief        Protocol for testing GPS receiver
 *   @file         TestProtocol.c
 *   @author       M. Zhokhova
 *   @date         16.01.06
 *   @version      1.0
 */
/*****************************************************************************/
#include <string.h>
#include <stdio.h>
#include "MainManager.h"

//========================================================================
/// @brief     Search string message
///
/// @param[in] pThis - pointer to UART driver
/// @param[in] data  - pointer to string data
/// @return    1 if message found otherwise 0
//========================================================================
int FrameWork_SearchMesStr(tTerminal *pThis, DATA_STRING *data)
{
  int length, i=0, end_len;

  /// get filled size receiver buffer
  length = FIFO_GetFilledSize(&pThis->ReceiveQueue);

  /// if buffer not empty
  while (length > 0)
  {
    /// if not beginning message
    if (data->length > 0)
    {
      /// start with last saved value
      i = data->length;
      /// length available data to be saved
      end_len = data->length + length;
    }
    else 
    {
      /// begin message search
      pThis->vtable.pfnRead(&pThis->vtable, &data->str[0], 1);
      if (data->str[0] == '$')
      {
        /// next value in receive buffer
        i = 1;
        /// length available data to be saved
        end_len = length;
      }
      else length--;  /// begin message does not search
    }

    /// control length available data to be saved
    if (end_len > MAX_MES_STR_LENGTH)
      end_len = MAX_MES_STR_LENGTH;

    // save data
    if (i != 0)
    {
      while(i<end_len)
      {
        /// end message search
        pThis->vtable.pfnRead(&pThis->vtable, &data->str[i], 1);
        if (data->str[i] == '\r')
        {
          data->length = i;
          /// message search is successful
          return 1;
        }
        else i++;
      }
      /// control length saved message 
      if (end_len == MAX_MES_STR_LENGTH)
      {
        /// does not message
        data->length = 0;
      }
      else
      {
        /// saved part of message
        data->length = end_len; 
      }
      /// continue search message
      length -= end_len;
    }
  }
  /// does not message in receive buffer
  return 0;
}

#ifdef DELAYED_OPMGRINIT
extern int delayed_OPMgr_Init ;
extern int new_OrbStatic ;
extern uint8 new_Synchronization ;
#endif

//========================================================================
/// @brief     String message parser
///
/// @param[in] pMainMgr   - pointer to MainManager object
/// @param[in] pFramework - pointer to Framework object
//========================================================================
void MainMgr_ParserStr(tMainManager *pMainMgr, tFramework *pFramework,
                       tNavTask     *pNavTask, tOPManager *pOPMgr)
{
  extern const tNavTask NavTaskState;
  char *command_code, *command = pMainMgr->MESSAGE.dataStr.str;
  static char str_ack[128];
  tTerminal *pThis = &pFramework->DebugTerminal;
  char simbol;
  uint32 PeriodTime = pNavTask->PeriodTime;
  int i;

  for(i=0;i<128;i++)
    str_ack[i] = 0;
  
  /// Check command string goodness
  if (strncmp(command,"$PSPI",5)) return;  // string is bad
  
  /// Command mnemonics is three letters at 7..9 position in the string
  simbol = command[5];
  command += 7;

  /// Decode command
  switch (simbol)
  {
  case 'S':
    {
      if (!strncmp(command,"DEF",3))
      {
        pMainMgr->Message = 0x00;
        strcpy(str_ack, "$PSPIR,DEF\r\n");
        pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 12);
      }
      if (!strncmp(command,"RST",3))
      {
#ifdef NEW_TMTC
	reset_type = 1 ;
#endif		  
        pMainMgr->flags.RESET = 1;
        pMainMgr->Message     = 0x00;
      }
      if (!strncmp(command,"BIT",3))
      {
        pMainMgr->flags.BIT = 1;
        pMainMgr->Message   = 0x00;
      }
      if (!strncmp(command,"OPMODE",6))
      {
        int8 OPmode = 0xf;
        command_code = command + 7;
        if (!strncmp(command_code, "0", 1))
          OPmode = 0;
        if (!strncmp(command_code, "1", 1))
          OPmode = 1;
        if (OPmode != 0xf)
        {
          /// OP initialization: no synchronization OP and navigation task solution
          if ((pMainMgr->config.OPmode == 0) && (OPmode == 1))
          {
#ifdef DELAYED_OPMGRINIT
			delayed_OPMgr_Init = 1 ;
			new_OrbStatic = pMainMgr->config_new.OrbStatic ;
			new_Synchronization = NO_SYNC ;
#else		
            OPMgr_Init(pOPMgr, pMainMgr->config_new.OrbStatic);
            pNavTask->Synchronization = NO_SYNC;
#endif			
          }
          /// OP reset: synchronization navigation task solution exist
          if ((pMainMgr->config.OPmode == 1) && (OPmode == 0))
          {
#ifdef DELAYED_OPMGRINIT
			delayed_OPMgr_Init = 1 ;
			new_OrbStatic = pMainMgr->config_new.OrbStatic ;
			new_Synchronization = SYNC_OK ;
#else		
            OPMgr_Init(pOPMgr, pMainMgr->config_new.OrbStatic);
            pNavTask->Synchronization = SYNC_OK;
#endif			
          }
          strcpy(str_ack, "$PSPIR,OPMODE\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 15);
          sprintf(pMainMgr->TestStr.str, "CONFIG: Nav_&OP:%d, modelOS:%d, I:%d, T:%d\r\n", OPmode, pMainMgr->config.OrbStatic, 
                  pMainMgr->config.IonosphereModel, pMainMgr->config.TroposphereModel);
          pThis->vtable.pfnWrite(&pThis->vtable, pMainMgr->TestStr.str, strlen(pMainMgr->TestStr.str));  
          pMainMgr->config.OPmode       = OPmode;
          pMainMgr->config.UpdateConfig = 0;
        }
      }      
      if (!strncmp(command, "VERSION", 7))
      {
        if (pFramework->version < 10)
        {
          if (pFramework->subversion < 10)
            sprintf(str_ack, "Verison: 0%d.0%d, flags: 0x%x\r\n", pFramework->version, pFramework->subversion, pFramework->BuiltInTest.statusFlags);
          else
            sprintf(str_ack, "Verison: 0%d.%d, flags: 0x%x\r\n", pFramework->version, pFramework->subversion, pFramework->BuiltInTest.statusFlags);
        }
        else
        {
          if (pFramework->subversion < 10)
            sprintf(str_ack, "Verison: %d.0%d, flags: 0x%x\r\n", pFramework->version, pFramework->subversion, pFramework->BuiltInTest.statusFlags);
          else
            sprintf(str_ack, "Verison: %d.%d, flags: 0x%x\r\n", pFramework->version, pFramework->subversion, pFramework->BuiltInTest.statusFlags);
        }
        pThis->vtable.pfnWrite(&pThis->vtable, str_ack, strlen(str_ack));
        sprintf(str_ack, "CONFIG: Nav_&OP:%d, modelOS:%d, I:%d, T:%d\r\n", pMainMgr->config.OPmode, pMainMgr->config.OrbStatic, 
          pMainMgr->config.IonosphereModel, pMainMgr->config.TroposphereModel);
        pThis->vtable.pfnWrite(&pThis->vtable, str_ack, strlen(str_ack));        
      }
      if (!strncmp(command,"ANT",3))
      {
        command_code = command + 4;
        if (!strncmp(command_code, "ON", 2))
        {
         // *(volatile uint8 *)0x68000004 &= (0xFF - ANTENNA) ;
          strcpy(str_ack, "$PSPIR,ANT\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 12);                    
        }
        else if (!strncmp(command_code, "OFF", 3))
        {
        //  *(volatile uint8 *)0x68000004 |= ANTENNA;
          strcpy(str_ack, "$PSPIR,ANT\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 12);                    
        }
      }      
      if (!strncmp(command,"PER",3))
      {
        int len;
        char time;
        command_code = command + 4;
        len = command_code - pMainMgr->MESSAGE.dataStr.str;
        len = pMainMgr->MESSAGE.dataStr.length - len;
        strncpy(&time, command_code, len);
        pMainMgr->NewPeriodTime       = atoi(&time);
        pMainMgr->NewTime             = fg_ON;
        pMainMgr->config.NavSolRate   = pMainMgr->NewPeriodTime;
        pMainMgr->config.UpdateConfig = 0;
        strcpy(str_ack, "$PSPIR,PER\r\n");
        pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 12);
      }
      if (!strncmp(command,"NME",3))
      {
        int on_off = 0;
        command_code = command + 4;
        if (!strncmp(command_code,"SAT",3)) // Message bit 0
        {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= SAT_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - SAT_MES);
            on_off = 2;
          }
        }
        if (!strncmp(command_code,"POS,",4)) // Message bit 1 
        {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= POS_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - POS_MES);
            on_off = 2;
          }
        }
        if (!strncmp(command_code,"DOP",3)) // Message bit 2
        {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= DOP_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - DOP_MES);
            on_off = 2;
          }
        }
        if (!strncmp(command_code, "RAW", 3)) // Message bit 3
        {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= RAW_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - RAW_MES);
            on_off = 2;
          }
        }
        if (!strncmp(command_code, "OPP", 3)) // Message bit 4
		    {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= OPP_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - OPP_MES);
            on_off = 2;
          }		  
		    } 
        if (!strncmp(command_code, "SQE", 3)) // Message bit 5
		    {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= SQE_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - SQE_MES);
            on_off = 2;
          }		  
		    } 
        if (!strncmp(command_code, "ATM", 3)) // Message bit 6
		    {
          command_code += 4;
          if (!strncmp(command_code,"ON",2))
          {
            pMainMgr->Message |= ATM_MES;
            on_off = 1;
          }
          if (!strncmp(command_code,"OFF",3))
          {
            pMainMgr->Message &= (0xFFFF - ATM_MES);
            on_off = 2;
          }		  
		    } 
        if (on_off == 1)
        {
          strcpy(str_ack, "$PSPIR,NME,ON\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 15);
        }
        if (on_off == 2)
        {
          strcpy(str_ack, "$PSPIR,NME,OFF\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 16);
        }
      }
      if (!strncmp(command,"BIN",3))
      {
        pMainMgr->BinStrFlag = fg_ON;
        pMainMgr->Message    = 0x0;
        FIFO_Init(&pFramework->DebugTerminal.ReceiveQueue,
          (tFifoItem *)pFramework->DebugTerminal.m_ReadBuffer,
          sizeof(pFramework->DebugTerminal.m_ReadBuffer));

        FIFO_Init(&pFramework->DebugTerminal.TransmitQueue,
          (tFifoItem *)pFramework->DebugTerminal.m_WriteBuffer,
          sizeof(pFramework->DebugTerminal.m_WriteBuffer));
      }
    }
    break;
  case 'Q':
    {
      if (!strncmp(command, "ANT", 3))
      {
        uint8 antenna = *(volatile uint8 *)0x68000004;

        if ((antenna&ANTENNA) == ANTENNA)
        {
          strcpy(str_ack, "$PSPIR,ANT,OFF\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 16);
        }
        else
        {
          strcpy(str_ack, "$PSPIR,ANT,ON\r\n");
          pThis->vtable.pfnWrite(&pThis->vtable, str_ack, 15);
        }
      }
      if (!strncmp(command, "PER", 3))
      {
        sprintf(str_ack, "$PSPIR,PER,%d\r\n", PeriodTime);
        pThis->vtable.pfnWrite(&pThis->vtable, str_ack, strlen(str_ack));
      }
    }
    break;
  }
}
