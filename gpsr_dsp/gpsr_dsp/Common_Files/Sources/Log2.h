/*****************************************************************************
*    Copyright (c) 2002 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Calculates rough estimation of Log2(x)
 *    @file                   Log2.h
 *    @author                 P. Martynovich, A. Nazarov
 *    @version                1.0
 */
/*****************************************************************************/
#ifndef _LOG2_H__
#define _LOG2_H__ 1

#include "comtypes.h"
#ifndef _TMS320C54X_ENABLE
  #define Log2      Log2Cversion
#else
  #define Log2      Log2C54
#endif

//=========================================================================
/// @brief      Calculates rough estimation of Log2(x) returns 0x8000
///             on negative of zero argument or number in Q9 format
///
/// @param[in]  AccA - int32 input
/// @return     int16 Log2 of input
//=========================================================================
EXTERN_C int16 Log2 (int32 x);

#endif //_LOG2_H__
