/*************************************************************************************
*    Copyright (c) 2005 Spirit Corp.
**************************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Report generator
*    @file                   Report.h
*    @author                 M. Zhokhova
*    @date                   25.06.2005
*    @version                1.0
*/
/*************************************************************************************/

#ifndef _REPORT_H
#define _REPORT_H

#include "../OP/OPManager.h"
#include "satellite.h"


// for target
#ifdef USE_DSPBIOS
//=====================================================================================
///  Send status string to UART
///
///  @param[in]  sat_id    - satelite number
///  @param[in]  pGPSchan  - pointer to GPS channel structure
///  @param[in]  ps_delay  - pseudodelay
//=====================================================================================
EXTERN_C void Stat2Uart(int8 sat_id, const tGPSchan *pGPSchan, const double ps_delay);

// for PC model
#else

EXTERN_C void SaveInFile(FILE *f, TEMP_EPH_GPS *pTmpGps, uint8 sv_num);

EXTERN_C void printAllRaw(FILE *logFile, tSatelliteGPS *pSat, tDecoderCommon *common, uint32 ms_count);

EXTERN_C void ReadEphFromFile(tDecoderCommon *pDecCommon);

//=====================================================================================
///  Print statistics string to specified file
///
///  @param[in]  logFile   - pointer to the file structure
///  @param[in]  pGPSchan  - pointer to GPS channel structure
///  @param[in]  decoder   - pointer to decoder structure
///  @param[in]  ms_count  - system time in ms
//=====================================================================================
EXTERN_C void printStatistics(FILE            *logFile, 
                              const tGPSchan  *pGPSchan, 
                              const tDecoder  *decoder, 
                              uint32           ms_count);
//EXTERN_C void printStatistics(FILE* logFile, const tGPSchan* pGPSchan, tRawDataFix *raw_data_fix, uint32 ms_count);

//=====================================================================================
///  Print DLL status string to specified file
///
///  @param[in]  logFile   - pointer to the file structure
///  @param[in]  pGPSchan  - pointer to GPS channel structure
///  @param[in]  decoder   - pointer to decoder structure
///  @param[in]  ms_count  - system time in ms
//=====================================================================================
EXTERN_C void printDll(FILE         *logFile, 
                       tGPSchan     *pGPSchan, 
                       tDecoder     *decoder, 
                       const uint32  ms_count);

//=====================================================================================
///  Print PLL status string to specified file (for floating point PLL-DLL)
///
///  @param[in]  logFile   - pointer to the file structure
///  @param[in]  pGPSchan  - pointer to GPS channel structure
//=====================================================================================
EXTERN_C void printPll(FILE* logFile,const tGPSchan *pGPSchan);

//=====================================================================================
///  Print raw data to specified file (for fix point PLL-DLL)
///
///  @param[in]  logFile          - pointer to the file structure
///  @param[in]  raw_data_fix     - pointer to raw data structure
///  @param[in]  dopplerPrimeQ10  - doppler prime in Q10
///  @param[in]  ms_count         - system time in ms
//=====================================================================================
EXTERN_C void printRaw(FILE         *logFile, 
                       tRawDataFix  *raw_data_fix, 
                       int32         dopplerPrimeQ10, 
                       int32         dopplerQ15,
                       int32         CN_dBHzQ7,
                       tLongFract48   delay,
                       tLongFract48   tauQ,
                       uint32        ms_count);

EXTERN_C void ReadEphFromFile(tDecoderCommon *pDecCommon);

EXTERN_C void printAllRaw(FILE *logFile, tSatelliteGPS *pSat, tDecoderCommon *common, uint32 ms_count);

EXTERN_C void printGPMatrix(tOPManager *pOPMgr);

#endif // USE_DSPBIOS

#endif // !_REPORT_H
