/*****************************************************************************
*    Copyright (c) 2001-2002 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Multiply a 32 bit number and a 32 bit number
 *    @file                   L_mpy_ll.h
 *    @author                 A.Nazarov
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef _L_MPY_LL_H___
#define _L_MPY_LL_H___  1
#include "comtypes.h"

#ifndef _TMS320C54X_ENABLE
  // C-language routines
#define L_mpy_ll      L_mpy_llCversion
#else
  // ported to 320C54x routines
#define L_mpy_ll      L_mpy_llC54
#endif

//=========================================================================
/// @brief      Multiply a 32 bit number (L_var1) and a 32 bit number
///             (L_var2), and return a 32 bit result. 
///             Precision conversion: Q result = Q(x+y-31)
///
/// @implementation: Performs a 31x31 bit multiply, Complexity=24 Ops.
///                  Let x1x0, or y1y0, be the two constituent halves
///                  of a 32 bit number.  This function performs the
///                  following:
///                  low = ((x0 >> 1)*(y0 >> 1)) >> 16     (low * low)
///                  mid1 = [(x1 * (y0 >> 1)) >> 1 ]       (high * low)
///                  mid2 = [(y1 * (x0 >> 1)) >> 1]        (high * low)
///                  mid =  (mid1 + low + mid2) >> 14      (sum so far)
///                  output = (y1*x1) + mid                (high * high)
///
/// @param[in]  L_var1 - int32 multiplier
/// @param[in]  L_var2 - int32 multiplier
/// @return     int32 product
//=========================================================================
EXTERN_C int32 L_mpy_ll(int32 x, int32 y);

#endif

