/*****************************************************************************
 *   Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  C/A codes
 *   @file                   caCode.c
 *   @author                 A. Nazarov, V. Chepelev
 *   @date                   21.11.2006
 *   @version                2.0
 */
/*****************************************************************************/
#ifndef CACODE_H__
#define CACODE_H__

#include "comtypes.h"
#include "DataForm.h"

#define CODE_LENGTH  (1023)  ///< C/A code length

//===================================================================
/// Get C/A code in form of sequence of +/-1 for GPS
///
/// @param[in] code[1024]  - array for output code
/// @param[in] SVnum       - satellite number (1...32)
//====================================================================
EXTERN_C void getCAcode (int8* restrict code, int SVnum);

#endif// CACODE_H__
