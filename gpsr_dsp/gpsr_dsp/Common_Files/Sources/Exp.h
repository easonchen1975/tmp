/*****************************************************************************
 *    Copyright (c) 2000 Spirit Corp.
 *****************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Getting exponent of number
 *    @file                   Exp.h
 *    @author                 Alex Nazarov
 *    @date                   10.03.2000
 *    @version                1.0
 */
/*****************************************************************************/
#ifndef __Exp_H
#define __Exp_H

#include "comtypes.h"
 
 #if defined(_TMS320C6X)
  #define PORTING_EXP      1
  #define Exp             ExpC64
  #define Exp0            Exp0C64
#else
  #define PORTING_EXP      0
  
  #define Exp             ExpCversion
  #define Exp0            Exp0Cversion
  
//=============================================
/// Getting exponent of number
///
/// @param[in]  Value - integer value
/// @return     exponent
//=============================================
EXTERN_C  int Exp (int32 Value);
  
//===============================================================
/// Getting exponent of number. 
/// If argument is zero, returns 0x1F (like in C6xx)
///
/// @param[in]  Value - integer value
/// @return     exponent
//================================================================
EXTERN_C  int Exp0 (int32 Value);
  
#endif   //defined(_TMS320C6X)

#if PORTING_EXP==1
  // Use inlined version of Exp() and Exp0()
  #include "Exp.c"
#endif // PORTING_EXP

#endif //  __EXP_H
