/**************************************************************************************
     Copyright (c) 2002 Spirit Corp.
***************************************************************************************
*
*    @project      GPS Receiver
*    @brief        Multiplies two Q31 fractional numbers
*    @file         Mul31.c
*    @author       Andrey Bykov
*    @date         21.11.2005
*    @version      1.0
*
**************************************************************************************/

#include "Mul31.h"

#ifndef   __MUL31_C 
#define   __MUL31_C

//=====================================================================================
/// Multiplies two Q31 fractional numbers and returns product in Q31 format
///
/// @param[in] x - multiplier 1
/// @param[in] y - multiplier 2
/// @return    product in Q31
//=====================================================================================
#if PORTING_MUL31 == 0
int32 Mul31(int32 x, int32 y)
{
   int32 c, p1, p2, p3, p4;
   
   p1 = (x & 0x0000ffffL) * (y >> 16);
   p2 = (y & 0x0000ffffL) * (x >> 16);
      
   c = ((p1 & 0x00007fffL) + (p2 & 0x00007fffL)) >> 15;
    
   if (y > 0)
     p1 = ((uint32)p1) >> 15;
   else
     p1 = p1 >> 15;
   if (x > 0)
     p2 = ((uint32)p2) >> 15;
   else
     p2 = p2 >> 15;
   
   p4 = ((x >> 16) * (y >> 16)) << 1;
   
   p3 = c + p1 + p2 + p4;
  
   return (p3);
}

#endif  // PORTING_MUL31

//=====================================================================================
/// Inline function for C64xx C-compiler
/// multiplies two Q31 fractional numbers and returns product in Q31 format
///
/// @param[in] x - multiplier 1
/// @param[in] y - multiplier 2
/// @return    product in Q31
//=====================================================================================
#if ((PORTING_MUL31==1) && defined(_TMS320C6400))
inline int32 Mul31C64(int32 x, int32 y)
{
  int32  p1,p2, p3;
  long  Acc;

  p1 = _mpyluhs(x,y);   // Xlow  * Yhigh
  p2 = _mpyhslu(x,y);   // Xhigh * Ylow 

  // Acc = p1 + p2;   
  Acc = _lsadd(p1, p2);   // Xlow  * Yhigh + Xhigh * Ylow
   
  p3 = (Acc >> 15) + (_mpyh(x,y)<<1);  // Acc>>15 + Xhigh*Yhigh <<1
  return p3;
}
#endif  //  ((PORTING_MUL31==1) && defined(_TMS320C6400))

#endif  // __MUL31_C
