/*****************************************************************************
*    Copyright (c) 2005 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  control functions
*    @file                   Control.h
*    @author                 A. Baloyan
*    @date                   25.06.2005
*    @version                1.0
******************************************************************************/
#ifndef _CONTROL_H_
#define _CONTROL_H_

#include "comtypes.h"

#define rad_to_degree       (float64)(180.0/M_PI)                 ///< constant for radians to degrees convertion
#define degree_to_rad       (float64)(M_PI/180.0)                 ///< constant for degrees to radians convertion
#define w_Earth             (float64)(7.2921151467e-5)            ///< angular velocity of Earth rotation [rad/s]
#define a_Earth             (float64)(6378136.)                   ///< Earth's semi-major axis [m]
#define e_Earth             (float64)(0.081819221455523)          ///< Earth's eccentricity   [-]
#define e2_Earth            (float64)(6.6943849995879152267e-3)   ///< e_Earth*e_Earth
#define e2_Sqrt_Earth       (float64)(9.9664718682210310284e-1)   ///< sqrt(1.0 - e2)
#define l_e2_Earth          (float64)(1.0067395018194728899)      ///< 1.0/(1.0 - e2)
#define a_e2_Sqrt_Earth     (float64)(6.3995926385316408599e6)    ///< a_Earth/e2_sqrt
#define l_a2_Earth          (float64)(2.4581730284595096476e-14)  ///< 1.0/(a_Earth*a_Earth),
#define a2_e2_Earth         (float64)(2.4747398900573916971e-14)  ///< a2_Earth/(1 - e_Earth*e_Earth)
#define mu_Earth            (float64)(3.986005e14)                ///< [m^3/s^2]value of Earth's universal gravitational parameters
#define cLight              (float64)299792458.                   ///< Light speed in free space [m/s]
#define l_cLight            (float64)(1./cLight)                  ///< 1 devided by the light speed [s/m]
#define f0_GPS              (float64)1575.42e6                    ///< Carrier frequency of GPS signals [Hz]

//===================================================================================
/// @brief      Function calculates x modulo y, the remainder zfof x/y. 
/// @param[in]  X - number type float64
/// @param[in]  Y - number type float64
/// @return     the remainder f, where x = ay + f for some integer a, and 0 < f < y 
//===================================================================================
EXTERN_C float64 Fmodulo(float64 X, float64 Y);


#endif //_CONTROL_H_
