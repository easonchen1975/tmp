/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Fast search algorithm
 *   @file                   Fsearch.c
 *   @author                 B. Oblakov
 *   @date                   16.06.2006
 *   @version                1.0
 */
/*****************************************************************************/

#ifndef FSEARCH_H__
#define FSEARCH_H__

#include "comtypes.h"
#include "DataForm.h"
#include "caCode.h"

#define FFT4096                     (4096)        ///< then 4096-points FFT
#define FFT4096_HALF                (FFT4096>>1)  ///< 64/size(tComplex16) = 2 then (4096/2)
#define FFT8                        (8)           ///< then 8-points FFT
#define SIGNAL_SCALE                (16)          ///< signal scale factor for GPS
#define REPLICA_SCALE               (16)          ///< replica scale factor for GPS
#define FFT_SCALE                   (4)           ///< FFT scale shift for GPS
#define CORR_SCALE                  (8)           ///< correlation scale factor
#define THRESH_SCALE                (3)           ///< threshold scale factor
#ifdef USE_DSPBIOS
#define SEARCH_THRESH       (double)(16.0)        ///< search threshold for GPS
#else
#define SEARCH_THRESH       (double)(30.0)        ///< search threshold for GPS
#endif
#define FFT8_RES           (float64)(0.125)       ///< 1/FFT8 length: FFT8 resolution
#define ADDITIONAL_POSITION         (60)          ///< additional position for search

/// Fast search algorithm structure
typedef struct
{
  tComplex16Aligned    cumCorr[(CODE_LENGTH<<1)][FFT8];           ///< cumulative correlations
  tComplex16Aligned64  Tmp_T[FFT4096_HALF];                       ///< buffer for temporary results
  tComplex16Aligned64  Repl_F[FFT4096_HALF + FSEARCH_SHIFT_MAX];  ///< buffer for replica
  tComplex16Aligned64  Sign_F[FFT4096_HALF];                      ///< signal buffer
  tComplex16Aligned64  wFFT[FFT4096_HALF];                        ///< twiddle coefficients
}
tFsearch;

/// Fast search algorithm structure
typedef struct
{
  int     isFound;  ///< 1 if satellite is found here
  int32   freqEst;  ///< frequency estimation, in kHz, Q11
  int     position; ///< position inside the epoch in samples at double chip rate
}
tFSearchRes;

//==================================================================================
/// Interface function for signal transformation to the frequency domain
///
/// @param[in]   pFsearch     - pointer to fast search algorithm structure
/// @param[out]  packedSignal - pointer to packed signal for 8 sequential epochs 
///                             at the double chip rate
//==================================================================================
EXTERN_C void FsearchSignalFFT(tFsearch *pFsearch, int32* packedSignal);

//==================================================================================
///  Function for C/A code replica FFT calculation
///
/// @param[in]   pFsearch - pointer to fast search algorithm structure
/// @param[in]   SVnum    - satellite PRN number
//==================================================================================
EXTERN_C void FsearchInit(tFsearch *pFsearch, int SVnum);

//==================================================================================
/// Interface function carries out satellite fast search in 8 given epochs
///
/// @param[in]   pFsearch     - pointer to fast search algorithm structure
/// @param[out]  pResult      - search results
/// @param[out]  nShift_min   - left boundary of Doppler range
/// @param[out]  nShift_max   - right boundary of Doppler range
/// @param[out]  packedSignal - pointer to buffer which consists Fourier transformed
///                             signal epochs
//==================================================================================
EXTERN_C void FsearchProcess(tFsearch       *pFsearch,
                             tFSearchRes    *pResult,
                             int16           nShift_min,
                             int16           nShift_max,
                             int32          *packedSignal);



#endif  // #define FSEARCH_H__
