/*****************************************************************************
*    Copyright (c) 2001-2002 Spirit Corp.
******************************************************************************
*
*    Project: ............   HF modem MIL-STD-188-110A
*    Title: ..............   Debug output
*    File name: ..........   Debug.c
*    Authors: ............   Alex G. Nazarov
*    Revision ............   1.1
*
*    Subroutines:
*    DEBUGprintf    print to the file appending to the end
*    DEBUGremove    remove given file
*    DEBUGprintfSP  decode signal points from complex numbers 
*                   and print them into the file
*
*    NOTE: code is present only in debug version when _DEBUG is defined
*          otherwise all functions are substrituted by void
*****************************************************************************/

//#include "Debug.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include "CachedFile.h"

//  forward static declarations
//-----------------------------
//static double  convertToSp (int* sp, const tComplex* y, int M);


/****************************************************************************
  Description: print to the file appending to the end

  Input:
  file      filename
  format    format string (see printf() function)
  ...       variable list of parameters
****************************************************************************/
//#ifdef _DEBUG
#if 1

void DEBUGprintf (const char* file, const char* format, ...)
{ 
  FILE *f;
  va_list arg_list;
  va_start (arg_list, format);
  f = CachedFileOpen (file);
  if (f)
  {
    if (sizeof(int16)==1)
    {
      char str[256];
      vsprintf (str,format,arg_list);
      fwrite (str, 1, strlen(str),f);
    }
    else
    {
        vfprintf (f, format, arg_list);
    }
    CachedFileClose (f);
  }
  va_end (arg_list);
}

/****************************************************************************
  Description: print to the file complex array appending to the end

  Input:
  file      filename
  format    format string (see printf() function)
  ...       variable list of parameters
****************************************************************************/
void DEBUGprintfComplex (const char* file, const char* format, const tComplex* array, int size)
{
  int i;
  for (i=0; i<size; i++)
    DEBUGprintf (file, format, array[i].re, array[i].im);
}


/****************************************************************************
  Description: remove debug file

  Input:
  file      filename
****************************************************************************/
void DEBUGremove (const char* file)
{
  CachedFileRemove (file);
}

/****************************************************************************
  Description: write unformatted data to the file appending to the end

  Input:
  file      filename
  variable  variable to be store
  size_t    size
****************************************************************************/
void DEBUGwrite  (const char* file, const void* variable, size_t size)
{
  // store in a byte array
  FILE *f;
  f = CachedFileOpen (file);
  if (f)
  {
        // on target system where sizeof(int16)==1 : convert to bytes
    if (sizeof(int16)==1)
    {
      char buffer[100],*pbytes;
      const int* data = (const int*)variable;
      while (size!=0)
      {
        int i,N;
        N = sizeof(buffer)/2;
        if (N<(int)size) N=size;
        size-=N;
        for (i=N,pbytes = buffer; i>0; i--)
        {
            *pbytes++ = (*data) & 0xFF;
            *pbytes++ = ((*data++)>>8) & 0xFF;
        }
        fwrite (buffer, 2, N, f);
      }
    }
    else
    {       // normal operation
      fwrite (variable, 1, size, f);
    }
    CachedFileClose (f);
  }
}

void DEBUGprintfHardDecisions (const char* file, const char* format, 
                                const tComplex* frame, int N,
                                const tComplex* pConstellation, 
                                int  ConstellationSize)
{
  for (;N>0; N--,frame++)
  {
    double  dist [64];
    int i,index;
    double  mindist;
    const tComplex  *z = pConstellation;
    tComplex  x;
    ASSERT (ConstellationSize <= sizeof(dist)/sizeof(dist[0]));

    x = *frame; 
      // compute distances
    for (i=0; i<ConstellationSize; i++,z++)
    { 
      double dx,dy;
      dx = z->re - x.re;
      dy = z->im - x.im;
      dist[i] = (dx*dx + dy*dy);
    }
      // select minimum
    mindist = dist[0];
    index = 0;
    for (i=1; i<ConstellationSize; i++)
    {
      if (dist[i]<mindist)
      {
        mindist = dist[i];
        index = i;
      }
    }
    DEBUGprintf (file, format, index);
  }
  //DEBUGprintf (file, "\n");
}

#endif  // #ifdef _DEBUG
