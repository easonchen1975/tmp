/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Main manager
 *   @file                   MainManager.c
 *   @author                 M. Zhokhova
 *   @date                   14.02.2006
 *   @version                1.0
 */
/*****************************************************************************/

#include "MainManager.h"
#include "Control.h"
#include "DataProtocol.h"
#include "comfuncs.h"
#include "LongFract.h"

#define Q21_1000       (0x7D000000)  ///< 1000 in Q21
#define Q5_1000        (0x7D00)      ///< 1000 in Q5
#define l_PI_180_10Q5  (0x479E)      ///< (180/pi)*10
#define C_LIGHT_Q0     (299792458)   ///< cLight
#define CLIGHT_l_F0Q23 (0x5F259711)  ///< (cLight/f0_GPS)*1e+3


extern uint32 can_or_uart, pre_can_or_uart;
uint16 TC_count, TC_success_count;

//==================================================================================
/// Convert fix-point raw data to message format
///
/// @param[in]  sat            - pointer to part message
/// @param[in]  pSat - pointer to current satellite
//==================================================================================
static void RawDataFixToMessage(tSatRM *sat, tSatelliteGPS *pSat);


// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC

static void clear_rx_fifo(tTerminal *pThis)
{
	int length, read_length;
	char tmp[16] ;

	/// get filled size receiver buffer
	length = FIFO_GetFilledSize(&pThis->ReceiveQueue);

	while ( length > 0 )
	{
		if ( length > sizeof(tmp) )
			read_length = sizeof(tmp) ;
		else
			read_length = length ;
			
		FIFO_GetBuffer(&pThis->ReceiveQueue, (tFifoItem *)tmp, read_length);
		length -= read_length ;
	}
	
	return ;
}

#if 0
static void clear_tx_fifo(tTerminal *pThis)
{
	int length, read_length;
	char tmp[16] ;

	/// get filled size receiver buffer
	length = FIFO_GetFilledSize(&pThis->TransmitQueue);

	while ( length > 0 )
	{
		if ( length > sizeof(tmp) )
			read_length = sizeof(tmp) ;
		else
			read_length = length ;
			
		FIFO_GetBuffer(&pThis->TransmitQueue, (tFifoItem *)tmp, read_length);
		length -= read_length ;
	}
	
	return ;
}
#endif

static int search_n_process_new_tc(tTerminal *pThis, int *p_mode, tMainManager *pMainMgr)
{
	int length;
	int i ;
	uint8 *p_frame ;
	uint8 chksum, type_id ;
	Tc_Frame_Gpsr_Icd_Mode *p_gpsr_icd_tc ;
	uint8 tc_tmp[128] ;
	static int delayed_soft_reset = 0 ;

	if ( delayed_soft_reset )
	{
		if ( ++ delayed_soft_reset > 100 )	// delay 100 ms to complete ACK sending
		{
			delayed_soft_reset = 0 ;
			pMainMgr->flags.RESET = 1;
			return 0;
		}
	}
	
	/// get filled size receiver buffer
	length = FIFO_GetFilledSize(&pThis->ReceiveQueue);

	if (length < TC_GPSR_ICD_MODE_FRAME_LENGTH)
		return 0;

	// snoop the first 6 bytes to check if new TC arrives
	FIFO_CopyBuffer(&pThis->ReceiveQueue, (tFifoItem *)&tc_tmp[0], 6);

#if 1	
	if ( *p_mode == DETAIL_MODE )
	{
		if ( *(unsigned int *)&tc_tmp[0] != nspo_sof )
			return 0 ;
	}		
	else	// BRIEF mode here
	{
		for ( i = 0 ; i <= 2 ; i ++ )
		{
			if ( ( tc_tmp[i+0] == START_OF_FRAME_0 ) && (tc_tmp[i+1] == START_OF_FRAME_1) && (tc_tmp[i+2] == START_OF_FRAME_2) && (tc_tmp[i+3] == START_OF_FRAME_3) )
				break ;
		}
		
		if ( i != 0 )
		{
			if ( i <= 2 )
			{
				// read out leading garbage chars, and re-do snoop 6 chars
				FIFO_GetBuffer(&pThis->ReceiveQueue, (tFifoItem *)&tc_tmp[0], i);
				FIFO_CopyBuffer(&pThis->ReceiveQueue, (tFifoItem *)&tc_tmp[0], 6);
			}
			else
			{
				// read out leading garbage chars
				FIFO_GetBuffer(&pThis->ReceiveQueue, (tFifoItem *)&tc_tmp[0], i);
				return 0 ;
			}
		}
	}
#else	
	if ( ( tc_tmp[0] != START_OF_FRAME_0 ) || (tc_tmp[1] != START_OF_FRAME_1) || (tc_tmp[2] != START_OF_FRAME_2) || (tc_tmp[3] != START_OF_FRAME_3) )
		return 0 ;
#endif	

	type_id = (tc_tmp[4] & 0xfc) >> 2 ;
	
	switch ( type_id )
	{
		case TC_GPSR_ICD_MODE_TYPE_ID:
			length = TC_GPSR_ICD_MODE_FRAME_LENGTH ;
			break ;
		case TC_GPSR_CONFIG_1_TYPE_ID:
			length = TC_GPSR_CONFIG_1_FRAME_LENGTH ;
			break ;
		case TC_GPSR_CONFIG_2_TYPE_ID:
			length = TC_GPSR_CONFIG_2_FRAME_LENGTH ;
			break ;
		case TC_GPSR_CONFIG_3_TYPE_ID:
			length = TC_GPSR_CONFIG_3_FRAME_LENGTH ;
			break ;
		case TC_WRITE_RFFE_REG_TYPE_ID:
			length = TC_WRITE_RFFE_REG_FRAME_LENGTH ;
			break ;
		case TC_GPSR_SOFT_RESET_TYPE_ID:
			length = TC_GPSR_SOFT_RESET_FRAME_LENGTH ;
			break ;
		case TC_GPSR_BOOT_CONFIG_TYPE_ID:
			length = TC_GPSR_BOOT_CONFIG_FRAME_LENGTH ;
			break ;
		case TC_GPSR_TLM_CONFIG_TYPE_ID:
			length = TC_GPSR_TLM_CONFIG_FRAME_LENGTH ;

			break ;
		case TC_GPSR_OUTPUT_MODE_SEL_ID:
			length = TC_GPSR_OUTPUT_MODE_SEL_FRAME_LENGTH ;

			break ;
		default:
			length = 0 ;
	}
	
	if ( length != ( (tc_tmp[4] & 0x03)*256+tc_tmp[5]) + TMTC_FRAME_OVERHEAD_LENGTH )
	{
		clear_rx_fifo(pThis);
		return 0 ;
	}
	
	// new TC arrival, get it from queue	
	if ( length > FIFO_GetFilledSize(&pThis->ReceiveQueue) )
		return 0;
	
	FIFO_GetBuffer(&pThis->ReceiveQueue, (tFifoItem *)&tc_tmp[0], length);

	TC_count++;

//#ifdef CAN_PORT_TMTC
#if 1
	switch ( type_id )
	{
		case TC_GPSR_CONFIG_1_TYPE_ID:
		case TC_GPSR_CONFIG_2_TYPE_ID:
		case TC_GPSR_CONFIG_3_TYPE_ID:
		case TC_GPSR_SOFT_RESET_TYPE_ID:
		case TC_GPSR_OUTPUT_MODE_SEL_ID:
			break;
		default:
			return 0;
	}
#endif
	
	// checksum
	chksum = 0 ;
	for ( i = 0 ; i < length ; i ++ )
		chksum += tc_tmp[i] ;
	
	// send ack
	gpsr_tc_ack_tm_frame.command_id = type_id ;
	gpsr_tc_ack_tm_frame.status = (chksum == 0) ? 0 : 1 ;
	
	p_frame = (uint8 *)&gpsr_tc_ack_tm_frame ;
	chksum = 0 ;
	for ( i = 0 ; i < TM_GPSR_TC_ACK_FRAME_LENGTH - 1 ; i ++ )
		chksum += p_frame[i] ;
	gpsr_tc_ack_tm_frame.chksum = - chksum ;
		
//#ifdef CAN_PORT_TMTC
	if(can_or_uart > OUTPUT_UART)
		pThis->vtable.pfnWriteBinary(&pThis->vtable, (uint8 *)&gpsr_tc_ack_tm_frame, TM_GPSR_TC_ACK_CAN_FRAME_LENGTH);
//#else
	else
		pThis->vtable.pfnWriteBinary(&pThis->vtable, (uint8 *)&gpsr_tc_ack_tm_frame, TM_GPSR_TC_ACK_FRAME_LENGTH);
//#endif
	
	if ( gpsr_tc_ack_tm_frame.status != 0 )	// error frame ?
		return 1 ;	

	// arrive here means TC command is correct
	TC_success_count++;

	if ( type_id == TC_GPSR_ICD_MODE_TYPE_ID )
	{
		p_gpsr_icd_tc = (Tc_Frame_Gpsr_Icd_Mode *)&tc_tmp[0] ;

		if ( p_gpsr_icd_tc->icd_mode == *p_mode )	// the same as current?
			return 1 ;
			
		if ( p_gpsr_icd_tc->icd_mode == DETAIL_MODE )
		{
			*p_mode = DETAIL_MODE ;
			pMainMgr->Message = (SAT_MES | POS_MES | OPP_MES);  /// SAT, POS, OPP  		
		}
		else
		{
			*p_mode = BRIEF_MODE ;
			pMainMgr->Message = 0 ;	// disabled in BRIEF mode  		
		}
		return 1 ;
	}
		
	if ( type_id == TC_GPSR_OUTPUT_MODE_SEL_ID )
	{
		p_gpsr_icd_tc = (Tc_Frame_Gpsr_Icd_Mode *)&tc_tmp[0] ;
		dbg_printf("Get CMD=%d \r\n", TC_GPSR_ICD_MODE_TYPE_ID);


		pre_can_or_uart = p_gpsr_icd_tc->icd_mode;
		can_or_uart = p_gpsr_icd_tc->icd_mode;

		if( 1 /*can_or_uart == OUTPUT_CAN*/){
		  FIFO_Init(
		    &pThis->TransmitQueue,
		    (tFifoItem *)pThis->m_WriteBuffer,
		    sizeof(pThis->m_WriteBuffer));
		}
		{
			//*p_mode = BRIEF_MODE ;
			pMainMgr->Message = 0 ;	// disabled in BRIEF mode  		
		}

		// select CAN1 or CAN2
		if( (can_or_uart==1) || (can_or_uart==2)){
			//int can_ch_sel = can_or_uart -1 ;

			int set_obc_can_ch(int can_ch );
			set_obc_can_ch(can_or_uart);
			//can_rx_config_sel(can_or_uart-1);
		}

		return 1 ;
	}
	if ( ( type_id == TC_GPSR_CONFIG_1_TYPE_ID ) || ( type_id == TC_GPSR_CONFIG_2_TYPE_ID ) || ( type_id == TC_GPSR_CONFIG_3_TYPE_ID ) )
	{
		process_gpsr_config_tc(pMainMgr, (void *)&tc_tmp[0], type_id) ;
		return 1 ;
	}
	
	if ( type_id == TC_WRITE_RFFE_REG_TYPE_ID )
	{
		process_write_rffe_reg_tc((Tc_Frame_Write_Rffe_Reg *)&tc_tmp[0]) ;
		return 1 ;
	}

	if ( type_id == TC_GPSR_SOFT_RESET_TYPE_ID )
	{
		if ( delayed_soft_reset == 0 )
		{
			reset_type = process_soft_reset_tc((Tc_Frame_Gpsr_Soft_Reset *)&tc_tmp[0]) ;
			if ( reset_type != 0 )
				delayed_soft_reset = 1 ;
		}	
		return 1 ;
	}
	
	if ( type_id == TC_GPSR_BOOT_CONFIG_TYPE_ID )
	{
		new_boot_config = ((Tc_Frame_Gpsr_Boot_Config *)&tc_tmp[0])->boot_config ;
		return 1 ;
	}

	if ( type_id == TC_GPSR_TLM_CONFIG_TYPE_ID )
	{
		uint8 tmp_tlm_config ;
		
		tmp_tlm_config = ((Tc_Frame_Gpsr_Tlm_Config *)&tc_tmp[0])->tlm_config ;
		if ( ( tmp_tlm_config == TLM_TYPE_NAV ) || ( tmp_tlm_config == TLM_TYPE_SCI ) || ( tmp_tlm_config == TLM_TYPE_S_NAV) )
			tlm_type = tmp_tlm_config ;
			
		return 1 ;
	}
	
	return 1 ;
}

#endif	// end  NEW_TMTC


//================================================================================
/// Main Manager initialization
///
/// @param[in]  pMainMgr  - pointer to instance
//================================================================================
void MainMgr_Init(tMainManager *pMainMgr)
{
  pMainMgr->PeriodRaw = pMainMgr->config.NavSolRate;

// Alex, 20140619, NEW TMTC ICD
#ifdef NEW_TMTC
{
	uint16 tmp_16 ;
	
	tmtc_mode = BRIEF_MODE ;
	tlm_type = TLM_TYPE_NAV ;
	
	nspo_sof = START_OF_FRAME_0 | ( START_OF_FRAME_1 << 8 ) | ( START_OF_FRAME_2 << 16 ) | ( START_OF_FRAME_3 << 24 ) ;
	
	gpsr_nav_tm_frame.header.sof[0] = gpsr_sci_tm_frame.header.sof[0] = gpsr_short_nav_tm_frame.header.sof[0] = START_OF_FRAME_0 ;
	gpsr_nav_tm_frame.header.sof[1] = gpsr_sci_tm_frame.header.sof[1] = gpsr_short_nav_tm_frame.header.sof[1] = START_OF_FRAME_1 ;
	gpsr_nav_tm_frame.header.sof[2] = gpsr_sci_tm_frame.header.sof[2] = gpsr_short_nav_tm_frame.header.sof[2] = START_OF_FRAME_2 ;
	gpsr_nav_tm_frame.header.sof[3] = gpsr_sci_tm_frame.header.sof[3] = gpsr_short_nav_tm_frame.header.sof[3] = START_OF_FRAME_3 ;
	
	gpsr_dbg_tm_frame.header.sof[0] = START_OF_FRAME_0 ;
	gpsr_dbg_tm_frame.header.sof[1] = START_OF_FRAME_1 ;
	gpsr_dbg_tm_frame.header.sof[2] = START_OF_FRAME_2 ;
	gpsr_dbg_tm_frame.header.sof[3] = START_OF_FRAME_3 ;
	
	tmp_16 = TM_GPSR_NAV_TLM_DATA_LENGTH | (TM_GPSR_NAV_TLM_TYPE_ID << 10);
	SET_SHORT_BE(&gpsr_nav_tm_frame.header.attribute[0], tmp_16) ;
	
	tmp_16 = TM_GPSR_SCI_TLM_DATA_LENGTH | (TM_GPSR_SCI_TLM_TYPE_ID << 10) ;
	SET_SHORT_BE(&gpsr_sci_tm_frame.header.attribute[0], tmp_16) ;
	
	tmp_16 = TM_GPSR_S_NAV_TLM_DATA_LENGTH | (TM_GPSR_S_NAV_TLM_TYPE_ID << 10);
	SET_SHORT_BE(&gpsr_short_nav_tm_frame.header.attribute[0], tmp_16) ;

	tmp_16 = TM_GPSR_DBG_MSG_DATA_LENGTH | (TM_GPSR_DBG_MSG_TYPE_ID << 10);
	SET_SHORT_BE(&gpsr_dbg_tm_frame.header.attribute[0], tmp_16) ;
	
	gpsr_tc_ack_tm_frame.header.sof[0] = START_OF_FRAME_0 ;
	gpsr_tc_ack_tm_frame.header.sof[1] = START_OF_FRAME_1 ;
	gpsr_tc_ack_tm_frame.header.sof[2] = START_OF_FRAME_2 ;
	gpsr_tc_ack_tm_frame.header.sof[3] = START_OF_FRAME_3 ;
	
	tmp_16 = TM_GPSR_TC_ACK_DATA_LENGTH | (TM_GPSR_TC_ACK_TYPE_ID << 10) ;
	SET_SHORT_BE(&gpsr_tc_ack_tm_frame.header.attribute[0], tmp_16) ;

	TC_count = 0;
	TC_success_count = 0;

}	
#endif	// end NEW_TMTC
  
}

volatile uint8 config_flag = 0;

#ifdef DELAYED_OPMGRINIT
int delayed_OPMgr_Init = 0 ;
int new_OrbStatic ;
uint8 new_Synchronization ;
#endif
//================================================================================
/// Main Manager control
///
/// @param[in]  pMainMgr   - pointer to Main Manager object
/// @param[in]  pFramework - pointer to Framework object
/// @param[in]  pSatMgr    - pointer to Satellite Manager object
/// @param[in]  pNavTask   - pointer to Navigation Task object
/// @param[in]  pOPMgr     - pointer to OP Manager object
//================================================================================
void MainMgr_Control(tMainManager      *pMainMgr,
                     tFramework        *pFramework,
                     tSatelliteManager *pSatMgr,
                     tNavTask          *pNavTask,
                     tOPManager        *pOPMgr)
{
  int i, OP_Init = 0;
  /// for target
  #ifdef USE_DSPBIOS
  
#ifdef EXT_SRAM
    if (* pFramework->DebugTerminal.FlagRead == TERMINAL_READ)
    {
      FIFO_PutBuffer(&pFramework->DebugTerminal.ReceiveQueue,
                     (tFifoItem *)pFramework->DebugTerminal.stringRead->str, 
                     pFramework->DebugTerminal.stringRead->length);
      * pFramework->DebugTerminal.FlagRead = TERMINAL_FREE;
#else
    if (pFramework->DebugTerminal.FlagRead == TERMINAL_READ)
    {
      FIFO_PutBuffer(&pFramework->DebugTerminal.ReceiveQueue,
                     (tFifoItem *)pFramework->DebugTerminal.stringRead.str, 
                     pFramework->DebugTerminal.stringRead.length);
      pFramework->DebugTerminal.FlagRead = TERMINAL_FREE;
#endif					 
    }
	
// Alex, 20140620, NEW TMTC ICD
#ifdef NEW_TMTC

	while ( 0 != search_n_process_new_tc(&pFramework->DebugTerminal, &tmtc_mode, pMainMgr) ) ;
	
	if ( tmtc_mode == BRIEF_MODE )
	{
		update_myUtc(pSatMgr) ;	// prepare UTC data
//		clear_rx_fifo(&pFramework->DebugTerminal);
	}
	else
#endif	// end NEW_TMTC
	{
		if (pMainMgr->BinStrFlag == fg_ON)
		{
		  int8 result=1;
		  // parser for binary
		  while (result != 0)
		  {
			result = MainMgr_SearchMesBin(&pFramework->DebugTerminal, &pMainMgr->MESSAGE.dataBin);
			if (result != 0)
			{
			  MainMgr_ParserBin(pMainMgr, pFramework, pSatMgr, pNavTask, pOPMgr);
			  pMainMgr->MESSAGE.dataBin.length = 0;
			}
		  }
		}
		else 
		{
		  // parser for string: test and debug message
		  if (FrameWork_SearchMesStr(&pFramework->DebugTerminal, &pMainMgr->MESSAGE.dataStr))
		  {
			MainMgr_ParserStr(pMainMgr, pFramework, pNavTask, pOPMgr);
			pMainMgr->MESSAGE.dataStr.length = 0;
		  }
		}
	}

    /// analyze clear ephemeris and almanac flags
    if (pMainMgr->flags.CLEAR == 1)
    {
      pMainMgr->flags.CLEAR = 0;
      pSatMgr->TopSystem.eph_set_flgs = 0;
      pSatMgr->TopSystem.alm_set_flgs = 0;
      for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
      {
        if (pSatMgr->gps_satellites[i].sv_num != SATELLITE_INVALID_ID)
        {
          pSatMgr->gps_satellites[i].status &= 0x3;  /// clear ephemeris and almanac flags
        }
      }
    }
    /// new configuration setup analyze
    if (pMainMgr->NewConfig == fg_ON)
    {
      config_flag = 1;
      /// OP initialization: no synchronization OP and navigation task solution
      if ((pMainMgr->config.OPmode == 0) && (pMainMgr->config_new.OPmode == 1))
      {
#ifdef DELAYED_OPMGRINIT
		delayed_OPMgr_Init = 1 ;
		new_OrbStatic = pMainMgr->config_new.OrbStatic ;
		new_Synchronization = NO_SYNC ;
#else		
        OPMgr_Init(pOPMgr, pMainMgr->config_new.OrbStatic);
        pNavTask->Synchronization = NO_SYNC;
#endif
		OP_Init = 1;
      }
      /// OP reset: synchronization navigation task solution exist
      if ((pMainMgr->config.OPmode == 1) && (pMainMgr->config_new.OPmode == 0))
      {
#ifdef DELAYED_OPMGRINIT
		delayed_OPMgr_Init = 1 ;
		new_OrbStatic = pMainMgr->config_new.OrbStatic ;
		new_Synchronization = SYNC_OK ;
#else		
        OPMgr_Init(pOPMgr, pMainMgr->config_new.OrbStatic);
        pNavTask->Synchronization = SYNC_OK;
#endif
		OP_Init = 1;
      }
      /// Dynamic model switcher
      if (pMainMgr->config.OrbStatic != pMainMgr->config_new.OrbStatic) 
      {
	    if (OP_Init == 0)
#ifdef DELAYED_OPMGRINIT
		{
			delayed_OPMgr_Init = 1 ;
			new_OrbStatic = pMainMgr->config_new.OrbStatic ;
			new_Synchronization = pNavTask->Synchronization ;
		}
#else		
			OPMgr_Init(pOPMgr, pMainMgr->config_new.OrbStatic);
#endif

        if ((pMainMgr->config.OPmode == 1) && (pMainMgr->config.OPmode == pMainMgr->config_new.OPmode))
#ifdef DELAYED_OPMGRINIT
			new_Synchronization = NO_SYNC ;
#else
			pNavTask->Synchronization = NO_SYNC;
#endif
      }      
      /// coefficients for DLL
      if ((pMainMgr->config.CoefDLL != pMainMgr->config_new.CoefDLL) 
           || (pMainMgr->config.DLLtime != pMainMgr->config_new.DLLtime))
      {
        PLLDLLSetDllCoef(&pSatMgr->coefPllDll, pMainMgr->config_new.CoefDLL, pMainMgr->config_new.DLLtime);
      }
      /// Navigation task solution period
      if (pMainMgr->config.NavSolRate != pMainMgr->config_new.NavSolRate)
      {
        /// if navigation task solution is disable then raise no navigation task solution flag
        /// otherwise calculate new period of navigation task solution
        if (pMainMgr->config_new.NavSolRate == 0)
          pNavTask->NoNavTaskSol = fg_ON;
        else
        {
          uint32 sec = (uint32)((uint32)((double)pSatMgr->ms_count/1000.)*1000.);

          pNavTask->PeriodTime = pMainMgr->config_new.NavSolRate;  /// in ms
          while (sec < (pSatMgr->ms_count + 100))
          {
            sec += pNavTask->PeriodTime;
          }
          pNavTask->CountPostNavTask = sec;
          pNavTask->NoNavTaskSol     = fg_OFF;
        }
      }
      /// baud rate and parity for RS422
      if (pMainMgr->config.BaudRate422 != pMainMgr->config_new.BaudRate422)
      {
        UartTerminalInit(&pFramework->DebugTerminal, pMainMgr->config_new.BaudRate422);
      }
      /// upload configuration setup in Flash
      if (pMainMgr->config_new.UpdateConfig == 1)
        if (pFramework->FlashDrv.flashData.controlConfig.SaveData == fg_OFF)
          pFramework->FlashDrv.flashData.controlConfig.SaveData = fg_ON;

      #ifdef USE_DSPBIOS
        HWI_disable();
      #endif
      _memcpy(&pMainMgr->config, &pMainMgr->config_new, LENGTH_CONFIG);
      #ifdef USE_DSPBIOS
        HWI_enable();
      #endif
      pMainMgr->NewConfig = fg_OFF;	  
    }
    if ((pMainMgr->BinStrFlag == fg_OFF) && (config_flag == 1))
    {
#ifdef NEW_TMTC
		if ( tmtc_mode == DETAIL_MODE )
#endif	// end NEW_TMTC
		{
			sprintf(pMainMgr->TestStr.str, "CONFIG: Nav_&OP:%d, modelOS:%d, I:%d, T:%d\r\n", pMainMgr->config.OPmode, pMainMgr->config.OrbStatic, 
				pMainMgr->config.IonosphereModel, pMainMgr->config.TroposphereModel);
			pFramework->DebugTerminal.vtable.pfnWrite(&pFramework->DebugTerminal.vtable, pMainMgr->TestStr.str, strlen(pMainMgr->TestStr.str));  
		}
		config_flag = 0;
    }
  #endif
}

//================================================================================
/// Set new period for navigation task solution
///
/// @param[in]  pMainMgr - pointer to Main Manager object
/// @param[in]  pNavTask - pointer to Navigation Task object
/// @param[in]  ms_count - current time [ms]
//================================================================================
void MainMgr_PeriodNavTask(tMainManager *pMainMgr,
                           tNavTask     *pNavTask,
                           uint32        ms_count)
{
  /// check new period time flag
  if (pMainMgr->NewTime == fg_ON)
  {
    /// if navigation task solution is disable then raise no navigation task solution flag
    /// and clear period time
    if (pMainMgr->NewPeriodTime == 0)
    {
      pNavTask->NoNavTaskSol = fg_ON;
    }
    else  /// set new period time
    {
      uint32 sec = (uint32)((uint32)((double)ms_count/1000.)*1000.);

      pNavTask->PeriodTime = pMainMgr->NewPeriodTime;
      while (sec < (ms_count + 100))
      {
        sec += pNavTask->PeriodTime;
      }
      pNavTask->CountPostNavTask = sec;
      pNavTask->NoNavTaskSol     = fg_OFF;
    }
    pMainMgr->NewTime = fg_OFF;
  }
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (RawDataFixToMessage, "fast_text_2")
	#endif
#endif

//==================================================================================
/// Convert fix-point raw data to message format
///
/// @param[in]  sat  - pointer to part message
/// @param[in]  pSat - pointer to current satellite
//==================================================================================
static void RawDataFixToMessage(tSatRM *sat, tSatelliteGPS *pSat)
{
  tLongFract tmp;  ///< temporary variable

  /// convert pseudo-range from ms to m
  LongFractMPY(&pSat->raw_data_fix.ps_delay, C_LIGHT_Q0, &tmp);  /// Q32*Q0 -> Q16
 
  /// save satellite ID and pseudo range
  sat->sv_id   = pSat->sv_num;
  sat->num_sat = 0;
  sat->psr_h   = tmp.integral;
  sat->psr_l   = (uint16)((tmp.fract>>16)&0xffff);
  
  /// convert pseudo-range rate from Hz to m/s
  LongFractMPY(&pSat->raw_data_fix.ps_freq, CLIGHT_l_F0Q23, &tmp);  /// Q32*Q23 -> Q39
  LongFractShr(&tmp, 7);  /// Q39 -> Q32 

  /// save pseudo-range rate
  sat->psrr      = tmp.integral;
  /// pseudo-range estimated error (bias), pseudo-range estimated error (noise) convert from m to mm 
  /// and saved, pseudo-range rate estimated error convert from m/s to mm/s and saved
  sat->psr_err_b = L_mpy_ls(pSat->est_err_fix.ps_delay_biasQ17, Q5_1000)>>7; // Q17*Q5 -> Q7
  sat->psr_err_n = (int16)(L_mpy_ls(pSat->est_err_fix.ps_delay_noiseQ17, Q5_1000)>>7);
  sat->psrr_err  = (int16)(L_mpy_ls(pSat->est_err_fix.ps_freqQ17, Q5_1000)>>7);
}


//==================================================================================
/// Channels Range Measurements for string mode
///
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//==================================================================================
#ifdef FASTER_TRACKING

static LIST12 SVraw = { 0 } ;
static int32 raw_measure_ms_count ;
static tRawDataFix raw_data_fix[GPS_SAT_CHANNELS_COUNT] ;

void MainMgr_NMEARawMeasur(tSatelliteManager *pSatMgr)
{
  int i;
  tSatelliteGPS *pSat;

  SVraw.N = 0;
  for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
  {
    if (pSatMgr->gps_satellites[i].sv_num != SATELLITE_INVALID_ID)
    {
      pSat = &pSatMgr->gps_satellites[i];
      if (pSat->raw_data_fix.attempt == 1)
	  {
        SVraw.Cells[SVraw.N] = pSat->sv_num;
		raw_data_fix[SVraw.N] = pSat->raw_data_fix ;
		SVraw.N ++ ;
	  }
    }
  }
  
  raw_measure_ms_count = pSatMgr->ms_count ;
}

void MainMgr_NMEARawMeasur_2(tSatelliteManager *pSatMgr)
{
  int i;
  char           buff[640];                          ///< temporary buffer
  tTerminal *pTerm = GetPrimaryDebugTerminal();
  int64 tmp_l;
  int32 tmp_s;
  tLongFract mul;
  char *str = &buff[0];
  int length = 0;

  for(i=0;i<640;i++)
    buff[i] = 0;

  // Command prefix:
  strcpy(str, "$RAW,\0");
  str += 5; length += 5;
  // time in s
  LongFractMulU32(raw_measure_ms_count, Q21_1000, &mul);
  LongFractShr(&mul, 21);
  tmp_l = ((int64)mul.integral<<32) | (int64)mul.fract&0xffffffff;
  Int64_to_String(tmp_l, 12, 6, str);
  str += 14; length += 14;
  Int_to_String(SVraw.N, 2, str);
  str += 3; length += 3;
  if (SVraw.N != 0)
  {
    for(i=0; i<SVraw.N;i++)
    {
      Int_to_String(SVraw.Cells[i], 2, str);
      str += 3; length += 3;
      LongFractMPY(&raw_data_fix[i].ps_delay, C_LIGHT_Q0, &mul);
      tmp_l = ((int64)mul.integral<<16) | (int64)((mul.fract>>16)&0xffff);
      Int64_to_String(tmp_l, 12, 3, str);
      str += 14; length += 14;
      LongFractMPY(&raw_data_fix[i].ps_freq, CLIGHT_l_F0Q23, &mul); // Q32*Q23 -> Q39
      LongFractShr(&mul, 7); // Q39 -> Q32 
      tmp_s = mul.integral;
      Int32_to_String(tmp_s, 9, 3, str);
      str += 11; length += 11;
      LongFractMPYU(&raw_data_fix[i].timeCoord, Q21_1000, &mul); // Q21*Q32 -> Q21
      LongFractShr(&mul, 21);
      tmp_l = ((int64)mul.integral<<32) | (int64)mul.fract&0xffffffff;
      Int64_to_String(tmp_l, 13, 6, str);
      str += 15; length += 15;
    }
  }
  str--;
  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';
  length += 2;
  pTerm->vtable.pfnWrite(&pTerm->vtable, buff, length);
}

#else

void MainMgr_NMEARawMeasur(tSatelliteManager *pSatMgr)
{
  LIST12 SVraw;
  int i;
  tSatelliteGPS *pSat;
  char           buff[640];                          ///< temporary buffer
  tTerminal *pTerm = GetPrimaryDebugTerminal();
  int64 tmp_l;
  int32 tmp_s;
  tLongFract mul;
  char *str = &buff[0];
  int length = 0;

  for(i=0;i<640;i++)
    buff[i] = 0;
	
  SVraw.N = 0;
  for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
  {
    if (pSatMgr->gps_satellites[i].sv_num != SATELLITE_INVALID_ID)
    {
      pSat = &pSatMgr->gps_satellites[i];
      if (pSat->raw_data_fix.attempt == 1)
        SVraw.Cells[SVraw.N++] = pSat->sv_num;
    }
  }
  // Command prefix:
  strcpy(str, "$RAW,\0");
  str += 5; length += 5;
  // time in s
  LongFractMulU32(pSatMgr->ms_count, Q21_1000, &mul);
  LongFractShr(&mul, 21);
  tmp_l = ((int64)mul.integral<<32) | (int64)mul.fract&0xffffffff;
  Int64_to_String(tmp_l, 12, 6, str);
  str += 14; length += 14;
  Int_to_String(SVraw.N, 2, str);
  str += 3; length += 3;
  if (SVraw.N != 0)
  {
    for(i=0; i<SVraw.N;i++)
    {
      pSat = SatMgr_GetSatellite(pSatMgr, SVraw.Cells[i]);
      Int_to_String(pSat->sv_num, 2, str);
      str += 3; length += 3;
      LongFractMPY(&pSat->raw_data_fix.ps_delay, C_LIGHT_Q0, &mul);
      tmp_l = ((int64)mul.integral<<16) | (int64)((mul.fract>>16)&0xffff);
      Int64_to_String(tmp_l, 12, 3, str);
      str += 14; length += 14;
      LongFractMPY(&pSat->raw_data_fix.ps_freq, CLIGHT_l_F0Q23, &mul); // Q32*Q23 -> Q39
      LongFractShr(&mul, 7); // Q39 -> Q32 
      tmp_s = mul.integral;
      Int32_to_String(tmp_s, 9, 3, str);
      str += 11; length += 11;
      LongFractMPYU(&pSat->raw_data_fix.timeCoord, Q21_1000, &mul); // Q21*Q32 -> Q21
      LongFractShr(&mul, 21);
      tmp_l = ((int64)mul.integral<<32) | (int64)mul.fract&0xffffffff;
      Int64_to_String(tmp_l, 13, 6, str);
      str += 15; length += 15;
    }
  }
  str--;
  *str++ = '\r';
  *str++ = '\n';
  *str++ = '\0';
  length += 2;
  pTerm->vtable.pfnWrite(&pTerm->vtable, buff, length);
}
#endif
//==================================================================================
/// ECEF Navigation And Status Message
///
/// @param[in]  ecef      - pointer to instance
/// @param[in]  User      - User structure
/// @param[in]  NavCond   - navigation conditions
//==================================================================================
void MainMgr_NavAndStatusECEF(tECEFNavStatus *ecef,
                              USER            User,
                              NAV_CONDITIONS  NavCond)
{
  memset(ecef, 0, sizeof(tECEFNavStatus));
  if (User.User_fix.fNT_Condition)
  {
    ecef->wn           = User.wn;
    ecef->mks_h        = (int32)(User.User_fix.t>>16);
    ecef->mks_l        = (uint16)(User.User_fix.t&0xffff);
    ecef->X            = User.User_fix.X;
    ecef->Y            = User.User_fix.Y;
    ecef->Z            = User.User_fix.Z;
    ecef->VX           = User.User_fix.Xdot;
    ecef->VY           = User.User_fix.Ydot;
    ecef->VZ           = User.User_fix.Zdot;
    ecef->OscillOffset = User.User_fix.OscillOffset;
    ecef->R            = User.User_fix.R;
    ecef->Almanac      = NavCond.alm_set_flgs;
    ecef->Health       = NavCond.health_flgs;
    ecef->Visibility   = NavCond.visibility_flgs;
    ecef->NavStatus    = NS_3D_MODE;
    ecef->NavStatus   |= NS_ECEF;
    ecef->CPU          = NavCond.CPU;
    ecef->GDOP         = User.User_fix.GDOP;
    ecef->PDOP         = User.User_fix.PDOP;
    ecef->HDOP         = User.User_fix.HDOP;
    ecef->TDOP         = User.User_fix.TDOP;
  }
  else
  {
    ecef->wn           = User.wn;
    ecef->mks_h        = (int32)(User.User_fix.t>>16);
    ecef->mks_l        = (uint16)(User.User_fix.t&0xffff);
    ecef->OscillOffset = User.User_fix.OscillOffset;
    ecef->R            = User.User_fix.R;
    ecef->Almanac      = NavCond.alm_set_flgs;
    ecef->Health       = NavCond.health_flgs;
    ecef->Visibility   = NavCond.visibility_flgs;
    ecef->NavStatus    = NS_NO_3D_MODE;
    ecef->NavStatus   |= NS_ECEF;
    ecef->CPU          = NavCond.CPU;
  }
}

//==================================================================================
/// Geo Navigation And Status Message
///
/// @param[in]  geo       - pointer to instance
/// @param[in]  User      - User structure
/// @param[in]  NavCond   - navigation conditions
//==================================================================================
void MainMgr_NavAndStatusGEO(tGeoNavStatus *geo,
                             USER           User,
                             NAV_CONDITIONS NavCond)
{
  memset(geo, 0, sizeof(tGeoNavStatus));
  if (User.User_fix.fNT_Condition)
  {
    geo->wn           = User.wn;
    geo->mks_h        = (int32)(User.User_fix.t>>16);
    geo->mks_l        = (uint16)(User.User_fix.t&0xffff);
    geo->Lat          = User.User_fix.Lat;
    geo->Lon          = User.User_fix.Lon;
    geo->Alt          = User.User_fix.H;
    geo->VNorth       = User.User_fix.Ndot;
    geo->VEest        = User.User_fix.Edot;
    geo->VUp          = User.User_fix.Udot;
    geo->OscillOffset = User.User_fix.OscillOffset;
    geo->R            = User.User_fix.R;
    geo->Almanac      = NavCond.alm_set_flgs;
    geo->Health       = NavCond.health_flgs;
    geo->Visibility   = NavCond.visibility_flgs;
    geo->NavStatus    = NS_3D_MODE;
    geo->NavStatus   |= NS_GEO;
    geo->CPU          = NavCond.CPU;
    geo->GDOP         = User.User_fix.GDOP;
    geo->PDOP         = User.User_fix.PDOP;
    geo->HDOP         = User.User_fix.HDOP;
    geo->TDOP         = User.User_fix.TDOP;
  }
  else
  {
    geo->wn           = User.wn;
    geo->mks_h        = (int32)(User.User_fix.t>>16);
    geo->mks_l        = (uint16)(User.User_fix.t&0xffff);
    geo->OscillOffset = User.User_fix.OscillOffset;
    geo->R            = User.User_fix.R;
    geo->Almanac      = NavCond.alm_set_flgs;
    geo->Health       = NavCond.health_flgs;
    geo->Visibility   = NavCond.visibility_flgs;
    geo->NavStatus    = NS_NO_3D_MODE;
    geo->NavStatus   |= NS_GEO;
    geo->CPU          = NavCond.CPU;
  }
}

//==================================================================================
/// OE Navigation And Status Message
///
/// @param[in]  oe        - pointer to instance
/// @param[in]  User      - User structure
/// @param[in]  NavCond   - navigation conditions
//==================================================================================
void MainMgr_NavAndStatusOE(tOENavStatus   *oe,
                             USER           User,
                             NAV_CONDITIONS NavCond)
{
  memset(oe, 0, sizeof(tOENavStatus));
  if (User.User_fix.fNT_Condition)
  {
    oe->wn           = User.wn;
    oe->mks_h        = (int32)(User.User_fix.t>>16);
    oe->mks_l        = (uint16)(User.User_fix.t&0xffff);
    oe->A            = User.User_fix.A;
    oe->Omega        = User.User_fix.Omega;
    oe->M            = User.User_fix.M;
    oe->I            = User.User_fix.I;
    oe->OmegaP       = User.User_fix.OmegaP;
    oe->Ecc          = User.User_fix.Ecc;
    oe->OscillOffset = User.User_fix.OscillOffset;
    oe->R            = User.User_fix.R;
    oe->Almanac      = NavCond.alm_set_flgs;
    oe->Health       = NavCond.health_flgs;
    oe->Visibility   = NavCond.visibility_flgs;
    oe->NavStatus    = NS_3D_MODE;
    oe->NavStatus   |= NS_OE;
    oe->CPU          = NavCond.CPU;
    oe->GDOP         = User.User_fix.GDOP;
    oe->PDOP         = User.User_fix.PDOP;
    oe->HDOP         = User.User_fix.HDOP;
    oe->TDOP         = User.User_fix.TDOP;
  }
  else
  {
    oe->wn           = User.wn;
    oe->mks_h        = (int32)(User.User_fix.t>>16);
    oe->mks_l        = (uint16)(User.User_fix.t&0xffff);
    oe->OscillOffset = User.User_fix.OscillOffset;
    oe->R            = User.User_fix.R;
    oe->Almanac      = NavCond.alm_set_flgs;
    oe->Health       = NavCond.health_flgs;
    oe->Visibility   = NavCond.visibility_flgs;
    oe->NavStatus    = NS_NO_3D_MODE;
    oe->NavStatus   |= NS_OE;
    oe->CPU          = NavCond.CPU;
  }
}

//==================================================================================
/// ECEF OP Navigation And Status Message
///
/// @param[in]  ecef       - pointer to instance
/// @param[in]  CoordValid - flag of valid user's coordinates
/// @param[in]  NavCond    - navigation conditions
/// @param[out] User_OP    - OP User structure
//==================================================================================
void MainMgr_OPNavAndStatusECEF(tECEFOPNavStatus *ecef,
                                NAV_CONDITIONS    NavCond,
                                int8              CoordValid,
                                USERop            User_OP)
{
  memset(ecef, 0, sizeof(tECEFOPNavStatus));
  if (User_OP.fOP_Condition)
  {
    ecef->wn           = User_OP.wn;
    ecef->mks_h        = (int32)(User_OP.t>>16);
    ecef->mks_l        = (uint16)(User_OP.t&0xffff);
    ecef->X            = User_OP.X;
    ecef->Y            = User_OP.Y;
    ecef->Z            = User_OP.Z;
    ecef->VX           = User_OP.Xdot;
    ecef->VY           = User_OP.Ydot;
    ecef->VZ           = User_OP.Zdot;
    ecef->OscillOffset = User_OP.OscillOffset;
    ecef->R            = User_OP.R;
    ecef->Almanac      = NavCond.alm_set_flgs;
    ecef->Health       = NavCond.health_flgs;
    ecef->Visibility   = NavCond.visibility_flgs;
    ecef->NavStatus    = NS_ECEF;
    ecef->NavStatus   |= (User_OP.fOP_Condition<<3)&0x18;
    if (CoordValid == OK_NAV_TASK)
      ecef->NavStatus |= NS_3D_MODE;
    ecef->CPU          = NavCond.CPU;
  }
  else
  {
    ecef->wn           = User_OP.wn;
    ecef->mks_h        = (int32)(User_OP.t>>16);
    ecef->mks_l        = (uint16)(User_OP.t&0xffff);
    ecef->Almanac      = NavCond.alm_set_flgs;
    ecef->Health       = NavCond.health_flgs;
    ecef->Visibility   = NavCond.visibility_flgs;
    ecef->NavStatus    = NS_ECEF;
    if (CoordValid == OK_NAV_TASK)
      ecef->NavStatus |= NS_3D_MODE;
    ecef->CPU        = NavCond.CPU;
  }
}

//==================================================================================
/// Geo OP Navigation And Status Message
///
/// @param[in]  geo        - pointer to instance
/// @param[in]  CoordValid - flag of valid user's coordinates
/// @param[in]  NavCond    - navigation conditions
/// @param[out] User_OP    - OP User structure
//==================================================================================
void MainMgr_OPNavAndStatusGEO(tGeoOPNavStatus *geo,
                               NAV_CONDITIONS   NavCond,
                               int8             CoordValid,
                               USERop           User_OP)
{
  memset(geo, 0, sizeof(tGeoNavStatus));
  if (User_OP.fOP_Condition)
  {
    geo->wn           = User_OP.wn;
    geo->mks_h        = (int32)(User_OP.t>>16);
    geo->mks_l        = (uint16)(User_OP.t&0xffff);
    geo->Lat          = User_OP.Lat;
    geo->Lon          = User_OP.Lon;
    geo->Alt          = User_OP.H;
    geo->VNorth       = User_OP.Ndot;
    geo->VEest        = User_OP.Edot;
    geo->VUp          = User_OP.Udot;
    geo->OscillOffset = User_OP.OscillOffset;
    geo->R            = User_OP.R;
    geo->Almanac      = NavCond.alm_set_flgs;
    geo->Health       = NavCond.health_flgs;
    geo->Visibility   = NavCond.visibility_flgs;
    geo->NavStatus    = NS_GEO;
    geo->NavStatus   |= (User_OP.fOP_Condition<<3)&0x18;
    if (CoordValid == OK_NAV_TASK)
      geo->NavStatus |= NS_3D_MODE;
    geo->CPU          = NavCond.CPU;
  }
  else
  {
    geo->wn           = User_OP.wn;
    geo->mks_h        = (int32)(User_OP.t>>16);
    geo->mks_l        = (uint16)(User_OP.t&0xffff);
    geo->Almanac      = NavCond.alm_set_flgs;
    geo->Health       = NavCond.health_flgs;
    geo->Visibility   = NavCond.visibility_flgs;
    geo->NavStatus    = NS_GEO;
    if (CoordValid == OK_NAV_TASK)
      geo->NavStatus |= NS_3D_MODE;
    geo->CPU          = NavCond.CPU;
  }
}

//==================================================================================
/// OE OP Navigation And Status Message
///
/// @param[in]  oe         - pointer to instance
/// @param[in]  CoordValid - flag of valid user's coordinates
/// @param[in]  NavCond    - navigation conditions
/// @param[out] User_OP    - OP User structure
//==================================================================================
void MainMgr_OPNavAndStatusOE(tOEOPNavStatus *oe,
                              NAV_CONDITIONS  NavCond,
                              int8            CoordValid,
                              USERop          User_OP)
{
  memset(oe, 0, sizeof(tOENavStatus));
  if (User_OP.fOP_Condition)
  {
    oe->wn           = User_OP.wn;
    oe->mks_h        = (int32)(User_OP.t>>16);
    oe->mks_l        = (uint16)(User_OP.t&0xffff);
    oe->A            = User_OP.A;
    oe->Omega        = User_OP.Omega;
    oe->M            = User_OP.M;
    oe->I            = User_OP.I;
    oe->OmegaP       = User_OP.OmegaP;
    oe->Ecc          = User_OP.Ecc;
    oe->OscillOffset = User_OP.OscillOffset;
    oe->R            = User_OP.R;
    oe->Almanac      = NavCond.alm_set_flgs;
    oe->Health       = NavCond.health_flgs;
    oe->Visibility   = NavCond.visibility_flgs;
    oe->NavStatus    = NS_OE;
    oe->NavStatus   |= (User_OP.fOP_Condition<<3)&0x18;
    if (CoordValid == OK_NAV_TASK)
      oe->NavStatus |= NS_3D_MODE;
    oe->CPU          = NavCond.CPU;
  }
  else
  {
    oe->wn           = User_OP.wn;
    oe->mks_h        = (int32)(User_OP.t>>16);
    oe->mks_l        = (uint16)(User_OP.t&0xffff);
    oe->Almanac      = NavCond.alm_set_flgs;
    oe->Health       = NavCond.health_flgs;
    oe->Visibility   = NavCond.visibility_flgs;
    oe->NavStatus    = NS_OE;
    if (CoordValid == OK_NAV_TASK)
      oe->NavStatus |= NS_3D_MODE;
    oe->CPU          = NavCond.CPU;
  }
}

//==================================================================================
/// Almanac Message
///
/// @param[in]  mesAlm     - pointer to instance
/// @param[in]  pTopSystem - pointer to Top System structure
//==================================================================================
void MainMgr_Almanac(tMesAlmanac *mesAlm, tTopSystem *pTopSystem)
{
  int    i, 
         j;
  uint8 *src;  ///< pointer to current almanac

  for(i=0; i<GPS_SATELLITE_COUNT; i++)
  {
    if ((pTopSystem->alm_set_flgs&(0x1<<i)) == (0x1<<i))
    {
      /// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
      if ((pTopSystem->DecCommon.TempAlmGPS[i].flag_bl == 0) || (pTopSystem->DecCommon.TempAlmGPS[i].flag == 1))
      {
        pTopSystem->DecCommon.TempAlmGPS[i].flag_bl = 1;
        _memcpy(&mesAlm->almanac[i], &pTopSystem->DecCommon.TempAlmGPS[i].TAlmGPS, sizeof(MES_ALM_GPS));
        pTopSystem->DecCommon.TempAlmGPS[i].flag_bl = 0;
      }
      else  /// if data is block then raw data is available
        _memcpy(&mesAlm->almanac[i], &pTopSystem->DecCommon.RawAlmGPS[i], sizeof(MES_ALM_GPS));
    }
    else
    {
      src = (uint8*)&mesAlm->almanac[i];
      for(j=0;j<SIZE_ALMANAC;j++)
        *src++ = 0;
    }
  }
}

//==================================================================================
/// Ephemeris Message
///
/// @param[in]  mesEph  - pointer to instance
/// @param[in]  pSatMgr - pointer to Top System structure
//==================================================================================
void MainMgr_Ephemeris(tMesEphemeris *mesEph, tSatelliteManager *pSatMgr)
{
  int            i, 
                 j;
  uint8         *src;   ///< pointer to current ephemeris
  tSatelliteGPS *pSat;  ///< pointer to current satellite

  for(i=0; i<GPS_SATELLITE_COUNT; i++)
  {
    if ((pSatMgr->TopSystem.eph_set_flgs&(0x1<<i)) == (0x1<<i))
    {
      /// check blocking data, if it is block but available flag is raise then data is copy in navigation task solution buffer
      if ((pSatMgr->TopSystem.DecCommon.TempEph[i].flag_bl == 0) || (pSatMgr->TopSystem.DecCommon.TempEph[i].flag == 1))
        _memcpy(&mesEph->ephemeris[i], &pSatMgr->TopSystem.DecCommon.TempEph[i].TEphGPS, sizeof(MES_EPH_GPS));
      else  /// if data is block then raw data is available
      {
        pSat = SatMgr_GetSatellite(pSatMgr, (i+1));
        if (pSat != NULL)
          _memcpy(&mesEph->ephemeris[i], &pSat->decoder.RawEph, sizeof(MES_EPH_GPS));
      }    
    }
    else
    {
      src = (uint8*)&mesEph->ephemeris[i];
      for(j=0;j<SIZE_EPHEMERIS;j++)
        *src++ = 0;
    }
  }
}

#ifdef EXT_SRAM
	#ifdef FASTER_TRACKING
	#pragma CODE_SECTION (MainMgr_MessagesForm, "fast_text_2")
	#endif
#endif

//==================================================================================
/// Time and Channels Status Data
/// Channels Range Measurements
///
/// @param[in]  pTimeChStatus - pointer to instance
/// @param[in]  pRawChannel   - pointer to instance
/// @param[in]  pSatMgr       - pointer to Satellite Manager object
//==================================================================================
void MainMgr_MessagesForm(tTimeChStatus     *pTimeChStatus,
                          tChRM             *pRawChannels,
                          tSatelliteManager *pSatMgr)
{
  int16           phase_err;      ///< phase error
  uint8           NumOfSats,      ///< number of satellites 
                  i;              ///< counter
  tLongFract      lf;             ///< current time
  tSatelliteGPS  *pSat;           ///< pointer to current satellite

  // Time and Channels Status data
  memset(pTimeChStatus, 0, sizeof(tTimeChStatus));
  // Channels Range Measurements
  memset(pRawChannels, 0, sizeof(tChRM));

  LongFractMulU32(pSatMgr->ms_count, Q21_1000, &lf);
  LongFractShr(&lf, 21);

  // GPS week number since epoch
  pRawChannels->wn = pTimeChStatus->wn = pSatMgr->wn;
  // GPS time tag from start of the week
  pRawChannels->mks_h  = pTimeChStatus->mks_h = ((lf.integral<<16)&0xffff0000) | ((lf.fract>>16)&0xffff);
  pRawChannels->mks_l  = pTimeChStatus->mks_l = (lf.fract&0xffff);

  NumOfSats = 0;
  for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
  {
    if (pSatMgr->gps_satellites[i].sv_num != SATELLITE_INVALID_ID)
    {
      pSat = &pSatMgr->gps_satellites[i];
      // Satellite ID, SNR, status, phase error and LOS for Time and Channels Status data
      pTimeChStatus->satCh[i].sv_id   = pSat->sv_num;
      pTimeChStatus->satCh[i].SNR     = pSat->SNR_log;
      pTimeChStatus->satCh[i].status  = pSat->status;
      phase_err = (int16)(mpy(pSat->gps_chan.pllDll.status.phase_err, l_PI_180_10Q5)>>19);  // Q14+Q5 -> Q19 -> Q0
      pTimeChStatus->satCh[i].phase_h = (int8)(phase_err>>8)&0xff;
      pTimeChStatus->satCh[i].phase_l = (uint8)(phase_err&0xff);
      // Satellite number, raw data, estimated errors for RFFE Channels Range Measurements
      if (pSat->raw_data_fix.attempt == 1)
      {
        RawDataFixToMessage(&pRawChannels->satRM[i], pSat);
        NumOfSats++;
      }
    }
  }
  // Satellites number for RFFE Channels Range Measurements
  pRawChannels->satRM[0].num_sat = NumOfSats;
}

//=====================================================================================
/// Line of Sight Vector Message
///
/// @param[in]  dataLOS        - pointer to instance
/// @param[in]  navtask_params - pointer to navigation task parameters
/// @param[in]  wn             - week number
/// @param[in]  t              - time of week
//=====================================================================================
void MainMgr_LOS(tDataLOS *dataLOS, tNavTaskParams *navtask_params, int16 wn, int64 t)
{
  uint8 i,
        NumOfSats = 0;  ///< satellites number

  dataLOS->wn    = wn;
  dataLOS->mks_h = (int32)(t>>16);
  dataLOS->mks_l = (uint16)(t&0xffff);

  /// satellites list forming
  for(i=0;i<GPS_SAT_CHANNELS_COUNT;i++)
  {
    if (navtask_params[i].sv_num != SATELLITE_INVALID_ID)
    {
      /// check is used in navigation task solution and is not bad status
      if ((navtask_params[i].USED == 1) && (navtask_params[i].BAD_SAT == 0))
      {
        dataLOS->satLos[NumOfSats].num_sat = 0;
        dataLOS->satLos[NumOfSats].sv_id   = navtask_params[i].sv_num;
        dataLOS->satLos[NumOfSats].SNR     = navtask_params[i].SNR_log;
        dataLOS->satLos[NumOfSats].elev    = navtask_params[i].sat_params.elev_i16;
        dataLOS->satLos[NumOfSats].azim    = navtask_params[i].sat_params.azim_i16;
        dataLOS->satLos[NumOfSats].losX    = navtask_params[i].sat_coord.x_los;
        dataLOS->satLos[NumOfSats].losY    = navtask_params[i].sat_coord.y_los;
        dataLOS->satLos[NumOfSats].losZ    = navtask_params[i].sat_coord.z_los;
        NumOfSats++;
      }
    }
  }
  dataLOS->satLos[0].num_sat = NumOfSats;
  if (NumOfSats < GPS_SAT_CHANNELS_COUNT)
  {
    for(i=NumOfSats;i<GPS_SAT_CHANNELS_COUNT;i++)
    {
      memset(&dataLOS->satLos[i], 0, sizeof(tSatLOS));
    }
  }
}












