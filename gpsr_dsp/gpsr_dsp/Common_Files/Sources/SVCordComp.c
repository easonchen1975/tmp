/*****************************************************************************
 *    Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  compute SV coordinations using ephemeris
 *   @file                   SVCordComp.c
 *   @author                 A. Baloyan
 *   @date                   25.06.2005
 *   @version                1.0
 */
/*****************************************************************************/

#include "SVCordComp.h"

//=======================================================================================
///  @brief     Compute GPS SV position from time and ephemeris data
///
///  @param[in]  pThis    - pointer to GPS satellite coordinates
///  @param[in]  pEphGps  - pointer to GPS ephemeris structure
///  @param[in]  sec      - desired time, seconds of day (tsv)
//=======================================================================================
void SatGPS_EphCoordCalc(tCoord *pThis, EPH_GPS *pEphGps, float64 sec)
{ 
  float64   dt,         a,        l_a,      ecc,      sq_ecc, 
            n0,         M,        E,        dE,       E0,   
            Edot,       cos_E,    sin_E,    nu,       r,    
            r_dot,      Phi,      Phi2dot,  cos_2Phi, sin_2Phi,
            wa_new,     cos_wa,   sin_wa,   wa_dot,   cos_wadot, 
            sin_wadot,  i_new,    cos_i,    sin_i,    i_dot, 
            la_new,     cos_la,   sin_la,   la_dot,   X, 
            Y,          Xdot,     Ydot,     temp,     dt_SV,
            t_SV;
  
  pEphGps->flag_bl = 1;
  ecc = pEphGps->e; 
  sq_ecc = sqrt(1 - ecc * ecc);
  
  // Semi-major axis of satellite orbit [m]
  a = pEphGps->roota; 
  a *= a; 
  l_a = 1.0 / a;
    
  dt = sec - pEphGps->toc;
  if(dt > S_HALF_WEEK)
    dt -= S_WEEK;
  else if(dt < - S_HALF_WEEK)
    dt += S_WEEK;

  /// SV's time shift approximation: rough
  dt_SV = pEphGps->af0 + (pEphGps->af1 + pEphGps->af2 * dt) * dt;

  /// SV coordinate computation
  t_SV = sec - dt_SV;

  dt = t_SV - pEphGps->toc;
  if(dt > S_HALF_WEEK)
    dt -= S_WEEK;
  else if(dt < - S_HALF_WEEK)
    dt += S_WEEK;

  /// SV's time shift approximation: accurate, compute SV's time shift relatively system time
  pThis->dt_SV = pEphGps->af0 + (pEphGps->af1 + pEphGps->af2 * dt) * dt - pEphGps->tgd;

  t_SV = sec - pThis->dt_SV;

  // Compute the difference between the pThis->EphGPSeneris epoch and sec
  dt = t_SV - pEphGps->toe;
  if(dt > S_HALF_WEEK) 
    dt -= S_WEEK; 
  else if(dt < -S_HALF_WEEK) 
    dt += S_WEEK;
   
  // Mean motion [rad/s]
  n0 = sqrt(3.986005e14 * l_a) * l_a;
  
  // corrected mean motion [rad/s]
  n0 += pEphGps->deltan;
  
  // Obtain eccentric anomaly by solving Kepler's equation.
  E = M = Fmodulo(pEphGps->m0 + n0 * dt, M_2PI);  // mean anomaly
  
  do
  { 
    E0 = E; 
    E = M + ecc * sin(E0);
  } 
  while(fabs(E - E0) > 1e-13);
  
  cos_E = cos(E);  
  sin_E = sin(E);
  dE = 1.0 / (1.0 - ecc * cos_E);
  Edot = dE * n0;
  
  #ifndef SIMULATOR
  // The relativistic correction term [s] computation
  pThis->dt_SV += -4.442807633e-10 * ecc * pEphGps->roota * sin_E;
  #endif
  
  // Compute the true anomaly
  nu = atan2(sq_ecc * sin_E,cos_E - ecc);
  
  // Compute the argument of latitude
  Phi    = nu + pEphGps->omega;
  Phi2dot = 2.0 * sq_ecc * dE * Edot;         // doubled Phi derivative
  
  // Generate harmonic correction terms for Phi
  cos_2Phi = cos(2.0 * Phi); 
  sin_2Phi = sin(2.0 * Phi);
  
  // Corrected argument of latitude, u
  wa_new = Phi + pEphGps->cuc * cos_2Phi + pEphGps->cus * sin_2Phi;
  cos_wa = cos(wa_new); 
  sin_wa = sin(wa_new);
  wa_dot = Phi2dot * (0.5 + pEphGps->cus * cos_2Phi - pEphGps->cuc * sin_2Phi);
  cos_wadot = -sin_wa * wa_dot;
  sin_wadot =  cos_wa * wa_dot;
  
  // Corrected orbital inclination, i
  i_new = pEphGps->i0 + pEphGps->idot * dt + pEphGps->cic * cos_2Phi + pEphGps->cis * sin_2Phi;
  cos_i = cos(i_new); 
  sin_i = sin(i_new);
  i_dot = pEphGps->idot + Phi2dot * (pEphGps->cis * cos_2Phi - pEphGps->cic * sin_2Phi);
  
  // Compute the longitude of the ascending node, W
  la_dot = pEphGps->omegadot - w_Earth;           // w_Earth = 7.2921151467e-5 rad/s
  la_new = pEphGps->omega0 + la_dot * dt - w_Earth * pEphGps->toe;
  cos_la = cos(la_new); 
  sin_la = sin(la_new);
  
  // Corrected radius, r
  r = a * (1 - ecc * cos_E) + pEphGps->crc * cos_2Phi + pEphGps->crs * sin_2Phi;
  r_dot = a * ecc * sin_E * Edot + Phi2dot * (pEphGps->crs * cos_2Phi - pEphGps->crc * sin_2Phi);
  pEphGps->flag_bl = 0;                           // pThis->EphGPSemeris parameters unblocking
  
  // Compute the satellite's position in its orbital plane
  X = r * cos_wa;
  Y = r * sin_wa;
  Xdot = r_dot * cos_wa + r * cos_wadot;
  Ydot = r_dot * sin_wa + r * sin_wadot;
  
  // Compute the satellite's position in WGS-84, [m]
  pThis->x = X * cos_la - Y * cos_i * sin_la;   // cos_i  is cos(i), sin_i  is sin(i);
  pThis->y = X * sin_la + Y * cos_i * cos_la;   // cos_wa is cos(u), sin_wa is sin(u);
  pThis->z = Y * sin_i;                         // cos_la is cos(W), sin_la is sin(W);
  
  // Compute the satellite velocities in WGS-84, [m/s]
  temp = Ydot * cos_i - Y * sin_i * i_dot;
  pThis->vx = Xdot * cos_la - temp * sin_la - la_dot * pThis->y;
  pThis->vy = Xdot * sin_la + temp * cos_la + la_dot * pThis->x;
  pThis->vz = Y * cos_i * i_dot + Ydot * sin_i;
  
  return;
}
