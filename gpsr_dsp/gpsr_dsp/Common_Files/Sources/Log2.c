/*****************************************************************************
*    Copyright (c) 2002 Spirit Corp.
******************************************************************************/
/**
 *    @project                GPS Receiver
 *    @brief                  Calculates rough estimation of Log2(x)
 *    @file                   Log2.c
 *    @author                 P. Martynovich, A. Nazarov
 *    @version                1.0
 */
/*****************************************************************************/

#ifndef _TMS320C54X_ENABLE

#include "Log2.h"
#include "Exp.h"
#include "comtypes.h"
#include "FractOper.h"

/// value for conversion from ln to log2
#define LOG2_CONST (0x38aa)  // 1-1/ln(2)

/*
    Algorithm: ln(1+x) ~ sum(a * T'(x))
    where T'(x) -- shifted Chebychev polynomials of first kind
    a(0) =  0.37645281291919543163
    a(1) =  0.34314575050761980479
    a(2) = -0.02943725152285941438
    a(3) =  0.00336708925556438925
    a(4) = -0.00043327588861004446

    MATLAB code:
    function y=myLog(x)
    x=x-1;
    a=[0.37645281291919543163 0.34314575050761980479 -0.02943725152285941438 0.00336708925556438925 -0.00043327588861004446];
    y = 2*x-1;
    T0 = 1;
    T1 = y;
    T2 = 2*y.*T1 - T0;
    T3 = 2*y.*T2 - T1;
    T4 = 2*y.*T3 - T2;
    y=a(1)*T0 + a(2)*T1 + a(3)*T2 + a(4)*T3 + a(5)*T4;;

    Accuracy: 1.1e-3

    References:
    Y.L Luk, Mathematical functions and their approximations, Academic Press, 1975
*/

//=========================================================================
/// @brief      Calculates rough estimation of Log2(x) returns 0x8000
///             on negative of zero argument or number in Q9 format
///
/// @param[in]  AccA - int32 input
/// @return     int16 Log2 of input
//=========================================================================
int16 Log2Cversion(int32 accA)
{
  int16 T1,T0,w16Res,w16Exp,y;
  int32 accB;

  if (accA<=0) return (int16)0x8000;

  w16Exp = Exp (accA);
  w16Res = 30 - w16Exp;
  accA ^= ((int32)1) << w16Res;
  accA <<= (w16Exp+1);  // ln(1 + x) = x - x*x/2 + x*x*x/4

  accA -= 0x40000000UL;
  y = (int16)(accA >> 15);  // 2*x-1

  T0 = 0x7FFF;
  T1 = y;

  accB = 808426260L + L_mult(T1,11244);
  accA  = L_mult(y,(int16)(T1>>1))>>14;
  accA -= T0;
  T0 = sature(accA);
  accB += L_mult(T0, -30867)>>5;

  accA  = L_mult(y,(int16)(T0>>1))>>14;
  accA -= T1;
  T1 = sature(accA);
  accA = L_mult(y, T1);
  accB += L_mult(T1,28245)>>8;

  accA  = L_mult(y,(int16)(T1>>1))>>14;
  accA -= T0;
  T0 = sature(accA);
  accB += L_mult(T0, -3635)>>8;

  // ln(1+x) -> log2(1+x)
  accA  =  L_mult(extract_h(accB), LOG2_CONST)>>1;
  accA +=  accB>>1;
  accA += (1L<<20);
  accA >>= 21;

  accA += w16Res << 9;

  return (int16)accA;  // |sign-1bit|integer part-6bits|fractional part-9bits|
}

#endif  // #ifndef _TMS320C54X_ENABLE
