/*****************************************************************************
*    Copyright (c) 2001-2002 Spirit Corp.
******************************************************************************
*
*    Project: ............   HF modem MIL-STD-188-110A
*    Title: ..............   Debug output
*    File name: ..........   Debug.h
*    Authors: ............   Alex G. Nazarov
*    Revision ............   1.0a
*
*    Subroutines:
*    DEBUGprintf    print to the file appending to the end
*    DEBUGremove    remove given file
*    DEBUGprintfSP  decode signal points from complex numbers 
*                   and print them into the file
*
*    NOTE: code is present only in debug version when _DEBUG is defined
*          otherwise all functions are substrituted by void
*****************************************************************************/

#ifndef _DEBUG_H__
#define _DEBUG_H__	1

#include "comtypes.h"
#include <stddef.h>

// print to the file appending to the end
//#ifdef _DEBUG
#if 1
EXTERN_C void DEBUGprintf (const char* file, const char* format, ...);
EXTERN_C void DEBUGremove (const char* file);
EXTERN_C void DEBUGwrite  (const char* file, const void* variable, size_t size);
EXTERN_C void DEBUGprintfHardDecisions (const char* file, const char* format, 
                                        const tComplex* frame, int N,
                                        const tComplex* pConstellation, 
                                        int  pConstellationSize);
EXTERN_C void DEBUGprintfComplex (const char* file, const char* format, const tComplex* array, int size);
#else
inline_ void DEBUGprintf (const char* file, const char* format, ...){}
inline_ void DEBUGremove (const char* file){}
inline_ void DEBUGwrite  (const char* file, const void* variable, size_t size) {}
inline_ void DEBUGprintfHardDecisions (const char* file, const char* format, 
                                        const tComplex* frame, int N,
                                        const tComplex* pConstellation, 
                                        int  pConstellationSize) {}
inline_ void DEBUGprintfComplex (const char* file, const char* format, const tComplex* array, int size) {}
#endif

#endif	// #ifndef _DEBUG_H__
