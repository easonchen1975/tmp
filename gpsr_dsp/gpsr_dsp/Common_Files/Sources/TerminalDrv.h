/*****************************************************************************
*    Copyright (c) 2003 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Interface to the debug terminal driver
*    @file                   TerminalDrv.h
*    @author                 D. Paroshin, M. Zhokhova
*    @date                   23.12.2005
*    @version                1.1
*/
/*****************************************************************************/

#ifndef  _TERMINAL_DRV_H
#define  _TERMINAL_DRV_H

//===================================================
/// I/O operations status
//===================================================
typedef enum
{
  io_ERROR,  /// Error flag
  io_OK      /// Normal status flag
} tIOstat;

//===================================================
/// Terminal driver structure
//===================================================
typedef struct tagTerminalDrv
{
  tIOstat (*pfnRead)   (struct tagTerminalDrv* pDrv, char *str, int MaxLen);   /// Read char string from console
  tIOstat (*pfnWrite)  (struct tagTerminalDrv* pDrv, const char *str, int Len); /// Write char string to console
  uint16  (*pfnReadBinary)  (struct tagTerminalDrv* pDrv, uint8 *pBuffer, uint16 Count); /// Read binary buffer from console
  uint16  (*pfnWriteBinary) (struct tagTerminalDrv* pDrv, const uint8 *pBuffer, uint16 Count); /// Write binary buffer to console
} tTerminalDrv;

#endif // _TERMINAL_DRV_H
