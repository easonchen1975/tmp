/*****************************************************************************
     Copyright (c) 2002 Spirit Corp.
******************************************************************************/
/**
*    @project                GPS Receiver
*    @brief                  Arctangent computation
*    @file                   Atan2.c
*    @authors                L.Purto, (wrapped by A. Nazarov)
*    @version                1.0
*/
/*****************************************************************************/

/*-----------------------------------------------
   do not compile when it must be linked as
   a part of common SPCORP library
-----------------------------------------------*/
#include "Atan2.h"
#include "Exp.h"
#include "comfuncs.h"
#include "baseop.h"

#define ATAN_ANGLES 15
#define ATAN_FAST_ANGLE 6

const uint16 aTanAngles[]=
{
  12868,      //arctan(1)/pi*(2^16)*pi/4
  7596,       //arctan(1/0x2)/pi*(2^16)*pi/4
  4014,       //arctan(1/0x4)/pi*(2^16)*pi/4
  2037,       //arctan(1/0x8)/pi*(2^16)*pi/4
  1023,       //arctan(1/0x10)/pi*(2^16)*pi/4
  512,        //arctan(1/0x20)/pi*(2^16)*pi/4
      //fast calculated angles:
      //Hi accurate angles (99.97% summary precision):
  256,        //arctan(1/0x40)/pi*(2^16)*pi/4
  128,        //arctan(1/0x80)/pi*(2^16)*pi/4
  64,         //arctan(1/0x100)/pi*(2^16)*pi/4
  32,         //arctan(1/0x200)/pi*(2^16)*pi/4
  16,         //arctan(1/0x400)/pi*(2^16)*pi/4
  8,          //arctan(1/0x800)/pi*(2^16)*pi/4
  4,          //arctan(1/0x1000)/pi*(2^16)*pi/4
  2,          //arctan(1/0x2000)/pi*(2^16)*pi/4
  1           //arctan(1/0x4000)/pi*(2^16)*pi/4
};

//===============================================================
/// Arctangent computation for small angle
///
/// @param[in]  Vector_re - re input value
/// @param[in]  Vector_im - im input value
/// @return     arctangent computation
//===============================================================
inline_  int16 ATan_SmallAngle (int16 Vector_re, int16 Vector_im)
{

  int16 i;
  int32 re, im;

  im = Vector_im << (ATAN_ANGLES-1);
  re = Vector_re << 15;

  if (!re)
  {
    if (im != 0)
    {
      return 1 << (ATAN_ANGLES-ATAN_FAST_ANGLE);
    }
    else
    {
      return 0;
    }
  }
  
  //  return (int16)( ((uint32)Vector.im << (ATAN_ANGLES-1)) / (uint16)Vector.re );
  for (i=0; i<16; i++)
  {
    im = L_subc(im, re);
  }
  return (int16)im;
}

//===============================================================
/// Arctangent computation for big angle
///
/// @param[in]  Vector_re - re input value
/// @param[in]  Vector_im - im input value
/// @return     arctangent computation
//===============================================================
inline_ int16 ATan_BigAngle (int16 Vector_re, int16 Vector_im)
{
  uint32 Real;
  uint16 Image, Shift, Step;
  uint16 OldImage;
  int16  Result=0;

  Image = Vector_im;
  Real  = Vector_re;

  Shift  = Exp0(Image | 1) - 16;
  Real  <<= Shift;
  Image <<= Shift;

  for ( Step=0; Step < ATAN_FAST_ANGLE;Step++ )
  {
    if ( Image >= Real>>Step )
    {
      OldImage = Image;
      Image = (uint16)( (uint32)Image-(Real>>Step) );
      Real += OldImage >> Step; //negligible (+=0) at small angles
      Result += aTanAngles[Step];
    }
  }
  Shift = Exp(Real);
  Real= (Real<<Shift)>>17;
  Image=(uint16)( ((uint32)Image<<Shift)>>17 );

  return Result + ATan_SmallAngle((uint16)Real, Image);
}

//========================================================
/// Angular accuracy 2.1e-4 (at unity circle)
///
/// @param[in]  Vector - the complex number (Q15)
/// @return     Arctangent (phase) (Q14)
//========================================================
int16 ATan2_fxp (tComplex16 Vector)
{
  int16 XorMask;
  int16 IsNeg   = 0;
  uint16 Result = 0;

  int16 Vector_re,  Vector_im;

  Vector_re = Vector.re;
  Vector_im = Vector.im;  

  XorMask = Vector_re & 0x8000; //Real<0

  if ( XorMask ) //Real<0
  {
    Vector_re = -Vector_re;
    Vector_im = -Vector_im;
  }

  if ( Vector_im < 0 )
  {
    IsNeg = 1;
    Vector_im = -Vector_im;
  }


  Vector_im = Min((uint16)Vector_im, 0x7FFF);
  Vector_re = Min((uint16)Vector_re, 0x7FFF);
 
  if ((uint16)Vector_im  >  (uint16)Vector_re>>(ATAN_FAST_ANGLE-1))
  {
    Result = ATan_BigAngle (Vector_re, Vector_im);
  }
  else
  {
    Result = ATan_SmallAngle (Vector_re, Vector_im);
  }

  if ( IsNeg )
    Result = -Result;

  //actually signed multiplication is acceptable
  return Result ^ XorMask;
}

