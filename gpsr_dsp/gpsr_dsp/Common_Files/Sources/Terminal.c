/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Terminal driver
 *   @file                   Terminal.c
 *   @author                 D. Paroshin, V. Yeremeyev
 *   @date                   28.06.2005
 *   @version                2.0
 */
/*****************************************************************************/

#include "Terminal.h"
#include "Comfuncs.h"
#include <stdio.h>

//==============================================================================
///  @brief      Read char string from host
///
///  @param[in]  pTerm  - pointer to instance
///  @param[out] str    - string read 
///  @param[in]  MaxLen - buffer size (maximum string length)
///  @return     operation status
//==============================================================================
tIOstat TRead(tTerminalDrv *pTerm, char *str, int MaxLen)
{
  // Q: zero char included in MaxLen?
  tTerminal *pTerminal= (tTerminal *) pTerm;
  uint16 readsize = Min(FIFO_GetFilledSize(&pTerminal->ReceiveQueue), MaxLen);
  FIFO_GetBuffer(&pTerminal->ReceiveQueue, (tFifoItem *)str, readsize);
  str[readsize] = 0;
  return io_OK;
}

//==============================================================================
///  @brief      Read binary data from host
///
///  @param[in]  pTerminal - pointer to instance
///  @param[out] pBuffer   - pointer to buffer
///  @param[in]  Count     - number of octets to write
///  @return     operation status
//==============================================================================
uint16 TReadBinary(tTerminalDrv *pTerminal, uint8 *pBuffer, uint16 Count)
{
  tTerminal *pTerm = (tTerminal *) pTerminal;
  uint16 readsize = Min(FIFO_GetFilledSize(&pTerm->ReceiveQueue), Count);
  FIFO_GetBuffer(&pTerm->ReceiveQueue, (tFifoItem *)pBuffer, readsize);
  return readsize;
}

//==============================================================================
/// @brief      Write char string to host
///
/// @param[in]  pTerm - pointer to instance
/// @param[in]  str   - string to write 
/// @param[in]  len   - string length
/// @return     operation status
//==============================================================================
tIOstat TWrite(tTerminalDrv *pTerm, const char *str, int len)
{
  tTerminal *pTerminal = (tTerminal *)pTerm;

  if(!FIFO_PutBufferVerified(&pTerminal->TransmitQueue, (tFifoItem *)str, len))
    return io_ERROR;
  
  return io_OK;
}

//==============================================================================
/// @brief      Write binary data to host
///
/// @param[in]  pTerm     - pointer to instance
/// @param[in]  pBuffer   - pointer to buffer to write 
/// @param[in]  Count     - number of octets to write
/// @return     operation status
//==============================================================================
uint16 TWriteBinary(tTerminalDrv *pTerm, const uint8 *pBuffer, uint16 Count)
{
  tTerminal *pTerminal = (tTerminal *)pTerm;
  
  return(FIFO_PutBufferVerified(&pTerminal->TransmitQueue, (tFifoItem *)pBuffer, Count) ?
         Count : 0);
}
