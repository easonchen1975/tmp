/*****************************************************************************
 *   Copyright (c) 2005 Spirit Corp.
 *****************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  UART driver structure and function definitions
 *                           @see Terminal.h
 *   @file                   UartTerminal.h
 *   @author                 V. Yeremeyev
 *   @date                   23.12.2005
 *   @version                1.1
 */
 /*****************************************************************************/

#ifndef _UART_TERMINAL_INCLUDED_
#define _UART_TERMINAL_INCLUDED_

#include "Terminal.h"

// Hardware routines

//=====================================================================================
/// @brief      Terminal driver initialization
///
/// @param[in]  pTerminal - pointer to terminal instance
/// @param[in]  pIntro    - reserved (not used)
/// @param[in]  pCaption  - terminal caption (GUI version only)
/// @param[in]  port      - terminal port
//=====================================================================================
EXTERN_C void UartTerminalInit(tTerminal *pTerminal, int8 BaudRate);

//=====================================================================================
/// @brief   Data tranzmit notification function
//
/// @param[in]  pTerminal - pointer to instance
//=====================================================================================
EXTERN_C void OnDataTransmitted(tTerminal *pTerminal);

//=====================================================================================
/// @brief   Data receave notification function
//
/// @param[in]   pTerminal - pointer to instance
/// @param[out]  flag      - pointer result flag
//=====================================================================================
EXTERN_C void OnDataReceived(tTerminal *pTerminal, uint8 *flag);

// UART driver functions

//=====================================================================================
/// @brief     Read char string from console
//
/// @param[in]   pTerminal  - pointer to instance
/// @param[out]  str        - pointer to string read from console
/// @param[in]   MaxLen     - maximum string length
/// @return      operation status
//=====================================================================================
tIOstat UartRead(tTerminalDrv *pTerminal, char *str, int MaxLen);

//=====================================================================================
/// @brief     Read binary buffer from console
//
/// @param[in]   pTerminal  - pointer to instance
/// @param[out]  pBuffer    - pointer to buffer read from console
/// @param[in]   Count      - number of bytes to be read
/// @return      operation status
//=====================================================================================
uint16  UartReadBinary(tTerminalDrv *pTerminal, uint8 *pBuffer, uint16 Count);

//=====================================================================================
/// @brief     Write char string in console
//
/// @param[in]  pTerminal  - pointer to instance
/// @param[in]  str        - pointer to string being written in console
/// @param[in]  MaxLen     - maximum string length
/// @return     operation status
//=====================================================================================
tIOstat UartWrite(tTerminalDrv *pTerminal, const char *str, int len);

//=====================================================================================
/// @brief     Write char string in console
//
/// @param[in]  pTerminal  - pointer to instance
/// @param[in]  pBuffer    - pointer to buffer being written in console
/// @param[in]  Count      - number of bytes to be write
/// @return     operation status
//=====================================================================================
uint16  UartWriteBinary(tTerminalDrv *pTerminal, const uint8 *pBuffer, uint16 Count);

#endif // !defined(_UART_TERMINAL_INCLUDED_)
