/*****************************************************************************
 *    Copyright (c) 2006 Spirit Corp.
******************************************************************************/
/**
 *   @project                GPS Receiver
 *   @brief                  Main manager
 *   @file                   MainManager.h
 *   @author                 M. Zhokhova
 *   @date                   14.02.2006
 *   @version                1.0
 */
/*****************************************************************************/

#ifndef _MAIN_MANAGER_H_
#define _MAIN_MANAGER_H_

#include "Global.h"
#include "DataForm.h"
#include "NavTask.h"
#include "Framework.h"
#include "..\OP\OPManager.h"

#define OUTPUT_UART 0
#define OUTPUT_CAN1  1
#define OUTPUT_CAN2  2
/// buffers for messages
typedef struct
{
  DATA_BIN     dataBin;  ///< for binary data
  DATA_STRING  dataStr;  ///< for string data
}
tMessage;

typedef struct 
{
  eFlag          BinStrFlag;           ///< binary or string message by RS422; default fg_OFF - string mode
  tMessage       MESSAGE;              ///< for binary or string messages

  DATA_STRING    TestStr;

  CONFIG_SETUP   config;               ///< configuration setup

  tInitMes       initMes;              ///< initial data message

  uint32         NewPeriodTime;        ///< new period fro navigation task solution by string mode
  eFlag          NewTime;              ///< flag

  uint8          Message;              ///< types of message for output after navigation task solution:
                                       ///< bit 0: SAT; bit 1: POS; bit 2: DOP;
                                       ///< bit 3: RAW; bit 4: OPP; bit 5: SQE

  uint32         PeriodRaw;            ///< period for raw data forming message by string mode

  tICDdata       data;                 ///< temporary buffers data for messages

  CONFIG_SETUP   config_new;           ///< new configuration setup
  eFlag          NewConfig;            ///< flag

  tFlagsControl  flags;                ///< control flags

  float32        timeTrackingSWI;      ///< time between function calls from SWITracking thread
}
tMainManager;

extern uint8 boot_status ;
extern int new_boot_config ;

// Alex, 20141119, Performance measurement for tracking thread
//#define UTILIZATION_ZONE	10
#define CPU_CYCLES_PER_MS	480000	// 480 MHz

extern uint16 pre_tracking_utilization[];
extern uint32 pre_max_utilization_time ;
extern uint16 pre_max_utilization ;


// Alex, 20140619, NEW TMTC ICD
#ifdef NEW_TMTC

	#define	DETAIL_MODE		1
	#define BRIEF_MODE		0

	#define TLM_TYPE_NAV	0
	#define TLM_TYPE_SCI	1
	#define TLM_TYPE_S_NAV	2
		
	extern int tmtc_mode ;
	extern int tlm_type ;
	extern int reset_type ;
	extern unsigned int nspo_sof ;
	extern Tm_Frame_Gpsr_Nav_Tlm gpsr_nav_tm_frame ;
	extern Tm_Frame_Gpsr_Short_Nav_Tlm gpsr_short_nav_tm_frame ;
	extern Tm_Frame_Gpsr_Sci_Tlm gpsr_sci_tm_frame ;	
	extern Tm_Frame_Gpsr_Tc_Ack gpsr_tc_ack_tm_frame ;
	extern Tm_Frame_Gpsr_Debug_Tlm gpsr_dbg_tm_frame ;
	
	EXTERN_C int dbg_printf(char *fmt, ...) ;

	EXTERN_C void output_new_tm_message(tMainManager      *pMainMgr,
         		                tFramework        *pFramework,
								tSatelliteManager *pSatMgr,
								tNavTask          *pNavTask,
								tOPManager        *pOPMgr);

	EXTERN_C void process_gpsr_config_tc(tMainManager *pMainMgr, void *p_tc, uint8 tc_id) ;	
	EXTERN_C void process_write_rffe_reg_tc(Tc_Frame_Write_Rffe_Reg *p_tc) ;
	EXTERN_C int process_soft_reset_tc(Tc_Frame_Gpsr_Soft_Reset *p_tc) ;
	EXTERN_C void update_myUtc(tSatelliteManager *pSatMgr) ;
	EXTERN_C void change_boot_config(int new_config) ;							
#endif	// end NEW_TMTC

//================================================================================
/// Main Manager initialization
///
/// @param[in]  pMainMgr  - pointer to instance
//================================================================================
EXTERN_C void MainMgr_Init(tMainManager *pMainMgr);           
                      
//================================================================================
/// Main Manager control
///
/// @param[in]  pMainMgr   - pointer to Main Manager object
/// @param[in]  pFramework - pointer to Framework object
/// @param[in]  pSatMgr    - pointer to Satellite Manager object
/// @param[in]  pNavTask   - pointer to Navigation Task object
/// @param[in]  pOPMgr     - pointer to OP Manager object
//================================================================================
EXTERN_C void MainMgr_Control(tMainManager      *pMainMgr,
                              tFramework        *pFramework,
                              tSatelliteManager *pSatMgr,
                              tNavTask          *pNavTask,
                              tOPManager        *pOPMgr);

//================================================================================
/// Set new period for navigation task solution
///
/// @param[in]  pMainMgr - pointer to Main Manager object
/// @param[in]  pNavTask - pointer to Navigation Task object
/// @param[in]  ms_count - current time [ms]
//================================================================================
EXTERN_C void MainMgr_PeriodNavTask(tMainManager *pMainMgr,
                                    tNavTask     *pNavTask,
                                    uint32        ms_count);

//==================================================================================
/// Time and Channels Status Data message forming 
/// RFFE Channels Range Measurements/Average Range Measurements
///
/// @param[in]  pTimeChStatus - pointer to instance
/// @param[in]  pRawChannels  - pointer to instance 
/// @param[in]  pSatMgr       - pointer to Satellite Manager object
/// @param[in]  time          - time of week
/// @param[in]  BitResult     - periodical BIT result
//==================================================================================
EXTERN_C void MainMgr_MessagesForm(tTimeChStatus     *pTimeChStatus,
                                   tChRM             *pRawChannels,
                                   tSatelliteManager *pSatMgr);

//=====================================================================================
/// Line of Sight Vectors Message
///
/// @param[in]  dataLOS        - pointer to instance
/// @param[in]  navtask_params - pointer to navigation task parameters
/// @param[in]  wn             - week number
/// @param[in]  t              - time of week
//=====================================================================================
EXTERN_C void MainMgr_LOS(tDataLOS       *dataLOS, 
                          tNavTaskParams *navtask_params, 
                          int16           wn, 
                          int64           t);

//==================================================================================
/// RFFE Channels Range Measurements for string mode
///
/// @param[in]  pSatMgr - pointer to Satellite Manager object
//==================================================================================
EXTERN_C void MainMgr_NMEARawMeasur(tSatelliteManager *pSatMgr);
#ifdef FASTER_TRACKING
EXTERN_C void MainMgr_NMEARawMeasur_2(tSatelliteManager *pSatMgr);
#endif
//==================================================================================
/// ECEF Navigation And Status Message
///
/// @param[in]  ecef      - pointer to instance
/// @param[in]  User      - User structure
/// @param[in]  NavCond   - navigation conditions
//==================================================================================
EXTERN_C void MainMgr_NavAndStatusECEF(tECEFNavStatus *ecef,
                                       USER            User,
                                       NAV_CONDITIONS  NavCond);

//==================================================================================
/// Geo Navigation And Status Message
///
/// @param[in]  geo       - pointer to instance
/// @param[in]  User      - User structure
/// @param[in]  NavCond   - navigation conditions
//==================================================================================
EXTERN_C void MainMgr_NavAndStatusGEO(tGeoNavStatus *geo,
                                      USER           User,
                                      NAV_CONDITIONS NavCond);

//==================================================================================
/// OE Navigation And Status Message
///
/// @param[in]  oe        - pointer to instance
/// @param[in]  User      - User structure
/// @param[in]  NavCond   - navigation conditions
//==================================================================================
EXTERN_C void MainMgr_NavAndStatusOE(tOENavStatus   *oe,
                                     USER            User,
                                     NAV_CONDITIONS  NavCond);

//==================================================================================
/// ECEF OP Navigation And Status Message
///
/// @param[in]  ecef       - pointer to instance
/// @param[in]  CoordValid - flag of valid user's coordinates
/// @param[in]  NavCond    - navigation conditions
/// @param[out] User_OP    - OP User structure
//==================================================================================
EXTERN_C void MainMgr_OPNavAndStatusECEF(tECEFOPNavStatus *ecef,
                                         NAV_CONDITIONS    NavCond,
                                         int8              CoordValid,
                                         USERop            User_OP);

//==================================================================================
/// Geo OP Navigation And Status Message
///
/// @param[in]  geo        - pointer to instance
/// @param[in]  CoordValid - flag of valid user's coordinates
/// @param[in]  NavCond    - navigation conditions
/// @param[out] User_OP    - OP User structure
//==================================================================================
EXTERN_C void MainMgr_OPNavAndStatusGEO(tGeoOPNavStatus *geo,
                                        NAV_CONDITIONS   NavCond,
                                        int8             CoordValid,
                                        USERop           User_OP);

//==================================================================================
/// OE OP Navigation And Status Message
///
/// @param[in]  oe         - pointer to instance
/// @param[in]  CoordValid - flag of valid user's coordinates
/// @param[in]  NavCond    - navigation conditions
/// @param[out] User_OP    - OP User structure
//==================================================================================
EXTERN_C void MainMgr_OPNavAndStatusOE(tOEOPNavStatus *oe,
                                       NAV_CONDITIONS  NavCond,
                                       int8            CoordValid,
                                       USERop          User_OP);

//==================================================================================
/// Almanac Message
///
/// @param[in]  mesAlm     - pointer to instance
/// @param[in]  pTopSystem - pointer to Top System structure
//==================================================================================
EXTERN_C void MainMgr_Almanac(tMesAlmanac *mesAlm,tTopSystem *pTopSystem);

//==================================================================================
/// Ephemeris Message
///
/// @param[in]  mesEph  - pointer to instance
/// @param[in]  pSatMgr - pointer to Top System structure
//==================================================================================
EXTERN_C void MainMgr_Ephemeris(tMesEphemeris *mesEph, tSatelliteManager *pSatMgr);

//===========================================================================
/// Write System files in sector FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
//===========================================================================
EXTERN_C void MainMgr_UploadFile(tFlashDrv *pFlashDrv);

//==========================================================================
/// Configuration setup structure initialize
///
/// @param[in]  pFlashDrv - pointer to instance
/// @param[in]  config    - pointer to configuration setup
/// @return     0 if check sum correct otherwise 1
//==========================================================================
EXTERN_C int MainMgr_ConfigInit(tFlashDrv *pFlashDrv, CONFIG_SETUP *config);

//====================================================================
/// Configuration setup write-in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  config    - pointer to configuration setup structure
//====================================================================
EXTERN_C void MainMgr_ConfigFlash(tFlashDrv    *pFlashDrv,
                                  CONFIG_SETUP *config);

//=============================================================================
/// Reads week data from FLASH sector
///
/// @param[in]  pFlashDrv - pointer to Flash Driver
/// @param[in]  weekData  - pointer to week data structure
//=============================================================================
EXTERN_C void MainMgr_WeekDataInit(tFlashDrv *pFlashDrv, tWeekData *weekData);

//====================================================================
/// Week data write-in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  weekData  - pointer to week data structure
//====================================================================
EXTERN_C void MainMgr_WeekDataFlash(tFlashDrv *pFlashDrv,
                                    tWeekData *weekData);

//=============================================================
/// Read ephemeris from FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pSatMgr   - pointer to Satellite Manager object
//=============================================================
EXTERN_C void MainMgr_EphemerisInit(tFlashDrv         *pFlashDrv,
                                    tSatelliteManager *pSatMgr);

//==============================================================
/// Read almanac from FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pSatMgr   - pointer to Satellite Manager object
//==============================================================
EXTERN_C void MainMgr_AlmanacInit(tFlashDrv         *pFlashDrv,
                                  tSatelliteManager *pSatMgr);

//====================================================================
/// Write ephemeris in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pTopSys   - pointer to Top System
//====================================================================
EXTERN_C void MainMgr_EphemerisFlash(tFlashDrv   *pFlashDrv,
                                     tTopSystem  *pTopSys);

//=======================================================================
/// Write almanac in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// @param[in]  pTopSys   - pointer to Top System
//=======================================================================
EXTERN_C void MainMgr_AlmanacFlash(tFlashDrv  *pFlashDrv,
                                   tTopSystem *pTopSys);

//====================================================================
/// Clear ephemeris and almanac in FLASH
///
/// @param[in]  pFlashDrv - pointer to Flash Driver object
/// return 0 if clearing processing, 1 if clearing finished
//====================================================================
EXTERN_C int MainMgr_EphAlmClear(tFlashDrv *pFlashDrv);

//========================================================================
/// @brief     Search string message
///
/// @param[in] pThis - pointer to UART driver
/// @param[in] data  - pointer to string data
/// @return    1 if message found otherwise 0
//========================================================================
EXTERN_C int FrameWork_SearchMesStr(tTerminal *pThis, DATA_STRING *data);

//=============================================================================
/// @brief     String message parser
///
/// @param[in] pMainMgr   - pointer to MainManager object
/// @param[in] pFramework - pointer to Framework object
//=============================================================================
EXTERN_C void MainMgr_ParserStr(tMainManager *pMainMgr, tFramework *pFramework,
                                tNavTask     *pNavTask, tOPManager *pOPMgr);

//========================================================================
/// @brief     Binary parser
/// 
/// @param[in] pMainMgr   - pointer to Main Manager object
/// @param[in] pFramework - pointer to Framework object
/// @param[in] pSatMgr    - pointer to Satellite Manager object
/// @param[in] pNavTask   - pointer to Navigation Task object
/// @param[in] pOPMgr     - pointer to OP Manager object
//========================================================================
EXTERN_C void MainMgr_ParserBin(tMainManager      *pMainMgr,
         		                    tFramework        *pFramework,
                  			        tSatelliteManager *pSatMgr,
		                            tNavTask          *pNavTask,
         		                    tOPManager        *pOPMgr);

#endif  // _MAIN_MANAGER_H_
